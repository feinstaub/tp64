Der Super-User root
===================

Wenn es heißt "gebe dein root-Passwort ein", dann ist folgendes gemeint:

"Das Root-Konto oder 'Superuser'-Konto ist das Benutzerkonto, das bei der Installation eines Betriebssystems angelegt werden muss und mit weitreichendsten Zugriffsrechten ausgestattet ist." (https://de.wikipedia.org/wiki/Root-Konto)

### Selbst installiertes Linux
Das root-Passwort hast du beim Installieren von openSUSE-Linux selber
festgelegt. Es ist dasselbe wie dein Benutzer-Passwort, da damals die
Option "gleiches Passwort für Systemadministrator verwenden" gewählt wurde.

### VirtualBox / osboxes-VM
Bei einem osboxes.org-Linux-Image lautet das root-Passwort: `osboxes.org`

(siehe http://www.osboxes.org/opensuse/)

Idealerweise wählt man sich für den persönlichen Computer ein persönliches,
nicht zu einfach zu erratendes root-Passwort, das aber auch nicht zu schwer zu tippen ist.

### root-Passwort-Wechsel
todo
