Internet: Datenaustausch
========================

<!-- toc -->

- [E-Mail](#e-mail)
- [FrameDrop](#framedrop)
- [Pastebin](#pastebin)
- [0bin.net](#0binnet)
- [Etherpad](#etherpad)
  * [Öffentlich](#offentlich)
  * [Privat](#privat)
- [Nextcloud](#nextcloud)

<!-- tocstop -->

E-Mail
------
Siehe [[Course:Einstieg_in_die_Programmierung_mit_Python/Eigenes_System/Internet-Tipps]] (inkl. Wegwerf-Mail-Accounts)

FrameDrop
---------
https://framadrop.org

Pastebin
--------
https://de.wikipedia.org/wiki/Pastebin

0bin.net
--------
Text, Bilder und Dateien client-seitig verschlüsselt und mit Expire-Zeit zum Austausch hochladen.

https://0bin.net

Etherpad
--------
* https://de.wikipedia.org/wiki/Etherpad
* http://etherpad.org/
    * Anbieter: https://github.com/ether/etherpad-lite/wiki/Sites-that-run-Etherpad-Lite

### Öffentlich
'''Achtung:''' Inhalte, die in ein öffentliches Etherpad geladen werden, können potentiell nie mehr gelöscht werden. Das heißt, genau drauf achten, die eigenen Daten und vor allem Informationen über unbeteiligte Dritte dort '''nicht''' hineinzuschreiben!

Öffentliche Etherpads sind geeignet zum Austausch unpersönlicher, rein technischer Informationen.

* Mit Bilder-Unterstützung
    * https://etherpad.net
        * Über diese Seite: https://factor.cc/index.php

* Die meisten sind ohne Bilder-Unterstützung
    * z. B. https://yourpart.eu/
    * z. B. http://etherpad2-p2plab.rhcloud.com

### Privat
Da Etherpad eine freie Software ist, kann man auch einen eigenen Server aufsetzen, wo man die Verwendung der Daten unter eigener Kontrolle hat.

Nextcloud
---------
* siehe README-nextcloud.md
