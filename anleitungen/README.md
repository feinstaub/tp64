Anleitungen
===========

GNU/Linux installieren:

* [GNU/Linux auf USB-Stick installieren](../themen/linux-installieren/opensuse-auf-usb-stick)

GNU/Linux, KDE/Plasma verwenden:

* [Offline-Wörterbuch mit KDing](offline-woerterbuch-kding)
* [Sprache umstellen](sprache-umstellen)
* todo: Programme entdecken mit YaST (View -> Package Groups etc.)
* [Schriftarten ansehen und neue installieren](schriftarten-beginner-opensuse)
* [Screenshot aufnehmen](screenshots-und-videos)
* [Dateien suchen](dateien-suchen)

Programmieren:

* [Python mit KDevelop](python-mit-kdevelop)
* [KDE-Plasma und KDE-Applikationen kompilieren](../themen/kde/kde-kompilieren.md)

Sprachen:

* [Texte von Englisch auf Deutsch übersetzen](translate-to-german)

Programme:

* 3D-Modellierung mit [Blender](blender-mini-howto)
* Texte schreiben, Tabellen und Präsentationen erstellen mit [LibreOffice](../themen/libreoffice)
* Texte erstellen und programmieren mit [Kate](../themen/kate)
* Im Internet browsen mit [Mozilla Firefox](mozilla-firefox)
* Im Netzwerk chatten mit [Beebeep](beebeep-chat)

Technik:

* [Bash - Hintergrundjobs mit Katze](bash-background-jobs-mit-oneko) - todo: move to ShellTux
* [Markdown für Einsteiger](markdown-primer)

Fortgeschritten:

* [USB-Stick klonen](usb-stick-klonen)

Projekte:

* [Projekt Oma-Linux](../themen/projekt-oma-linux)
* Linux-basierte Smartphone-Apps mit Plasma Mobile: siehe [11](einstieg-jgst-11.md)

Hinweise:

* Die meisten Anleitungen beziehen sich auf das neuste openSUSE Tumbleweed Linux.

Weitere Ideen / TODOs
---------------------
### Sortier-Algorithmus
* Was ist ein Algorithmus?
* Denke dir einen Sortieralgorithmus aus...
* ...

### strace - Highscore-Datei finden
**strace** - trace system calls and signals

Highscore-Datei finden

### Wine
* https://www.winehq.org/
