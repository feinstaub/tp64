Lehrmaterial-Sammlungen
=======================

Filius - Die Lernanwendung zu Rechnernetzen
-------------------------------------------
* http://www.lernsoftware-filius.de
* basiert auf Java

Javascript
----------
* Deutsch:
    * http://www.jshero.net

Blogs von Lehrenden
--------------------
* https://antlarr.io/
    * 2019: "The Browser Tutorial"

Weiteres
--------
* https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Unsortiert/Externe_Materialien
    * todo: move to here

Unsortiert
----------
* Was ist Freie Software? Und Material
    * [Werkzeugkasten Freie Software](https://www.technologiestiftung-berlin.de/fileadmin/daten/media/publikationen/161213_Medien_in_die_Schule-Werkzeugkasten_Freie_Software.pdf)
        * Geschichte, Philosophie, Freie Software in der Bildung, Übersicht über Freie Software für Schule und Unterricht
        * todo: auswerten

* https://www.wissensfabrik-deutschland.de
    * "In dem Bildungsprojekt IT2School entdecken, erforschen und erlernen SchülerInnen Informationstechnologie auf spielerische Weise. Dabei behandeln sie grundlegende Fragen zu Kommunikation, Daten, Programmiersprache und dem Zusammenspiel von Hard- und Software."

* [Vorteile + Hindernisse Freier Software in der Bildung (CLT 2015)](https://www.youtube.com/watch?v=uyDi6pkc6l8), Guido Arnold
    * 9:30 min: Hinweise zu Auflistungen von verfügbarer freie Software: am besten siehe Wikipedia
        * weil eine eigene Liste ist schwer zu maintainen
        * wo?

* Programmieren ist ein Handwerk?
    * https://www.youtube.com/watch?v=dvwkaHBrDyI
        * Talentierte lernen am besten Programmieren, indem sie bei vorhandenen Programmen Code beisteueren; unter Anleitung

* Englisch-sprachiger Raum mit teilweise deutschen Übersetzungen
    * https://www.freecodecamp.org
        * "freeCodeCamp is a small nonprofit that helps busy people learn to code for free."
        * HTML5 und CSS
        * Bootstrap
        * JQuery
        * JavaScript
        * OO und Functional
        * Algorithms
        * Interaktive Elemente
    * khanacademy.org
        * Beispiele auf Deutsch:
            * **Binärsuche**: https://de.khanacademy.org/computing/computer-science/algorithms/binary-search/a/binary-search
                * mit interaktiven Elementen
                * Nachteile: immer wieder prominente Verknüpfung mit Google, Facebook und Twitter
        * Meta
            * "Why is Khan Academy a not-for-profit?"
                * https://khanacademy.zendesk.com/hc/en-us/articles/202483500-Why-is-Khan-Academy-a-not-for-profit-
            * "Can I use Khan Academy’s videos/name/materials/links?"
                * https://khanacademy.zendesk.com/hc/en-us/articles/202262954-Can-I-use-Khan-Academy-s-videos-name-materials-links-
                * "Is it okay to monetize Khan Academy videos by offering services around it?"
                    * https://www.quora.com/Is-it-okay-to-monetize-Khan-Academy-videos-by-offering-services-around-it
            * Code: http://khan.github.io/
                * https://khan.github.io/KaTeX/
            * Offline-Version: https://learningequality.org/ka-lite/

Ideen
-----
* https://www.didacta-digital.de/lernen-lehren/schlaflos-in-moskau-programmieren-in-der-schule, 2017
