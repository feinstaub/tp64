FLTK
====

<!-- toc -->

<!-- tocstop -->

FAQ
---
### Intro
* https://www.fltk.org/doc-1.3/intro.html
* https://www.fltk.org/doc-1.3/basics.html#basics_eventloop
* Examples
    * https://www.fltk.org/doc-1.3/examples.html
    * siehe Inst.

### Wie funktioniert resize?
* https://www.fltk.org/articles.php?L415+I80+T+P1+Q
* ?
    * https://typeclasses.com/timepieces/lesson-4
* examples
    * resizebox

### Callbacks
* https://www.fltk.org/doc-1.3/common.html#common_callbacks

### Button with Image
* https://stackoverflow.com/questions/43593658/fltk-button-with-an-image

### Animation
* http://seriss.com/people/erco/fltk/#AnimateDrawing

### Custom Draw
* override draw()
* Pixel malen: begin_points()
* https://www.fltk.org/doc-1.3/drawing.html#ssect_Fast

### Erco's FLTK Cheat Page
* http://seriss.com/people/erco/fltk

### fullscreen
* https://www.fltk.org/doc-1.3/classFl__Window.html#ac3f59c5dfbf15d4ae5485c5a8c73b0c3

### Höhe/Breite vom Fenster/Widget
* https://www.fltk.org/doc-1.3/common.html#common_sizeposition
* h(), w() https://www.fltk.org/doc-1.3/classFl__Widget.html#aeeabd61e2d10c76cd6600dbbfe951f71
* box() https://www.fltk.org/doc-1.3/classFl__Widget.html#a9d1f48fbbe4d885e81df38b2a93b614f

### Position auf dem Bildschirm
* x_root() https://www.fltk.org/doc-1.3/classFl__Window.html#a26be3fa772a044d4c922b261b1df60bd

### GUI designer
* https://www.fltk.org/doc-1.3/fluid.html#fluid_what_is_fluid

### todo: Overlay_Window
* https://www.fltk.org/doc-1.3/classFl__Overlay__Window.html
    * flush()

### Timer, Timeout
* add_timeout() https://www.fltk.org/doc-1.3/classFl.html#a23e63eb7cec3a27fa360e66c6e2b2e52

### Farben ändern
* global: background(): https://www.fltk.org/doc-1.3/classFl.html#a422da0dfc6aa51721e7c9a6ccf5b90ef
