#include "catch.hpp"

// https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md

#include "tictactoe10.h"

unsigned int Factorial( unsigned int number ) {
    return number <= 1 ? number : Factorial(number-1)*number;
}

TEST_CASE("Factorials are computed", "[factorial]") {
    REQUIRE(Factorial(1) == 1);
    REQUIRE(Factorial(2) == 2);
    REQUIRE(Factorial(3) == 6);
    REQUIRE(Factorial(10) == 3628800);
}

TEST_CASE("find_x_pos", "[find_x_pos]") {
    int len;
    int x;
    x = game::find_x_pos(&len, "_#_#");
    REQUIRE(x == 1);
    REQUIRE(len == 3);

    x = game::find_x_pos(&len, "##");
    REQUIRE(x == 0);
    REQUIRE(len == 2);

    x = game::find_x_pos(&len, "");
    REQUIRE(x == -1);
}
