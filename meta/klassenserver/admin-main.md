ODROID Admin Main
=================

Überblick
---------
* Erstmalige Einrichtung eines neuen ODROID mit Plasma Netrunner installiert:
    * [admin-odroid-vorbereiten.md](admin-odroid-vorbereiten.md)
    * Einloggen: $ ssh ag@ag-pc

* Notwendige Pakete für tp64 installieren:
    * $ sudo apt-get install git cowsay xcowsay figlet moon-buggy moon-lander oneko
    * Ggf. später für ShellTux:
        * [admin-odroid-python3.5-installieren.md](admin-odroid-python3.5-installieren.md)

* Freigabe für Downloads einrichten:
    * [admin-odroid-samba-freigabe.md](admin-odroid-samba-freigabe.md)

* Neuen Schüler-User anlegen:
    * [admin-new-student-account.md](admin-new-student-account.md)
    * siehe auch [README.md](README.md)

* tp64 an gemeinsamer Stelle installieren:
    * $ cd /home/ag
    * $ git clone https://framagit.org/feinstaub/tp64.git
    * Danach:
        * $ cd tp64
        * $ git pull

* System runterfahren:
    * $ sudo shutdown now

X-Forwarding, ssh, Qt Application Theme
---------------------------------------
### Einloggen mit X
    $ ssh -X ag@ag-pc
    $ ag@ag-pc:~$ dolphin
        Falls sich Dolphin ohne Styles und Icons öffnet, dann siehe unten QT_QPA_PLATFORMTHEME
    $ ag@ag-pc:~$ export QT_QPA_PLATFORMTHEME=kde
    $ ag@ag-pc:~$ dolphin
        Dolphin öffnet sich mit korrekten Styles und Icons

### QT_QPA_PLATFORMTHEME von Remote oder Xfce
QT5 Theme richtig einstellen für z. B. Icons in Kate und Dolphin

Hinweis `export XDG_CURRENT_DESKTOP=kde` zwar geht auch, aber das wird dann z. B. von XFCE wieder überschrieben:

XDG_CURRENT_DESKTOP=XFCE

So geht es nicht unter XFCE: in die .profile eintragen oder die .bashrc, sondern so: in die .xsessionrc

    touch ~/.xsessionrc
    # dort reinschreiben:
    export QT_QPA_PLATFORMTHEME=kde

siehe http://www.webupd8.org/2015/11/configure-qt5-application-style-icons.html
siehe https://superuser.com/questions/990860/setting-path-variable-in-xfce4

Für andere User kopieren (gilt nur fuer direktes Anmelden am Rechner):

    chmod go+rw
    # Attribute beibehalten, sonst gehört die Datei root
    U=user1 ; sudo cp -a .xsessionrc /home/$U/ ; sudo chown $U /home/$U/.xsessionrc ; chgrp users /home/$U/.xsessionrc

Wenn von Remote über X-Session (z. B. von Windows aus), dann muss man die .profile bearbeiten:

    U=tu2 ; cat /home/$U/.profile ; read ; sudo bash -c "echo 'export QT_QPA_PLATFORMTHEME=kde' >> /home/$U/.profile"

TODO: write while loop and check if alreay exits

Mögliche Themen
---------------
* shelltux-Tutorial
* Remote Python programmieren lassen per Kate
* für jeden Teilnehmer User anlegen, solange Rechner nur Windows können
* Lego-Roboter
* Kleines Labyrinth aufbauen und mit Hilfe von bash, mc durchsuchen lassen.
    * Dateitypen mit file, Dateien entpacken etc. Einloggen mit ssh oder so

* Alternative zu "Einloggen auf dem ODROID von Windows-Rechner"
    * Cygwin/Portable
    * Logge dich ein auf IP ... mit gast/gast (TODO: einrichten)
    * TODO: howto SSH
    * Script ausführen: $ sudo useradd ... username ... password
    * Tutor holen (gibt root-Passwort ein)

### "UNIX Add Existing User To A Secondary Group"
* https://www.cyberciti.biz/faq/unix-add-user-to-group/
    * siehe man usermod
        * usermod -Ga gruppe1,gruppe2 user
            * ohne a wird der User aus den nicht gelisteten Gruppen entfernt

### Remote Audio
* todo: wie kann man über ssh -X Audio übertragen?
* Probleme:
    * https://askubuntu.com/questions/70560/why-am-i-getting-this-connection-to-pulseaudio-failed-error/172888
    * http://psung.blogspot.de/2011/09/network-audio-with-pulseaudio-made.html
* Übergangslösung: python http server und dort Dateien bereitstellen
