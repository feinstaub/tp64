Weitere: Kurse / Workshops
==========================

* Unsortierte Gedankenstütze zu Workshops etc. zu folgenden Themen
    * [Lokale Datensicherung im Privatumfeld](workshop-lokale-datensicherung)
    * [Smartphone und Privatsphäre](workshop-smartphone)
    * [Freie Software: ein Überblick](fs-overview)
    * [Freie Software: ein Workshop](fs-workshop)
* [Lizenz](LIZENZ)
