Beispiel neues Smartphone 2017 (2)
==================================

Kriterien
---------
* Gebrauchtes Gerät
* Preis nicht viel mehr als 100 EUR
    * wenn man weniger Leistung akzeptiert, geht es auch billiger
* Gängiges, oft verwendetes Modell, aber nicht von Google
* Kompatibel mit LineageOS
    * mit der neusten LineageOS-Version, siehe z. B. https://download.lineageos.org/d855
* Ohne SIM-Lock
* Gute Ausstattung, z. B.
    * (min 2 GB RAM)
        * Vergleich Samsung S2: 1 GB
    * (interner Speicher 16 GB)
    * (Vorder- und Rückkamera)
    * GPS
    * WLAN
    * Bluetooth
    * Auswechselbarer Akku (nicht verklebt o. ä.)
    * Slot für externe SD-Karte
* Einige Tage Recherche-Arbeit reinstecken
* Nicht zu groß
    * Samsung Galaxy S2 ist ca. 12,5 cm hoch
    * LG G3 D855 14,3 cm (7,5 cm breit)
        * 2 oder 3 GB
* Akkulaufzeit?
    * ca. 1 Tag
        * http://www.giga.de/smartphones/lg-g3/specials/lg-g3-akku-kapazitaet-und-laufzeit/
        * https://www.androidpit.de/lg-g3-test
        * http://www.chip.de/artikel/LG-G3_32GB-Handy-Test_69729394.html
    * Alternativen
        * https://www.androidpit.de/android-smartphones-mit-der-besten-akkulaufzeit
            * Motorola Moto E 2015 -> LineageOS-kompatibel, 1 GB RAM
    * siehe auch: https://www.androidpit.de/das-ende-laengerer-akkulaufzeiten-willkommen-in-der-stagnation

Beispiel: "LG G3 D855" ("damals saugut" / LG-Flaggschiff)

* Auf dem Markt seit Mai 2014
    * d. h. heute immer noch sehr gut brauchbar
* Kompatibel mit LineageOS?
    * ja, siehe https://wiki.lineageos.org/devices/
    * Rooten, Installation
        * siehe https://wiki.lineageos.org/devices/d855
            * CPU: Quad-core Krait 400 2.5 GHz
* [Offizielle Produktseite](http://www.lg.com/pa_en/cellphones/lg-G3-D855-Titanium)
* [ebay-Angebot 10. Dez. 2017](https://www.ebay.de/itm/LG-G3-D855-16GB-Titan-ohne-Simlock-top-Zustand/253287105423)

Überlegungen
------------
* Gebraucht bei E-Bay kaufen?
    * Das heißt, man hat keine Hersteller-Garantie mehr
        * Für was benötigt man überhaupt Hersteller-Garantie?
            * Wenn etwas *nicht selbstverschuldet* innerhalb der Gewährleistung von 2 - 3 kaputt geht
        * Was geht meistens am Smartphone kaputt oder warum?
            * Bildschirm (runtergefallen, draufgesetzt)
                * Diese Sachen muss man sowieso selber bezahlen
            * Kopfhörerstecker, USB-Stecker locker
                * aber lässt man das dann einsenden?! -> nein, wegen Daten

* Zusätzlich zur eingebauten SD-Karte eine eigene kaufen?
    * Könnte bei Datenrettung hilfreich sein, falls das Gerät(edisplay) kaputt geht und man kein aktuelles Backup hat

Reparieren
----------
* z. B. https://www.doc-phone.de/
    * "Gebraucht kaufen?" (teurer, aber dafür bessere Garantie? / Noch keine Erfahrung damit)
        * https://www.rebuy.de
            * [Beispiel via ebay](http://www.ebay.de/itm/LG-D855-G3-16GB-titan-voll-funktionsfahig/352181808543)
        * https://www.flip4new.de

Sonstiges
---------
* Handy-Finder, z. B. nach Abmessungen filtern
    * http://www.areamobile.de/handyfinder
