#include "tictactoe_easy.h"
#include "tictactoe_hard.h"
#include "hangman.h"

#include <iostream>
#include <string>

using namespace std;

void start(){
    int i=0;
    string eingabe;

    while(i==0){
        cout << "Wähle ein Spiel: (gebe help für hilfe ein)" << endl;
        cin >> eingabe;

        if(eingabe=="ttt"){
            tictacto();
        }

        if(eingabe=="sp"){
            tictacto_hard();
        }

        if(eingabe=="hm"){
            hangman_start();
        }
        if(eingabe=="leaf"){ // leave
            i++;
        }
    }
}

int main (){
//     tictacto();
//     tictacto_hard();
    start();
    return 0;
}
