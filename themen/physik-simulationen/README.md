Physik-Simulationen
===================

Tools
-----
* **Kmplot**: "Mathematical function plotter for KDE"
* **Labplot**: "KDE-application for interactive graphing and analysis
    of scientific data. LabPlot provides an easy way to create, manage
    and edit plots. It allows you to produce plots based on data from a
    spreadsheet or on data imported from external files. Plots can be
    exported to several pixmap and vector graphic formats."
    * 2017: https://labplot.kde.org/2017/04/09/labplot-2-4-released (inkl. Demo-Video für ein Feature)
* Weitere: https://userbase.kde.org/Applications/Science

Altes Zeug
----------
* **planets** im Education-Repository - Simuliert Planetenbahnen
    * schon etwas älter
    * Bedienung nicht ganz einfach
    * Anleitung siehe http://sodilinux.itd.cnr.it/sdl6x2/documentazione/planets/note_planets.odt (2003)
    * oder $ man planets
