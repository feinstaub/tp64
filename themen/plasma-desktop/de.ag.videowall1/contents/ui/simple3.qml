import QtQuick 2.0
import QtMultimedia 5.0

Rectangle {
    color: "#000000"

    MediaPlayer {
        id: mediaplayer1
        source: "videos/video1.ogv"
        autoPlay: true
        loops: MediaPlayer.Infinite
        volume: 0.0
    }

    VideoOutput {
        source: mediaplayer1

        // Das ganze Rechteck soll mit dem Video gefüllt sein:
        anchors.fill: parent

        // Füllungsmodus:
        fillMode: VideoOutput.PreserveAspectFit
    }
}
