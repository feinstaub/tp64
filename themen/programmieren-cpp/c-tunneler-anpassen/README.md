Tunneler anpassen
=================

Ein Programm kompilieren und anpassen am Beispiel Tunneler. Programmiersprache C

<!-- toc -->

- [Quellcode holen und bauen](#quellcode-holen-und-bauen)
  * [Quellcode-Paket holen](#quellcode-paket-holen)
  * [Quellcode-Archiv entpacken](#quellcode-archiv-entpacken)
  * [Quellcode kopieren ===](#quellcode-kopieren-)
  * [Vorbereiten des Bauens](#vorbereiten-des-bauens)
  * [Projekt bauen und ausführen](#projekt-bauen-und-ausfuhren)
- [Programm analysieren und ändern / Aufgaben](#programm-analysieren-und-andern--aufgaben)
  * [Titel ändern](#titel-andern)
  * [Aufgabe: x,y](#aufgabe-xy)
  * [Farbe ändern](#farbe-andern)
  * [Geschwindigkeit ändern](#geschwindigkeit-andern)
  * [Englische Vokablen](#englische-vokablen)
  * [Änderungen und Original vergleichen](#anderungen-und-original-vergleichen)
- [Ausblick](#ausblick)
  * [Ideen zur Umsetzung](#ideen-zur-umsetzung)
- [Nächste Schritte](#nachste-schritte)

<!-- tocstop -->

**Ziel:** Lernen wie vorhandene Programme an eigene Bedürfnisse angepasst werden können.

```
_______________________________
< Und nun zum Tunneler-Spiel... >
-------------------------------
   \
    \
     .--.
    |o_o |
    |:_/ |
    //   \ \
    (|     | )
    /'\_   _/`\
    \___)=(___/

                            ______________
                            < Sehr schön. >
                            --------------
                                   \   ^__^
                                    \  (oo)\_______
                                       (__)\       )\/\
                                           ||----w |
                                           ||     ||
_________________________________________
/ Das Original-Tunneler wurde in Turbo    \
| Pascal programmiert und war auf DOS     |
| beliebt. Es wurde von Taneli Kalvas aus |
| Finnland im Jahr 2003 mit der           |
| Cross-Platform-Bibliothek SDL in der    |
\ Sprache C nachprogrammiert.             /
-----------------------------------------
\
 \
     .--.
    |o_o |
    |:_/ |
    //   \ \
    (|     | )
    /'\_   _/`\
    \___)=(___/

                            ____________
                            < Und jetzt? >
                            ------------
                                   \   ^__^
                                    \  (oo)\_______
                                        (__)\       )\/\
                                            ||----w |
                                            ||     ||
________________________________________
/ Wir holen uns den freien Quellcode und \
| machen unsere eigenen Änderungen an   |
\ dem Programm.                          /
----------------------------------------
\
 \
     .--.
    |o_o |
    |:_/ |
   //   \ \
   (|     | )
   /'\_   _/`\
   \___)=(___/
```

## Quellcode holen und bauen
### Quellcode-Paket holen
Erst schauen wir, was es für Pakete gibt, die den Namen "tunneler" enthalten.

    $ zypper se tunneler

Ausgabe:

    Loading repository data...
    Reading installed packages...

    S | Name     | Summary                          | Type
    --+----------+----------------------------------+-----------
    i | tunneler | Clone of legendary Tunneler game | package
    | tunneler | Clone of legendary Tunneler game | srcpackage

Das "i" in der ersten Spalte bedeutet, dass das Paket "tunneler" installiert ist.

Das Paket in der zweiten Zeile ist das '''Quell-Paket''', siehe "Type" in der letzten Spalte (merkwürdig: auch wenn man das Paket im nächsten Schritt installiert, erscheint kein "i" in der ersten Spalte).

Das Quellpaket installieren wird in zwei Schritten mit der source-install-Option:

1) Abhängigkeiten installieren

    $ sudo zypper source-install tunneler

2) Quell-Code ins eigene Home-Verzeichnis kopieren:

    $ zypper source-install tunneler

Die mit source-install geholten Quellpakete befinden sich dann hier: '''~/rpmbuild/SOURCES/'''

Der Quellcode von Tunneler ist in dieser Datei: '''tunneler-1.1.1.tar.bz2''' (Versionsnummer kann abweichen)

Die Datei ist ein Archiv und muss erst entpackt werden, damit wir weiterarbeiten können. Das machen wir im nächsten Abschnitt.

### Quellcode-Archiv entpacken
Die im Verzeichnis ~/rpmbuild/SOURCES/ befindliche Datei '''tunneler-1.1.1.tar.bz2''' enthält die Quellcode-Dateien, die mit bz2 komprimiert gepackt sind. Diese müssen erst entpackt werden.

Erst ins richtige Verzeichnis wechseln:

    $ cd ~/rpmbuild/SOURCES/

(Die meisten Fehler passieren, wenn man nicht im richtigen Verzeichnis ist. Das Verzeichnis sieht man links neben dem Prompt oder man fragt es mit dem Befehl '''pwd''' ab.)

Schauen, ob die Datei da ist:

    ~/rpmbuild/SOURCES> ls -l
    total 784
    -rw-r--r-- 1 gregor users  78228 Aug 14  2009 tunneler-1.1.1.tar.bz2
    -rw-r--r-- 1 gregor users    154 Aug 14  2009 tunneler.desktop
    -rw-r--r-- 1 gregor users   4675 Aug 14  2009 tunneler.svg

Entpacken:

    $ tar xvjf tunneler-1.1.1.tar.bz2

Ein neuer Ordner "tunneler-1.1.1" wurde erstellt. Inhalt zur Kontrolle auflisten.

1) Sind wir noch im richtigen Verzeichnis?

    $ pwd
    /home/<dein_name>/rpmbuild/SOURCES/

2) Auflisten:

    $ cd tunneler-1.1.1
    $ ls -l

    -rwxr-xr-x 1   6404 Jun  3  2007 acinclude.m4
    -rw-r--r-- 1  31565 Jun  5  2008 aclocal.m4
    -rw-r--r-- 1      0 Jun  3  2007 AUTHORS
    -rw-r--r-- 1      0 Jun  3  2007 ChangeLog
    -rwxr-xr-x 1 142434 Jun  5  2008 configure
    -rw-r--r-- 1    444 Jun  5  2008 configure.in
    -rw-r--r-- 1  15119 Jun  3  2007 COPYING
    -rwxr-xr-x 1  17574 Sep 24  2007 depcomp
    -rw-r--r-- 1   9416 Sep 24  2007 INSTALL
    -rwxr-xr-x 1  13184 Sep 24  2007 install-sh
    -rw-r--r-- 1     14 Jun  3  2007 Makefile.am
    -rw-r--r-- 1  17567 Jun  5  2008 Makefile.in
    -rwxr-xr-x 1  11135 Sep 24  2007 missing
    -rw-r--r-- 1      0 Jun  3  2007 NEWS
    -rw-r--r-- 1   2840 Jun  5  2008 README
    drwxr-xr-x 2   4096 Jun  5  2008 src

### Quellcode kopieren ===
Wir fangen in den nächsten Abschnitten an, die Quellcode-Dateien zu modifizieren. Damit wir im Nachhinein besser vergleichen können, was im Vergleich geändert wurde, fertigen wir eine Kopie des Quellcode-Ordners an.

Den Ordner "tunneler-1.1.1" umbenennen in "tunneler-2":

    $ cd ~/rpmbuild/SOURCES/
    $ mv tunneler-1.1.1 tunneler-2

Das Quellcode-Archiv nochmal entpacken:

    $ tar xvjf tunneler-1.1.1.tar.bz2

Wir haben jetzt zwei Ordner mit demselben Inhalt. Den "tunneler-1.1.1" lassen wir so, wie er ist. Im Folgenden arbeiten wir in "tunneler-2".

(Die Vorgehensweise bis hierhin ist für jedes openSUSE-Paket ähnlich durchzuführen.)

### Vorbereiten des Bauens
Bevor aus dem Quellcode eine ausführbare Datei gebaut werden kann, muss die Umgebung initialisiert werden. Das machen wir mit den folgenden Befehlen:

    $ pwd
    /home/<dein_user>/rpmbuild/SOURCES
    $ cd tunneler-2/
    $ export LIBS=-lm
    $ ./configure

'''Erinnerung:''' Die Zeilen, die mit einem $ anfangen sind die Befehle, die ohne das $-Zeichen nach der Reihe abgetippt werden sollten. Bestätigung jeweils mit der Entertaste. Nach jedem Befehl ist die Konsolenausgabe zu prüfen, ob das Ergebnis ohne Fehlermeldungen ablief, ansonsten funktionieren die nächsten Befehle möglicherweise nicht richtig.

Was haben wir damit gemacht?

1) Schauen, ob wir im richtigen Verzeichnis sind.

2) Ins tunneler-2-Verzeichnis wechseln.

3) Eine notwendige Option setzen (warum? --> [https://build.opensuse.org/package/show?project=games&package=tunneler siehe hier]).

4) Projekt einmalig "konfigurieren".

(Die Vorgehensweise bis hierhin ist für eine bestimmte Menge von Programmen - die, die die GNU autotools verwenden - ähnlich)

### Projekt bauen und ausführen
    $ pwd
    /home/<dein_user>/rpmbuild/SOURCES/tunneler-2
    $ cd src
    $ make
    $ ./tunneler

Mit dem ersten Schritt wechseln wir in den src-Ordner.

Der make-Befehl startet die Kompilierung, denn da Tunneler ist in der Programmiersprache C geschrieben ist, muss der Quellcode erst kompiliert werden. Den Prozess nennt man allgemein "Bauen". Unter "Bauen" kann man aber auch das Programmieren einer Software an sich verstehen.

Mit dem letzten Befehl wird die Datei ''tunneler'', die durch den erstmaligen make-Aufruf erzeugt wurde, ausgeführt: Das Spiel startet. :-)

## Programm analysieren und ändern / Aufgaben

Wir arbeiten in folgendem Verzeichnis: '''~/rpmbuild/SOURCES/tunneler-2/src/'''

### Titel ändern
Wir machen nun unsere erste Änderung am Programm.

'''Öffne die Datei main.c''' mit Kate.

Suche nach der Zeile "PutStr( 18, 20, "Tunneler v." VERSION, color[12] );"

Tipp: Zeile 1014 (Erinnerung: mit F11 blendet man die Zeilennummerierung in Kate ein)

Tipp: mit Strg+G springt man in eine Zeile

Tipp: mit Strg+F sucht man in der Datei.

Die Code-Zeile sorgt dafür, dass auf dem Startbildschirm der Name des Spiels und seine Version angezeigt wird. Damit wir unser geändertes Programm vom Original unterscheiden können, mache folgendes:

'''Ändere die Zeile''' von vorher:

    PutStr( 18, 20, "Tunneler v." VERSION, color[12] );

auf nachher:

    PutStr( 5, 20, ">> Tunneler 2.0 <<" , color[12] );

'''Speichern.'''

'''Bauen:'''

    $ pwd
    /home/<dein_user>/rpmbuild/SOURCES/tunneler2/src
    $ make

Falls Fehler auftauchen, korrigieren.

'''Ausführen:'''

    $ ./tunneler

Man kann die beiden Befehle auch in einer Zeile ausführen, weil man das öfter braucht:

    $ make && ./tunneler

Wenn alles fehlerfrei lief, dann sieht der Startbildschirm von Tunneler und entsprechend anders aus.

### Aufgabe: x,y
Die Funktion PutStr, die du eben geändert hast, hat folgende Parameter:

    PutStr(x, y, text, farbe)

Ändere die Werte von x und y und schaue, was passiert.

### Farbe ändern
* Öffne die Datei '''graphics.c'''
* Suche nach /* Player 0 colors */
* Darunter ist eine Zeile, die mit color[30] = beginnt.
* Ändere folgendes von: 0xff, 0x00, 0x00...
    * Die Farben sind RGB-codiert, also Rot, Grün, Blau. 0x00 ist 0% von der betreffenden Farbe und 0xff ist 100%.
    * Das heißt dann also: 100% rot, 0 grün, 0 blau, also insgesamt rot
* ...auf: 0x00, 0x00, 0xff (also: 0 rot, 0 grün, 100% blau, insgesamt: blau)

'''Kompilieren und starten:'''

    $ make && ./tunneler

Aufgabe:

1) Ändere die Farbe des linken Spielers (inklusive der Umrahmung des Startfeldes) in '''gelb'''. Finde dazu heraus, welche Code-Zeilen für welche Teilfarbe verantwortlich sind.

2) Ändere die Farbe des rechten Spielers in '''weiß'''.

### Geschwindigkeit ändern
* '''game.h''' öffnen
* Suche nach:
** #define TANK_SPEED     130.0
** #define DIG_SPEED      60.0

### Englische Vokablen
* tank = Panzer
* speed = Geschwindigkeit
* dig = graben

=> Je mehr englische Vokabeln du kannst, umso einfacher ist es, Quellcode zu verstehen!

Ansonsten kann man englische Wörter online unter http://dict.leo.org nachschlagen. Oder besser offline mit '''KDing''' (Paket kding installieren, starten, danach Tastenkombination Meta+Umschalt+T verwenden).

Aufgabe:

* Vertausche die beiden Zahlen für TANK_SPEED und DIG_SPEED.
* Kompilieren, starten und ausprobieren:

    $ make && ./tunneler

Wie verhält sich der Panzer nun? Versuche den eigenen Kugeln davonzufahren.

### Änderungen und Original vergleichen
Da du nun einige Änderungen am Code gemacht hast, ist es gut zu sehen, was eigentlich alles im Vergleich zum Original geändert wurde.

Dies kannst du damit machen:

    $ cd ~/rpmbuild/SOURCES/
    $ kdiff3 tunneler-1.1.1 tunneler-2

(todo: mehr erklären; disable whitespace)

## Ausblick

### Ideen zur Umsetzung
* Selber den Code weiter '''analysieren''' und Änderungen vornehmen.
* Die Texte vom Englischen '''ins Deutsche übersetzen'''.
* '''Schriftart verkleinern''', damit mehr Text hinpasst.
* AI-Namen vergeben (Parameter -ai 1 um '''-ainame''' "Name1" erweitern)
* Per Tastendruck die '''Kartenübersicht einblenden'''
* Die Geschwindigkeit des Gefährts mittels '''Programmparameter''' einstellen.
* Die Geschwindigkeit des Gefährts mittels '''Sondertaste''' erhöhen und erniedrigen.
* Aktuellen **Punktestand** oben anzeigen
* Wenn mehr als 5 Punkte Unterschied => Automatischer **Fahrzeug-Energieausgleich**

## Nächste Schritte
* siehe README2.md
