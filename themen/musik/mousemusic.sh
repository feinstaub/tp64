#!/bin/bash

while true ; do
    # siehe https://stackoverflow.com/questions/8480073/how-would-i-get-the-current-mouse-coordinates-in-bash
        # Maustest: $ watch -t -n 0.1 xdotool getmouselocation
    eval $(xdotool getmouselocation --shell)
    ./playtone.sh $X $Y &      # besser siehe mousemusic3.sh
    ms=$(echo $Y/1000 | bc -l)
    sleep $ms                  # besser siehe mousemusic3.sh
done
