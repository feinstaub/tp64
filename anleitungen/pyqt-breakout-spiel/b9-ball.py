#!/usr/bin/python3

from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QPen, QBrush, QColor, QResizeEvent, QFont, QFontDatabase
from PyQt5.QtCore import Qt, QTimer, QRect
from PyQt5.QtMultimedia import QSound
import sys, random

class Ball:
    mx = 100     # Mittelpunkt x
    my = 100     # Mittelpunkt y
    r = 10     # Radius
    x = -1     # x links
    y = -1     # y oben
    x2 = -1    # x rechts
    y2 = -1    # y unten
    vx = 15
    vy = 10

    def set_mx(self, mx):
        self.mx = mx
        self.x = self.mx - self.r
        self.y = self.my - self.r
        self.x2 = self.mx + self.r
        self.y2 = self.my + self.r

    def set_my(self, my):
        self.my = my
        self.x = self.mx - self.r
        self.y = self.my - self.r
        self.x2 = self.mx + self.r
        self.y2 = self.my + self.r

    def next_step(self):
        self.set_mx(self.mx + self.vx)
        self.set_my(self.my + self.vy)

class Fenster(QWidget):
    # Feld
    fx = 2
    fy = 50
    fh = -1
    fw = -1
    fx2 = -1  # x rechts
    fy2 = -1  # y unten

    # Racket/Schläger
    rx = 20
    ry = 0
    rw = 100
    rh = 20

    # Ball
    b = Ball()

    sound1 = None

    def __init__(self):
        super().__init__()
        self.setGeometry(300, 300, 400, 300)
        self.setWindowTitle("Regen")
        self.setMouseTracking(True)
        self.setFocusPolicy(Qt.ClickFocus)

        # Timer starten:
        timer = QTimer(self)
        timer.setSingleShot(False)
        timer.timeout.connect(self.mainTimer) # siehe auch http://zetcode.com/gui/pyqt5/eventssignals/
        timer.start(50) # 50 Millisekunden

        self.sound1 = QSound("jump1.wav", self)

        self.show()

    def resizeEvent(self, e):
        w = e.size().width()
        h = e.size().height()
        self.fw = w - self.fx - 1
        self.fh = h - self.fy - 1
        self.fx2 = self.fx + self.fw
        self.fy2 = self.fy + self.fh
        self.ry = h - self.rh - 10
        self.update()

    def mouseMoveEvent(self, e):
        x = e.x()
        y = e.y()
        self.rx = x
        self.update()

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Left:
            self.rx = self.rx - 25
        elif e.key() == Qt.Key_Right:
            self.rx = self.rx + 25
        self.update()

    def mainTimer(self):
        self.b.next_step()
        # rechts
        if self.b.x2 >= self.fx2:
            self.b.set_mx(self.fx2 - self.b.r)
            self.b.vx = -self.b.vx
            self.sound1.play()
        # unten
        if self.b.y2 >= self.fy2:
            self.b.set_my(self.fy2 - self.b.r)
            self.b.vy = -self.b.vy
            self.sound1.play()
        # links
        if self.b.x <= self.fx:
            self.b.set_mx(self.fx + self.b.r)
            self.b.vx = -self.b.vx
            self.sound1.play()
        # oben
        if self.b.y <= self.fy:
            self.b.set_my(self.fy + self.b.r)
            self.b.vy = -self.b.vy
            self.sound1.play()

        self.update()

    def drawTopText(self, qp, text):
        qp.setPen(QColor(0, 0, 255))
        qp.setFont(QFont('Open Sans Light', 14))
        qp.drawText(0, 20, text)

    def drawBackground(self, qp):
        qp.setPen(QPen(QBrush(QColor(255, 0, 0)), 2.0))
        qp.setBrush(QBrush(QColor(0, 0, 0)))
        qp.drawRect(self.fx, self.fy, self.fw, self.fh)

    def drawRacket(self, qp):
        qp.setBrush(QBrush(QColor(255, 0, 0)))
        qp.drawRect(self.rx, self.ry, self.rw, self.rh)

    def drawBall(self, qp):
        qp.setPen(QPen(QColor(0, 0, 0), 2.0))
        qp.setBrush(QBrush(QColor(255, 0, 255)))
        qp.drawEllipse(self.b.x, self.b.y, self.b.r * 2, self.b.r * 2)

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawTopText(qp, "Ball: " + str(self.b.mx) + "," + str(self.b.my))
        self.drawBackground(qp)
        self.drawRacket(qp)
        self.drawBall(qp)
        qp.end()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWidget = Fenster()
    sys.exit(app.exec_())
