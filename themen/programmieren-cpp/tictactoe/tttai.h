#include <vector>

namespace game
{
// siehe auch
//  https://towardsdatascience.com/tic-tac-toe-creating-unbeatable-ai-with-minimax-algorithm-8af9e52c1e7d

const int winfields[8][3] = {   { 0, 1, 2 },
                                { 3, 4, 5 },
                                { 6, 7, 8 },

                                { 0, 3, 6 },
                                { 1, 4, 7 },
                                { 2, 5, 8 },

                                { 0, 4, 8 },
                                { 2, 4, 6 } };

int get_first_free(const std::vector<int>& feld)
{
    for (int i = 0; i < 9; i++)
    {
        if (feld[i] == -1)
            return i;
    }

    return -1;
}

/**
 *
 */
int get_index_to_close(int a, int b, int c, int ai)
{
    if (a == -1 && b == ai && c == ai)
        return 0;

    if (a == ai && b == -1 && c == ai)
        return 1;

    if (a == ai && b == ai && c == -1)
        return 2;

    return -1;
}

int find_first_closable_tworow(const std::vector<int>& feld, int ai)
{
    for (int i = 0; i < 8; i++) {
        int win_i_toclose = get_index_to_close(
            feld[winfields[i][0]], feld[winfields[i][1]], feld[winfields[i][2]], ai);

        if (win_i_toclose > -1)
            return winfields[i][win_i_toclose];
    }

    return -1;
}

int find_first_closable_tworow_gegner(const std::vector<int>& feld, int ai)
{
    return find_first_closable_tworow(feld, ai == 0 ? 1 : 0);
}


/**
 * feld:    Das Feld: -1, nicht besetzt, 0..n: Spieler0..n
 * ai:      Die Spieler-Nummer der KI
 */
int ai_move(const std::vector<int>& feld, int ai)
{
    // Stratgie:
    // 1. Drei zumachen, wenn möglich
    // 2. Verteidigung (3 zumachen verhindern)
    // 3. Angriff
    // 4. Default (das nächste freie Feld)

    // 1. Drei zumachen
    int move = find_first_closable_tworow(feld, ai);

    // 2. Verteidigung
    if (move == -1)
        move = find_first_closable_tworow_gegner(feld, ai);

    // 3. Angriff
    // TODO

    // 4. Default
    if (move == -1)
        move = get_first_free(feld);

    return move;
}
}
