#include <iostream> // cout, cin
#include <string>
#include <vector>
#include <termios.h>    // termios, tcgetattr, tcsetattr
#include <fcntl.h>      // fcntl
#include <unistd.h>  // STDIN_FILENO

using namespace std;

void zeichne_feld(const vector<int>& feld)
{
    cout << "           " << feld[0] << " | " << feld[1] << " | " << feld[2] << "\n";
    cout << "          ---|---|---\n";
    cout << "           " << 5 << " | " << 5 << " | " << 5 << "\n";
    cout << "          ---|---|---\n";
    cout << "           " << 5 << " | " << 5 << " | " << 5<< "\n";
}

class Person
{
public:
    Person(const string& n, int a, const string& f)
    {
        cout << "ctor für " << n << "\n";
        name = n;
        alter = a;
        set_farbe(f);
    }

    ~Person()
    {
        cout << "Auf Wiedersehen, " << name << "\n";
    }

    int alter;
    string name;

    void set_farbe(string f)
    {
        if (f == "grün")
            farbe = 1;
        else if (f == "rot")
            farbe = 2;
        else
            farbe = 0;
    }

    string get_farbe() const
    {
        if (farbe == 1)
            return "GRÜN";
        else if (farbe == 2)
            return "ROT";
        else
            return "unbekannt";
    }

private:
    int farbe;
};

// void print_person_lisa(const Person& p)
// {
//     Person p1 = p;
//     p1.name = "Lisa";
//     cout << p1.name << " ist " << p1.alter << " Jahre alt.\n";
// }

void print_person(const Person& p)
{
    cout << p.name << " ist " << p.alter << " Jahre alt. Die Lieblingsfarbe ist " << p.get_farbe() << ".\n";
}

struct Benutzer
{
    string name;
    string passwort;
};

void print_liste(const vector<Benutzer>& liste)
{
    for (Benutzer b : liste)
    {
        cout << b.name << " pw: " << b.passwort << endl;
    }
}

bool passwort_korrekt(const vector<Benutzer>& liste, const string& name, const string& pw)
{
    for (Benutzer b : liste)
    {
        if (b.name == name) {
            if (b.passwort == pw) {
                return true;
            }
        }
    }

    return false;
}

/**
 * geht nicht; clears the screen
 * https://stackoverflow.com/questions/4772061/how-do-i-use-getch-from-curses-without-clearing-the-screen/4772357#4772357
 */
// string read_pass1()
// {
//     filter(); // does not work
//     SCREEN *s = newterm(NULL, stdin, stdout);
//     noecho();
//     cbreak();
//     keypad(stdscr, TRUE); // 'arrow left' can be read with KEY_LEFT (instead of dealing with escape sequences)
//     string pw;
//     char ch;
//     while ((ch = getch()) != ERR) {
//         if (ch == '\n') {
//             break;
//         }
//         cout << "*" << flush;
//         pw += ch;
//     }
//     endwin();
//     return pw;
// }

/**
 * https://stackoverflow.com/questions/31925911/getch-not-working-without-initscr
 * "If you cycle between the canonical mode (requires new line for the process to begin) and non-canonocal mode (Keypress is more than enough)."
 *
 * https://stackoverflow.com/questions/421860/capture-characters-from-standard-input-without-waiting-for-enter-to-be-pressed
 */
string read_pass2()
{
    struct termios oldt, newt;
    //int oldf;

    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    //oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
    //fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

    string pw;

    while (true)
    {
        char ch = getchar(); // stdio.h
        pw += ch;
        cout << "*";
        if (ch == '\n') {
            break;
        }
//         if (ch == '\033')
//         {
//             printf("Arrow key\n"); ch=-1; break;
//         }
//         else if(ch == -1) // by default the function returns -1, as it is non blocking
//         {
//             continue;
//         }
    }

    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    //fcntl(STDIN_FILENO, F_SETFL, oldf);

    return pw;
}

int main()
{
    using namespace std;

    cout << "Bitte Passwort eingeben: ";
    string pw1 = read_pass2();
    cout << endl << pw1 << endl;

    return 0;

    vector<int> v;
    v.push_back(1);
    v.push_back(5);
    v.push_back(10);
    cout << v[0] << endl;
    cout << v[1] << endl;
    cout << v[2] << endl;
    for (int z : v)
    {
        cout << z << endl;
    }

    v.pop_back();

    for (int i; i < v.size(); i++)
    {
        cout << v[i] << endl;
    }

    if (v.size() == 0) {
        cout << "leer\n";
    } else {
        cout << "nicht leer\n";
    }

    vector<string> v2;
    v2.push_back("Hallo");
    v2.push_back("Test");
    v2.push_back(".");

    bool crash = false;

    if (crash) {
        //
        for (int i = 3; i <= 5; i++)
        {
            //cout << ".";
            //v2[i] = "Baum"; // Warum crasht das Programm nicht? -> undefined behaviour, -fstack-protector-all does not work here
            v2[i] = "Baum etwas länger"; // Crash!
            //v2.at(i + 3);
            //v2.assign(i + 3, "Baum"); // macht was ganz anderes
        }

        cout << "----\n";
        for (string s : v2)
        {
            cout << s << endl;
        }

        return 0;
    }

    Benutzer b { "Anton", "1234" };
    vector<Benutzer> benutzerliste;
    benutzerliste.push_back(b);
    b.name = "Berta";
    b.passwort = "hallo";
    benutzerliste.push_back(b);
    b.name = "Caesar";
    b.passwort = "ave";
    benutzerliste.push_back(b);

    print_liste(benutzerliste);

    string user;
    string pw;
    cout << "SPERRE!!! Bitte gib Benutzername und Passwort ein (mit Leerzeichen getrennt): ";
    cin >> user;
    cin >> pw;

    bool ok = passwort_korrekt(benutzerliste, user, pw);
    cout << "passwort_korrekt: " << user << " " << pw << " " << ok << "\n";

    if (!ok) {
        cout << "FALSCH!!!\n";
        return 0;
    }

    Person p1("Klaus", 14, "rot");
    Person p2("Lisa", 15, "grün");

    print_person(p1);
    print_person(p2);

    return 0;


    cout << "Willkommen bei TicTacToe\n";

    string name1;
    string name2;

//     cout << "Spieler 1? ";
//     cin >> name1;
//
//     cout << "Spieler 2? ";
//     cin >> name2;
//
//     cout << "Hallo " << name1 << " und " << name2 << endl;

    vector<int> feld = { -1, -1, -1,
                         -1, -1, -1,
                         -1, -1, -1 };

    cout << "Hier ist das Feld\n\n";
    zeichne_feld(feld);

    return 0;
}
