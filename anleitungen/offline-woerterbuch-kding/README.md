Offline-Wörterbuch mit Ding, KDing
==================================

<!-- toc -->

- [Ding](#ding)
  * [Installieren](#installieren)
  * [Starten](#starten)
- [Tastenkürzel festlegen](#tastenkurzel-festlegen)
  * [Global Shortcuts entdecken](#global-shortcuts-entdecken)
- [Stufe 2 - notify-send, xclip](#stufe-2---notify-send-xclip)
- [Stufe 3 - xlip-xcowsay-Rechner auf Meta+R](#stufe-3---xlip-xcowsay-rechner-auf-metar)
- [Stufe 4 - Töne mit Audacity und aplay auf Tasten legen](#stufe-4---tone-mit-audacity-und-aplay-auf-tasten-legen)
- [Weiteres](#weiteres)
  * [Online-Wörterbücher](#online-worterbucher)
  * [xclip für Bash](#xclip-fur-bash)
  * [KDing](#kding)
  * [KDing auf Qt5 portieren](#kding-auf-qt5-portieren)

<!-- tocstop -->

Ding/KDing ist ein Offline-Wörterbuch für Deutsch <-> Englisch.

Baustein: "Ding"
Baustein: "Custom Shortcut"

Ding
----
### Installieren
* ...

### Starten
* krunner -> ding
* Clipboard wird automatisch eingefügt

Tastenkürzel festlegen
----------------------
* Plasma **Custom Shortcuts** aufrufen
    * New
    * Trigger
        * Tastenkombination **Meta+Umschalt+T** (T für "Translate" = "Übersetzen")
    * Action
        * ding

![](tastatur-meta-umschalt-t.png)

### Global Shortcuts entdecken
* Plasma **Global Shortcuts** aufrufen
    * Schauen, was es so gibt
    * insbesondere
        * Dolphin
        * KRunner
        * Launch Konsole
        * Spectacle / Baustein: "Screenshot"
            * ...
        * KDE Daemon
        * ksmserver
        * KWin
        * Plasma
* siehe auch plasma-desktop/

Stufe 2 - notify-send, xclip
----------------------------

    sudo zypper install libnotify-tools

Befehl

    notify-send "$(xclip -o)"

auf eine globale Taste legen.

Stufe 3 - xlip-xcowsay-Rechner auf Meta+R
-----------------------------------------
    python3 -c 'import sys; print(sys.argv[1].strip())' "$(xclip -o)" | bc | xcowsay

Stufe 4 - Töne mit Audacity und aplay auf Tasten legen
------------------------------------------------------
* Audacity -> Erzeugen -> Klang oder Risset-Trommel (Hz) oder Zupfen (Midi-Tonhöhe)
    * Frequenzen siehe https://de.wikipedia.org/wiki/Frequenzen_der_gleichstufigen_Stimmung
    * Export als .ogg (oder besser .wav)
* Abspielen mit ogg123 (aplay)
* Legen auf Strg+Meta+F5,F6,F7,F8

    ogg123 /home/.../audio/440.ogg
    notify-send -t 500 A ; ogg123 /home/.../audio/440.ogg

Weiteres
--------
### Online-Wörterbücher
* http://dict.tu-chemnitz.de/dings.cgi
* http://dict.leo.org, inklusive Forum
* Englisch-Lehrer nach zuverlässigen Online-Angeboten fragen

### xclip für Bash
* ding geht auch ohne xclip mit Zwischenablage!
* ding $(xclip -o)

### KDing
* Installieren via https://software.opensuse.org/package/kding, Education-Repository
* Tastenkürzel in den Optionen: **Meta+Umschalt+T**

### KDing auf Qt5 portieren
* Derzeit ist kding noch in Qt4 implementiert. Es müsste auch Qt5 portiert werden.
