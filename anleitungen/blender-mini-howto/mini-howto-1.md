Blender-Mini-Howto
==================

Unsortiertes Blender-Wissen.

<!-- toc -->

- [Achtung: Speichern](#achtung-speichern)
- [Genereller Hinweis](#genereller-hinweis)
- [Modellieren](#modellieren)
- [Kamera einstellen](#kamera-einstellen)
- [Navigieren in der 3D-Ansicht](#navigieren-in-der-3d-ansicht)
- [Rendern](#rendern)
- [Text erstellen](#text-erstellen)
  * [Schriftart ändern](#schriftart-andern)
- [Animation](#animation)
- [Aufgabe: Grußkarte mit Animation erstellen](#aufgabe-grusskarte-mit-animation-erstellen)
- [Material](#material)
- [Audio-Dateien ins Video einbinden](#audio-dateien-ins-video-einbinden)
- [Unsortiert](#unsortiert)

<!-- tocstop -->

Achtung: Speichern
------------------
* Achtung: wenn man Blender mit dem (X) an der Fensterleiste schließt, dann gibt es KEINE Rückfrage, ob man Speichern möchte.
    * Also vorher speichern!

Genereller Hinweis
------------------
* mit kleinen Testprojekten (oder bei Animationen kleineren Endauflösungen) beginnen, um Blender besser kennenzulernen, damit man bei einer Animation, die ansonsten sehr lange zum Rendern braucht, keine böse Überraschung erlebt, weil etwas anders läuft, als gedacht.

Modellieren
-----------
* https://docs.blender.org/manual/en/dev/modeling/index.html

Kamera einstellen
-----------------
* Referenz (engl.) https://docs.blender.org/manual/en/dev/editors/3dview/navigate/camera_view.html

* Basis-Tasten und Funktionen:
    * Numpad0 (All modes): zwischen Kamera- und Benutzerperspektive wechseln
    * Ctrl-Alt-Numpad0 (Object Mode): Move Active Camera to View
    * N (-> Properties anzeigen): "Lock Camera to View"

Navigieren in der 3D-Ansicht
----------------------------
* https://docs.blender.org/manual/en/dev/editors/3dview/navigate/walk_fly.html

Rendern
-------
* https://docs.blender.org/manual/en/dev/render/index.html

Text erstellen
--------------
* Create --> Other --> Text
* Edit Mode, um Text zu editieren

### Schriftart ändern
* Properties editor --> Object Data ("F") --> Font Panel --> Regular
    * siehe z. B. hier http://blender.stackexchange.com/questions/46874/how-to-change-font-for-text-objects

* siehe auch [Schriftarten ansehen und neue installieren](schriftarten-beginner-opensuse)


Animation
---------
* Beispiel: [test1-animation.blend](files/test1-animation.blend)

Aufgabe: Grußkarte mit Animation erstellen
------------------------------------------
* Beispiel: [aufgabe-grußkarte-mit-animation.blend](files/aufgabe-grußkarte-mit-animation.blend)
    * dazu auch den Ordner Dr_Sugiyama mit der Schriftart herunterladen und parallel zur Datei legen

Material
--------
* Blender-Material-Datenbank: http://blendermada.com/
    * Addon: http://blendermada.com/addon/ (mit Tutorial-Video)
    * Modelle: http://blendermada.com/uploads/stuff/

Weiteres:

* Material zuweisen: https://docs.blender.org/manual/en/dev/render/blender_render/materials/assigning_a_material.html

* Holzmaterial für Cycles: https://blenderartists.org/forum/showthread.php?246113-A-fine-procedural-wood-material-for-Cycles (inkl. Download)
* Glänzendes Material erzeugen - für Anfänger (englisch): https://www.blendernation.com/2016/11/29/glossy-cycles-material-beginners/
    * inkl. Tipp wie man sich unten rechts ein kleines Render-Vorschaufenster hinbaut.
* Holz-Textur: http://blender-archi.tuxfamily.org/File:TiZeta_SmlssWood3.jpg
* Holz: https://www.blenderguru.com/tutorials/photorealistic-wood/

Arbeiten mit Texturen:

...todo...

* Übersicht: https://docs.blender.org/manual/en/dev/render/blender_render/textures/index.html
* Einführung: https://docs.blender.org/manual/en/dev/render/blender_render/textures/introduction.html
* Panel: https://docs.blender.org/manual/en/dev/render/blender_render/textures/texture_panel.html

Audio-Dateien ins Video einbinden
---------------------------------
* im *Video Sequence Editor*
* Encoding einstellen, siehe z. B. hier: http://blender.stackexchange.com/questions/32264/adding-sound-in-blender
* siehe auch https://docs.blender.org/manual/en/dev/editors/vse/sequencer/strips/audio.html
* ggf. openSUSE-Multimedia-Codes installieren

Unsortiert
----------
* 2017-04-21 (todo):
    * Blender Tutorial #1 - Grundlagen und Modelling [German][HD] - https://www.youtube.com/watch?v=pDS5zkqT0Ls, 20 min, Storm2141
    * Blender Tutorial #2 - Materials und Textures [German][HD] - https://www.youtube.com/watch?v=HDWYvy57TOs, 9 min, Storm2141
    * Tut 80 - Blender Fluid-Simulation einfach nachbearbeiten statt lange Baken - https://www.youtube.com/watch?v=gd62kL4jWMc, 15 min, gimpgustl
    * Tut 94 -- Deutsch: Blender Cycles Alien-Raumschiff im Nebel mit Displacement-Map erstellen - https://www.youtube.com/watch?v=9EYcIRxqXCE, 55 min, gimpgustl
