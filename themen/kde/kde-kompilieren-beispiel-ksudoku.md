KSudoku kompilieren und ändern
==============================

<!-- toc -->

- [Init](#init)
- [git clone](#git-clone)
- [Bauen - Versuch 1 - from repo - fail](#bauen---versuch-1---from-repo---fail)
- [Bauen - Versuch 2 - source install - fail](#bauen---versuch-2---source-install---fail)
- [Bauen mit kdesrc-build](#bauen-mit-kdesrc-build)
  * [News / Planet](#news--planet)
  * [kdesrc-build aktualisieren](#kdesrc-build-aktualisieren)
  * [kdesrc-build laufen lassen](#kdesrc-build-laufen-lassen)
- [Ausführen](#ausfuhren)
- [Code ändern und ausführen](#code-andern-und-ausfuhren)
  * [qDebug()](#qdebug)
- [Anforderung umsetzen: Add Generate Puzzle Screen - fortgeschritten](#anforderung-umsetzen-add-generate-puzzle-screen---fortgeschritten)
- [Anforderung umsetzen: Fertige Nummern hervorheben](#anforderung-umsetzen-fertige-nummern-hervorheben)
- [Weitere Tickets](#weitere-tickets)

<!-- tocstop -->

Init
----
* Was ist KSudoku? -> Webseite: https://www.kde.org/applications/games/ksudoku/
    * Enthält auch 3D-Anteile mit OpenGL
    * Das CMake-Build-System
    * Später: Lerne mit bugs.kde.org und Phabricator umzugehen
* Erste Schritte zum KDE-Kompilieren siehe README.md

git clone
---------
    mkdir ~/dev/src
    git clone kde:ksudoku
    cd ksudoku

Bauen - Versuch 1 - from repo - fail
------------------------------------
    mkdir build
    cd build
    cmake ..

Could not find ... KF5KDEGames ... .

CMakeLists.txt: # find_package(KF5KDEGames 4.9.0 REQUIRED)

Dann:
/home/gregor/dev/src/ksudoku/build/src/gui/ui_configgame.h:1:10: fatal error: klocalizedstring.h: No such file or directory
 #include <klocalizedstring.h>
          ^~~~~~~~~~~~~~~~~~~~
obwohl vorhanden.

Bauen - Versuch 2 - source install - fail
-----------------------------------------
    sudo zypper si ksudoku

Source package 'ksudoku' for package 'ksudoku' not found.

Bauen mit kdesrc-build
----------------------
### News / Planet
* https://www.purinchu.net/wp/2018/04/14/fancy-status-updating-in-kdesrc-build/
    * "Fancy status updating in kdesrc-build"

### kdesrc-build aktualisieren
    $ cd ~/kde/src/kdesrc-build
    $ git pull

### kdesrc-build laufen lassen
    $ . dev-kf5-env-setup
    $ kdesrc-build ksudoku

Packages for kdesrc-build missing.

    $ sudo zypper in perl-YAML-PP
    $ kdesrc-build ksudoku

Fails to configure. Again with depencencies

    $ kdesrc-build --include-dependencies ksudoku

    ...
    Building extra-cmake-modules from frameworks (1/79)
    ...
    wait
    ...

Ausführen
---------
    $ ksudoku

Code ändern und ausführen
-------------------------
Code hier ändern:

    ~/kde/src/kde/kdegames/ksudoku/

z. B.

    #include <QDebug>
    qDebug() << "Hallo";

Entweder so ausführen (lokal):

    $ cd ~/kde/src/build/kde/kdegames/ksudoku
    $ make && src/ksudoku

oder mit install:

    $ cd ~/kde/src/build/kde/kdegames/ksudoku
    $ make install && ksudoku

### qDebug()
* https://stackoverflow.com/questions/28540571/how-to-enable-and-disable-qdebug-messages
* https://stackoverflow.com/questions/3886105/how-to-print-to-console-when-using-qt

Anforderung umsetzen: Add Generate Puzzle Screen - fortgeschritten
------------------------------------------------------------------
* siehe https://bugs.kde.org/show_bug.cgi?id=242300#c9

Anforderung umsetzen: Fertige Nummern hervorheben
-------------------------------------------------
* siehe https://bugs.kde.org/show_bug.cgi?id=203813

Weitere Tickets
---------------
* todo: Ticketliste
