Binäres Zahlensystem
====================

Tabelle mit kcalc
-----------------
* Erstelle eine Umrechnungstabelle Binär / Dezimal mit kcalc.
    * oder siehe hier: https://ascii.cl/conversion.htm, http://www.exploringbinary.com/decimal-binary-conversion-table/
* Kannst du ein Verfahren entwerfen, für eine beliebe Zahl von Binär nach Dezimal oder umgekehrt entwerfen?

exploringbinary
---------------
* http://www.exploringbinary.com/how-i-taught-third-graders-binary-numbers/
    * Zahl 27 in verschiedenen Zahlensystemen
* ["What a Binary Counter Looks and Sounds Like"](http://www.exploringbinary.com/what-a-binary-counter-looks-and-sounds-like/)
    * nachprogrammieren: todo
* http://www.exploringbinary.com/how-i-taught-my-mother-binary-numbers/
* http://www.exploringbinary.com/visualizing-consecutive-binary-integers/
* http://www.exploringbinary.com/why-0-point-1-does-not-exist-in-floating-point/


Moved from wiki
---------------
* Arbeitsblatt 1
    * Öffne das Arbeitsblatt http://ddi.uni-wuppertal.de/material/materialsammlung/mittelstufe/binaer/ab_01_rechnen_im_binaersystem.pdf und arbeite es durch. (Hinweis: Für manche Aufgaben sind auf der letzten Seite die Lösungen. Am besten erst dann nachschauen, wenn du mit deiner Gruppe nicht mehr selber weiterkommst)

* Arbeitsblatt 2
    * http://projekte.gymnasium-odenthal.de/informatik/dateien/Informatik/Jahrgangsstufe%2008-09/Unterrichtsreihen/03%20Binaerzahlen/Dokumente-Arbeitsblaetter/Skript%20Binaersystem.pdf (inklusive negative Zahlen und Hexadezimal)

* Arbeitsblatt 3
    * Öffne die Webseite http://www.ulthryvasse.de/index.html und arbeite sie durch. Rechts ist ein Menü, um jeweils das nächste Kapitel zu öffnen.
