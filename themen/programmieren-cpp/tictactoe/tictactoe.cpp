#include <iostream> // cout, cin
//#include <cstdlib>  // atoi
#include <vector>
#include "tools.h"
#include "game.h"
#include "tttai.h"

using namespace std;    // using namespace std werden wir später weglassen
using namespace game;   // von game.h

/**
 * globale Variablen
 */
Controller ctrl;
Feld feld;

void play_player_sound(int player)
{
    if (player == 0) {
        tools::play_ogg_async("sounds/beltHandle1.ogg");
    } else {
        tools::play_ogg_async("sounds/beltHandle2.ogg");
    }
}

void play_error_sound()
{
    tools::play_ogg_async("sounds/metalLatch.ogg");
}

void play_win_sound()
{
    tools::play_wav_blocking("sounds/VictorySmall.wav", 0.1);
}

void play_intro_sound()
{
    tools::play_wav_blocking("sounds/Touch.wav");
}

void lese_spieler_namen()
{
    ctrl.read_default_player_names();

    play_player_sound(0);
    cout << "Spieler x (Namen eingeben) " << "[" + ctrl.spieler.name0 + "]: ";
    string name;
    getline(cin, name); //cin >> game::spieler.name1;
    if (!name.empty()) {
        ctrl.spieler.name0 = name;
    }

    play_player_sound(1);
    cout << "Wenn der Name mit KI beginnt => Künstliche Intelligenz" << endl;
    cout << "Spieler o (Namen eingeben) " << "[" + ctrl.spieler.name1 + "]: ";
    getline(cin, name);
    if (!name.empty()) {
        ctrl.spieler.name1 = name;
    }

    ctrl.save_player_names();
}

/**
 * Zeichen, das auf dem Feld ausgegeben werden soll
 */
char f(int y, int x)
{
    switch (feld.get(y, x))
    {
        case -1: return to_string(y * 3 + x + 1)[0]; // Zahl, die für unbesetztes Feld zu drücken ist;
        case  0: return 'X';
        case  1: return 'o';
        default: return 'E';
    }
}

// void zeichne_feld_grau()
// {
//     printf("           %c | %c | %c\n", f(0, 0), f(0, 1), f(0, 2));
//     printf("          -----------\n");
//     printf("           %c | %c | %c\n", f(1, 0), f(1, 1), f(1, 2));
//     printf("          -----------\n");
//     printf("           %c | %c | %c\n", f(2, 0), f(2, 1), f(2, 2));
// }

// Mehr Farben siehe https://en.wikipedia.org/wiki/ANSI_escape_code#3/4_bit Spalte "FG Code"
int color_x = 34; // blau
int color_0 = 32; // grün

/**
 * print player
 */
std::string pp(int y, int x)
{
    int color = 0; // normal
    if (feld.get(y, x) == 0) { // spieler 0
        color = color_x; // blau
    } else if (feld.get(y, x) == 1) {
        color = color_0; // grün
    }
    return terminal::bold_col(f(y, x), color);
}

void draw_line(int len, char c)
{
    for (int i = 0; i < len; i++)
    {
        cout << c;
    }
}

void zeichne_feld_farbig()
{
    cout << "           " << pp(0, 0) << " | " << pp(0, 1) << " | " <<  pp(0, 2) << "\n";
    cout << "          ---|---|---\n";
    cout << "           " << pp(1, 0) << " | " << pp(1, 1) << " | " << pp(1, 2) << "\n";
    cout << "          ---|---|---\n";
    cout << "           " << pp(2, 0) << " | " << pp(2, 1) << " | " << pp(2, 2) << "\n";
}

void zeichne_bildschirm(string errmsg)
{
    terminal::clear();

    cout << "TicTacToe\n";
    cout << "=========\n";
    cout << "Spieler " << terminal::bold_col('X', color_x) << ": '" << ctrl.spieler.name0 << "' | Spieler ";
    cout << terminal::bold_col('o', color_0) << ": '" << ctrl.spieler.name1 << "'\n";

    if (ctrl.cur_player() == 0) {
        draw_line(12 + ctrl.spieler.name0.size() + 1, '-');
    } else {
        draw_line(12 + ctrl.spieler.name0.size() + 4, ' ');
        draw_line(12 + ctrl.spieler.name1.size() + 1, '-');
    }

    cout << "\n\n";

    //zeichne_feld_grau();
    zeichne_feld_farbig();

    cout << "\n";

    string msg = " (";
    msg += errmsg;
    msg += ")" ;
    if (!errmsg.empty()) {
        cout << errmsg << "\n";
    }
    cout << "[Setzen: 1-9 | Abbruch: q]: ";
}

/**
 * Einstieg
 */
int main()
{
    tools::random_init();

    play_intro_sound();

    cout << "Willkommen bei TicTacToe" << endl;

    lese_spieler_namen();
    cout << "Spieler x = " << ctrl.spieler.name0 << " und o = " << ctrl.spieler.name1 << "\n";

    cout << "Wer fängt an? ";
    ctrl.set_random_start();

    bool running = true;
    string errmsg;
    while (running)
    {
        bool runde_zuende = false;
        bool frage_nochmal = false;
        Ergebnis ergebnis = feld.check_ergebnis();
        switch (ergebnis)
        {
        case Ergebnis::Keiner:
            // weiterspielen
            break;
        case Ergebnis::SpielerX:
        case Ergebnis::SpielerO:
        case Ergebnis::Unentschieden:
            ctrl.set_next_player(); // auf aktuellen Spieler zurückstellen
            runde_zuende = true;
            break;
        }

        zeichne_bildschirm(errmsg);

        if (runde_zuende) {
            string nachricht;

            switch (ergebnis)
            {
            case Ergebnis::SpielerX:
                nachricht = "Glückwunsch!!! " + ctrl.spieler.name0 + " hat gewonnen.";
                break;
            case Ergebnis::SpielerO:
                nachricht = "Glückwunsch!!! " + ctrl.spieler.name1 + " hat gewonnen.";
                break;
            case Ergebnis::Unentschieden:
                nachricht = "Glückwunsch?! Es ist unentschieden.";
                break;
            default:
                break; // nichts
            }

            if (!nachricht.empty()) {
                cout << "\n\n" << nachricht << "\n\n";
                play_win_sound();
                frage_nochmal = true;
            }
        }

        if (frage_nochmal) { // ggf. diese Ausgabe nach "zeichne_bildschirm" verlagern
            cout << "Nochmal spielen? [j/n] ";
            string antwort;
            cin >> antwort;
            if (antwort == "j") {
                feld.reset();
                if (ergebnis == Ergebnis::Unentschieden) {
                    ctrl.set_random_start();
                } else {
                    ctrl.set_next_player();
                }
                continue;   // Schleife fortsetzen
            } else {
                break;      // Schleife beenden
            }
        }

        int feld_index = -1;    // Feld-Index, der von Computer oder Mensch gewählt wird

        if (ctrl.cur_player_is_ai()) {
            const vector<int>& f = feld.get_feld();
            feld_index = ai_move(f, ctrl.cur_player());
            tools::sleep_ms(500);
        } else {
            string eingabe;
            cin >> eingabe;
            if (eingabe == "q") {
                break;
            }
            int input = atoi(eingabe.c_str()); // 0 bei ungültiger Zahl
            if (input != 0) {
                feld_index = input - 1;
            }
        }

        bool ok = feld.set(feld_index, ctrl.cur_player());
        if (ok) {
            play_player_sound(ctrl.cur_player());
            ctrl.set_next_player();
            errmsg = "";
        } else {
            play_error_sound();
            errmsg = "Ungültige Eingabe";
        }
    }

    cout << "Auf Wiedersehen!!! :-)\n";

    return 0;
}
