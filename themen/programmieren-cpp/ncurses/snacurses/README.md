Snacurses
=========

Snake with C++ and Ncurses

Build with make.

Andere Snake-Beispiele:

* https://github.com/FedeDP/Snake/blob/master/snake.c
* https://github.com/jvns/snake/blob/master/src/
* weitere Ideen zum Spielfeld: https://tonisagrista.com/blog/2019/learning-ncurses/

Siehe auch themen/programmieren-games/README.md
