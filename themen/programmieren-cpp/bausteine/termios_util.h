#include <termios.h>    // termios, tcgetattr, tcsetattr
#include <fcntl.h>      // fcntl
#include <unistd.h>  // STDIN_FILENO
#include <iostream>

namespace term
{
void show_cursor(bool show)
{
    using namespace std;
    // CSI ? 25 h 	DECTCEM Shows the cursor, from the VT320.
    // CSI ? 25 l 	DECTCEM Hides the cursor.
    if (show) {
        cout << "\x1B[?25h";
    } else {
        cout << "\x1B[?25l";
    }
}
}

/**
 * siehe auch tictactoe/b4.cpp
 */
struct termios_guard
{
    /**
     * true: normal
     * false: Jede Eingabe geht direkt an getchar etc. (anstelle erst mit Enter)
     */
    bool canon = true;

    /**
     * true: normal: Tippe Taste => sehe Buchstabe
     * false: "versteckt"
     */
    bool echo = true;

    /**
     * true: normal: getchar() wartet, bis Taste gedrückt
     * false: getchar() liefert -1, wenn keine Taste gedrückt
     */
    bool blocking = true;

    bool show_cursor = true;

    void setup()
    {
        termios newt;

        tcgetattr(STDIN_FILENO, &oldt);
        newt = oldt;
        if (!canon) {
            newt.c_lflag &= ~ICANON;
        }
        if (!echo) {
            newt.c_lflag &= ~ECHO;
        }
        tcsetattr(STDIN_FILENO, TCSANOW, &newt);

        int newf;
        oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
        if (!blocking) {
            fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
        }

        if (!show_cursor) {
            old_show_cursor = true;
            term::show_cursor(false);
        }
    }

    ~termios_guard()
    {
        tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
        term::show_cursor(old_show_cursor);
    }

private:
    termios oldt;
    int oldf;
    bool old_show_cursor;
};
