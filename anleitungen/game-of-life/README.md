Game of Life
============

<!-- toc -->

- [Eigenes Programm](#eigenes-programm)
  * [Repräsentation des Automaten als 2-D-Array](#reprasentation-des-automaten-als-2-d-array)
  * [Ausgabe auf die Konsole](#ausgabe-auf-die-konsole)
  * [Regeln anwenden](#regeln-anwenden)
  * [Ausgabe](#ausgabe)
  * [Graphische Ausgabe und Play-Knopf](#graphische-ausgabe-und-play-knopf)
  * [Interaktives Bearbeiten mit Stift](#interaktives-bearbeiten-mit-stift)
  * [Text-Fenster mit Vorschau für Austauschformat](#text-fenster-mit-vorschau-fur-austauschformat)
  * [Weitere Ideen](#weitere-ideen)
- [Das rle-Format](#das-rle-format)
  * [Beispiel: Gleiter](#beispiel-gleiter)
  * [Hinweise für Programme](#hinweise-fur-programme)
  * [B3/S23](#b3s23)
- [Programm: Golly](#programm-golly)
  * [Kompilieren](#kompilieren)
  * [Starten](#starten)
- [Projekt-Ideen](#projekt-ideen)
  * [Game of Life Plasma Wallpaper Plugin](#game-of-life-plasma-wallpaper-plugin)

<!-- tocstop -->

Eigenes Programm
----------------
Zellulärer Automat.

### Repräsentation des Automaten als 2-D-Array
- ...
- hart-codiertes Muster
- ...

### Ausgabe auf die Konsole
...

### Regeln anwenden
1. Tote Zelle wird lebendig bei genau 3 lebenden Nachbarzellen

```
##       ##
#.  -->  ##
```

2. Zelle bleibt am Leben bei 2 oder 3 lebenden Nachbarn, ansonsten stirbt sie

```
#.#       .#.
.#.  -->  .#.

    .         #
###  -->  .#.
    .         #

```

### Ausgabe
- Eingabe-Taste => Clear Screen => neue Ausgabe

### Graphische Ausgabe und Play-Knopf
- Das Array mit QPainter ausgeben
    - erst mal feste Größe
- Einzelschritt vorwärts, Play, Stop, Geschwindigkeit
- Scrollen

### Interaktives Bearbeiten mit Stift
- ...

### Text-Fenster mit Vorschau für Austauschformat
- vom Gitter ins RLE-Format (siehe unten)
- vom RLE-Format ins Gitter
- direkt beim Bearbeiten auf beiden Seiten (mit Signal/Slot)

### Weitere Ideen
- Größe der Welt einstellen
- Gitterlinien ja/nein
- Buttons mit Vorlagen wie Gleiter etc.
    - direkt einfügen
    - an die Maus hängen und bei Klick einfügen (z. B. Gleiter für 4 Richtungen)
- Button mit großem Radiergummi
- Wrap am Rand

Das rle-Format
--------------
http://www.conwaylife.com/wiki/Run_Length_Encoded

Siehe beispiel1.rle

    #CXRLE Pos=-20,-10

    = Kommentar / kann weggelassen werden


    x = 3, y = 7, rule = B3/S23

    x = Breite des Musters
    y = Höhe des Musters
    rule = Regel, die angewendet werden soll, bei uns immer B3/S23 / kann weggelassen werden


    obo2$obo3$ooo$bob!

    o   = ausgefülltes Feld (lebende Zelle)
    b   = leeres Feld (tote Zelle)
    $   = Zeilenende / neue Zeile
    2$  = zwei neue Zeilen
    3$  = drei neue Zeilen
    !   = Ende

So sieht das Muster dann aus:

    x x

    x x


    xxx
     x

### Beispiel: Gleiter

    #C Das ist ein Gleiter
    x = 3, y = 3
    bo$2bo$3o!

### Hinweise für Programme
http://www.conwaylife.com/wiki/Run_Length_Encoded

    Lines in the RLE file must not exceed 70 characters, although it is a good idea for RLE readers to be able to cope with longer lines. DOS, Unix and Mac newline conventions are all acceptable.

    Anything after the final ! is ignored. It used to be common to put comments here (starting on a new line), but the usual method for adding comments is now by means of #C lines (see below).

### B3/S23
https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life

"B3/S23: A cell is Born if it has exactly three neighbours, Survives if it has two or three living neighbours, and dies otherwise"

Programm: Golly
---------------
### Kompilieren

    git clone https://git.code.sf.net/p/golly/code

ggf. gui-wx/build.ninja anpassen

    # additional link flags for opengl
    -extralibs_opengl = -lwx_gtk2u_gl-3.0 -lGL -lGLU
    +extralibs_opengl = -lwx_gtk3u_gl-3.1 -lGL -lGLU

Bauen

    cd gui-wx
    ninja

### Starten

    cd ..
    ./golly

Beispiele links im Baum ausprobieren. Drauf achten, dass es sich um Game-of-Life-Regeln handelt.

Projekt-Ideen
-------------
### Game of Life Plasma Wallpaper Plugin
- GoL als Hintergrundbild
    - z. B. wenn die Maus länger als drei Sekunden über einer Stelle schwebt, wird ein Läufer erzeugt, der Richtung Mitte läuft
    - Ideen für Optionen: https://www.youtube.com/watch?v=mjivQEjGvvk
        - https://play.google.com/store/apps/details?id=cdev.life&hl=en_US
