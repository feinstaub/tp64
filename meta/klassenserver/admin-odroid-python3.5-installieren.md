ODROID Python > 3.4 installieren
================================

Versuch 2 - ging nicht
----------------------
Direkt mit einer apt-Quelle.

* $ cat /etc/apt/sources.list

        deb http://httpredir.debian.org/debian/ jessie main contrib non-free
        deb http://security.debian.org/ jessie/updates main contrib non-free
        deb http://httpredir.debian.org/debian/ jessie-updates main contrib non-free
        deb http://httpredir.debian.org/debian/ jessie-backports main contrib non-free

* GEHT NICHT:
    * $ sudo apt-get install software-properties-common python-software-properties
        * siehe http://lifeonubuntu.com/ubuntu-missing-add-apt-repository-command/
        * sonst fehlt apt-get-repository

    * https://askubuntu.com/questions/865554/how-do-i-install-python-3-6-using-apt-get
        * $ sudo add-apt-repository ppa:jonathonf/python-3.6
            You are about to add the following PPA to your system:
            A plain backport of *just* Python 3.6. System extensions/Python libraries may or may not work.

            Don't remove Python 3.5 from your system - it will break.
            More info: https://launchpad.net/~jonathonf/+archive/ubuntu/python-3.6
            Press [ENTER] to continue or ctrl-c to cancel adding it

        GEHT NICHT:
            aptsources.distro.NoDistroTemplateException: Error: could not find a distribution template for Netrunner/avalon

        * Dasselbe für $ sudo add-apt-repository ppa:deadsnakes/ppa
    * Also erst mal mit Python 3.4 auskommen.
        * TODO: mit docker oder vagrant etc. arbeiten!


Versuch 1 - ging nicht
----------------------
siehe:

    * https://gist.github.com/jaydenkieran/75b2bbc32b5b70c4fdfb161ecdb6daa2
    * https://github.com/pyenv/pyenv-installer
    * https://github.com/pyenv/pyenv

$ curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash

    ...

    WARNING: seems you still have not added 'pyenv' to the load path.

    # Load pyenv automatically by adding
    # the following to ~/.bash_profile:

    export PATH="/home/ag/.pyenv/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"


Add this to .bashrc

    export PATH="~/.pyenv/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"

Re-login

$ pyenv install --list

    ...

$ pyenv install 3.6.3

    Downloading Python-3.6.3.tar.xz...
    -> https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tar.xz
    Installing Python-3.6.3...

    ...Dauert einige Minuten...

    BUILD FAILED (Netrunner 16.09 using python-build 20160602)

    Inspect or clean up the working tree at /tmp/python-build.20180106131828.2669
    Results logged to /tmp/python-build.20180106131828.2669.log

    Last 10 log lines:
        ensurepip._main()
    File "/tmp/python-build.20180106131828.2669/Python-3.6.3/Lib/ensurepip/__init__.py", line 189, in _main
        default_pip=args.default_pip,
    File "/tmp/python-build.20180106131828.2669/Python-3.6.3/Lib/ensurepip/__init__.py", line 102, in bootstrap
        _run_pip(args + [p[0] for p in _PROJECTS], additional_paths)
    File "/tmp/python-build.20180106131828.2669/Python-3.6.3/Lib/ensurepip/__init__.py", line 27, in _run_pip
        import pip
    zipimport.ZipImportError: can't decompress data; zlib not available
    Makefile:1079: recipe for target 'install' failed
    make: *** [install] Error 1

$ less /tmp/python-build.20180106131828.2669.log

    import pip
    zipimport.ZipImportError: can't decompress data; zlib not available
    Makefile:1079: recipe for target 'install' failed
    make: *** [install] Error 1

TODO später???:

"pyenv global 3.5.2

To test that this worked, try running the command python --version on your system to confirm the version being used."
