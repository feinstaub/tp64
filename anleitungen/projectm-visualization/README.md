ProjectM-Musik-Visualisierung
=============================

<!-- toc -->



<!-- tocstop -->

* [Screenshots](https://github.com/projectM-visualizer/projectm#screenshots)
* Milkdrop-kompatibel
* eingebaut in Clementine, aber funktioniert nicht auf Anhieb

Installieren:

    $ sudo zypper install projectM-qt5 projectM-data


Starten:

    $ projectM-pulseaudio

Dann Musik starten.


Optionen:

* F1 - Hilfe
* f - Vollbild
* m - Menü


Probleme:

Falls nicht im Takt, ggf.:

    $ sudo zypper install pavucontrol
    $ pavucontrol

Bei **Recording** die richtige Audio-Quelle auswählen.
