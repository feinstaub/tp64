MusicTyper
==========

TODO
----
  [ ] typer - Schreiben üben mit Musik (Hallo_ o muss länger gedrückt werden,
     weil passend Musik kommt); analog Frets on Fire - pro Taste ca. 0,5 Sekunden Musik,
     die vorher mit yt-dl download  (wer durchschreibt hört das ganze Lied am Stück)

  [ ] LyricsTyper - Type the lyrics fluently that drives the song play fluentliy,
        e.g. Rihanna - we found love, mit QMediaPlayer (oder eine andere Bibliothek) fadeout kurz vor aus;
            (das fade out kann auch erstmal wegbleiben)
                --> https://www.sfml-dev.org/documentation/2.5.1/classsf_1_1Music.php#details
        man kann Zeiten aufsammeln, wenn besonders schnell; aber bei Fehler sofort Stop

  [ ] Widget: Klaviertastatur mit Noten und Tasten

  [ ] siehe unten

### fix UTF-8 handling
* do not rely on it
* or understand and use it
    * https://stackoverflow.com/questions/30995246/substring-of-a-stdstring-in-utf-8-c11
    * https://stackoverflow.com/questions/402283/stdwstring-vs-stdstring
    * https://www.cprogramming.com/tutorial/unicode.html

### FLTK
* Rich text display widget
    * https://www.fltk.org/doc-1.3/classFl__Text__Display.html#details
    * https://www.fltk.org/doc-1.3/classFl__Text__Display.html

### Suche nach Audio Libraries und Fade out
* https://www.sfml-dev.org/documentation/2.5.1/classsf_1_1Music.php#details
    * geht gut
    * https://www.sfml-dev.org/documentation/2.5.1/classsf_1_1SoundStream.php#afdc08b69cab5f243d9324940a85a1144

* LISTE: https://superpowered.com/audio-library-list
    * https://sol.gfxile.net/soloud/
        * schwierig zu kompilieren
* https://www.splashkit.io/api/audio/
    * zu fett?
