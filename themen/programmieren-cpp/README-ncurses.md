Ncurses
=======

<!-- toc -->

- [Bücher](#bucher)
  * [Ncurses-Wiki-Buch](#ncurses-wiki-buch)
  * [rheinwerk](#rheinwerk)
- [Programme](#programme)
  * [scratchpad](#scratchpad)
  * [colorwords](#colorwords)
  * [Aufgabe: Spirale](#aufgabe-spirale)
  * [wordwall](#wordwall)
  * [snacurses](#snacurses)
  * [tictactoe10](#tictactoe10)
  * [Python](#python)
- [FAQ](#faq)
  * [Wie komme ich in die ncurses-Header-Datei?](#wie-komme-ich-in-die-ncurses-header-datei)
- [Weiteres](#weiteres)

<!-- tocstop -->

Code siehe Unter-Ordner: ncurses/

Bücher
------
### Ncurses-Wiki-Buch
* Wiki-Buch (deutsch): https://de.wikibooks.org/wiki/Ncurses:_Inhaltsverzeichnis, 2006
    * https://de.wikibooks.org/wiki/Ncurses:_Grundlegendes
    * gut als Grundlage, um Funktionen, Klassen (ctor/dtor) etc. zu lernen

### rheinwerk
* http://openbook.rheinwerk-verlag.de/linux_unix_programmierung/Kap13-002.htm, 2006
    * Tasten
    * Scrolling
        * Raumschiff im Weltall
    * Attribute und Farben
    * Beispiel-Code

Programme
---------
### scratchpad
* nc1.cpp
* nc2.cpp
* nc3.cpp
* nc4.cpp
* nc5.cpp
    * Raumschiff
    * Sonderzeichen
    * srand
    * halfdelay = ?
* nc6.cpp - Formulare

### colorwords
* Einstieg mit farbigen Buchstaben - in Form von Dreiecken (bzw. Weihnachtsbäumen)

### Aufgabe: Spirale
```
 --------------->---|
                    |
    |------         |
    |               |
    --------<-------
```

mit Farbwechsel

### wordwall
* Eine rechteckiger Bereich mit Worten
* Gelesen aus einer Datei

### snacurses
* Snake mittels NCurses
* so kann man ein Wort von links nach rechts durchlaufen lassen

### tictactoe10
* siehe dort
    * Wrapper für ncurses: tencurses

### Python
* siehe programmieren-python/curses-python
    * casino
        * kessel
    * to sort

FAQ
---
### Wie komme ich in die ncurses-Header-Datei?
cpp -v

    /usr/lib64/gcc/x86_64-suse-linux/9/include
    /usr/local/include
    /usr/lib64/gcc/x86_64-suse-linux/9/include-fixed
    /usr/lib64/gcc/x86_64-suse-linux/9/../../../../x86_64-suse-linux/include
    /usr/include

/usr/include/ncurses.h

Weiteres
--------
* RxTerm
    * https://hackernoon.com/building-reactive-terminal-interfaces-in-c-d392ce34e649
        * https://github.com/loopperfect/rxterm
