Lokale Datensicherung / Präsenz-Inhalte
=======================================

Bisheriges Material
-------------------
* siehe [Präsenzmaterial (BISHER)](http://feinstaub.github.io/fs-workshop.de/datensicherung.html)
* Korrekturen / Erweiterungen Mai 2017:
    * DONE Nov 2017: Einstieg: ein verbranntes Bild weniger
    * DONE Nov 2017: 3.2: USB-Stick-Preise
    * DONE Nov 2017: 4.1: Link zum sicheren Surfen unter tp64 (mit uBlock Origin) (war aber noch wikitolearn)
    * DONE Nov 2017: bei mehr als 3 Leuten: Vorne die Einrichtung von FreeFileSync vorführen
        * kleinschrittige Anleitung zum Auswählen der Ordner von "links und rechts"
            * Nov 2017: das mache ich lieber vor
    * Liste von Standard-Ignore-Filter-Pfaden erstellen (z. B. Cache-Verzeichnisse)
        * Nov 2017: für Windows im aktuellen Kurs sammeln

Backup-Strategie bei wenig Daten
--------------------------------
* Ordner_2016-02-15 anlegen und manuelle Kopie

Selber machen: Backup verifzieren
---------------------------------
* Dateien im Backup einmal selber aufmachen

Empfehlung E-Mail-Sicherung
---------------------------
(bereits im Mai 2017 nach fs-workshop.de übertragen nach "6.1.2. Empfehlung E-Mail-Sicherung und Anbieter")

* Erster Schritt: Ein E-Mail-Programm verwenden
    * z. B. Mozilla Thunderbird
* Zweiter Schritt: E-Mail-Ordner sichern

### Exkurs: Anbieter
* (je nach Privatsphäreempfinden): E-Mail-Anbieter wechseln
    * z. B. posteo.de oder mailbox.org
