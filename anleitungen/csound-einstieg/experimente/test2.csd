<CsoundSynthesizer>
<CsInstruments>
; ------ Unvermeidbarer Allgemeinkram ------
; Abtastfrequenz in Hz, je nach Wunsch und Soundkarte
sr         =          44100
; Blockgroesse fuer das Rendering (Control Ratio), je nach Wunsch
ksmps      =          126
; Control-Frequenz in Hz, ist immer sr/ksmps, darf trotzdem nicht fehlen!
kr         =          350
; Anzahl der Audiokanaele, je nach Wunsch und Soundkarte
nchnls     =          2
; ------ Einfacher Subtraktivsynthesizer ------
; Beim Start schon mal globale Sinustabelle vorberechnen, fuer vco-Opcode
gisin      ftgen      0,0,4096,10,1.0
; Beim Start schon mal Speicher reservieren fuer 32 Stimmen, Instrument 1
           prealloc   1,32
; Beim Start MIDI-Controller 80...83 auf Mittelstellung initialisieren
           initc7     1,80,0.5
           initc7     1,81,1
           initc7     1,82,0
           initc7     1,83,0
; Der Code fuer den zentralen Berechnungszyklus, Instrument 1
instr 1
; Frequenz in Hz aus der MIDI-Notennummer
ifreq      cpsmidi
; Amplitude (hier zwischen 0 bis 10000) aus der MIDI-Note-On-Velocity
iamp       ampmidi    10000
; Controller 80 fuer Verstimmung des 2. Oszillators
kdetune    ctrl7      1, 80, 0.936, 1.063
; Controller 81 fuer Pulsweite
kpwidth    ctrl7      1, 81, 0, 1
; Controller 82 fuer LFO-Amplitude
klfoamp    ctrl7      1, 82, 0, 127
; Controller 83 fuer LFO-Frequenz
klfofrq    ctrl7      1, 83, 0, 12.7
; LFOszillator, Dreieck
klfo       lfo        klfoamp, klfofrq, 1
; 1. VCOszillator, Dreieck
avco1      vco        iamp,ifreq+klfo,3,kpwidth,gisin,1/ifreq
; 2. VCOszillator, Dreieck
avco2      vco        iamp,ifreq*kdetune+klfo,3,kpwidth,gisin,1/ifreq
; Zusammenmischen
avco       =          avco1 + avco2
aout       =          avco
; Ergebnis in Stereo ausgeben
           outs       aout,aout
; Fertig
endin

</CsInstruments>
<CsScore>
; Aktiviere Berechnung fuer 24 Stunden
f 0 86400
; Fertig
e
</CsScore>
<CsOptions>
; Setze Ausgabefenstergroesse
; Unterdruecke Pseudo-Graphik-Ausgaben
; Unterdruecke den Echtzeitbetrieb stoerende Ausgaben
; Setze Audio-Puffergroesse
-d -m0 -b504
</CsOptions>
</CsoundSynthesizer>
