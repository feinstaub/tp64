Anleitung KDE-Identity anlegen
==============================

Um an bestimmten Stellen am KDE-Projekt mitwirken zu können, muss man sich eine KDE-Identität anlegen.

Das geht so:

* https://identity.kde.org aufrufen
* Oben rechts auf "Register" drücken
* Den https://www.kde.org/code-of-conduct durchlesen und akzeptieren.
    * Es geht grob darum, freundlich zu sein: "Be considerate, Be respectful, Be collaborative, Be pragmatic, Support others in the community, Get support from others in the community"
* Mit Vorname, Nachname und E-Mail-Adresse registrieren.
    * Es besteht _keine_ Pflicht zur Angabe des richtigen Namens. Am besten verwende für den Anfang ein nettes Pseudonym. Der Name kann später noch geändert werden.
    * Rücksprache bezüglich der E-Mail-Adresse
* Register etc.
