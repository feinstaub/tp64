#include "tools.h"
#include "programkeys.h"
#include "noten_tasten.h"
#include "player.h"
#include "virtpiano.h"

void print_menu(const ProgramKeys& pkeys)
{
    using namespace std;
    tools::terminal_clear();
    cout << "  PIANO TYPER" << endl;
    cout << "---------------" << endl;
    for (const ProgramKey& pkey : pkeys.get()) {
        cout << pkey.to_string() + " ";
    }
    cout << endl;
}

void demo_play_sheet()
{
    using namespace std;
    MusicSheet sheet = MusicSheet::from_file("noten/tetris-theme.msheet").at(0);
    //sheet.fill_sample();
    cout << sheet.title_summary() << "..." << endl;
    cout << "Virtual Piano string: " << sheet.noten_to_virtual_piano_string() << endl;
    NotePlayer player;
    player.play(sheet);
    // TODO: how to interrupt? (except for Ctrl+C)?
}

void play_one_note()
{
    using namespace std;
    int taste = tools::readInt("Welche Taste? 1..88: ");
    int duration_s = 2;
    cout << duration_s << " Sekunde(n)..." << flush;
    play_note_from_taste_async(taste, duration_s);
    tools::sleep_ms(duration_s * 1000);
    cout << "Fertig." << flush;
    tools::sleep_ms(500);
    cout << endl;
}

int main(int argc, char **argv)
{
    using namespace std;

    // TODO:
    //      2. read sheet file for elise

    ProgramKeys pkeys;
    const int key_show_keys         = pkeys.register_key('H', 0, "Zeige Tasten");
    const int key_demo_play_sheet   = pkeys.register_key('D', 0, "Demo Play Sheet");
    const int key_play_1_note       = pkeys.register_key('N', 0, "Play 1 Note");
    const int key_virt_piano        = pkeys.register_key('V', 0, "Virtual Piano");
    const int key_exit              = pkeys.register_key('E', 2, "Exit");

    while (true)
    {
        print_menu(pkeys);
        char ch = 0;
        {
            tools::terminal_noncanoical_guard g(false);
            ch = getchar();
        }

        int key_handle = pkeys.key_pressed(ch);

        try {
            if (key_handle == key_show_keys) {
                print_tasten();
                cout << "Press any key..." << flush;
                tools::terminal_noncanoical_guard g(false);
                getchar();
            } else if (key_handle == key_demo_play_sheet) {
                demo_play_sheet();
            } else if (key_handle == key_play_1_note) {
                play_one_note();
            } else if (key_handle == key_virt_piano) {
                VirtPiano virtPiano;
                virtPiano.start();
            } else if (key_handle == key_exit) {
                break;
            }
        }
        catch (const exception& e) {
            cout << "Problem: '" << e.what() << "'. EXIT.\n";
            break;
        }
    }

    return 0;
}
