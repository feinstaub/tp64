openSUSE - Sonstiges
====================

Live-DVD für openSUSE-Tumbleweed
--------------------------------
* https://en.opensuse.org/openSUSE:Tumbleweed_installation besuchen.
* Live-DVD herunterladen:
    * "KDE **Live** CD - x86_64" (für 64 Bit)
    * "KDE **Live** CD - i686" (für 32 Bit)
* ggf. Checksumme der iso-Datei prüfen und Brennen mit K3B (Klick auf die iso-Datei)
    * siehe "openSUSE-Installations-DVD beschaffen" in [README.md](README.md)

Partitionierung
---------------
Ältere Informationen zu openSUSE Leap 42.1.

* Bei der **Partitionierung** folgende Einteilung vornehmen
    * Eingebaute Platte: /dev/sda (nicht verwenden!), aber dafür /dev/sdb (wenn das der Stick ist)
    * 10 GB root (/) EXT4 - Systemdateien
    * 2 GB swap - Auslagerungsplatz
    * 14 GB /home EXT4 - Benutzerdaten
    * 2 GB FAT32 - Auf diesen Bereich kann man dann auch unter Windows zugreifen.
        * Achtung: möglicherweise muss diese Partition ganz an den Anfang, damit es wirklich funktioniert.

Bild
----
![](Install_Linux-gefunden-bei-wekan.jpg)
