Audio
=====

siehe auch ../musik

<!-- toc -->

- [Einstieg](#einstieg)
  * [Audiodateien auf der Shell wiedergeben](#audiodateien-auf-der-shell-wiedergeben)
  * [Audio mit der Shell aufnehmen und wiedergeben](#audio-mit-der-shell-aufnehmen-und-wiedergeben)
  * [SFXR Qt (mit QML)](#sfxr-qt-mit-qml)
  * [Midi](#midi)
  * [audaspace](#audaspace)
- [Programmieren](#programmieren)
  * [Qt Soundgenerator](#qt-soundgenerator)
  * [Frequenzen, Noten, Piano](#frequenzen-noten-piano)
- [Sounds und Musik für eigene Programme finden](#sounds-und-musik-fur-eigene-programme-finden)
- [Siehe auch](#siehe-auch)

<!-- tocstop -->

Einstieg
--------
### Audiodateien auf der Shell wiedergeben
* siehe anleitungen/offline-woerterbuch-kding/README.md
    * aplay für .wav
    * ogg123 für .ogg
* sox

### Audio mit der Shell aufnehmen und wiedergeben
Baustein: Audio, Shell, todo, siehe "Ding"

z. B.

    arecord -vv -fdat test.wav
    #       -vv = verbose = Ausgabe über den Status des Programms
    #           -fdat     = Audio-Format: 16 bit little endian, 48000, stereo; siehe man arecord
    # Strg+C
    aplay test.wav

    Alternativ: ffmpeg -f pulse -i default test.wav

TODO: PID speichern zum Remote-Beenden

Verweis auf Audacity.

Midi
----
### Abspielen
* timidity
    * TODO?: MIDI mit `timidity -ig <file>`
        * siehe /dataArchiv/2012 Audio Misc/Games sounds/

* [KMid](https://www.kde.org/applications/multimedia/kmid)

### Noten extrahieren
* TODO
    * mit MuseScore?
    * https://stackoverflow.com/questions/49339622/how-to-extract-individual-chords-rests-and-notes-from-a-midi-file ?

### Analyse, music21
* https://towardsdatascience.com/midi-music-data-extraction-using-music21-and-word2vec-on-kaggle-cb383261cd4e
    * https://web.mit.edu/music21/
    * (https://en.wikipedia.org/wiki/Word2vec)

### Sonstiges
* ["Ted's Linux MIDI Guide"](http://tedfelix.com/linux/linux-midi.html), 2018, todo

Weiteres
--------
### SFXR Qt (mit QML)
* 2018 Blog-Eintrag: http://agateau.com/2018/sfxr-qt/
    * Code: https://github.com/agateau/sfxr-qt
        * selber kompilieren
    * Original: http://www.drpetter.se/project_sfxr.html

### audaspace (py, C++)
* Schöne Musik: dev/godot/minilens/audio/main_menu.ogg
* audaplay xxx
* Parallel abspielbar
* Python samples: https://audaspace.github.io/bindings/tutorials.html
    * siehe audaspace/audaspace1.py
        * TODO: module aud not found

Programmieren
-------------
### Qt Soundgenerator
* https://github.com/picaschaf/Soundgenerator
    * "Simple sound generator in Qt." / API sieht sehr einfach aus für einfache Töne
    * sudo zypper in libqt5-qtmultimedia-devel
    * qmake-qt5
    * make # does not compile

### Frequenzen, Noten, Piano
* https://en.wikipedia.org/wiki/A440_%28pitch_standard%29
    * "A440 (pitch standard)"
    * Bild mit einem Piano mit hervorgehobenem C und A

Sounds und Musik für eigene Programme finden
--------------------------------------------
* http://agateau.com/2018/pixelwheels-0-6-0/
    * opengameart.org
        * e.g. https://opengameart.org/content/50-rpg-sound-effects
            * "50 sound effects for RPG/fantasy/adventure games"
            * beltHandle1.ogg
            * beltHandle2.ogg
            * ...
    * freesound.org
    * PixelWheels: http://agateau.com/projects/pixelwheels/
* Sounds: khangman, kgoldrunner, sauerbraten, sdl-ball, hegewars, minetest
* Musik: tong, extreme-tuxracer
* Musik und Sounds: supertuxkart, supertux2, xmoto

Siehe auch
----------
* C/C++: c-tunneler-anpassen/README2.md
* C/C++: tictactoe
* audaspace, siehe oben
* Python: anleitungen/pyqt-breakout-spiel/b10-racket.py
* csound
* sox - Swiss Army knife of sound processing tools
* ../musik
    * themen/musik/pyaudio-and-to-sort/arbeitsblatt-audio.meta
    * pyaudio-and-to-sort
* themen/programmieren-games/README.md
