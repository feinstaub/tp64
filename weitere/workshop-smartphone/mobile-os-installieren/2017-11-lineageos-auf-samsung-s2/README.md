LineageOS auf Samsung Galaxy S2
===============================

FAIL

<!-- toc -->

- [FAIL](#fail)
- [Installation instructions](#installation-instructions)
  * [Arbeitsverzeichnis anlegen](#arbeitsverzeichnis-anlegen)

<!-- tocstop -->

FAIL
----
* siehe auch [Replicant 6 auf Samsung Galaxy S2](../replicant6-auf-samsung-s2)

* erster Versuch gescheitert.

* Schauen, ob kompatibel: https://download.lineageos.org/
    * Downloads: https://download.lineageos.org/i9100

Installation instructions
-------------------------
https://wiki.lineageos.org/devices/i9100/install

* adb installiert? --> ja
* USB debugging enabled? --> ja?
* Boot into download mode:
    * With the device powered off, hold Volume Down + Home + Power.
* USB-Kabel einstecken
* heimdall print-pit
    * "If the device reboots, Heimdall is installed and working properly."

    --> GING NICHT

* https://download.lineageos.org/i9100
    * lineage-14.1-20171122-nightly-i9100-signed.zip
        * https://mirrorbits.lineageos.org/full/i9100/20171122//lineage-14.1-20171122-nightly-i9100-signed.zip

### Arbeitsverzeichnis anlegen

    $ mkdir ~/dev/lineageos_s2/
    $ cd ~/dev/lineageos_s2/

lineage-14.1-20171122-nightly-i9100-signed.zip dort reinlegen

Neu starten.

Mit adb hochladen.

    $ adb devices

    * daemon not running. starting it now on port 5037 *
    * daemon started successfully *


    List of devices attached
    00095da2148b6f  device

OK

    $ adb push lineage-14.1-20171122-nightly-i9100-signed.zip /sdcard/
    3541 KB/s (300278747 bytes in 82.807s)

ca. 280 MB haben ca. 1min und 20 Sekunden gedauert.

* With the device powered off, hold Volume Down + Home + Power.

* Der Bildschirm zeigt
    "ODIN MODE
    PRODUCT NAME: GT-i9100
    CUSTOM BINARY DOWNLOAD: NO
    CURRENT BINARY: SAMSUNG OFFICIAL

    Downloading...
    Do not turn off target !!"
