Programmieren
=============

<!-- toc -->

- [Themenfelder](#themenfelder)
- [Versionsnummern](#versionsnummern)
- [Kopieren von Dateien](#kopieren-von-dateien)
- [Code-Versionierung online](#code-versionierung-online)
- [Informationsquellen für Technik-Interessierte](#informationsquellen-fur-technik-interessierte)
  * [KDE-Community](#kde-community)
  * [Gnome-Community](#gnome-community)
- [Werkzeuge](#werkzeuge)
  * [hyperfine](#hyperfine)
- [IDEs](#ides)
- [Weitere Programmiersprachen](#weitere-programmiersprachen)
  * [Processing](#processing)

<!-- tocstop -->

Noch unsortierte Themensammlung. Teilweise fortgeschritten.

Themenfelder
------------
* [Mathematische Funktionsplots](math-plot.md)

Versionsnummern
---------------
0.3.3? 0.1.2.3.4.5? Was ist eine gute Art Programme und Bibliothken zu versionieren?

Siehe z. B. [Semantic Versioning](http://semver.org/): Versionsnummern für Bibliothken vergeben. Das Wissen ist auch für Programme interessant.

Kopieren von Dateien
--------------------
Beim Hochkopieren kannst man das Verzeichnis '__pycache__' und '.idea' weglassen.

Siehe z. B.:

    $ rsync -av --exclude __pycache__ --exclude .idea "quell-verzeichnis" zielverzeichnis

Damit kann man effizient Verzeichnisbäume kopieren und z. B. bestimmte Verzeichnisse weglassen (https://stackoverflow.com/questions/4585929/how-to-use-cp-command-to-exclude-a-specific-directory).

Code-Versionierung online
-------------------------
* öffentlich
    * z. B. KDE playground, siehe [KDE Policies/Application_Lifecycle](https://community.kde.org/Policies/Application_Lifecycle)
    * (github)

Informationsquellen für Technik-Interessierte
---------------------------------------------
### KDE-Community
* siehe [kde](../kde)

### Gnome-Community
* http://planet.gnome.org
    * Infos rund um den GNOME-Desktop und andere freie Software

Werkzeuge
---------
### hyperfine
* A command-line benchmarking tool
* https://github.com/sharkdp/hyperfine
* gefunden auf:
    * ["Speeding up Cornercases"](https://www.angrycane.com.br/en/2018/06/19/speeding-up-cornercases/), C++, 2018, planet.kde

IDEs
----
* Kate
* KDevelop
* Gnome Builder
* Thonny (Python)
* CodeLite

Weitere Programmiersprachen
---------------------------
### Processing
* https://de.wikipedia.org/wiki/Processing
    * Java
    * GPL bzw. LGPL
    * Deutschsprachige Tutorials
        * http://michaelkipp.de/processing/
* Download for Linux
* OpenProcessing, https://processing.org/
* Processing.py für Python
    * https://py.processing.org/reference/ - schöne Übersicht
