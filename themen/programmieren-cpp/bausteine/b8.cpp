#include <stdio.h>
#include <iostream>
#include "termios_util.h"

using namespace std;

int main()
{
    {
        termios_guard tg;
        tg.canon = false;
        tg.setup();

        cout << "Tippe solange, bis 'e'..." << endl;
        char ch = 0;
        while (ch != 'e')
        {
            ch = getchar(); // stdio.h
        }
        cout << endl;
    }

    {
        termios_guard tg;
        tg.canon = false;
        tg.echo = false;
        tg.setup();

        cout << "Tippe solange (versteckt), bis 'e'..." << endl;
        char ch = 0;
        while (ch != 'e')
        {
            ch = getchar();
            cout << "*";
        }
        cout << endl;
    }

    {
        termios_guard tg;
        tg.echo = false;
        tg.setup();

        cout << "Tippe Buchstabe (kein Echo), dann Enter; bis 'e'..." << endl;
        char ch = 0;
        while (ch != 'e')
        {
            ch = getchar();
            cout << ch;
        }
        cout << endl;
    }

    {
        termios_guard tg;
        tg.echo = false;
        tg.canon = false;
        tg.blocking = false;
        tg.setup();

        cout << "echo=false, blocking=false 'e'..." << endl;
        char ch = 0;
        while (ch != 'e')
        {
            ch = getchar();
            if (ch >= 'A') {
                cout << ch;
            } else {
                cout << ".";
            }
        }
        cout << endl;
    }

    return 0;
}
