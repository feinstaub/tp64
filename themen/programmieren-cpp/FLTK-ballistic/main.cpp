#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/fl_draw.H>

int x_root;
int y_root;

void fscr_callback(Fl_Widget *w, void *data) {
    Fl_Window* window = w->window();
    if (window->fullscreen_active()) {
        window->fullscreen_off();
        window->resize(x_root, y_root, 340, 480);
    } else {
        x_root = window->x_root();
        y_root = window->y_root();
        window->resize(0, 0, 1920, 1080);
        window->fullscreen();
    }
}

class MyWindow : public Fl_Double_Window
{
public:
    MyWindow(int w, int h, const char* title)
        : Fl_Double_Window(w, h, title)
    {
        Fl::add_timeout(timer_s, &timeout, (void*)this);
    }

    static void timeout(void* this_p)
    {
        auto* this_ = static_cast<MyWindow*>(this_p);
        this_->next_frame();
        this_->redraw();
        Fl::add_timeout(this_->timer_s, &timeout, this_p);
    }

    void draw() override
    {
        if (!fullscreen_active()) {
            Fl_Window::draw();
            make_black = true;
        } else {
            if (make_black) {
                fl_rectf(0, 0, w(), h(), FL_BLACK);
                make_black = false;
            }
        }

        fl_rectf(x, y + fall, 3, 3, FL_RED);
        fl_color(255, 0, 0);
        fl_begin_points();
        fl_color(FL_BLUE);
        fl_point(200, 450);
        fl_end_points();
    }

protected:
    void next_frame()
    {
        x += x_add * dir;
        fall += fall_add;
        if (fall > 400) {
            fall_add = -1.5;
        } else if (fall < 0) {
            fall_add = 0.4;
        }
        if (x > 1000) {
            dir = -1;
            y += 5;
        } else if (x < 10) {
            dir = 1;
            y += 5;
        }
    }

private:
    double timer_s = 0.010;
    int x = 0;
    int y = 400;
    double fall = 0.0;

    int x_add = 10;
    double fall_add = 0.4;
    int dir = 1;
    bool make_black = false;
};

int main(int argc, char **argv) {
    auto *window = new MyWindow(340, 480, "Hallo");
    //Fl_Window *window = new Fl_Window(1920, 1080);
    //window->fullscreen();
    // resizable?
    Fl_Box *box = new Fl_Box(20,40,300,100,"Hello, World!");
    box->box(FL_UP_BOX);
    box->labelfont(FL_BOLD+FL_ITALIC);
    box->labelsize(36);
    box->labeltype(FL_SHADOW_LABEL);
    box->align(FL_ALIGN_RIGHT | FL_ALIGN_INSIDE);

    auto* box2 = new Fl_Box(0, 0, 300, 40, "Test");
    //box2->align(FL_ALIGN_LEFT); // does not work?

    auto* box3 = new Fl_Box(0, 0, 40, 40, "@->");
    //box3->align(FL_ALIGN_RIGHT);

    auto* button = new Fl_Button(0, 150, 100, 20, "fscr");
    button->callback(fscr_callback, nullptr);

    window->end();
    window->show(argc, argv);
    return Fl::run();
}
