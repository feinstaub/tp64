import QtQuick 2.0

Item {
    id: block

    property string color1: "#FF0000"
    property string color2: "#AA0000"

    Rectangle {
        anchors.fill: parent
        anchors.margins: 1
        color: block.color1
        border.width: 1
        border.color: block.color2
        radius: 5
    }
}
