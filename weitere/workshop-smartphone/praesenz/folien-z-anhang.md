Anhang
------

### alt: https://www.mehr-freiheit.org/limux

"Öffentliche Infrastruktur muss unabhängig von kommerziellen Einzelinteressen, die bekanntermaßen Innovationen ausbremsen, bleiben. Freie Software bietet die einmalige Möglichkeit in gemeinsame Werte zu investieren und von den Beiträgen aller zu profitieren, während die Kontrolle, was wann eingesetzt wird, erhalten bleibt. Lokale Dienstleister, die in gesundem Wettbewerb miteinander stehen, stärken die lokale Wirtschaft und sichern die optimale Nutzung von Steuergeldern."

---

## Apple vs. Langlebigkeit

* Je älter ein Apple-Gerät wird, desto unwahrscheinlicher sind Sicherheits-Updates von Apple.
* Die Geräte sind so konzipiert, dass keine andere Betriebssystem-Software, außer die von Apple darauf laufen kann.
* Das heißt ein altes Gerät kann man - obwohl es noch funktionstüchtig wäre - leider nur noch dem Recycling zuführen.
* https://stallman.org/apple.html

---

##  Von mehr-freiheit.org / München

* mehr-freiheit.org
* "Freie Computerprogramme, die Deine Rechte als Nutzer/-in respektieren und schützen."
* Sortiert nach Kategorien.
* Lauffähig auf Windows, Mac, GNU/Linux

---

## Was ist »Freie Software«?

* siehe https://mehr-freiheit.org/pdf/web.pdf
    * "Freie Computerprogramme, die Deine Rechte als Nutzer/-in respektieren und schützen."
    * vier wesentliche Freiheiten
    * Lizenz
    * Quellcode
    * Finanzierung
    * Ethische Grundlage

---

## Freie SW einfordern

* Freie Software im öffentlich-rechtlichen Bereich einfordern
* z. B. Krankenkasse, Bank, falls dort unfreie Apps angeboten werden

---

## freiesoftwareog.org (offline)

* Flyer mit Beispielen für freie Software-Programme
    * http://www.freiesoftwareog.org/downloads/FSOG_Flyer-Debian.pdf

---

## Überwachungsblocker

* Gemenge-Lage um Werbeblocker: z. B. ["Verlegerverband warnt vor Googles Adblocker"](https://www.heise.de/newsticker/meldung/Verlegerverband-warnt-vor-Googles-Adblocker-3733634.html), 04.06.2017

---

## E-Mail-Anbieter

* Wechselprozess:
    * Erst beim neuen Dienst anmelden
    * Später bei Yahoo etc. abmelden

---

## TL-Frage: Was ist ein Internet-Konto?

* Ein anderer Rechner im Internet mit Benutzernamen, Passwort und ggf. Daten

---

## Mehr zu F-Droid

- Oktober 2017: [F-Droid 1.0](https://f-droid.org/de/2017/10/10/fdroid-is-1.0.html)
- Repomaker: https://f-droid.org/de/repomaker/
    - nicht ein einziges Repository, sondern mehrere unabhängige

---

## App-Listen

- TODO: Übersicht von Wetterapps etc.
    - Forecastie

---

## Mehr WhatsApp-Alternativen

- **Kontalk**
    - "Bei der Verifizierung ist ein Fehler aufgetreten. Bitte versuchen Sie später es noch einmal.". Warum?

- "Eine mögliche Zwischenlösung wäre, parallel zu WhatsApp andere Programme zu installieren, die man seinen Kontakten als alternative datenschutzfreundliche Möglichkeit zur Kommunikation anbieten kann, wenn Bedarf besteht.
WhatsApp für mich das typische Beispiel eines massiven Vendor-Lockins (https://de.wikipedia.org/wiki/Lock-in-Effekt, „Ziel des Anbieters ist es, seine Kunden zwecks Gewinnmaximierung durch technische, physikalische oder sonstige Abhängigkeiten an das Unternehmen oder Produkt zu binden. Diese Kundenbindung führt dazu, dass Kunden ein Wechsel zu anderen Anbietern oder Produkten erschwert oder gar unmöglich gemacht wird.“). Sich daraus zu befreien erfordert – je nachdem wie tief man schon drin sitzt – möglicherweise immensen persönlichen Kraftaufwand.
Aus persönlicher Erfahrung kann ich sagen: wenn man sich erst gar nicht auf das Programm eingelassen hat, ist es relativ einfach sich davon weiterhin fernzuhalten, wenn man es mit verständnisvollen Menschen zu tun hat. Daher finde ich es allgemein wichtig, Menschen zu unterstützen, die WhatsApp erst gar nicht nutzen wollen, indem man sie aus essentiellen Kommunikationen nicht ausschließt und nicht nötigt, WhatsApp zu installieren."

---

## Facebook-Likebutton
- TODO: Facebook-Like-Button-Beispiel:
    - http://www.kn-online.de/News/Nachrichten-aus-Ploen/Vogelsterben-am-Ploener-See-Erkenntnisse-zur-Todesursache-erwartet
	- Warum blockt uBlock Origin nicht alles von Facebook weg?

---

## Beispiel "VanillaMusic"
- Extra das Lyrics-Modul als separate App programmiert, damit die Haupt-App (Musikspieler) kein Recht braucht, um auf das Internet zuzugreifen

---

## Privatsphäre / Überwachung

- https://www.androidpit.de/tipps-sicherheit-datenschutz-smartphone, 2016
    - User-Comment:
        Von norton security gibt es diesbezüglich einen sehr passenden Artikel: -- alle haben etwas zu verbergen

        "WARUM IST DIE PRIVATSPHÄRE IM INTERNET WICHTIG?

        Wie wichtig die Privatsphäre im digitalen Zeitalter ist, kann man erst verstehen, wenn man genau weiß, was damit gemeint ist. Erst dann kann man ihre Bedeutung richtig einschätzen. Nur allzu oft sind Internetnutzer der Meinung 'Ich tue nichts Unrechtes, deshalb habe ich auch nichts zu verbergen '. Mit diesem Mythos möchten wir hier aufräumen. Tatsache ist: Recht oder Unrecht, wir haben sehr wohl einiges zu verbergen.

        "WIR ALLE HABEN ETWAS ZU VERBERGEN. DAS KÖNNEN DIE HÖHE UNSERES GEHALTS, UNSERE KRANKENAKTE ODER UNSERE BANKKONTONUMMER SEIN. ETWAS ZU VERBERGEN IST AN SICH NICHTS SCHLECHTES."

        PRIVATSPHÄRE – WAS IST DAS?

        Privatsphäre ist, wenn Sie vor dem Schlafengehen die Vorhänge zuziehen. Privatsphäre ist die Untersuchung bei Ihrem Hausarzt hinter geschlossenen Türen. Während wir uns in unserem Alltagsleben über diese Art der Privatsphäre kaum Gedanken machen, ist das Konzept von Privatspäre im digitalen Umfeld nicht so greifbar. Das liegt in erster Linie daran, dass manche Menschen einfach eine falsche Vorstellung davon haben, was digitale Privatsphäre eigentlich ist.

        WAS GENAU VERSUCHEN WIR ZU SCHÜTZEN?

        Manche glauben, dass es nur um das geht, was sie tun. Doch das ist nur die eine Seite der Medaille. Online-Privatsphäre betrifft weniger das, was Sie tun, sondern vielmehr wer Sie sind UND was Sie tun. Daten im Internet sind für andere wertvoll. Sie werden gestohlen, verkauft, gesammelt und analysiert.

        Die Privatsphäre hat viele Facetten. Dabei geht es um das, was Sie tun, und wer Sie sind. Wer sie sind, wird durch Ihre personenbezogenen Daten verkörpert – Name, Geburtsdatum, Anschrift, Sozialversicherungsnummer, Telefonnummern und zahlreiche weitere Daten. Das, was Sie tun, sind die Suchen, die Sie durchführen, die Websites, die Sie besuchen, die Artikel, die Sie lesen, und sogar, was Sie im Internet einkaufen.

        Sobald Sie eine App herunterladen, eine Website besuchen oder in sozialen Medien unterwegs sind, können Sie davon ausgehen, dass das jeweilige Unternehmen Daten über Sie sammelt. Inzwischen erledigen wir unglaublich viele Dinge mithilfe unserer Computer und Mobilgeräte. Wir kaufen ein, holen uns medizinischen Rat, buchen Urlaube, sind mit Freunden und Verwandten in Kontakt und erledigen noch tausend andere Dinge über das Internet. Mit all diesen Aktivitäten hinterlassen Nutzer ungewollt eine breite digitale Spur aus Daten über ihre Person. Auch wenn dies nicht unbedingt personenbezogene Daten sind, zeichnen diese Unternehmen dennoch Ihre Online-Aktivitäten auf und sammeln diese Informationen, um sich ein besseres Bild von Ihnen zu machen.

        WOZU KÖNNEN DIESE DATEN VERWENDET WERDEN?

        Ein vollständiger Datensatz über eine Person kann ein nettes Sümmchen einbringen – und zwar ganz legal. Inzwischen gibt es so genannte 'Datenbroker', die Daten über Millionen von Menschen sammeln und speichern. Die Daten werden von ihnen analysiert, zu Paketen zusammengestellt und dann ohne Wissen oder Erlaubnis des Benutzers verkauft. Datenbroker sammeln und verkaufen Informationen zu verschiedenen Zwecken an andere Unternehmen, beispielsweise für gezielte Werbung, Bonitätsprüfungen und Direktmarketing. Diese Daten sind jedoch in der Regel anonymisiert und enthalten keine personenbezogenen Daten.

        WARUM IST PRIVATSPHÄRE WICHTIG?

        Wir alle haben etwas zu verbergen. Das können die Höhe unseres Gehalts, unsere Krankenakte oder unsere Bankkontonummer sein. Etwas zu verbergen ist an sich nichts Schlechtes. Derartige Informationen gehören bestimmt nicht zu den Dingen, von denen Sie möchten, dass sie in Ihrem sozialen Netzwerk für alle Welt sichtbar verbreitet werden. Daher sollten wir darauf bedacht sein, unser Recht auf Privatsphäre zu schützen.

        Anfang des Jahres kaufte eine junge Frau einige harmlose Dinge wie Wattebäusche, eine unparfümierte Lotion und einige Vitamine im Internet. Aufgrund dessen, was das Unternehmen bereits über sie wusste, konnte es richtig vorhersagen, dass sie schwanger war, und schickte ihr per Post Coupons für Babyartikel. Das Dumme war nur, dass sie ein Teenager war und diese Coupons ihrem Vater (sehr zu seiner Bestürzung) verrieten, dass sie tatsächlich schwanger war.

        Denken Sie immer daran: Ihre Privatsphäre gehört IHNEN. Es sind Ihre Informationen, Ihre Gewohnheiten, Ihre Verhaltensmuster und Ihre Aktivitäten. Deshalb sollten Sie alles tun, um sie zu schützen."

        [Quelle: de.norton.com ]

        Und dazu gehören, wie im Artikel auch treffend formuliert, Alternativen zu WhatsApp, Google und co, als da wären z.B. Threema, wire oder Signal als Messenger, Startpage als Suchmaschine, protonmail als sicherer Mailclient für verschlüsselten Mailverkehr, und...und...und...
