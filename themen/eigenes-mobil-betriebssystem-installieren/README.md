Eigenes Mobil-Betriebssystem installieren
=========================================

Beispiele siehe [Replicant 6, LineageOS](../weitere/workshop-smartphone/mobile-os-installieren/)

Weiteres
--------
* [Auswahlprozess beim Kauf](../../weitere/workshop-smartphone/neues-smartphone-2018-1.md)
* [App-Auswahl](../../weitere/workshop-smartphone/)
