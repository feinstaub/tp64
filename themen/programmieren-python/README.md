Programmiersprache Python
=========================

<!-- toc -->

- [Einstiege](#einstiege)
  * [Turtle auf Opentechschool](#turtle-auf-opentechschool)
  * [Qt Painter mit Breakout](#qt-painter-mit-breakout)
  * [if und while mit Warenautomat](#if-und-while-mit-warenautomat)
  * [CS-Circles](#cs-circles)
  * [python4kids.net/how2think](#python4kidsnethow2think)
  * [python-kurs.eu](#python-kurseu)
  * [Weitere](#weitere)
- [Thonny Python IDE](#thonny-python-ide)
  * [Weitere: KDevelop](#weitere-kdevelop)
- [Weiterführend](#weiterfuhrend)
  * [Spieleprogrammierung: Pygame für Python 3](#spieleprogrammierung-pygame-fur-python-3)
  * [Spieleprogrammierung: allgemein](#spieleprogrammierung-allgemein)
- [Python FAQ](#python-faq)
  * [How to flush output of Python print?](#how-to-flush-output-of-python-print)
  * [print no line break](#print-no-line-break)

<!-- tocstop -->

Einstiege
---------
### Turtle auf Opentechschool
* [Python-Einstieg mit Turtle](../../anleitungen/python-turtle/python-turtle-on-opentechschool.md)

### Qt Painter mit Breakout
* [Breakout-Spiel mit PyQt](../../anleitungen/pyqt-breakout-spiel)
* siehe auch [Python-Qt-Einstieg](../../anleitungen/python-qt)

### if und while mit Warenautomat
* [Warenautomat](../../anleitungen/python-warenautomat/README.md)

### CS-Circles
* Online
* [Python-Grundlagen-Kurs auf CS-Circles](../../anleitungen/python-on-cscircles.md)

### python4kids.net/how2think
* guter Einstieg
* [Python / How to think like a computer scientist](../../anleitungen/python-howtothink.md)

### python-kurs.eu
* mit Vorkenntnissen
* [Python3-Tutorial (python-kurs.eu)](../../anleitungen/python-kurs.eu.md)

### Weitere
* Deutsches Python-Tutorial
    * Start: https://py-tutorial-de.readthedocs.io/de/python-3.3/introduction.html
    * Probiere den Python-Code aus
        * indem du diese Python-Online-Shell verwendest: https://repl.it/languages/python3
        * oder den deinen Editor
    * Bekannte Probleme:
        * Was ist eine Fließkommazahl? --> Das ist noch nicht wichtig zu wissen; bei Interesse siehe [https://de.wikipedia.org/wiki/Gleitkommazahl Wikipedia].
        * Der Unterstrich (_) funktioniert bei mir nicht. --> Die Online-Konsole scheint das nicht zu unterstützen. In diesem Fall überspringen.
        * Der Abschnitt zu "komplexen Zahlen" ist unverständlich. --> Wenn du noch nicht gelernt hast, was komplexe Zahlen sind, dann bitte überspringen.

* todo:
    * [Python-Code_in_Dateien](http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Programmier-Einstieg/Python-Code_in_Dateien)

Thonny Python IDE
-----------------
* https://thonny.org/
* https://realpython.com/python-thonny/ (todo)

### Weitere: KDevelop
* siehe anleitungen/python-mit-kdevelop/README.md

Weiterführend
-------------
* siehe programmieraufgaben.md

### Spieleprogrammierung: Pygame für Python 3
* $ sudo zypper install python3-pygame
* https://www.pygame.org/docs/tut/PygameIntro.html
    * Deutsche Anleitung: https://www.spieleprogrammierer.de/wiki/Pygame-Tutorial
    * Deutsche Übersetzung der Original-Doku: http://www.pygame-doku.laymaxx.de (braucht noch [Hilfe bei der Übersetzung](../anleitungen/translate-to-german))
* Tutorial-Beispiel siehe [pygame3.py](../unsorted/pygame3.py)

### Spieleprogrammierung: allgemein
* Tutorials: https://www.spieleprogrammierer.de/wiki/Kategorie:Tutorial
    * Framerate-unabhängige Spiellogik: https://www.spieleprogrammierer.de/wiki/Framerate-unabh%C3%A4ngige_Spiellogik
    * 2D-Kollisionserkennung: https://www.spieleprogrammierer.de/wiki/2D-Kollisionserkennung
    * https://www.spieleprogrammierer.de/wiki/Konzepte_f%C3%BCr_Jump_and_Run-Spiele
    * https://www.spieleprogrammierer.de/wiki/Wann_macht_ein_Spiel_Spa%C3%9F%3F
    * https://www.spieleprogrammierer.de/wiki/Wegfindung_mit_A*
    * Die SDL: https://www.spieleprogrammierer.de/wiki/SDL-Tutorial
* siehe auch http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Unsortiert/Python3-Game-Programming
    * todo: move to here
    * Löve/love, Beispiel
        * https://www.proli.net/2017/07/05/snap-in-discover-and-the-gnulinux-desktop/
        * http://www.omgubuntu.co.uk/2017/01/oh-my-giraffe-ubuntu-snap-download

Python FAQ
----------
### How to flush output of Python print?
* https://stackoverflow.com/questions/230751/how-to-flush-output-of-python-print

    import sys
    sys.stdout.flush()

### print no line break

    print("Sage etwas: ", end='')
