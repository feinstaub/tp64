LAN-Chat mit BeeBEEP
====================

<!-- toc -->

- [Was ist BeeBEEP?](#was-ist-beebeep)
- [Installieren](#installieren)
  * [Details](#details)
- [Starten](#starten)
- [Jemanden anschreiben](#jemanden-anschreiben)
- [Konfigurieren](#konfigurieren)
- [Weiterführend](#weiterfuhrend)

<!-- tocstop -->

Was ist BeeBEEP?
----------------
* Mit BeeBEEP kann man innerhalb eines lokalen Netzwerks Nachrichten austauschen.
* Es funktioniert ohne eine Server-Komponente.
* Die Nachrichten werden verschlüsselt übertragen.
* Man kann auch Dateien austauschen.
* Die Software ist mit Qt/C++ programmiert und läuft auf mehreren Betriebssystemen.
* Webseite: http://beebeep.sourceforge.net
* Freie Software: GPLv3

Installieren
------------
* Voraussetzung: [Wie installiert man Software (todo-link)](todo)
* Via software.opensuse.org
* Paketname: **beebeep**
* Repository: **network**

### Details
* https://software.opensuse.org aufrufen
* Auf openSUSE Tumbleweed umstellen
![](software-opensuse-auf-tumbleweed.png)
* Nach "beebeep" suchen
* openSUSE Tumbleweed -> "Show unstable packages"
* ("1-Click Install" rechts funktioniert hier NICHT, weil dort ein falsches Repo ausgewählt wird: ARM_Factory anstelle von Tumbleweed)
* "64 Bit" anklicken
* Open with: "Install/Remove Software" auswählen, falls nicht schon getan. OK
* root-Passwort eingeben
* YaST öffnet sich. Unten rechts Accept (Akzeptieren) drücken.

Starten
-------
* Firewall ausschalten, sonst können einen die anderen nicht sehen
    * [alte Anleitung](http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Eigenes_System/Lokales_Netzwerk)
    * NEU: KRunner -> Firewall (root-Passwort eingeben) -> "Stop Firewall Now"

* beebeep über das K-Menü oder KRunner starten.

* Folgenden Dialog mit OK bestätigen:

![](beebeep-startup-1.png)

Jemanden anschreiben
--------------------
* Gewünschten Benutzer rechts anklicken und etwas schreiben.

Konfigurieren
-------------
* Eigenen Benutzernamen einstellen (todo)

Weiterführend
-------------
* Weitere Hilfe zum Programm
    * http://beebeep.sourceforge.net/help.php
* Der Autor
    * http://beebeep.sourceforge.net/me.php
        * Hobbyprojekt
        * "See? That was nothing. But that's how it always begins. Very small." (Filmzitat)
* Chatbot-Ideen
    * Auto-reply-Taschenrechner
    * Chatbot Eliza einbauen: http://www.linuxjournal.com/content/it-live-or-it-chatboteliza
    * Ein figlet senden?
    * siehe User-Request: https://sourceforge.net/p/beebeep/tickets/297/ (Chatbot)
    * https://snoonet.org/gonzobot Command List
    * Chat-Bots senden sich gegenseitig irgendwas zu (Berechungsanfragen?) und man schaut dabei zu?
        * z. B. ["Chatbots at the ready"](https://www.youtube.com/watch?v=IMsd-TAUqv8) -> GO
        * Bot 1: Was ist <5 * 8>?
        * Bot 2: 5 * 8 = 40
        * Bot 1: Gut gemacht (richtig / super / klasse / toll). Was ist <5 + 10>?
        * ...
