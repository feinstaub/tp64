#!/bin/bash

# MORE scripts see meta/

# NOTE: this uses the globally installed script, codestruct-util
# needs https://github.com/jonschlinkert/markdown-toc
# use `npm install` to init
# find . -type f -iname '*.md' -exec echo Process {}... \; -exec markdown-toc -i {} \;
export PATH=$(pwd)/node_modules/.bin:$PATH
tool-markdown-toc-all
