#!/usr/bin/env python3

# hilfe:py:input

print("Wie alt bist du?")
a = input()

b = input("Wie groß bist du (in cm)? ")

print("Du bist " + a + " Jahre alt.")
print("Du bist " + b + " cm groß.")

# AUFGABEN:
#
# 1. Was ist der Unterschied zwischen den beiden Varianten von input()?
#
# 2. Frage nach dem Namen des Benutzers und gebe ihn in den anderen Antworten mit aus.
#
# 3. Schreibe eine Ausgabe "Hallo X, du bist Y Jahre alt und Z cm groß."
