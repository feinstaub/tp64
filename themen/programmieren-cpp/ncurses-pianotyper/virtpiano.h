#pragma once
#include <filesystem>
#include <deque>
#include "noten_tasten.h"
#include "programkeys.h"
#include "tools.h"
#include "player.h"

class VirtPiano
{
public:
    void print_menu(const ProgramKeys& pkeys)
    {
        using namespace std;
        tools::terminal_clear();
        cout << "Virtual Piano: ";
        for (const ProgramKey& pkey : pkeys.get()) {
            cout << pkey.to_string() + " ";
        }
        cout << endl;
    }

    void start()
    {
//             cout << "Hallo" << flush;
//             // TODO read sheet file for elise and display notes, then let the user play it
//             //      change overtone
//             tools::sleep_ms(500);


        using namespace std;

        MusicSheet sheet = MusicSheet::from_file("noten/fuer-elise.msheet").at(0);
        //MusicSheet sheet = MusicSheet::from_file("noten/tetris-theme.msheet").at(0);

        ProgramKeys pkeys;
        const int key_show_keys         = pkeys.register_key('H', 2, "Zeige Noten");
        const int key_exit              = pkeys.register_key('E', 2, "Zurück");
        // todo: dauer up, down
        // todo: overtone plus, minus
        // TODO: Enter: cycle Song?

        deque<int> tasten;
        while (true)
        {
            print_menu(pkeys);

            cout << sheet.title_summary() << "..." << endl;
            cout << "Virtual Piano string: " << sheet.noten_to_virtual_piano_string() << endl;
            deque<int> tt = tasten;
            reverse(tt.begin(), tt.end()); // algorithm
            int i = 0;
            for (int taste : tt) {
                if (i == 0) {
                    cout << "[ ";
                }
                cout << taste << "(" << taste_zu_name(taste) << ") ";
                if (i == 0) {
                    string s(taste, '#');
                    cout << s << " ]" << endl << "    ";
                }
                i++;
            }
            //cout << ".";

            // TODO: print status: dauer, overtone, note etc.
            // TODO: Akkord-Erkennung

            char ch = 0;
            {
                tools::terminal_noncanoical_guard g(false);
                ch = getchar();
            }

            int key_handle = pkeys.key_pressed(ch);

            if (key_handle == key_show_keys) {
                print_tasten();
                cout << "Press any key..." << flush;
                tools::terminal_noncanoical_guard g(false);
                getchar();
            } else if (key_handle == key_exit) {
                break;
            } else {
                string vpstr;
                if (pkeys.shiftPressed()) {
                    vpstr = "S";
                }
                vpstr += tolower(ch);
                int taste = virtualpiano_zu_taste(vpstr);
                if (taste != -1) {
                    double duration_s = 0.4;
                    play_note_from_taste_async(taste, duration_s);
                    tasten.push_back(taste);
                    if (tasten.size() > 8) {
                        tasten.pop_front();
                    }
                }
            }
        }
    }

private:
};
