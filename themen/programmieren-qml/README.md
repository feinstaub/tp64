QML
===

<!-- toc -->

- [Qt-Code-Beispiele](#qt-code-beispiele)
- [Experimente](#experimente)
- [Animiertes Hintergrundbild](#animiertes-hintergrundbild)
- [Mehr](#mehr)
  * [Python](#python)
  * [Qt Quick Designer](#qt-quick-designer)

<!-- tocstop -->

Qt-Code-Beispiele
-----------------
Beispiele installieren (430 MB)

    sudo zypper in "libqt5-*-examples"

    rpm -ql libqt5-qtdeclarative-examples | grep same

* samegame
    * /usr/lib64/qt5/examples/quick/tutorials/samegame/samegame4/
    * samegame ausführen: Spiel mit schönen '''Partikeleffekten''' erscheint.
    * /usr/share/doc/packages/qt5/qtdoc/qtdoc-demos-samegame-example.html

* maroon
    * maroon ausführen: Ein schönes '''Meeresspiel''' erscheint.
    * /usr/share/doc/packages/qt5/qtdoc/qtdoc-demos-maroon-example.html
    * todo: Wo ist das Paket mit den Quellen?

Experimente
-----------
* canvas
* color-dialog-example
* video-example2

Animiertes Hintergrundbild
--------------------------
* siehe [animiertes-hintergrundbild-mit-qml](../plasma-desktop/animiertes-hintergrundbild-mit-qml.md)

Mehr
----
### Python
http://pyqt.sourceforge.net/Docs/PyQt5/qml.html

### Qt Quick Designer
* 2017: ["Qt Quick Designer – The Coffee Machine"](https://blog.qt.io/blog/2017/07/05/qt-quick-designer-coffee-machine/)
