#include <iostream>

namespace game {

/**
 * "##"   => x = 0, len = 2
 * "_#_#" => x = 1, len = 3
 *
 * x -1 => len undefined
 */
int find_x_pos(int* len, const std::string& s)
{
    int p1 = s.find("#");
    if (p1 == std::string::npos) {
        return -1;
    }
    int p2 = s.find("#", p1 + 1);
    if (p2 == std::string::npos) {
        return -1;
    }
    *len = p2 - p1 + 1;
    return p1;
}
}
