#!/bin/bash

# see https://commons.wikimedia.org/wiki/Commons:Video
wget -O video1.ogv https://upload.wikimedia.org/wikipedia/commons/9/9a/Time_Lapse_of_New_York_City.ogv
wget -O video2.ogv https://upload.wikimedia.org/wikipedia/commons/5/50/Flash-Lightning_over_Germany.ogv

# https://commons.wikimedia.org/w/index.php?title=Special:Search&profile=images&fulltext=Search&search=waterfall++video&uselang=en&searchToken=c74j8ok8voi7zvj0mwpzcwwos
# Akame 48 Waterfalls-Senjyudaki-video.ogv
wget -O video3.ogv https://upload.wikimedia.org/wikipedia/commons/2/27/Akame_48_Waterfalls-Senjyudaki-video.ogv

# "Daft Punk - Fall" passt gut als Hintergrundmusik
