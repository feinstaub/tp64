import QtQuick 2.0
import "samegame.js" as SameGame

Rectangle {
    id: screen

    width: 490; height: 720

    SystemPalette { id: activePalette }

    Component.onCompleted: {
        SameGame.startNewGame()
    }
    onWidthChanged: {
        SameGame.startNewGame()
    }
    onHeightChanged: {
        SameGame.startNewGame()
    }

    Item {
        width: parent.width
        anchors { top: parent.top; bottom: toolBar.top }

        MyBackground {
            id: background
            anchors.fill: parent
        }
    }

    Rectangle {
        id: toolBar
        width: parent.width; height: 30
        color: activePalette.window
        anchors.bottom: screen.bottom

        Button {
            anchors { left: parent.left; verticalCenter: parent.verticalCenter }
            text: "New Game"
            onClicked: SameGame.startNewGame()
        }

        Text {
            id: score
            anchors { right: parent.right; verticalCenter: parent.verticalCenter }
            text: "Score: Who knows?"
        }
    }
}
