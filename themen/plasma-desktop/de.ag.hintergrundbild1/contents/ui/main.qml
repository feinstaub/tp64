import QtQuick 2.0
import QtMultimedia 5.0

Item {
    Rectangle {
        id: rectangle1
        color: "#1d3be3"

        anchors.fill: parent

        Rectangle {
            x: 79
            y: 107
            width: 570
            height: 228
            color: "#aaa7ff"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            AnimatedImage { id: animation; source: "animated-image.gif" }
        }
    }
}
