Aufgabe: Pong
=============

<!-- toc -->



<!-- tocstop -->

Voraussetzung: Grundkenntnisse in Python3 und Qt.

Programmiere das Spiel [Pong](https://de.wikipedia.org/wiki/Pong) mit Python3 und Qt5.

Die Aufgabe ist erfüllt, wenn

* der linke Spieler mit den Tasten W und S seinen Schläger steuert
* der rechte Spieler mit den Cursor-Tasten seinen Schläger steuert
* Punkte gezählt und angezeigt werden
* das Programm auch bei deinem Nachbarn auf dem Rechner läuft

Bonus:

* Töne ausgeben
* Punktestand mittels '''xcowsay''' ausgeben
* Optional eigene Pong-Regeln einführen und umsetzen

todo: Tipps und Hinweise
