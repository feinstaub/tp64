Manjaro-Linux-Installation-Notes
================================

<!-- toc -->

- [Dez 2018](#dez-2018)
  * [Pakete](#pakete)
  * [Probleme](#probleme)
  * [User-Guide](#user-guide)
  * [audaspace](#audaspace)
  * [Dateisystem](#dateisystem)
  * [TODO](#todo)
- [FAQ](#faq)
- [VNC-Server](#vnc-server)
- [manjaro32.org](#manjaro32org)

<!-- tocstop -->

Dez 2018
--------
* Vorteile oder gleich zu openSUSE
    * Manjaro-Settings für Sprachen, Kernel und Hardware-Erkennung
    * für AG-Inf
        * enthaelt scratch
        * python3 als default
        * yakuake und htop vorinstalliert
        * moon-lander via AUR

### Pakete
* FreeFileSync nicht als Paket, aber binaer von Webseite
    * aber von AUR!
    * Problem with https://aur.archlinux.org/packages/freefilesync-bin/
        * TODO: try later
* git-cola via AUR
* kdevelop
    * suggestions: gdb qt5-doc cmake cppcheck heaptrack kdevelop-clang-tidy clazy
* virt-manager: OK

### Probleme
* hibiscus via AUR
    * error: jameica-linux64-2.8.2.zip ... FAILED (unknown public key 5A8ED9CFC0DB6C70)
    * Fehler bekannt, siehe https://aur.archlinux.org/packages/hibiscus
        * DONE: report error
        * TODO: fix it

### User-Guide
file:///usr/share/doc/manjaro/Beginner_User_Guide.pdf

* empfiehlt openSUSE ImageWriter (imagewriter)
* Manjaro 32 Bit http://manjaro32.org/
* AUR verwenden, wie?
    * install trizen and enable in options, see https://wiki.manjaro.org/index.php/Arch_User_Repository
        * TODO: lesen! ...
* weiter mit Part II Installing
    * ... ...

### audaspace
* siehe audio, musik

### Dateisystem
* welches Dateisystem? -> ext4

### TODO
* tunneler paketieren!
* moon-lander-OpenSUSE-Patches in eigenes Repo
* LVM fuer Partionierung ausprobieren?
* CLion und PyCharm verwenden?
    * gute Features: https://www.jetbrains.com/clion/

FAQ
---
* AUR aktivieren
    * trizen installieren, siehe https://www.slant.co/versus/4906/4910/~yaourt_vs_trizen

VNC-Server
----------
* VNC Server und TightVNC auf Windows ausprobieren
    * moon-lander (SDL) zum Vergleich der Animationsgeschw. ausprobieren
        * OpenSSH: https://wiki.archlinux.org/index.php/OpenSSH#X11_forwarding
            * geht flüssig im Vergleich zu ssh+X

* Installation und Config
    * https://wiki.archlinux.org/index.php/TigerVNC#Running_vncserver_for_virtual_(headless)_sessions
        * TODO

manjaro32.org
-------------
manjaro-xfce-18.0.0-x32-stable-minimal-i686.iso "Date: 2018-12-18 00:47"

TODO: ausprobieren
