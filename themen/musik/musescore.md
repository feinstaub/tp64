MuseScore
=========

Start
-----
Erste Schritte mit MuseScore, dem Programm um Notenblätter zu erzeugen, abzuspielen und zu drucken.

1. Schaue, ob MuseScore bereits installiert ist. Wenn nicht, dann installiere es z. B. via 'software.opensuse.org'. Wähle dabei das 'Education'-Repository aus.

2. Starte 'MuseScore' und wähle die "Erste Schritte"-Partitur. Diese liegt nur auf Englisch vor.

3. Drücke den 'Play'-Knopf in der Symbolleiste.

4. Lese dir folgende Anleitung durch: https://musescore.org/de/handbuch/noteneingabe-0 (deutsch). Danach kannst du selbst eigene Note eingeben und bearbeiten.

Weiteres
--------
* Handbuch Deutsch: https://musescore.org/de/handbuch
