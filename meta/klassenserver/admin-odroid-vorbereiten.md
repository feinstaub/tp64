ODROID C1+ vorbereiten
======================

* Kein Linux-Rechner vorhanden? -> ODROID verwenden

ODROID
------
* https://de.wikipedia.org/wiki/ODROID

ODROID-C1+
----------
### Spezifikation
* AmLogic S805 Quad-Core Cortex A5 @1500MHz
* 4xUSB 2.0,1xUSB 2.0 OTG
* LAN (10/100/1000)
* 1 GB DDR3 RAM
* Micro SD Card Slot
* eMMC Slot

* Material:
    * Forum: https://forum.odroid.com/
    * Magazin: https://magazine.odroid.com/
        * LineageOS installieren

### Aufbau
* 16 GB SanDisk Ultra Micro-SD-Karte
    * unten einstecken
* LAN-Kabel anschließen
* Netzteil anschließen
    * Rote LED leuchtet
    * Blaue LED fängt an zu blinken und hört nicht mehr auf
* Einloggen
    * Browser: http://live-pc.local:5900 (noVNC)
    * Login: live
    * Password: live
* Erkunden
    * Netrunner Core OS (https://www.netrunner.com)
    * Plasma 5.7.4
    * Firefox installiert
    * python3 installiert

### Einmalig einrichten

Klick auf "First Time Run This Installer" -> Netrunner Core Installationsprogramm

* Sprache: Deutsch
* Standort: Berlin
* Tastatur: German, Default
* Benutzer:
    * ag
    * pw: linux (Passwort ändern! Denn das ist der Root-User)
        * Ändern mit passwd: --> linux123
    * Name des Computers: ag-pc
* Skript: Es gab eine Fehlermeldung beim Partitionieren, aber trotzdem auf Weiter geklickt
* Installieren: Alles erledigt
    * Neustarten oder in der Live-Umgebung fortfahren
* "Jetzt neustarten"-Haken drinlassen und Beenden drücken.

* http://live-pc.local:5900 geht jetzt nicht mehr :(
    * "Server disconnected (code: 1006)"

    $ ssh ag@ag-pc
        Permission denied (publickey).

    $ ssh-copy-id -i ~/.ssh/id_dsa.pub ag@ag-pc
        /usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/gregor/.ssh/id_dsa.pub"
        /usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
        /usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
        Permission denied (publickey).

Einloggen, X und Runterfahren
-----------------------------
### sshd für Passwort-Login konfigurieren

* SD-Karte in einen Linux-Rechner stecken und mit dem Plasma-Device-Manager mounten
    * wenn der Schalter bei dem Micro-zu-Normal-SD-Adapater auf der falschen Stellung steht, schlägt das Mounten fehl
* Datei editieren ([siehe auch](https://askubuntu.com/questions/101670/how-can-i-allow-ssh-password-authentication-from-only-certain-ip-addresses))
    $ sudoedit /etc/ssh/sshd_config

    vorher: PasswordAuthentication no
    nachher: PasswordAuthentication yes

* Unmount
* Micro-SD-Karte wieder in den Odroid einstecken und starten (= Netzteil anschließen)

    $ ssh ag@ag-pc
    ag@ag-pc's password:

    The programs included with the Debian GNU/Linux system are free software;
    the exact distribution terms for each program are described in the
    individual files in /usr/share/doc/*/copyright.

    Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
    permitted by applicable law.
    Last login: Fri Nov 17 13:14:15 2017 from linux-vu7g.fritz.box

### Mit X-Forwarding einloggen
siehe admin-main.md

### Welche Rechte hat ag?

    $ ag@ag-pc:~$ groups
    ag lp sudo audio video network storage

(siehe auch "Remote Audio")

### System runterfahren

    $ ag@ag-pc:~$ sudo shutdown now
        [sudo] password for ag:                          (Passwort von ag)
        Connection to ag-pc closed by remote host.
        Connection to ag-pc closed.

Warte bis die blaue LED nicht mehr blinkt. Dann Netzstecker ziehen.

### ssh-copy-id

    $ ssh-copy-id -i ~/.ssh/id_dsa.pub ag@ag-pc
    /usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/gregor/.ssh/id_dsa.pub"
    /usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
    /usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
    ag@ag-pc's password:

    Number of key(s) added: 1

    Now try logging into the machine, with:   "ssh 'ag@ag-pc'"
    and check to make sure that only the key(s) you wanted were added.


    $ ssh ag@ag-pc
    The programs included with the Debian GNU/Linux system are free software;
    the exact distribution terms for each program are described in the
    individual files in /usr/share/doc/*/copyright.

    Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
    permitted by applicable law.
    Last login: Fri Nov 17 13:25:53 2017 from linux-vu7g.fritz.box

OK.

git installieren
----------------
    $ ag@ag-pc:~$ git
        Could not find the database of available applications, run update-command-not-found as root to fix this
        git: command not found

    $ ag@ag-pc:~$ sudo update-command-not-found git
        Downloading complete file http://httpredir.debian.org/debian/dists/jessie/main/Contents-armhf.gz
        % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                        Dload  Upload   Total   Spent    Left  Speed
        100   329  100   329    0     0   4061      0 --:--:-- --:--:-- --:--:--  4112
        100 25.7M  100 25.7M    0     0  6127k      0  0:00:04  0:00:04 --:--:-- 6361k
        Downloading complete file http://httpredir.debian.org/debian/dists/jessie/contrib/Contents-armhf.gz
        % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
        ...
        ...
        ...

        bleibt hängen
        Ctrl+C
        nochmal
        bleibt wieder hängen

    $ ag@ag-pc:~$ sudo apt-get install git
        ...
        Nach dieser Operation werden 16,6 MB Plattenplatz zusätzlich benutzt.
        Möchten Sie fortfahren? [J/n]
        ...
        E: Einige Archive konnten nicht heruntergeladen werden; vielleicht »apt-get update« ausführen oder mit »--fix-missing« probieren?

    $ ag@ag-pc:~$ sudo apt-get update
        ...
        Es wurden 19,5 MB in 24 s geholt (806 kB/s).
        W: Fehlschlag beim Holen von http://ds9-eu.s3-website.eu-central-1.amazonaws.com/dci/packages/extras/dists/jessie/main/binary-armhf/Packages  404  Not Found

        E: Einige Indexdateien konnten nicht heruntergeladen werden. Sie wurden ignoriert oder alte an ihrer Stelle benutzt.

    $ ag@ag-pc:~$ sudo apt-get install git

OK

Nicht-sudo-User anlegen
-----------------------
* siehe [admin-new-student-account.md](admin-new-student-account.md)

Weiteres
--------
### X2Go installieren?
* todo: Braucht man das?
* Server: https://wiki.x2go.org/doku.php/doc:installation:x2goserver
    * Debian: $ sudo apt-get install x2goserver x2goserver-xsession

    [sudo] password for ag:
    Paketlisten werden gelesen... Fertig
    Abhängigkeitsbaum wird aufgebaut.
    Statusinformationen werden eingelesen.... Fertig
    E: Paket x2goserver kann nicht gefunden werden.
    E: Paket x2goserver-xsession kann nicht gefunden werden.
