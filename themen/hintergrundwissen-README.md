Hintergrundwissen
=================

Unsortiert
----------
* TODO: Move "Internet-Tipps" (Browser, E-Mail, Suchmaschine) to here from here: https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Eigenes_System/Internet-Tipps
    * v. a. https://search.creativecommons.org
        * Was sind die Creative Commons? Welche Lizenzen gibt es und warum? -> https://creativecommons.org/licenses/?lang=de
    * searx
    * YaCy: http://yacy.net/de/
        * https://de.wikipedia.org/wiki/YaCy
        * http://search.yacy.net/
* Online-Banking mit Hibiscus
