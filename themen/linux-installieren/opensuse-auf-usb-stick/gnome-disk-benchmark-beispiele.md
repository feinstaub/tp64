Benchmark-Beispiele
===================

1a-USB3.0-HardDrive-500GB-100samples-1MB:

![](images-benchmark/1a-USB3.0-HardDrive-500GB-100samples-1MB.png)

2a-USB3.0-Stick-16GB-100samples-1MB-Test-dauerte-10x-länger-als-1:

![](images-benchmark/2a-USB3.0-Stick-16GB-100samples-1MB-Test-dauerte-10x-länger-als-1.png)

3a-Interne-SSD-500GB-100samples-1MB:

![](images-benchmark/3a-Interne-SSD-500GB-100samples-1MB.png)

3c-Interne-SSD-500GB-10samples-100MB:

![](images-benchmark/3c-Interne-SSD-500GB-10samples-100MB.png)

4a-USB2.0-HardDrive-80GB-100samples-1MB:

![](images-benchmark/4a-USB2.0-HardDrive-80GB-100samples-1MB.png)

5a-USB2.0-Stick-1GB-100samples-1MB:

![](images-benchmark/5a-USB2.0-Stick-1GB-100samples-1MB.png)

6a-USB2.0-Stick-4GB-10samples-10MB:

![](images-benchmark/6a-USB2.0-Stick-4GB-10samples-10MB.png)

6b-USB2.0-Stick-4GB-100samples-1MB:

![](images-benchmark/6b-USB2.0-Stick-4GB-100samples-1MB.png)

7a-USB-ScanDisk-Ultra-Fit-32-GB:

![](images-benchmark/7a-USB-ScanDisk-Ultra-Fit-32-GB-Lucas.png)

8a-USB-Stick-32GB-Kingston-Data-Traveler:

![](images-benchmark/8a-USB-Stick-32GB-Kingston-Data-Traveler-made-in-Taiwan-Julian.png)

9a-USB-Stick-32-GB-Toshiba:

![](images-benchmark/9a-USB-Stick-32-GB-Toshiba-Matthias.png)
