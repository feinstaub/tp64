import QtQuick 2.0

Item {
    width: 800
    height: 600
    Rectangle {
        id: rectangle1
        anchors.fill: parent
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }

            GradientStop {
                position: 1
                color: "#0725ca"
            }
        }
    }
}

