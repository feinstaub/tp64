#pragma once

#include <cstdlib> // std::rand

/**
 * h: height
 */
void draw_tree(int y, int x, int h)
{
    for (int i = 0; i < h; i++) {
        for (int t = 0; t < i * 2 + 1; t++) {
            // 0..1
            // 0..3
            // 0..5
            // 0..7

            mvprintw(y + i, x + t + (h - i) - 1, "T");
        }
    }
}

void demo6()
{
    myncurses_init guard;
    start_color();
    curs_set(0); // hide cursor
    clear();

    //init_pair(0, COLOR_RED,     COLOR_BLACK); // weiß
    init_pair(1, COLOR_RED,     COLOR_BLACK);
    init_pair(2, COLOR_GREEN,   COLOR_BLACK);
    init_pair(3, COLOR_YELLOW,  COLOR_BLACK);
    init_pair(4, COLOR_BLUE,    COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(6, COLOR_CYAN,    COLOR_BLACK);
    init_pair(7, COLOR_WHITE,   COLOR_BLACK); // grau

//     color_set(2, 0);
//     draw_tree(0, 0, 10);


    while (true)
    {
        int r = std::rand();
        int y = r % 20;
        int x = r % 100;
        int h = r % 10 + 4;
        int c = r % 7 + 1;

        color_set(c, 0);
        draw_tree(y, x, h);
        refresh();
        usleep(70 * 1000);
    }

    getch();
}
