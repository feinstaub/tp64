Klassenregeln
=============

Ziele
-----
* **Kontrollierter Anfang**
    * Laptops zu
    * Runde 0 (vor Beginn der Stunde)
        * Fragen vom letzten Mal klären / ohne Laptop
    * Runde 1
        * ggf. Klassenregeln wiederholen
        * Der Reihe nach berichten, was er vorhat
            * Wenn bereit für etwas neues, dann warten bis Ende der Runde 1
        * Zeit für allgemeine Fragen
    * Runde 2
        * Arbeitsblätter klar?
        * was neues? -> gemeinsam mit Kleingruppe neue Aufgabenstellung besprechen
    * Los gehts
* **Ruhiges Arbeiten mit Arbeitsblättern**
    * **Gemeinsame Basis** / kleinster gemeinsamer Nenner:
        * ruhige Atmosphäre, um Konzentration zu ermöglichen
        * unterschiedliche Altersstufen, Interessen und Geschwindigkeiten
        * deshalb Arbeitsblätter und Ruhe
    * Tabu!
        * Bei anderen Schülern ohne Einverständnis Maus, Tastatur oder Rechner anfassen, ist nicht erlaubt!
    * Fragen sind erwünscht
        * vorher melden
        * Allerdings können nicht alle Fragen sofort geklärt werden und es gibt auch keinen Anspruch darauf
            * Fragen gerne aufschreiben (z. B. in einer Text-Datei) und dem Kursleiter später zusenden
* **Pünktliches Ende** der Stunde
    * Grund: Anschlusstermine
    * Lösung: 10 Minuten vor Ende gibt es ein Signal; daraufhin alles soweit sichern und beginnen den PC herunterzufahren
        * auch wenn es noch so schön ist, zuhause kann man weitermachen oder die nächste Stunde

Fragen und Antworten
--------------------
* Austausch ist wichtig. Wie aber kann man ein **Rufen durch den Raum vermeiden**?
    * Den Empfänger der Botschaft nicht laut rufen, sondern ruhig hingehen und leise ansprechen
        * Wenn der Empfänger beschäftigt ist, in Ruhe lassen
    * Per Chat-Programm anschreiben und warten
* Was ist, wenn man **bei einem Arbeitsblatt nicht weiterkommt?**
    * Leise jemand anderes fragen (siehe auch voriger Punkt)
    * Aufschreiben, was genau das Problem ist und melden und warten

