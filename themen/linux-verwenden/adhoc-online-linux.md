Ad-hoc-Online-Linux
===================

OnWorks
-------
* Kubuntu   - https://www.onworks.net/os-distributions/ubuntu-based/free-kubuntu-online
    * root-Passwort: 123456

* OpenSUSE  - https://www.onworks.net/os-distributions/rpm-based
    * Sprache kann via auf Yast auf Deutsch umgeschaltet werden, Logout, Login mit Root-Passwort
    * OpenSUSE for Education. Viele Programme vorinstalliert.
