PianoTyper
==========

<!-- toc -->

<!-- tocstop -->


- NEXT
    - parse für elise
    - show notes im virtualpiano format
        - per Tastendruck die Referenztabelle ausgeben inkl. "VP" Tasten (mit und ohne Shift; ggf. als Unicodezeichen)
    - Piano-Mode: Noten abtippen erzeugt Musik (Shift = längerer Ton)
    - später:
        - Modi definieren und mit verschiedenen Screens arbeiten
        - Typer mode: in den Noten markieren, wie weit man im Lied schon ist
        - Typer mode 2: Text (z. B. Matrix oder Star Trek) mit Liednoten mappen und dann normaler Typer mode
    - noten/-Verzeichnis scannen und Inhalt anbieten
    - siehe htop: Tasten und Bedeutung mit Background anzeigen
    - next sheets
        - https://virtualpiano.net/twinkle-twinkle-little-star/
        - https://virtualpiano.net/happy-birthday/

### Music Machine (sox)
- für Elise
    * Noten und Frequenzen rausarbeiten
        * Klavier lernen für Elise: https://www.youtube.com/watch?v=uE9LNoqWqU0, mit Taktell
        * siehe auch virtualpiano, todo: noten E4, F3 etc.
- Noten => ogg/wav-Dateien (mit Audacity oder mit soundgen)
    * Töne mit sox generieren
        * Beispiele https://www.audiosciencereview.com/forum/index.php?threads/howto-sox-audio-tool-as-a-signal-generator.4242/
            * `sox -r 48000 -n -b 16 -c 2 tmp.wav synth 5 pl "C" pl "D" && play tmp.wavsox -r 48000 -n -b 16 -c 2 tmp.wav synth 5 pl "C" pl "D" && play tmp.wav`
            * `sox -n tmp.wav synth 1 sin 100+500 && play tmp.wav`
            * Zwei synths hintereinander: `sox -n tmp.wav synth .8 sin 100+500 sin 500 delay 0 .8 && play tmp.wav`
        * Gitarre: http://scruss.com/blog/2017/12/19/synthesizing-simple-chords-with-sox/ inkl. Noten-Notation
        * Tutorial: http://billposer.org/Linguistics/Computation/SoxTutorial.html
- Shift+Taste => längerer Ton
- Rythmus an/aus
- Möglicherweise besser mit Qt wegen Fullscreen und "Key Release", um die Tonlänge genau festzulesen
        (oder mit SDL? Aber damit ist es schwieriger Textausgaben zu machen => Qt mit Fullscreen it is)
    * genug langen Ton mit sox erzeugen (ggf. sound files cachen)
    * Key release => kill
* Beispiele
    * Virtuelle Klaviere:
        * https://virtualpiano.net/
            * https://virtualpiano.net/fur-elise/ - Buchstaben ("fDfDfadsp tupa uOas ufDfDfadsp"...) können direkt eingegeben werden
        * https://www.onlinepianist.com/virtual-piano: hier sieht man, wie man die Tasten auch verteilen kann
        * https://virtualpiano.eu/ - viele Tonvarianten
    * Synthesizer
        * https://electrictelepathy.com/web-apps/virtual-synthesizer-online/
        * https://midi.city/
- TODO
    - Akkorde (-> SheetColumn)
    - (Anzeige-Format per F-Taste umstellen: DE, EN)
    - siehe dort: https://www.moviequotesandmore.com/best-quotes-from-the-matrix-movies

Musik-Wissen
------------
* https://de.wikipedia.org/wiki/Frequenzen_der_gleichstufigen_Stimmung

### Virtual-Piano
* https://virtualpiano.net/music-sheets/how-to-play/

    a s d f     Play each note after a short pause
    asdf        Play notes one after the other
    [a s d f]   Play the sequence at fast speed
    [asdf]      Play notes together simultaneously
    as | df     Play notes after a long pause
    [as] [df]   Play notes together == ???

### Songs
https://virtualpiano.net/music-sheets/how-to-play/

    Songs From Movies
    Songs From Games
    Songs From TV
    Songs From The Stage
