#include <iostream> // cout, cin
#include <ctime>        // std::time
//#include <string> // not needed when iostream is included

/**
 * forward-Deklarationen und Übersicht
 * (todo: am cpp-Buch orientieren bzw. Verweise zum Weiterlesen)
 */
void zeichne_feld1();           // Zeichnen mit cout (printf siehe bausteine2)
void type_cast();               // ...
void random_test();             // Experimente mit Random und Einführung Schleifen
// while-Schleife: random bis Zahl gefunden
// todo https://en.cppreference.com/w/cpp/header/algorithm
void modulo();                  // Modulo
// todo: if
void modulo_rand();             // Experimente mit Modulo
void array1();                  // Was ist ein Array am Beispiel int[5]
void array_und_spielfeld1();    // Array als Spielfeld darstellen, mit Funktion void zeichne_feld1(int* feld), Array als Parameter übergeben (Schritt 1)
void hex1();                    // ...
void pointer1();                // Was ist ein Zeiger (= Pointer)?
void pointer_als_out_parameter(); // ...
void pointer_und_array1();      // ... siehe dort ...
// todo: Namensräume
// todo: stack overflow, "wie kann man das erreichen?"
void array_overflow();          // Experiment mit Overflow
// todo typ-Konvertierungen
void array2d();                 // Experimente mit 2d-Arrays

void ploymorph1();

/**
 * Hauptprogramm bausteine1
 */
int main() {
    //zeichne_feld1();
    //type_cast();
    //random_test();
    //modulo();
    //array1();
    //array_und_spielfeld1();
    //hex1();
    //pointer1();
    //pointer_als_out_parameter();
    //pointer_und_array1();
    //array_overflow();
    //array2d();
    ploymorph1();
    return 0;
}

void zeichne_feld1()
{
    //                       \n == new line == Neue Zeile
    std::cout << "   |   |   \n";
    std::cout << "-----------\n";
    std::cout << "   |   |   \n";
    std::cout << "-----------\n";
    std::cout << "   |   |   \n";

//     for (int a = 0; a < 10; a++) {
//         std::cout << "-";
//     }

    //                endl == end line == Neue Zeile
    std::cout << std::endl;
}

/**
 * siehe auch Datentypen:
 * https://de.wikibooks.org/wiki/C%2B%2B-Programmierung/_Einf%C3%BChrung_in_C%2B%2B/_Variablen,_Konstanten_und_ihre_Datentypen
 */
void type_cast()
{
    printf("type_cast...\n");

    { // scope
        int a = 2;
        int b = 3;
        int c = a / b;
        printf("%d / %d = %d\n", a, b, c);
    }

    double a = 2.0;
    double b = 3.0;
    double c = a / b;
    printf("%f / %f = %f\n", a, b, c);

    int d = (int)c; // cast
    printf("%d\n", d);

    int i1 = 2;
    int i2 = 3;
    c = ((double)i1) / -i2;
    printf("%.3f\n", c);

    printf("%.30f\n", (float)(2.0 / 3.0));
    printf("%.30f\n", 2.0 / 3.0);
    printf("%.30Lf\n", (long double)2.0 / 3.0);

    printf("%e\n", 2.0 / 3.0);

    printf("---%s---\n", "Welt");

//     for (int i = 0; i < 100; i++) {
//         printf("%10d\n", i);
//     }
}

/**
 * zeigt, dass man erst den Zufallsgenerator initalisieren muss
 */
void random_test()
{
    for (int i = 0; i < 10; i++)
    {
        printf("%d ", std::rand() % 10);
    }

    printf("\n");

    std::srand(std::time(nullptr));

    for (int i = 0; i < 10; i++)
    {
        printf("%d ", std::rand() % 10);
    }

    exit(1);
}

void modulo()
{
    for (int a = 0; a < 20; a++)
    {
        printf("%d %% %d = %d\n", a, 3, a % 3);
    }

    printf("\n");

    for (int a = 0; a < 20; a++)
    {
        //       v
        printf("%2d %% %d = %d\n", a, 3, a % 3);
    }
}

void modulo_rand()
{
    for (int a = 0; a < 20; a++) {
        int i = std::rand();
        printf("%d: %d\n", a, i % 3);
    }
}

void array1()
{
    //  0       1       2       3       4       5
    //  int     int     int     int     int     <out of bounds>
    //                                          wer will das sehen? -> array_overflow
    //
    int arr[5];

    //feld[0] = 1;
    //feld[1] = 20;
    //feld[2] = 300;
    //feld[3] = 4000;
    //feld[4] = 50000;
    //std::cout << feld[0] << std::endl;
    //std::cout << feld[1] << std::endl;
    //std::cout << feld[2] << std::endl;
    //std::cout << feld[3] << std::endl;
    //std::cout << feld[4] << std::endl;

    for (int i = 0; i < 5; i++) {
        printf("arr am Index %d = %d\n", i, arr[i]);
    }
    printf("\n");

    for (int i = 0; i < 5; i++) {
        arr[i] = 0;
    }

    for (int i = 0; i < 5; i++) {
        printf("arr[%d] = %d\n", i, arr[i]);
    }
}

/**
 * Warum "int*"?
 *  1. weil das bei Arrays so ist, oder
 *  2. siehe Pointer
 */
void zeichne_feld_arr(int* feld)
{
    printf(" %d | %d | %d\n", feld[0], feld[1], feld[2]);
    printf("-----------\n");
    printf(" %d | %d | %d\n", feld[3], feld[4], feld[5]);
    printf("-----------\n");
    printf(" %d | %d | %d\n", feld[6], feld[7], feld[8]);
}

// f = feld
// 2 = to = zu
// c = character = Zeichen
std::string f2c(int f)
{
    if (f < 10) {
        return std::to_string(f);
    } else if (f == 10) {
        return "o";
    } else if (f == 11) {
        return "x";
    }

    return "E"; // ERROR
}

void zeichne_feld_arr2(int* feld)
{
    printf(" %s | %s | %s\n", f2c(feld[0]).c_str(), f2c(feld[1]).c_str(), f2c(feld[2]).c_str());
    printf("-----------\n");
    printf(" %s | %s | %s\n", f2c(feld[3]).c_str(), f2c(feld[4]).c_str(), f2c(feld[5]).c_str());
    printf("-----------\n");
    printf(" %s | %s | %s\n", f2c(feld[6]).c_str(), f2c(feld[7]).c_str(), f2c(feld[8]).c_str());
}


void array_und_spielfeld1()
{
    //    ------> x
    // |  0  1  2
    // |  3  4  5
    // |  6  7  8
    // v

    //  1-9: nichts gesetzt = frei
    //  10: o
    //  11: x
    int feld[9];

    // Initialisieren
    for (int i = 0; i < 9; i++) {
        feld[i] = i + 1;
    }

    feld[2] = 10;
    feld[3] = 11;

    // Funktion aufrufen
    zeichne_feld_arr(feld);
    zeichne_feld_arr2(feld);
}

void hex1()
{
    for (int i = 0; i < 100; i++) {
        printf("%3d  %3o  %3x\n", i, i, i);
    }
}

void pointer1()
{
    // siehe auch https://www.peacesoftware.de/ckurs12.html
    // Video-Tutorial: https://de.wikipedia.org/wiki/Zeiger_(Informatik)

    int a = 10;
    int b = 20;
    int c = 30;

    // Variable     Adresse      Wert   Größe
    //              (Beispiel)          (Bytes)
    //              --------------------------------
    // a            604         10      4   XXXX
    // b            600         20      4   XXXX
    // c            596         30      4   XXXX

    printf("Größen:\n");
    //                   lu = long unsigned integer
    //
    printf("sizeof(a) = %lu\n", sizeof(a));  // 4   // Ein int belegt im Speicher 4 Bytes: XXXX
    printf("sizeof(b) = %lu\n", sizeof(b));  // 4
    printf("sizeof(c) = %lu\n", sizeof(c));  // 4

    printf("Adressen:\n");
    //                       p      The void * pointer argument is printed in hexadecimal
    //
    //
    printf("a = %d   Adresse: %p   Dezimal: %lu\n", a, (void*)&a, (long)&a);
    printf("b = %d   Adresse: %p   Dezimal: %lu\n", b, (void*)&b, (long)&b);
    printf("c = %d   Adresse: %p   Dezimal: %lu\n", c, (void*)&c, (long)&c);
    //      a = 10 Adresse: 0x7ffcb5f97b7c   Dezimal: 140723361512316
    //      b = 20 Adresse: 0x7ffcb5f97b78   Dezimal: 140723361512312
    //      c = 30 Adresse: 0x7ffcb5f97b74   Dezimal: 140723361512308
    // Die Adressen ändern sich bei jeder Ausführung.

    printf("Abstände:\n");
    //                      long decimal
    //
    printf("von a nach b = %ld\n", (long)&b - (long)&a);
    printf("von b nach c = %ld\n", (long)&c - (long)&b);

    // Die Variablen in einer Funktion werden auf dem Stack (Stapel) angelegt.
    // Der Speicher wird "von unten nach oben" belegt.

    // TODO: .............
    int* p = &a;
    printf("&a = p = %p\n", p);
    printf("*p = %d\n", *p); // Dereferenzierungsoperator
    a = 103;
    printf("*p = %d\n", *p);
    *p = 205;
    printf("a = %d\n", a);

    int* m = new int; *m = 50;
    int* n = new int; *n = 60;
    int* o = new int; *o = 70;
    double* p1 = new double; *p1 = 80.1;
    double* p2 = new double; *p2 = 90.1;
    printf("sizeof(m) = %lu\n", sizeof(m));  // 8
    printf("m         Adresse: %p   Dezimal: %lu\n", (void*)&m, (long)&m);      // die Pointer selber liegen auf dem Stack
    printf("n         Adresse: %p   Dezimal: %lu\n", (void*)&n, (long)&n);
    printf("o         Adresse: %p   Dezimal: %lu\n", (void*)&o, (long)&o);
    printf("p1        Adresse: %p   Dezimal: %lu\n", (void*)&p1, (long)&p1);
    printf("p2        Adresse: %p   Dezimal: %lu\n", (void*)&p2, (long)&p2);

    printf("*m = %d   Adresse: %p   Dezimal: %lu\n", *m, (void*)m, (long)m);    // Die mit new alloktieren Speicherbereiche liegen auf dem Heap
    printf("*n = %d   Adresse: %p   Dezimal: %lu\n", *n, (void*)n, (long)n);    // Abstand ist immer 32 Bytes, todo: warum? --> siehe aligment?
    printf("*o = %d   Adresse: %p   Dezimal: %lu\n", *o, (void*)o, (long)o);
    printf("*p1= %f   Adresse: %p   Dezimal: %lu\n", *p1, (void*)p1, (long)p1);
    printf("*p2= %f   Adresse: %p   Dezimal: %lu\n", *p2, (void*)p2, (long)p2);
    delete p2; delete p1; delete o; delete n; delete m;

    // aligment: z. B. https://stackoverflow.com/questions/29812534/c-heap-allocations-32-byte-aligned-by-default
    // Zeit und Speicher messen: https://codeforces.com/blog/entry/49371
    //      /usr/bin/time -v ./bausteine
    //      Maximum resident set size (kbytes)
    for (int i = 0; i < 10000; i++)
    {
        //printf("int a00%d;\n", i);
        //double* d = new double;
    }
    // https://softwareengineering.stackexchange.com/questions/310658/how-much-stack-usage-is-too-much
    //  ulimit -a
    //  Maximum stack size                                           (kB, -s) 8192
    //  (Warning: this routine is obsolete.  Use getrlimit(2), setrlimit(2), and sysconf(3) instead.)

    // https://www.quora.com/What-is-the-simplest-and-most-accurate-way-to-measure-the-memory-used-by-a-program-in-a-programming-contest-environment
    //
    // high address         ...
    //                      stack
    //                       |
    //                       v
    //
    //                       ^
    //                       |
    //                      heap
    //                      ...
    // low address          ...
}

int summe(int a, int b)
{
    int c = a + b;
    return c;
}

double div1(int a, int b)
{
    return (double)a / b;
}

void multi(int* summe, double* division, int a, int b)
{
    *summe = a + b;
    *division = (double)a / b;
}

void pointer_als_out_parameter()
{
    int s = summe(100, 3);
    double d = div1(100, 3);
    printf("summe() = %d\n", s);
    printf("div1()  = %f\n", d);

    multi(&s, &d, 100, 3);
    printf("s = %d, d = %f\n", s, d);
}

void pointer_und_array1()
{
    // Speicher: XXXX|XXXX|XXXX|XXXX|XXXX (5 x 4 Bytes direkt hintereinander)
    int arr[5];

    for (int i = 0; i < 5; i++) {
        arr[i] = (i + 1) * 100;
    }

    // Variable     Adresse      Wert   Größe
    //              (Beispiel)          (Bytes)
    //              --------------------------------
    // arr[0]==arr  10008        100        4   XXXX
    // arr[1]       10012        200        4   XXXX
    // arr[2]       10016        300        4   XXXX
    // arr[3]       10020        400        4   XXXX
    // arr[4]       10024        500        4   XXXX

    printf("arr    = %d   Adresse: %p   Dezimal: %lu\n",    *arr,   (void*)&arr,    (long)&arr);
    printf("arr[%d] = %d   Adresse: %p   Dezimal: %lu\n", 0, arr[0], (void*)&arr[0], (long)&arr[0]);
    printf("arr[%d] = %d   Adresse: %p   Dezimal: %lu\n",  1, arr[1], (void*)&arr[1], (long)&arr[1]);
    printf("arr[%d] = %d   Adresse: %p   Dezimal: %lu\n",  2, arr[2], (void*)&arr[2], (long)&arr[2]);
    printf("arr[%d] = %d   Adresse: %p   Dezimal: %lu\n",  3, arr[3], (void*)&arr[3], (long)&arr[3]);
    printf("arr[%d] = %d   Adresse: %p   Dezimal: %lu\n",  4, arr[4], (void*)&arr[4], (long)&arr[4]);

    int* p = arr;
    // Variable     Adresse      Wert
    //              (Beispiel)
    //              ------------------
    // ...
    // p            10000        10008       // p zeigt also auf die Adresse von arr[0]

    printf("p              Adresse: %p   Dezimal: %lu\n",  (void*)&p, (long)&p);
    printf("*p     = %d   Adresse: %p   Dezimal: %lu\n",  *p, (void*)p, (long)p);
    printf("p: %d, %d, %d, ...\n", p[0], p[1], p[2]);
    arr[0] = 7000;
    printf("p: %d, %d, %d, ...\n", p[0], p[1], p[2]);

}

void array_overflow()
{
    int arr[5];
    printf("Overflow...\n");
    int a = 5;
    while (true) {
        printf("a = %d...\n", a);
        arr[a] = 10;
        a++;
    }
}

void array2d()
{
    //                        arr[1][0]
    //                        v
    //  [ int int int int ] [ int int int int ] [ int int int int ]
    // => 3x ein Array mit 4 Elementen hintereinander
    int arr[3][4]; // 12 Elemente insgesamt

    printf("fill_with_numbers\n");
    for (int i = 0; i < 12; i++)
    {
        int* a = reinterpret_cast<int*>(&arr[0]);
        a[i] = i;
    }

    printf("Print v1:\n");
    for (int y = 0; y < 3; y++) {
        for (int x = 0; x < 4; x++) {
            printf("%2d ", arr[y][x]);
        }
        printf("\n");
    }

    printf("Print v2:\n");
    for (int i = 0; i < 12; i++)
    {
        int* a = reinterpret_cast<int*>(&arr[0]);
        printf("%2d ", a[i]);
    }
    printf("\n");
}

class A {
public:
    virtual void run()
    {
        std::cout << "A" << std::endl;
    }
};

class B : public A {
public:
    void run() override
    {
        std::cout << "B " << name << std::endl;
    }

    std::string name = "hallo";
};

void func1(A a)
{
    a.run();
}

void func2(A& a)
{
    a.run();
}

void ploymorph1()
{
    A a;
    B b;

    func1(a);
    func1(b);

    func2(a);
    func2(b);
}
