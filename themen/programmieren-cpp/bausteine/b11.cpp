#include <iostream>
#include <string>

using namespace std;

int mult(int a, int b)
{
    int ergebnis = 0;
    for (int i = 0; i < a; i++)
    {
        ergebnis = ergebnis + b;
    }

    return ergebnis;
    //return a * b;
}

//            5      3
int mult2(int a, int b)
{
    int ergebnis = 0;
    while (a > 0)
    {
        ergebnis += b;
        a--;
    }

    return ergebnis;
    //return a * b;
}


void rechnen()
{
    cout << "Was wollen Sie denn rechnen?\n";
    cout << "+ = Plus\n";
    cout << "- = Minus\n";
    cout << "* = Multiplizieren\n";
    cout << "/ = Dividieren\n";

    string op;
    cin >> op;

    int zahl1;
    int zahl2;
    cout << "Erste Zahl: ";
    cin >> zahl1;
    cout << "Zweite Zahl: ";
    cin >> zahl2;

    if (op == "+")
    {
        cout << zahl1 << " + " << zahl2 << " = " << zahl1 + zahl2 << endl;
    }
    else if (op == "*")
    {
        cout << zahl1 << " * " << zahl2 << " = " << mult(zahl1, zahl2) << endl;
    }
    else
    {
        cout << "Noch nicht fertig";
    }
}

int main()
{
    int ergebnis = mult(5, 3);
    cout << "ergebnis = " << ergebnis << endl;

    ergebnis = mult2(5, 3);
    cout << "ergebnis = " << ergebnis << endl;
    return 0;

    cout << "Hallo" << endl;
    cout << "Was wollen Sie tun?\n";
    cout << "a) nichts\n";
    cout << "b) Rechnen\n";

    string auswahl;

    cout << "Ihre Wahl: ";
    cin >> auswahl;

    if (auswahl == "a")
    {
        cout << "Ciao.";
    }
    else if (auswahl == "b")
    {
        rechnen();
    }
    else
    {
        cout << "Falsche Wahl";
    }

    return 0;
}
