git
===

<!-- toc -->

  * [Für Einsteiger](#fur-einsteiger)
  * [GUIs](#guis)
  * [FAQ](#faq)
- [Rebase only part of a branch](#rebase-only-part-of-a-branch)
- [How can I recover a lost commit in Git?](#how-can-i-recover-a-lost-commit-in-git)

<!-- tocstop -->

Für Einsteiger
--------------
* https://svij.org/blog/2014/10/25/git-fur-einsteiger-teil-1/

GUIs
----
* https://git-scm.com/
* Hier gibt es eine Liste von GUIs: https://git-scm.com/download/gui/linux
* git-cola

FAQ
---
# Rebase only part of a branch
* https://stackoverflow.com/questions/34926253/rebase-only-part-of-a-branch

    git rebase --onto basebranch commit1 branch1

Damit werden alle Commits *nach* commit1 bis zu branch1 auf den Branch basebranch aufgespielt.

# How can I recover a lost commit in Git?
* https://stackoverflow.com/questions/10099258/how-can-i-recover-a-lost-commit-in-git

    git reflog

und dann Cherry-Pick oder ähnliches.
