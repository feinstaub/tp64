#!/usr/bin/env python3
#
debug=True
debug=False

version=0.31
author="Ludger Humbert, Martin Reinertz, Christiane Borchel"
email="ponto@seminar.ham.nw.schule.de"
licence="released under the GPL Version 3.0"
license=licence
#
# ponto
# is an open source product and licensed under GPL
# GNU-Licence
#
# changelog:
# ----------
# Sa 27. Jun 23:12:37 CEST 2015
# ponto.py [0.31] LudgerHumbert
# - debug disabled
#
# Do 3. Apr 18:22:05 CEST 2014
# ponto.py [0.3] LudgerHumbert
# - migrating to Python 3.x
# - optimized a few constructs (if x == True => if x etal.)
#
# Thu Apr 09 06:54 CET 2006
# ponto.py [0.26] MartinReinertz
# - minor bug fix
#
# Thu Mar 29 04:55 CET 2006
# ponto.py [0.25] MartinReinertz
# - some bug fixes and improvements in preparation for lecture at
#   Eschenbach, Bavaria
#
# Mo Mar 29 00:26 CET 2005
# ponto.py [0.24] MartinReinertz
# - ponto now also features a page-class (Seite) - all pages have to be
#   retrieved directly from the corresponding document object - usage:
#	Document_object.gibSeite(number). If the desired page does not yet / no
#	longer exist, the method returns None. If a page ceases to exist AFTER it
#	has been created, the page object's attributes ALL become None. Pages ARE
#	NOT linked to sections, but the section currently displayed on, let's say,
#   Document_object.gibSeite(5) can always be retrieved.
# - the changes introduced in this release have made some major changes to the
#	English translation script necessary - the latter will be updated within the
#	upcoming week
# - sleep(10) function added to prevent error message when invoking OpenOffice
#	Writer - although it didn't work for me.
#
# Su Mar 13 20:55 CET 2005
# ponto.py [0.23] MartinReinertz
# - aliases TEXTDOKUMENT, Textdokument & Textmarke added
#
# Su Mar 13 20:30 CET 2005
# ponto.py [0.22] MartinReinertz
# - class name aliases set which comply with names used in school textbooks
#
# Wed Mar 09 23:14 CET 2005
# ponto.py [0.21] MartinReinertz
# - ponto can now SAVE OOWriter-Documents - usage:
#	Document_object.speichern(filename [optional if already saved])
# - minor bugfix in IMPORT method
#
# Sa Mar 06 01:57 CET 2005
# ponto.py [0.20] MartinReinertz
# - ponto can now IMPORT OOWriter-Documents which comply with the structure of
#	documents created via ponto - usage: Dokument1 = ponto.Dokument(filename)
#	note that the file must be located in the openoffice/program directory
# - added "Dokument.gibAbsatz(index)" method which makes direct access to
#	paragraphs possible (i.e. without reference to a Cursor object)
# - several minor bugs removed
# - few documentation strings modififed
#
# Mo Nov 29 20:55 CET 2004
# ponto.py [0.19] ChristianeBorchel
# - documentation strings modified.
#
# Mo Nov 23 18:30:02 CET 2004
# ponto.py [0.18] MartinReinertz
# - minor change regarding license, version & author information
#
# So Nov 22 01:36:17 CET 2004
# ponto.py [0.17] MartinReinertz
# - page styles (Seitenvorlagen) can now be assigned to sections (Abschnitte)
# - new sections (Abschnitte) need to be passed a page style (Seitenvorlage)
#	when instantiated
# - there now automatically is an initial section (Abschnitt) with page
#	style "None"
# - license, version & author information added to source code
#
# Sa Nov 21 11:46:09 CET 2004
# ponto.py [0.16]
# -- some minor improvements regarding comments
#
# ponto.py [0.15]
# -- no log entry
#
# Sa Nov 13 13:17:55 CET 2004
# ponto.py [0.14] MartinReinertz
# - first public available version published under GPL
#
# Do Nov 11 22:03:40 CET 2004, lh
# ponto.py [0.13] MartinReinertz
#
# Version vom 10.11.04 (Mittwoch)
# - ";" aus dem Sourcecode entfernt
# - neue Methode "gibZeichen" zu Absatz hinzugefügt
# - Farbbezeichnungen auf deutsche Variante beschränkt
#


# Detailänderungen wg. Mac OS X eingefügt
import locale
# Um nervende Fehlermeldungen beim Mac zu entfernen
try:
    locale.setlocale(locale.LC_ALL, '')
except locale.Error:
    pass # oh well

## die Pfade zu dem mit OpenOffice.org ausgeliefertem
## python:
#/usr/local/OpenOffice.org1.1.2/program/python
#
#/Applications/OpenOffice1.1.2/program/python

# wenn eine Doku erzeugt wird, bitte
# happydoc mit den Optionen --no_private_names --no_comments aufrufen

import sys
# Test:
if debug: print (sys.path)
#
# ggf. mit dem Modul platform herausfinden, auf welcher Basis gearbeitet wird?
# auch andere Varianten berücksichtigen: z.B. Android
# Mac OS X
sys.path.append("/Applications/OpenOffice1.1.2/program")
# Linux
sys.path.append("/usr/lib/libreoffice/program")

try:
    import readline
except ImportError:
    pass

else:
    import rlcompleter
    readline.parse_and_bind("tab: complete")

import uno
import unohelper

from com.sun.star.beans import PropertyValue
from unohelper import Base, systemPathToFileUrl, absolutize
from os import getcwd
from time import sleep

from com.sun.star.script import CannotConvertException

from com.sun.star.awt.FontWeight import BOLD
from com.sun.star.awt.FontWeight import NORMAL
FW_BOLD= BOLD
FW_NORMAL= NORMAL

from com.sun.star.awt.FontSlant import ITALIC
from com.sun.star.awt.FontSlant import NONE
FS_ITALIC= ITALIC
FS_NONE= NONE

from com.sun.star.awt.FontUnderline import SINGLE
from com.sun.star.awt.FontUnderline import NONE
FU_SINGLE= SINGLE
FU_NONE= NONE

from com.sun.star.text.ControlCharacter import PARAGRAPH_BREAK

from com.sun.star.style.ParagraphAdjust import LEFT
from com.sun.star.style.ParagraphAdjust import RIGHT
from com.sun.star.style.ParagraphAdjust import CENTER
from com.sun.star.style.ParagraphAdjust import BLOCK

#Deklaration von Ausrichtungen:
Linksbuendig= LEFT
Rechtsbuendig= RIGHT
Zentriert= CENTER
Blocksatz= BLOCK

from com.sun.star.style.BreakType import NONE
from com.sun.star.style.BreakType import PAGE_BEFORE

from com.sun.star.style.LineSpacingMode import FIX
from com.sun.star.style import LineSpacing

#Deklaration von Farben:
Rot= 16711680
Schwarz= 0
Weiss= 16777215
Gelb= 16776960
Blau= 255
Gruen= 26112
Braun= 669728
Lila= 6684876

#für den Schrifthintergrund:
Transparent= -1


#---Klasse Cursor --------------------------------------------------------------
class Cursor:
#Klassenattribute:
  dokument= "" #Referenz zum zugehörigen Dokumentobjekt
  komponente= "" #Verweis auf die zum Dokument gehörige Komponente

  def __init__(self, dokument, komponente):
    self.dokument= dokument
    self.komponente= komponente

  def gibAktuellePosition(self):
    """interne Methode"""
    ###internal method###
    return(self.komponente.CurrentController.getViewCursor())

  def gibViewCursor(self):
    """interne Methode"""
    ###internal method###
    return(self.komponente.CurrentController.getViewCursor())

  def gibPageCursor(self):
    """interne Methode"""
    ###internal method###
    return(self.komponente.CurrentController.getPageCursor())


  def gibAktuellePositionInAbsatzliste(self):
    """interne Methode"""
    ###internal method###
    absaetze= self.dokument.gibAbsaetze()
    if absaetze!=[]:
      absatzgefunden= False
      for moeglicher_absatz in absaetze:
        index= 0
        for moegliche_position in moeglicher_absatz[1]:
          if moegliche_position==1:
            absatzgefunden= True
            break
          index= index + 1
        if absatzgefunden:
          break
      #Wenn kein Absatz mit Cursor gefunden wurde, steht der Cursor vor dem
      #ersten Absatz. Dies wird mit Rückgabe index=-1 und
      #moeglicher_absatz=absaetze[0] quittiert
      if not absatzgefunden:
        moeglicher_absatz= absaetze[0]
        index= -1
    else:
      #wenn gar kein Absatz im Dokument existiert
      moeglicher_absatz= None
      index= -1

    #Danach sind folgende Werte wichtig:
    #a enthält das Absatzobjekt des Absatzes, in dem der Cursor steht
    #-------------------
    #index enthält:
    #-1 = gar kein Absatz im Dokument vorhanden ODER Cursor steht VOR dem
    #	ersten Absatz
    #index >= 0 = Cursor steht NACH  diesem Zeichen in -->
    #--> Absatz a

    #Diese beiden Werte noch zurückgeben:
    return([moeglicher_absatz,index])


  def geheVorZeichenInAbsatz(self, absatz, zeichen):
    """interne Methode"""
    ###internal method###
    absaetze= self.dokument.gibAbsaetze()

    index_von_absatz= -1
    moeglicher_index= 0
    for a in absaetze:
      if a[0]==absatz:
        index_von_absatz= moeglicher_index
      moeglicher_index= moeglicher_index+1

    #Finden der aktuellen Cursorposition in Absatzliste:
    [absatz_mit_cursor, cursor_index]= self.gibAktuellePositionInAbsatzliste()

    #Finden des Indexes von absatz_mit_cursor in Absatzliste:
    moeglicher_index= 0
    index_von_absatz_mit_cursor= -1
    for a in absaetze:
      if a[0]==absatz_mit_cursor[0]:
        index_von_absatz_mit_cursor= moeglicher_index
      moeglicher_index= moeglicher_index+1

    #Bewegen des Cursors vor das übergebene Zeichen in übergebenem Absatz:
    if index_von_absatz_mit_cursor>index_von_absatz:
      while absaetze[index_von_absatz][1][absaetze[index_von_absatz][0].gibZeichenobjektliste().index(zeichen)]!=1:
        self.zurueck()
    elif index_von_absatz_mit_cursor<index_von_absatz:
      while absaetze[index_von_absatz][1][absaetze[index_von_absatz][0].gibZeichenobjektliste().index(zeichen)]!=1:
        self.vor()
    elif cursor_index>absaetze[index_von_absatz][0].gibZeichenobjektliste().index(zeichen):
      while absaetze[index_von_absatz][1][absaetze[index_von_absatz][0].gibZeichenobjektliste().index(zeichen)]!=1:
        self.zurueck()
    elif cursor_index<absaetze[index_von_absatz][0].gibZeichenobjektliste().index(zeichen):
      while absaetze[index_von_absatz][1][absaetze[index_von_absatz][0].gibZeichenobjektliste().index(zeichen)]!=1:
        self.vor()
    #elif cursor_index==absaetze[index_von_absatz][0].gibZeichenobjektliste().index(zeichen):
      #self.vor()
    #Einen Schritt zurück braucht's dann noch:
    self.zurueck()


  def gibZeichen(self):
    """Liefert Referenz auf nächstes Zeichenobjekt rechts des Cursors"""
    ###returns a pointer to the character instance next to the cursor###
    [absatz, cursor_index]= self.gibAktuellePositionInAbsatzliste()
    #Rückgabe des NÄCHSTEN Zeichenobjekts nur möglich, wenn der Cursor nicht
    #am Ende des Dokuments steht:
    if cursor_index+1 < len(absatz[0].gibZeichenobjektliste()):
      return(absatz[0].gibZeichenobjektliste()[cursor_index+1])
    elif self.dokument.gibAbsaetze().index(absatz) < len(self.dokument.gibAbsaetze())-1:
      return(self.dokument.gibAbsaetze()[self.dokument.gibAbsaetze().index(absatz)+1][0].gibZeichenobjektliste()[0])
    else:
      return(None)


  def vor(self):
    """setzt den Cursor ein Zeichen nach rechts, das Zeichen wird dabei NICHT
       markiert"""
    ###moves the cursor one character to the right (forward).###
    absaetze= self.dokument.gibAbsaetze()
    self.komponente.CurrentController.getViewCursor().goRight(1,False)

    #Finden der bisherigen Cursorposition in Absatzliste:
    [absatz_mit_cursor, index]= self.gibAktuellePositionInAbsatzliste()

    #Nur etwas tun, wenn Cursor nicht am Ende des letzten Absatzes steht:
    if absatz_mit_cursor[0].gibNachfolger() or index < len(absatz_mit_cursor[1])-1:

      #Löschen der alten Cursorposition:
      absatz_mit_cursor[1][index]= 0

      #Setzen der neuen Cursorposition in Absatzliste:
      if index < len(absatz_mit_cursor[1])-1:
        absatz_mit_cursor[1][index+1]= 1
      else:
        absatz_mit_cursor[0].gibNachfolger()[1][0]= 1

      return(True)

    else:
      return(False)


  def zurueck(self):
    """setzt den Cursor ein Zeichen nach links (zurueck), das Zeichen wird
       dabei NICHT markiert"""
    ###moves the cursor one character to the left (back)###
    absaetze= self.dokument.gibAbsaetze()
    self.komponente.CurrentController.getViewCursor().goLeft(1,False)

    #Finden der bisherigen Cursorposition in Absatzliste:
    [absatz_mit_cursor, index]= self.gibAktuellePositionInAbsatzliste()

    #Nur etwas tun, wenn Cursor NICHT vor dem allerersten Absatz
    #(bzw. im leeren Dokument) steht
    if index != -1:

      #Löschen der alten Cursorposition:
      absatz_mit_cursor[1][index]= 0

      #Setzen der neuen Cursorposition in Absatzliste:
      if index > 0:
        absatz_mit_cursor[1][index-1]= 1
      elif absatz_mit_cursor[0].gibVorgaenger():
        absatz_mit_cursor[0].gibVorgaenger()[1][len(absatz_mit_cursor[0].gibVorgaenger()[1])-1]= 1
        #Setzen der CursorPosition im Vorgänger-Absatz
        #Andernfalls stehen jetzt ALLE Pos. auf 0                                                                                                    #(d.h. wenn Cursor VOR erstem Absatz)
      return(True)

    else:
      return(False)


  def markiereVor(self):
    """interne Methode"""
    ###internal method###
    absaetze= self.dokument.gibAbsaetze()
    self.komponente.CurrentController.getViewCursor().goRight(1,True)

    #Finden der bisherigen Cursorposition in Absatzliste:
    [absatz_mit_cursor, index]= self.gibAktuellePositionInAbsatzliste()

    #Nur etwas tun, wenn Cursor nicht am Ende des letzten Absatzes steht:
    if absatz_mit_cursor[0].gibNachfolger() or index < len(absatz_mit_cursor[1])-1:

      #Löschen der alten Cursorposition:
      absatz_mit_cursor[1][index]= 0

      #Setzen der neuen Cursorposition in Absatzliste:
      if index < len(absatz_mit_cursor[1])-1:
        absatz_mit_cursor[1][index+1]= 1
      else:
        absatz_mit_cursor[0].gibNachfolger()[1][0]= 1
    return


  def markierungAufheben(self):
    """interne Methode"""
    ###internal method###
    self.komponente.CurrentController.getViewCursor().collapseToEnd()


  def markiereZurueck(self):
    """interne Methode"""
    ###internal method###
    absaetze=self.dokument.gibAbsaetze()
    self.komponente.CurrentController.getViewCursor().goLeft(1,True)

    #Finden der bisherigen Cursorposition in Absatzliste:
    [absatz_mit_cursor, index]= self.gibAktuellePositionInAbsatzliste()

    #Nur etwas tun, wenn Cursor NICHT vor dem allerersten Absatz (bzw. im leeren Dokument) steht
    if index != -1:

      #Löschen der alten Cursorposition:
      absatz_mit_cursor[1][index]= 0

      #Setzen der neuen Cursorposition in Absatzliste:
      if index > 0:
        absatz_mit_cursor[1][index-1]= 1
      elif absatz_mit_cursor[0].gibVorgaenger():
        absatz_mit_cursor[0].gibVorgaenger()[1][len(absatz_mit_cursor[0].gibVorgaenger()[1])-1]= 1
        #Setzen d. CPos im Vorgaenger-Absatz
        #Andernfalls stehen jetzt ALLE Pos. auf 0                                                                                                    #(d.h. wenn Cursor VOR erstem Absatz)
    return


  def drehe_links(self):
    """ohne Kommentar"""
    ###jokes should not be commented on###
    print ("Seh' ich aus wie ein dreieckiger Roboter?")
    return


#---Klasse Dokument ------------------------------------------------------------
class Dokument:
  def __init__(self, dateiname=""):
    """neues oder gespeichertes Writerdokument (mit Namen "dateiname") öffnen"""
    if dateiname=="":
      #neues Writerdokument öffnen
      #"self.komponente" ist die neue, für dieses Dokument zuständige Komponente
      localContext= uno.getComponentContext()
      resolver= localContext.ServiceManager.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", localContext )
      smgr= resolver.resolve("uno:socket,host=localhost,port=2002;urp;StarOffice.ServiceManager")
      remoteContext= smgr.getPropertyValue("DefaultContext")
      desktop= smgr.createInstanceWithContext( "com.sun.star.frame.Desktop",remoteContext)

      #Hier(!!!):
      self.komponente= desktop.loadComponentFromURL( "private:factory/swriter","_blank", 0,())
      self.xTextInterface= self.komponente.Text

      #Und hier wird nun ein Cursorobjekt ins Leben gerufen und an das
      #Dokument-Objekt durch Parameterübergabe gebunden:
      self.cursor= Cursor(self, self.komponente)

      #self.absaetze wird leer initialisiert:
      self.absaetze= []

      #Vielleicht später mal:
      #Leere Seite und Seitenliste des Dokuments werden angelegt.
      #Abschnitt= None
      #Seitenvorlage= None # lässt sich nicht ändern
      #Seitenzahl= 1
      #self.Seiten= [Seite(None, None, 1)]
      #Vorläufig erstmal:
      self.Seiten= []

      #Attribut 'dateiname' setzen:
      self.dateiname= dateiname
      return

    else:
      #gespeichertes Writerdokument mit Namen "dateiname" öffnen
      #"self.komponente" ist die neue, für dieses Dokument
      #zuständige Komponente
      localContext= uno.getComponentContext()
      resolver= localContext.ServiceManager.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", localContext )
      smgr= resolver.resolve("uno:socket,host=localhost,port=2002;urp;StarOffice.ServiceManager")
      remoteContext= smgr.getPropertyValue("DefaultContext")
      desktop= smgr.createInstanceWithContext( "com.sun.star.frame.Desktop",remoteContext)

      #Bestehendes Dokument laden:
      cwd= systemPathToFileUrl( getcwd() )
      url_datei= absolutize( cwd, systemPathToFileUrl(dateiname) )
      self.komponente=desktop.loadComponentFromURL( url_datei, "_blank", 0, () )
      self.xTextInterface= self.komponente.Text

      #Und hier wird nun ein Cursorobjekt ins Leben gerufen und an das
      #Dokument-Objekt durch Parameterübergabe gebunden:
      self.cursor= Cursor(self, self.komponente)

      #Mit diesem geht's dann zunächst direkt an den Anfang des Dokuments:
      self.cursor.gibAktuellePosition().jumpToFirstPage()
      self.cursor.gibAktuellePosition().jumpToStartOfPage()

      #self.absaetze wird leer initialisiert:
      self.absaetze= []

      #-----
      #Analysiere und erstelle Struktur (dafür wird zweitweise ein weiteres
      #OpenOffcie Writer-Dokument geöffnet:
      self_kopie= Dokument()

      #Extraktion der Absätze im gespeicherten Dokument:
      self_kopie_textinhalt= self.xTextInterface.getString()
      self_kopie_stringabsaetze= self_kopie_textinhalt.split("\n")

      #Einfügen der Absätze in neu geöffnetes Writer-Dokument:
      for stringabsatz in self_kopie_stringabsaetze:
        if stringabsatz!="":
          self_kopie.erzeugeAbsatz(stringabsatz)

      #Setzen des Cursors auf den Anfang des neuen Dokuments:
      self_kopie.gibCursor().geheVorZeichenInAbsatz(self_kopie.gibAbsaetze()[0][0], self_kopie.gibAbsaetze()[0][0].gibZeichenobjektliste()[0])

      #Analyse der Absätze + Zeichen und Aufbau der Ponto-Sturktur:
      letzter_pagestylename= self.cursor.gibAktuellePosition().PageStyleName
      for absatz in self_kopie.gibAbsaetze():
        #Setzen der Absatzattribute:
        absatz[0].setzeAusrichtung(self.cursor.gibAktuellePosition().ParaAdjust)
        absatz[0].setzeEinzugLinks(self.cursor.gibAktuellePosition().ParaLeftMargin)
        absatz[0].setzeEinzugRechts(self.cursor.gibAktuellePosition().ParaRightMargin)
        absatz[0].setzeEinzugErstzeile(self.cursor.gibAktuellePosition().ParaFirstLineIndent)
        #absatz[0].setzeEinzugRestzeilen(self.cursor.gibAktuellePosition().ParaFirstLineIndent)# <-- Identisch mit setzeEinzugLinks()
        absatz[0].setzeAbstandOben(self.cursor.gibAktuellePosition().ParaTopMargin)
        absatz[0].setzeAbstandUnten(self.cursor.gibAktuellePosition().ParaBottomMargin)
        #absatz[0].setzeZeilenabstand(self.cursor.gibAktuellePosition().ParaFirstLineSpacing) <-- Muss noch bearbeitet werden (wie auch setzeZeilenabstand() selbst)

	#Erzeugen und Setzen eines neuen Abschnitts inkl. ebenfalls erzeugter
	#Seitenvorlage, falls angebracht:
    #print self.cursor.gibAktuellePosition().PageStyleName
    if self.cursor.gibAktuellePosition().PageStyleName != letzter_pagestylename:
      #Gesetzt werden muss der "DisplayName" des PageStyles.
      #Deswegen ist das hier alles so umständlich:
      pageStyles= self.komponente.StyleFamilies.getByName("PageStyles")
      pageStyle= pageStyles.getByName(self.cursor.gibAktuellePosition().PageStyleName)
      self_kopie.erzeugeAbschnitt(Seitenvorlage(self_kopie, pageStyle.DisplayName))

      letzter_pagestylename = pageStyle.DisplayName

      #Durchgehen der einzelnen Zeichen des jeweiligen Absatzes und Setzen
      #aller Attributwerte. Im gespeicherten Dokument MUSS der Cursor direkt über
      #die viewCursor-Funktionen gesteuert werden, die
      #Cursor.gibAktuellePositon() zur Verfügung stellt:
      for zeichen in absatz[0].gibZeichenobjektliste():
        self.cursor.gibAktuellePosition().goRight(1,True)

        zeichen.setzeSchriftfarbe(self.cursor.gibAktuellePosition().CharColor)
        zeichen.setzeHintergrundfarbe(self.cursor.gibAktuellePosition().CharBackColor)
        zeichen.setzeSchriftart(self.cursor.gibAktuellePosition().CharFontName)

        zeichen.setzeFett(self.cursor.gibAktuellePosition().CharWeight==FW_BOLD)
        zeichen.setzeKursiv(self.cursor.gibAktuellePosition().CharPosture==FS_ITALIC)
        zeichen.setzeUnterstrichen(self.cursor.gibAktuellePosition().CharUnderline==FU_SINGLE)

        zeichen.setzeDurchgestrichen(self.cursor.gibAktuellePosition().CharCrossedOut)
        zeichen.setzeSchriftgroesse(self.cursor.gibAktuellePosition().CharHeight)
        self.cursor.gibAktuellePosition().collapseToEnd()
        self_kopie.gibCursor().vor()

      #Schliessen des kopierten Dokuments:
      self_kopie.gibKomponente().dispose()

      #Jetzt werden noch die Verweise "umgebogen":
      self.absaetze= self_kopie.absaetze[:]

      self_kopie.komponente= self.komponente
      self_kopie.xTextInterface= self.xTextInterface
      self_kopie.cursor= self.cursor

      #Zum Schluss noch schnell an den Anfang des Dokuments:
      self.gibCursor().geheVorZeichenInAbsatz(self.gibAbsaetze()[0][0], self.gibAbsaetze()[0][0].gibZeichenobjektliste()[0])

      #Und Attributwert für 'dateiname' setzen:
      self.dateiname= dateiname
      return


  def speichern(self, dateiname=""):
    """Speichert das aktuelle Dokument unter dem Dateinamen dateiname ab.
    Handelt es sich um ein geladenes Dokument, muss kein neuer Dateiname
    angegeben werden."""
    if dateiname=="" and not self.komponente.hasLocation():
      print ("""Bitte geben Sie beim Aufruf von Dokument.save() einen
      Dateinamen an, wenn Ihr Dokument noch nie gespeichert wurde!""")
      return
    elif dateiname=="" and self.komponente.hasLocation():
      self.komponente.store()
      return
    else:
      #Pfad zum OpenOffice/program Verzeichnis zu Dateiname hinzufuegen
      cwd= systemPathToFileUrl( getcwd() )
      url_datei= absolutize( cwd, systemPathToFileUrl(dateiname) )
      self.komponente.storeAsURL(url_datei, ())

	#Und schliesslich wird noch das Attribut 'dateiname' entsprechend
	#dem neuen Namen gesetzt:
    self.dateiname= dateiname
    return


  def erzeugeAbschnitt(self, seitenvorlage):
    """Diese Methode legt einen neuen Abschnitt NACH dem aktuellen Absatz an
       (mit Seitenumbruch)"""
    ###constructs a new section after the paragraph the cursor is currently in.###

    #Feststellen, zu welchem Abschnitt der aktuelle Absatz bisher gehörte.
    #cursor_index verweist auf das Zeichen, NACH DEM der Cursor steht - daher
    #das kann es sein, dass der Cursor in Wahrheit genau am Anfang des NÄCHSTEN
    #Absatzes steht:
    [erster_absatz, cursor_index]= self.cursor.gibAktuellePositionInAbsatzliste()
    if not erster_absatz:
      print("\nFEHLER: Dies wuerde einen LEEREN Abschnitt erzeugen,")
      print("was in OpenOffice unmöglich ist!\n")
      return(None)
    elif cursor_index == len(erster_absatz[0].gibTextinhalt())-1:
      #Der folgende Nachfolger kann auch None sein, wenn der Cursor NACH dem
      #letzten Absatz am Anfang der neuen Zeile steht. Dies muss gleich
      #berücksichtigt werden, da ein leerer Abschnitt entstehen würde.
      if not erster_absatz[0].gibNachfolger():
        print("\nFEHLER: Dies würde einen LEEREN Abschnitt erzeugen,")
        print("was in OpenOffice unmöglich ist!\n")
        return(None)
      else:
        erster_absatz= erster_absatz[0].gibNachfolger()

    #Setzen der neuen Zugehörigkeit für ALLE Absätze, die zum selben
    #alten Abschnitt gehörten + Zwischenspeicherung dieser Absätze in einer
    #Liste (absaetze_fuer_neuen_abschnitt):
    absaetze_fuer_neuen_abschnitt= []

    #Angleichungen für Abschnittzuordnungen der Absätze vornehmen:
    alterabschnitt= erster_absatz[0].gibAbschnitt()
    naechster_absatz_im_alten_abschnitt= erster_absatz
    while naechster_absatz_im_alten_abschnitt:
      if naechster_absatz_im_alten_abschnitt[0].gibAbschnitt()==alterabschnitt:
        absaetze_fuer_neuen_abschnitt.append(naechster_absatz_im_alten_abschnitt[0])
        naechster_absatz_im_alten_abschnitt= naechster_absatz_im_alten_abschnitt[0].gibNachfolger()
      else:
        #Im aktuellen (alten) Abschnitt befinden sich keine weiteren Absätze:
        naechster_absatz_im_alten_abschnitt =None

    #Erzeugen des neuen Abschnitts (seitenvorlage, absatzliste, Verweis auf Dokument, Abschnitt vorm ertsen Absatz einfuegen?)
    neuerabschnitt= Abschnitt(seitenvorlage, absaetze_für_neuen_abschnitt, self, False)

    return(neuerabschnitt)


  def erzeugeAbsatz(self, text):
    """Diese Methode legt einen neuen Absatz an."""
    ###constructs a new paragraph###

    #Leere Absätze werden nicht angelegt:
    if text=="":
      print ("Leere Absätze werden nicht angelegt/ignoriert!")
      return(None)

    #Feststellen, ob Absatz mit gleichem Text bereits existiert.
    #Falls ja, bekommt der neue Absatz eine ID!=0 (ID=Nr. der Wiederholungen
    #bei Erstellung des Absatzes)
    absatzid= 0
    for a in self.absaetze:
      if a[0].gibTextinhalt()==text:
        absatzid= a[0].gibID()
        absatzid= absatzid + 1
    #Dieses Vorgehen ist etwas undurchsichtig - im wesentl. geschieht folgendes:
    #Wenn ein identischer Absatz gefunden wurde, wird dessen ID ausgelesen.
    #Diese ID+1 ist dann potentiell die ID des neuen (identischen) Absatzes.
    #Sollte jedoch noch ein identischer Absatz gefunden werden, wird
    #der soeben berechnete Wert von absatzid wieder überschrieben.

    #Neuen Absatz erstellen - zunächst wird der Text von möglichen
    #Zeilenumbrüchen ('\n') bereinigt.
    #Finales '\n" wird angehängt, nachher in den Text geschrieben wird jedoch
    #ein Control-Character (PARAGRAPH_BREAK)
    text_retter= ""
    for zeichen in text:
      if zeichen!='\n':
        text_retter= text_retter+zeichen
    text= text_retter+'\n'

    #Nun wird die Position des neuen Absatzes bestimmt
    #(und der Cursor entsprechend bewegt):

    #Finden der aktuellen Cursorplatzierung in Absatzliste, Zwischenspeichern
    #des Absatzobjektes und der Position des Cursors IN diesem Absatzobjekt
    #(zur Bedeutung der Werte von "index" siehe
    #Cursor.gibAktuellePositionInAbsatzListe()
    [absatz_mit_cursor, cursor_index]= self.cursor.gibAktuellePositionInAbsatzliste()

    #Weiter unten erstelltes Absatzobjekt an Stelle INDEX+1 (s.o.) in Liste der
    #Absätze (Elemente: [Absatz,Cursorplatzierung] einfuegen. Der Cursor steht
    #nach Erzeugung eines neuen Absatzes ja zwangsweise NACH diesem Absatz.

    #Es gibt  verschiedene Ausgangssituationen:

    #Variante 1 - Cursor steht inmitten eines Absatzes
    #Reaktion: Bewegen des Cursors ans Ende des Absatzes, danach Ausführen der
    #Aktionen für Variante 2
    while absatz_mit_cursor and cursor_index < len(absatz_mit_cursor[1])-1 and cursor_index !=-1:
      self.cursor.vor()
      [absatz_mit_cursor, cursor_index]= self.cursor.gibAktuellePositionInAbsatzliste()

    #Zwischendurch (also nachdem die Position gefunden wurde) wird der neue
    #Absatz als Objekt angelegt...:
    neuerabsatz= Absatz(self, text, False)

    #...und die alte Cursorposition in der Absatzliste(!) gelöscht:
    if absatz_mit_cursor and cursor_index!=-1:
      absatz_mit_cursor[1][cursor_index]= 0

    #Variante 2 - Cursor steht ganz am Ende eines Absatzes:
    #	len(absatz_mit_cursor[1])-1
    #(d.h.  direkt vor dem ersten Zeichen eines anderen Absatzes
    #einfaches Einfügen genügt, sofern der Cursor nicht vor dem ersten
    #	Zeichen des ersten Absatzes steht.
    #Steht er doch dort, wird genauso verfahren, als existiere noch kein Absatz
    #	im Dokument (Einfügen an Pos. 0)
    if absatz_mit_cursor and cursor_index!=-1:
      self.absaetze[self.absaetze.index(absatz_mit_cursor)+1:self.absaetze.index(absatz_mit_cursor)+1]= [[neuerabsatz,[0]*(len(text)-1)+[1]]]
    else:
      self.absaetze[0:0]= [[neuerabsatz,[0]*(len(text)-1)+[1]]]

    #Schliesslich wird noch der Abschnitt des neuen Absatzes gesetzt,
    #und der Absatz der Absatzliste des Abschnitts hinzugefuegt:
    if neuerabsatz.gibNachfolger():
      neuerabsatz.setzeAbschnitt(neuerabsatz.gibNachfolger()[0].gibAbschnitt())
      index_von_nachfolger_absatz=neuerabsatz.gibNachfolger()[0].gibAbschnitt().gibAbsaetze().index(neuerabsatz.gibNachfolger()[0])
      neuerabsatz.gibNachfolger()[0].gibAbschnitt().fuegeEinInAbsaetze(index_von_nachfolger_absatz,neuerabsatz)
    elif neuerabsatz.gibVorgaenger():
      neuerabsatz.setzeAbschnitt(neuerabsatz.gibVorgaenger()[0].gibAbschnitt())
      index_von_vorgaenger_absatz=neuerabsatz.gibVorgaenger()[0].gibAbschnitt().gibAbsaetze().index(neuerabsatz.gibVorgaenger()[0])
      neuerabsatz.gibVorgaenger()[0].gibAbschnitt().fuegeEinInAbsaetze(index_von_vorgaenger_absatz+1,neuerabsatz)
    else:
      #Nun kann es sich bei neuerabsatz nur noch um den allerersten (und
      #	einzigen) Absatz handeln.
      #Dieser benötigt (für die Seitenvorlage, d.h. damit auch hier eine
      #gesetzt werden kann) auch zwingend einen Abschnitt
      #inkl. der Seitenvorlage, die an der aktuellen Cursorposition (=ganz am
      #	Anfang) eingestellt ist. Diese kann aber NICHT
      #in PageDescName eingetragen werden.
      pageStyles= self.komponente.StyleFamilies.getByName("PageStyles")
      pageStyle= pageStyles.getByName(self.cursor.gibAktuellePosition().PageStyleName)

      abschnitt0= Abschnitt(Seitenvorlage(self,pageStyle.DisplayName), [neuerabsatz], self, True)
      #neuerabsatz.setzeAbschnitt(abschnitt0)

    #Referenz auf den soeben eingefügten Absatz zurückgeben
    return(neuerabsatz)


  def erzeugeSeitenvorlage(self, name):
    """Erzeugt ein Seitenvorlage-Objekt (muss Teil der Standard-Seitenvorlagen
       in OOWriter sein). Der Vorlagenname aus OpenOffice wird als Zeichenkette
       (String) übergeben."""
    return(Seitenvorlage(self,name))

  #Vielleicht später mal:
  #def setzeSeiten(self, seitenliste):
    """interne Methode"""
    ###internal method###
    #self.Seiten= seitenliste
    #return


  def gibKomponente(self):
    """interne Methode"""
    ###internal method###
    return(self.komponente)


  def gibXTextInterface(self):
    """interne Methode"""
    ###internal method###
    return(self.xTextInterface)


  def gibAbsaetze(self):
    """interne Methode"""
    ###internal method###
    return(self.absaetze)

  def gibAbschnitt(self, index):
    """Liefert das Abschnitt-Objekt Nr. [index]"""
    ###Returns section object no. [index]###
    abschnitte= []
    for absatz in self.absaetze:
      if absatz[0].gibAbschnitt() not in abschnitte:
        abschnitte.append(absatz[0].gibAbschnitt())
    if index < len(abschnitte):
      return(abschnitte[index])
    else:
      return(None)

  def gibAbsatz(self, index):
    """Liefert das Absatz-Objekt Nr. [index]"""
    ###Returns paragraph object no. [index]###
    if index < len(self.absaetze):
      return(self.absaetze[index][0])
    else:
      return(None)

  def gibSeiten(self):
    """Liefert eine Liste mit den Seitenobjekten des Dokuments"""
    ###Returns a list with all page objects in the document###
    return(self.Seiten)

  def gibSeite(self, nr):
    """Liefert das Seitenobjekt zu Seite Nr. (nr.)"""
    ###Returns the page object for page no. (nr.)###
    for s in self.Seiten:
      if s.gibSeitenzahl()==nr:
        return(s)
        break

      #Aktuelle Position des Cursors merken:
      [absatz,index]= self.cursor.gibAktuellePositionInAbsatzliste()

      #Überprüfen, ob Seite überhaupt da ist
      aktuelle_seitenzahl= self.cursor.gibAktuellePosition().getPage()
      while aktuelle_seitenzahl < nr and not self.cursor.vor():
        aktuelle_seitenzahl= self.cursor.gibAktuellePosition().getPage()
      while aktuelle_seitenzahl > nr and not self.cursor.zurueck():
        aktuelle_seitenzahl= self.cursor.gibAktuellePosition().getPage()

    if aktuelle_seitenzahl==nr:
      seite= Seite(self, nr)
      self.Seiten.append(seite)
    else:
      seite= None

    #Und den Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      self.cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      self.cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      self.cursor.vor()

    return(seite)


  def gibCursor(self):
    """Liefert das zum Dokument gehoerige Cursor-Objekt"""
    ###returns the cursor object belonging to a document###
    return(self.cursor)


# Klasse Abschnitt -------------------------------------------------------------
class Abschnitt:
  def __init__(self, seitenvorlage, absatzliste, dokument, istvorerstemabsatz):
    """Interner Konstruktor. Nicht von Hand aufrufen."""

    #absatzliste[0] enthält den ersten Absatz des zukünftigen neuen
    #	Abschnittes
    #Zunächst wird geprüft, ob der Abschnitt angelegt werden soll, wenn gerade
    #	der erste Absatz eingefügt wird. Wenn dies nämlich so ist, muss der
    #	Abschnitt erstellt werden, OBWOHl der Absatz weder Vorgänger noch
    #	Nachfolger hat.
    if not istvorerstemabsatz:
      if not absatzliste[0].gibVorgaenger():
        print("\nFEHLER: Dies würde einen LEEREN Abschnitt erzeugen,")
        print("was in OpenOffice unmöglich ist!\n")
        return(None)
      elif absatzliste[0].gibAbschnitt()!=absatzliste[0].gibVorgaenger()[0].gibAbschnitt():
        print("\nFEHLER: Dies würde einen LEEREN Abschnitt erzuegen,")
        print("was in OpenOffice unmöglich ist!\n")
        return(None)
      elif dokument.gibAbsaetze() [len(dokument.gibAbsaetze())-1] [1] [len(dokument.gibAbsaetze()[len(dokument.gibAbsaetze())-1][1])-1]==1:
        dokument.gibCursor().zurueck()

    self.dokument= dokument
    self.komponente= self.dokument.gibKomponente()
    self.absaetze= absatzliste[:]
    self.seitenvorlage= seitenvorlage

    #Vielleicht mal später:
    #self.Seiten= seitenliste
    #for seiten in self.Seiten:
      #setzeAbschnitt(self)

    #Setzen der Abschnittzugehoerigkeiten der betroffenen Absatzobjekte
    #und Entfernen derselben aus bisherigen Abschnittslisten:
    for a in self.absaetze:
      ehemals_zugehoeriger_abschnitt= a.gibAbschnitt()

      if ehemals_zugehoeriger_abschnitt:
        ehemalige_absatzliste= ehemals_zugehoeriger_abschnitt.gibAbsaetze()[:]
        del ehemalige_absatzliste[ehemalige_absatzliste.index(a)]
        ehemals_zugehoeriger_abschnitt.setzeAbsaetze(ehemalige_absatzliste)

      #In jedem Fall muss aber der Abschnitt neu gesetzt werden.
      a.setzeAbschnitt(self)

    self.cursor= self.dokument.gibCursor()
    #self.cursor.gibAktuellePosition().BreakType= PAGE_BEFORE
    #Die Seitenvorlage darf u.U. NICHT in PageDescName
    #gespeichert werden, wenn dieser Abschnitt naemlich der 0te Abschnitt im
    #Dokument ist und automatisch beim Erzeugen des allerersten Absatzes
    #angelegt wird.
    if self.absaetze[0] != self.dokument.gibAbsaetze()[0][0]:
      self.cursor.gibAktuellePosition().PageDescName= self.seitenvorlage.gibName()

    #Vielleicht später mal:
    #Nun sehen wir uns an, ob sich durch die Erzeugung des Abschnitts
    #die Seitenzahl des Dokuments erhöht hat. Falls ja, müssen
    #entsprechende "Umbauarbeiten" vorgenommen werden:
    #dokument_seitenzahl= self.dokument.gibKomponente().PageCount
    #if len(self.dokument.gibSeiten()) < dokument_seitenzahl:
      #dokument_seiten= self.dokument.gibSeiten()[:]e Seitenzahl

      #neue_seite= Seite( self.dokument.gibSeiten()[dokument_seiten.index(len(self.Seiten))].gibAbschnitt(), self.dokument.gibSeiten()[dokument_seiten.index(len(self.Seiten))].gibSeitenvorlage(), self.dokument.gibSeiten()[dokument_seiten.index(len(self.Seiten))].gibSeitenzahl())
      #dokument_seiten[dokument_seiten.index(self.Seiten[len(self.Seiten)-1]):dokument_seiten.index(self.Seiten[len(self.Seiten)-1])]=neue_seite
      #self.Seiten.append(neue_seite)

      #Seitenzahlen in Gesamtliste wieder in Ordnung bringen:
      #for seite in dokument_seiten:
        #seite.setzeSeitenzahl(dokument_seiten.index(seite)+1)

      #Und aktualisierte Seitenliste an Dokument übergeben:
      #self.dokument.setzeSeiten(dokument_seiten)


  def erzeugeAbsatz(self, text):
    """Diese Methode legt einen neuen Absatz direkt in einem vorher
       deklarierten Abschnitt an. Alle Absätze, die so ins Leben gerufen werden,
       erscheinen sequentiell, d.h. im Abschnitt werden sie der Reihe nach
       eingefügt. Ist eine andere Position gewünscht, kann dies unter Verwendung
       von Cursorbewegungen erreicht werden."""
    ###constructs a new paragraph in a certain section. No cursor position is required.###

    #Cursor an die richtige Position im Abschnitt bewegen (HINTER das letzte
    #Zeichen im aktuellen Abschnitt, d.h. VOR dem ersten Zeichen in dem
    #neuen Abschnitt)
    letzter_absatz_in_abschnitt= self.absaetze[len(self.absaetze)-1]
    letztes_zeichen_in_absatz=letzter_absatz_in_abschnitt.gibZeichenobjektliste()[len(letzter_absatz_in_abschnitt.gibZeichenobjektliste())-1]
    self.cursor.geheVorZeichenInAbsatz(letzter_absatz_in_abschnitt, letztes_zeichen_in_absatz)

    #Falls noch ein nachfolgender Abschnitt existiert, wird zunächst
    #der einleitende Seitenumbruch (d.h. die zugeordnete Seitenvorlage) gelöscht:
    self.cursor.vor()
    if letzter_absatz_in_abschnitt.gibNachfolger():
      self.cursor.gibAktuellePosition().PageDescName= ""

    #Der neue Absatz wird angelegt:
    neuer_absatz_im_abschnitt= self.dokument.erzeugeAbsatz(text)
    #Flugs wird der neue Absatz in die Absatzeliste des Abschnitts eingefügt,
    #sowie das Abschnittsattribut des Absatzes geändert
    neuer_absatz_im_abschnitt.setzeAbschnitt(self)
    self.absaetze.append(neuer_absatz_im_abschnitt)

    #Update der Werte von oben (letzter_absatz_in_abschnitt hat sich geändert,
    #wenn eingefügter Absatz nun aktuell der letzte im Abschnitt ist):
    letzter_absatz_in_abschnitt= self.absaetze[len(self.absaetze)-1]
    letztes_zeichen_in_absatz= letzter_absatz_in_abschnitt.gibZeichenobjektliste()[len(letzter_absatz_in_abschnitt.gibZeichenobjektliste())-1]
    #Seitenumbruch wird ggfs. wieder AN RICHTIGE STELLE gesetzt:
    if letzter_absatz_in_abschnitt.gibNachfolger():
      self.cursor.geheVorZeichenInAbsatz(letzter_absatz_in_abschnitt, letztes_zeichen_in_absatz)
      self.cursor.vor()
      self.cursor.gibAktuellePosition().PageDescName=self.seitenvorlage.gibName()

    return(neuer_absatz_im_abschnitt)


  def setzeSeitenvorlage(self, seitenvorlage):
    """Weist einem Abschnitt eine Seitenvorlage zu"""
    ###Assigns a pagestyle to a section###

    if self.absaetze[0] == self.dokument.gibAbsaetze()[0][0]:
      print ("""Die Seitenvorlage des ersten Abschnittes kann in OpenOffice
	      Writer nicht verändert werden!""")
      return

    cursor= self.dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    #Cursor in den ersten Absatz des Abschnitts setzen...
    cursor.geheVorZeichenInAbsatz(self.absaetze[0], self.absaetze[0].zeichenobjektliste[len(self.absaetze[0].zeichenobjektliste)//2])
    #...und dann die Seitenvorlage des Abschnitts setzen
    self.cursor.gibAktuellePosition().PageDescName=seitenvorlage.gibName()

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()

    #Zum Schluss wird noch der Attributwert gesetzt:
    self.seitenvorlage= seitenvorlage


  def setzeAbsaetze(self, absatzliste):
    """interne Methode"""
    ###internal method###
    self.absaetze= absatzliste
    return


  def setzeSeiten(self, seitenliste):
    """interne Methode"""
    ###internal method###
    self.Seiten= seitenliste
    return


  def fuegeEinInAbsaetze(self, index, absatz):
    """interne Methode"""
    ###internal method###
    self.absaetze[index:index]= [absatz]
    return


  def gibDokument(self):
    """liefert das zum Abschnitt gehörige Dokument"""
    ###returns the document belonging to a section###
    return(self.dokument)

  def gibKomponente(self):
    """interne Methode"""
    ###internal method###
    return(self.komponente)

  def gibSeitenvorlage(self):
    """Liefert die zu einem Abschnitt gehörige Seitenvorlage."""
    ###Returns a section's pagestyle.###
    return(self.seitenvorlage)

  def gibAbsaetze(self):
    """liefert die Liste der im Abschnitt enthaltenen Absätze"""
    ###returns the list of paragraphs the section contains###
    return(self.absaetze)

  def gibSeiten(self):
    """Liefert die Liste der Seiten, über die sich der Abschnitt erstreckt"""
    ###Returns a list of all pages the section extends over###
    return(self.Seiten)


#---Klasse Absatz --------------------------------------------------------------
class Absatz:
  def __init__(self, dokument, text, anchorabsatz):
    """Interner Konstruktor. Nicht von Hand aufrufen."""

    #Initialisierung der Attributwerte:
    #self.zeichenanzahl= len(text)
    #self.ausrichtung= dokument.gibCursor().gibAktuellePosition().ParaAdjust
    #print (dokument.gibCursor().gibAktuellePosition().ParaAdjust)
    #self.einzuglinks= dokument.gibCursor().gibAktuellePosition().ParaLeftMargin
    #self.einzugrechts= dokument.gibCursor().gibAktuellePosition().ParaRightMargin
    #self.einzugerstzeile= dokument.gibCursor().gibAktuellePosition().ParaFirstLineIndent
    #self.einzugrestzeilen= self.einzuglinks
    #self.abstandoben= dokument.gibCursor().gibAktuellePosition().ParaTopMargin
    #self.abstandunten= dokument.gibCursor().gibAktuellePosition().ParaBottomMargin

    self.zeichenanzahl= len(text)

    self.ausrichtung= Linksbuendig
    dokument.gibCursor().gibAktuellePosition().ParaAdjust= Linksbuendig

    self.einzuglinks= 0
    dokument.gibCursor().gibAktuellePosition().ParaLeftMargin= 0

    self.einzugrechts= 0
    dokument.gibCursor().gibAktuellePosition().ParaRightMargin= 0

    self.einzugerstzeile= 0
    dokument.gibCursor().gibAktuellePosition().ParaFirstLineIndent= 0
    self.einzugrestzeilen= self.einzuglinks

    self.abstandoben= 0
    dokument.gibCursor().gibAktuellePosition().ParaTopMargin= 0

    self.abstandunten= 0
    dokument.gibCursor().gibAktuellePosition().ParaBottomMargin= 0

    #self.zeilenabstand= 1
    self.zeichenobjektliste= []
    self.abschnitt= None

    #Speicherung des zugehoerigen Dokuments in Attribut
    self.dokument= dokument
    self.xTextInterface= dokument.gibXTextInterface()


    #Dann wird der an den Konstruktuor übergebene Text (in Parameter text) in
    #die globale Instanzvariable "Textinhalt" kopiert (ohne finales "\n")
    self.textinhalt= ""
    for z in text:
      if z!='\n':
        self.textinhalt= self.textinhalt + z

    #Schliesslich wird noch eine Liste mit Zeichenobjekten (enstprehchend der
    #Zeichen im String text (d.h. inkl. finalem "\n") angelegt - deswegen
    #zunächst neu initialisiert:
    self.zeichenobjektliste= []
    for z in text:
      self.zeichenobjektliste.append(Zeichen(self.dokument, self, z))

    #Anzeigen des Textes in OpenOffice + Finales PARAGRAPH_BREAK:
    #insertString benötigt unser XText-Interface und eine XTextRange
    #(geliefert durch Cursor.gibAktuellePosition):
    #Aufruf: insertString(anAktuellerCursorPosition, String, 0=danach nicht
    #markiert)
    aktuelleposition= dokument.cursor.gibAktuellePosition()
    self.xTextInterface.insertString(dokument.cursor.gibAktuellePosition(), self.textinhalt, 0)

    #Einfügen des ControlCharacters in den Text:
    self.xTextInterface.insertControlCharacter(dokument.cursor.gibAktuellePosition(), PARAGRAPH_BREAK, 0 )

    #Und wieder Anhängen des finalen "\n" an self.Textinhalt:
    self.textinhalt= self.textinhalt + "\n"


  def gibTextinhalt(self):
    """Liefert Text des Absatzes als String"""
    ###returns the textstring of a paragraph###
    return(self.textinhalt)


  def gibZeichenanzahl(self):
    """Liefert die Anzahl der Zeichen des Absatzes"""
    ###returns the number of characters of a paragraph###
    return(self.zeichenanzahl)


  def gibAbschnitt(self):
    """Liefert den Abschnitt, zu dem der Absatz gehört"""
    ###returns the section belonging to a paragraph###
    return(self.abschnitt)


  def gibBereich(self):
    """Liefert den Bereich, zu dem der Absatz gehört"""
    ###returns the section belonging to a paragraph (alternative class)###
    return(self.bereich)


  def gibAusrichtung(self):
    """Liefert den Attributwert von 'ausrichtung'"""
    ###returns the value of the attribute 'ausrichtung'###
    return(self.ausrichtung)


  def gibEinzugLinks(self):
    """Liefert den Attributwert von 'einzugLinks'"""
    ###returns the value of the attribute 'einzugLinks'###
    #ACHTUNG: Dieser Wert ist stets identisch mit self.einzugrestzeilen -
    #wird jedoch im bayerischen Konzept zusätzlich aufgeführt
    return(self.einzuglinks)


  def gibEinzugRechts(self):
    """Liefert den Attributwert von 'einzugRechts'"""
    ###returns the value of the attribute 'einzugRechts'###
    return(self.einzugrechts)


  def gibEinzugErstzeile(self):
    """Liefert den Attributwert von 'einzugErstzeile'.
       In englischsprachigen Textdokumenten wird die erste Zeile von
       Absätzen haeufig weiter eingerueckt als die uebrigen Zeilen,
       die Restzeilen."""
    ###returns the value of the attribute 'einzugErstzeile'###
    return(self.einzugerstzeile)


  def gibEinzugRestzeilen(self):
    """Liefert den Attributwert von 'einzugErstzeile'. In
       englischsprachigen Textdokumenten wird die erste Zeile von Absätzen
       häufig weiter eingerückt als die übrigen Zeilen, die Restzeilen."""
    ###returns the value of the attribute 'einzugErstzeile'###

    #ACHTUNG: Dieser Wert ist stets identisch mit self.einzuglinks - wird
    #jedoch im bayerischen Konzept zusätzlich aufgeführt
    return(self.einzugrestzeilen)


  def gibAbstandOben(self):
    """Liefert den Attributwert von 'abstandOben'"""
    ###returns the value of the attribute 'abstandOben'###
    return(self.abstandoben)


  def gibAbstandUnten(self):
    """Liefert den Attributwert von 'abstandUnten'"""
    ###returns the value of the attribute 'abstandUnten'###
    return(self.abstandunten)


  def gibDokument(self):
    """Liefert Dokument-Objekt, zu dem der Absatz gehört"""
    ###returns the document that the paragraph belongs to###
    return(self.dokument)


  def gibID(self):
    """Liefert ID des Absatzes (wichtig für Absätze mit identischem Text.
       Die später erzeugten Absätze erhalten höhere ID-Nummern)"""
    ###returns the ID of a paragraph. Relevant if several paragraphs of a
    ###document contain the same string.###
    return(self.absatzid)


  def gibNachfolger(self):
    """Liefert Referenz auf den Nachfolger eines Absatzes (sofern vorhanden);
       ansonsten None"""
    ###returns a pointer to the successor of a paragraph. If there is no
    ###successor: None will be returned###
    absaetze= self.dokument.gibAbsaetze()

    nachfolger= None
    for aktueller_absatz in absaetze:
        if aktueller_absatz[0]==self:
          nachfolger= aktueller_absatz
        elif nachfolger:
          nachfolger= aktueller_absatz
          break

    if nachfolger[0]==self:
      nachfolger= None

    return(nachfolger)


  def gibVorgaenger(self):
    """Liefert Referenz auf den Vorgänger eines Absatzes (sofern vorhanden),
       ansonsten None"""
    ###returns a pointer to the predecessor of a paragraph. If there is no
    ###predecessor, None will be returned###
    absaetze= self.dokument.gibAbsaetze()
    vorgaenger= None
    for moeglicher_absatz in absaetze:
      if moeglicher_absatz[0]==self:
        break
      else:
        vorgaenger= moeglicher_absatz
    return(vorgaenger)


  def gibZeichenobjektliste(self):
    """Liefert Referenz auf die Zeichenobjektliste. In der Zeichenobjektliste
       sind alle Objekte der Klasse Zeichen, die zu dem Absatz gehören,
       gespeichert. Die Methode kann auch für die direkte Manipulation von
       Zeichen verwendet werden.
       Bsp: 'absatz1.gibZeichenobjektliste()[5].setzeFett()' """
    ###returns a pointer to the list of characters belonging to a paragraph.
    ###With this method you can directly access character objects and use
    ###their methods afterwards.
    ###Example: 'absatz1.gibZeichenobjektliste()[5].setzeFett()'###
    return(self.zeichenobjektliste)


  def gibZeichen(self, index):
    """Diese Methode liefert das Zeichenobjekt eines Absatzes an Position
       'index' (angefangen bei 0)"""
    ###This method returns the character object of a paragraph at
    ###position 'index' (starting at 0)###
    if debug: print (index)
    if index < len(self.zeichenobjektliste):
      return(self.zeichenobjektliste[index])
    else:
      return(None)


  def setzeTextinhalt(self, text):
    """Interne Methode"""
    ###Internal method###
    self.textinhalt= text


  def setzeAbschnitt(self, abschnitt):
    """Interne Methode"""
    ###Internal method###
    self.abschnitt= abschnitt


  def setzeBereich(self, bereich):
    """Interne Methode"""
    ###Internal method###
    self.bereich= bereich


  def setzeAusrichtung(self, ausrichtung):
    """mit dieser Methode kannst du die Ausrichtung eines Absatzes verändern.
    Der Wert des Attributs 'ausrichtung' kann so auf die Werte Linksbuendig,
    Rechtsbuendig, Blocksatz oder Zentriert gesetzt werden. """
    ###changes the alignment of a paragraph###

    #Aktuelle Position des Cursors merken:
    cursor= self.dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    #Cursor vor erstes Zeichen im Absatz setzen...
    cursor.geheVorZeichenInAbsatz(self, self.zeichenobjektliste[len(self.zeichenobjektliste)//2])
    #...und dann die Ausrichtung des Absatzes setzen:
    cursor.gibAktuellePosition().ParaAdjust=ausrichtung

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()

    #Zum Schluss noch Setzen des eigentlichen Attributwerts:
    self.ausrichtung= ausrichtung
    return


  def setzeEinzugLinks(self, einzug):
    """Mit dieser Methode lässt sich der linksseitige Einzug eines Absatzes setzen. Angabe in (mm/100)"""
    ###sets the left-sided indentation to a certain value (in mm/100).###

    #ACHTUNG: Diese Methode ist IDENTISCH mit Absatz.setzeEinzugRestzeilen - sie wird jedoch im bayerischen Konzept zusaetzlich aufgefuehrt!

    #Aktuelle Position des Cursors merken:
    cursor= self.dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    #Cursor vor das erste Zeichen im Absatz setzen...
    cursor.geheVorZeichenInAbsatz(self, self.zeichenobjektliste[len(self.zeichenobjektliste)//2])
    #...und dann die Ausrichtung des Absatzes setzen:
    cursor.gibAktuellePosition().ParaLeftMargin=einzug

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()

    #Zum Schluss: Setzen des eigentlichen Attributwerts:
    self.einzuglinks= einzug
    self.einzugrestzeilen= einzug
    return


  def setzeEinzugRechts(self, einzug):
    """Mit dieser Methode laesst sich der rechtsseitige Einzug eines Absatzes setzen. Angabe in (mm/100)"""
    ###sets the right-sided indentation to a certain value (in mm/100).###

    #Aktuelle Position des Cursors merken:
    cursor= self.dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    #Cursor vor das erste Zeichen im Absatz setzen...
    cursor.geheVorZeichenInAbsatz(self, self.zeichenobjektliste[len(self.zeichenobjektliste)//2])
    #...und dann die Ausrichtung des Absatzes setzen:
    cursor.gibAktuellePosition().ParaRightMargin= einzug

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()

    #Zum Schluss noch setzen des eigentlichen Attributs:
    self.einzugrechts= einzug
    return


  def setzeEinzugErstzeile(self, einzug):
    """Mit dieser Methode lässt sich der linksseitige Einzug der ersten Zeile
       eines Absatzes setzen. Angabe in (mm/100). In englischsprachigen
       Textdokumenten wird die erste Zeile eines Absatzes häufig weiter
       eingerückt, als die übrigen Zeilen (Restzeilen)."""
    ###sets the indentation of the first line to a certain value (in mm/100).###

    #Aktuelle Position des Cursors merken:
    cursor= self.dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    #Cursor vor erstes Zeichen im Absatz setzen...
    cursor.geheVorZeichenInAbsatz(self, self.zeichenobjektliste[len(self.zeichenobjektliste)//2])
    #...und dann die Ausrichtung des Absatzes setzen:
    cursor.gibAktuellePosition().ParaFirstLineIndent=einzug

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()

    #Zum Schluss noch Setzen des Attributwerts:
    self.einzugerstzeile= einzug
    return


  def setzeEinzugRestzeilen(self, einzug):
    """Mit dieser Methode lässt sich der linksseitige Einzug eines Absatzes
       setzen. Angabe in (mm/100). In englischsprachigen Textdokumenten wird
       die erste Zeile eines Absatzes häufig weiter eingerückt als die übrigen
       Zeilen (Restzeilen). Diese Methode bewirkt das gleiche wie
       setzeEinzugLinks(). Sie erhöht lediglich die Verständlichkeit der
       Programme, wenn man auch die Methode setzeEinzugErstzeile() verwendet."""
    ###sets the indentation of all lines of a paragraph except the first one to a certain value (in mm/100).###

    #ACHTUNG: Diese Methode ist IDENTISCH mit Absatz.setzeEinzugLinks - sie wird jedoch im bayerischen Konzept zusaetzlich aufgefuehrt!

    #Aktuelle Position des Cursors merken:
    cursor= self.dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    #Cursor vor das erste Zeichen im Absatz setzen...
    cursor.geheVorZeichenInAbsatz(self, self.zeichenobjektliste[len(self.zeichenobjektliste)//2])
    #...und dann die Ausrichtung des Absatzes setzen:
    cursor.gibAktuellePosition().ParaLeftMargin= einzug

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()

    #Zum Schluss noch Setzen des Attributwerts:
    self.einzugrestzeilen= einzug
    self.einzugLinks= einzug
    return


  def setzeAbstandOben(self, abstand):
    """Hiermit kannst du den Abstand oberhalb eines Absatzes setzen. Angabe in (mm/100)."""
    ###sets the spacing above a paragraph to a certain value (in mm/100).###

    #Aktuelle Position des Cursors merken:
    cursor=self.dokument.gibCursor()
    [absatz,index] = cursor.gibAktuellePositionInAbsatzliste()

    #Cursor vor das erste Zeichen im Absatz setzen...
    cursor.geheVorZeichenInAbsatz(self, self.zeichenobjektliste[len(self.zeichenobjektliste)//2])
    #...und dann die Ausrichtung des Absatzes setzen:
    cursor.gibAktuellePosition().ParaTopMargin= abstand

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()

    #Zum Schluss noch Setzen des Attributwerts:
    self.abstandoben= abstand
    return


  def setzeAbstandUnten(self, abstand):
    """Hiermit kannst du den Abstand unterhalb eines Absatzes setzen. Angabe in (mm/100)."""
    ###sets the spacing under a paragraph to a certain value (in mm/100).###

    #Aktuelle Position des Cursors merken:
    cursor= self.dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    #Cursor vor erstes Zeichen im Absatz setzen...
    cursor.geheVorZeichenInAbsatz(self, self.zeichenobjektliste[len(self.zeichenobjektliste)//2])
    #...und dann die Ausrichtung des Absatzes setzen:
    cursor.gibAktuellePosition().ParaBottomMargin=abstand

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()

    #Zum Schluss noch Setzen des Attributwerts:
    self.abstandunten= abstand
    return


  def setzeZeilenabstand(self, zeilenabstand):
    """Hiermit kannst du den Wert des Attributs Zeilenabstand ändern.
       Gewünschte Werte bitte in mm angeben. Zur Zeit funktioniert diese
       Methode nicht."""
    ###sets the linespacing to a certain value (in mm). Currently non-functional.###
    #MUSS NOCH BEARBEITET WERDEN!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    #Aktuelle Position des Cursors merken:
    cursor= self.dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    #Cursor vor das erste Zeichen im Absatz setzen...
    cursor.geheVorZeichenInAbsatz(self, self.zeichenobjektliste[len(self.zeichenobjektliste)//2])

    #...und dann die Ausrichtung des Absatzes setzen:
    linespacing_instanz= LineSpacing()
    linespacing_instanz.Mode= FIX
    linespacing_instanz.Height= zeilenabstand

    cursor.gibAktuellePosition().ParaLineSpacing= linespacing_instanz

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()

    #Zum Schluss noch Setzen des Attributwerts:
    self.zeilenabstand= zeilenabstand
    return


#class Zeichen -----------------------------------------------------------------
class Zeichen:
  def __init__(self, dokument, absatz, symbol):
    """Interner Konstruktor. Nicht von Hand aufrufen."""

    self.Symbol= symbol
    self.Dokument= dokument
    self.Absatz= absatz

    #Zeichenattribute (werden teils je nach aktueller Auswahl, teils auf Standardwerte gesetzt)
    cursor= self.Dokument.gibCursor()

    #self.Fett:
    self.Fett= False
    cursor.gibAktuellePosition().CharWeight= FW_NORMAL

    #self.Kursiv:
    self.Kursiv= False
    cursor.gibAktuellePosition().CharPosture= FS_NONE

    #self.Unterstrichen:
    self.Unterstrichen= False
    cursor.gibAktuellePosition().CharUnderline= FU_NONE

    #self.Durchgestrichen:
    self.Durchgestrichen= False
    cursor.gibAktuellePosition().CharCrossedOut= False

    #self.Schriftart (wird aus aktueller Einstellung übernommen):
    self.Schriftart= cursor.gibAktuellePosition().CharFontName
    #self.Schriftgroesse (wird aus aktueller Einstellung übernommen):
    self.Schriftgroesse= cursor.gibAktuellePosition().CharHeight

    #self.Schriftfarbe:
    self.Schriftfarbe= Schwarz
    cursor.gibAktuellePosition().CharColor= Schwarz
    #self.Hintergrundfarbe:
    self.Hintergrundfarbe= Weiss
    cursor.gibAktuellePosition().CharBackColor= Transparent

 #set-Methoden:

  def setzeSymbol(self, symbol):
    """Mit dieser Methode kannst du ein einzelnes Zeichen (bzw. das angezeigte
    Symbol in OpenOffice!) gegen ein anderes austauschen"""
    ###With this method you can the symbol which represents this character object in OpenOffice.###

    #Aktuelle Position des Cursors merken:
    cursor= self.Dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    cursor.geheVorZeichenInAbsatz(self.Absatz, self)
    cursor.markiereVor()
    self.Dokument.gibXTextInterface().insertString(self.Dokument.gibCursor().gibAktuellePosition(), symbol, True)
    self.Absatz.setzeTextinhalt(self.Absatz.gibTextinhalt()[0:self.Absatz.gibZeichenobjektliste().index(self)] + symbol + self.Absatz.gibTextinhalt()[self.Absatz.gibZeichenobjektliste().index(self)+1:])
    cursor.markierungAufheben()
    cursor.zurueck()
    self.Symbol= symbol

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()


  def setzeSchriftfarbe(self, r, *g, **b):
    """Mit dieser Methode kannst Du die Schriftfarbe eines Zeichens einstellen.
       Als Parameter müssen der Methode drei ganze Zahlen zwischen 0 und 255
       übergeben werden. Die erste Zahl steht für die Farbe Rot, die zweite
       Zahl für die Farbe Gruen und die dritte Zahl für die Farbe Blau.
       Aus diesen drei Farben kannst Du Dir die übrigen Farben zusammenmischen.
       Je höher eine der drei Zahlen ist, desto stärker ist der entsprechende
       Farbton in der Schriftfarbe enthalten.
       Schwarz ist z.B. (0,0,0) (Abwesenheit aller Farben) und
       weiss (255, 255, 255) (Anwesenheit aller Farben).
       Alternativ kannst Du statt der drei Zahlen auch einen vordefinierten
       Farbwert einstellen: es gibt Rot, Gruen, Blau, Schwarz, Weiss, Gelb,
       Lila. Aber Achtung! Vordefinierte Farbwerte müssen vor ihrer
       Verwendung importiert werden, z.B. mit 'from ponto import Blau'."""
    ###With this method you can change the fontcolor of a character.###

    # auch wenn es nicht sehr hübsch ist: es funktioniert
    if g!=():
      f= (int (r) & 255) << 16 | (int (g[0]) & 255) << 8 | (int (g[1]) & 255)
    else:
      f=r

    #Aktuelle Position des Cursors merken:
    cursor= self.Dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    cursor.geheVorZeichenInAbsatz(self.Absatz, self)
    cursor.markiereVor()
    cursor.gibAktuellePosition().CharColor=f
    cursor.markierungAufheben()
    cursor.zurueck()
    self.Schriftfarbe= f

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()


  def setzeHintergrundfarbe(self, r, *g, **b):
    """Hiermit kannst Du die Hintergrundfarbe eines Zeichens einstellen.
       Als Parameter müssen der Methode drei ganze Zahlen zwischen 0 und 255
       übergeben werden. Die erste Zahl steht für die Farbe Rot, die zweite
       Zahl für die Farbe Gruen und die dritte Zahl für die Farbe Blau.
       Aus diesen drei Farben kannst Du Dir die übrigen Farben zusammenmischen.
       Je höher eine der drei Zahlen ist, desto stärker ist der entsprechende
       Farbton in der Schriftfarbe enthalten.
       Schwarz ist z.B. (0,0,0) (Abwesenheit aller Farben) und
       weiss (255, 255, 255) (Anwesenheit aller Farben).
       Alternativ kannst Du statt der drei Zahlen auch einen vordefinierten
       Farbwert einstellen: es gibt Rot, Gruen, Blau, Schwarz, Weiss, Gelb,
       Lila. Aber Achtung! Vordefinierte Farbwerte müssen vor ihrer
       Verwendung importiert werden, z.B. mit 'from ponto import Blau'."""
    ###With this method you can change the backgroundcolor of a character.###

    if g!=():
      f= (int (r) & 255) << 16 | (int (g[0]) & 255) << 8 | (int (g[1]) & 255)
    else:
      f=r

    #Aktuelle Position des Cursors merken:
    cursor= self.Dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    cursor=self.Dokument.gibCursor()
    cursor.geheVorZeichenInAbsatz(self.Absatz, self)
    cursor.markiereVor()
    cursor.gibAktuellePosition().CharBackColor= f
    cursor.markierungAufheben()
    cursor.zurueck()
    self.Hintergrundfarbe= f

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()


  def setzeSchriftart(self, schriftart):
    """Mit dieser Methode kannst du die Schriftart eines  Zeichens einstellen.
    Der Methode muss beim Aufruf genau ein Parameter übergeben werden
    der Name der gewünschten Schriftart als Zeichenkette (String)."""
    ###With this method you can change font type of a character.###

    #Aktuelle Position des Cursors merken:
    cursor= self.Dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    cursor= self.Dokument.gibCursor()
    cursor.geheVorZeichenInAbsatz(self.Absatz, self)
    cursor.markiereVor()
    cursor.gibAktuellePosition().CharFontName=schriftart
    cursor.markierungAufheben()
    cursor.zurueck()
    self.Schriftart= schriftart

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()


  def setzeFett(self, w):
    """Mit dieser Methode kannst Du einstellen, ob das Zeichen fett dargestellt
       werden soll oder nicht. Der Methode muss beim Aufruf ein Wahrheitswert
       als Parameter übergeben werden. In Ponto gibt es zwei Wahrheitswerte:
       True und False. Möchtest Du also, dass ein Zeichen fett dargestellt wird,
       rufst Du setzeFett() mit dem Wahrheitswert True auf, willst Du, dass es
       normal dargestellt wird, mit dem Wahrheitswert False."""
    ###With this method you can determine, whether a character is printed bold.###

    #Aktuelle Position des Cursors merken:
    cursor= self.Dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    cursor= self.Dokument.gibCursor()
    cursor.geheVorZeichenInAbsatz(self.Absatz, self)
    cursor.markiereVor()
    if w:
      cursor.gibAktuellePosition().CharWeight= FW_BOLD
    else:
      cursor.gibAktuellePosition().CharWeight= FW_NORMAL
    cursor.markierungAufheben()
    cursor.zurueck()
    self.Fett= w

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()


  def setzeKursiv(self, w):
    """Mit dieser Methode kannst Du einstellen, ob das Zeichen kursiv
       dargestellt werden soll oder nicht. Der Methode muss beim Aufruf ein
       Wahrheitswert als Parameter übergeben werden. In Ponto gibt es zwei
       Wahrheitswerte: True und False. Möchtest Du also, dass ein Zeichen
       kursiv dargestellt wird, rufst Du setzeKursiv() mit dem Wahrheitswert
       True auf, willst Du, dass es normal dargestellt wird, mit dem
       Wahrheitswert False."""
    ###With this method you can determine, whether a character is printed in italics.###

    #Aktuelle Position des Cursors merken:
    cursor= self.Dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    cursor= self.Dokument.gibCursor()
    cursor.geheVorZeichenInAbsatz(self.Absatz, self)
    cursor.markiereVor()
    if w:
      cursor.gibAktuellePosition().CharPosture= FS_ITALIC
    else:
      cursor.gibAktuellePosition().CharPosture= FS_NONE
    cursor.markierungAufheben()
    cursor.zurueck()
    self.Kursiv= w

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()


  def setzeUnterstrichen(self, w):
    """Mit dieser Methode kannst Du einstellen, ob das Zeichen unterstrichen
       werden soll, oder ob es ohne Unterstreichung dargestellt werden soll.
       Der Methode muss beim Aufruf ein Wahrheitswert als Parameter übergeben
       werden. In Ponto gibt es die zwei Wahrheitswerte True und False.
       Möchtest Du also, dass ein Zeichen unterstrichen dargestellt wird,
       rufst Du setzeUnterstrichen() mit dem Wahrheitswert True auf, willst
       Du, dass es normal dargestellt wird, mit dem Wahrheitswert False."""
    ###With this method you can determine, whether a character is underlined.###

    #Aktuelle Position des Cursors merken:
    cursor= self.Dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    cursor= self.Dokument.gibCursor()
    cursor.geheVorZeichenInAbsatz(self.Absatz, self)
    cursor.markiereVor()
    if w:
      cursor.gibAktuellePosition().CharUnderline= FU_SINGLE
    else:
      cursor.gibAktuellePosition().CharUnderline= FU_NONE
    cursor.markierungAufheben()
    cursor.zurueck()
    self.Unterstrichen= w

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()


  def setzeDurchgestrichen(self, w):
    """Mit dieser Methode kannst Du einstellen, ob das Zeichen durchgestrichen werden soll
    oder ob es nicht durchgestrichen dargestellt werden soll. Der Methode muss beim Aufruf
    ein Wahrheitswert als Parameter übergeben werden. In Ponto gibt es die zwei
    Wahrheitswerte: True und False. Moechtest Du also, dass ein Zeichen durchgestrichen
    dargestellt wird, rufst Du setzedurchgestrichen() mit dem Wahrheitswert True auf,
    willst Du, dass es normal dargestellt wird, mit dem Wahrheitswert False."""
    ###With this method you can determine, whether a character is crossed out.###

    #Aktuelle Position des Cursors merken:
    cursor= self.Dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    cursor=self.Dokument.gibCursor()
    cursor.markiereVor()
    cursor.gibAktuellePosition().CharCrossedOut=w
    cursor.markierungAufheben()
    cursor.zurueck()
    self.Durchgestrichen= w

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()


  def setzeSchriftgroesse(self, z):
    """Mit dieser Methode kannst Du die Schriftgrösse einstellen. Der Methode muss beim Aufruf
    genau eine Zahl als Parameter übergeben werden. Diese Zahl steht für die Schriftgrösse des Zeichens."""
    ###With this method you can change the fontsize of a character.###

    #Aktuelle Position des Cursors merken:
    cursor= self.Dokument.gibCursor()
    [absatz,index]= cursor.gibAktuellePositionInAbsatzliste()

    cursor=self.Dokument.gibCursor()
    cursor.geheVorZeichenInAbsatz(self.Absatz, self)
    cursor.markiereVor()
    cursor.gibAktuellePosition().CharHeight= z
    cursor.markierungAufheben()
    cursor.zurueck()
    self.Schriftgroesse= z

    #Cursor an vorherige Position setzen
    if index==-1:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      cursor.geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      cursor.vor()


  #get/gib-Methoden
  def gibSymbol(self):
    """liefert das vom Zeichenobjekt repraesentierte symbol."""
    ###returns the symbol represented by the character object.###
    return(self.Symbol)

  def gibFett(self):
    """liefert den Wahrheitswert des Attributs 'Fett'."""
    ###returns the value of the attribute 'Bold'.###
    return self.Fett

  def gibKursiv(self):
    """liefert den Wahrheitswert des Attributs 'Kursiv'."""
    ###returns the value of the attribute 'Italic'.###
    return self.Kursiv

  def gibUnterstrichen(self):
    """liefert den Wahrheitswert des Attributs 'Unterstrichen'."""
    ###returns the value of the attribute 'Underlined'.###
    return self.Unterstrichen

  def gibDurchgestrichen(self):
    """liefert den Wahrheitswert des Attributs 'durchgestrichen'."""
    ###returns the value of the attribute 'Crossedout'.###
    return self.Durchgestrichen

  def gibSchriftfarbe(self):
    """liefert den Wert des Attributs 'Schriftfarbe' in der internen OpenOffice-Farbdarstellung."""
    ###returns the value of the attribute 'Fontcolor' in the internal OpenOffice-Colordisplay.###
    return self.Schriftfarbe

  def gibHintergrundfarbe(self):
    """liefert den Wert des Attributs 'Hintergrundfarbe' in der internen OpenOffice-Farbdarstellung."""
    ###returns the value of the attribute 'Backgroundcolor' in the internal OpenOffice-Colordisplay.###
    return self.Hintergrundfarbe

  def gibSchriftgroesse(self):
    """liefert die Schriftgroesse eines Zeichens."""
    ###returns the fontsize of a character.###
    return self.Schriftgroesse

  def gibSchriftart(self):
    """liefert den Wert des Attributs 'Schriftart'"""
    ###returns the value of the attribute 'Font'###
    return self.Schriftart


#class Seite -----------------------------------------------------------
class Seite:
  def __init__(self, dokument, seitenzahl):
    #self.Abschnitt= abschnitt
    #self.Seitenvorlage= seitenvorlage
    self.Dokument= dokument
    self.Seitenzahl= seitenzahl

  #set/setze-Methoden --------------------------
  #Vielleicht später mal:
  #def setzeAbschnitt(self, abschnitt):
    #"""Interne Methode"""
    ####internal method###
    #self.Abschnitt= abschnitt

  #Vielleicht später mal:
  #def setzeSeitenvorlage(self, seitenvorlage):
    #"""Interne Methode"""
    ####internal method###
    #self.Seitenvorlage= seitenvorlage

  def setzeSeitenzahl(self, seitenzahl):
    """Interne Methode"""
    ###internal method###
    self.setzeSeitenzahl= seitenzahl


  #get/gib-Methoden ---------------------------
  def gibAbschnitt(self):
    """Liefert Referenz auf das Abschnitt-Objekt, dessen Absätze die Seite (momentan) enthaelt."""
    ###returns a reference to the section object whose paragraphs the page contains at the moment###

    #Aktuelle Position des Cursors merken:
    [absatz,index]= self.Dokument.cursor.gibAktuellePositionInAbsatzliste()

    #Überprüfen, ob Seite überhaupt da ist
    aktuelle_seitenzahl= self.Dokument.gibCursor().gibAktuellePosition().getPage()
    while aktuelle_seitenzahl < self.Seitenzahl and  self.Dokument.gibCursor().vor():
      aktuelle_seitenzahl= self.Dokument.gibCursor().gibAktuellePosition().getPage()
    while aktuelle_seitenzahl > self.Seitenzahl and  self.Dokument.gibCursor().zurueck():
      aktuelle_seitenzahl= self.Dokument.gibCursor().gibAktuellePosition().getPage()

    #Ein Abschnitt wird nur dann zurückgegeben, wenn die Seite überhaupt noch
    #im Dokument existiert:
    if aktuelle_seitenzahl==self.Seitenzahl:
      [absatz_fuer_abschnitt,index_nichtwichtig]= self.Dokument.cursor.gibAktuellePositionInAbsatzliste()
      abschnitt= absatz_fuer_abschnitt[0].gibAbschnitt()
    else:
      abschnitt=None

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      self.Dokument.gibCursor().geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      self.Dokument.gibCursor().geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      self.Dokument.gibCursor().vor()

    return(abschnitt)


  def gibSeitenvorlage(self):
    """Liefert den aktuellen Wert des Attributs 'Seitenvorlage'"""
    ###returns the current value of the attribute 'PageStyle'###

    #Aktuelle Position des Cursors merken:
    [absatz,index]= self.Dokument.cursor.gibAktuellePositionInAbsatzliste()

    #Überprüfen, ob Seite überhaupt da ist
    aktuelle_seitenzahl= self.Dokument.gibCursor().gibAktuellePosition().getPage()
    while aktuelle_seitenzahl < self.Seitenzahl and self.Dokument.gibCursor().vor():
      aktuelle_seitenzahl= self.Dokument.gibCursor().gibAktuellePosition().getPage()
    while aktuelle_seitenzahl > self.Seitenzahl and self.Dokument.gibCursor().zurueck():
      aktuelle_seitenzahl= self.Dokument.gibCursor().gibAktuellePosition().getPage()

    #Eine Seitenvorlage wird überhaupt nur dann potentiell zurückgegeben, wenn
    #die Seite noch im Dokument existiert:
    if aktuelle_seitenzahl==self.Seitenzahl:
      [absatz_fuer_seitenvorlage,index_nichtwichtig]= self.Dokument.cursor.gibAktuellePositionInAbsatzliste()
      seitenvorlage= absatz_fuer_seitenvorlage[0].gibAbschnitt().gibSeitenvorlage()
    else:
      seitenvorlage= None

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      self.Dokument.gibCursor().geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      self.Dokument.gibCursor().geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      self.Dokument.gibCursor().vor()

    return(seitenvorlage)


  def gibSeitenzahl(self):
    """Liefert den Wert des Attributs Seitenzahl"""
    ###returns the value of the attribute 'PageNumber'###

    #Aktuelle Position des Cursors merken:
    [absatz,index]= self.Dokument.cursor.gibAktuellePositionInAbsatzliste()

    #Überprüfen, ob Seite überhaupt da ist
    aktuelle_seitenzahl = self.Dokument.gibCursor().gibAktuellePosition().getPage()
    while aktuelle_seitenzahl < self.Seitenzahl and self.Dokument.gibCursor().vor():
      aktuelle_seitenzahl = self.Dokument.gibCursor().gibAktuellePosition().getPage()
    while aktuelle_seitenzahl > self.Seitenzahl and self.Dokument.gibCursor().zurueck():
      aktuelle_seitenzahl = self.Dokument.gibCursor().gibAktuellePosition().getPage()

    #Eine Seitenzahl wird nur dann zurückgegeben, wenn die Seite überhaupt noch
    #im Dokument existiert:
    if aktuelle_seitenzahl==self.Seitenzahl:
      szahl= self.Seitenzahl
    else:
      szahl= None

    #Und Cursor wieder an alte Position zurücksetzen:
    if index==-1:
      self.Dokument.gibCursor().geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index+1])
    else:
      self.Dokument.gibCursor().geheVorZeichenInAbsatz(absatz[0],absatz[0].gibZeichenobjektliste()[index])
      self.Dokument.gibCursor().vor()

    return(szahl)


#class Seitenvorlage -----------------------------------------------------------
class Seitenvorlage:
  def __init__(self, dokument, pagestyle_name):
    self.Dokument= dokument
    self.Komponente= self.Dokument.gibKomponente()
    self.Cursor= self.Dokument.gibCursor()
    self.Pagestyle_name= pagestyle_name

    #pageStyleName= self.cursor.gibAktuellePosition().PageStyleName
    pageStyles= self.Komponente.StyleFamilies.getByName("PageStyles")
    pageStyle= pageStyles.getByName(pagestyle_name)

    self.SeitenrandOben= pageStyle.TopMargin
    self.SeitenrandUnten= pageStyle.BottomMargin
    self.SeitenrandLinks= pageStyle.LeftMargin
    self.SeitenrandRechts= pageStyle.RightMargin
    self.Breite= pageStyle.Width
    self.Hoehe= pageStyle.Height
    self.Spaltenzahl= pageStyle.TextColumns.getColumnCount()

  def gibName(self):
    """Liefert den Wert des Attributs 'Name' (pagestyle_name)"""
    ###Returns the value of the attribute 'Name' (pagestyle_name)###
    return(self.Pagestyle_name)

  def gibSeitenrandOben(self):
    """Liefert den Wert des Attributs 'SeitenrandOben'"""
    ###Returns the value of the attribute 'TopMargin'###
    return(self.SeitenrandOben)

  def gibSeitenrandUnten(self):
    """Liefert den Wert des Attributs 'SeitenrandUnten'"""
    ###Returns the value of the attribute 'BottomMargin'###
    return(self.SeitenrandUnten)

  def gibSeitenrandLinks(self):
    """Liefert den Wert des Attributs 'SeitenrandLinks'"""
    ###Returns the value of the attribute 'LeftMargin'###
    return(self.SeitenrandLinks)

  def gibSeitenrandRechts(self):
    """Liefert den Wert des Attributs 'SeitenrandRechts'"""
    ###Returns the value of the attribute 'RightMargin'###
    return(self.SeitenrandRechts)

  def gibBreite(self):
    """Liefert den Wert des Attributs 'Breite'"""
    ###Returns the value of the attribute 'Width'###
    return(self.Breite)

  def gibHoehe(self):
    """Liefert den Wert des Attributs 'Hoehe'"""
    ###Returns the value of the attribute 'Height'###
    return(self.Hoehe)

  def gibSpaltenzahl(self):
    """Liefert den Wert des Attributs 'Spaltenzahl'"""
    ###Returns the value of the attribute 'NoOfTextColumns'###
    return(self.Spaltenzahl)


#Zusätzliche Aliase zur Komformität mit Bezeichnern in Schulbüchern:
CURSOR= Cursor
Textmarke= Cursor
TEXTMARKE= Cursor

DOKUMENT= Dokument
Textdokument= Dokument
TEXTDOKUMENT= Dokument

ABSCHNITT= Abschnitt
ABSATZ= Absatz
ZEICHEN= Zeichen
SEITE= Seite
SEITENVORLAGE= Seitenvorlage


#---------------------------
#Zusätzl. Funktion:

def basis():
    localContext= uno.getComponentContext()
    resolver= localContext.ServiceManager.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", localContext)
    try:
      ctx= resolver.resolve("uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext")
    except:
      import os
      try:
        # die folgenden Zeile funktioniert unter Linux
        os.system('soffice "--accept=socket,host=localhost,port=2002;urp;" &')
        ctx= resolver.resolve("uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext")
      except:
        print ("Es kann noch keine Verbindung zu OpenOffice hergestellt werden.\nOpenoffice/LibreOffice wird nun gestartet.")
        sleep(10)

      ctx= resolver.resolve("uno:socket,host=localhost,port=2002;urp;StarOffice.ComponentContext")

basis()
