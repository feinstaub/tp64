Screenshots
===========

Ein Screenshot ist eine Kopie des Bildschirminhaltes.

<!-- toc -->

- [Screenshot aufnehmen](#screenshot-aufnehmen)
- [Bildschirmvideo aufnehmen](#bildschirmvideo-aufnehmen)
  * [peek](#peek)
- [Ähnliche Programme](#ahnliche-programme)
  * [Kazam Screencaster](#kazam-screencaster)
  * [Simple Screen Recorder](#simple-screen-recorder)
  * [DoTheEvo/screen-capture-general](#dotheevoscreen-capture-general)
- [Exkurs](#exkurs)
  * [Weiteres auf DoTheEvo: ANGRYsearch](#weiteres-auf-dotheevo-angrysearch)
  * [Weiteres auf DoTheEvo: Infographiken zu Programmen](#weiteres-auf-dotheevo-infographiken-zu-programmen)

<!-- tocstop -->

Screenshot aufnehmen
--------------------
Unter KDE/Plasma wird das Programm Spectacle (globale Taste **Druck** oder **Print Screen**) verwendet.

![](tastatur-print-screen.png)

Dort kann man auch auswählen, ob nur ein bestimmter rechteckiger Bereich aufgenommen werden soll.

Am Ende kann man das Bild an ein Programm zur Bearbeitung schicken, es speichern oder es per Drag&Drop woanders hinziehen.


Bildschirmvideo aufnehmen
-------------------------
### peek
Mit **peek** ($ sudo zypper install peek) kann man animierte gif-Bilder eines Bildschirmbereiches aufnehmen.

* Peek starten
* Größe und Position des Fensters auswählen
* Record drücken
* Aufnahme ist gestartet
* Stop drücken
* Auswählen, wo das Video gespeichert werden soll.

Ähnliche Programme
------------------
### Kazam Screencaster
* ...

### Simple Screen Recorder
* ...

### DoTheEvo/screen-capture-general

https://github.com/DoTheEvo/screen-capture-general - enthält "bash scripts for screenshots, gif recording, video recording".

Abhängigkeiten:

* maim - for making screenshots
* slop - to allow maim area screnshots
* xdotool - to allow active window screenshots
* imagemagick - for converting png to jpg
* sox - for playing the shutter sound
* byzanz - for recording the gif
* ffmpeg - for recording the video and coverting to webm


Exkurs
------

### Weiteres auf DoTheEvo: ANGRYsearch

https://github.com/DoTheEvo/ANGRYsearch - enthält "Linux file search, instant results as you type"

### Weiteres auf DoTheEvo: Infographiken zu Programmen

https://github.com/DoTheEvo/infographics (todo: genauer anschauen)
