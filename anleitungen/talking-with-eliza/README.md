Eliza, die virtuelle Therapeutin
================================

<!-- toc -->

- [Installieren](#installieren)
- [Englische Therapie-Session](#englische-therapie-session)
- [Therapie-Session auf Deutsch](#therapie-session-auf-deutsch)
- [Ideen für ein eigenes Programm](#ideen-fur-ein-eigenes-programm)
- [Weiteres](#weiteres)
  * [ELIZA+](#eliza)

<!-- tocstop -->

Installieren
------------
* Auf software.opensuse.org nach dem Paket **perl-Chatbot-Eliza** suchen:

![](inst-1-find-eliza.png)

* Repository **devel:languages:perl** auswählen und 1-Click-Install klicken

* Unnötige Repositories abwählen:

![](inst-2-select-repos.png)

* Weiter weiter etc.

TODO: auf Manjaro-Linux?

Englische Therapie-Session
--------------------------
    $ perl /usr/share/doc/packages/perl-Chatbot-Eliza/examples/simple

    Welcome to your therapy session.
    Your therapist's name is Liz.

    Liz:    Is something troubling you?
    you:    Tomorrow I have to write an English exam.
    Liz:    That's quite interesting.


Therapie-Session auf Deutsch
----------------------------
    $ mkdir -p ~/ag/eliza
    $ cd ~/ag/eliza
    $ cp /usr/share/doc/packages/perl-Chatbot-Eliza/examples/deutsch.txt .
    $ perl /usr/share/doc/packages/perl-Chatbot-Eliza/examples/deutsch

    Hans:   Was hast du denn?
    you:    Mir ist langweilig.
    Hans:   Bist du zu mir gekommen, weil dir langweilig ist?
    you:    Ehrlich gesagt ja.
    Hans:   Ich verstehe.

Ideen für ein eigenes Programm
------------------------------
* Eliza-Therapy-PyQt-GUI
    * "Ich glaub' ich brauch' ne Therapie..."
    * Listbox mit schönen Chat-Items

Weiteres
--------
### ELIZA+
* https://github.com/JEFworks/ELIZAplus
