Behandlung von Linux-Ausnahme-Situationen
=========================================

siehe auch ../linux-installieren/

Graphische Oberfläche ist weg - Teil 1
--------------------------------------
* **Strg+Alt+F7** drücken.

Immer noch nicht da? Dann weiter zu Teil 2.

Graphische Oberfläche ist weg - Teil 2
--------------------------------------
* Auf ein virtuelles Terminal wechseln (mit **Strg+Alt+F1**), falls nicht schon da.

```
Welcome to openSUSE Tumbleweed 20170316-Kernel 4.10.3-1-default (tty1).


emp1s2:
Linux-y3kw login:
```

* **Einloggen** mit Benutzer und Passwort.

* **Prüfen**, ob der Display-Manager läuft.

```
$ systemctl status display-manager
```

Fall 1: Er läuft; in der zweiten Zeile steht dann:
```
Active: active (running)
```

Fall 2: Er läuft nicht:
```
Active: inactive (dead)
```

* In beiden Fällen kann man versuchen den **Display-Manager neu zu starten**:

```
$ sudo systemctl restart display-manager
```
Wenn alles gut läuft, dann erscheint der graphische Login-Bildschirm.

### Hinweise
- `systemctl` ist ein Systemprogramm, mit dem man unter anderem Hintergrunddienste verwalten kann.
- Der Display-Manager ist das, was dafür sorgt, dass man einen graphischen Einlog-Bildschirm bekommt.
- basiert teilweise auf https://github.com/sddm/sddm/issues/336

Verschiedenes
-------------
* ["btrfs process uses 50% of processor and lags everything"](https://forums.opensuse.org/showthread.php/523513-btrfs-process-uses-50-of-processor-and-lags-everything?p=2831469#post2831469), 2017
