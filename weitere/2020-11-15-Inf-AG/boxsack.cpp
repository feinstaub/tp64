#include <iostream>
#include <string>
#include <vector>

using namespace std;

class object {
public:
    object(float leben,int basisschaden,float ausdauer);
    void datenausgabe();
    void schlagen(object* ziel);
    void einstecken(float schaden);
private:
    float leben_;
    int basisschaden_;
    float ausdauer_;
};

object::object(float leben,int basisschaden,float ausdauer):
    leben_(leben),
    basisschaden_(basisschaden),
    ausdauer_(ausdauer)
    {}

void object::datenausgabe(){
    cout << "Das Object besitzt noch " << leben_ << "Leben.\n";
    cout << "Es hat einen Standartschaden von " << basisschaden_ <<".\n";
    cout << "Und hat noch eine Ausdauer von " << ausdauer_ << "Prozent.\n";
}

void object::schlagen(object* ziel){
    float damage = 0;
    damage = basisschaden_ * ausdauer_;
    ausdauer_ = ausdauer_ - 0.1;
    ziel->einstecken(damage);
}

void object::einstecken(float damage){
    leben_=leben_-damage;
}

int main(){
    object boxer(100,10,100);
    object boxsack(1000,0,0);
//     object *schadenuber;
//     schadenuber=&boxsack;
    boxer.schlagen(&boxsack);
    boxsack.einstecken(200);
    boxsack.datenausgabe();
    boxer.datenausgabe();
}
