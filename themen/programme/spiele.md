Spiele-Liste
============

<!-- toc -->

- [Anleitung: Software von opensuse.org installieren](#anleitung-software-von-opensuseorg-installieren)
- [Für Zuhause](#fur-zuhause)
  * [Übung für Zuhause: Moon-Lander installieren](#ubung-fur-zuhause-moon-lander-installieren)
  * [Übung für Zuhause: Super-Tux-Kart installieren](#ubung-fur-zuhause-super-tux-kart-installieren)
  * [Potential zum Selber-Coden](#potential-zum-selber-coden)
  * [Konsolenspiele](#konsolenspiele)
  * [Spiele-Listen](#spiele-listen)
  * [Kurz angespielt auf openSUSE Tumbleweed](#kurz-angespielt-auf-opensuse-tumbleweed)
- [Spiele (weitere)](#spiele-weitere)
  * [Klein](#klein)
  * [KDE-Spiele im Überblick](#kde-spiele-im-uberblick)
  * [Groß](#gross)
  * [archive.org](#archiveorg)

<!-- tocstop -->

Anleitung: Software von opensuse.org installieren
-------------------------------------------------
* TODO, siehe: ~/dataDocuments/FS_Computer-AG/src-arbeitsblatt-pub/arbeitsblatt-software-opensuse-org.adoc --> übertragen nach hier

Für Zuhause
-----------
### Übung für Zuhause: Moon-Lander installieren
* Paketname: moon-lander

### Übung für Zuhause: Super-Tux-Kart installieren
TODO, genauer beschreiben:

* Schauen, ob games repo schon da
    * $ zypper se supertuxkart
* bei software.opensuse.org das games-Repo (Screenshot) auswählen

### Potential zum Selber-Coden
* Moon Lander
    * siehe c-cpp-moonlander/README.md
* moon-buggy
    * Anzahl Leben erhöhen
* Road Fighter (2D-Rennen von oben, nur geradeaus)
    * Menge an Treibstoff erhöhen
    * TODO
* Tunneler
    * siehe wikitolearn

### Konsolenspiele
* **moon-buggy**

### Spiele-Listen
* siehe unten

### Kurz angespielt auf openSUSE Tumbleweed

Alphabetische Liste der Paketnamen.

2 = mittel / 3 = gut

* **Dolphin-Island-2**
    * 2
    * Retro-Jump-and-run, weibliche Hauptrolle; Hinweis: Strg drücken, um bei den Dialogen weiterzukommen
* **cuyo**
    * 3
    * Tetris-ähnlich, aber mit jedem Level andere Graphik und leicht andere Regeln
        * Graphiken mergen sich zusammen, je nachdem wieviele nebeneinander liegen
* **asciiportal**
    * 3
    * 2D-Logik-Portalspiel im Textmodus; nicht ganz leicht
* **Brain Party**
    * 3
    * Geschicklichkeits- und Denkspielesammlung
* **caph**
    * 2
    * mit dem Stift physikalische Objekte zeichnen zum Lösen von Aufgaben
* **cubosphere** - Logik und Geschicklichkeit
    * 3
    * mit einem Ball auf 3-dimensionalen Wegen zum Ziel kommen
    * https://sourceforge.net/projects/cubosphere/
    * Code with Bazaar: https://sourceforge.net/p/cubosphere/code/

    Segfault am Ende in __run_exit_handlers:

    Thread 1 "cubosphere" received signal SIGSEGV, Segmentation fault.
    0x00007ffff4e0e579 in FT_Done_Face () from /usr/lib64/libfreetype.so.6
    (gdb) bt
    #0  0x00007ffff4e0e579 in FT_Done_Face () from /usr/lib64/libfreetype.so.6
    #1  0x00007ffff6fc0acf in TTF_CloseFont () from /usr/lib64/libSDL_ttf-2.0.so.0
    #2  0x00005555555a2135 in TLoadedFont::Clear() ()
    #3  0x000055555557be1a in TCuboGame::~TCuboGame() ()
    #4  0x00007ffff67197cc in __run_exit_handlers () from /lib64/libc.so.6
    #5  0x00007ffff67198fc in exit () from /lib64/libc.so.6
    #6  0x00007ffff6701ff2 in __libc_start_main () from /lib64/libc.so.6
    #7  0x000055555556dbda in _start ()
    (gdb)

* **digger**
    * 2
    * Old-School Diamanten aufsammeln. Achtung vor Aliens.
* **Dust Racing 2D**
    * 2
    * 2D-Rennen von oben mit Qt-Strecken-Editor
* **Emilia Pinball** (emilia)
    * 2
    * Nettes Pinballspiel mit zwei verschiedenen Tischen
* **KBounce**
    * wie Antix
    * Ideen: mit Tastatur bedienen
* **KReversi**
    * Ideen: Farben anpassbar machen, Quit-Bug beim Player-Select-Dialog fixen
* **Minilens**
    * 3
    * Ein süßer Aufräumroboter in einem idyllischen Atomkraft-Szenario; Geschicklichkeit; nette Musik
    * Basiert auf der freien und plattformunabhängigen **Godot-Spiele-Engine** mit integriertem Entwicklungsstudio
    * Überblick siehe https://de.wikipedia.org/wiki/Godot_Engine
    * Projekt-Webseite: https://godotengine.org/
* **Ninja-Training**
    * 3
    * einfaches Laufspiel; basiert auf Godot-Engine
* **Ri-li**
    * 3
    * Snake-artiges Spiel mit Holzeisenbahnen
* **Veraball**
    * 2
    * mit einem Ball auf 3-dimensionalen Wegen zum Ziel kommen / schwer zu kontrollieren

1 = nicht so toll / 0 = ging gar nicht

* 2h4u
    * 1
    * Hinweis: Programmname '2H4U', zwei-spaltiges Tetris mit Wallbreaker-Komponente, die auf Anhieb nicht aktiv war
* abuse-free
    * 0
* angrydd
    * 1
    * 150 MB - tetris-ähnlich, multiplayer-fähig, wirkt eher billig
* CubaLetra
    * 0
    * Wortwürfel? / ging nicht: Symbol Lookup Error
* fofix - Fret's on Fire
    * 0
    * Gitarrenspiel / ging nicht: python-numeric fehlt
* heroes-tron
    * 1
    * Tron-ähnlich, viele Features, aber schlecht Graphik, Vollbildmodus mit $ heroes -F
* jetpaca
    * 1
    * Fliegendes Schaf, Steuerung nicht verstanden
* MouseBoat
    * 1
    * eine Maus angelt in einem Teich; Steuerung nicht ganz verstanden; basiert auf Godot-Engine
* Planet-Rider
    * 1
    * wirkt billig; basiert auf Godot-Engine
* Reaction
    * 0
    * ging nicht
* Road Fighter
    * 1
    * Autorennen. irgendwann geht immer der Sprit aus. Soll das so sein?

Schießspiele / nicht empfohlen

* 0verkill
    * 0
    * Shooter-Spiel auf de Konsole, "could not connect"
* **AstroMenace**
    * 2
    * Weltraumschießen
* bitfighter
    * 1
    * Online-Multiplayer, keiner da, stürzt bei Selektion des Servers ab
* blobAndConquer
    * 1
    * zu krasse Musik
* DynaDungeons
    * 1
    * Bomberman-Klon, ohne KI

Unsortiert:

* amoebax
    * 1
    * schönes Design; ansonsten ähnlich wie Tetris; multiplayer-fähig
* barbie_seahorse_adventures
    * 3
    * schönes Super-Mario-ähnliches Jump'n'Run; Retro; pixelig
* briquolo
    * 2
    * Break-out in Schräg-3D mit einigen Specials
* ceferino
    * 2
    * nettes Geschicklichkeitsspiel; nach oben mit einem Messer grüne Bälle treffen
* chroma
    * 3
    * schwieriges Denkspiel; einfach, aber wirksam; schönes Design
* colobot
    * 3
    * komplexes und einladendes Weltraumabenteuer, aber mit guten Instruktionen
    * C++-Code: https://github.com/colobot/colobot
* cutemaze
    * 2
    * Nettes Labyrinthspiel; Qt-basiert
* darkplaces
    * 0
    * game data missing
* double-cross
    * 2
    * Tetris von zwei Seiten; gewöhnungsbedürftige Musik; in Python geschrieben
* endgame-singularity
    * 3
    * Klick-Strategie-Spiel; passende Hintergrundmusik; eine AI wird intelligent
* foobillard
    * 2
    * 3D-Billard-Simulation
* formido
    * 2
    * Ballern, psychedelische Käfer
* fretscpp
    * 1
    * Menüs defekt
* galaxis
    * TODO
* gnurobbo
    * TODO
* gplanarity
    * TODO
* gti
    * 2
    * für git-Benutzer
* lightyears
    * 1
    * Weltraum-Stragegiespiel
* liquidwar
    * 2
    * sieht zwar billig aus, aber macht Spaß. Man muss seine Flüssigkeit so steuern, dass die andere aufgesogen wird
* lutris
    * 1
    * Spieleinstallerplattform / mit Steam ?
* marsshooter
    * 3
    * verrücktes und schnelles Weltraumschießen mit guter Musik. Einfach und kurzweilig.
* meandmyshadow
    * 3
    * Schönes Rätsel-Spielkonzept mit Spieler und Schatten, schöne Musik, schöne Optik
* mirrormagic
    * 2
    * Schönes Spiel-Physik-Spiel
* moon-lander
    * 2
    * Graviations-Mond-Landspiel
* notpacman
    * 3
    * Pacman, aber mit Physik (Feld kann gedreht werden)
* nottetris2
    * 3
    * Tetris ohne "Gitter" mit Physik
* paintown
    * 0
    * ging nicht
* pink-pony
    * 3
    * TRON mit Ponys und Herzen
* nogravity
    * 1
    * schlecht aussehendes Weltraumgeballer
* neverball
    * 3
    * Schönes Ballrollspiel
* nodereviver
    * 2
    * Nettes Spiel, lustige Texte, wird schnell schwer
* openclonk
    * 2
    * 2D-Tunnel-Adventure mit schöner Graphik
* performous
    * 1
    * Karaoke, ging nicht so wirklich, Equipment fehlt?



Spiele (weitere)
-----------------
Im folgenden werden Spiele aufgelistet, die mit einer Freien-Software-Lizenz ausgestattet sind.

### Klein
In diesem Abschnitt werden kleine, aber feine Spiele für Zwischendurch vorgestellt.

    ! openSUSE-Package-Name !! Repository   !! Beschreibung
    |-
    | kollision             || Standard     || Geschicklichkeit - Auge-Maus-Koordination
    |-
    | kbreakout             || Standard     || Breakout
    |-
    | kblocks               || Standard     || Tetris-Klon
    |-
    | tunneler              || games        || Durch die Erde graben und den Gegner besiegen
    |-
    | tong                  || games        || Kombination aus Tetris und Pong
    |-
    | ---                   || ---          || ---
    |-
    | jumpnbump             || Standard     || Lustiges Spiel für 2 bis 4 Personen
    |-
    | sdl-ball              || Standard     || Breakout
    |-
    | ---                   || ---          || ---
    |-
    | mrrescue              || Standard     || Mr Rescue - Pixeliger Feuerwehrmann mit 8-Bit-Musik, Old-School


### KDE-Spiele im Überblick
* Sortiert nach Kategorie: https://games.kde.org/
* Alphabetisch: https://www.kde.org/applications/games

Gnome-Spiele:

* https://wiki.gnome.org/Attic/Games
* https://wiki.gnome.org/Apps/Games/ (Stand 2016: derzeit nur mit flatpak zu installieren)

### Groß

In diesem Abschnitt sind aufwändig entwickelte Spiele zu finden.

    ! openSUSE-Package-Name !! Repository   !! Beschreibung
    |-
    | supertuxkart          || Standard     || SuperTuxKart - Autorennen (3D)
    |-
    | ultimatestunts        || games        || Ultimate Stunts (ustunts) - Autorennen und vor allem [http://www.ultimatestunts.nl/documentation/en/trackedit.htm Strecken selber bauen] (3D), Musik: <source>$ rpm -ql ultimatestunts-data | grep ogg</source>
    |-
    | pingus                || Standard     || [https://pingus.seul.org/welcome.html Freier Lemmings-Klon], deutsches Intro-Tutorial
    |-
    | xmoto                 || games        || XMoto - Motorcross-Simulation
    |-
    | minetest              || games        || [http://www.minetest.net/ Minetest], Minecraft-Klon
    |-
    | speed-dreams          || games        || Speed Dreams - Autorennen (3D) (1,5 GB)
    |-
    | torcs                 || games        || TORCS - Autorennen (3D)
    |-
    | vdrift                || games        || VDrift - Autorennen (3D) (1 GB)
    |-
    | bzflag                || Standard     || BZFlag - Mehrspieler-Panzer-Spiel (3D)
    |-
    | sauerbraten           || games        || Cube 2: Sauerbraten - Egoshooter (Einzel- und Mehrspieler) (3D)
    |-
    | Funktioniert nicht:   ||              ||
    |-
    | stuntrally (non-oss?, segfault?) || games || Stunt Rally - Autorennen (3D)

### archive.org
Folgende Spiele sind zwar keine freie Software, können aber online über einen freien Webbrowser gespielt werden.

"Das Internet Archive in San Francisco ist ein gemeinnütziges Projekt, das 1996 von Brewster Kahle gegründet wurde. Es hat sich die Langzeitarchivierung digitaler Daten in frei zugänglicher Form zur Aufgabe gemacht." (https://de.wikipedia.org/wiki/Internet_Archive)

MS-DOS:
* [https://archive.org/details/msdos_Prince_of_Persia_1990 Prince of Persia] ([https://www.youtube.com/watch?v=6dOhUCvqV7o Walkthrough])
* [https://archive.org/details/msdos_The_Adventures_of_Captain_Comic_1988 The Adventures of Captain Comic] - Jump'n'Run, ([http://messui.the-chronicles.org/cheats/37.txt Tasten])
* [https://archive.org/details/msdos_Stunts_1990 Stunts] - Autorennen
* [https://archive.org/details/msdos_The_Lion_King_1994 The Lion King] - Jump'n'Run
* [https://archive.org/details/softwarelibrary_msdos_games/v2 Übersicht]

Windows95-Gallerie:
* [https://archive.org/details/win3_Brickbus Brickbuster (1995)]
** Andere Variante auf OpenProcessing: http://www.openprocessing.org/sketch/134612
** Siehe auch '''kbreakout'''
** Tipp: selber nachprogrammieren
* [https://archive.org/details/softwarelibrary_win3_showcase&tab=collection Übersicht]

Apple II:
* [https://archive.org/details/apple_ii_library_games Übersicht]
