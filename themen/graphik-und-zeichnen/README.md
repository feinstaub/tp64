Graphik und Zeichnen
====================

<!-- toc -->

- [Pixel und Farben](#pixel-und-farben)
  * [Wie funktionieren Farben am Computer?](#wie-funktionieren-farben-am-computer)
  * [KColorChooser](#kcolorchooser)
  * [KMag](#kmag)
- [Inkscape](#inkscape)
- [Kolourpaint und GIMP](#kolourpaint-und-gimp)

<!-- tocstop -->

Pixel und Farben
----------------
### Wie funktionieren Farben am Computer?
Mit Rot, Grün, Blau (RGB) kann man alle Farben durch Mischen darstellen.

Oder anders formuliert: "Ein RGB-Farbraum ist ein additiver Farbraum, der Farbwahrnehmungen durch das additive Mischen dreier Grundfarben (Rot, Grün und Blau) nachbildet." (https://de.wikipedia.org/wiki/RGB-Farbraum)

### KColorChooser
Experimentiere mit dem Programm KColorChooser.

1. Die Werte rechts für Rot, Grün und Blau können 0 bis 255 betragen. Trage einige Kombinationen aus 0 und 255 ein und schaue dir die Farben an.
2. Verschiebe das Kreuz im quadratischen Feld mit der Maus und beobachte die Werte für Rot, Grün und Blau.
3. Verschiebe den Helligkeitsregler rechts und beobachte die Werte für Rot, Grün und Blau.
4. Verwende den Knopf "Bildschirmfarbe auswählen" ("Pick Screen Color") rechts, fahre mit der Maus über den Bildschirm und hole dir damit die Farbe von Bildschirmpixeln.

### KMag
1. Starte das Programm KMag.
2. Stelle die Refresh-Rate auf sehr hoch.
3. Stelle die Vergrößerung auf 1:20 und klicke auf den "Maus folgen"-Knopf.
4. Bewege die Maus über interessante Stellen auf dem Bildschirm.

Inkscape
--------
* [Inkscape Einstieg](../..anleitungen/inkscape-einstieg/README.md)

Kolourpaint und GIMP
--------------------
* TODO:
    * Kolourpaint
    * GIMP
