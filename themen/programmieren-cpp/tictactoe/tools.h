#pragma once

#include <iostream>
#include <ctime>        // std::time
#include <unistd.h>     // fork, execl, usleep
#include <fcntl.h>      // open
#include <pwd.h>        // getpwuid
#include <sys/stat.h>   // mkdir
#include <errno.h>

namespace tools
{
void random_init()
{
    std::srand(std::time(nullptr));
}

void sleep_ms(int ms)
{
    usleep(ms * 1000);
}

void play_ogg_blocking(const std::string& path)
{
    // ogg123 outputs stuff on stderr; therefore we redirect it
    std::string cmd = "ogg123 " + path + " 2>/dev/null"; // add & to make it non-blocking even if program exits
    std::system(cmd.c_str());
}

void play_ogg_async(const std::string& path)
{
    //https://stackoverflow.com/questions/43501760/is-stdsystem-or-exec-better-practice
    int child = fork();
    if (child == 0) {
        int fd = open("/dev/null", O_WRONLY);
        dup2(fd, 2);  // redirect stderr to /dev/null to not print anything to console

        //               v warum doppelt?
        execlp("ogg123", "ogg123", path.c_str(), (char*)0);
    }
}

/**
 * path: path to wave file
 * vol:  0..x,  0     : Stille,
 *              [0, 1]: Leiser als Original
 *              1     : Original-Lautstärke
 *              > 1   : Lauter als Original
 */
void play_wav_blocking(const std::string& path, double vol = 1)
{
    std::string cmd = "play " + path + " vol " + std::to_string(vol) + " 2>/dev/null";
    std::system(cmd.c_str());
}

int play_background_music(const std::string& path)
{
    // TODO
    return path.size();
}

// void stop_background_music(int handle)
// {
//     handle = 0;
//     // TODO
// }

// todo: replace: https://stackoverflow.com/questions/3418231/replace-part-of-a-string-with-another-string

bool starts_with(const std::string& s, const std::string& needle)
{
    return s.rfind(needle) == 0;
}

const char* get_homedir()
{
    const char* homedir = nullptr;
    if ((homedir = getenv("HOME")) == nullptr) {
        homedir = getpwuid(getuid())->pw_dir;
    }
    return homedir;
}

/**
 * Upon successful completion, these functions shall return 0.  Otherwise, these functions shall return −1
 */
int mkdir(const char* path)
{
   return ::mkdir(path, 777);
}

/**
 * return empty string on error
 */
std::string make_config_dir(const char* appid)
{
    std::string path = std::string(get_homedir()) + "/.config/";
    if (mkdir(path.c_str()) != 0 && errno != EEXIST)
        return std::string();
    path = path + appid;
    if (mkdir(path.c_str()) != 0 && errno != EEXIST)
        return std::string();
    return path;
}

} // namespace tools

namespace terminal
{
void clear(bool include_scrollback = false)
{
    // https://stackoverflow.com/questions/4062045/clearing-terminal-in-linux-with-c-code
    // -> https://en.wikipedia.org/wiki/ANSI_escape_code#Unix-like_systems
    //    https://stackoverflow.com/questions/6486289/how-can-i-clear-console
    //std::system("clear");
    if (include_scrollback) {
        printf("\x1B[3J");
    }
    printf("\x1B[2J\x1B[H"); // 2J = Clear Screen, H = move curser to the top-left corner
}

/**
 * color: 32: grün, 34: blau, weitere siehe https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
 */
std::string bold_col(char c, int color)
{
    return std::string("\x1B[1;") + std::to_string(color) + "m" + c + "\x1B[1;0m";
}

// https://en.wikipedia.org/wiki/ANSI_escape_code#Terminal_output_sequences
//                  B=down
//                        D=left
void cursor_down()
{
    std::cout << "\033[B";
}

void cursor_down_fwrite()
{
    // Escape sequences     https://en.cppreference.com/w/cpp/language/escape
    // ASCII Chart          https://en.cppreference.com/w/cpp/language/ascii

    char d[3];
    d[0] = 27;
    d[1] = '[';
    d[2] = 'B';
    // https://en.cppreference.com/w/cpp/io/c/fwrite
    fwrite(&d, 1, 3, stdout);
}

/**
 * x = 1..n
 * y = 1..m
 */
void cursor_pos(int x, int y)
{
    printf("\033[%d;%dH", x, y);
}

void cursor_left()
{
    std::cout << "\033[D";
}

} // namespace terminal
