HTML-Einstieg
=============

<!-- toc -->

- [Schnelleinstieg](#schnelleinstieg)
  * [Grundgerüst](#grundgerust)
  * [Erster Inhalt](#erster-inhalt)
  * [Struktur-Elemente](#struktur-elemente)

<!-- tocstop -->

Schnelleinstieg
---------------
* Voraussetzungen
    * Wissen, wie man mit Kate Textdateien bearbeitet.
    * Wissen, wie man sich mit Dolphin Verzeichnisse anschauen kann.

### Grundgerüst
* Kopiere die Kopiervorlage von hier https://wiki.selfhtml.org/wiki/HTML/Tutorials/HTML5-Grundger%C3%BCst mit Kate in eine Text-Datei.
* Speichere die Datei unter **~/ag/html/seite1.html**.
* Gehe mit Dolphin in das Verzeichnis und klicke die Datei an.
* Sie öffnet sich mit Firefox.
* Die Seite ist noch leer.

### Erster Inhalt
* Sichtbarer Inhalt kommt zwischen die body-Tags
    * ```<body>
    Mein Inhalt
    </body>
    ```
    * Speichere die Datei ab und drücke im Firefox F5. Der Text "Mein Inhalt" sollte nun sichtbar werden.

### Struktur-Elemente
* **Überschriften**, Textabsätze und **Listen**: https://wiki.selfhtml.org/wiki/HTML/Tutorials/HTML-Einstieg/Kapitel3
* Fett und kursiv:
    * ```<b>fett</b>, <i>kursiv</i>```
* **Tabellen**: https://wiki.selfhtml.org/wiki/HTML/Tutorials/HTML-Einstieg/Kapitel4
* **Hyperlinks**: https://wiki.selfhtml.org/wiki/HTML/Tutorials/HTML-Einstieg/Kapitel6
* **Bilder**: https://wiki.selfhtml.org/wiki/HTML/Tutorials/HTML-Einstieg/Kapitel7

Mit diesen Elementen kannst du eine eigene Anleitung schreiben.
