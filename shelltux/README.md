ShellTux... has a new home
==========================

New location: https://framagit.org/feinstaub/shelltux

* Bausteine
    * Baustein: "$PATH": eigenes ls vs. which ls; PATH verändern
    * Baustein: "Symlink"
    * Baustein: "Make install": eigenes Programm installieren /usr/local/bin

* Inbox 2020
    * paste -s -d +

How the migration was done
--------------------------
See https://help.github.com/articles/splitting-a-subfolder-out-into-a-new-repository/

Clone the repository to a new location, e.g. new/tp64.

    cd new/tp64

    git filter-branch --prune-empty --subdirectory-filter shelltux

Move the repository base folder from new/tp64 to shelltux.

Create new framagit project called 'shelltux' at https://framagit.org/projects/new

Use command line instructions from Gitlab for 'Existing Git repository'

    git remote rename origin old-origin
    git remote add origin https://framagit.org/feinstaub/shelltux.git
    git push -u origin --all
    git push -u origin --tags

Siehe dort:

* espeak
* nms

Inbox 2019
----------
* Deutsches Wikibuch zu Shellprogrammierung: https://de.wikibooks.org/wiki/Linux-Praxisbuch/_Shellprogrammierung
