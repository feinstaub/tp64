Python-Einstieg mit Computer Science Circles
============================================

Start
-----
* Los geht's mit [**0: Hallo, Welt!**](https://cscircles.cemc.uwaterloo.ca/de)
    * Hinweise:
    * Anders als auf der Webseite empfohlen arbeiten wir ohne Anmeldung. Die Texte und Übungen kann man sehr gut auch so bearbeiten.
        * Die Übungskontrolle erfordert einen Online-Zugang.
        * todo: an einer Offline-Variante arbeiten
    * [Code-Spickzettel](https://cscircles.cemc.uwaterloo.ca/cheatsheet-de)

Häufige Fragen zu einzelnen Aufgaben
------------------------------------
* ...

Hintergrundinfos
----------------
* Computer Science Circles der University of Waterloo
    * https://cscircles.cemc.uwaterloo.ca/ack/
        * "This website is open-source, thanks to a SIGCSE Special Projects grant! The course materials are available under a Creative Commons license. See the git repository or the authoring page for details."
        * github-Repo: https://github.com/cemc/cscircles-wp-content
        * Online Python Tutor: https://github.com/cemc/OnlinePythonTutor
            * http://pythontutor.com/
    * Die deutsche Fassung wird von den Bundesweiten Informatikwettbewerben angeboten (www.bwinf.de)
        * Träger: Gesellschaft für Informatik
