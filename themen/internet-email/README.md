Internet: E-Mail
================

* siehe http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Eigenes_System/Internet-Tipps#E-Mail (TODO: move)
    * Allgemein
    * Mozilla Thunderbird
        * Erweiterung: XNote++
        * PLUS: Online-Kalender-Integration mit Lightning und CalDAV: siehe z. B. https://posteo.de/hilfe/wie-kann-ich-mit-thunderbird-meine-termine-synchronisieren
    * E-Mail-Provider-Auswahl
    * Verschlüsselung
    * Wegwerf-Adressen
        * PLUS: https://temp-mail.org, mit API: https://temp-mail.org/en/api
        * weitere: https://10minutemail.com, https://dropmail.me

* Gibt es einen E-Mail-Dienst, der Privatsphäre garantiert und kostenlos ist?
    * NEIN, derzeit nicht
