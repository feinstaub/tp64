// Passwordspeicher mit Dateien

#include <iostream>
#include <fstream>
#include <vector>
#include <exception>

using namespace std;

struct Benutzer
{
    string name;
    string passwort;
};

void print_liste(const vector<Benutzer>& liste)
{
    for (Benutzer b : liste)
    {
        cout << "Name: " << b.name << " pw: " << b.passwort << endl;
    }
}

void schreibe_namen()
{
    ofstream file("pw.data");
    file << "Anke geheim\nBert 123\n";
    file.close();
}

vector<Benutzer> lese_namen()
{
    vector<Benutzer> benutzer;

//     Benutzer b { "Anton", "1234" };
//     benutzer.push_back(b);

    string filename = "pw.data";
    ifstream ifs(filename);
    if (!ifs.is_open()) {
        throw runtime_error("Unable to load file: " + filename);
    }

    while (!ifs.eof())
    {
        string name;
        string pw;
        ifs >> name;
        ifs >> pw;

        if (name.empty()) {
            break;
        }
        Benutzer b { name, pw };
        benutzer.push_back(b);
    }

    return benutzer;
}

int main()
{
    schreibe_namen();
    vector<Benutzer> benutzer = lese_namen();
    print_liste(benutzer);
    return 0;
}
