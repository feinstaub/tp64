Programm-Liste Multimedia
=========================

<!-- toc -->

- [Multimedia](#multimedia)
  * [Clementine - Musikbibliothek und -spieler](#clementine---musikbibliothek-und--spieler)
  * [youtube-dl](#youtube-dl)
  * [Weitere Musikspieler](#weitere-musikspieler)
  * [SMPlayer - Videospieler](#smplayer---videospieler)
- [Weiteres](#weiteres)
  * [Spracherkennung](#spracherkennung)

<!-- tocstop -->

Multimedia
----------
### Clementine - Musikbibliothek und -spieler
...

### youtube-dl
* https://wiki.ubuntuusers.de/youtube-dl/
    * openSUSE-Paket: https://software.opensuse.org/package/youtube-dl
* Zeitversetztes Schauen von öffentlich-rechtlichen Mediathek-Inhalten

Beispiel:

    youtube-dl --extract-audio --audio-format vorbis --audio-quality 6 URL

### Weitere Musikspieler
* https://www.reddit.com/r/linux/comments/9tjo94/what_music_player_do_you_use/
* Clementine Qt5: https://github.com/clementine-player/Clementine/tree/qt5
* gmusicbrowser
* Audacious
* Elisa
* Cantata

### SMPlayer - Videospieler
...

Weiteres
--------
### Spracherkennung
* Mycroft AI

* Spracherkennung mit KDE/'''Simon''' (hat bei mir bisher nicht funktioniert):
    * Projektseite: https://simon.kde.org/
    * Aktivität 2016: Es besteht Interesse, Simon weiterzuentwickeln, siehe diesen [http://www.mail-archive.com/kde-devel@kde.org/msg09063.html Beitrag aus dem Mailinglisten-Archiv].
    * Aktuelle Situation der Spracherkennungssoftware unter Linux: https://en.wikipedia.org/wiki/Speech_recognition_software_for_Linux
    * Für freie Software-Implementierungen ist es wichtig, frei verfügbare Sprach-Korpusse zu haben. Dieses Ziel setzt sich z. B. '''VoxForge''': [https://en.wikipedia.org/wiki/VoxForge Wikipediaseite], [http://www.voxforge.org/home/about Über das Projekt], [http://www.voxforge.org/home/listen Beispiele von Sprachdateien zum Anhören]
    * basiert auf http://cmusphinx.sourceforge.net/ - Open Source Speech Recognition Toolkit
