Webentwicklung
==============

Nützliche Tipps aus dem Internet
--------------------------------
### Web-App-Frameworks
* 2017-06: [Progressive Web App for WikiToLearn](http://baldi.me/blog/how-to-pwa)
    * von Cristian Baldi
    * Übersicht und Bewertung von Javascript-Frameworks für moderne Webanwendungen.
    * u. a. Vue.js
