// https://de.wikibooks.org/wiki/Ncurses:_Grundlegendes
// sudo zypper install ncurses-devel
// g++ -o ncurses1 ncurses1.cpp -lncurses

#include <ncurses/curses.h>
#include <stdlib.h> //noetig fuer atexit()
#include <locale.h> // für setlocale
#include <unistd.h>

void quit()
{
  endwin();
}

void loop()
{
    for (int i = 0; i < 50; i++) {
        usleep(50000);
        clear();
        mvprintw(8 + (i % 10) - 5, i, "LINES: %d", LINES);
        refresh();
    }
}

int main(void)
{
  // setlocale (LC_ALL, "de_DE"); // https://www.unixboard.de/threads/ncurses-probleme-mit-umlauten-und-ein-paar-sonderzeichen.28910/
  int x, y;

  initscr();
  atexit(quit);
  curs_set(0);

  mvprintw(3, 5, "LINES: %d", LINES);
  mvprintw(4, 5, "COLS:  %d", COLS);

  getyx(stdscr, y, x);
  mvprintw(5, 5, "Momentane Cursorposition:  [%d, %d]", y, x);

  getbegyx(stdscr, y, x);
  mvprintw(6, 5, "Koordinatenursprung:       [%d, %d]", y, x);

  getmaxyx(stdscr, y, x);
  mvprintw(7, 5, "Fenstergröße:              [%d, %d]", y, x);

  mvaddstr(11, 2, "Taste drücken -> Ende");
  refresh();

  loop();

  getch();
  return(0);
}

