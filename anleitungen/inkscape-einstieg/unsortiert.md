Inkscape-Einstieg (unsortiert)
==============================

Lernen
------
* https://inkscape.org/de/lernen/
    * Beginne mit "Grundlagen"

Übersetzung?
------------
* siehe hier https://inkscape.org/de/mitmachen/ubersetzung/
    * aber die Tutorials sind schon übersetzt

Entwicklung?
------------
* https://inkscape.org/de/entwickeln/
    * z. B. Erweiterungen in Python
* Wiki: http://wiki.inkscape.org/wiki/index.php/Inkscape
    * Projekt
        * Gallerien
            * z. B. http://jelly.haifashion.eu/
    * Doku
    * Help Inkscape Without Coding
    * Developer Documentation

Weiteres
--------
### Animation?
* https://inkscape.org/en/learn/animation/
    * Synfig Studio: https://www.synfig.org/
    * Sozi: http://sozi.baierouge.fr/pages/10-about.html
* SMIL
    * https://css-tricks.com/smil-is-dead-long-live-smil-a-guide-to-alternatives-to-smil-features/
    * https://css-tricks.com/guide-svg-animations-smil/
    * [animate-test.html](animate-test.html)
        * https://developer.mozilla.org/en-US/docs/Web/SVG/Element/animate
        * https://developer.mozilla.org/en-US/docs/Web/SVG/SVG_animation_with_SMIL
* Javascript
    * https://medium.com/@aniboaz/animate-svg-4fa7dd00e860
        * https://maxwellito.github.io/vivus/, https://github.com/maxwellito/vivus
        * http://mojs.io/




### Buch
* Bücher und andere Referenzen: https://inkscape.org/de/lernen/bucher
* Inkscape-Referenz: http://write.flossmanuals.net/inkscape
