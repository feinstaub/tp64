ODROID als lokaler Server
=========================

Vorbereitung (Tutor)
--------------------
* siehe [admin-main.md](admin-main.md)
* $ ping ag-pc --> 192.168.178.33
    * Falls nicht geht, dann mit `$ ip address` auf dem Rechner selbst rausfinden (mittels HDMI-Kabel einen Bildschirm anschließen)
    * z. B. 10.2.133.168
    * Hinweis: HDMI überträgt auch Audio (siehe auch "Remote Audio")
    * Idee: ein Startup-Script am Anfang liest die eigene IP-Adresse vor

Einloggen auf dem ODROID von Windows-Rechner
--------------------------------------------
siehe README-xserver-for-win.md

ShellTux starten
----------------
1. Verbinde dich mit deinem Benutzer per SSH auf ag-pc (ggf. in /etc/hosts eintragen):

    ssh deinuser@ag-pc

2. Dann starte ShellTux:

    shelltux
