#!/bin/bash

# $1: Frequenz in Hertz
# $2: Dauer in Millisekunden

# Bei hohen Frequenzen geht es irgendwann nicht mehr höher, im Ggs. zu playtone-simpleaudio.sh

# abgewandelt von https://unix.stackexchange.com/questions/1974/how-do-i-make-my-pc-speaker-beep

echo "playtone f=$1 d=$2"

ms=$(echo $2/1000 | bc -l)

# OLD:
# speaker-test --frequency $1 --test sine > /dev/null &
# pid=$!
# sleep $ms
# kill -9 $pid

# NEW:
sox -n tmp.wav synth $ms sin $1 && play tmp.wav
