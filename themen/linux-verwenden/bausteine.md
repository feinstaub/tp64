Linux-Bausteine
===============

loginctl
--------
Baustein: "Sessions"

Ctrl+Alt+F2

Ctrl+Alt+F7

    loginctl unlock-session 2

    sleep 30 && loginctl unlock-session 2

Was ist ein TTY?

* https://en.wikipedia.org/wiki/Teleprinter "(teletypewriter, Teletype or TTY)"
    * mit Bildern
    * mit Bild: https://askubuntu.com/questions/481906/what-does-tty-stand-for
* Text-Terminal: https://en.wikipedia.org/wiki/Computer_terminal#Text_terminals
    * http://www.linusakesson.net/programming/tty/index.php - "The TTY demystified"
* https://en.wikipedia.org/wiki/Tty_(unix)
    * "tty is a command in Unix and Unix-like operating systems to print the file name of the terminal connected to standard input."

### Crash the screen locker
* htop
* sort by PID
* scroll down
* select ALL processes of the screen locker with space
* immediately switch back to TTY 7
