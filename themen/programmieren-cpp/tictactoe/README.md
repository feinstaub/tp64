TicTacToe
=========

<!-- toc -->

- [Allgemeines](#allgemeines)
  * [Stop teaching C (at the beginning)](#stop-teaching-c-at-the-beginning)
- [Projekt-Struktur](#projekt-struktur)
  * [Zwei Varianten](#zwei-varianten)
  * [Namensräume](#namensraume)
- [TODO](#todo)
- [NCurses Music Machine (Qt, sox)](#ncurses-music-machine-qt-sox)
- [Optional](#optional)
- [Siehe auch / weitere](#siehe-auch--weitere)
  * [programming tasks](#programming-tasks)
  * [Pentago implementieren](#pentago-implementieren)

<!-- tocstop -->

Austausch: https://cryptpad.fr/code/#/2/code/edit/sAs3vqXY+HsepLz-QyusstSk/
Kompilieren:

Möglichkeit 1: g++ tictactoe.cpp && ./a.out
Möglichkeit 2: make && ./tictactoe

Allgemeines
-----------
### Stop teaching C (at the beginning)
* https://www.reddit.com/r/cpp/comments/g7u18r/cppcon_2015_kate_gregory_stop_teaching_c/, cppcon 2015
    * ...
    * (RAII)
    * start with string and cout
        * NOT: printf, char* strings, [] arrays, pointers (arithmetic, new and delete by hand)
            * do this later
        * NOT: sizes of ints and doubles; just let string appear as built-in type (it's a feature)
        * "+": just use it (operator overloading is intuitive)
        * if "Kate" then "hey I know you"
    * Show them using the debugger
    * vector
        * behave like objects
        * "a = b. What does it? Copy. Correct. Move on"
        * OO
        * algorithm
        * just use begin(), end(), do not discuss iterators yet
    * using namespace std on day 1 is ok
    * c++
        * objects and classes
        * operator overloading
        * templates
        * const
        * exceptions
        * std lib
    * <algorithm>
    * lambdas
        * e.g. count with loop, then count, then count_if (e.g. odd numbers)
        * find, find_if
        * sort
    * polymorphism
        * start with references (to avoid pointers, because of get rid of deref * or ->; to avoid compiler voodoo and put *s and &s everywhere until it compiles)
        * explain int& r = i; r = 7;
        * write a function that changes its arg
        * then Rect, NamedRect (with a name attrib and overloaded function), and then debug and see with and without attrib and call function
    * 29:15, const: good introduced by pass by reference
        * could not somebody change the real rect?! yes, thats why we use const
            * talk about intent: const is used to make intent clear (also talk about intent with hand-rolled for loops vs. find etc.)
    * scope
        * no heap yet => stack semantics lifetime
            * for normal modern C++ app programmer most things can happen on the stack (use string, use vector, use smartpointers)
        * focus on non-memory cleanup (e.g. close the file)
            * this is where C++ is good: the deterministic destruction of non-mem resources (vs. garbage collector)
        * destructors
        * choose examples carefully (because of maybe RVO)
        * next: exceptions
    * reasons to teach pointers
        * heap alloc
            * new/delete: 5 minutes (contrast stack semantics and RAII; contrast Java/C#), that is for library writers but not for us
                * declare that hard and old-fashioned way of working with the heap
                * shared_ptr<> and unique_ptr<>, make_unique
                    * gut, wenn man davor schon vector<> hatte
                    * gutes Beispiel für shared_ptr finden
                    * also unique_ptr als Ziel erklären, aber ein use case für shared_ptr finden, wo er die Ausnahme und nicht die Norm ist
                        * unique_ptr: "you cannot copy, only move. thats it"
                * ...
        * return nullptr as signal value
        * "pointers are kind of like references"
        * non-owning raw pointers are fine!
            * example ...
    * advanced: for library writers
        * new, delete
        * write and find memory leaks
        * etc.
    * makes C++ feel not that hard; makes course more fun
        * better modern C++ programmer
        * deficiencies in reading old code
    * for kids, C++ as first language is possible
        * "if you want the computer do stuff all over again, use a loop, this is how it looks like"
        * (https://libcinder.org ? (only macos and windows), make something shiny => use FLTK?)
        * learn a bit that C++/compilers are a dynamic environment
    * book? no, make the world better through indirection, so make the world better please :)

Projekt-Struktur
----------------
### Zwei Varianten
Es gibt zwei Varianten des Spiels

* 1. Variante: Einfach - Kommt ohne zusätzliche Bibliotheken aus; z. B. Clear terminal mit Escape-Sequenzen
    * **Basiert auf den Empfehlungen von Kate Gregory**
        * tictactoe.cpp
        * game.h
        * tttai.h
* 2. Variante: Ncurses - Verwendet Ncurses für Konsolenausgaben
* TODO: 3. Variante mit Qt

Beide Varianten teilen sich folgende Dateien:

* Makefile - zum Bauen
* game.h - Spiellogik
* tools.h - Nützliche Werkzeuge

So unterscheiden sich die Varianten:

* 1. Variante: Plain
    * nutzt tictactoe.cpp
* 2. Variante: mit Ncurses
    * nutzt tictactoe10.cpp und tencurses.cpp

### Namensräume
* namespace scratch (tictactoe.cpp)
    * Erklärungen einzelner C++-Konzepte

* namespace tools (tools.h)
    * ...

* namespace terminal (tools.h)
    *
    * ...

* game (game.h und tictactoe*.h)
    * ...

TODO
----
[ ] ncurses-Variante
    - getch und cursor-Steuerung
    - timeout (Balken läuft zurück)
    - Feld siehe ttt10/field.txt

[ ] siehe FLTK-MusicTyper

[ ] AI Teil 2

[ ] QT-Variante
    - mit QWindow -> Fullscreen
        - ...
        - game of life
        - labyrinth
        - ...

Optional
--------
[ ] Spielstand für Ctrl+C speichern, wollen Sie fortfahren?

[ ] punktestand

[ ] Gewinnstrecke blinken lassen

[ ] play_background_music

[ ] spieler können sich sound aussuchen

- https://de.wikibooks.org/wiki/Ncurses:_Was_fehlt
    - https://invisible-island.net/cdk/cdk.html
- https://en.wikipedia.org/wiki/Category:Software_that_uses_ncurses
    - ...
- http://www.etcwiki.org/wiki/Best_ncurses_linux_console_programs#Multitail
    - Multitail

Siehe auch / weitere
--------------------
### programming tasks
https://rosettacode.org/wiki/Category:Programming_Tasks
    https://rosettacode.org/wiki/Tic-tac-toe#C.2B.2B

### Pentago implementieren
* siehe z. B. https://perfect-pentago.net
