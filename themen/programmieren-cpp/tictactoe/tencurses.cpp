#include "tencurses.h"
//#include <ncurses/curses.h>
//#include <ncurses/form.h>       // FIELD, FORM
#include <curses.h>
#include <form.h>       // FIELD, FORM
#undef getch

namespace tc {

struct FormImpl
{
    FIELD  **fi;
    FORM   *fo;
    int     count = 0;

    FormImpl(const std::vector<Field>& fields)
    {
        count = fields.size();
        fi = (FIELD **)calloc(count + 1, sizeof(FIELD *));
        int i = 0;
        for (auto& f : fields) {
            fi[i] = new_field(f.h, f.w, f.y, f.x, 0, 0);
            i++;
        }
        fi[i] = 0; // required

        init_pair(2, COLOR_BLACK, COLOR_WHITE);

         for (i = 0; i < count; i++)
         {
            set_field_fore(fi[i], COLOR_PAIR(2));
            set_field_back(fi[i], COLOR_PAIR(2));
         }

        fo = new_form(fi);
        post_form(fo);
    }

    void wa_make_visible()
    {
        form_driver(fo, REQ_FIRST_FIELD);
        for (int i = 0; i < count; i++) {
            form_driver(fo, 'a');
            form_driver(fo, REQ_CLR_FIELD);
            form_driver(fo, REQ_NEXT_FIELD);
        }
        form_driver(fo, REQ_FIRST_FIELD);
    }

    ~FormImpl()
    {
        unpost_form(fo);
        free_form(fo);

        for (int i = 0; i < count; i++)
        {
            free_field(fi[i]);
        }

        free(fi);
    }
};

Form::Form(const std::vector<Field>& fields)
{
    impl = std::make_unique<FormImpl>(fields);
}

Form::~Form()
{
}

// TODO: das muss in die Main-App
//      TenCurses muss ein flacher Wrapper werden (curses.h einbinden und Konstanten und Funktionen verwenden können);
//            ncurses kann eh nicht in mehreren Instanzen laufen
int Form::driver(int c)
{
    switch (c)
    {
    case KEY_RIGHT:
    case '\n': // KEY_ENTER: so geht es nicht
    case '\t': // KEY_STAB:  so geht es nicht
        form_driver(impl->fo, REQ_NEXT_FIELD);
        break;

    case KEY_LEFT:
        form_driver(impl->fo, REQ_PREV_FIELD);
        break;

    default: /* Feldeingabe */
        form_driver(impl->fo, c);
    }

    return 0;
}

void Form::wa_make_visible()
{
    impl->wa_make_visible();
}

TenCurses::~TenCurses()
{
    if (!isendwin()) {
        ::endwin();
    }
}

void TenCurses::init()
{
    // https://stackoverflow.com/questions/29335758/using-kbhit-and-getch-on-linux
    ::initscr();
    ::start_color(); // "curses supports color attributes on terminals with that capability.  To use these routines start_color must be called, usually right after initscr"

    ::cbreak();   // disable line-buffering
    ::noecho();   // keine Zeichen, die getippt wurden, ausgeben
    keypad(stdscr, TRUE); // damit bei Form-Nutzung die Cursor-Tasten richtig funktionieren (z. B. c == KEY_RIGHT)

    // scrollok(stdscr, TRUE); // = ?

    // nodelay(stdscr, TRUE);  // The nodelay option causes getch to be a non-blocking call.
    // timeout(10);  // wait x milliseconds for input, siehe snacurses

    //::curs_set(0); // cursor verstecken

    //printf("Hallo"); // should not be used with ncurses active!
}

void TenCurses::endwin()
{
    ::endwin();
}

int TenCurses::refresh()
{
    return ::refresh();
}

int TenCurses::curs_set(int state)
{
    return ::curs_set(state);
}

void TenCurses::move(int y, int x)
{
    ::move(y, x);
}

void TenCurses::printw(const char *fmt...)
{
    va_list args;
    va_start(args, fmt);
    ::vw_printw(stdscr, fmt, args);
    va_end(args);
}

void TenCurses::printw(int y, int x, const char *fmt...)
{
    move(y, x);
    printw(fmt);
}

int TenCurses::getch()
{
    return ::wgetch(stdscr);
}

void TenCurses::napms(int ms)
{
    ::napms(ms);
}

Form& TenCurses::create_form(const std::vector<Field>& fields)
{
    form_ = std::make_unique<Form>(fields);
    refresh();
    return *form_;
}

} // namespace tc
