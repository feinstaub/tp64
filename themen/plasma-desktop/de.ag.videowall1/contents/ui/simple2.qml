import QtQuick 2.0
// für den MediaPlayer- und VideoOutput-Typ:
import QtMultimedia 5.0

Rectangle {
    color: "#000000"

    MediaPlayer {
        // Die ID (= Identifizier = Bezeichner dieses Objekts)
        id: mediaplayer1

        // Pfad zu dem Video, das abgespielt werden soll:
        source: "videos/video2.ogv"

        // Das Video soll sofort anfangen:
        autoPlay: true

        // Das Video soll unendlich wiederholt werden:
        loops: MediaPlayer.Infinite

        // Die Lautstärke soll Null sein (1.0 ist Maximum)
        volume: 0.0
    }

    // Das MediaPlayer-Objekt spielt die Videodatei ab, aber
    // es muss auch irgendwo angezeigt werden.
    // Stell dir den MediaPlayer wie einen DVD-Spieler vor
    // und das folgende VideoOutput-Objekt wie einen Bildschirm.
    // Mit der ID werden Abspielgerät und Bildschirm verbunden.
    VideoOutput {
        // Die ID des MediaPlayer-Objekts
        source: mediaplayer1
    }
}
