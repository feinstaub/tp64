openSUSE Tumbleweed Probleme und Lösungen
=========================================

<!-- toc -->

- [2018: Windows-10-Rechner bootet nicht vom Stick, obwohl Installation geklappt hat](#2018-windows-10-rechner-bootet-nicht-vom-stick-obwohl-installation-geklappt-hat)
- [2018: 32-Bit-Rechner: Bild flackert](#2018-32-bit-rechner-bild-flackert)
- [2017-2018: Tumbleweed von einem anderen Stick installiert - Starten hängt über 2 Minuten](#2017-2018-tumbleweed-von-einem-anderen-stick-installiert---starten-hangt-uber-2-minuten)
- [2018: Schriften viel zu klein auf HiDPI-Bildschirm](#2018-schriften-viel-zu-klein-auf-hidpi-bildschirm)
- [2017-2018: Zwei Tastatursprachen-Symbole neben der Uhr (ibus)](#2017-2018-zwei-tastatursprachen-symbole-neben-der-uhr-ibus)
- [2018: Nach mehreren Monaten während eines Upgrades plötzlich Dateisystem](#2018-nach-mehreren-monaten-wahrend-eines-upgrades-plotzlich-dateisystem)
  * [Snapper deaktiveren](#snapper-deaktiveren)
  * [Analyse wegen /.snapshots](#analyse-wegen-snapshots)
- [2017: Tumbleweed von DVD installiert - cd:///?devices](#2017-tumbleweed-von-dvd-installiert---cddevices)

<!-- tocstop -->

* siehe auch [README.md](README.md)

2018: Windows-10-Rechner bootet nicht vom Stick, obwohl Installation geklappt hat
---------------------------------------------------------------------------------
* Lösung: Schnellstart deaktivieren, siehe ["Windows 10: Schnellstart deaktivieren - so geht's"](https://praxistipps.chip.de/windows-10-schnellstart-deaktivieren-so-gehts_50919)


2018: 32-Bit-Rechner: Bild flackert
-----------------------------------
* Lösung: trotzdem einloggen. Dann die Einstellungen für "Compositor" starten
* "Tearing Prevention" auf "Never" oder "Nie" stellen (NICHT auf Automatisch):

![](img-compositor-prevent-flickering.png)


2017-2018: Tumbleweed von einem anderen Stick installiert - Starten hängt über 2 Minuten
----------------------------------------------------------------------------------------
* Problem: das Booten dauert über 2 Minuten
    * Diagnose: Esc drücken
        * es erscheint eine Meldung ähnlich dieser: "a start job is running for dev-sdc1.device"
* Lösung siehe [Forum](https://forums.opensuse.org/showthread.php/520153-a-start-job-is-running-for-dev-sdc1-device-after-plugging-off-old-harddisc)

Schritt 1 - Gerätenamen der Root-Partition rausfinden:

    $ df | grep /$
    /dev/sda1       63488000  38409860  24270188  62% /

In diesem Fall /dev/sda1.

Schritt 2 - Boot Loader konfigurieren

* KRunner -> Boot Loader -> Kernel Parameters
* Den Wert bei 'resume=' überprüfen. Der Wert beginnt entweder mit '/dev/disk/by-uuid/...' (das ist ok) oder ist '/dev/sdc1' oder ähnlich.
    * Im zweiten Fall den Wert auf den Gerätenamen aus Schritt 1 abändern.

Schritt 3 - Neustarten

Neustarten, um zu schauen ob es geklappt hat.


2018: Schriften viel zu klein auf HiDPI-Bildschirm
--------------------------------------------------
* Bugs:
    * [Add "Display Configuration" to Desktop toolbox and/or desktop context menu](https://bugs.kde.org/show_bug.cgi?id=355679)
        * "Scale Display"
    * [Make Desktop toolbox also available in System Tray](https://bugs.kde.org/show_bug.cgi?id=389249)

2017-2018: Zwei Tastatursprachen-Symbole neben der Uhr (ibus)
-------------------------------------------------------------
    $ sudo zypper remove ibus

Und dann ggf. die Tastatursprache via Plasma konfigurieren.

Mögliche Reports:

* https://bugs.kde.org/show_bug.cgi?id=339270 - KDE doesn't automatically launch the input method framework(e.g. ibus-daemon)
    * not reported yet: "A related observation: on openSUSE Tumbleweed ibus gets installed by default. Since it interferes with the Plasma keyboard layout settings (I have two configure: US and DE), I always remove ibus which solves the problems. It is inconvenient though that ibus will be added again when doing a dist upgrade."
* https://bugs.kde.org/show_bug.cgi?id=341314 - Plasma 5 somehow falls back to US keyboard after startup
    * recommend uninstall
* related
    * https://bugs.kde.org/show_bug.cgi?id=342979 - IBus is shown even when set to "Hidden" in "Entries" settings panel
    * https://bugs.kde.org/show_bug.cgi?id=345037 - konsole integration with ibus input broken


2018: Nach mehreren Monaten während eines Upgrades plötzlich Dateisystem
------------------------------------------------------------------------
### Snapper deaktiveren
* siehe
    * http://linux-club.de/wiki/opensuse/Tipp:_Snapper_entsch%C3%A4rfen
    * https://forums.opensuse.org/showthread.php/516766-Is-it-safe-to-simply-uninstall-snapper (feature of btrfs)

* Hier wird snapper konfiguriert: /etc/snapper/configs/root

* Ausprobieren:

```
# fraction of the filesystems space the snapshots may use
SPACE_LIMIT="0.5"
```

Den Standartwert von 0.5 auf 0.1 (bei Root-Partitionsgröße von 10 GB / USB-Stick) oder 0.3 (bei Root-Partitionsgröße von 60 GB) umstellen.

z. B. mit $ sudo nano /etc/snapper/configs/root

### Analyse wegen /.snapshots

* Bekanntes Problem
    * https://forums.opensuse.org/showthread.php/521290-snapshots-are-filling-root-directory-up - ".snapshots are filling root directory up"
* Clear all Snapper snapshots - https://unix.stackexchange.com/questions/181621/clear-all-snapper-snapshots

Wieviel verbrauchen die Snapshots? (https://unix.stackexchange.com/questions/185764/how-do-i-get-the-size-of-a-directory-on-the-command-line)

    $ sudo du -sh /.snapshots/
    393G    /.snapshots/

Das scheint nicht die richtige Größe zu sein, weil die /-Partition nur ca. 60 GB groß ist.

OFFEN: Wie findet man heraus wieviel Platz ein Snapshot verbraucht. In YAST-Snapper wird es auch nicht angezeigt.

Ähnliche Meldungen:

* ["Auto cleanup old snapshots on lack of free space"](https://github.com/openSUSE/snapper/issues/50)

* https://www.suse.com/documentation/sles11/stor_admin/data/trbl_btrfs_volfull.html
    * "If Snapper is running for the Btrfs file system, the "No space left on device" problem is typically caused by having too much data stored as snapshots on your system."
    * ...
    * $ snapper -c root list
    * $ snapper -c root delete #
        * "Ensure that you delete the oldest snapshots first. The older a snapshot is, the more disk space it occupies."

Lösche alles außer das hier:

$ sudo snapper -c root list
Type   | # | Pre # | Date                        | User | Cleanup | Description           | Userdata
-------+---+-------+-----------------------------+------+---------+-----------------------+---------
single | 0 |       |                             | root |         | current               |
single | 1 |       | Mi 08 Mär 2017 21:12:56 CET | root |         | first root filesystem |

    $ df
    Ergebnis: 96% belegt, statt 100%, 3 GB frei. Das ist noch zuwenig. (siehe unten, man musste wohl noch warten!)

    $ sudo snapper -c root delete 0
    Illegal snapshot.

    $ sudo snapper -c root delete 1
    Deleting snapshot failed.

    $ sudo du -sh /.snapshots/
    58G     /.snapshots/

    1-2 min WARTEN

    $ df
    Ergebnis: 46% belegt. Nun 33 GB frei!


2017: Tumbleweed von DVD installiert - cd:///?devices
-----------------------------------------------------
* Probleme beim Installieren neue Software
    * Lösung: Repository "cd:///?devices..." entfernen; es wird nicht mehr benötigt
