KDE-Plasma-Desktop
==================

<!-- toc -->

- [KRunner](#krunner)
  * [Starten](#starten)
  * [Rechnen](#rechnen)
- [Zwischenablage](#zwischenablage)
- [Hintergrundbilder](#hintergrundbilder)
  * [Neue Hintergrundbilder finden](#neue-hintergrundbilder-finden)
  * [Eine einfache Farbe einstellen](#eine-einfache-farbe-einstellen)
  * [Ein Hintergrundbild selber malen](#ein-hintergrundbild-selber-malen)
  * [Ein animiertes Hintergrundbild selber gestalten](#ein-animiertes-hintergrundbild-selber-gestalten)
  * [Weitere](#weitere)
  * [Fragen](#fragen)
- [Scrollen](#scrollen)
  * [Scrollen auf dem Lautsprechersymbol](#scrollen-auf-dem-lautsprechersymbol)
- [Visuelle Effekte](#visuelle-effekte)
  * [Virtuelle Desktops](#virtuelle-desktops)
- [Prozess-Monitor: KSysGuard](#prozess-monitor-ksysguard)
  * [Häufige Fragen](#haufige-fragen)
  * [Bugs / Wishes](#bugs--wishes)
- [Scripting / Programmieren](#scripting--programmieren)
  * [Benachrichtigungen per Script](#benachrichtigungen-per-script)
  * [Programmier-Vorlagen mit KAppTemplate](#programmier-vorlagen-mit-kapptemplate)
- [Netzwerkmonitor KNemo](#netzwerkmonitor-knemo)
- [System Monitor Desktop-Applet](#system-monitor-desktop-applet)
- [Fehler gefunden? -> Report Bug](#fehler-gefunden---report-bug)
- [Verschiedenes / News / Planet KDE](#verschiedenes--news--planet-kde)
  * [Plasma Pass](#plasma-pass)
  * [Plasma’s Desktop Scripting API](#plasmas-desktop-scripting-api)
  * [Plasma Startup](#plasma-startup)
  * [Declarative Widgets - adding Qt Widgets support to QML](#declarative-widgets---adding-qt-widgets-support-to-qml)
  * [Kali Linux](#kali-linux)

<!-- tocstop -->

KRunner
-------
### Starten
* Normalerweise öffnet sich mit **Alt+Leertaste** oben die krunner-Leiste z. B. zum Starten von Programmen.
* Wenn der Desktop angezeigt wird (also alle Fenster minimiert sind), dann kann man **einfach lostippen** (z. B. "firefox") und die krunner-Leiste erscheint automatisch.

### Rechnen
* **KCalc** ist ein Taschenrechner, den man verwenden kann.
* Mit KRunner kann man sich den Weg über das Aufrufen des Taschenrechners sparen und bekommt das Ergebnis direkt angezeigt.
    * z. B. "5 * 25" eingeben (ohne Anführungszeichen) und das Ergebnis 125 wird direkt angezeigt.
    * plus: +, minus: -, mal: *, geteilt: /

Zwischenablage
--------------
Es gibt zwei Zwischenablagen:

* Die mit Strg+C und Strg+V
* Die mit der mittleren Maustaste
* Programmierung
    * Mit dem Programm **xclip** kann man in eigenen Programmen mit der Zwischenablage interagieren, [https://scribblesandsnaps.com/2016/12/09/geotagging-in-digikam-with-a-lazy-bash-script/ siehe z. B. hier].

Hintergrundbilder
-----------------
### Neue Hintergrundbilder finden
* Rechte Maustaste auf Desktop -> Einstellungen
* dann entweder
    * Eingebaute Hintergrundbilder ausprobieren
    * oder"Get new wallpapers"
    * oder eigene Internetrecherche
        * z. B. http://www.vladstudio.com (vladstudio)

### Eine einfache Farbe einstellen
* Plain color -> Pick screencolor

### Ein Hintergrundbild selber malen
* Kolourpaint
* GIMP
* Inkscape

### Ein animiertes Hintergrundbild selber gestalten
* [animiertes-hintergrundbild-mit-qml.md](animiertes-hintergrundbild-mit-qml.md)
    * 1. Bildquelle: https://www.opensuse.org/searchPage/assets/videos/open-suse-welcome.gif
    * 2. de.ag.hintergrundbild1 unter ~/.local/share/plasma/wallpapers/ plazieren
    * 3. Plasma-Neustart nicht nötig (aber bei nachträglichen Änderungen)
    * Quelle: https://forum.kde.org/viewtopic.php?f=289&t=131783

* qml-video-hintergrund.md

* Mehr zu QML siehe

### Weitere
* Weltkarte, Bild des Tages, Diashow
* Spezialbilder Haenau, Hunyango

### Fragen
* Wie kann man bei einem vorhandenen Hintergrundbild sehen, wo auf der Festplatte es liegt?
    - seit Plasma 5.11.0 gibt es eine Funktion dafür, siehe https://bugs.kde.org/show_bug.cgi?id=371247 (weil diese Funktion vorher von einem Benutzer als wünschenswert gemeldet wurde)

Scrollen
--------
Unter Plasma gilt: da wo sich der Mauszeiger befindet, während man scrollt, ist das Scrollziel. Man muss also nicht extra klicken.

### Scrollen auf dem Lautsprechersymbol
* Einfach über den Lautsprechersymbol scrollen (kein Klick nötig) verändert die Lautstärke.

Visuelle Effekte
----------------
### Virtuelle Desktops
* ...
* Animationseffekte: ![](img/desktop-cube-animation.png)

Prozess-Monitor: KSysGuard
--------------------------
* Drücke **Strg+Esc** um den System-Monitor zu öffnen. Damit kannst du unter anderem jedes Programm suchen und beenden ("kill").

### Häufige Fragen
* Warum z. B. nur 25 % statt 100 %? (Tipp: verwende **ksysguard**, um herauszufinden, wieviele Prozessor-Kerne deine CPU hat.

### Bugs / Wishes
* ["Add KSysGuard to Desktop toolbox"](https://bugs.kde.org/show_bug.cgi?id=389251)
* ["Tray and taskbar icon should show cpu usage"](https://bugs.kde.org/show_bug.cgi?id=152608)

Scripting / Programmieren
-------------------------
### Benachrichtigungen per Script
* Mit dem Programm '''notify-send''' kann man in eigenen Programmen Benachrichtigungen anzeigen.

Beispiel 1:

    $ notify-send "Hallo das ist ein Test"

Beispiel 2 - zeige die Nachricht nur 1 Sekunde (= 1000 Millisekungen) lang:

    $ notify-send "Hallo das ist ein Test" -t 1000

Beispiel 3 - zeige ein Icon, eine Zusammenfassung und einen ausführlicheren Text:

    $ notify-send -i colors-luma Zusammenfassung "Ausführlicher Text"

Welche Icons es gibt, sieht man hier: $ ls /usr/share/icons/breeze/actions/32/

### Programmier-Vorlagen mit KAppTemplate
* https://www.kde.org/applications/development/kapptemplate/

Welche Vorlagen gibt es?

    r@linux:/usr/share/kdevappwizard/templates> ls
    cmake_kdevplugin.tar.bz2  cpp-plasmoid.tar.bz2            kde-frameworks5.tar.bz2     plasma-wallpaper-with-qml-extension.tar.bz2  qt5-qml2.tar.bz2
    cmake_plaincpp.tar.bz2    empty.tar.bz2                   kpartsapp.tar.bz2           qmake_qt5guiapp.tar.bz2                      runner.tar.bz2
    cmake_qt5guiapp.tar.bz2   ion-dataengine.tar.bz2          ktexteditor-plugin.tar.bz2  qml-plasmoid.tar.bz2
    cmake_qt5-qml2.tar.bz2    kde-frameworks5-simple.tar.bz2  plasma-wallpaper.tar.bz2    qml-plasmoid-with-qml-extension.tar.bz2

Wo kommen diese Vorlagen her (denn im Code - https://cgit.kde.org/kapptemplate.git/tree/ - sind sie nicht)?

    r@linux:/usr/share/kdevappwizard/templates> for a in $(ls) ; do echo "$a --> $(rpm -qf $a)"   ; done
    cmake_kdevplugin.tar.bz2 --> kdevplatform-5.2.3-1.2.x86_64
    cmake_plaincpp.tar.bz2 --> kdevplatform-5.2.3-1.2.x86_64
    cmake_qt5guiapp.tar.bz2 --> kdevplatform-5.2.3-1.2.x86_64
    cmake_qt5-qml2.tar.bz2 --> kdevplatform-5.2.3-1.2.x86_64
    cpp-plasmoid.tar.bz2 --> plasma-framework-devel-5.47.0-1.2.x86_64
    empty.tar.bz2 --> kdevplatform-5.2.3-1.2.x86_64
    ion-dataengine.tar.bz2 --> plasma5-workspace-devel-5.13.1-1.1.x86_64
    kde-frameworks5-simple.tar.bz2 --> kapptemplate-18.04.2-1.1.x86_64
    kde-frameworks5.tar.bz2 --> kapptemplate-18.04.2-1.1.x86_64
    kpartsapp.tar.bz2 --> kparts-devel-5.47.0-1.1.x86_64
    ktexteditor-plugin.tar.bz2 --> ktexteditor-5.47.0-1.1.x86_64
    plasma-wallpaper.tar.bz2 --> plasma-framework-devel-5.47.0-1.2.x86_64
    plasma-wallpaper-with-qml-extension.tar.bz2 --> plasma-framework-devel-5.47.0-1.2.x86_64
    qmake_qt5guiapp.tar.bz2 --> kdevplatform-5.2.3-1.2.x86_64
    qml-plasmoid.tar.bz2 --> plasma-framework-devel-5.47.0-1.2.x86_64
    qml-plasmoid-with-qml-extension.tar.bz2 --> plasma-framework-devel-5.47.0-1.2.x86_64
    qt5-qml2.tar.bz2 --> kdevplatform-5.2.3-1.2.x86_64
    runner.tar.bz2 --> krunner-devel-5.47.0-1.1.x86_64

Sonstiges

* TODOs: https://techbase.kde.org/Projects/KAppTemplate/TODO
    * "Move the functionality into a shared library that can be used from KAppTemplate and KDevelop (to remove duplicated code)."
* Siehe auch KDevelop -> New from Template, bugs und wishes --> Vorlagen

Netzwerkmonitor KNemo
---------------------
* https://www.kde.org/applications/internet/knemo/

System Monitor Desktop-Applet
-----------------------------
* Simple System Monitor
    * https://store.kde.org/p/1173509/
    * https://github.com/dhabyx/plasma-simpleMonitor

Fehler gefunden? -> Report Bug
------------------------------
* https://bugs.kde.org besuchen

* EINMALIG: Oben in der Leiste "New Account" klicken
    * Mit einer E-Mail-Adresse registrieren. Nicht mit der privaten Hauptadresse, sondern mit einer eigenen spezielle für öffentliche Sachen.
    * ggf. Name / Pseudonym vergeben

* Einen Bug für z. B. "Touchpad - System Setting Module" einstellen
    * Oben in der Leiste auf "New"
    * Relativ weit unten das Produkt "Touchpad-KCM" anklicken.
        * Welches Produkt man bei welchem Fehler genau auswählen muss, ist Erfahrungssache. Ist nicht immer einfach herauszufinden. Im Zweifel "plasmashell" auswählen.
    * Bei Component "kcm" auswählen.
    * Version auswählen, falls bekannt.
    * Summary eingeben (auf Englisch)
    * Es wird eine Liste mit möglichen bereits vorhandenen Bugs angezeigt. Dort schauen, ob der eigene Fehler möglicherweise schon vorhanden ist.
        * Wenn ja, muss man den Bug nicht nochmal melden. Man kann aber bei dem vorhandenen dazuschreiben, dass man denselben Fehler hat.
    * Description: genau beschreiben, wie man den Bug reproduzieren kann (auf Englisch).

Verschiedenes / News / Planet KDE
---------------------------------
### Plasma Pass
* 2018: https://www.dvratil.cz/2018/05/plasma-pass/

### Plasma’s Desktop Scripting API
* https://medium.com/kdeok/how-to-create-a-theme-that-looks-and-feels-like-unity-using-plasmas-desktop-scripting-api-efe33d36bec6
    * "How to create a theme that looks and feels like Unity using Plasma’s Desktop Scripting API"

### Plasma Startup
* http://blog.davidedmundson.co.uk/blog/plasma-startup/
    * Why is startup slow?
    * Profiling
    * systemd

### Declarative Widgets - adding Qt Widgets support to QML
* https://www.kdab.com/declarative-widgets/

### Kali Linux
* 2017: ["Make your Kali Linux Supercool using KDE Plasma Ghost Theme"](https://www.youtube.com/watch?v=bmoT2Z-aBpk)
