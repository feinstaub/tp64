#
# https://audaspace.github.io/bindings/tutorials.html
#

import aud, time
device = aud.Device()

def play1():
    sine = aud.Sound.sine(440)
    sine2 = aud.Sound.sine(640)
    square = sine.threshold()
    square2 = sine2.threshold()

    handle = device.play(square)
    time.sleep(0.5)
    handle.pitch = 0.5
    time.sleep(2.0)
    handle.pitch = 1.0
    handle.pause()
    handle2 = device.play(square2)
    time.sleep(0.5)
    handle2.pause()
    handle.resume()
    time.sleep(0.5)
    handle2.resume()
    time.sleep(0.5)

def play2():
    # https://github.com/KOBUGE-Games/minilens/blob/master/audio/main_menu.ogg
    sound = aud.Sound.file('/run/media/manjaro/76df7532-ed17-4937-a9bd-236c8818a3d6/gregor/dev/godot/minilens/audio/main_menu.ogg')
    handle = device.play(sound)
    time.sleep(1.0)
    handle2 = device.play(sound) # 1 Sekunde versetzt gleichzeitig abspielen
    
    while handle.status:
        time.sleep(0.1)
        
#play1()
play2()

# todo:
#   siren with location,
#   tetris with parseNote!
#   Here are the examples: https://github.com/audaspace/audaspace/tree/master/bindings/python/examples
