#include <iostream>
#include <vector>
#include <algorithm>
#include <unistd.h>

using namespace std;

void sleep_ms(int ms)
{
    usleep(ms * 1000);
}

void clear()
{
    // https://en.wikipedia.org/wiki/ANSI_escape_code#Unix-like_systems
    cout << "\033[2J\033[H";
}

string farbe(string s)
{
    return "\033[34;107m" + s + "\033[0m";
}

void play_sound(const string& file, bool blocking = true)
{
    string cmd = "play " + file;
    if (!blocking) {
        cmd += " &";
    }
    system(cmd.c_str());
}

/**
 * https://de.wikipedia.org/wiki/Frequenzen_der_gleichstufigen_Stimmung
 * A = 440 Hz
 *
 * bessere Version siehe ncurses-pianotyper
 */
void play_note(double sec, int freq)
{
    string cmd = "sox -n tmp.wav synth " + to_string(sec)
        + " sin " + to_string(freq) + " && play tmp.wav 2> /dev/null";
    cout << cmd << endl;
    system(cmd.c_str());
}

int main()
{
    cout << "Hallo\n";

    vector<int> v = { 1, 2, 3, 4 };
    v.push_back(5);

    reverse(v.begin(), v.end());
    for (int e : v) {
        cout << "e = " << e << "\n";
    }

    vector<string> sv;
    sv.push_back("Hallo");
    sv.push_back("Test");
    sv.push_back("Back");

    for (string e : sv) {
        cout << e << "\n";
    }

    vector< vector<string> > map;
    // "a", "b", "c"
    // "d", "e", "f"
    map.push_back({ "a", "b", "c" });
    map.push_back({ "d", "e", "f" });
    cout << map[0][0] << endl; // a
    cout << map[0][1] << endl; // b
    cout << map[0][2] << endl; // c
    cout << map[1][2] << endl; // f

    sleep_ms(1000);
    //clear();
     for (int i = 0; i < 5; i++) {
//         //play_sound("sounds/Touch.wav");
         play_note((6 - i) * 0.05, (i + 1) * 100);
//         //sleep_ms(100);
     }
    //cout << "Test" << farbe("Hallo") << "Stop\n";
//    sleep_ms(1000);
    return 0;
}
