Projekt Oma-Linux
=================

Selber GNU/Linux lernen geht am besten, wenn man die Computer von anderen, die Hilfe brauchen, damit ausstattet und regelmäßig wartet. Da Omas (und Opis) in ihrer Rolle als User sich oft mit Windows sowieso nicht so gut auskennen, sind sie perfekte Kandidaten für GNU/Linux:

* Die **Bedienung ist einfach**, intuitiv und die ändert sich nicht dauernd.
* Es gibt **weniger Fallen**, in die man tappen kann. Zum Beispiel keine systemseitigen Werbeangebote und andere Klickfallen.
* Die **Privatsphäre** ist standardmäßig gewahrt.
* **Bessere Sicherheit** (siehe BSI-Auswertung zum Thema Virenscanner)

Folgendes hilft bei der Installation:

* openSUSE Tumbleweed installieren, [auf Stick](../themen/linux-installieren/opensuse-auf-usb-stick) oder am besten direkt auf Festplatte
    * (vorher selber ausprobieren und eine Weile damit arbeiten, um Erfahrung zu sammeln)
* Administration:
    * siehe z. B. codestruct scripts
* Erklärungen, warum eigentlich Linux:
    * siehe z. B. "Linux User im Bereich der Kirchen e.V."
        * https://luki.org/argumente/warum-linux, https://luki.org/uber-luki, https://digitale-nachhaltigkeit.net etc.

Tipps:

* **Diashow** von schönen Bildern **auf dem Login-Bildschirm**
    * KRunner -> **Screen Locking** -> Wallpaper -> Slideshow -> Ordner mit Omas Lieblingsbildern auswählen :-)

Bekannte Fehler und Baustellen, die man angehen kann:

* 2017-04:
    * BUG: Drag&Drop Icon to desktop does not respect drop position
        * fixed with Plasma 5.10?
    * WISH: Meta+L should trigger user log out
        * todo: link to bug report

