#pragma once

#include <iostream>
#include <stdio.h>      // getchar
#include <ctime>        // std::time
#include <unistd.h>     // fork, execl, usleep
#include <fcntl.h>      // open
#include <pwd.h>        // getpwuid
#include <sys/stat.h>   // mkdir
#include <errno.h>
#include <termios.h>    // termios, tcgetattr, tcsetattr
//#include <fcntl.h>      // fcntl
#include <unistd.h>  // STDIN_FILENO

namespace tools
{
void random_init()
{
    std::srand(std::time(nullptr));
}

void sleep_ms(int ms)
{
    usleep(ms * 1000);
}

bool starts_with(const std::string& s, const std::string& needle)
{
    return s.rfind(needle) == 0;
}

/**
 * https://www.learncpp.com/cpp-tutorial/5-10-stdcin-extraction-and-dealing-with-invalid-text-input/
 */
int readInt(std::string prompt)
{
    while (true) // Loop until user enters a valid input
    {
        std::cout << prompt;
        int x;
        std::cin >> x;

        if (std::cin.fail()) // has a previous extraction failed?
        {
            // yep, so let's handle the failure
            std::cin.clear(); // put us back in 'normal' operation mode
            std::cin.ignore(32767,'\n'); // and remove the bad input
        }
        else // else our extraction succeeded
        {
            std::cin.ignore(32767, '\n'); // clear (up to 32767) characters out of the buffer until a '\n' character is removed
            return x; // so return the value we extracted
        }
    }
}

void terminal_clear(bool include_scrollback = false)
{
    // https://stackoverflow.com/questions/4062045/clearing-terminal-in-linux-with-c-code
    // -> https://en.wikipedia.org/wiki/ANSI_escape_code#Unix-like_systems
    //    https://stackoverflow.com/questions/6486289/how-can-i-clear-console

    // \x1B == \033 == 27
    std::cout << "\x1B[2J\x1B[H"; // 2J = Clear Screen, H = move curser to the top-left corner
}

/**
 * tt = terminal text
 *
 * color: 32: grün, 34: blau
 * weitere siehe https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
 * TODO: Background türkis
 */
std::string tt_bold_color(const std::string& text, int color)
{
    return std::string("\033[1;") + std::to_string(color) + "m" + text + "\033[1;0m";
}

/**
 * siehe auch tictactoe/b4.cpp
 */
struct terminal_noncanoical_guard
{
    terminal_noncanoical_guard(bool echo = true)
    {
        termios newt;

        tcgetattr(STDIN_FILENO, &oldt);
        newt = oldt;
        newt.c_lflag &= ~ICANON;
        if (!echo) {
            newt.c_lflag &= ~ECHO;
        }
        tcsetattr(STDIN_FILENO, TCSANOW, &newt);

//         while(1)
//         {
//             char ch = getchar(); // stdio.h
//             pw += ch;
//             cout << "*";
//             if (ch == '\033')
//             { printf("Arrow key\n"); ch=-1; break;}
//             else if(ch == -1) // by default the function returns -1, as it is non blocking
//             {
//             continue;
//             }
//         }
    }

    ~terminal_noncanoical_guard()
    {
        tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
    }

private:
    termios oldt;
};

} // namespace tools
