import QtQuick 2.0

Block {
    property bool isAlive: true

    color1: {
        if (isAlive) return "green"
        else return "blue"
    }
}
