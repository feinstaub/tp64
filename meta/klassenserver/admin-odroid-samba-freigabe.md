ODROID Samba-Freigabe
=====================

Samba-Freigabe
--------------
siehe http://ardupilot.org/dev/docs/odroid-wifi-access-point-for-sharing-files-via-samba.html
    (interessant auch: Wifi Access Point einrichten)

* Login mit ag
* $ mkdir -p samba_share/public

$ sudo nano /etc/samba/smb.conf

Am Ende einfügen:

    [public]
    path = /home/ag/samba_share/public
    browsable = yes
    writable = no
    guest ok = yes
    read only = yes

Das Share ausprobieren:

* Auf Windows
    * \\ag-pc
    * oder: \\10.2.133.168
    * (oder: \\192.168.178.33)
    * Es muss ein public-Ordner da sein
Auf Linux:
    * smb://ag-pc/
        * ggf. 10.2.133.168 in die /etc/hosts eintragen

