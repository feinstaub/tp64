Meta
====

HOWTO Bearbeiten
----------------
* EINMALIG: [grip und markdown-toc installieren](../anleitungen/markdown-primer)
* `$ ./run_offline_grip &` im root-Verzeichnis starten
* Mit `$ markdown-toc -i README.md` TOC neu generieren
* Darauf achten, dass vor einer Aufzählung eine Leerzeile ist, sonst rendert der Offline-Renderer nicht richtig (dem Github-Flavour ist das egal)

Evaluation von Dokumentationssystemen
-------------------------------------
* [Dokumentationssysteme](dokumentationssysteme)

Lehrmaterial / Blogs
--------------------
* [Sammlung von potentiellem Lehrmaterial](../weitere/lehrmaterial)

Sonstige Inhalte
----------------
* [$ ./create-cowsay-dialogs](create-cowsay-dialogs)

Offline-Verwendung
------------------
* todo: tp64 offline verwenden und vor allem durchsuchen
* IDEE: für zentralen Server:
    - HTTP-Markdown-Server
        - ggf. mit "Serve Home Folder", damit jeder auf seinen Home-Folder zugreifen kann, ggf. "geheime" URL
        - eigene Scripte triggern plus Ausgabe
        - Nachteil: bestimmte Dinge (wie Audio) sind mit einem eigenen Rechner einfach besser zu machen

Klassenregeln
-------------
* [Klassenregeln](klassenregeln.md)

Jugendschutz
------------
* todo: gibt es eine Browser-Erweiterung für individuellen clientseitigen Schutz?
