// wordwall BREITE HÖHE [ Wort1 Wort2 Wort3 ... ]
// wordwall --ncurses [ Wort1 Wort2 Wort3 ... ]

// Referenz:
// ITEM: main               - https://en.cppreference.com/w/cpp/language/main_function
// ITEM: printf
// ITEM: std::string
// ITEM: std::vector
// ITEM: std::atoi          - kein Fehlercheck, https://en.cppreference.com/w/cpp/string/byte/atoi
// ITEM: std::stoi          - wirft Exception bei Fehler, https://en.cppreference.com/w/cpp/string/basic_string/stol
// ITEM: std::rand          - https://en.cppreference.com/w/cpp/numeric/random/rand
// ITEM: Datei einlesen C++ - https://de.wikibooks.org/wiki/C-Programmierung:_Dateien
// ITEM: Datei einlesen C   - http://www.willemer.de/informatik/cpp/fileop.htm

// TODO:
    // read file: read words from word list
    // read line by line
    // pick random
    // pick by length to fill
    // pick by case (Großschreibung)

/**
 * less /usr/share/dict/words  --> Englische Worte, auch Großbuchstaben, aber auch mit Apostroph (')
 *
 * Deutsche Wörter:
 *  $ sudo zypper install aspell-de
 *  welche Dateien?
 *      $ rpm -ql aspell-de
 *
 * $ aspell -l de dump master > words-de.txt
 * ggf. $ aspell -l de dump master | iconv -f ISO_8859-16 -t utf-8 > words-de.txt
 *
 * cat words-de.txt | wc -l
 * 312068
 *
 * Order by length:
 *  $ cat de.txt | perl -e 'print sort { length($a) <=> length($b) } <>' | less
 */

#include <cstdio>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
// #include <stdlib.h>
// #include <ncurses.h>
// #include <chrono>
// #include <cstring>
// #include <queue>

void main_noncurses(int w, int h, const std::vector<std::string>& words)
{
    printf("TODO non curses\n");
    printf("Words: ");
    for (const std::string& w : words) {
        std::cout << w << " ";
    }
    printf("\n");
    for (int y = 0; y < h; y++) {
        for (int x = 0; x < w; x++) {
            printf("X");
        }
        printf("\n");
    }

    std::fstream f;
    std::string s;
    f.open("words-de.txt", std::ios::in);
    while (!f.eof())
    {
        getline(f, s);
        printf("%s\n", s.c_str());
    }
    f.close();

    printf("\n");
}

void main_curses(const std::vector<std::string>& words)
{
    printf("TODO ncurses\n"); // Farben etc.
    printf("Words: ");
    for (const std::string& w : words) {
        std::cout << w << " ";
    }
    printf("\n");
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Zuwenig Optionen\n");
        return 1;
    }

    if (std::string(argv[1]) == "--ncurses") {
        std::vector<std::string> words { argv + 1, argv + argc };
//         for (int i = 1; i < argc; i++) {
//             printf("%s\n", argv[i]);
//             words.push_back(argv[i]);
//         }
        main_curses(words);
    } else {
        int w;
        int h;

        if (argc < 3) {
            printf("Zuwenig Optionen\n");
            return 1;
        }

        w = std::stoi(argv[1]);
        h = std::stoi(argv[2]);
        std::vector<std::string> words { argv + 3, argv + argc };

        main_noncurses(w, h, words);

    }
    return 0;
}
