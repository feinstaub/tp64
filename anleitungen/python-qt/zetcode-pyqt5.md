zetcode.com/gui/pyqt5
=====================

<!-- toc -->

- [Einleitung](#einleitung)
- [Erste kleine Programme](#erste-kleine-programme)
- [Menüs und Toolbars](#menus-und-toolbars)
- [Layout management](#layout-management)
- [Events und Signale](#events-und-signale)
- [Dialoge](#dialoge)

<!-- tocstop -->

http://zetcode.com/gui/pyqt5/

Weil das Tutorial sehr kompakt ist, geben wir pro Abschnitt Hilfestellungen und gesonderte Erklärungen.

Gehe ''der Reihe nach'' jeden Abschnitt durch. Arbeitsanweisungen und häufig gestellte Fragen und Probleme werden in den Abschnitten jeweils als Unterabschnitt geklärt.

### Einleitung
Durchlesen: http://zetcode.com/gui/pyqt5/introduction/

### Erste kleine Programme
Durchlesen und dabei die Beispiele selber ausprobieren und die Erklärungen nachvollziehen:

* http://zetcode.com/gui/pyqt5/firstprograms/

### Menüs und Toolbars
http://zetcode.com/gui/pyqt5/menustoolbars/

...

### Layout management
http://zetcode.com/gui/pyqt5/layout/

...

### Events und Signale
http://zetcode.com/gui/pyqt5/eventssignals/

...

### Dialoge
http://zetcode.com/gui/pyqt5/dialogs/

...
