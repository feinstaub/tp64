Moon-Lander anpassen
====================

Voraussetzungen
---------------
* Umgang mit der Bash-Shell, siehe z. B. ShellTux
* Programmiersprachen: C und teilweise C++ (siehe Makefile)
    * siehe programmieren-cpp/README-c.md

Programm installieren
---------------------
Installieren:

    $ sudo zypper install moon-lander

Programm ausprobieren
---------------------
Mache dich in maximal 10 Minuten mit dem Spiel Moon-Lander vertraut (ohne Ton).

Programm starten:

    $ moon-lander

Quellcode-Paket holen
---------------------
### Finden

    $ zypper se moon-lander

    S  | Name        | Summary              | Type
    ---+-------------+----------------------+-----------
    i+ | moon-lander | A 2D Game of Gravity | package
       | moon-lander | A 2D Game of Gravity | srcpackage

In the Status-Spalte S steht, ob das Paket vom Benutzter installierte wurde (i+) oder automatisch (i) oder nicht installiert ist (leer).
Die Quellpakete (**srcpackage** in der Spalte Type) werden nie als installiert angezeigt.

### Abhängigkeiten und Quellcode-Paket installieren

Möglicherweise benötigt das Quellpaket noch weitere abhängige Pakete. So werden diese installiert:

    $ sudo zypper source-install moon-lander

    The following 2 NEW packages are going to be installed:
        libSDL_image-devel libSDL_mixer-devel

Die mit (sudo) source-install geholten Quellpakete befinden sich hier: /usr/src/packages/SOURCES/. Diese verwenden wir aber nicht, sondern rufen source-install nochmal ohne sudo auf:

    $ zypper source-install moon-lander

Die mit geholten Quellpakete befinden sich hier: **~/rpmbuild/SOURCES** und **~/rpmbuild/SPECS/**

### Quellcode vorbereiten

    $ cd ~/rpmbuild/SPECS
    $ rpmbuild -bp moon-lander.spec

Als Ergebnis befindet sich der vollständige Quellcode nun hier: **~/rpmbuild/BUILD/moon-lander/**

Diesen Ordner kopieren wir in unser Arbeitsverzeichnis **~/ag/moon-lander**

    $ mkdir ~/ag/moon-lander
    $ cd ~/ag/moon-lander
    $ cp -r ~/rpmbuild/BUILD/moon-lander/* .
    $ ls

sollte folgendes liefern:

    cross-make.sh  DT_drawtext.h  game_lib.c  images      Makefile        moon-lander    README.txt
    DT_drawtext.c  fonts          gamelib.h   install.sh  Makefile.win32  moon_lander.c  sounds


Programm bauen und ausführen
----------------------------

    $ cd ~/ag/moon-lander
    $ make clean && make
    $ ./moon-lander.bin

(make clean am besten immer mit ausführen, damit immer sauber gebaut wird)

Quellcode ändern
----------------
Wir machen nun eine einfache Änderung am Programm:

* Öffne die Datei **moon_lander.c** mit Kate.
* Finde den Text "Free ship every 10,000 points." vom Startbildschirm und ändere ihn ab in: "Alle 10.000 Punkte gibt es ein neues Schiff."
* Speichern.

Mit dem nächsten Befehl kompilieren wir den geänderten Quellcode und führen danach das Programm aus:

    $ make && ./moon-lander.bin


Aufgabe 1: Audio auskommentieren
--------------------------------
siehe

    play_audio(game->on, 0);


### oder: 1 b Lautstärke ändern
siehe

    Mix_VolumeChunk(game.on, 20);


Aufgabe 2: Übersetzen
---------------------
* Texte auf Deutsch übersetzen

Aufgabe 3: Bilder
-----------------
* Neue Hintergrundbilder
    * siehe images/backgrounds
    * Kopieren / Löschen / Ändern
    * Dazu #define DATAPATH "/usr/share/moon-lander/" auf "" setzen

* Schiff-Bild ändern
    * siehe images/newship.png
    * Dazu #define DATAPATH "/usr/share/moon-lander/" auf "" setzen

Aufgabe 4: Einen Fehler finden
------------------------------
### Fehlerbeschreibung
* Mit F den Flight Director einschalten
* Nach oben beschleunigen bis die Rakete nach oben rausfliegt und länger.
* Sobald die Anzeige des Flight Directors unten rausgeht, stürzt das Programm ab.

### Programm mit KDevelop öffnen
* KDevelop starten
* Project -> Open / Import Project...
* ~/ag/moon-lander auswählen

### Launch Configuration einrichten
* Run -> Configure Launches...
* moon-lander -> Add New... -> Compiled Binary
* Executable: /home/gregor/ag/moon-lander/moon-lander.bin
* Dependencies: Targets -> moon-lander hinzufügen. Damit wird immer vor Ausführung des Programms der Code kompiliert.

### Programm starten
* Execute Launch
* Konsolenausgaben unter dem "Run"-Tab

### Programm mit Debugger starten
* Makefile aufmachen und die Zeile abändern:
    * von: CFLAGS=-Wall $(RPM_OPT_FLAGS) `sdl-config --cflags`
    * nach: CFLAGS=-g -Wall $(RPM_OPT_FLAGS) `sdl-config --cflags`
    * also ein -g einfügen
* $ make clean
* $ make
* Debug Launch
* Bei Absturz => unten den Tab "Framestack" aufrufen

siehe auch https://docs.kde.org/trunk5/en/extragear-kdevelop/kdevelop/debugging-programs-in-kdevelop.html

### Problem lösen
* Herausfinden, warum es abstürzt und Problem lösen.

Weitere Ideen
-------------
* Anzeige für Autopilot an/aus
* "Moonlander Duell Mode"
    * Das Fenster wird doppelt so breit.
    * Links Spieler 1 (Tasten A und D für links/rechts und W für vertikale Beschleunigung)
    * Rechts Spieler 2 (Cursor-Tasten wie gehabt)
    * Wer die Plattform schneller erreicht, dessen Punkte werden verdoppelt (oder so)
    * Später: Mensch vs. AI.
* Auf SDL 2 und C++ portieren
* Projektstruktur aufräumen (separate README, LICENSE, CREDITS etc.)

Siehe auch
----------
* Moon-Buggy anpassen (todo)
    * 2019: kein OpenSuse-Paket mehr
    * https://github.com/seehuhn/moon-buggy
    * sudo zypper install automake makeinfo
    * ./autogen.sh
    * ./configure
    * make
    * ./moon-buggy

* Tunneler anpassen
