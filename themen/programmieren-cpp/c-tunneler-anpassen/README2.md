Tunneler anpassen 2
===================

<!-- toc -->

- [Nachtrag: Entpacken](#nachtrag-entpacken)
- [Debugging mit printf()](#debugging-mit-printf)
- [Die Funktion system()](#die-funktion-system)
- [Pixel-Schrift verkleinern](#pixel-schrift-verkleinern)
- [Weitere Ideen](#weitere-ideen)
  * [system-Aufrufe](#system-aufrufe)
- [Die Sprache C](#die-sprache-c)
  * [Bücher](#bucher)
  * [C-Strings](#c-strings)

<!-- tocstop -->

## Nachtrag: Entpacken

Alternativ kann man ein Archiv auch mit graphischen Tools entpacken:

Verzeichnis ~/rpmbuild/SOURCES/ mit Dolphin öffnen

Rechte Maustaste auf tunneler-1.1.1.tar.bz2 -> Extract -> Extract archive here, autodetect subfolder

## Debugging mit printf()
'''main.c:''' - Achte auf die erste geschweifte Klammer auf und die letzte geschweifte Klammer zu.

    int main( int argc, char *argv[] )
    {
        printf("main start\n");
        ...
        printf("main end\n");
    }

## Die Funktion system()

'''main.c:'''

    printf("main start\n");
    system("cowsay 'Tunneler startet!'");
    system("notify-send 'Tunneler startet!'");
    system("bash -c 'play /usr/share/tong/media/tick.ogg &'");
    system("bash -c 'play /usr/share/alienarena/data1/sound/music/dm-saucer.ogg &'");

'''tunneler.c:'''

    /* Make new ammo */
    if( Tank[i].fire && Time_Now() - Tank[i].last > FIRE_DELAY && Tank[i].deathc <= 0.0 )
    {
            system("bash -c 'play /usr/share/supertuxkart/data/sfx/goo.ogg &'");

## Pixel-Schrift verkleinern
'''graphics.c:'''

    void PutPixel_1( int x, int y, Uint32 color, int res_factor)
    {
        SDL_Rect rect;

        int rx = RES_X * res_factor;
        int ry = RES_Y * res_factor;

        rect.x = Video_X*x/rx;
        rect.y = Video_Y*y/ry;
        rect.w = Video_X*(x+1)/rx - Video_X*x/rx;
        rect.h = Video_Y*(y+1)/ry - Video_Y*y/ry;

        SDL_FillRect( screen, &rect, color );
    }

    void PutPixel( int x, int y, Uint32 color )
    {
        PutPixel_1(x, y, color, 1);
    }

    void PutChar( int x, int y, char ch, Uint32 color )
    {
        int i, j;

        for( i = 0; i < 8; i++ ) {
        for( j = 0; j < 8; j++ ) {
            if( font8x8[j][i][(int)ch] )
            PutPixel_1( x + j, y + i, color, 2 );
        }
        }
    }

## Weitere Ideen
### system-Aufrufe
* '''espeak'''
* '''xcowsay'''

tunneler.c:

    /* Death */
        int is_dead = 0;
    if( Tank[i].Shields <= 0.0 && Tank[i].deathc <= 0.0 )
    {
        Tank[i].Shields = 0.0;
        Explosion( Tank[i].x, Tank[i].y, 30, 1 );
        Tank[i].deathc = 4.0;
        Tank[i].deaths++;
            is_dead = 1;
    }
    else if( Tank[i].Energy <= 0.0 && Tank[i].deathc <= 0.0 )
    {
        Tank[i].Energy = 0.0;
        Explosion( Tank[i].x, Tank[i].y, 30, 1 );
        Tank[i].deathc = 4.0;
        Tank[i].deaths++;
            is_dead = 1;
    }

    if (is_dead)
        {
            char cmd[100];
            sprintf(cmd, "notify-send 'Spieler %d hat das Zeitliche gesegnet.'", i);
            system(cmd);

            // 0: anz. tot rechts => Gewinn links
            sprintf(cmd, "bash -c 'xcowsay \"Spieler %d hat das Zeitliche gesegnet.\nPunktestand:\nlinks=%d, rechts=%d\"' &", i, Tank[0].deaths, Tank[1].deaths);
            system(cmd);
        }

* Nachdem die Auflösung der Schrift vergrößert wurde:
    * '''Längere Texte''' möglich
    * '''Spielernamen als Parameter''' übergeben und in den Settings anstelle von "Tank 1" und "Tank 2" verwenden.

* Programmiere dein eigenes Tunneler-Spiel mit Python und Qt.

## Die Sprache C
### Bücher
* Lerne C programmieren auf Wikibooks: https://de.wikibooks.org/wiki/C-Programmierung
* Lerne C programmieren: https://www.proggen.org/doku.php?id=c:start

### C-Strings
* Am besten (ähnlich wie bei Datenbanken) schon vorher festlegen wie lange der String maximal sein soll.
    * siehe "Nullterminiert": http://www.c-howto.de/tutorial-strings-zeichenketten-nullterminiert.html
* [http://www.c-howto.de/tutorial-strings-zeichenketten-stringfunktionen.html Länge]
* [http://www.c-howto.de/tutorial-strings-zeichenketten-stringfunktionen-kopieren-strcpy.html Kopieren]
* Variable length string in Blöcken: https://de.wikipedia.org/wiki/Variable_Length_Array
