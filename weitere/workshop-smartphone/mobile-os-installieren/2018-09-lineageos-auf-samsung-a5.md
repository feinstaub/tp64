LineageOS auf Samsung A5
========================

<!-- toc -->

- [Modell](#modell)
  * [Samsung Galaxy A5 (2016) oder (2017)?](#samsung-galaxy-a5-2016-oder-2017)
  * [TWRP installieren](#twrp-installieren)
  * [LineageOS installieren](#lineageos-installieren)

<!-- tocstop -->

Modell
------
### Samsung Galaxy A5 (2016) oder (2017)?
* Siehe offizielle Liste: https://wiki.lineageos.org/devices/
    * Gerät - Codename
    * Galaxy A5 **(2017) - a5y17lte**
        * Seite: https://wiki.lineageos.org/devices/a5y17lte inklusive Anleitung
    * Galaxy A7 (2017) - a7y17lte
    * Galaxy A5 **(2016) ist nicht dabei**

* Websuche nach "Galaxy A5 (2016)"
* Ergebnis
    * Codename **a5xeltexx** (oder a5xelte ohne xx?)
    * ["How to Install Lineage OS 14.1 On Samsung Galaxy A5 (2016)"](https://www.getdroidtips.com/lineage-os-14-1-samsung-galaxy-a5-2016/), 2018
        * Laut User-Comments scheint einiges nicht zu gehen
        * Generell sind diese Hinweise zu beachten:
            * "This will work on Samsung Galaxy A5 2016 (Don’t Try this in Any other device)"
            * ...
            * "You will loose the original ROM or any CUSTOM ROM if you already installed on your phone. So make sure to Backup your phone before doing this step using TWRP or CWM or any Custom Recovery."
                * selber noch nicht gemacht
            * ...
            * "You must install TWRP or any Custom Recovery on your phone."
                * TWRP ist zu empfehlen
    * ["[ROM] Lineage 15.1 for Galaxy A5 2016 [UNOFFICIAL]"](https://forum.xda-developers.com/samsung-a-series/orig-development/rom-lineage-15-1-galaxy-a5-2016-t3747109). 2018. Das **XDA-Forum ist prinzipiell eine gute Quelle**
        * Dort sind die Downloads verlinkt.
            * " Lineage 15.1 builds for Samsung Galaxy A5 2016: mega.nz"
            * Magisk und OpenGApps braucht man wahrscheinlich nicht
        * Unter "Installation" die einzelnen Schritte
        * Darunter steht, welche Element funktionieren und welche nicht.
            * Es scheint alles zu gehen, außer NFC.

### TWRP installieren
Der erste Schritt für eine LineageOS-Installation ist es also, TWRP zu installieren.

(Generell gilt: wenn man was falsch macht, geht das Gerät möglicherweise kaputt)

* Hier mögliche Anleitungen:
    * [als Video, 10 min](https://www.youtube.com/watch?v=zVntYlMc_uY)
        * Hier sieht man auch, wie man die Knöpfe für den Download-Boot-Mode drückt
            * siehe auch "Boot into download mode: With the device powered off, hold Volume Down + Home + Power." (https://wiki.lineageos.org/devices/a5y17lte/install für das Samsung A5 2017)
    * [in Textform](https://twrp.me/samsung/samsunggalaxya52016exynos.html)

* Wenn das geschafft ist, sollte man sich sicher sein wie man nach TWRP bootet und auch wieder rauskommt.

### LineageOS installieren
* Für die LineageOS-Installation muss man möglicherweise auf eine neue Version von TWRP updaten, siehe https://forum.xda-developers.com/samsung-a-series/orig-development/rom-lineage-15-1-galaxy-a5-2016-t3747109
    * "Download and flash new TWRP 3.2.1 from TWRP Official Website"
    * Das geht dann aber einfach, wenn man TWRP schon installiert hat.
