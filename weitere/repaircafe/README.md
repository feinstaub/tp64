Einstieg - RepairCafe
=====================

RepairCafe Standard
-------------------
* siehe z. B. http://www.repaircafe-bergstrasse.de

RepairCafe Kids
---------------
* siehe z. B. http://naturschutzzentrum-bergstrasse.de/events/event/repair-kids/
    * siehe Code:
        * [Hallo-Linux-mit-Pinguin.odt](file:///~/tp64/repaircafe/Hallo-Linux-mit-Pinguin.odt)
        * [turtleblume.py](file:///~/tp64/repaircafe/turtleblume.py)

Häufige Fragen
--------------
### Probleme mit Apple-Produkten?

Apple setzt sich GEGEN ein "Recht auf Reparieren" ein und verfolgt somit eine Geschäftspolitik, die sich grundsätzlich gegen die Ideen von Repair-Cafes richtet.

Was soll man also mit solchen Produkten anfangen? --> So lange verwenden, bis kaputt und dann von einem anderen Hersteller die Produkte erwerben.

Medienberichte dazu:

* ["Gegen "Recht auf Reparatur": Apple macht weiter Lobbyarbeit"](https://www.heise.de/mac-and-i/meldung/Gegen-Recht-auf-Reparatur-Apple-macht-weiter-Lobbyarbeit-3717600.html), heise.de, 18.05.2017
* ["Mekka für Hacker: Apple argumentiert gegen "Recht auf Reparatur""](https://www.heise.de/mac-and-i/meldung/Mekka-fuer-Hacker-Apple-argumentiert-gegen-Recht-auf-Reparatur-3631249.html), heise.de, 20.02.2017
* ["Wer hat Angst vor dem Recht auf Reparatur?"](http://www.zeit.de/digital/internet/2017-02/recht-auf-reparatur-apple-gesetz-usa), zeit.de, Feb 2017
* ["Apple will "Recht auf Reparatur" verhindern"](https://www.golem.de/news/lobbyismus-apple-will-recht-auf-reparatur-verhindern-1705-127922.html), golem.de, Mai 2017
* ["Warum sich Apple gegen ein "Recht auf Reparatur" wehrt"](http://www.sueddeutsche.de/digital/ersatzteile-warum-sich-apple-gegen-ein-recht-auf-reparatur-wehrt-1.3411716), SZ.de, 09.03.2017
    * "[...] wird es immer schwieriger, die Apple-Geräte selbst zu reparieren. Es gebe immer mehr Elektronik, die Bauteile würden immer feiner. Auch nicht gut sei es, wenn jetzt in den Macbooks Pro der Arbeitsspeicher fest verlötet ist. Ersatzteile von Apple bekommen [...] seine Mitarbeiter nicht."
