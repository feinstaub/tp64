Dateien suchen
==============

<!-- toc -->

- [Dolphin](#dolphin)
- [KFind](#kfind)
- [Kommandozeile](#kommandozeile)
- [Neuste Tools](#neuste-tools)
  * [FSearch - fast file search utility](#fsearch---fast-file-search-utility)
  * [ANGRYsearch - fast file search utility](#angrysearch---fast-file-search-utility)

<!-- tocstop -->

Dolphin
-------
Strg+F drücken.

KFind
-----
```
$ sudo zypper install kfind
```

* z. B. Einschränkung auf Datum möglich

Kommandozeile
-------------
* locate
* find - Doku nötig

Neuste Tools
------------

### FSearch - fast file search utility

https://github.com/cboxdoerfer/fsearch

"inspired by Everything Search Engine. It's written in C and based on GTK+3."

Selber kompilieren:

```
$ cd ~/dev
$ git clone https://github.com/cboxdoerfer/fsearch.git
$ cd fsearch

$ ./autogen.sh

# Es kommt zu Fehlermeldungen, weil Programme fehlen.
# zypper se -f intltoolize

$ sudo zypper install intltool glib-devel glibc-devel glib2-devel automake autoconf

$ ./autogen.sh

$ ./configure

# configure: error: no acceptable C compiler found in $PATH

$ sudo zypper install gcc

$ ./configure
./configure: line 6384: syntax error near unexpected token `-std=c11,'
./configure: line 6384: `AX_CHECK_COMPILE_FLAG(-std=c11, CFLAGS+=" -std=c11" ,'

# see https://github.com/cboxdoerfer/fsearch/issues/4
# Comment out those 4 lines

$ ./configure
# No package 'gtk+-3.0' found

$ sudo zypper install gtk3-devel

$ ./configure

$ make

$ cd src
$ ./fsearch
```

Menü -> Preferences -> Database:

* Include: /home/*yourname*
* Include: /usr
* Exclude: /home/*yourname*/.cache


### ANGRYsearch - fast file search utility

* https://github.com/DoTheEvo/ANGRYsearch - "Linux file search, instant results as you type"
* Geschrieben mit Python3 und PyQt5.
* openSUSE-Paket: https://software.opensuse.org/package/angrysearch - Repository von 'home:KAMiKAZOW' wählen
* Bedienung siehe Anleitung: https://github.com/DoTheEvo/ANGRYsearch
* FSearch scheint etwas ausgereifter zu sein.
