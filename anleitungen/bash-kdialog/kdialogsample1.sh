#!/bin/bash

kdialog --msgbox Das
kdialog --yesno ist
# http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO-6.html
if [ $? ]; then
    echo Ja
else
    echo Nein
fi
kdialog --msgbox ein
kdialog --msgbox Test.

# http://tldp.org/HOWTO/Bash-Prog-Intro-HOWTO-7.html
for i in $( ls /etc/pulse ); do
    echo item: $i
    kdialog --msgbox $i
done
