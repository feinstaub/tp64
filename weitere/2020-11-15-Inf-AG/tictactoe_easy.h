#include <string>
#include <iostream>

using namespace std;

void tictacto(){
    int i=0;
    int pl=1;
    string cord[3][3]={{"-","-","-"},{"-","-","-"},{"-","-","-"}};
    int eingabe,eingabez;
    int abbruch=1;
    string spst="Unentschieden";
    while(i==0){
        int belegt=0;
        string zeichen;
        for(int a=0;a<=2;a++){//Überprüfschleife
            for(int b=0;b<=2;b++){
                if(cord[a][b]=="x" || cord[a][b]=="o"){// schaut ob Feld voll
                    belegt++;
                    if(belegt==9){
                        abbruch=0;
                    }
                }
                int poss[8][4]={{-1,-1,-2,-2},{-1,0,-2,0},{-1,+1,-2,+2},
                                {0,1,0,2},{1,1,2,2},{1,0,2,0},{1,-1,2,-2},
                                {0,-1,0,-2}};
                int pos1,pos12,pos2,pos22;
                if(cord[a][b]=="x"){
                    for(int c=0;c<=7;c++){
                        pos1=a + poss[c][0];
                        pos12=b + poss[c][1];
                        pos2=a + poss[c][2];
                        pos22=b + poss[c][3];
                        if(pos1>=0 && pos1<=2 && pos12>=0 && pos12<=2 &&
                            pos2>=0 && pos2<=2 && pos22>=0 && pos22<=2){
                            if("x"==cord[pos1][pos12] && "x"==cord[pos2][pos22]){
                                abbruch=0;
                                spst="Gewonnen Hat Spieler1:";
                            }
                        }
                    }
                }
                zeichen="o";
                if(cord[a][b]=="o"){
                    for(int c=0;c<=7;c++){
                        pos1=a - poss[c][0];
                        pos12=b - poss[c][1];
                        pos2=a - poss[c][2];
                        pos22=b - poss[c][3];
                        if(pos1>=0 && pos1<=2 && pos12>=0 && pos12<=2 &&
                            pos2>=0 && pos2<=2 && pos22>=0 && pos22<=2){
                            if("o"==cord[pos1][pos12] && "o"==cord[pos2][pos22]){
                                abbruch=0;
                                spst="Gewonnen Hat Spieler2:";
                            }
                        }
                    }
                }
            }
        }
        if(abbruch==1){
            cout << "Es ist Spieler " << pl << " an der Reihe.\n" << endl;
        }
        cout <<" |0|1|2|\n";
        cout <<"0|"<<cord[0][0]<<"|"<<cord[0][1]<<"|"<<cord[0][2]<<"|\n";
//         cout <<"|-|-|-|\n";
        cout <<"1|"<<cord[1][0]<<"|"<<cord[1][1]<<"|"<<cord[1][2]<<"|\n";
//         cout <<"|-|-|-|\n";
        cout <<"2|"<<cord[2][0]<<"|"<<cord[2][1]<<"|"<<cord[2][2]<<"|\n";
//         cout <<"|-|-|-|\n";
        cout <<endl;
        if(abbruch==1){
            cout <<"Mache eine Eingabe Spieler " << pl <<" 1.Zahl(y):\n";
            cin >> eingabe;
            cout << "2.Zahl(x):\n";
            cin >> eingabez;
            cout <<endl;
        }
        if(pl==1){
            if(cord[eingabe][eingabez]!="x" && cord[eingabe][eingabez]!="o"){
                cord[eingabe][eingabez]="x";
                pl=2;
            }
        }
        if(pl==2){
            if(cord[eingabe][eingabez]!="x" && cord[eingabe][eingabez]!="o"){
                cord[eingabe][eingabez]="o";
                pl=1;
            }
        }
        if(abbruch==0){
            i++;
            cout << spst <<":Das Spiel ist vorbei.\n";
        }
    }

}
