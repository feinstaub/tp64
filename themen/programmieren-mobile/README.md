Programmieren - Mobil
=====================

KDE Kirigami
------------
* 2019
    * ["Creating a Plasma Mobile application"](https://nicolasfella.wordpress.com/2019/02/20/creating-a-plasma-mobile-application/)
    * https://mglapps.frama.io/ - list of Mobile GNU/Linux Apps
* 2018
    * Entwicklungen zu Kirigami: http://notmart.org/blog/2018/08/akademy-2018/
    * [User Question: With Some Free Software Phone Projects Ending, What Does Plasma Mobile's Future Look Like?](https://dot.kde.org/2017/04/28/user-question-some-free-software-phone-projects-ending-what-does-plasma-mobiles-future), kde.org, 2017
    * [Halium](https://halium.org/announcements/2017/04/24/halium-is-in-the-air.html)
