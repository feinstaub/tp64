Dokumenationssysteme
====================

<!-- toc -->

- [Kritieren](#kritieren)
- [README.md (Markdown mit grip)](#readmemd-markdown-mit-grip)
- [Wikitolearn (Mediawiki)](#wikitolearn-mediawiki)
- [asciidoc](#asciidoc)
- [Nikola](#nikola)
- [Pelican](#pelican)
- [Jekyll](#jekyll)

<!-- tocstop -->

Kritieren
---------

Folgende Eigenschaften sind wünschenswert:

* Freie Software (Voraussetzung)
* Bearbeitung mit freien Tools, z. B. Texteditor (Voraussetzung)
* Im Browser / Online darstellbar (Voraussetzung)
* Offline-Bearbeitung
* Durchsuchbarkeit
* Versionierung
* Vergleichsfähigkeit der Versionen
* Zusammenarbeit mit anderen
* Hübsches Ergebnis
* Im Browser / Online darstellbar
* Backup-Fähigkeit
* Einbinden von Bildern, Dateien und Tabellen
* Einfache Bedienung
* Einfaches Setup
* Möglichst geringe Fremdabhängigkeit
* Übersichtlichkeit durch Inhaltsverzeichnisse

README.md (Markdown mit grip)
-----------------------------
* Im Browser / Online darstellbar: JA, z. B. mittels Github
* **Offline-Bearbeitung**: JA (bei grip der Offline-Renderer einschalten; mit git arbeiten hilft beim Draften und Konsolidieren von Inhalten)
* **Durchsuchbarkeit**: JA, offline mittels Funktionen des Texteditors (hilft beim Refaktorieren) und online via Github
* Versionierung: JA, weil Textdateien
* Vergleichsfähigkeit der Versionen: JA, weil Textdateien
* Zusammenarbeit mit anderen: JA, mit git-Provider
* Hübsches Ergebnis: JA
* Backup-Fähigkeit: JA
* Einbinden von Bildern: JA
* Einbinden von Dateien: JA
* Einbinden von Tabellen: ?
* Einfache Bedienung: JA
* Einfaches Setup: JA (einige wenige Scripts notwendig)
* Möglichst geringe Fremdabhängigkeit: JA

Wikitolearn (Mediawiki)
-----------------------
* Im Browser / Online darstellbar: JA
* **Offline-Bearbeitung: NEIN** (zwar temporär möglich; aber auf Dauer ist es einfach nervig, für kleine Korrekturen online sein und sich einloggen zu müssen; das muss nicht sein)
* Durchsuchbarkeit: JEIN: offline nur mit Hilfe des exportierten XMLs; online ja, aber die Ergebnisliste ist etwas unübersichtlich
* Versionierung: JA
* Vergleichsfähigkeit der Versionen: JA, online oder mittels Backup-Extrakten
* Zusammenarbeit mit anderen: JA, nur Browser nötig
* Hübsches Ergebnis: JA
* Backup-Fähigkeit: JA, aber umständlich
* Einbinden von Bildern: JA, aber umständlich
* Einbinden von Dateien: JA, aber umständlich
* Einbinden von Tabellen: JA
* Einfache Bedienung: JA
* Einfaches Setup: JA (wenn man es gehostet bekommt)
* Möglichst geringe Fremdabhängigkeit: NEIN

asciidoc
--------
* Im Browser / Online darstellbar: JA, z. B. mittels github.io oder andere html-Host-Seite
* Offline-Bearbeitung: JA
* Durchsuchbarkeit: JEIN: offline mittels Texteditor; nicht online
* Versionierung: JA, weil Textdateien
* Vergleichsfähigkeit der Versionen: JA, weil Textdateien
* Zusammenarbeit mit anderen: JEIN, weil kompliziertes Setup
* Hübsches Ergebnis: JA
* Backup-Fähigkeit: JA
* Einbinden von Bildern: JA
* Einbinden von Dateien: JA
* Einbinden von Tabellen: JA
* Einfache Bedienung: JEIN
* Einfaches Setup: NEIN, weil viel Eigenscripts
* Möglichst geringe Fremdabhängigkeit: JA

Nikola
------
* https://getnikola.com/
* "Nikola — Static Site Generator"

Pelican
-------
todo

Jekyll
------
Wirkt verbuggt.
