C++ lernen
==========

<!-- toc -->

- [C++-Einstieg auf wikibooks](#c-einstieg-auf-wikibooks)
  * [Start](#start)
  * [Online-Compiler zum Schnellstart](#online-compiler-zum-schnellstart)
  * [Infos](#infos)
  * [Alternative gute Inhalte auf willemer.de](#alternative-gute-inhalte-auf-willemerde)
- [Wiki-Buch zur C-Programmierung](#wiki-buch-zur-c-programmierung)
  * [Aufgaben](#aufgaben)
- [Dateien lesen und schreiben](#dateien-lesen-und-schreiben)
  * [willemer.de fileop (fstream)](#willemerde-fileop-fstream)
  * [C-Wiki-Buch: Dateien (Streams, fopen, Dateidescriptoren, open)](#c-wiki-buch-dateien-streams-fopen-dateidescriptoren-open)
- [Debugging, Debugger](#debugging-debugger)
  * [gdb](#gdb)
  * [Gede](#gede)
- [Weiteres](#weiteres)
  * [Große Zahlen mit GMP](#grosse-zahlen-mit-gmp)
  * [Pipes](#pipes)
- [Programmieraufgaben](#programmieraufgaben)
  * [Allgemein](#allgemein)
  * [Moonlander anpassen](#moonlander-anpassen)
- [KDE-Programme selber kompilieren](#kde-programme-selber-kompilieren)
- [Referenz](#referenz)
- [C++ IDEs](#c-ides)
  * [KDevelop](#kdevelop)
  * [CodeLite](#codelite)
  * [Code::Blocks](#codeblocks)
- [SDL](#sdl)
  * [Programme / Spiele mit SDL](#programme--spiele-mit-sdl)
  * [Tutorial: SDL, Collision Detection, OpenGL etc. / lazyfoo](#tutorial-sdl-collision-detection-opengl-etc--lazyfoo)
- [Bibliotheken](#bibliotheken)
  * [JSON](#json)
- [Fortgeschritten](#fortgeschritten)
  * [gdb](#gdb-1)
  * [hotspot](#hotspot)
  * [clang](#clang)
- [Bücher zum Selberlesen](#bucher-zum-selberlesen)

<!-- tocstop -->

C++-Einstieg auf wikibooks
--------------------------
### Start
[C++-Programmierung auf wikibooks](https://de.wikibooks.org/wiki/C%2B%2B-Programmierung)

* enthält einen einfachen Taschenrechner
* Erklärungen zu Pointern; sehr gut, um Speicherverwaltung zu lernen
* stellenweise sehr lückenhaft und alleine nicht als Lehrbuch geeignet

* Ein- und Ausgabe: https://de.wikibooks.org/wiki/C%2B%2B-Programmierung/_Einf%C3%BChrung_in_C%2B%2B/_Einfache_Ein-_und_Ausgabe
    * erklärt leider nicht cin::ignore in Kombination mit getline
    * Verweis auf printf und scanf siehe C: https://de.wikibooks.org/wiki/C-Programmierung:_Einfache_Ein-_und_Ausgabe
        * schöne Übersicht der Umwandlungszeichen

* Kommentare: https://de.wikibooks.org/wiki/C%2B%2B-Programmierung/_Einf%C3%BChrung_in_C%2B%2B/_Kommentare
    * ...

* Rechnen: https://de.wikibooks.org/wiki/C%2B%2B-Programmierung/_Einf%C3%BChrung_in_C%2B%2B/_Rechnen_(lassen)
    * ...

* Variablen: https://de.wikibooks.org/wiki/C%2B%2B-Programmierung/_Einf%C3%BChrung_in_C%2B%2B/_Variablen,_Konstanten_und_ihre_Datentypen
    * stellenweise kompliziert
    * Aufgabe:
        * mit for-Schleife einige ASCII-Zeichen ausgeben

* ...

* Weitere Grundelemente: https://de.wikibooks.org/wiki/C%2B%2B-Programmierung/_Weitere_Grundelemente
    * ...
    * Zeiger: ... Baustein: Zeiger TODO
    * ...
    * Feler/Arrays:
        * C-Buch ist ausführlicher
    * Zeichenketten: https://de.wikibooks.org/wiki/C%2B%2B-Programmierung/_Weitere_Grundelemente/_Zeichenketten
        * ...
        * getline
    * ...

### Online-Compiler zum Schnellstart
* http://cpp.sh/
    * Einfacher C++-Compiler mit Standard-Input und einigen Optionen
    * Initial-Beispiel für ADL mit getline (https://stackoverflow.com/questions/31598707/why-can-i-call-getline-without-using-stdgetline)
    * todo: std::printf - add this to c++ Buch
* https://ideone.com/HqdEdb
* https://www.onlinegdb.com/ - C++-Compiler mit Online-GDB mit Tutorials
* Weitere Tools
    * https://godbolt.org/ - Verschiedene C++ zu Assembler
    * https://cppinsights.io/ - auto, casts, range-based loops etc. shown
    * https://wandbox.org/ - another online compiler (different standards), ohne Standard-Input

### Infos
* enthält Übungen
* todo: passende Anmerkungen, Ergänzungen und weitere Übungen zu diesem Buch
    * how to start with C++ under openSUSE with Kate, KDevelop etc. --> siehe themen/kdevelop/README.md
* todo: Quick Start g++, Installation, Kompilieren etc.
* todo: how to find include file?

### Alternative gute Inhalte auf willemer.de
* http://www.willemer.de/informatik/cpp/

Wiki-Buch zur C-Programmierung
------------------------------
* https://de.wikibooks.org/wiki/C-Programmierung
    * https://de.wikibooks.org/wiki/C-Programmierung:_Einfache_Ein-_und_Ausgabe
    * ...
    * Zeiger
    * Arrays: https://de.wikibooks.org/wiki/C-Programmierung:_Arrays
        * mit anschaulicher Erklärung
    * ...
    * https://de.wikibooks.org/wiki/C-Programmierung:_Pr%C3%A4prozessor
    * ...
    * https://de.wikibooks.org/wiki/C-Programmierung:_ASCII-Tabelle

* weitere Bücher: https://de.wikibooks.org/wiki/Regal:Programmierung
    * C++-Referenz - https://de.wikibooks.org/wiki/C%2B%2B-Referenz

### Aufgaben
    Sinuswerte in 10er Schritten ausgeben
    Auf der Spitze stehendes Dreieck
    Skalarprodukt zweier Vektoren
    Polygonzüge für geometrische Linien
    Letzten Buchstaben in String finden
    Entwickeln Sie eine Funktion zur Simulierung einer Skytale
    Zeichenketten vergleichen
    Elektrische Messdaten
    Häufigkeit von Worten

Dateien lesen und schreiben
---------------------------
Baustein: Dateien lesen und schreiben

### willemer.de fileop (fstream)
* mit fstream: http://www.willemer.de/informatik/cpp/fileop.htm
    * inkl. fopen
* Beispiel siehe tictactoe

### C-Wiki-Buch: Dateien (Streams, fopen, Dateidescriptoren, open)
* https://de.wikibooks.org/wiki/C-Programmierung:_Dateien
    * (alt: https://de.wikibooks.org/wiki/C%2B%2B-Programmierung:_Dateizugriff)

    * Beispiele: sample-code/file1.cpp

Debugging, Debugger
-------------------
### gdb, inkl. Tipps
* Tasten: https://sourceware.org/gdb/current/onlinedocs/gdb/TUI-Keys.html#TUI-Keys
    * C-x o
* Tasten TUI-Single-Key-Mode: https://sourceware.org/gdb/current/onlinedocs/gdb/TUI-Single-Key-Mode.html#TUI-Single-Key-Mode
    * v - info locals
* Stack - https://sourceware.org/gdb/current/onlinedocs/gdb/Stack.html#Stack
    * https://sourceware.org/gdb/current/onlinedocs/gdb/Frames.html#Frames
        * "each frame is the data associated with one call to one function"
    * https://sourceware.org/gdb/current/onlinedocs/gdb/Selection.html#Selection
        * up
        * down
    * https://sourceware.org/gdb/current/onlinedocs/gdb/Frame-Info.html#Frame-Info
        * info args
        * info locals
* Run backward
    * https://sourceware.org/gdb/current/onlinedocs/gdb/Reverse-Execution.html#Reverse-Execution
* https://sourceware.org/gdb/current/onlinedocs/gdb/Value-History.html#Value-History
    * ...
* ...

* Liste von Front-Ends: https://sourceware.org/gdb/wiki/GDB%20Front%20Ends

### Gede
* http://acidron.com/gede/pmwiki.php?n=Downloads.Releases
    * funktioniert einfach und gut

### CodeLite (gdb)
* https://codelite.org/
    * kompliziert ein Projekt aufzusetzen und loszudebuggen

GUI-Toolkits
------------
Listen:

    * https://de.wikipedia.org/wiki/Liste_von_GUI-Bibliotheken#C++
    * https://en.wikipedia.org/wiki/List_of_widget_toolkits#Based_on_C++_(including_bindings_to_other_languages)
    * https://philippegroarke.com/posts/2018/c++_ui_solutions/ 2018
    * https://www.slant.co/topics/12868/~cross-platform-c-gui-toolkits

### FLTK
* https://de.wikipedia.org/wiki/Fast_Light_Toolkit
* siehe README-FLTK

### Nana
* http://nanapro.org/en-us/
* http://nanapro.org/en-us/blog/2016/05/an-introduction-to-nana-c-library/
    * Einfaches Tutorial
* Pro
    * modern C++

### MyGUI
* http://mygui.info/
    * todo

### Qt
* Con
    * ...

### U++
* https://www.ultimatepp.org/
    * Umfangreiche Doku
    * Con
        * Kein Paket / Probleme beim Kompilieren
* https://de.wikipedia.org/wiki/Ultimate%2B%2B
* mit IDE: https://www.ultimatepp.org/app$ide$GettingStarted$en-us.html
* vs. Qt: https://www.ultimatepp.org/www$uppweb$vsqt$en-us.html

### Wt (Web Toolkit)
* https://de.wikipedia.org/wiki/Wt_(Web_Toolkit)
    * "zur Entwicklung von Webanwendungen. Wt ermöglicht, in C++ komplette Ajax-Anwendungen (Fast-CGI oder Standalone) zu entwickeln."

### imgui
* https://github.com/ocornut/imgui

### Game-GUI (out-dated)
* http://voxels.blogspot.com/2015/07/opengl-game-gui-widgets-with-source.html
    * "One does not simply... make a GUI"
    * https://github.com/sp4cerat/Game-GUI

Weiteres
--------
### Große Zahlen mit GMP
* https://gmplib.org/manual/C_002b_002b-Interface-General.html

### Pipes
http://openbook.rheinwerk-verlag.de/linux_unix_programmierung/Kap09-002.htm#RxxKap09002040002D51F049100

"Die Pipes sind neben den Sockets eine der am häufigsten verwendeten Interprozesskommunikations-Techniken"

unistd.h

...TODO...

Programmieraufgaben
-------------------
### Allgemein
siehe ../anleitungen/programmieraufgaben.md

### Moonlander anpassen
siehe anleitungen

KDE-Programme selber kompilieren
--------------------------------
* Programme aus https://userbase.kde.org/Applications/Education/de selber kompilieren und etwas ändern.

Referenz
--------
* https://en.cppreference.com/w/cpp/algorithm/max
* https://en.cppreference.com/w/c/thread/thrd_sleep

C++ IDEs
--------
### KDevelop
* siehe themen/kdevelop/README.md
    * siehe c-cpp-moonlander/README.md

### CodeLite
* https://codelite.org/
* Mit gdb-Support
* TODO: ausprobieren!
    * CMake support?
    * how to install
        * Tumbleweed: packaged
        * Kubuntu: ?

### Code::Blocks
* (ein vorhandenes Projekt zu importieren: ging nicht auf Anhieb)

SDL
---
### Programme / Spiele mit SDL
* Moon-Lander (C++)
* 3Guys (C)
    * Verwendung von DATA_DIR
* [Einstieg in SDL auf proggen.org](https://www.proggen.org/doku.php?id=sdl:start) (todo)

### Tutorial: SDL, Collision Detection, OpenGL etc. / lazyfoo
* http://lazyfoo.net/tutorials/SDL/29_circular_collision_detection/index.php (TODO)
* siehe auch c-with-3guys

Bibliotheken
------------
### JSON
* https://github.com/nlohmann/json
* https://rapidjson.org/

Fortgeschritten
---------------
### gdb
* ...

### hotspot
* 2017: [hotspot – a GUI for the Linux perf profiler](https://www.kdab.com/hotspot-gui-linux-perf-profiler/)

### clang
* https://clang.llvm.org/comparison.html
* https://clang.llvm.org/features.html

Bücher zum Selberlesen
----------------------
* Der C++-Programmierer: C++ lernen – professionell anwenden – Lösungen nutzen. Aktuell zu C++17 (Englisch) Gebundenes Buch – 6. November 2017
    * von Ulrich Breymann (Autor), 40 EUR
        * "arbeitete als Systemanalytiker und Projektleiter in der Industrie und der Raumfahrttechnik und lehrte Informatik an der Hochschule Bremen. Er arbeitete an dem ersten C++-Standard mit und ist ein bekannter Autor zu den Themen C++, STL und Java ME.", legt Wert auf Didaktik und gute Programmiermethodik
    * https://www.hanser-fachbuch.de/buch/Der+C+Programmierer/9783446448841
    * ...
    * Das GUI-Kapitel behandelt Qt
    * Makefiles
    * Unit-Tests
    * ...
* C++ für Spieleprogrammierer (Englisch) Gebundenes Buch – 18. Januar 2016
    * von Heiko Kalista, 35 EUR
* Der Stroustroup
* Beginning-C-Through-Game-Programming
* Die Core Guidelines
