Dateien austauschen mit Droopy
==============================

<!-- toc -->

- [Das Ziel](#das-ziel)
- [Droopy herunterladen und starten](#droopy-herunterladen-und-starten)
  * [Droopy-Script herunterladen](#droopy-script-herunterladen)
  * [Ordner vorbereiten](#ordner-vorbereiten)
  * [Droopy starten und lokal testen](#droopy-starten-und-lokal-testen)
  * [Die eigene IP-Adresse finden](#die-eigene-ip-adresse-finden)
  * [Schicke dir selber eine Datei](#schicke-dir-selber-eine-datei)
- [Dateien empfangen](#dateien-empfangen)
  * [Datei gesendet bekommen](#datei-gesendet-bekommen)
  * [Problem: Adresse nicht gefunden? - Ping](#problem-adresse-nicht-gefunden---ping)
  * [Problem: Adresse nicht gefunden? - Firewall](#problem-adresse-nicht-gefunden---firewall)
  * [Datei hochladen](#datei-hochladen)
  * [Schauen, ob Dateien angekommen sind](#schauen-ob-dateien-angekommen-sind)
- [Droopy beenden](#droopy-beenden)
- [Weitere Ideen](#weitere-ideen)
- [ALT: wikitolearn](#alt-wikitolearn)

<!-- tocstop -->

Das Ziel
--------
Du stellst mit Hilfe des Programms **droopy** auf deinem Rechner einen Webdienst bereit, mit dem dir andere Personen Dateien zusenden können.
Droopy kann auch so eingestellt werden, dass du damit Dateien für andere freigeben kannst.

Droopy herunterladen und starten
--------------------------------
In diesem Abschnitt lernst du wie man mit Droopy einen HTTP-Dienst startet.

### Droopy-Script herunterladen
* Erstelle einen Ordner **~/ag/droopy**
* Öffne eine Konsole und lade mit wget das droopy-Python-Script herunter:

```
$ wget https://raw.githubusercontent.com/stackp/Droopy/master/droopy
```

### Ordner vorbereiten
* Du befindest dich im Ordner ~/ag/droopy
    * Der Besucher deiner Upload-Seite soll mit einem Bild begrüßt werden. Daher lege die Bild-Datei open-suse-welcome.gif (von https://www.opensuse.org/searchPage/) in den Ordner ~/ag/droopy

* Erstelle nun einen neuen Ordner **~/ag/droopy/uploads** (dort werden die hochgeladenen Dateien gespeichert)

Schaue, ob die Verzeichnis- und Datei-Struktur nun so aussieht:

    ~/ag/droopy
        - uploads/
        - droopy
        - open-suse-welcome.gif

### Droopy starten und lokal testen
Öffne eine Konsole und starte droopy folgendermaßen:

    $ python3 droopy -d uploads -m "Hallo, Gecko erwartet Dateien..." -p open-suse-welcome.gif

In der Konsolenausgabe steht nun so etwas wie:

    HTTP server starting... Check it out at http://localhost:8000

Öffne die angegebene Adresse (http://localhost:8000) mit einem Web-Browser, z. B. Mozilla Firefox.

Wenn nun eine Webseite mit deinem Text ("Hallo, Gecko A erwartet Dateien...") zu sehen ist, ist der Test erfolgreich.

### Die eigene IP-Adresse finden
Öffne eine Konsole, um die IP-Adresse deines Rechnerns herauszufinden.

    $ hostname -I

oder:

    $ cowsay $(hostname -I | sed "s/ .*//")

(Quelle: https://www.linuxtrainingacademy.com/determine-public-ip-address-command-line-curl/)

Die Ausgabe sieht ungefähr so aus:

    192.168.178.32 2a02:908:2610:300:3866:d363:13d8:ebfc 2a02:908:2510:250:d5a1:1c55:12dc:238

Die erste Zahl ("192.168.178.32") ist die gesuchte Adresse.

Ersetze in der Address-Liste des Browsers das "localhost" aus vom vorigen Schritt durch die eben herausgefundene IP-Adresse und lade die Seite erneut.

    z. B. http://192.168.178.32:8000

Wenn die gleiche Webseite ohne Fehler wie im vorigen Abschnitt erscheint, ist der Test erfolgreich.

### Schicke dir selber eine Datei
* Rufe deine Droopy-Webseite auf.
* Klicke auf "Durchsuchen"
* Wähle eine Datei auf deinem Rechner aus.
* Klicke auf "Senden".
* Es erscheint die Meldung "Datei empfangen! Weitere Datei senden".

Schaue nun im Ordner **~/ag/droopy/uploads** nach, ob die Datei angekommen ist.

Dateien empfangen
-----------------
Bitte eine andere Person im Raum, folgendes zu tun.

### Datei gesendet bekommen
* Die andere Person soll die http-Adresse aus dem vorigen Abschnitt mit einem Webbrowser aufrufen: z. B. http://192.168.178.32:8000
* Es sollte die bekannte Droopy-Webseite erscheinen.
* Wenn nicht, dann schaue dir die folgenden zwei Abschnitte an.

### Problem: Adresse nicht gefunden? - Ping
Die andere Person soll den ping-Befehl verwenden, um zu schauen, ob dein Rechner überhaupt erreichbar ist, z. B.:

    $ ping 192.168.178.32

Die korrekte Ausgabe sieht ungefähr so aus.

    PING 192.168.178.32 (192.168.178.32) 56(84) bytes of data.
    64 bytes from 192.168.178.32: icmp_seq=1 ttl=64 time=0.067 ms
    64 bytes from 192.168.178.32: icmp_seq=2 ttl=64 time=0.053 ms
    64 bytes from 192.168.178.32: icmp_seq=3 ttl=64 time=0.057 ms
    ...

Unterbreche den ping-Befehl mit Strg+C.

Wenn der Ping ok war, aber die Adresse nicht funktioniert, dann liegt es möglicherweise an der Firewall:

### Problem: Adresse nicht gefunden? - Firewall
Eine aktivierte Firewall auf deinem Rechner könnte die Ursache sein.

Lösung: [Firewall deaktivieren](../anleitungen/firewall-ausschalten)

### Datei hochladen
Erkläre der Person wie man eine Datei hochlädt (genauso wie du es vorhin selber gemacht hast) und sie soll das durchführen mit einer Datei ihrer Wahl.

### Schauen, ob Dateien angekommen sind
Schaue wieder in den Ordner ~/ag/droopy/uploads. Dort sollten sich nun die eben hochgeladenen Datei oder Dateien nun befinden.

Droopy beenden
--------------
Droopy wird beendet, indem du Strg+C in der Konsole drückst, wo Droopy läuft. Nun können keine Dateien mehr hochgeladen werden.

Weitere Ideen
-------------
* Schau in die **Droopy-Ausgabe** auf der Konsole während die Droopy-Seite auf deinem Rechner aufgerufen wird.
* Die Droopy-Option **"Downloads bereitstellen"** aktivieren.
    * siehe `$ python3 droopy --help`
* Ändere den Port von 8000 auf den **HTTP-Standardport 80** und bemerke, dass die Port-Angabe dann im Browser weggelassen werden kann.
* --save-config
    * droopy ausführbar machen
    * Verknüpfung auf Desktop mit konsole wrapper
        * konsole -e ~/ag/droopy/droopy
        * Workpath anpassen funktioniert nicht. Man muss oder mit absolutem upload path und -p für das Bild arbeiten
* Das **Droopy-Script anpassen**
    * Eine **Plasma-Benachrichtigung, wenn eine neue Datei hochgeladen wurde**
    * z. B. Option für separates Verzeichnis für Uploads und Downloads
    * z. B. bei den Downloads auch die Dateigröße anzeigen

ALT: wikitolearn
----------------
todo: entfernen: == Rechner B: Dateien vorbereiten ==

Auf dem Rechner B einige Dateien vorbereiten:

* Erstelle einen neuen Ordner ~/ag/Tolle-Dateien-fuer-A.
* Lege in diesen Ordner folgende Dateien:
** Eine Text-Datei (entweder selbererstellt oder kopiere die Datei /etc/services)
** Ein kleines (mit LibreOffice Writer selbsterstelltes) odt-Textdokument.
** Ein Bild, z. B. selbstgemalt mit Kolourpaint.
** Eine Musik-Datei, z. B. BoxCat_Games_-_19_-_Mission.mp3, heruntergeladen von http://freemusicarchive.org/music/BoxCat_Games (wenn du den Song später z. B. in einem eigenen Programm verwenden ''und veröffentlichen'' möchtest, dann achte darauf, dass er unter einer [https://de.wikipedia.org/wiki/Creative_Commons Creative Commons]-Lizenz zur Verfügung gestellt wurde)


== Schaubild: Was ist passiert? ==
    1: 192.168.x.y:8000 im Browser eingeben und Enter drücken.
    2: Anfrage kommt bei Droopy an.
    3: Droopy liefert den Aufbau der Webseite.
    4: Der Browser zeigt die Webseite an und ist bereit Dateien hochzuladen.
    (Das Hochladen von Dateien funktioniert ähnlich)

    ========================================================================
    #                                                                      #
    #  Rechner A                                                           #
    #                                                                      #
    #   ---------------------------  3  ---------------------------------  #
    #   | Droopy-Python-Script    |---->| Firewall                      |  #
    #   | startet HTTP-Server auf |     | lässt eingehende Verbindungen |  #
    #   | Port 8000.              |<----| auf allen Ports durch         |  #
    #   ---------------------------  2  ---------------------------------  #
    #                                        ^    |                        #
    ========================================================================
                                            |    |
                                            |    |
        ------------------------------------------------------------
        |                 Lokales Netzwerk (LAN)                   |
        ------------------------------------------------------------
                                            |    |
                                            |    |
                                            |    |
    ========================================================================
    #                                        |    |                        #
    #  Rechner B                             |    |                        #
    #                                        |    v                        #
    #   ---------------------------  1  ---------------------------------  #
    #   | Firefox-Webbrowser      |---->| Firewall                      |  #
    #   | fragt 192.168.x.y:8000  |     | lässt ausgehende Verbindungen |  #
    #   | an                      |<----| auf allen Ports durch         |  #
    #   ---------------------------  4  ---------------------------------  #
    #                                                                      #
    ========================================================================
