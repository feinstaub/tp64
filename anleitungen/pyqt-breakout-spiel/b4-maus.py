#!/usr/bin/python3

from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QPen, QBrush, QColor, QResizeEvent, QFont, QFontDatabase
from PyQt5.QtCore import Qt, QTimer, QRect
import sys

class Fenster(QWidget):
    # Schläger
    rx = 20
    ry = 0
    rw = 100
    rh = 20

    # Ball
    bx = 0
    by = 0
    br = 10 # radius

    def __init__(self):
        super().__init__()
        self.setGeometry(300, 300, 400, 300)
        self.setWindowTitle("Hallo Maus")
        self.setMouseTracking(True) # damit mouseMoveEvent funktioniert
        self.show()

    def resizeEvent(self, e):
        h = e.size().height()
        self.ry = h - self.rh - 10
        self.update()

    def mouseMoveEvent(self, e):
        x = e.x()
        y = e.y()
        self.bx = x
        self.by = y
        self.rx = x
        self.update()

    def drawTopText(self, qp, text):
        qp.setPen(QColor(0, 0, 255))
        qp.setFont(QFont('Open Sans Light', 14))
        qp.drawText(0, 20, text)

    def drawBackground(self, qp):
        qp.setPen(Qt.NoPen)
        qp.setBrush(QBrush(QColor(0, 255, 255)))
        qp.drawRect(0, 50, self.width(), self.height() - 50)

    def drawRacket(self, qp):
        qp.setBrush(QBrush(QColor(255, 0, 0)))
        qp.drawRect(self.rx, self.ry, self.rw, self.rh)

    def drawBall(self, qp):
        qp.setPen(QPen(QColor(0, 0, 0), 4.0))
        qp.setBrush(QBrush(QColor(255, 0, 255)))
        qp.drawEllipse(self.bx - self.br, self.by - self.br, self.br * 2, self.br * 2)

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawTopText(qp, "Ball: " + str(self.bx) + "," + str(self.by))
        self.drawBackground(qp)
        self.drawRacket(qp)
        self.drawBall(qp)
        qp.end()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWidget = Fenster()
    sys.exit(app.exec_())
