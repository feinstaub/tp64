## Aufbau
* Was ist Privatsphäre?
* Warum und welche Apps (z. B. WhatsApp) und Dienste (z. B. Facebook) die Privatsphäre auch von Dritten verletzen
    * Whats-App-Alternativen
* Was ist Freie Software?
* Freie-Software-Varianten zu proprietären Programmen/Apps
    * App-Store F-Droid
    * Konkrete Listen von Apps
* Ausblick

---

## Privatsphäre und Smartsphone-Apps: Anschaulicher Einstieg
* ["Wenn die Verkäuferin eine App wäre (Versteckte Kamera)"](https://www.youtube.com/watch?v=wHo755bxByI), Stiftung Warentest, 2015, 2 min

---

## Privatsphäre vs. Überwachung: soziale Dämpfung
- ["Snowden-Enthüllungen führten zu Selbstzensur"](http://www.spektrum.de/news/snowden-enthuellungen-fuehrten-zu-selbstzensur/1408899), 2016
    - "Die Suchanfragen für verdächtige Begriffe bei Wikipedia sind nach dem NSA-Skandal eingebrochen. Das lässt sich durch den Chilling-Effekt erklären: Menschen schränken ihr an sich legales Verhalten ein, wenn sie überwacht werden."
    - ""Man möchte ja eigentlich gut informierte Bürger haben", sagte Penney gegenüber der "Post", "aber wenn die Menschen zu verängstigt oder abgeschreckt sind, sich über wichtige politische Angelegenheiten wie Terrorismus oder nationale Sicherheit zu informieren, ist das eine echte Bedrohung für eine angemessene demokratische Debatte.""

---

## Privatsphäre vs. Facebook
* siehe daily: "Cambridge Analytica: Wie Facebook-Daten verwendet werden können"

---

## Privatsphäre und Smartsphone-Apps: Presse-Meldungen
* ["Auf iPhone und AndroidLauschangriff auf dem Smartphone: Knapp 1000 Apps und Spiele hören ständig zu"](https://www.stern.de/digital/smartphones/lauschangriff-auf-dem-smartphone--knapp-1000-apps-und-spiele-lauschen-staendig-mit-7807034.html), stern, Januar 2018

---

## Privatsphäre und Smartsphone-Apps: Sonstiges
* Das Smartphone als Wanze?
    * siehe "Privatsphäre und Smartsphone-Apps: Presse-Meldungen" und "WhatsApp"

---

## Privatsphäre und Smartsphone
* Das Potential zur Verletzung der Privatsphäre ist mehrdimensional
    * eigene Privatsphäre
    * Privatsphäre Dritter
    * Verletzung durch Staat
        * siehe auch [Vorratsdatenspeicherung](https://digitalcourage.de/blog/2017/warum-ist-die-vorratsdatenspeicherung-gefaehrlich), [Petition](https://aktion.digitalcourage.de/civicrm/petition/sign?sid=2&reset=1)
    * Verletzung durch Unternehmen
    * Verletzung durch Kriminellen
* Hier eine mögliche Liste (oben: maximale Privatsphäre, nach unten: weniger Privatsphäre)
    * Kein Mobiltelefon (=> kein Tracking durch Mobilfunkanbieter)
    * Kein Smartphone (=> nur Tracking durch Mobilfunkanbieter; keine Apps)
    * Smartphone mit freiem Betriebssystem
        * Das [Librem 5](https://puri.sm/shop/librem-5/) (in Entwicklung, siehe unten)
        * Replicant, siehe unten
        * LineageOS, siehe unten
    * Nutzung freier Apps
        * F-Droid, siehe unten
        * Web-Browser, siehe unten
    * Nutzung einzelner kritischer Apps wie Google-Dienste und -Playstore
    * Nutzung besonders kritischer Apps wie WhatsApp oder dubiose Spiele
    * Smartphone mit Google-Diensten
    * Smartphone von einem Mainstream-Hersteller gekauft

---

## Smartsphone / Computer: Schichten
```
andere Rechner / "Cloud"        Webseiten und Dienste im Internet

                                ^  |
                                |  v

Smartphone / Computer           Apps                    (z. B. Web-Browser, Messenger,
                                                        Wetter-App, Spiel)

                                Hersteller-Apps         (oft nicht deinstallierbar)

                                Google-Android-Apps     (vorgeschrieben, nicht deinstallierbar,
                                                        jederzeit aktivierbar)

                                Android-Betriebssystem  (freie Software) --> LineageOS

                                Linux-Kernel            (freie Software)

                                Hardware                (das Gerät an sich)
```

---

## Privatsphäre: Sonstiges
* ["Die Big Brother Awards 2018: Von Windows 10, eHealth, Hessentrojanern und anderen Datenkraken"](https://www.heise.de/newsticker/meldung/Die-Big-Brother-Awards-2018-Von-Windows-10-eHealth-Hessentrojanern-und-anderen-Datenkraken-4028349.html), heise.de, 2018
    * "Dank der Telemetrie von Windows 10 hat Microsoft zum zweiten Mal einen Big Brother Award errungen" vs. Behörden-Vorgaben
    * Soma Analytics
    * "Was heute von ahnungslosen Verbrauchern praktiziert wird, die sich einen mithörenden Freund in die Wohnung stellen, bringt padeluun von Digitalcourage auf die Palme. Für ihn ist Amazons Alexa – stellvertretend für Siri, Google Assistant, Microsoft Cortana, Samsung Bixby und Nuance ausgezeichnet – "eine Abhörschnittstelle, die sich zum Beispiel als Wecker tarnt, aber ein allwissender Butler in fremden Diensten ist, der sich von mir höchstpersönlich ins Schlafzimmer tragen und an das weltweite Überwachungsnetz anschließen lässt.""
        * Spracherkennung an sich gut, "Doch der Verbund mit der dahinter liegenden Kontrollstruktur ist problematisch, unter vielen Aspekten."
    * 2015: Hello Barbie
* ["Privatsphäre auf dem Smartphone: So schützt Ihr Eure Daten "](https://www.androidpit.de/tipps-sicherheit-datenschutz-smartphone), 2016
    * siehe erster Benutzer-Kommentar

---

## WhatsApp: Schlüsselereignisse
* vor 2014: WhatsApp war das erste Jahr kostenlos und dann kostete etwas
* 2014: [Facebook kauft WhatsApp für 19.000.000.000 Dollar](http://www.faz.net/aktuell/wirtschaft/agenda/facebook-kauft-whatsapp-mark-zuckerbergs-mega-deal-12811209.html)
    * WhatsApp wird kostenlos
    * Unabhängigkeitsversprechen: ["WhatsApp founder Jan Koum claimed there would be no changes. “Here’s what will change for you, our users: nothing. WhatsApp will remain autonomous and operate independently,”](https://techcrunch.com/2016/08/25/whatsapp-to-share-user-data-with-facebook-for-ad-targeting-heres-how-to-opt-out/)
* 2016: ["Weitergabe der Nutzerdaten an Facebook"](https://de.wikipedia.org/wiki/WhatsApp#Weitergabe_der_Nutzerdaten_an_Facebook)
    * Versprechen gebrochen
* 2018: Datenskandal rund um Cambridge Analytica

---

## WhatsApp vs. deutsches Datenschutzrecht
* Verletzt geltendes Datenschutzrecht (automatisches Hochladen aller Kontaktdaten ohne die betreffenden Personen zu fragen)
    * ["Thüringens Datenschützer: Whatsapp wird meist rechtswidrig genutzt"](https://www.heise.de/newsticker/meldung/Thueringens-Datenschuetzer-Whatsapp-wird-meist-rechtswidrig-genutzt-3983437.html), heise.de, 2018
* ["WhatsApp: Telefonnummern-Upload illegal?"](https://www.kuketz-blog.de/whatsapp-telefonnummern-upload-illegal/), Kuketz-Blog, 2017

---

## WhatsApp vs. Nutzung in der Schule
* ["Whatsapp nutzen, um Schüler und Eltern auf dem Laufenden zu halten? Laut Datenschützern ist das unzulässig."](https://www.didacta-digital.de/lernen-lehren/finger-weg-vom-klassenchat-datenschutz-in-der-schule), didacta-digital.de, 2017

---

## Facebook und Kinder
- [Experten kritisieren Facebook wegen "Messenger Kids"](https://www.heise.de/newsticker/meldung/Experten-kritisieren-Facebook-wegen-Messenger-Kids-3955253.html), heise.de, 2018
    - "Facebook gefährdet die Gesundheit von Kindern – das zeigen viele Studien. In einem offenen Brief fordern Experten deshalb die Abschaltung des "Messenger Kids". Dessen Zielgruppe: Sechs- bis Zwölfjährige."
    - siehe auch User-Comments
    - ["Facebook will dein Kind"](https://www.heise.de/tr/artikel/Facebook-will-dein-Kind-3996594.html), 2018
        - "Mit Messenger Kids spricht Facebook erstmals gezielt Kinder unter 13 Jahren an. Die App soll harmlos sein und sieht auch so aus, doch Bildungsexperten wollen sie stoppen."

---

## WhatsApp und Monopolstellung
* Monopol-Zwang: wer WhatsApp nutzt, nötigt andere sich dem Facebook-Netzwerk anzuschließen
    * Es geht auch anders: **z. B. E-Mail: verschiedene unabhängige Anbieter; trotzdem kann jeder jedem Nachrichten schreiben**
    * WhatsApp könnte Gruppen und Broadcastlisten auch mit E-Mail integrieren, um Menschen ohne WhatsApp zu integrieren. Bisher nicht der Fall.

---

## WhatsApp: Ende-zu-Ende-verschlüsselt?
* ["Entwickler: Facebook kann WhatsApp-Chats einsehen – trotz Ende-zu-Ende-Verschlüsselung"](https://www.heise.de/mac-and-i/meldung/Entwickler-Facebook-kann-WhatsApp-Chats-einsehen-trotz-Ende-zu-Ende-Verschluesselung-4023461.html), heise.de, 2018
* ["Auch zweiter WhatsApp-Gründer verlässt Facebook"](https://www.heise.de/newsticker/meldung/Auch-zweiter-WhatsApp-Gruender-verlaesst-Facebook-4038141.html), heise.de, 2018
    * "es habe Streit mit Facebook um die Datennutzung sowie die strikte Verschlüsselung bei WhatsApp gegeben"

---

## WhatsApp und Facebook
* WhatsApp gehört dem Facebook-Konzern. Dort wird die Privatsphäre der Anwender ganz klein geschrieben.
    * Hintergrund-Artikel, z. B. ["Get your loved ones off Facebook."](http://www.salimvirani.com/facebook) (englisch)
* ["Was Facebook mit Ihren Whatsapp-Daten vorhat"](http://www.sueddeutsche.de/digital/datenschutz-was-facebook-mit-ihren-whatsapp-daten-vorhat-1.3511586), sueddeutsche.de, 2016
* ["Politische Werbung auf Facebook - Datenschützer schlagen Alarm"](http://faktenfinder.tagesschau.de/inland/datenschutz-facebook-101.html)

---

## WhatsApp: Kritik-Sammlung und Sonstiges
* [Umfassende Sammlung auf Wikipedia](https://de.wikipedia.org/wiki/WhatsApp#Sicherheit_und_Kritik)
* [6 gute Gründe zu wechseln](https://netzpolitik.org/2017/hintertuer-bei-whatsapp-sechs-gute-gruende-den-messenger-zu-wechseln/), netzpolitik.org
* https://www.basicthinking.de/blog/2017/06/27/whatsapp-urteil/
* http://www.zeit.de/digital/datenschutz/2017-06/whatsapp-richter-bad-hersfeld-datenschutz
* Kostenlos, aber nicht frei(heitsgewährend) (transparent etc., siehe Freie Software)
    * Proprietäre Lizenz, siehe z. B. [Wikipedia](https://de.wikipedia.org/wiki/Whatsapp)

---

## Facebook: Kritik-Sammlung
* [Umfassende Sammlung auf Wikipedia](https://de.wikipedia.org/wiki/Kritik_an_Facebook)
* [Umfassende Sammlung auf der persönlichen Webseite von Richard Stallman](https://stallman.org/facebook.html)

---

## WhatsApp: Wege zum Beenden der WhatsApp-Nutzung / freie Apps
* Solange WhatsApp und Facebook sich privatsphärenfeindlich verhalten, ist eine weitere Nutzung nicht sinnvoll.
* Wege zum Beenden der Nutzung von WhatsApp
    * Freie Messenger-App installieren
    * Freunde bitten, *parallel* zu WhatsApp die freie App zu installieren
* Beispiel einer freien Messenger-App:
    - **Riot** / Matrix
        - Anmeldung mit Nutzername und Passwort
        - optionale Ende-zu-Ende-Verschlüsselung
        - Gruppenfunktion
* Listen
    - ["Alternativen zu WhatsApp und Threema – Instant Messenger"](https://digitalcourage.de/digitale-selbstverteidigung/alternativen-zu-whatsapp-und-threema-instant-messenger)
        - "Weg vom WhatsApp-Monopol"
        - Auswahl von empfohlenen Messengern
            - Riot / Matrix
            - Signal
            - Conversations
        - Auswahl von nicht empfohlenen Messengern
            - Telegram
            - Kontalk
            - Threema
            - WhatsApp
    - [prism-break.org-Liste zu Messenger-Apps](https://prism-break.org/de/categories/android/#instant-messaging)
* Weitere freie Messenger-Apps
    - [Wire](https://wire.com/de/)
    - [Briar](https://briarproject.org/about.html) (kein zentraler Server)
* Beispiele von nicht freien Messenger-Apps:
    - Threema: nicht frei, aber schon mal ein Anfang

---

## Alternative zu Whatsapp/Threema-Gruppe
* Alternative Messenger mit Gruppenfunktion verwenden
    * siehe oben
    * siehe auch [fsfe-Mail 2018](https://lists.fsfe.org/pipermail/fsfe-de/2018-April/010244.html)
* **Mailing-Liste**
    * [Was ist das?](https://de.wikipedia.org/wiki/Mailingliste)
        * "Eine Mailingliste (engl. mailing list) bietet einer geschlossenen Gruppe von Menschen die Möglichkeit zum Nachrichtenaustausch in Briefform, also eine Vernetzung mit elektronischen Mitteln. Dieser Nachrichtenaustausch ist innerhalb der Gruppe öffentlich. Besonders häufig sind Mailinglisten im Internet, wo sie mittels E-Mail realisiert werden."
    * Gut geeignet für Vereine
    * Wie kann man das einrichten?
        * siehe z. B. [Posteo](https://posteo.de/hilfe/gibt-es-mailinglisten-bei-posteo)

---

## Internet-Grundlagen: Der Web-Browser
* Bekannte Browser
    * **Mozilla Firefox**
        * mittels F-Droid (siehe unten) unter **Fennec** zu finden
            * Hinweis eines Lesers [dieses Artikels](https://www.kuketz-blog.de/f-droid-und-app-alternativen-android-unter-kontrolle-teil3/): "Als Laie und regelmäßiger Leser fiel mir folgendes bei F-Droid auf: Der Download des Browsers „Fennec“ produziert die Meldungen „Diese Anwendung bewirbt nicht freie Erweiterungen“ sowie „Diese Anwendung verfolgt und versendet Ihre Aktivitäten“. Die gleichen Meldungen werden beim Firefox angezeigt und bei diesem zusätzlich „Der Originalcode ist nicht völlig frei“.", Antwort in den Artikel-Kommentaren.
    * Google Chrome - nicht empfohlen, keine freie Software
        * Basiert zwar auf dem Freien-Software-Projekt **Chromium**, aber plus proprietäre Google-Tracking-Erweiterungen
    * [Iridium Browser](https://iridiumbrowser.de/)
        * Freie Variante auf Basis von Chromium
            * [hier gefunden](https://lists.fsfe.org/pipermail/fsfe-de/2018-April/010213.html), 2018
    * Microsoft Edge - keine freie Software
    * Opera - keine freie Software

---

## Basisschutz im Internet: Überwachungsblocker / Werbeblocker
* Werbeblocker sind vor allem Überwachungsblocker
* Für Mozilla Firefox:
    * [uBlock Origin](https://addons.mozilla.org/firefox/addon/ublock-origin/)
        * [Anleitung](https://www.kuketz-blog.de/ublock-origin-schutz-gegen-tracker-und-werbung/)
* Eine andere Suchmaschine als Google verwenden:
    * startpage.com sucht via Google, aber (zumindest) die Suchanfragen des Nutzers bleiben gegenüber Google anonym
* Hintergrund: Werbebranche
    * Vieles der digitalen Werbung enthält Anteile, die die Daten des Nutzers ungefragt abgreifen
    * Bestimmte Arten von Internet-Werbung stellen ein IT-Sicherheitsrisiko dar
    * Tipp: Internet-Angebote, die viel und/oder aufdringliche Werbung schalten, gibt es auch die Option diese **einfach nicht zu verwenden**

---

## Internet-Grundlagen: E-Mail und E-Mail-Anbieter
* Aktuelle Situation für viele:
    * Startseite mit Werbung (= Tracker) und irrelevanten Nachrichten überfrachtet
    * irgendwann falsch geklickt, dann immer Werbung z. B. für Treppenlifte
    * Beispiel yahoo.com
        * ["Yahoo muss erneut Massenhack beichten: Eine Milliarde Opfer"](https://www.heise.de/newsticker/meldung/Yahoo-muss-erneut-Massenhack-beichten-Eine-Milliarde-Opfer-3570674.html), heise.de, 2016
* Positive Beispiele:
    * **Posteo.de** oder **Mailbox.org**
        * keine Werbung; 1 EUR / Monat
* E-Mails auf dem eigenen Rechner mit **Mozilla Thunderbird**
    * für Android-Smartphone: **K-9-Mail**
* **HTML**-Schutz sollte aktiviert sein
    * siehe z. B. https://posteo.de/blog/update-informationen-zu-efail-meldungen
        * "So blockieren Sie das Nachladen externer Inhalte/das Ausführen von HTML-Code"
    * siehe https://dot.kde.org/2018/05/15/efail-and-kmail
* Frage: E-Mails nur auf dem Anbieter-Server belassen oder auf eigenen Rechner laden?
    * Je nachdem wie wichtig einem alte E-Mails sind, siehe dieses Beispiels eines anderen Online/Cloud-Dienstes ["Deleting the Family Tree"](http://www.slate.com/articles/technology/future_tense/2015/04/myfamily_shuttered_ancestry_com_deleted_10_years_of_my_family_history.html)
        * "When Ancestry.com shuttered its social network for relatives, it erased 10 years’ worth of my family’s correspondence and memories.", 2015

---

## Software und Vertrauen
* Quellcode sollte frei (einsehbar, änderbar) sein, ansonsten z. B.
    * Verletzung der Privatsphäre auf Smartphones
    * siehe Motorsteuersoftware bei Dieselmotoren
* Vertrauen ist gut, unabhängige Kontrolle und Einflussmöglichkeiten sind aber dennoch notwendig
    * vergleiche Prozesse in der Demokratie
    * Kontrolle bei Software passiert über den Quellcode
    * siehe auch BSI (Bundesamt für Informationssicherheit)
* Smartphone = Computer
* Sieht Google alles?
    * Nicht nur Google, siehe [Artikel zu Datenlieferanten](https://www.kuketz-blog.de/wir-sind-nur-noch-datenlieferanten-not-my-data-teil1), 2015
* Nicht-transparente Software auf Millionen Geräten mit Mikrofonen, Kameras und Antennen => hohes Missbrauchspotential

---

## Was ist Freie Software?
- Was ist Freie Software?
    - siehe [Wikipedia-Artikel](https://de.wikipedia.org/wiki/Freie_Software)
    - "freiheitsgewährend"
    - auch OpenSource genannt
    - Software ist etwas besonders, weil es das Verhalten eines Gerätes bestimmt und der Nutzer nicht sieht, was hinter den Kulissen passiert
    - Ausgleich zwischen dem krassen Informationsgefälle zwischen Entwickler und Nutzer
    - [Werkzeugkasten Freie Software in der Bildung](https://www.technologiestiftung-berlin.de/fileadmin/daten/media/publikationen/161213_Medien_in_die_Schule-Werkzeugkasten_Freie_Software.pdf)
- Fragen zu Software allgemein
    - Transparenz?
    - Mitbestimmung?
    - Kontrolle?
- [Android](https://de.wikipedia.org/wiki/Android_(Betriebssystem))
    - freie [Apache-Lizenz](https://de.wikipedia.org/wiki/Apache-Lizenz)
    - basiert auf [Linux](https://de.wikipedia.org/wiki/Linux_(Kernel)), GPLv2-lizensiert
- Ein Weg: zusammen mit anderen Menschen digitale Eigenkontrolle erreichen (-> z. B. in der KDE-Gemeinschaft)

---

## Freie Software: Werkstatt-Vergleich
* "Aber muss ich mich mit den komplexen Details der Software auskennen, um freie Software verwenden zu können?"
* Werkstatt-Vergleich
    * Wenn ich Probleme mit dem Auto habe, gehe ich zur Werkstatt meines Vertrauens. Dafür muss ich nicht verstehen, wie das Auto genau funktioniert.
    * Mit freier Software ist Vergleichbares möglich: Wenn ich Probleme mit meinem Computer/Smartphone habe, gehe ich zum Computershop meines Vertrauens
        * Denn: mit freien Software-Lizenzen ist es erlaubt, dass jeder helfen darf

---

## Frage: Welche Apps sind nötig? Welche kann ich löschen?
* Überlegen: was brauche ich?
* Auf freie Software achten
* Unnötige Google-Apps und Hersteller-Apps sind meist nicht deinstallierbar
    * beim nächsten Kauf auf Google-freies Android achten, siehe "Android-Varianten ohne Google / Freies Betriebssystem / LineageOS"
* Ansonsten:
    * Für jede App einzeln
        * Anklicken
        * Wenn eine Meldung zum Zustimmen von speziellen Geschäftsbedingungen erscheint, zumachen und löschen
    * Für die restlichen Apps:
        * Im Internet danach suchen und schauen was da steht.
        * Brauche ich das?
            * Nein? -> Löschen
            * Ja? -> nach freier Alternativen suchen
    * Für die restlichen Apps:
        * Anklicken
        * Erschließt sich mir der Sinn der App? Nein? -> Löschen

---

## Frage: Wie sichere ich mich ab, dass nicht jeder auf mein Handy gucken kann?
* Standardmäßig kann nicht "jeder" ("wer ist jeder?") auf das Smartphone schauen, sondern "nur"
    * Profis
    * Unternehmen, von denen man Apps installiert hat (je nach Berechtigungen), z. B. Facebook bei WhatsApp
    * Google bei Android
* Ein Weg der Absicherung:
    * alles Unnötige löschen
    * nur freie Software verwenden
    * freien Web-Browser mit Überwachungsblocker (Werbeblocker) verwenden (siehe oben)
    * beim nächsten Smartphone drauf achten, ein freies Betriebssystem zu wählen (siehe unten)

---

## Frage: Was geschieht auf Android mit den Daten in meinem Adressbuch?
* Zunächst sind die eingegeben Daten ("Kontakte") nur auf dem eigenen Gerät vorhanden
    * Wer hat Zugriff auf die Daten? -> zunächst niemand, außer man selbst
* Bei Nutzung eines Google-Kontos *können* die Kontakte mit diesem Konto verknüpft werden
    * Im Fall der Verknüpfung werden alle Kontakte auf Google-Server hochgeladen.
    * Warum? -> Die Kontaktdaten sind im Falle z. B. des Geräteverlusts oder -wechsels bequem gesichert.
    * Wer hat Zugriff auf die Daten? -> Die Kontakte können von Google eingesehen werden.
        * Probleme:
            * Eine Verarbeitung zu unbekannten Zwecken findet statt.
            * Zugriff von Überwachungsdiensten ist einfach möglich.
* Bei Nutzung von WhatsApp
    * ALLE Kontakte (auch die von Dritten ohne WhatsApp) werden auf WhatsApp-Server (und damit wahrscheinlich auch auf Facebook-Server) hochgeladen.
    * Warum? -> Einerseits damit der Nutzer in WhatsApp Kontakte finden kann; aber auch damit WhatsApp und Facebook möglichst viel über die Beziehungen seiner Nutzer erfährt.
    * Wer hat Zugriff auf die Daten? -> WhatsApp und Facebook
        * Probleme:
            * Die Daten werden zu allen möglichen Zwecken ausgewertet.
            * Private Beziehungsnetzwerke sind dem Unternehmen bekannt.
            * Daten werden gespeichert und können in Zukunft zu noch unbekannten Zwecken verwendet werden.
            * Zugriff von Überwachungsdiensten ist einfach möglich.
    * Hinweis: ["Whatsapp erhöht Mindestalter auf 16 Jahre"](http://www.sueddeutsche.de/digital/whatsapp-mindestalter-1.3957013), 2018
        * "Die DSGVO sieht bei Jugendlichen bis 16 Jahren eine Zustimmungspflicht der Eltern zur Datenverarbeitung vor."
* Bei Nutzung von anderen dubiosen Apps, die Zugriff auf die Kontakte verlangen
    * Warum? -> oft: Daten sammeln
    * Probleme siehe oben

---

## Frage: Wie sicher sind die Kalender im Netz?
* Wer sieht meine Daten?
    * Normalerweise hat nur der Anbieter des Cloud-Dienstes Zugriff auf alle synchronisierten Kalenderdaten
    * Vertraue ich dem Anbieter?
    * Gilt für jeden Online/Cloud-Dienst: im Falle einer Sicherheitslücke auf Anbieterseite (z. B. "Meltdown/Spectre" 2018) können die Daten auch öffentlich werden; je nachdem was der Angreifer damit vorhat.

---

## Frage: Vorteile/Nachteile ohne/mit Google-Konto
* Was geht ohne Google-Konfo nicht?
    * (wer auf den Google-Komfort auf Kosten der Privatsphäre von sich und anderen verzichten möchte, hat keine Nachteile)

Ohne Google-Konto: keine Daten-Synchronisation mit den Google-Servern, d. h. keine bequeme Datensicherung:

* Vorteil: es werden auch keine Bilder ungewünscht synchronisiert
* Manuelle Datensicherung nötig mittels USB-Verbindung
    * Überlegen: was ist mir wichtig? --> z. B. ...
        * Kontakte / Kontakte selber sichern --> siehe "Android-Kontakte: Backup/Datensicherung ohne Google"
        * Bilder
        * Downloads
    * Überlegen: auf was kann ich im Falle eines Datenverlustes verzichten? --> z. B. ...
        * alte SMS-Nachrichten

Ohne Google-Konto kein Google Playstore (zum Installieren von Apps):

* Alternative: F-Droid
    * freie Apps installieren
    * oder freie Software-Apps als APK herunterladen (auf vertrauenswürdige Quelle achten)

* Automatische System-Updates?

* siehe auch "Google-Konto löschen"

---

## Frage: Gefahren von Lokalisationsdiensten? Kann sich da jemand einhacken?
* Wer sieht meine Daten, wenn ich GPS verwende?
    * Wenn Google verwendet wird, dann Google und wahrscheinlich die Überwachungsdienste
    * Je nach App und aktuellen Sicherheitslücken auch die versierte Öffentlichkeit
        * ["Fitness-App verrät sensible militärische Geodaten"](http://www.sueddeutsche.de/digital/strava-fitness-app-verraet-sensible-militaerische-geodaten-1.3845538), 2018
    * Wenn auf dem eigenen Gerät freie Software verwendet wird, die nicht mit einem Online-Dienst verknüpft sind, bleiben die Daten privat, z. B. ...
        * Lokalisierung auf OpenStreetMap oder Routenplaner-App
* Prinzipiell sieht der Mobilfunkanbieter mittels Funkzellen immer den ungefähren Standort des Mobiltelefons
    * Vorratsdatenspeicherung?
    * Einige Menschen lehnen daher ein eigenes Mobiltelefon grundsätzlich ab; siehe z. B. https://stallman.org/rms-lifestyle.html -> Cellular Phones
* Können Einbrecher das nutzen?
    * Solange man kein "Smart-Home" hat, sollte Smartphone-seitig nichts passieren.
    * Außer man postet bei Facebook öffentlich, wann man in Urlaub fährt.

---

## Android-Kontakte: Backup/Datensicherung ohne Google
Eigene Offline-Sicherung:

* Kontakte mit keinem Online-/Cloud-Konto verknüpfen
* Regelmäßiger **Export in eine vcf-Datei**: ["Android: Kontakte sichern - so klappt's"](https://www.heise.de/tipps-tricks/Android-Kontakte-sichern-so-klappt-s-3931602.html), heise.de, 2018
    * und dann diese vcf-Datei auf den eigenen Rechner kopieren

Online/"Cloud"-Lösung ohne Google, also die Daten automatisch auf einen anderen fremden Rechner sichern, der aber zumindest nicht von Google betrieben wird:

* zum Beispiel mit einem **Nextcloud**-Server: ["App-Tipps: Nextcloud mit dem Smartphone verbinden"](https://kaffeeringe.de/5459/nextcloud-perfekt-mit-dem-smartphone-verbinden/), 2017
    * dort wird auch erklärt, wie man seinen Kalender mit Nextcloud synchronisiert
* Vorteil: bequem (wenn man einmal einen Nextcloud-Anbieter ausgewählt und die Apps eingerichtet hat)
* Nachteil: die Kontaktdaten sind immer noch irgendwo auf fremden Rechnern
    * Vorteil: man kann sich den Anbieter aussuchen und für seine Dienste bezahlen (siehe z. B. https://eqipe.ch/nextcloud/)

---

## Freie Software in der öffentlichen Verwaltung
* ["Stadt Dortmund stellt Weichen Richtung Open Source"](https://www.heise.de/newsticker/meldung/Stadt-Dortmund-stellt-Weichen-Richtung-Open-Source-4017620.html), 2018
    * ["Wer das Monopol verlässt, wird unter Druck gesetzt"](https://www.tagesspiegel.de/weltspiegel/sonntag/cyber-attacken-auf-staatliche-it-wer-das-monopol-verlaesst-wird-unter-druck-gesetzt/19628246-2.html), tagesspiegel, 2017
        * "„Der Wechsel zu Linux könnte von Microsoft als Bedrohung des Monopols gesehen werden“, warnte die Direktion in einem internen Memo, das Investigate Europe vorliegt. Das könne zu „Aktionen führen, um diese Politik der Gendarmerie zu diskreditieren“."

* ["Bundescloud: Open-Source mit Nextcloud statt Dropbox oder Google Drive"](https://www.heise.de/ix/meldung/Bundescloud-Open-Source-mit-Nextcloud-statt-Dropbox-oder-Google-Drive-4026111.html), heise.de, 2018
    * "Künftig setzt die Bundesverwaltung auf Nextcloud. Rund 300.000 Mitarbeiter in Behörden und Ministerien sollen mit der Open-Source-Software arbeiten."
    * Die Gründe passen zum Wunsch nach Privatsphäre.

---

## Beispiel LiMux / Öffentliches Geld ("Public Money? Public Code!")
- [LiMux](https://de.wikipedia.org/wiki/LiMux)
- [Open-Source-Software in öffentlichen Einrichtungen](https://de.wikipedia.org/wiki/Open-Source-Software_in_%C3%B6ffentlichen_Einrichtungen)
    - insbesondere auch die [lange Liste von Beispielen](https://de.wikipedia.org/wiki/Open-Source-Software_in_%C3%B6ffentlichen_Einrichtungen#Beispiele_von_Migrationsprojekten)
- Public Money? Public Code!
    - https://publiccode.eu/de (--> Intro-Video auf deutsch)
- https://muenchen-bleibt-frei.de/
    - Unabhängigkeit von einzelnen Anbietern
    - Makroökonomische Vorteile
    - Digitale Teilhabe
    - Nachhaltigkeit
    - Sicherheit und Datenschutz
- wurde 2017 leider abgesägt (Lobbyismus)
- 2018: ["Open Source in Schweizer Unternehmen und Behörden im Aufwind"](https://www.heise.de/newsticker/meldung/Open-Source-in-Schweizer-Unternehmen-und-Behoerden-im-Aufwind-4088182.html)

---

## Probleme mit proprietärer Software in der öffentlichen Verwaltung
* ARD-Doku: Das Microsoft-Dilemma, 2018
    * siehe auch https://stallman.org/microsoft.html

* ["Bund will Windows 10 über Bundesclient sicher nutzen können"](https://www.heise.de/ix/meldung/Bund-will-Windows-10-ueber-Bundesclient-sicher-nutzen-koennen-3907088.html), 2017
    - "Um den Einsatz von Windows 10 rechtskonform und sicher zu gestalten, lässt der Bund für seine Verwaltung den sogenannten Bundesclient entwickeln. Über dessen Sicherheitseigenschaften schweigt sich das Bundesinnenministerium allerdings aus."

---

## LibreOffice
* Früher OpenOffice von Sun Microsystems
* Dann Übernahme durch Oracle und Abspaltung als LibreOffice unter der neuen Stiftung The Document Foundation
* [Mehr zur Geschichte](https://de.wikipedia.org/wiki/LibreOffice#Geschichte)
* The Document Foundation
    * "„Vereinfacht gesagt, ist das Ziel der Stiftung die Entwicklung eines Repertoires digitaler Produktivitäts- und Kreativitätswerkzeuge der nächsten Generation, indem eine nachhaltige, unabhängige und umfassende Community zur Entwicklung Freier und Open-Source-Software [FOSS] auf der Basis offener Standards, gefördert wird.“"
* [Webseite und Download](https://www.libreoffice.org/)
* In der Schule
    * http://www.medien-in-die-schule.de/werkzeugkaesten/werkzeugkasten-freie-software/software/libreoffice-office-programmpaket/
    * https://luki.org/2016/10/libreoffice-trifft-auf-schule/

---

## Freie Software: Vorteile / Nachteile
- Vorteile/Nachteile:
    - siehe ["Vergleich mit Handschellen"](https://www.gnu.org/philosophy/practical.de.html)
    - siehe ["Wann Freie Software (aus praktischer Sicht) nicht besser ist"](https://www.gnu.org/philosophy/when-free-software-isnt-practically-superior.html)
    - Proprietäre SW
        - Praktisch für Unternehmen: Investitionsschutz
        - Praktisch für Endnutzer: viel - möglicherweise komfortable - Software
            - auf Kosten von gewissen Freiheiten und der Privatsphäre

---

## Freie Software erkennen
- Wie erkenne ich freie Software?
- An der Lizenz erkennen: z. B. [**GPLv3**](https://de.wikipedia.org/wiki/GNU_General_Public_License)
    - Tipp: das betreffende Programm in der Wikipedia suchen und nach der Lizenz schauen
    - "proprietär" ist das Gegenteil von freier Software
- Weitere freie Lizenzen: Apache, BSD, MIT, ...
- Wo sehe ich die Lizenz?
    - Im Google-Playstore kann man die Software-Lizenz von Apps leider nicht erkennen
        * d. h. entweder einzeln im Internet nachprüfen oder F-Droid (siehe unten) verwenden
    - Im F-Droid steht die Lizenz bei jeder App dabei

---

## Beispiel: John-Deere-Traktor
* ["Finger weg von deinem eigenen Traktor: John Deere wehrt sich gegen "Traktor-Modding""](http://www.heise.de/newsticker/meldung/Finger-weg-von-deinem-eigenen-Traktor-John-Deere-wehrt-sich-gegen-Traktor-Modding-2616920.html), 22.04.2015, heise.de
    * "John Deere betreibt Lobbyismus, um Farmer weiterhin davon abzuhalten, ihre hochmodernen Traktoren umzuprogrammieren. Der Hersteller begründet den Schritt mit dem Schutz geistigen Eigentums."
    * "Das Beispiel der Farmer weist auf ein wachsendes Problem hin in einer Welt mit immer mehr smarten Geräten: Hersteller können ihre Marktmacht missbrauchen, um die Besitzrechte der Kunden massiv einzuschränken. Der Verbraucher hat ohne eine klare rechtliche Klärung meist das Nachsehen."
* dazu ["We Can’t Let John Deere Destroy the Very Idea of Ownership"](http://www.wired.com/2015/04/dmca-ownership-john-deere), 21.04.2015, engl.
    * "John Deere and General Motors want to eviscerate the notion of ownership. Sure, we pay for their vehicles. But we don’t own them. Not according to their corporate lawyers, anyway."
* dazu ["Farmers are pirating John Deere tractor software to stick it to the man"](https://www.extremetech.com/computing/246314-farmers-pirating-john-deere-tractor-software-stick-man), 2017, engl.
    * Eigene Reparatur des Traktors wegen Hersteller-Software nicht möglich

---

## F-Droid: der freie App-Store
- Frage: Kann F-Droid parallel zum Google-Playstore verwendet werden? --> JA
    - Das Google-Konto kann auch später entfernt werden (siehe "Google-Konto löschen")
- **F-Droid installieren**
    - https://f-droid.org besuchen, F-Droid herunterladen und installieren, [deutsche Anleitung](https://www.kuketz-blog.de/f-droid-und-app-alternativen-android-unter-kontrolle-teil3/), 2017
- [Allgemeine Informationen auf Wikipedia](https://de.wikipedia.org/wiki/F-Droid)
- Neuigkeiten zu F-Droid
    - ["Neue Zusammenarbeit, um Überwachung aufzudecken"](https://f-droid.org/de/2017/12/14/new-collaborations-on-exposing-tracking.html), Dez 2017
        - "Seit 2010 arbeitet die F-Droid-Community daran, ausschließlich 100% überprüfte Freie Software anzubieten und alle Formen der Überwachung, Werbung und “problematische Merkmale”, die für gewöhnlich in Apps gefunden werden, kenntlich zu machen. F-Droid bietet ein komplettes App-Ökosystem, in dem Anwender aktiv auf Überwachung und Werbung in den Apps hingewiesen werden, und sich informiert entscheiden können. Wir haben das durch die Arbeit vieler engagierter Freiwilliger erreicht, die Apps, so wie sie vorlagen, überprüften und die darin gefundenen Dinge kennzeichneten."
        - "Forscher von Exodus Privacy und Yale Privacy Lab arbeiten daran, einen Schritt weiter zu gehen, indem sie Werkzeuge schaffen, die das Auffinden all der verschiedenen Formen der Überwachung, die Apps beinhalten können, zu einem automatisierten Prozess machen."
            - https://privacylab.yale.edu/
                - "The Secret Sharers: Are Your Favorite Apps Watching You? - “Many of the most popular apps in the Google Play store contain trackers: you download the app, and the trackers sweep up a variety of data… Such tactics create a ‘power asymmetry’ for marketers"
        - "Milliarden von Anwendern auf der Welt verwenden mittlerweile mobile Plattformen, die auf Freier Software basieren, wie eben Android. Für viele sind Mobilgeräte das Hauptportal ins Internet. Diese Geräte sammeln und übertragen häufig vertrauliche Details ihrer Nutzer, wie Verhalten und Standort, mittels komplizierter und versteckter Verfahren."

---

## Google-Konto löschen
* Menü -> Einstellungen -> Konten -> Google-Konto -> auf die E-Mail-Adresse drücken -> oben rechts "Mehr" (oder "...") -> Konto entfernen
* ["Google-Konto vom Android-Handy löschen – so geht’s"](https://www.giga.de/downloads/android-6.0-marshmallow/tipps/google-konto-loeschen-von-android-handy-so-geht-s/), 2017
* siehe auch "Frage: Vorteile/Nachteile ohne/mit Google-Konto"

---

## App-Listen (1)
* Beispielzusammenstellung von Apps auf LineageOS ohne Google mit F-Droid als App-Store
    * **OpenCamera** - Kamera-App
    * **QR Scanner** - App zum Scannen von QR-Codes
    * **Vanilla Music** - Musikspieler
    * Web-Link auf [Wetter FFM](http://wetterstationen.meteomedia.de/station=106370&wahl=vorhersage) - Wettervorhersagediagramm
    * **Ghost Commander** - Dateimanager
        * enthält einen Texteditor zum Bearbeiten einer Textdatei für Notizen, z. B. "Notizen.txt"
    * **Book Reader** - Lesen von E-Books. Links zu freie Bibliotheken sind enthalten
    * **[Maps](https://f-droid.org/packages/com.github.axet.maps/)** - (Offline) Karten und Navigation (Auto, Fahrrad, Fuß), basiert auf OpenStreetMap / Routenplaner
        * Alternative zu GoogleMaps
    * **[Transportr](https://transportr.grobox.de/)** - freie Variante des DB-Navigators; funktioniert weltweit
    * **Riot.im** - basiert auf dem Matrix-Protokoll, freie Variante von WhatsApp
        * [(eher technischer) Artikel zu Matrix / Riot](https://www.golem.de/news/echtzeitkommunikation-ausprobiert-willkommen-in-der-matrix-1703-126197.html), golem.de, 2017
    * **Silence** - verschlüsselte SMS schreiben
    * **Fennec** - Mozilla Firefox

* Weitere Apps
    * **AnySoftKeyboard** - bessere Tastatur in verschiedenen Sprachen
    * **K-9-Mail** - E-Mail-Client, vergleichbar mit Mozilla Thunderbird
    * **c:geo** - Geo-Caching-Helfer
    * **A Photo Manager** - Alternative zur Gallerie
    * **OsmAnd** - OpenStreetMap/Navigation

---

## App-Listen (2)
Von anderen gepflegte Listen

* ["F-Droid und App-Alternativen – Android unter Kontrolle Teil3"](https://www.kuketz-blog.de/f-droid-und-app-alternativen-android-unter-kontrolle-teil3/), 2017
    * Der Artikel geht auf die "2.1 Die Intransparenz der Datenverarbeitung" ein
    * Es wird die "2.3 Installation der F-Droid App" erklärt
    * Anschließend gibt es "3. Unsere App-Vorschläge aus dem F-Droid Store" inklusive Installationshinweise
        * Internet, Browser, E-Mail, Kalender, Messenger, Multimedia, PDF-Viewer, Systemprogramme, Navigation und Karten, Wetter
        * Freie soziale Netzwerke: z. B. Diaspora
        * Nextcloud

* [Prism-Break-Liste für Android](https://prism-break.org/de/categories/android/)
    * Zu jeder bekannten proprietären App werden Alternativen aufgeführt
    * Bereiche: Anonymisierungsnetzwerke, App Store, Authentication, Bookmark Sync, Cellular, DNS, E-Mail-Konten, E-Mail-Alternativen, E-Mail-Programme, E-Mail-Verschlüsselung, Enterprise Suite, Dateisynchronisation, Finanzen, Sofortnachrichten, IRC, E-Mail-Server, Medienveröffentlichung, Mesh-Netzwerke, Operating Systems (Mobile), Password Managers, Produktivität, SIP-Server, Soziale Netzwerke, Video & Stimme, VPN-Konten, VPN-Programme, Webbrowser-Plugins, Web-Browser, Web-Hosting, Websuche, Karten
    * [auch für andere Plattformen](https://prism-break.org/de/) außer Android

* ["Alternativen zu WhatsApp und Threema – Instant Messenger"](https://digitalcourage.de/digitale-selbstverteidigung/alternativen-zu-whatsapp-und-threema-instant-messenger)
    * [Liste von mobilen Instant-Messengern](https://de.wikipedia.org/wiki/Liste_von_mobilen_Instant-Messengern) (nicht alle frei)

* [Liste von freien und Open-Source-Android-Applikationen auf Wikipedia](https://de.wikipedia.org/wiki/Liste_von_freien_und_Open-Source-Android-Applikationen)
    * [Weitere Auswahl von Apps](https://de.wikipedia.org/wiki/F-Droid#Auswahl_verf%C3%BCgbarer_Apps)

---

## Android-Varianten ohne Google / Freies Betriebssystem / LineageOS
- [**LineageOS**](https://de.wikipedia.org/wiki/LineageOS) (für Einsteiger empfohlen)
- [**Replicant**](https://de.wikipedia.org/wiki/Replicant_(Betriebssystem)) (nur freie Software und freie Gerätetreiber)
    - [Webseite](https://www.replicant.us/)
    - [Beispiel-Shop](https://tehnoetic.com/tehnoetic-s2-phone-replicant)
- Artikel-Serie zu [Android ohne Google](https://www.kuketz-blog.de/your-phone-your-data-teil1)
- Aktion: ["Befreie Dein Android!"](https://fsfe.org/campaigns/android/android.de.html)

---

## Wo neue/gebrauchte Geräte kaufen?
- gebraucht, z. B.
    - [Technoethical](https://tehnoetic.com/),
        * [gebrauchtes Samsung S2](https://tehnoetic.com/tehnoetic-s2-phone-replicant)
    - **E-Bay: "LineageOS"** - Handys ohne Vertrag
        - [Ergebnisliste](https://www.ebay.de/sch/i.html?_from=R40&_trksid=p2380057.m570.l2632.R2.TR4.TRC2.A0.H0.Xlineageo.TRS0&_nkw=lineageos&_sacat=9355), 2018 findet man z. B.
            * ["Motorola Moto G 16GB schwarz, incl. Lineageos 7.1.2 + root"](https://www.ebay.de/itm/Motorola-Moto-G-16GB-schwarz-incl-Lineageos-7-1-2-root/253600511289?hash=item3b0bc4b539:g:4Z0AAOSwP4darAPn)
            * ["Samsung Galaxy S5 Root LineageOS 14.1"](https://www.ebay.de/itm/Samsung-Galaxy-S5-Root-LineageOS-14-1/183201583095?hash=item2aa7aa4ff7:g:kqcAAOSwhZ9a5fGe)
- neu
    - TODO: mit LineageOS vorinstalliert

---

## Smartphone ohne Android: Plasma Mobile und Librem 5
- [Plasma Mobile](https://dot.kde.org/2017/09/14/plasma-mobile-and-purisms-librem-5-free-smartphone)
    - von [KDE](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future)
        - "A world in which everyone has control over their digital life and enjoys freedom and privacy."
        - [Details](https://dot.kde.org/2016/04/05/kde-presents-its-vision-future)
        - siehe auch Diskussion zum [Ziel: Privacy Software](https://phabricator.kde.org/T7050)
    - [Hilfe nötig](https://vizzzion.org/blog/2017/09/help-us-create-a-privacy-focused-free-software-smartphone/)
    - zum Beispiel auf dem **[Librem 5](https://puri.sm/shop/librem-5/)** - Telefon mit Fokus auf Sicherheit und Privatsphäre
        - "Can I run Android apps? - Not day 1. However there is a lot of interest in including a isolation layer that will be able to power Android applications natively. We have added that as a stretch goal to quantify the effort."
- "Sicherheit vs. Privatsphäre"
    - Siehe ["Die Update-Problematik bei Android – Android unter Kontrolle Teil2"](https://www.kuketz-blog.de/die-update-problematik-bei-android-android-unter-kontrolle-teil2/), 2017
    - Siehe auch https://www.kuketz-blog.de/your-phone-your-data-light-android-unter-kontrolle/

---

## Ausblick
- Ausblick PC:
    - Freie Software auf Windows installieren
    - Später: Linux installieren
        - Linux lernen
            - zum Beispiel Online 2018: https://open.hpi.de/courses/linux2018
        - an Schulen
            - http://www.rhs-dreieich.de/unterrichtsangebote/arbeitsgemeinschaften/linux/
            - https://wiki.ubuntuusers.de/Edubuntu/Schulen/
- Bücher
    - Roman: Zero
    - Roman: The Circle
    - Sachbuch: Digitale Drecksarbeit
    - Sachbuch: Sie wissen alles
    - Sachbuch: Markus Morgenroth - Sie kennen dich! Sie haben dich! Sie steuern dich!
- Lesenswerte Blogs:
    - https://www.kuketz-blog.de/empfehlungsecke/
    - weitere?

---
