LineageOS 14.1 auf Motorola G (1. Gen.)
=======================================

OK

<!-- toc -->

- [Bestellen](#bestellen)
- [Erstes Auspacken](#erstes-auspacken)
  * [Google-Dienste-Uninstall fail](#google-dienste-uninstall-fail)
  * [WLAN fail](#wlan-fail)
  * [USB-Datentransfer fail](#usb-datentransfer-fail)
- [Google-Freies Lineage installieren](#google-freies-lineage-installieren)
  * [Anleitungen und Infos](#anleitungen-und-infos)
  * [Sicherung des aktuellen Systems](#sicherung-des-aktuellen-systems)
  * [Lineage runterladen und installieren](#lineage-runterladen-und-installieren)
  * [Datenverbindung via SFTP](#datenverbindung-via-sftp)
  * [Kontakte importieren](#kontakte-importieren)
  * [Noch eine Sicherung des nun aktuellen Systems](#noch-eine-sicherung-des-nun-aktuellen-systems)
  * [Regelmäßiges Backup Moto G --> Home-Computer](#regelmassiges-backup-moto-g----home-computer)

<!-- tocstop -->

Bestellen
---------
* bei E-Bay, ca. 60 EUR
* LineageOS 14.1 vorinstalliert inkl. TWRP (Teamwin)

Erstes Auspacken
----------------
### Google-Dienste-Uninstall fail
* Auf dem Gerät läuft der Google Play Store - nur deaktivierbar, aber nicht deinstallierbar
* außerdem die Google-Dienste, nicht deaktivierbar
* siehe https://www.reddit.com/r/LineageOS/comments/763npd/uninstall_play_store/
    * https://f-droid.org/packages/de.j4velin.systemappmover

### WLAN fail
* Es wurden alle WLANs erkannt, außer die Fritzbox in der Wohnung (nicht meine eigene), in der ich mich befand

### USB-Datentransfer fail
* per USB scheint es prinzipiell unter Linux schwierig zu sein, erst recht ohne WLAN
    * siehe https://www.maketecheasier.com/access-android-device-with-linux-file-managers/
        * "Access via MTP (aka “The Hard Way”)"
        * "Access via SFTP (aka “The Easy Way”)"
            * vielleicht damit? - https://f-droid.org/packages/org.primftpd/
                * benötigt WLAN

Google-Freies Lineage installieren
----------------------------------
### Anleitungen und Infos
* https://wiki.lineageos.org/devices/falcon

### Sicherung des aktuellen Systems
* Reboot to Recovery (Team Win Recovery Project, TWRP)
* Sicherung erstellen (1. Spalte, 2. Zeile), alles außer User-Speicher oder so
    * Dies ist sinnvoll, falls irgendwas mit der Folge-Installation fehlschlägt

### Lineage runterladen und installieren
* lineage-14.1-20180307-nightly-falcon-signed.zip runterladen
* auf einen USB-Stick
* mittels USB-OTG anschließen
* TWRP -> Restore -> zip auswählen
* Warten. Fertig. Reboot. First-Run-Wizard

### Datenverbindung via SFTP
* F-Droid installieren
* **primitiveftpd** installieren
    * starten, user und passwort vergeben
* Via WLAN von Linux drauf zugreifen
* Kontakt-Datei und Musik überspielen

### Kontakte importieren
* done

### Noch eine Sicherung des nun aktuellen Systems
* Reboot to Recovery (Team Win Recovery Project, TWRP)
* "Sichern" (1. Spalte, 2. Zeile)
* zu sicherende Partionen auswählen (alle):
    * Boot 10 MB
    * Cache 15 MB
    * Data (ohne /data/media) 648 MB
    * Recovery (10 MB)
    * System (571 MB)
* "Speicher auswählen"
    * intern
* Start.
* 125 Sekunden... Done.
* **1,2 GB** kopieren via SFTP > WLAN zum Backup-Rechner, ca. 15 Minuten.

### Regelmäßiges Backup Moto G --> Home-Computer
* **Kontakte als vcf-Datei** exportieren
    * siehe z. B. https://www.heise.de/tipps-tricks/Android-Kontakte-sichern-so-klappt-s-3931602.html
* siehe auch
    * **own-lineageftp-backup**
        * Permission problems
* **WLAN** aktivieren
* **primitiveftpd** starten
    * wichtig: 3. Option ("Android Storage Access Framework SAF") muss aktiviert sein
    * Dann klappt auch sftp.
    * Mit den anderen Optionen konnte aber auch mit nur ftp das Verzeichnis nicht gelistet werden
* **Dolphin** alt: ftp://user@192.168.178.37:12345/storage/emulated/0/
* **FileZilla**: sftp://user@192.168.178.37:1234/ (geht automatisch in storage/emulated/0 rein)
    * Passwort: siehe Einstellungen
    * alles recht langsam, SFTP oder FTP egal
* Backup: Daten kopieren/verschieben
    * Bilder
        * vom moto löschen bzw. verschieben
    * Documents
        * Kontakte
    * data
        * notes.txt separat kopieren
    * Bisher noch zuviel manuell
* primitiveftpd stoppen
