Zehnfingersystem
================

<!-- toc -->

- [Einstieg Online](#einstieg-online)
- [Einstieg Offline](#einstieg-offline)
- [Einstieg Programmieraufgabe](#einstieg-programmieraufgabe)
- [Weitere Programmier-Ideen](#weitere-programmier-ideen)
  * [Multiplayer-"TyperShark" / Kooperativ](#multiplayer-typershark--kooperativ)
  * [Online-Spiele](#online-spiele)
  * [Wettbewerb](#wettbewerb)

<!-- tocstop -->

Baustein: "10-Finger"

* Tastatur
* auch „Tastschreiben“ oder „10-Finger-Tastschreiben“, siehe https://de.wikipedia.org/wiki/Zehnfingersystem
    * Bild: https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/QWERTZ-10Finger-Layout.svg/400px-QWERTZ-10Finger-Layout.svg.png

Einstieg Online
---------------
* http://www.easy-10-finger.de/index.php
* TuxType auf OnWorks Ad-Hoc-Linux

Einstieg Offline
----------------
* tipp10
* Alternativen:
    * tuxtype
        * auch auf OnWorks Ad-Hoc-Linux
    * GNU Typist (gtypist, für Kommandozeile, todo: gibt es das auf Deutsch?)

Einstieg Programmieraufgabe
---------------------------
* mit ncurses: Wörter (erst hart-codiert, dann z. B. aus dict) laufen von links nach rechts
    * Anfangen zu tippen
    * Wort wird eingeloggt (Fettschrift)
    * erfolgreich => Wort geht weg
    * ...vormachen wie es geht und dann zum Variieren motivieren
    * Geschwister zeigen / zeigt wie gut es ist, ohne hinzuschauen tippen zu können

Typeracer
---------
* https://play.typeracer.com/
    * you just typed a movie quote...
    * https://www.moviequotesandmore.com/best-quotes-from-the-matrix-movies/
        * "Let me tell you why you’re here. You know something. What you know, you can’t explain, but you feel it. You felt it your entire life. There’s something’s wrong with the world. You don’t know what, but it’s there. Like a splinter in your mind, driving you mad. It is this feeling that has brought you to me. Do you know what I’m talking about?"
        * https://www.quotes.net/mquote/60254
    * StarTrek Transcripts: StarTrekTranscripts/www.chakoteya.net/movies/movie7.html

Weitere Programmier-Ideen
-------------------------
### Multiplayer-"TyperShark" / Kooperativ
* Ideen https://www.keybr.com/multiplayer - mit Autorennen

### Online-Spiele
* Multiplayer: https://www.keybr.com/multiplayer
* https://zty.pe/ - mit Raumschiffen und leider mit Waffen

### Wettbewerb
* siehe ShellTux
