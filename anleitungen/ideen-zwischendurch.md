Ideen für Zwischendurch
=======================

Zur Auflockerung oder zum Ausklang.

ELIZA
-----
* [Talking with ELIZA](../anleitungen/talking-with-eliza)
    * Falls man mal bei der aktuellen Aufgabe nicht weiterkommt (und Spielen und Videos schauen nicht erlaubt ist)

Filme
-----
### 2018 - Das Microsoft-Dilemma
* Doku: [Das Microsoft-Dilemma](http://www.daserste.de/information/reportage-dokumentation/dokus/videos/das-microsoft-dilemma-video-100.html), 45 min, ARD

### 2017 - Public Money Public Code
* Video: [deutsche Version](https://publiccode.eu/de/), 4 min

### Was ist GNU/Linux? / Geschichte / Freie Software
* **Doku: "Codename: Linux", 2001**, 50 min
    * Richard Stallman über Rezepte
    * wie es dazu kam, dass Linus GPL verwendete (wegen des Compilers)
    * etc.
    * 15 min: GPL-Erklärung, Korrektur: Änderungen am Code müssen nur dann veröffentlicht werden, wenn auch das Programm veröffentlicht wird!
    * etc.
    * [Deutsche Fassung auf ARTE](https://www.youtube.com/watch?v=SzKEi5AHZf4)
    * basiert auf UNIX
        * Jurrasic Park lief auch mit UNIX
* **ZDF-Info-Doku "Die Reise des Pinguins", 2009**, 30 min
    * Kritik: es wird zu oft betont, dass freie Software kostenlos ist, obwohl das nicht der Punkt ist!
    * [Teil 1](https://www.youtube.com/watch?v=ybhq9HNXolM)
    * [Teil 2](https://www.youtube.com/watch?v=pI-wIWTjrcc)
        * Linux-Magazin-Redakteur stellt heraus, dass es nicht auf den Preis ankommt, sondern darauf, dass je mehr freie Software wir heute herstellen, desto leichter haben es die Menschen in der Zukunft.
    * [Teil 3](https://www.youtube.com/watch?v=lXZN443_wU8)
        * LiMux München
    * [komplett](https://www.youtube.com/watch?v=UVbChs7DUtg) (schlechtere Quali?)
* **Doku: Revolution OS, 2001**, 1h 25min
    * (bessere Tonqualität, mit englischen Untertiteln): [YouTube](https://www.youtube.com/watch?v=J1bBG1NtL18)
    * Kurzbiographien der einzelnen Personen in dieser [YT-Description](https://www.youtube.com/watch?v=jw8K460vx1c)
    * [IMDB](http://www.imdb.com/title/tt0308808)
* "GNU/Linux am Beispiel Knoppix und Co - Vortrag von Klaus Knopper"
    * [YouTube-Vorlesungsmitschnitt](https://www.youtube.com/watch?v=lZ6tD_NA57Q), an der Uni Leipzig, 2015, 1h 20min
        * ab ca. 42 min: "Softwarekrise", Komplexität, Anwender früher und heute, Problem des auseinanderklaffenden Wissens der User
* Richard Stallman
    * ["Richard Stallman: Freie Software für mehr Sicherheit im Netz"](https://www.youtube.com/watch?v=lONkIbwFoMM), ARTE, 2015, 4 min
        * freie SW ist nur Teil der Lösung gegen Einschränkung der Bürgerrechte; man müsse sich auch politisch organisieren; Demokratie
    * ["Richard Stallman - Free Software, Your Freedom, Your Privacy"](https://www.youtube.com/watch?v=80N-dyGIkTE), Vortrag, aufgenommen am 14. Februar 2017 in Köln, über die Themen freie Software, Freiheit und Privatsphäre, 1h 50min
    * ["Ende der 70er: Vom Papierstau zur freien Software - Eine Gegengeschichte der Internetze - ARTE"](https://www.youtube.com/watch?v=-2J-XFCREoQ), 5 min
    * ["1970: Von Beruf System-Hacker - Eine Gegengeschichte der Internetze - ARTE"](https://www.youtube.com/watch?v=g4WWqdb1Nls), 3 min
    * ["1970: Kein Passwort - Eine Gegengeschichte der Internetze - ARTE"](https://www.youtube.com/watch?v=RBkdpZWvyXU), 3 min
* Freie Software
    * ["Freie Software - Missverständnisse aufklären - Matthias Kirschner CLT 2016"](https://www.youtube.com/watch?v=VwDWf9hie1A), 40 min
        * Folgende falsche Aussagen:
        * "1) Freie Software ist kostenlos.
        * 2) Mit Freier Software kann man kein Geld verdienen.
        * 3) Wer Freie Software ändert, muss die Änderungen wieder allen verfügbar machen.
        * 4) Freie Software ist (un)sicherer als proprietäre Software.
        * 5) Freie Software ist das überlegene Entwicklungsmodell.
        * 6) Freie Software bringt mir nichts, wenn ich nicht programmieren kann"

### Persönlichkeiten rund ums Internet
* Film über [Aaron Swartz](https://de.wikipedia.org/wiki/Aaron_Swartz): [The Internet's Own Boy (2014)](https://www.youtube.com/watch?v=9vz06QO3UkQ) (engl., 1:45 h)
    * Mitentwickler von https://de.wikipedia.org/wiki/Markdown und RSS, Mitgründer von reddit
        * Tools: ["Tiny Tiny RSS is a free and open source web-based news feed (RSS/Atom) reader and aggregator"](https://tt-rss.org/)
    * [Trailer](https://www.youtube.com/watch?v=RvsxnOg0bJY)

### Thema: Überwachung durch Computer
* Thema: Snowden und Überwachung dank leistungsfähiger Computer:
    * ARD-Doku ["Jagd auf Snowden"](https://www.youtube.com/watch?v=ymKcefjlwhQ), 45 min, 2015
    * Doku: Citizenfour, 2014
    * Kinofilme: ["Snowden Trailer"](https://www.youtube.com/watch?v=QlSAiI3xMh4), 2016

### Verschiedenes
* Vortrag: ["Dezentralisierung gegen Überwachung im Internet [Kieler Linux Tage 2016]"](https://www.youtube.com/watch?v=Wofvu-k1_kk), 1 h
    * u. a. http://alternativeto.net/software/adobe-dreamweaver/?license=free - Lizenz auswählen
    * Facebook
* LibreOffice
    * Vortrag: eher zäh: ["Libre Office - Wozu gibts den Verein? Florian Effenberger"](https://www.youtube.com/watch?v=R4g1Iachyeo), 25 min
* http://internetz.arte.tv/
* Vortrag: ["FreeYourAndroid - Freie Software auf Android-Geräten"](https://www.youtube.com/watch?v=8InaATg22Qk), 2014, 33 min

* auf Englisch
    * Interview: Stephen Fry über [Free Software Foundation - Gnu - Linux](https://www.youtube.com/watch?v=Wugzc58Qrl8), 6 min, 2009
        * Vergleich mit Leitungsrohren im Haus, die man zwar nicht selber versteht, aber von Freunden reparieren lassen kann. Dabei verstößt man gegen kein Gesetz. Im Ggs. zu proprietärer Software.
        * todo
