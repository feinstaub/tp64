Eigene openSUSE-Live-DVD zusammenstellen
========================================

**Eigene openSUSE-Live-DVD zusammenstellen** (mit selbst ausgesuchten Programmen)

(das Thema kann eigentlich gelöscht werden, weil wann braucht man schon mal eine eine Live-DVD?)

TODO

- Diese Live-CD (https://en.opensuse.org/openSUSE:Tumbleweed_installation), nur selbst gemacht
- https://forums.opensuse.org/showthread.php/523001-How-to-create-an-openSUSE-Tumbleweed-ISO-image-with-broadcom-wl-and-other-packages-pre-installed
- KIWI: https://build.opensuse.org/package/show/openSUSE:Factory:Live/kiwi-image-livecd-kde
- KIWI doc: https://doc.opensuse.org/projects/kiwi/doc/#part.basics
- siehe auch:
    - KDE Neon: https://neon.kde.org/download
    - https://community.kde.org/Neon/Docker
- siehe auch:
    - [Was ist KDE Argon und Kypton?](https://news.opensuse.org/2016/02/19/opensuse-offers-choices-for-kde-git-builds/) (git-builds von Leap oder Tumbleweed)
        - [Beispiel auf OBS](https://build.opensuse.org/package/show/KDE:Medias/openSUSE-Krypton-stable-i686)
- Erster Versuch:
    - siehe https://suse.github.io/kiwi/installation.html
        - $ sudo zypper install python3-kiwi qemu qemu-linux-user qemu-kvm
        - $ git clone https://github.com/SUSE/kiwi-descriptions
    - siehe https://suse.github.io/kiwi/quickstart.html
        - $ sudo kiwi-ng --type iso system build --description kiwi-descriptions/suse/x86_64/suse-tumbleweed-JeOS --target-dir _tmp/myimage_iso
        - ($ sudo kiwi-ng --type vmx system build --description kiwi-descriptions/suse/x86_64/suse-leap-42.2-JeOS --target-dir _tmp/myimage_vmx (not tested))
        - Ergebnis: LimeJeOS-tumbleweed.x86_64-1.99.1.iso
        - mit Oracle VirtualBox starten
            - Neu
            - Keine Festplatte
            - IDE -> LiveCD anhaken -> ISO auswählen
        - mit QEMU
            - $ qemu-system-x86_64 -cdrom LimeJeOS-tumbleweed.x86_64-1.99.1.iso -m 2048
                - dauert länger und kommt nicht zum Login
            - $ qemu-system-x86_64 -boot c -cdrom LimeJeOS-tumbleweed.x86_64-1.99.1.iso -m 4096
                - ebenso
- TODO:
    - wo findet man das kiwi-Rezept (config.xml) für die Tumbleweed-KDE-Live-CD (https://en.opensuse.org/openSUSE:Tumbleweed_installation)
        - fvogt_vps fragen

- Alternative:
    - openSUSE-Edu Li-f-e (Linux for Education): http://www.opensuse-education.org/
        - Download 3,7 GB iso-Datei: https://sourceforge.net/projects/opensuse-edu/
