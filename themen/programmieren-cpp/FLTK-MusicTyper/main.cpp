//#include <SFML/Graphics.hpp>    // must be before FLTK
#include <SFML/Audio.hpp>

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Output.H>
#include <FL/fl_draw.H>

#include <ctime>

struct Music
{
    void start()
    {
        if (!music.openFromFile("audio/main_menu.ogg")) {
            // return -1; // error
        }
        music.setLoop(true);
        music.setVolume(10);
        music.play();
    }

    void pause()
    {
        music.pause();
    }

    void unpause()
    {
        music.play();
    }

    void set_delta_vol(int delta)
    {
        int newvol = music.getVolume() + delta;
        if (newvol < 0)
            newvol = 0;
        music.setVolume(newvol);
        if (delta > 0) {
            music.setPitch(1.5);
        } else {
            music.setPitch(0.5);
        }
    }

    // https://www.sfml-dev.org/documentation/2.5.1/classsf_1_1Music.php
    sf::Music music;
};

// https://www.fltk.org/doc-1.3/subclassing.html
class TyperWidget : public Fl_Group
{
public:
    TyperWidget(int x, int y, int w, int h, const char *l=0)
        : Fl_Group(x, y, w, h)
    {
        // coords of subwigets seem to be global, so x and y of ctor must be respected
        objectiveBox = new Fl_Box(x, y, w, 40);
        objectiveBox->labelfont(FL_BOLD);
        objectiveBox->labelsize(FONT_SIZE);
        objectiveBox->align(FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
        objectiveBox->box(FL_NO_BOX); // https://www.fltk.org/doc-1.3/common.html#common_boxtypes
        //textBox->box(FL_FLAT_BOX);

        textBox = new Fl_Box(x, y, w, 40);
        textBox->labelfont(FL_BOLD);
        textBox->labelsize(FONT_SIZE);
        textBox->align(FL_ALIGN_LEFT | FL_ALIGN_INSIDE);
        textBox->box(FL_NO_BOX);
        textBox->labelcolor(FL_GREEN);

        end();
    }

    void set_objective_text(const std::string& text)
    {
        _objectiveText = text;
        objectiveBox->copy_label(text.c_str());
    }

    void set_text(const std::string& text)
    {
        textBox->redraw_label(); // needed because of NO_BOX
        textBox->copy_label(text.c_str());
    }

    /**
     * return true if OK else false
     */
    bool add_one_char(const std::string& text)
    {
        textBox->redraw_label(); // needed because of NO_BOX
        _text += text;
        return update_from_text();
    }

    bool backspace()
    {
        if (_text.size() > 0) {
            _text = _text.substr(0, _text.size() - 1);
        }
        return update_from_text();
    }

    bool update_from_text()
    {
        textBox->copy_label(_text.c_str());
        return check_current_text();
    }

    bool check_current_text()
    {
        if (_objectiveText.rfind(_text) == 0) {
            _matches = true;
        } else {
            _matches = false;
        }

        if (_matches)
            textBox->labelcolor(FL_GREEN);
        else
            textBox->labelcolor(FL_RED);

        return _matches;
    }


private:
    Fl_Box* objectiveBox;
    Fl_Box* textBox;

    std::string _objectiveText;
    std::string _text;
    bool _matches = true;

    const int FONT_SIZE = 36;
};

/**
 * Type correctly to keep the music going
 */
class TypeForMusic : public Fl_Group
{
public:
    TypeForMusic(int x, int y, int w, int h, const char *l=0)
        : Fl_Group(x, y, w, h)
    {
        typerWidget = new TyperWidget(x, y, w, h);
        typerWidget->set_objective_text("Hallo,␣das␣ist␣ein␣Test.");

//         auto* tw2 = new TyperWidget(x + 150, y + 220, 100, 100);
//         // https://www.fltk.org/doc-1.3/common.html#common_sizeposition
//         tw2->position(0, 0); // ok, works

        end();

        // TODO: intercept keyboard on window: (on window, not on Widget, because widget should only be the display)
    }

    int handle(int event) override
    {
        switch (event) {
            case FL_FOCUS: // https://www.fltk.org/doc-1.3/events.html#events_focus
                return 1; // want focus
            case FL_KEYDOWN:
                //typerWidget->set_text(std::to_string(std::time(0)));
                //typerWidget->set_text(Fl::event_text());
                bool match = false;
                if (Fl::event_key() == FL_BackSpace) {
                    typerWidget->backspace();
                } else {
                    std::string key = Fl::event_text();
                    if (key == " ") {
                        key = "␣"; // TODO: with this backspace does not work
                    }
                    match = typerWidget->add_one_char(key);
                }

                if (match) {
                    if (!state.started) {
                        state.started = true;
                        music.start();
                    }

                    // TODO: reset timeout for fadeout later (timeout? signal? callback?) Use progressbar to accumulate time

                    if (state.text_mismatch) {
                        state.text_mismatch = false;
                        music.unpause();
                    }
                } else {
                    state.text_mismatch = true;
                    // TODO: play annoying buzzer
                    music.pause();
                }

                return 1;
        }
        return 0;
    }

private:
    struct State
    {
        bool started = false;
        bool text_mismatch = false;
    };

private:
    TyperWidget* typerWidget;
    State state;
    Music music;
};

const int INITIAL_W = 600;
const int INITIAL_H = 400;

int main(int argc, char **argv)
{
    auto *window = new Fl_Double_Window(INITIAL_W, INITIAL_H, "Music Typer");
    //Fl_Window *window = new Fl_Window(1920, 1080);
    //window->fullscreen();
    // resizable?
//     Fl_Box *box = new Fl_Box(20,40,300,100,"Hello, World!");
//     box->box(FL_UP_BOX);
//     box->labelfont(FL_BOLD+FL_ITALIC);
//     box->labelsize(36);
//     box->labeltype(FL_SHADOW_LABEL);

    auto* box2 = new Fl_Box(0, 0, 300, 40, "Loslegen, indem die erste Taste gedrückt wird...");
    box2->align(FL_ALIGN_LEFT | FL_ALIGN_INSIDE);

    //auto* box3 = new Fl_Box(0, 0, 40, 40, "@->");
    //box3->align(FL_ALIGN_RIGHT);

    auto* button = new Fl_Button(0, 150, 100, 20, "fscr");

    auto* button_downvol = new Fl_Button(0, 170, 100, 20, "down vol");
    /**
     * - http://bannalia.blogspot.com/2016/07/passing-capturing-c-lambda-functions-as.html
     * - https://andreldm.com/2016/04/02/replacing-fltk-callbacks-lambdas.html
     *     - https://github.com/libsigcplusplus/libsigcplusplus
     */
    auto cb = [](Fl_Widget* w, void* data) {
        Music& music = *static_cast<Music*>(data);
        music.set_delta_vol(-10);
    };
    //button_downvol->callback(cb, &music);

    auto* button_upvol = new Fl_Button(0, 190, 100, 20, "up vol");
//     button_upvol->callback([](Fl_Widget* w, void* data) {
//         Music& music = *static_cast<Music*>(data);
//         music.set_delta_vol(10);
//         }, &music);

    auto* typeForMusic = new TypeForMusic(0, 100, INITIAL_W, INITIAL_H - 100);
    typeForMusic->take_focus(); // give focus

    window->end();
    window->show(argc, argv);
    return Fl::run();
}
