#!/usr/bin/env python3

import turtle
import math

turtle.speed(10)
turtle.pensize(2)

def haus(a):
    """
    a = Länge der Grundfläche

         3
       4  3
      4    3
    4555555 2
    78 8  6 2
    7  66 8 2
    111111111
    """

    c = math.sqrt(2 * a**2)
    d = c / 2

    turtle.forward(a) # 1
    turtle.left(90)
    turtle.forward(a) # 2
    turtle.left(45)
    turtle.forward(d) # 3
    turtle.left(90)
    turtle.forward(d) # 4
    turtle.left(135)
    turtle.forward(a) # 5
    turtle.right(135)
    turtle.forward(c) # 6
    turtle.right(135)
    turtle.forward(a)
    turtle.right(135)
    turtle.forward(c)

turtle.setheading(0) # 0 == east
haus(200)

turtle.exitonclick()
