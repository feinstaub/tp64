// gleichzeitig Tasten drücken
// https://cboard.cprogramming.com/c-programming/172122-using-ncurses-1-output-window-updating-1-input-keyboard.html

#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <chrono>
#include <cstring>
#include <string>

int main() {
    char buf[100] = {0}, *s = buf;
    int ch, cnt = 0, n = 1;
    WINDOW *w;

    if ((w = initscr()) == NULL) {
        fprintf(stderr, "Error: initscr()\n");
        exit(EXIT_FAILURE);
    }
    curs_set(0);
    keypad(stdscr, TRUE);
    noecho();
    cbreak();      // disable line-buffering
    timeout(10);  // wait 100 milliseconds for input

    long oldms = 0;
    int side = 1;
    int x = 1;
    int delta_y = 0;
    int p1x = 0;
    int p1y = 8;
    int p2x = 0;
    int p2y = 9;
    while (n != 0) {

        using namespace std::chrono;
        long curms = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        if (curms - oldms > 50) {
            oldms = curms;
            cnt++;

            if (x == 0 || x == 30) {
                side = -side;
            }
            if (side == 1) {
                x++;
            } else {
                x--;
            }
        }

        erase();
        mvprintw(0, 0, "cnt: %d", cnt);
        mvprintw(1, 0, "buf: %s", buf);
        mvprintw(2, 0, "int: %d", n);
        mvprintw(3, 0, "int: %d", curms);
        mvprintw(4 + delta_y, x, "Hallo");
        mvprintw(5 + delta_y, x, "[ %s ]", buf);
        if (strcmp("Hallo", buf) == 0) {
                mvprintw(6, 0, "MATCH");
        }
        if (std::string("Hallo") == std::string(buf)) {
                mvprintw(7, 0, "MATCH");
        }
        mvprintw(p1y, p1x, "P1");
        mvprintw(p2y, p2x, "P2");
        refresh();

        // getch (with cbreak and timeout as above)
        // waits 100ms and returns ERR if it doesn't read anything.
        if ((ch = getch()) != ERR) {
            if (ch == '\n') {
                //*s = 0;
                sscanf(buf, "%d", &n);
                s = buf;
                *s = 0;
            }
            else if (ch == KEY_BACKSPACE) {
                if (s > buf)
                    *--s = 0;
            }
            else if (ch == KEY_DOWN) {
                delta_y++;
                p1y++;
            }
            else if (ch == KEY_UP) {
                delta_y--;
                p1y--;
            }
            else if (ch == KEY_LEFT) {
                p1x--;
            }
            else if (ch == KEY_RIGHT) {
                p1x++;
            }
            else if (ch == 'a') {
                p2x--;
            }
            else if (ch == 'd') {
                p2x++;
            }
            else if (s - buf < (long)sizeof buf - 1) {
                *s++ = ch;
                *s = 0;
            }
        }
    }

    delwin(w);
    endwin();
    return 0;
}
