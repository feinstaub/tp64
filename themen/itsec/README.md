IT-Sicherheit
=============

siehe daily-choices-info-package/Computer/it-sicherheit.md

2019
----
* "World's Most Famous Hacker Kevin Mitnick & KnowBe4's Stu Sjouwerman Opening Keynote", https://www.youtube.com/watch?v=iFGve5MUUnE, 2017, 30 min
    * ...
    * Zutrittskarte klauen
        * violate physical security to break IT security
    * ...

vor 2019
--------
* [Schwachstellen von KNX-Systemen](https://www.antago.info/index.php?id=74)
* [Video zu Rubber Ducky](https://www.youtube.com/watch?v=sbKN8FhGnqg)
* [BSI und Freie Software ein](https://www.bsi.bund.de/DE/Themen/DigitaleGesellschaft/FreieSoftware/freiesoftware_node.html)
* [BSI zu Virenscanner unter Linux, 2013](https://www.bsi-fuer-buerger.de/SharedDocs/Downloads/DE/BSIFB/Publikationen/BSIe009_Ubuntu.pdf?__blob=publicationFile&v=1)
* https://stallman.org/microsoft.html (englisch)
