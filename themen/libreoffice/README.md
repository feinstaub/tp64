LibreOffice
===========

<!-- toc -->

- [Einstieg 1](#einstieg-1)
  * [Weihnachtskarte mit Writer gestalten](#weihnachtskarte-mit-writer-gestalten)
  * [Zeichnen mit Draw](#zeichnen-mit-draw)
- [Einstieg 2](#einstieg-2)
  * [Calc](#calc)
- [Anleitungen, Handbücher, Übungen](#anleitungen-handbucher-ubungen)
  * [Offizielle Hilfe](#offizielle-hilfe)
- [Fortgeschritten: Calc: Formeln](#fortgeschritten-calc-formeln)
  * [Logische Operationen](#logische-operationen)
  * [Strings](#strings)
  * [Funktionen für Tabellen](#funktionen-fur-tabellen)
- [Verschiedenes](#verschiedenes)
  * [OpenThesaurus](#openthesaurus)
  * [Impress: Präsentationen erstellen](#impress-prasentationen-erstellen)
  * [Calc: Vorschau im Webbrowser](#calc-vorschau-im-webbrowser)
  * [Calc: Alternative Diagramm-Erstellung mit RAWGraphs](#calc-alternative-diagramm-erstellung-mit-rawgraphs)
  * [Draw: Alternative Zeichenprogramme](#draw-alternative-zeichenprogramme)

<!-- tocstop -->

Baustein: "LibreOffice"

Einstieg 1
----------
...freie Alternative zu MS Office kennenlernen

### Weihnachtskarte mit Writer gestalten
* Texte schreiben mit **Writer**
    * ...

### Zeichnen mit Draw
* Zeichnen mit **Draw**
    * ...
    * Alternative Inkscape, siehe inkscape-einstieg

Einstieg 2
----------
### Calc
* Tabellen berechnen und Diagramme erstellen mit **Calc**
    * ...

Anleitungen, Handbücher, Übungen
--------------------------------
### Offizielle Hilfe
* https://de.libreoffice.org/get-help/feedback/
* Handbücher: https://de.libreoffice.org/get-help/documentation/
    * Umfangreiche Einstiege, Erste Schritte Gute Gliederung, Writer, Calc, etc.
* Youtube-Kanal: https://www.youtube.com/c/LibreOfficeTheDocumentFoundation
* Video-Anleitungen: https://de.libreoffice.org/get-help/neue-seite-2/
    * speziell für Writer etc.: https://de.libreoffice.org/get-help/neue-seite-2/videoanleitungenwriter?stage=Stage
* Übungsaufgaben: https://de.libreoffice.org/get-help/uebungsaufgaben/
    * u. a. auch Bilder zuschneiden [mit diesem Bild](img/LOPlakat_2015.png), Kapitelnummerierung
* Literatur/Bücher: https://de.libreoffice.org/get-help/literatur/
* Anwenderlösungen: https://de.libreoffice.org/get-help/users/#vereine
    * http://www.it.bistum-wuerzburg.de/unsere-programme/libreoffice--openoffice-/libreoffice-dokumentationen
    * http://robert.familiegrosskopf.de/index_2.php?&weite=&hoehe=&Inhalt=xml_formulare

(Das md zu PDF umwandeln als Material für TL)

Fortgeschritten: Calc: Formeln
------------------------------
### Logische Operationen
* Nicht: https://help.libreoffice.org/6.0/de/text/scalc/01/04060105.html

### Strings
* Regex:
    * https://ask.libreoffice.org/en/question/89130/why-does-search-with-regular-expression-regex-fail-with-value/
    * https://help.libreoffice.org/6.0/de/text/shared/01/02100001.html?&DbPAR=WRITER&System=WIN
* https://www.ablebits.com/office-addins-blog/2016/06/01/split-text-string-excel/

### Funktionen für Tabellen
* VERGLEICH: https://help.libreoffice.org/6.0/de/text/scalc/01/04060109.html?DbPAR=CALC#bm_id3158407

Verschiedenes
-------------
### OpenThesaurus
* Der Saurier - The Saurus - Thesaurus -> "Synonyme und Assoziationen"
    * https://www.openthesaurus.de
    * https://www.openthesaurus.de/about/download
    * "OpenThesaurus ist ein deutschsprachiges Wörterbuch für Synonyme und Assoziationen. Man kann damit Wörter mit gleicher oder ähnlicher Bedeutung nachschlagen."
    * "Jeder kann bei OpenThesaurus mitmachen und Fehler korrigieren oder neue Synonyme einfügen."

### Impress: Präsentationen erstellen
* Präsentationen erstellen mit **Impress**

### Calc: Vorschau im Webbrowser
* Datei -> Vorschau im Webbrowser (engl. Preview in Webbrowser)
    * erzeugt eine HTML-Vorschau

### Calc: Alternative Diagramm-Erstellung mit RAWGraphs
* Online-App: http://app.rawgraphs.io/
* Quellcode und Offline-Usage: https://github.com/densitydesign/raw/

### Draw: Alternative Zeichenprogramme
* Vektor-Zeichenprogramme: **Inkscape**, **Dia**

* https://degooglisons-internet.org/alternatives?l=en
    * https://svg-edit.github.io

* https://www.draw.io
    * https://github.com/jgraph/drawio
    * https://github.com/jgraph/drawio-desktop - Desktop version mit Electron

* **Pencil** - unter anderem Flussdiagramme erstellen
    * http://pencil.evolus.vn
    * auf openSUSE Li-f-e-DVD, openSUSE-Paket: pencil
