#pragma once

//#include <cstdio>   // printf
#include <cmath>    // pow
#include <string>   // std::string
#include <vector>   // vector
#include <map>      // map
#include <iostream> // cout
#include <iomanip>  // setw
#include <algorithm>    // transform
#include <sstream>  // stringstream
#include <fstream>  // ifstream
#include <filesystem>   // filesystem
#include <stdexcept>    // std::runtime_error
#include "json.hpp"

struct VPKey
{
    VPKey()
    {
        c = 0; // == ?
    }

    /**
     * a  => a
     * Sa => A
     */
    VPKey(const std::string& s)
    {
        int i = 0;
        if (s.at(0) == 'S') {
            shift = true;
            i = 1;
        }
        c = std::toupper(s[i]);
    }

    /**
     * ?  , 0 => 0 unbekannt
     * ' ', 1 => 1 Pause
     * '|', 2 => 2 Lange Pause
     * 'a'    => a
     * 'A'    => A
     * ...
     */
    VPKey(char c_)
    {
        if (c_ == 0 || c_ == 1 || c_ == 2) {
            c = c_;
            return;
        }

        if (c == ' ') {
            c = 1;
            return;
        }

        if (c == '|') {
            c = 2;
            return;
        }

        c = std::toupper(c_);
        if (c_ < 'a' && c_ >= 'A') {
            shift = true;
        }

        // < 'A' kommen die Zahlen
    }

    bool operator==(const VPKey& other)
    {
        return c == other.c && shift == other.shift;
    }

    /**
     * s mit "Shift". Das wird deswegen benötigt,
     * weil auf der Tastatur bei den Zahlen 1..9 nicht die
     * Sonderzeichen verwendet werden, sondern Shift+'Zahl'
     */
    std::string to_string_s()
    {
        if (c == 0) {
            return "?";
        } else if (c == 1) {
            return " ";
        } else if (c == 2) {
            return "|";
        }

        std::string s;
        if (shift) {
            s = "Shift+";
        }
        s += c;
        return s;
    }

    /**
     * 'a' oder 'A' oder '1' etc.
     * oder ' ' (Pause)
     * oder '|' (Lange Pause)
     * oder '?' (unbekannt)
     */
    char to_vpsheet_char()
    {
        if (c == 0) {
            return '?';
        } else if (c == 1) {
            return ' ';
        } else if (c == 2) {
            return '|';
        }

        char result = c;
        if (!shift) {
            result += ('a' - 'A');
        }
        return result;
    }

public:
    char c; // character, Großbuchstabe
    bool shift = false;
};

std::map<int, VPKey> virtualpianomap;

/**
 * https://virtualpiano.net/
 */
void init_virtualpianomap()
{
    using namespace std;
    namespace fs = std::filesystem;
    string filename = "virtualpiano_map.txt";
    if (!fs::exists(filename)) {
        throw runtime_error(filename + " not found");
    }

    ifstream ifs(filename);
    if (!ifs.is_open()) {
        throw std::runtime_error("Unable to load file: " + filename);
    }

    while (!ifs.eof())
    {
        string taste_s;
        string s;
        ifs >> taste_s;
        if (taste_s.empty()) { // last entry
            continue;
        }
        int taste = stoi(taste_s);
        ifs >> s;

        if (s == "?") {
            continue;
        }

        virtualpianomap[taste] = VPKey(s);
    }
}

/**
 * Sa => A
 */
int virtualpiano_zu_taste(const std::string& s)
{
    using namespace std;

    if (virtualpianomap.empty()) {
        init_virtualpianomap();
    }

    VPKey vpkey(s);
//     int taste = *(find_if(virtualpianomap.begin(), virtualpianomap.end(),
//                         [vpkey&](const std::pair<int, VPKey>& item) { return item.second == vpkey; })).first;
    int taste = -1;
    for (auto item : virtualpianomap)
    {
        if (item.second == vpkey) {
            taste = item.first;
            break;
        }
    }
    return taste;
}

int virtualpiano_zu_taste(char c)
{
    using namespace std;

    if (virtualpianomap.empty()) {
        init_virtualpianomap();
    }

    VPKey vpkey(c);
    int taste = -1;
    for (auto item : virtualpianomap) {
        if (item.second == vpkey) {
            taste = item.first;
            break;
        }
    }
    return taste;
}

VPKey taste_zu_virtualpiano(int taste)
{
    using namespace std;

    if (virtualpianomap.empty()) {
        init_virtualpianomap();
    }

    if (virtualpianomap.find(taste) != virtualpianomap.end()) {
        return virtualpianomap[taste];
    }

    return VPKey();
}

/**
 * https://de.wikipedia.org/wiki/Frequenzen_der_gleichstufigen_Stimmung
 *
 * Standardmäßig haben wir 88 Tasten mit der Nummer von 1 bis 88
 * Taste  1 ist "A0"  ist   28 Hz (gerundet)
 * Taste 49 ist "A4"  ist  440 Hz                    virtualpiano: "p"
 * Taste 50 ist "A#4" ist  466 Hz                    virtualpiano: "Shift+p"
 * Taste 51 ist "B4"  ist  494 Hz                    virtualpiano: "a"
 * Taste 52 ist "C5"  ist  523 Hz                    virtualpiano: "s"
 * Taste 88 ist "C8"  ist 4186 Hz
 *
 * n = Tastennummer
 * f = Frequenz
 * Die Formel ist: f(n) = 2 hoch ((n - 49) / 12) * 440
/*/
int taste_zu_frequenz(int tasten_nr)
{
    return round(pow(2.0, static_cast<double>(tasten_nr - 49) / 12.0) * 440.0);
    //return pow(2.0, static_cast<double>(tasten_nr) / 10.0);
}

enum class Lang
{
    EN,
    DE
};

// von catch.hpp
char toLowerCh(char c) {
    return std::tolower(c);
}
void toLowerInPlace(std::string& s) {
    std::transform(s.begin(), s.end(), s.begin(), toLowerCh);
}
std::string toLower(std::string const& s) {
    std::string lc = s;
    toLowerInPlace(lc);
    return lc;
}

/**
 * split at space
 */
std::vector<std::string> split(const std::string s)
{
    // https://www.fluentcpp.com/2017/04/21/how-to-split-a-string-in-c/
    std::istringstream iss(s);
    std::vector<std::string> result(std::istream_iterator<std::string>{iss}, std::istream_iterator<std::string>());
    return result;
}

//                                    0    1      2    3      4    5    6      7    8      9    10     11
std::vector<std::string> names_en = { "C", "C#",  "D", "D#",  "E", "F", "F#",  "G", "G#",  "A", "A#",  "B" };
std::vector<std::string> names_de = { "c", "cis", "d", "dis", "e", "f", "fis", "g", "gis", "a", "ais", "h" };

/**
 * https://de.wikipedia.org/wiki/Frequenzen_der_gleichstufigen_Stimmung
 * http://www.sengpielaudio.com/Rechner-notennamen.htm - mit Matrix "Frequenzen der gleichstufigen Stimmung − Tabelle"
 *
 * Taste 1  = A0
 * Taste 2  = A#0
 * Taste 3  = B
 * ...
 */
std::string taste_zu_name(int nr, Lang lang = Lang::EN)
{
    using namespace std;
    int name_idx = (nr + 8) % 12;
    string name;
    if (lang == Lang::EN) {
        int n = (nr + 8) / 12;
        name = names_en[name_idx] + to_string(n);
    } else {
        int n = (nr + 8) / 12 - 2;
        string base_name = names_de[name_idx];
        if (n < 0) {
            // https://en.cppreference.com/w/cpp/locale/ctype/tolower
            base_name[0] = toupper(base_name[0]);
            name = base_name + "_" + to_string(-n);
        }
        else if (n == 0) {
            base_name[0] = toupper(base_name[0]);
            name = base_name;
        }
        else if (n == 1) {
            name = base_name;
        }
        else if (n > 1) {
            name = base_name + "^" + to_string(n - 1);
        }
    }
    return name;
}

void print_tasten()
{
    using namespace std;
    //printf("%10s%10s%10s%10s\n", "Taste", "Freq", "EN", "DE");
    cout << right << setw(10) << "Taste" << setw(10) << "Freq" << "    "
        << left << setw(10) << "EN" << setw(10) << "DE" << right
        << left << setw(10) << "Virt Piano" << right
        << "\n";
    for (int i = 1; i <= 88; i++)
    {
        cout << setw(10) << i << setw(10) << taste_zu_frequenz(i) << "    "
            << left << setw(10) << taste_zu_name(i) << setw(10) << taste_zu_name(i, Lang::DE) << right
            << left << setw(10) << taste_zu_virtualpiano(i).to_string_s() << right
            << "\n";
    }
}

/**
 * "A4" -> 49
 * TODO: inefficient solution!!!
 *
 * return 1..88 or 0 if not found
 */
int name_en_zu_taste(const std::string& name_en)
{
    for (int i = 1; i <= 88; i++)
    {
        if (taste_zu_name(i) == name_en)
            return i;
    }
    return 0;
}

class Note
{
public:
    /**
     * 1..88: Note
     * 0: Stille
     */
    int key = 0;

    /**
     * inv_dur: inverse duration (todo: besserer Begriff)
     *
     * 1: ganze Note
     * 2: halbe
     * 4: viertel
     * 8: achtel
     */
    int inv_dur = 4;
};

/**
 * Eine Spalte an Noten, die gleichzeitig gespielt werden
 */
class SheetColumn
{
public:
    std::vector<Note> notes;
};

class MusicSheet
{
public:
    std::string title;
    std::string source;
    std::vector<SheetColumn> noteColumns;

    /**
     * 1. A4
     * 2. A4|B4
     */
    void fill_sample()
    {
        SheetColumn sc;
        Note note;
        note.key = 49;
        sc.notes.push_back(note);
        noteColumns.push_back(sc);

        note.key = 51;
        sc.notes.push_back(note);

        noteColumns.push_back(sc);
    }

    std::string title_summary()
    {
        return "'" + title + "' mit " + std::to_string(noteColumns.size()) + " Note(n)";
    }

    std::string noten_to_virtual_piano_string()
    {
        using namespace std;
        string result;
        for (const SheetColumn& sc : noteColumns)
        {
            if (sc.notes.size() != 1) {
                throw runtime_error("sc.notes.size() != 1 not supported yet");
            }

            Note note = sc.notes[0]; // todo...

            if (note.key == 0) {
                result += " ";
            } else {
                VPKey vpkey = taste_zu_virtualpiano(note.key);
                result += vpkey.to_vpsheet_char();
            }
        }
        return result;
    }

    static void add_from_simple1_format(std::vector<SheetColumn>* columns, const std::string& s)
    {
        using namespace std;
        for (const string& note_name : split(s))
        {
            SheetColumn sc;
            Note note;
            note.key = name_en_zu_taste(note_name);
            sc.notes.push_back(note);
            columns->push_back(sc);
        }
    }

    /**
     * e.g. "fDfDfadsp tupa uOas ufDfDfadsp"
     */
    static void add_from_virtualpiano_format(std::vector<SheetColumn>* columns, const std::string& s)
    {
        using namespace std;

        for (char c : s)
        {
            SheetColumn sc;
            Note note;

            if (c == ' ') {
                note.key = 0; // Stille
            } else if (c == '|') {
                note.key = 0; // Längere Stille
                note.inv_dur = 2;
            } else {
                int taste = virtualpiano_zu_taste(c);
                if (taste == -1) {
                    string ch = " "; // string + char nicht erlaubt
                    ch[0] = c; // TODO: geht das besser?!

                    throw runtime_error("virtualpiano character invalid: "
                                        + ch + " (int: " + to_string((int)c) + ")");
                }
                note.key = taste;
            }

            sc.notes.push_back(note);
            columns->push_back(sc);
        }
    }

    static std::vector<MusicSheet> from_file(const std::string& filename)
    {
        using namespace std;
        namespace fs = std::filesystem;
        using json = nlohmann::json;

//         if (!fs::exists(filename)) {
//         }

        // https://stackoverflow.com/questions/2602013/read-whole-ascii-file-into-c-stdstring
        ifstream file(filename);
        if (!file.is_open()) {
            throw std::runtime_error("Unable to load file: " + filename);
        }
        stringstream buffer;
        buffer << file.rdbuf();

        auto j = json::parse(buffer.str());
        auto root = j["tp64_music_sheet"];
        string default_title = root["title"]; // must be present
        string default_source = root["source"] != nullptr ? root["source"] : "";
        string default_format = root["format"] != nullptr ? root["format"] : "";

        vector<MusicSheet> result;

        for (auto sheet : root["sheets"])
        {
            MusicSheet sheet_out;

            string title = sheet["title"] != nullptr ? sheet["title"] : "";
            string source = sheet["source"] != nullptr ? sheet["source"] : "";
            string format = sheet["format"] != nullptr ? string(sheet["format"]) : default_format;
            sheet_out.title = title.empty() ? default_title : title;
            sheet_out.source = source.empty() ? default_source : source;

            if (format == "simple1")
            {
                for (auto notes_line : sheet["notes"])
                {
                    add_from_simple1_format(&sheet_out.noteColumns, notes_line);
                }
            }
            else if (format == "virtualpiano")
            {
                for (auto notes_line : sheet["notes"])
                {
                    add_from_virtualpiano_format(&sheet_out.noteColumns, notes_line);
                }
            }
            else
            {
                throw runtime_error("Format nicht unterstützt: " + format);
            }

            result.push_back(sheet_out);
        }

        return result;
    }

    MusicSheet from_virtualpiano_string(const std::string& text)
    {
        MusicSheet sheet;
        // TODO (siehe oben, schon da)
        throw std::runtime_error("....");
        return sheet;
    }
};
