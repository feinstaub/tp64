#!/usr/bin/env python3

# sudo zypper install python3-devel
# pip install --user simpleaudio
#
# https://stackoverflow.com/questions/260738/play-audio-with-python
# http://simpleaudio.readthedocs.io/en/latest/installation.html

import numpy as np
import simpleaudio as sa
import sys

def play_tone(freq_hz, duration_ms):
    # calculate note frequencies
    A_freq = freq_hz
    Csh_freq = A_freq * 2 ** (4 / 12)
    E_freq = A_freq * 2 ** (7 / 12)

    # get timesteps for each sample, T is note duration in seconds
    sample_rate = 44100
    T = duration_ms / 1000
    t = np.linspace(0, T, T * sample_rate, False)

    # generate sine wave notes
    A_note = np.sin(A_freq * t * 2 * np.pi)

    # concatenate notes
    audio = A_note
    # normalize to 16-bit range
    audio *= 32767 / np.max(np.abs(audio))
    # convert to 16-bit data
    audio = audio.astype(np.int16)

    # start playback
    play_obj = sa.play_buffer(audio, 1, 2, sample_rate)

    # wait for playback to finish before exiting
    play_obj.wait_done()

if __name__ == "__main__":
    # execute only if run as a script
    play_tone(float(sys.argv[1]), float(sys.argv[2]))

