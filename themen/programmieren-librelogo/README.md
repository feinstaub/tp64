LibreLogo
=========

<!-- toc -->

- [Einstieg](#einstieg)
  * [Demo-Programm](#demo-programm)
  * [Anleitungen und Aufgaben](#anleitungen-und-aufgaben)
- [Aufgaben für Klasse 5 und 6](#aufgaben-fur-klasse-5-und-6)
  * [Symbolleiste aktivieren](#symbolleiste-aktivieren)
  * [Das Haus vom Nikolaus](#das-haus-vom-nikolaus)
  * [Ein Blatt voller Kreise](#ein-blatt-voller-kreise)
  * [Vom Dreieck zum Kreis, 360 Grad](#vom-dreieck-zum-kreis-360-grad)
  * [Eigenen Namen mit eigenen Buchstaben zeichnen](#eigenen-namen-mit-eigenen-buchstaben-zeichnen)
  * [Würfel erzeugen](#wurfel-erzeugen)
  * [Haus vom Nikolaus in verschiedenen Größen](#haus-vom-nikolaus-in-verschiedenen-grossen)
- [Demos](#demos)
  * [Baum des Pythagoras](#baum-des-pythagoras)
  * [Pyramide aus Kreisen](#pyramide-aus-kreisen)
  * [Rekursiver Schwamm](#rekursiver-schwamm)
  * [Wikipedia-Illustrationen](#wikipedia-illustrationen)
  * [Programmier-Ideen](#programmier-ideen)

<!-- tocstop -->

Einstieg
--------
### Demo-Programm

```
WIEDERHOLE 3 [
VOR 100
LINKS 120
]
a = 0
WIEDERHOLE 20 [
wenn a < 10 [
    KREIS 50 + a * 10
][
    QUADRAT a * 10
]
RECHTS 30
VOR 50
a = a + 1
]
ENDE
```

### Anleitungen und Aufgaben
* Start mit
    * http://www.librelogo.de/, 2015

* Schrittweise diese **deutsche Anleitung** und **Befehlsreferenz** durchgehen:
    * https://help.libreoffice.org/latest/de/text/swriter/librelogo/LibreLogo.html
    * (alt: https://help.libreoffice.org/Writer/LibreLogo_Toolbar/de)

* Quick-Start auf Englisch: http://librelogo.org/quick-start/
    * Webseite: http://librelogo.org/en/

* Modulo und andere Beispiele: https://it.wikibooks.org/wiki/Piccolo_manuale_di_LibreLogo/Testo_completo

Aufgaben für Klasse 5 und 6
---------------------------
* Befehle einüben / Schrittweise neue Befehle / gegenseitig helfen

### Symbolleiste aktivieren
...

### Das Haus vom Nikolaus
```
   .
  / \
  ---
 | X |
  ---
```

### Ein Blatt voller Kreise
* A4-Seite mit Kreisen

```
OOOO
OOOO
OOOO
OOOO
```

* Mit Farben variieren
* Mit Formen variieren

```
OoOo
oOoO
OoOo
oOoO
```

* Mit Reihenfolge variieren

```
1234        1234
8765        5678
```

* Mit Zufallspositionen
* **TODO: Gebirge-Algorithmus**

### Vom Dreieck zum Kreis, 360 Grad
```
                     ..
 /\         --      /  \
/  \       |  |    |    |
^^^^        --      \--/
```

    ZU Form C L
    WIEDERHOLE C [
    WENN ZÄHLER % 2 == 0 [ SF "rot" ] [ SF "blau" ]
    VOR L / C * 3
    LINKS 360 / C
    ]
    ENDE

    SÄUBERN
    WIEDERHOLE 20 [
    FLIEGEN
    Z = 120 + (ZÄHLER - 1) % 5 * 120
    S = 120
    WENN ZÄHLER > 5  [ S = 2 * 120 ]
    WENN ZÄHLER > 10 [ S = 3 * 120 ]
    WENN ZÄHLER > 15 [ S = 4 * 120 ]
    POS [S, Z]
    RICHTUNG 0
    LAUFEN
    Form ZÄHLER + 2 100
    ]

### Eigenen Namen mit eigenen Buchstaben zeichnen
```
 ___    __
| o |  | o)
|   |  | o)

Zeichne_A
Zeichne_B
```

    ZU Zeichne_A L
    LINKS 30
    VOR L
    LINKS 120
    VOR L
    LINKS 120
    VOR L * 0,1
    LINKS 60
    VOR L * 0,5
    RECHTS 60
    VOR L * 0,2
    RECHTS 60
    VOR L * 0,5
    LINKS 60
    VOR L * 0,3
    LINKS 90
    ENDE

    ZU Zeichne_B L
    ; ...
    ENDE

    SÄUBERN
    FLIEGEN
    POS [200, 200]
    RICHTUNG 0
    LAUFEN
    Zeichne_A 100
    Zeichne_B 100

### Würfel erzeugen
```
------
| o   |
|   o |    etc.
------
```

### Haus vom Nikolaus in verschiedenen Größen
* Funktionen lernen

Demos
-----
### Baum des Pythagoras
* siehe pythagobaum.odt

### Pyramide aus Kreisen
```
säubern
anfang
rechts 90
fliegen
verbergen ; damit geht es schneller
zurück 200
z = 10
solange z >= 0 [
    FÜLLFARBE [z % 4 + 5]
    a = z
    solange a >= 0 [
        kreis 40
        vor 40
        a = a - 1
    ]

    zurück 40*(z+1)
    links 90
    vor 35
    rechts 90
    vor 20
    z = z - 1
]
```
BILD „hallo.svg“ [
...
...
]

### Rekursiver Schwamm
```
Säubern
fliegen
verbergen
zu weiss a
	füllfarbe „weiß“
	quadrat a
ende
zu move x y
	position [x, y]
	richtung 0
ende
zu weiss8 a x y i; weiß an den 8 Stellen, relativ zur x y
	move x y
	weiss a
	wenn i > 0 [
	weiss8 a/3 x - a y - a 	i - 1
	weiss8 a/3 x  y - a 		i - 1
	weiss8 a/3 x  + a y - a	i - 1
	weiss8 a/3 x - a y		i - 1
	weiss8 a/3 x + a y		i - 1
	weiss8 a/3 x - a y + a 	i - 1
	weiss8 a/3 x  y + a 		i - 1
	weiss8 a/3 x  + a y + a	i - 1
	]
ende
zu start a
	x = a / 2 + 50
	y = x
	move x y
	FÜLLFARBE „schwarz“
	quadrat a
	weiss8 a/3 x y 3
ende
start 450
```

### Wikipedia-Illustrationen
Wikipedia-Illustrationen, die mit LibreLogo erstellt wurden, mit Source-Code: https://commons.wikimedia.org/wiki/Category:Images_with_LibreLogo_source_code

Hier verlinkt: http://librelogo.org/resources/

### Programmier-Ideen
* Klaviertastatur zeichnen, siehe auch qklavier.py
    * mit Noten-Buchstaben
