#!/bin/bash

TARGET=/usr/share/plasma/wallpapers/de.ag.hintergrundbild2
# ~/.local/share/plasma/wallpapers does not work. Why? (even with KAppTemplate version)
sudo rm -R $TARGET.installed
sudo cp -R . $TARGET.installed
echo Done.
