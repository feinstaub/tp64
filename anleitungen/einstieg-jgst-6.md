Einstieg - Jgst. 5 und 6
========================

Ggf. unter Anleitung von erfahrenen Schülern.

Start
-----
* GNU/Linux installieren:
    * siehe Start-Abschnitt von [Einstieg Jgst. 7](einstieg-jgst-7.md)
    * oder GNU/Linux Live-DVD
* Programmiereinstieg:
    * [Python-Einstieg mit Turtle](python-turtle/python-turtle-on-opentechschool.md)
    * [Python-Grundlagen-Kurs auf CS-Circles](python-on-cscircles.md)
    * siehe auch [Einstieg Jgst. 7](einstieg-jgst-7.md)
* cowsay, siehe Baustein AsciiArt
* Computer und Graphik:
    * [Pixel und Farben](../themen/graphik-und-zeichnen/README.md)
* ShellTux
* Scratch?

Tastschreiben lernen
--------------------
siehe themen/zehnfingersystem/zehnfingersystem-README.md

Aufgaben für 5. Klasse
----------------------
* 2019, sortieren:
    * https://www.uebungskoenig.de/mathe/5-klasse/rechengesetze-der-addition-rechnen-mit-klammern/
    * https://www.klassenarbeiten.de/gymnasium/klasse5/mathematik/nat%C3%BCrliche-zahlen/

Weiterführend
-------------
* Den eigenen **Plasma-Desktop anpassen**:
    * [Animiertes Hintergrundbild](../themen/animiertes-hintergrundbild-mit-qml.md)

* [3D-Modellierung mit Blender](../anleitungen/blender-mini-howto)

* Vorhandene **Software selber kompilieren** und ändern:
    * mit C++/Qt, am einem Beispiel siehe hier https://userbase.kde.org/Applications/Education/de (Das KDE Education Projekt)
    * Vorbereitung für C++: [C++-Programmierung auf wikibooks](../themen/programmieren-cpp/)
    * siehe c-tunneler-anpassen/README.md
