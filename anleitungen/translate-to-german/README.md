Texte von Englisch auf Deutsch übersetzen
=========================================

![](mono-edu-languages.png)

Zur Übung der Englisch-Lesefähigkeiten und zum Training der Deutsch-Formulierungsfähigkeit bietet es sich an, frei lizensierte englische Dokumentationtexte auf Deutsch zu übersetzen.

Als praxisnahes Beispiel eignet sich das KDE-Community-Wiki.

Als Anfänger ist es sinnvoll, bei weniger frequentierten Seiten anzufangen und sich - je mehr Übung man hat - langsam zu den relevanteren Seiten vorzuarbeiten.

Einfache Beispiele, mit denen man anfangen könnte:

* **KDE-Applikationsbeschreibungen** https://userbase.kde.org/Applications aufrufen
    * Oben Deutsch wählen: https://userbase.kde.org/Applications/de
    * Sektion "Bildung" anklicken und wieder auf Deutsch schalten: https://userbase.kde.org/Applications/Education/de
    * Die Webseite des Programms Kalzium (Periodensystem) wählen: https://userbase.kde.org/Kalzium
        * Feststellen, dass es dort keine deutsche Übersetzung gibt
        * Anfangen zu übersetzen (siehe Anleitung)
            * Vorher vielleicht das Programm selber installieren und schauen, ob es funktioniert wie beschrieben
        * Tipp zum Verlinkten Benutzerhandbuch: https://docs.kde.org/trunk5/en/kdeedu/kalzium/index.html
            * Im Link das "en" durch "de" ersetzen
    * Oder weiter durchklicken bis zu einem Programm, das einen interessiert
* Deutsche Übersetzung der **Pygame**-Original-Doku: http://www.pygame-doku.laymaxx.de
    * Schon der erste Link "Introduction" führt zu einer Seite, die **noch nicht übersetzt** ist: http://www.pygame-doku.laymaxx.de/tut/intro/intro.html


Anleitung übersetzen:

* https://userbase.kde.org/Tasks_and_Tools/de#Arbeiten_mit_Sprachen (2013)
    * "Eine Seite für die Übersetzung vorzubereiten"
    * "Übersetzen einer Seite"
    * "Übersetzen mit offline Werkzeugen"
    * "Wie man ein UserBase Handbuch nach Docbook konvertiert"
    * typographische Richtlinien beachten
        * https://userbase.kde.org/Typographical_Guidelines (dieser Text wurde z. B. noch nicht ins Deutsche übersetzt)
* englisch: https://userbase.kde.org/Translate_a_Page
* deutsch: https://userbase.kde.org/Translate_a_Page/de
* https://userbase.kde.org/Translation_Workflow/German (2012)

[Anleitung KDE-Identity anlegen](../../themen/kde/kde-identity-anlegen.md):

* Damit kann man sich in die KDE-Wikis einloggen.

Werkzeuge und Tipps:

* [Offline-Wörterbuch mit KDing](offline-woerterbuch-kding), insbesondere Schnellzugriff mit Tasten und Verweis auf Online-Angebote
* https://userbase.kde.org/Typographical_Guidelines

Fortgeschrittene Beispiele / Ideen für Projekte:

* **Benutzerhandbücher** von Software übersetzen
    * todo: wie kann man ein KDE-Benutzerhandbuch überhaupt auf Deutsch aufrufen?
* TODO: gute und frei lizensierte **Python-Turtle-Referenz für Kinder finden** und auf Deutsch übersetzen
    * z. B. das hier braucht noch Hilfe: Deutsche Übersetzung der Pygame-Doku: http://www.pygame-doku.laymaxx.de/ (englisches Original: https://www.pygame.org/docs/tut/PygameIntro.html)
