#include <iostream> // cout, cin
#include <string>
#include <vector>
#include <memory>   // unique_ptr

using namespace std;

class Form
{
public:
    void set_pos(int x_, int y_)
    {
        x = x_;
        y = y_;
    }

    virtual void draw()
    {
        cout << "???";
    }

protected:
    int x;
    int y;
};

class Dreieck : public Form
{
    void draw() override
    {
        cout << "  .  \n";
        cout << " . . \n";
        cout << ".....\n";
    }
};

class Rechteck : public Form
{
    void draw() override
    {
        cout << ".....\n";
        cout << "|   |\n";
        cout << ".....\n";
    }
};

class RechteckAbegrundet : public Form
{
    void draw() override
    {
        cout << " ... \n";
        cout << "|   |\n";
        cout << " ... \n";
    }
};

using FormPtr = unique_ptr<Form>;

int main()
{
    vector<FormPtr> formen;

    formen.push_back(make_unique<Form>());
    formen.push_back(make_unique<Dreieck>());
    formen.push_back(make_unique<Rechteck>());
    formen.push_back(make_unique<RechteckAbegrundet>());

    // https://stackoverflow.com/questions/41060167/strange-error-use-of-deleted-function-stdunique-ptr-tp-dpunique-ptr-wh/41061374
    for (FormPtr& form : formen)
    {
        form->draw();
        cout << "\n\n";
    }
    cout << endl;

    return 0;
}
