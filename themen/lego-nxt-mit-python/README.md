Lego NXT mit Python
===================

<!-- toc -->

- [Einmalig: Python-Modul nxt-python installieren](#einmalig-python-modul-nxt-python-installieren)
  * [nxt-python installieren](#nxt-python-installieren)
  * [USB-Verbindung einrichten](#usb-verbindung-einrichten)
- [Beispiele ausprobieren](#beispiele-ausprobieren)
- [nxtfilemgr](#nxtfilemgr)
- [Fehlerbehebung](#fehlerbehebung)
- [Ideen](#ideen)
- [Verwandte Themen](#verwandte-themen)
  * [Veraltet? - Python 3 GTK+ 3 auf openSUSE installieren](#veraltet---python-3-gtk-3-auf-opensuse-installieren)
  * [Mit SSH auf anderen Rechner verbinden](#mit-ssh-auf-anderen-rechner-verbinden)
  * [Hintergrund](#hintergrund)

<!-- tocstop -->

* Kein Linux-Rechner vorhanden? -> [Klassenserver verwenden](../klassenserver)

Einmalig: Python-Modul nxt-python installieren
-----------------------------------------------
* siehe arbeitsblatt-lego.html
    * todo: übertragen

### nxt-python installieren

    # Arbeitsverzeichnis erstellen...
    cd ~
    mkdir ag/nxt
    cd ag/nxt
    # python-nxt-Bibliothek herunterladen...
    git clone https://github.com/eelviny/nxt-python
    cd nxt-python
    # python-nxt lokal installieren (erst anschauen, dann ausführen)
    ag@ag-pc:~/ag/nxt/nxt-python$ cat install.sh
        #!/bin/bash

        echo "This builds nxt-python for Python 3"
        echo "and installs it to the user's home directory."
        echo "Root is not required."
        echo ""

        rm -Rf ~/.local/lib/python3.*/site-packages/nxt
        python3 setup.py install --user

    ./install.sh

Ausprobieren:

    $ python3
    >>> import nxt

Keine Fehlermeldung? => OK (das heißt python-nxt ist installiert; aber das heißt noch nicht, dass alles schon geht).

Im nächsten Schritt wird der USB-Anschluss konfiguriert.

### USB-Verbindung einrichten

(basiert auf dieser Anleitung: https://github.com/Eelviny/nxt-python/wiki/Installation)

PythonUSB (http://walac.github.io/pyusb/) installiern, um mit Python den USB-Anschluss nutzen zu können:

    sudo zypper install python3-usb

Test (schlägt fehl):

    cd ~/.local/bin
    ./nxt_test

    No brick was found.
        Is the brick turned on?
        For more diagnosing use the debug=True argument or
        try the 'nxt_test' script located in /bin or ~/.local/bin
    Error while running test:
    File "./nxt_test", line 25, in <module>
        b = nxt.locator.find_one_brick(debug=debug)
    File "/home/gregor/.local/lib/python3.6/site-packages/nxt/locator.py", line 141, in find_one_brick
        raise BrickNotFoundError

USB einrichten, einmalig systemweit (UDEV-Regel anlegen, siehe auch https://de.wikipedia.org/wiki/Udev):

    sudo groupadd lego
    # als root mit su, sudo geht da nicht:
    echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="0694", GROUP="lego", MODE="0660"' > /etc/udev/rules.d/70-lego.rules
    # Strg+D
    # Rechte setzen
    sudo chmod u+w /etc/udev/rules.d/70-lego.rules

Einmalig pro User:

    sudo usermod -a -G lego <username>

Wobei <username> durch den eigenen Benutzernamen ersetzt werden muss. Dieser kann mit `$ whoami` herausgefunden werden.

Test, ob in der Gruppe (ggf. neu einloggen):

    groups
    # lego muss in der Ausgabe dabei sein

Test (erfolgreich):

    nxt_test

    info:
    (b'NXT\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00', '00:16:53:07:D4:C2', 0, 13172)
    Play test sound...
    ...done.
    NXT brick name: b'NXT\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
    Host address: 00:16:53:07:D4:C2
    Bluetooth signal strength: 0
    Free user flash: 13172
    Protocol version 1.124
    Firmware version 1.31
    Battery level 7464 mV

OK.

Beispiele ausprobieren
----------------------

Auschecken:

    # Zurück ins Arbeitsverzeichnis
    cd ~/ag/nxt
    # Beispiel-Repository herunterladen:
    git clone https://github.com/Eelviny/nxt-python-examples
    cd nxt-python-examples
    git checkout master

Ausführen:

    # Sound check (keine weiteren Anschlüsse nötig):
    ./mary.py

    # Einen Motor an einen beliebigen Port (A, B oder C) anschließen:
    ./test_motor.py

    # Den Geräusch-Sensor an Port 1 anschließen:
    ./clapper1.py

nxtfilemgr
----------
* nxtfilemgr verwendet "import gi"
    * PyGObject (import gi), siehe https://pygobject.readthedocs.io/en/latest/
    * $ sudo zypper install python3-gobject

Starten

    nxtfilemgr

Ausprobieren: Eine .rso-Datei auswählen, Execute-Button => eine Sound-Datei wird abgespielt.


Fehlerbehebung
--------------
* Brick: Orangene Taste drücken zum Anschalten
* nxt.locator.find_one_brick() findet nichts
    * Restart udev: sudo udevadm control --reload-rules
        * http://unix.stackexchange.com/questions/39370/how-to-reload-udev-rules-without-reboot
* siehe auch https://github.com/Eelviny/nxt-python/issues/72
    * seems to work now
* $ udevadm monitor
    * Zeigt an, wenn USB-Geräte angeschlossen oder entfernt werden.
    * Den angezeigen Pfad kann man im folgenden Befehl verwenden:
        * $ udevadm info --path PFAD
            * PFAD ist z. B. /devices/pci0000:00/0000:00:1a.0/usb1/1-1/1-1.2

Ideen
-----
* 1x Klatschen == Drehen, noch mal Klatschen == Stop

* PyQt-GUI zum Fernsteuern eines Roboters (Regler -> Motoren)
    * oder: WebGUI für Remote-Rechner.

* Pong

    2x  <--- OOOOO ---> (fahrbarer Schläger)
                ^
                v
               ===  (hier muss etwas montiert sein, damit der Ball angestoßen werdne kann)
    1x Ball
    Dann bräuchte man noch ein abgeschlossenes Feld und idealerweise eine Erkennung, ob der Ball "im Aus" war.

Verwandte Themen
----------------
### Veraltet? - Python 3 GTK+ 3 auf openSUSE installieren
Ausführen (schlägt fehl)

    nxt_filer
    Traceback (most recent call last):
    File "/home/user/.local/bin/nxt_filer", line 18, in <module>
        import gtk
    ModuleNotFoundError: No module named 'gtk'

* nxt_filer => verwendetet veraltetes "import gtk"

Das hier - https://python-gtk-3-tutorial.readthedocs.io/en/latest/install.html - scheint nicht nötig zu sein,
es ist auch kein fertiges Paket python3-gtk3 oder ähnlich für openSUSE vorhanden; obsolete?

Selber bauen, siehe https://python-gtk-3-tutorial.readthedocs.io/en/latest/install.html

* $ sudo zypper install jhbuild
* $ jhbuild sanitycheck
* $ jhbuild build pygobject
    * Missing packages?
    * $ sudo zypper install itstool python3-cairo python3-cairocffi python3-cairo-devel libffi-devel
* $ jhbuild build pygobject
    * cloning, configuring, compiling, WAIT

    ...
    ...
    I: Install complete: 80 files copied
    *** success *** [8/8]

* ~/jhbuild now contains 14.700 files, 350 MB

* $ jhbuild build gtk+-3 # not found, so we try... (TODO: report bug to the manual)
* $ jhbuild build gtk+
* $ sudo zypper install xmlto ragel ragel-devel
* $ jhbuild build gtk+
    * cloning, configuring, compiling, WAIT
    * Build schlägt fehl

### Mit SSH auf anderen Rechner verbinden
* [SSH](../remote-desktop/ssh-opensuse)

### Hintergrund
* Es gibt verschiedene Arten von [Robotern](https://de.wikipedia.org/wiki/Roboter). Nur eine davon ist der [Humanoide Roboter](https://de.wikipedia.org/wiki/Humanoider_Roboter).
