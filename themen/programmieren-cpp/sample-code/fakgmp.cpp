// https://de.wikibooks.org/wiki/C%2B%2B-Programmierung/_Weitere_Grundelemente/_Rekursion
#include <iostream>       // Für std::cin und std::cout
#include <gmpxx.h>

// https://gmplib.org/manual/C_002b_002b-Interface-General.html
void t()
{
    mpz_class a, b, c;

    a = 1234;
    b = "-5678";
    c = a + b;
    std::cout << "sum is " << c << "\n";
    std::cout << "absolute value is " << abs(c) << "\n";
}

mpz_class fakultaet(mpz_class zahl) {
    if (zahl <= 1) {
        return 1; // Die Fakultät von 0 und 1 ist als 1 definiert.
    } else {
        return zahl * fakultaet(zahl - 1);
    }
}


int main ()
{
    mpz_class zahl = 15;
    std::cout << "Bitte Zahl eingeben: ";
    std::cin >> zahl;                           // Zahl einlesen
    std::cout << "Die Fakultät von " << zahl << // Antwort ausgeben
        " ist " << fakultaet(zahl) << "." << std::endl;

    return 0;
}
