#!/usr/bin/python3
# -*- coding: utf-8 -*-

import time
#import sys

i = 1
zahl = 2

while (True):
    is_prim = True

    if zahl == 1:
        is_prim = False
    elif zahl == 2:
        pass
    else:
        for k in range(2, int(zahl / 2.0)):
            # print("   TEST {0}: k={1}".format(zahl, k))
            if zahl % k == 0:
                is_prim = False
                break

    if is_prim:
        print(str(i) + ": " + str(zahl))
        #sys.stdout.flush()

        time.sleep(0.2)
        i += 1

    if zahl == 2:
        zahl -= 1 # damit wir unten mit zahl += 2 immer die ungeraden Zahlen erwischen

    zahl += 2
