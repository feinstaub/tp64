KDevelop
========

<!-- toc -->

- [Projekt: Neu von Vorlage](#projekt-neu-von-vorlage)
- [Launch Configurations](#launch-configurations)
- [Python mit KDevelop](#python-mit-kdevelop)
- [C mit KDevelop](#c-mit-kdevelop)
- [Bugs and Wishes](#bugs-and-wishes)
  * [Vorlagen](#vorlagen)
  * [Python](#python)

<!-- tocstop -->

todo

Projekt: Neu von Vorlage
------------------------
* siehe auch KAppTemplate
* z. B. Project -> New from Template -> Qt -> Graphical -> C++ KDE Frameworks
    * Name: MyKdeApp1
    * Ort: /home/uer/dev/from-template
    * Version control: git
    * OK
    * Configure cmake build: OK
    * Warte, rechte Maustaste -> Build
    * Execute -> Add compiled binary -> OK
    * Programm ausprobieren (Menü, Toolbar, Settings, Shortcuts etc.)

Launch Configurations
---------------------
...

Python mit KDevelop
-------------------
* siehe [Python mit KDevelop](../../anleitungen/python-mit-kdevelop)

C mit KDevelop
--------------
* siehe c-cpp-moonlander/README.md

Bugs and Wishes
---------------
### Vorlagen
* warte: ["KDE/Qt/C++/Python application template: add a distro agnostic package method"](https://bugs.kde.org/show_bug.cgi?id=397268)
    * A commonly asked question for people starting with programming is - "How can I run my application on another computer?"
    * An advanced topic would be to have a cross compilation environment setup to target the Windows platform, e.g. see https://stackoverflow.com/questions/10934683/how-do-i-configure-qt-for-cross-compilation-from-linux-to-windows-target

* warte: ["Add a CLI similar to what Javascript frameworks have"](https://bugs.kde.org/show_bug.cgi?id=397267) - KAppTemplate

### Python
* siehe http://pythonhosted.org/arcade/index.html (weitere/wikitolearn-backup/README.txt)
    * Hinweis zu kdevelop: Das kann leider nicht in den virtualenv-arcade-Bibliothek reinschauen
        * siehe ["Python: autocompletion not available for library installed in virtualenv"](https://bugs.kde.org/show_bug.cgi?id=368970)
            * fixed 2016-10-23, check again
