import QtQuick 2.0

Item {
    Item {
        x: 100
        y: 100
        width: 100
        height: 200
        MyEllipse {} // http://doc.qt.io/qt-4.8/qmlreusablecomponents.html

        // TODO: ellipse with subellipse that follows mouse
    }

    Item {
        x: 200
        y: 100
        width: 100
        height: 200
        MyEllipse {}
    }

    Item {
        x: 300
        y: 100
        width: 100
        height: 200
        MyEllipse {}
    }


    Canvas {
        id: root
        // canvas size
        width: 200; height: 200

        // handler to override for drawing
        onPaint: {
            // get context to draw with
            var ctx = getContext("2d")

            // setup the stroke
            ctx.lineWidth = 1
            ctx.strokeStyle = "black"

            // setup the fill
            ctx.fillStyle = "steelblue"

            ctx.beginPath()
            ctx.ellipse(0, 0, width, height) // subpath, see http://doc.qt.io/qt-5/qml-qtquick-context2d.html#ellipse-method
            ctx.closePath()
            ctx.fill()
            ctx.stroke()

            // TODO: reuse this item!

//             // begin a new path to draw
//             ctx.beginPath()
//             // top-left start point
//             ctx.moveTo(50,50)
//             // upper line
//             ctx.lineTo(width-50,50)
//             // right line
//             ctx.lineTo(150,150)
//             // bottom line
//             ctx.lineTo(50,150)
//             // left line through path closing
//             ctx.closePath()
//             // fill using fill style
//             ctx.fill()
//             // stroke using line width and stroke style
//             ctx.stroke()
        }

        //anchors.fill: parent
    }



}
