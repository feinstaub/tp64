Mein Breakout-Spiel Teil 2
==========================

<!-- toc -->

- [Schritt 5: Tasten](#schritt-5-tasten)
- [Schritt 6: Timer](#schritt-6-timer)
- [Schritt 7: Regen](#schritt-7-regen)
- [Schritt 8: Sounds](#schritt-8-sounds)

<!-- tocstop -->

Schritt 5: Tasten
-----------------
Schaue dir in [b5-tasten.py](b5-tasten.py) an wie man auf Tastatur-Ereignisse reagiert.

Hier sind alle verfügbaren Tasten aufgelistet: http://doc.qt.io/qt-5/qt.html#Key-enum

AUFGABE 1: Sorge dafür, dass der Schläger links und rechts im Feld bleibt.

AUFGAGE 2: Der Ball soll sich mit den Tasten A, S, D, W steuern lassen.

AUFGABE 3: Gebe den Namen der zuletzt gedrückten Taste oben im Text aus.


Schritt 6: Timer
----------------
Schaue dir in [b6-timer.py](b6-timer.py) an wie man einen Timer startet, der z. B. alle 50 Millisekunden tickt.

AUFGABE 1: Mache den Timer langsamer (1 Sekunde) und schneller.

AUFGABE 2: Sorge dafür, dass der Ball das Feld nicht verlässt.


Schritt 7: Regen
----------------
In [b7-regen.py](b7-regen.py) wird gezeigt, wie man einen schönen Regen über das Spielfeld regnen lässt.

AUFGABE: Erzeuge einen Starkregen.


Schritt 8: Sounds
-----------------
In [b8-sound.py](b8-sound.py) ist ein Sound eingebaut. Dazu brauchst du noch die Datei [jump1.wav](jump1.wav).

(Eigene Sounds erzeugen z. B. mit diesem Programm: https://github.com/agateau/sfxr-qt, siehe auch themen/musik)

Weiter mit [README3.md](README3.md).
