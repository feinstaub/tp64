#!/bin/bash

Xalt=-1
Yalt=-1
while true ; do
    # siehe https://stackoverflow.com/questions/8480073/how-would-i-get-the-current-mouse-coordinates-in-bash
    eval $(xdotool getmouselocation --shell)
    if [[ $Xalt != $X || $Yalt != $Y ]] ; then
        xdotool getmouselocation # print
        # ./playtone.sh $X $Y
        Xalt=$X
        Yalt=$Y
    fi
done
