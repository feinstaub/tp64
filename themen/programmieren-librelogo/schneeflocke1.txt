; Deutsche Hilfe: Menu Hilfe -> LibreOffice Hilfe -> nach LibreLogo suchen; inklusive Sprachreferenz
; Feature-Präsentation: http://www.numbertext.org/logo/librelogo.pdf

ZU ELEM s it
    global f, d
   WENN it = 0 [
        VOR s
   ] [
    ; STIFTFARBE BELIEBIG
    ; STIFTFARBE [100*it - 50, 50, 100*it]
    wenn it = 1 [
    wenn f = 0 [
           STIFTFARBE [255, 0, 0]
       ] [ wenn f = 1 [
	   	STIFTFARBE [255, 0, 255]
       ] [ STIFTFARBE [0, 0, 255] ] ]
          d = (d + 1) % 4
          wenn d = 0 [
             f = (f + 1) % 3
          ]
    ]

    kante = s / 3
    it1 = it - 1
    ELEM kante it1
    LINKS 45
    ELEM kante it1
    RECHTS 90
    ELEM kante it1
    LINKS 45
    ELEM kante it1
   ]
ENDE

VERBERGEN
SÄUBERN
FLIEGEN
POS [10, 200]
RICHTUNG 3h
LAUFEN
 STIFTBREITE 0
global f  ; FARBE
global d  ; Delay farb-Änderung
f  = 0
d = 0
ELEM 7cm 3

; AUSGABE "Wort"
