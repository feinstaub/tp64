#pragma once
#include <iostream>
#include <vector>
#include <fstream>
#include "tools.h"

namespace game
{

/**
 * Enthält die Anzeigenamen der Spieler
 */
struct Spieler
{
    std::string name0 = "Niko";    // X
    std::string name1 = "Maya G."; // o
};

/**
 * Steuert, welcher Spieler das Spiel beginnt und wer gerade dran ist.
 * Speichert, wie die Spieler heißen und ob sie AI sind.
 */
class Controller
{
public:
    void read_default_player_names()
    {
        std::fstream file("tictactoe.data", std::ios::in);

        int i = 0;
        std::string s[2];
        while (!file.eof() && file.good()) // falls Datei nicht da, dann ist file.good() false
        {
            if (i == 0 || i == 1)
                getline(file, s[i]);
            else
                break;
            i++;
        }

        if (i == 2) { // ganz oder gar nicht
            spieler.name0 = s[0];
            spieler.name1 = s[1];
        }

        file.close();
    }

    void save_player_names()
    {
        std::fstream file("tictactoe.data", std::ios::out);
        file << spieler.name0 << std::endl;
        file << spieler.name1;
        file.close();
    }

    int cur_player()
    {
        return cur_p;
    }

    bool cur_player_is_ai()
    {
        if (cur_player() == 0 && spieler.name0.rfind("KI") == 0)
            return true;
        else if (cur_player() == 1 && spieler.name1.rfind("KI") == 0)
            return true;
        return false;
    }

    void set_random_start()
    {
        cur_p = std::rand() % 2; // "rand() is not recommended for serious random-number generation needs"
    }

    void set_next_player()
    {
        cur_p = cur_p ? 0 : 1;
    }

public:
    Spieler spieler; // Spielernamen

private:

    // welcher Spieler ist dran
    // 0 == Spieler 1, x
    // 1 == Spieler 2, o
    int cur_p = 0;

};

enum class Ergebnis
{
    Keiner = -1,
    SpielerX,
    SpielerO,
    Unentschieden
};

/**
 * Enthält das Spielfeld
 */
class Feld
{
public:
    Feld()
    {
        //feld.resize(9);
        reset();
    }

    void reset()
    {
        // init with -1
        for (int i = 0; i < 9; i++) {
            feld[i] = -1;
        }
    }

    void debug_print_feld()
    {
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                printf("%d ", feld[y * 3 + x]);
            }
            printf("\n");
        }
        printf("\n");
    }

    int get(int y, int x) const
    {
        return feld[y * 3 + x];
    }

    /**
     * pos:     0..8
     * s:       welcher Spieler: 0 = "X"   1 = "O"
     * return:  true, wenn gültiger Zug, sonst false
     */
    bool set(int index, int s)
    {
        if (index < 0 || index > 8) {
            return false;
        }
        if (feld[index] == -1) { // nur, wenn Feld frei...
            feld[index] = s;
            return true;
        }
        return false;
    }

    Ergebnis check_ergebnis()
    {
        for (int i = 0; i < 8; i++) {
            if (feld[winfields[i][0]] == feld[winfields[i][1]]
                && feld[winfields[i][1]] == feld[winfields[i][2]]) {
                if (feld[winfields[i][0]] == 0)
                    return Ergebnis::SpielerX;
                else if (feld[winfields[i][0]] == 1)
                    return Ergebnis::SpielerO;
            }
        }

        for (int i = 0; i < 9; i++) {
            if (feld[i] == -1) {
                return Ergebnis::Keiner;
            }
        }
        return Ergebnis::Unentschieden;
    }

    const std::vector<int>& get_feld() const
    {
        return feld;
    }

private:
    //    ------> x
    // |  0  1  2
    // |  3  4  5
    // |  6  7  8
    // v
    // y                  y=0, x=2  y=1 x=1
    //                    |         |
    //              [ 0 1 2 3 4 5 6 7 8 ]
    std::vector<int> feld = { -1, -1, -1,
                              -1, -1, -1,
                              -1, -1, -1 };
    //
    // -1: nichts gesetzt = frei
    //  0: x
    //  1: o

    //const int winfields[8][3] = {
    const std::vector<std::vector<int>> winfields = {
                                  { 0, 1, 2 },
                                  { 3, 4, 5 },
                                  { 6, 7, 8 },

                                  { 0, 3, 6 },
                                  { 1, 4, 7 },
                                  { 2, 5, 8 },

                                  { 0, 4, 8 },
                                  { 2, 4, 6 } };
};

} // game
