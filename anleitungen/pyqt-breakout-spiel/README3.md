Mein Breakout-Spiel Teil 3
==========================

<!-- toc -->



<!-- tocstop -->

* b9-ball.py
    * Ball als Klasse
    * Feld variabel
    * Reflexion an allen Ecken

* b10-racket.py
    * Reflexion am Schläger und Punktestand

* b11-startmodus.py
    * Spielmodus (Ball am Schläger halten bei Start und Ballverlust)
    * Schläger bleibt im Feld

* b12-bricks.py
    * Steinreihe am Anfang erzeugen, mit Random-Farben
    * Steine zeichnen
    * Steine per Maus-Klick erzeugen
    * ballAmRacketBehandlung extrahieren
    * Steine-Hit-Test
    * Spielmodus AlleSteineWeg

Bonus:

- draw image
    - Icons laden
- Tasten mit besserer Wiederholrate (die sofort loslegt; mit key down und up arbeiten)
- Maus-Cursor verstecken
- Powerups
    - Ball schlägt durch
    - Blöcke zwei oder mehrfach treffen
    - Ball wird mit Maus gesteuert
    - Schläger wird größer oder kleiner
    - Schläger wechselt Farbe
    - Regen
        - mit Abschattung durch Objekte
- mehr Text, mehr Töne
- Schläger-Automatik
- Qt-Layouts und Widgets: mehrere Breakout-Widgets in einem Fenster (Mensch spielt eins davon und die anderen sind automatisch im AI-Modus)
