#!/bin/bash

# COPIED from workshop-smartphone
# more details there

# Dependencies
# ------------
# - markdown-pdf
#    $ cd ~
#    $ npm install --save phantomjs
#    $ npm install --save markdown-pdf
#    https://github.com/alanshaw/markdown-pdf/issues/30 -- Links are not created as links

VERSION=2017-1

mkdir -p _out

# https://forum.antergos.com/topic/5239/phantomjs-complains-about-missing-qt-platform-plugin-xcb/3
#
export QT_QPA_PLATFORM=''

# hyperlinks still do not work but at least the url is visible (TODO)
# cwd nötig, damit relative Bildpfade funktionieren
markdown-pdf --cwd praesenz -o _out/freie-software-überblick-$VERSION.pdf praesenz/folien.md
