Freie Software: Workshop
========================

* TL haben eigene Rechner dabei
* siehe [Freie Software: ein Überblick](../fs-overview)

* Inhalte:
    * auf z. B. Posteo.de und Mozilla Thunderbird umsteigen
    * siehe Werkzeugkasten Freie Software: 161213_Medien_in_die_Schule-Werkzeugkasten_Freie_Software.pdf

Google-Alternativen
-------------------
* Etherpad statt GoogleDocs
    * FramaPad: https://framapad.org

Doodle-Alternative
------------------
* Dudle: https://dudle.inf.tu-dresden.de/
    - https://www.zendas.de/themen/internet/doodle.html (Universität Stuttgart)
    - [Alternative Terminplaner](https://blog.llz.uni-halle.de/2013/01/alternative-terminplaner), Uni Halle, 2013

* FramaDate: https://framadate.org/ statt Doodle

Weitere freie Software-Alternativen
-----------------------------------
* Datenaustausch (beide Online, Peer to Peer):
    * sharedrop.io
* Datenaustausch (Async, über Server):
    * https://framadrop.org/ (Alternative zu https://wetransfer.com/)
        * "Framadrop is derived from Lufi software created by Luc Didry - Lufi is under the free license AGPL."
            * https://framagit.org/fiat-tux/hat-softwares/lufi
        * Mit Löschlink
* Sonstige Online-Dienste:
    * https://framasoft.org/
    * Online SVG-Editor - https://framavectoriel.org
