csound-floss-manual-de
======================

<!-- toc -->

- [Csound-Einführung](#csound-einfuhrung)
- [Grundlagen](#grundlagen)
  * [Digitale Audioverarbeitung](#digitale-audioverarbeitung)
  * [Tonhöhe und Frequenz](#tonhohe-und-frequenz)
  * [Intensitäten](#intensitaten)
  * [Zufall](#zufall)

<!-- tocstop -->

Csound-Einführung
-----------------
Basiert auf: http://write.flossmanuals.net/csound/preface/

"Csound is one of the best known and longest established programs in the field of audio programming. It was developed in the mid-1980s at the Massachusetts Institute of Technology (MIT) by Barry Vercoe but Csound's history lies even deeper within the roots of computer music: it is a direct descendant of the oldest computer program for sound synthesis, 'MusicN', by Max Mathews. Csound is free and open source, distributed under the LGPL licence, and it is maintained and expanded by a core of developers with support from a wider global community."

"Csound has been growing for 30 years. There is rarely anything related to audio that you cannot do with Csound. You can work by rendering offline, or in real-time by processing live audio and synthesizing sound on the fly. You can control Csound via MIDI, OSC, through a network, within a browser or via the Csound API (Application Programming Interface). Csound will run on all major platforms, on phones, tablets and tinyware computers. In Csound you will find the widest collection of tools for sound synthesis and sound modification, arguably offering a superset of features offered by similar software and with an unrivaled audio precision."

"Csound is simultaneously both 'old school' and 'new school'."

"Is Csound difficult to learn? [...] a text-based programming language is often easier to use and debug, and many people prefer to program by typing words and sentences rather than by wiring symbols together using the mouse."

"The basics of the Csound language are a straightforward transfer of the signal flow paradigm to text."

Beispiel Signalfluss:

    Frequenz 400 Hz           Amplitude 0.2
         |                         |
         |                         |
          -------------.-----------
                       |
                       v
                     Signal
                       |
                       v
                      Out

Mögliche Transformation in Csound-Code:

         instr   Sine
    aSig poscil  0.2, 400
         out     aSig
         endin

Erklärungen zum Code:

* **instr Sine**: Beginne eine Instruktion mit dem Namen Sine
* **endin**: Beende diese Instruktion
* **aSig**: Ein Audio-Signal mit Namen aSig
* **poscil**: _p_ recision _oscil_ lator = Präzisionsoszillator, 0.2: Amplitute, 400: Frequenz, siehe http://csound.com/docs/manual/poscil.html
* **out aSig**: Das Audio-Signal aSig ausgeben

Ab Csound 6 geht das auch so:

        instr Sine
    out poscil(0.2, 400)
        endin

"More than 15 years after the milestone of Richard Boulanger's Csound Book, the Csound FLOSS Manual is intended to offer an easy-to-understand introduction and to provide a centre of up to date information about the many features of Csound, not as detailed and as in depth as the Csound Book, but including new information and sharing this knowledge with the wider Csound community."

"Frequently this manual will link to other more detailed resources such as the Canonical Csound Reference Manual, the main support documentation provided by the Csound developers and associated community over the years, and the Csound Journal (edited by James Hearon and Iain McCurdy), a roughly quarterly online publication with many great Csound-related articles."

- http://write.flossmanuals.net/csound/how-to-use-this-manual/

Grundlagen
----------
### Digitale Audioverarbeitung
http://write.flossmanuals.net/csound/a-digital-audio/

- Elements of a Sound Wave
- Sampling
- Aliasing
    - mit Beispiel
- Bits, Bytes and Words. Understanding Binary.
    - Bit-depth Resolution

### Tonhöhe und Frequenz
http://write.flossmanuals.net/csound/b-pitch-and-frequency/

Frequenz und Periode

    Frequency = 1/Period         Period = 1/Frequency

* Eine Frequenz von 1 Hz bedeutet eine Periode von 1.
* Eine Frequenz von 10 Hz bedeutet eine Periode von 0.1.

Hörgrenzen: Der Mensch kann typischerweise Töne zwischen 20 Hz und 20.000 Hz (= 20 kHz) hören.

EXAMPLE 01B01_LimitsOfHearing.csd



    -odac -m0


    ;example by joachim heintz
    sr = 44100
    ksmps = 32
    nchnls = 2
    0dbfs = 1

    instr 1
            prints  "Playing %d Hertz!", p4
    asig    oscils  .2, p4, 0
            outs    asig, asig
    endin



    i 1 0 2 10
    i . + . 100
    i . + . 1000
    i . + . 10000
    i . + . 20000



Logarithms, Frequency Ratios and Intervals: ...

- "Examples of logarithmic scales include the decibel scale, the Richter scale for measuring earthquake magnitudes and the astronomical scale of stellar brightnesses. Musical frequencies also work on a logarithmic scale; more on this later."

In der Musik beschreibt ein **Interval** den Abstand zwischn zwei Noten.
In der Standard-Musik-Notation ist das Intervall zwischen zwei benachbarten Noten leicht zu bestimmen.
...
Wenn man es mit Hz zu tun hat, sind die Dinge anders. Eine Differenz von z. B. 100 Hz ist nicht immer das gleiche musische Internvall.
Das liegt daran, dass die musischen Intervall werden in Frequenz-Quotienten dargestellt. Eine Oktave z. B. ist immer 2:1. Das heißt, jedesmal wenn man den Hz-Wert verdoppelt, springt man im musischen Intervall eine Oktave nach oben.

Beispiele:

* Eine Flöte kann die Note A mit 440 Hz spielen. Die Note A eine Oktave höher ist dann 880 Hz.
* Ein Piccolo - das Instrument im Orchester mit der höchsten Tonlage - kann die Frequenz von 2000 Hz spielen. Und auch eine Oktave höher, nämlich 4000 Hz.

Der Abstand der Noten von jeweils einer Oktave ist im ersten Fall 440 Hz und beim zweiten 1000 Hz.

 "We can use simple ratios to represent a number of familiar intervals; for example the unison: (1:1), the octave: (2:1), the perfect fifth (3:2), the perfect fourth (4:3), the major third (5:4) and the minor third (6:5)"

EXAMPLE 01B02_Adding_vs_ratio.csd



    -odac -m0


    ;example by joachim heintz
    sr = 44100
    ksmps = 32
    nchnls = 2
    0dbfs = 1

    instr 1
            prints  "Playing %d Hertz!", p4
    asig    oscils  .2, p4, 0
            outs    asig, asig
    endin

    instr 2
            prints  "Adding %d Hertz to %d Hertz!", p5, p4
    asig    oscils  .2, p4+p5, 0
            outs    asig, asig
    endin

    instr 3
            prints  "Applying the ratio of %f (adding %d Hertz)
                     to %d Hertz!", p5, p4*p5, p4
    asig    oscils  .2, p4*p5, 0
            outs    asig, asig
    endin


    ;adding a certain frequency (instr 2)
    i 1 0 1 100
    i 2 1 1 100 100
    i 1 3 1 400
    i 2 4 1 400 100
    i 1 6 1 800
    i 2 7 1 800 100
    ;applying a certain ratio (instr 3)
    i 1 10 1 100
    i 3 11 1 100 [3/2]
    i 1 13 1 400
    i 3 14 1 400 [3/2]
    i 1 16 1 800
    i 3 17 1 800 [3/2]



**MIDI Notes**
"Csound can easily deal with MIDI notes and comes with functions that will convert MIDI notes to Hertz values and back again. In MIDI speak A440 is equal to A4 and is MIDI note 69. You can think of A4 as being the fourth A from the lowest A we can hear; well, almost hear."
"Caution: like many 'standards' there is occasional disagreement about the mapping between frequency and octave number. You may occasionally encounter A440 being described as A3."

### Intensitäten
http://write.flossmanuals.net/csound/c-intensities/

- Sound intensity level (SIL) in Watt pro m²
- Bel beschreibt das Verhältnis von einer Intensität zu einer anderen.
- I0 ist auf die untere Hörgrenze festgelegt: 10_hoch_-12 W / m² bei 1000 Hz
- 1 Bel wird in 10 dB unterteilt.
- ...
- Sound Pressure Level (SPL)
- ...
- "Working with digital audio means working with amplitudes. Any audio file is a sequence of amplitudes."
- ...
- todo

### Zufall
http://write.flossmanuals.net/csound/d-random/

...todo...
