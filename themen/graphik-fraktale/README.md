Fraktale Geometrie
==================

Kurvenfraktale
--------------
* Aufgabe siehe python-turtle-on-opentechschool.md
* Buchempfehlung bei Interesse am Thema:
    * Buch: "Die fraktale Geometrie der Natur" von Mandelbrot, 1987
        * nicht ganz leicht zu verstehen, aber kann dennoch sehr interessant sein, insbesondere die Generatorenbilder
    * siehe unten

* IDEE: Projekt "Fraktales-Muster-Generator"
    * L-Systeme mit Turtle
        * http://kevs3d.co.uk/dev/lsystems
            * https://de.wikipedia.org/wiki/Lindenmayer-System
            * weiteres: HTML5 Canvas demos http://kevs3d.co.uk/dev/
    * ["Generalized Koch Snowflakes"](http://archive.bridgesmathart.org/2000/bridges2000-301.pdf)
    * ["Erstellung eines Programms zur dreidimensionalen Darstellung ausgewählter Fraktale"](https://www.informatik.uni-leipzig.de/~meiler/Schuelerseiten.dir/MSeelemann_SGrehl/MSeelemann_SGrehl.pdf)

Andere Fraktale
---------------
* siehe auch ["6 of the Best Free Linux Fractal Tools"](https://www.linuxlinks.com/fractaltools/)
    * https://software.opensuse.org/package/xaos
    * https://software.opensuse.org/package/xfractint
