// https://de.wikibooks.org/wiki/C%2B%2B-Programmierung/_Einf%C3%BChrung_in_C%2B%2B/_Schleifen
#include <iostream>

int main() {
    unsigned i = 1;                  // 2 Variablen anlegen
    unsigned benutzer = 0;

    std::cin >> benutzer;            // Benutzer gibt Zahl ein
    std::cout << benutzer;
    std::cout << "Hallo";

    if (i <= benutzer) {             // Benutzereingabe erreicht?
        std::cout << "1\n";          // Ausgabe der Zahl 1
        ++i;                         // Ausgegebene Zahlen mitzählen
    } else {
        return 0;                    // Anwendung beenden
    }

    if (i <= benutzer) {             // Benutzereingabe erreicht?
        std::cout << "2\n";          // Ausgabe der Zahl 2
        ++i;                         // Ausgegebene Zahlen mitzählen
    } else {
        return 0;                    // Anwendung beenden
    }
    // ...
    if (i <= benutzer) {             // Benutzereingabe erreicht?
        std::cout << "4294967295\n"; // Ausgabe der Zahl 4294967295
        ++i;                         // Ausgegebene Zahlen mitzählen
    } else {
        return 0;                    // Anwendung beenden
    }
    // Wenn Ihr Compiler diese Stelle erreichen soll, brauchen Sie einen leistungsstarken
    // Rechner und ein robustes Betriebssystem.
    // Um dieses Problem zu lösen, setzen Sie sich einfach in die nächste Zeitmaschine
    // und bestellen Sie sich einen Rechner aus dem Jahr 2020!
}
