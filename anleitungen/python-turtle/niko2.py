#!/usr/bin/env python3

import turtle
import math

turtle.speed(40)
turtle.pensize(2)

zack_len = 10

def forward_zack(a):
    """
    /\/\/\_

    /\/\/|_

    / = zack_len

    Grundlänge gl = zack_len * sqrt(2) / 2

    Wie oft passt gl in die Länge? -> n = floor (a / gl)

    Restlänge rl = a - n * gl

    TODO: zentrieren: -/\/\/\-

    """
    gl = zack_len * math.sqrt(2) / 2
    n = math.floor(a / gl)
    rl = a - n * gl
    for i in range(0, n):
        if i % 2 == 0:
            turtle.left(45)
        else:
            turtle.right(90)

        turtle.forward(zack_len)

        if i % 2 == 1:
            turtle.left(45)

    if n % 2 == 1:         # /|_
        turtle.right(135)
        turtle.forward(gl)
        turtle.left(90)
        turtle.forward(rl)
    else:                  # \_
        turtle.forward(rl)

def zack_test():
    global zack_len
    zack_len_orig = zack_len
    zack_len = 50

    l = 140
    turtle.setpos(0, 0)
    turtle.forward(l)
    turtle.penup()
    turtle.setpos(0, 10)
    turtle.pendown()
    turtle.color("red")
    forward_zack(l)
    turtle.color("black")

    l = 160
    turtle.penup()
    turtle.setpos(0, 50)
    turtle.pendown()
    turtle.forward(l)
    turtle.penup()
    turtle.setpos(0, 60)
    turtle.pendown()
    turtle.color("red")
    forward_zack(l)
    turtle.color("black")

    zack_len = zack_len_orig

def haus(a):
    """
    a = Länge der Grundfläche

         3
       4  3
      4    3
    4555555 2
    78 8  6 2
    7  66 8 2
    111111111
    """

    c = math.sqrt(2 * a**2)
    d = c / 2

    forward_zack(a) # 1
    turtle.left(90)
    forward_zack(a) # 2
    turtle.left(45)
    forward_zack(d) # 3
    turtle.left(90)
    forward_zack(d) # 4
    turtle.left(135)
    forward_zack(a) # 5
    turtle.right(135)
    forward_zack(c) # 6
    turtle.right(135)
    forward_zack(a)
    turtle.right(135)
    forward_zack(c)

#zack_test()

turtle.setheading(0) # 0 == east
haus(200)

turtle.exitonclick()
