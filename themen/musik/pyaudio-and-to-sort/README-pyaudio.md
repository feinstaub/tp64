Pyaudio
=======

Audio
-----
* python-PyAudio (https://people.csail.mit.edu/hubert/pyaudio/)
    ** http://stackoverflow.com/questions/9770073/sound-generation-synthesis-with-python
* python-pygame / SDL / http://stackoverflow.com/questions/260738/play-audio-with-python
    ** http://stackoverflow.com/questions/10110905/simple-wave-generator-with-sdl-in-c

2016 Musik
----------
Funktionierendes Python-Beispiel mit pyaudio

    pyaudio2.py

### What are the notes for Tetris on piano?
    http://www.answers.com/Q/What_are_the_notes_for_Tetris_on_piano
        Line 1: E6 B5 C6 D6 C6 B5 A5 A5 C6 E6 D6 C6 B5 C6 D6 E6 C6 A5 A5
        Line 2: D6 F6 A6 G6 F6 E6 C6 E6 D6 C6 B5 B5 C6 D6 E6 C6 A5 A5

    https://www.youtube.com/watch?v=d56hzSMrl7M
        Tetris Theme - Korobeiniki [Easy Piano Tutorial]

### Noten / Frequenzen

https://de.wikipedia.org/wiki/Frequenzen_der_gleichstufigen_Stimmung

        [   'G4': 391.995, 'G#4', 415.305,
            'A4': 440,
            'A#4': 466.164, 'B4': 493.883, 'C5': 523.251, 'C#5': 554.365,
            'D5': 500, 'D#5': 500, 'E5': 500, 'F5': 500, 'F#5': 500, 'G5': 500, 'G#5': 500, # todo
            'A5': 880,
            'A#5': 500 # todo
            'B5': 987.767, 'C6': 1046.50, 'C#6': 1108.73, 'D6': 1174.66, 'D#6': 1244.51, 'E6': 1318.51, 'F6': 1396.91, 'F#6': 1479.98, 'G6': 1567.98, 'G#6': 1661.22,

        ]

    http://www.deimos.ca/notefreqs/

Notenwert (Länge)
    https://de.wikipedia.org/wiki/Notenwert

https://en.wikipedia.org/wiki/LilyPond
    LilyPond Essay, was Notenvisualisierung ausmacht

https://en.wikipedia.org/wiki/Frescobaldi_%28software%29
    Frescobaldi is an editor for LilyPond music files.
