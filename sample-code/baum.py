#!/usr/bin/env python3

import time

def baumkrone(zeile):
    i = 0
    while i < zeile:
        print("*", end='')
        i += 1
    print()

baumkrone(1)
baumkrone(2)
baumkrone(3)
baumkrone(4)
baumkrone(5)


def baumkrone(zeile):
    i = 0
    while i < 2 * zeile - 1:
        print("*", end='')
        i += 1
    print()

baumkrone(1)
baumkrone(2)
baumkrone(3)
baumkrone(4)
baumkrone(5)

def baumkrone(zeile, size):
    i = 0
    while i < size - zeile + 1:
        print(" ", end='')
        i += 1

    i = 0
    while i < 2 * zeile - 1:
        print("*", end='')
        i += 1
    print()
    time.sleep(0.1)

baumkrone(1, 7)
baumkrone(2, 7)
baumkrone(3, 7)
baumkrone(4, 7)
baumkrone(5, 7)

def baum(size):
    i = 0
    while i < size:
        baumkrone(i + 1, size)
        i += 1
    print()

baum(3)
baum(4)
baum(7)
baum(15)

def baum(size):
    i = 0
    while i < size:
        baumkrone(i + 1, size)
        i += 1

    i = 0
    while i < size / 4:
        k = 0
        breite = size / 3
        print(" " * int(size - breite / 2 + 1), end='')
        while k < breite:
            print("*", end='')
            k += 1
        print()
        i += 1

    print()

baum(3)
baum(4)
baum(7)
baum(15)
