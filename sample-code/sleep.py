#!/usr/bin/python3

import time

i = 0

while (True):
    print("{} Sekunde(n) vergangen.".format(i))
    time.sleep(1.0)
    i += 1
