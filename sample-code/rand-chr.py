#!/usr/bin/python3

# hilfe:py:randint

import random

i = 0
while (True):
    s = ""
    for a in range(0, 20):
        c = random.randint(65, 122)
        s += chr(c)
    print(str(i) + ":" + s)
    i += 1
