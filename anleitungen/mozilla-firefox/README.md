Mozilla Firefox Web-Browser
===========================

<!-- toc -->

- [uBlock Origin (Erweiterung)](#ublock-origin-erweiterung)
  * [Einleitung](#einleitung)
  * [Testseite mit Werbung](#testseite-mit-werbung)
  * [Installieren](#installieren)
  * [Einzelne Elemente manuell blockieren / Platzhalter](#einzelne-elemente-manuell-blockieren--platzhalter)
- [Häufige Fragen](#haufige-fragen)
  * [Historie löschen](#historie-loschen)
- [Flagfox (Erweiterung)](#flagfox-erweiterung)
- [Verschiedenes](#verschiedenes)
  * [View Page Info](#view-page-info)
  * [Mehr](#mehr)

<!-- tocstop -->

uBlock Origin (Erweiterung)
---------------------------
Baustein: "Werbeblock"

### Einleitung
uBlock Origin ist eine Firefox-Erweiterung. Damit lassen sich Überwachungsscripts und Werbung blockieren.

* Erhöht die IT-Sicherheit, denn unseriöse Werbung kann ein Eintrittstor für Schadsoftware sein
* Erhöht die Privatsphäre, denn Tracker-Scripts werden oft verwendet, um ungefragt das Nutzerverhalten aufzuzeichnen
* Entfernt Werbeclips aus YouTube-Videos
* Erhöht die Surf-Geschwindkeit, da weniger unnötige Daten für Werbebanner aus dem Internet geladen werden müssen
* Durch das Entfernen von animierten Werbeelementen wird der Prozessor weniger belastet

### Testseite mit Werbung
Rufe zuerst eine Test-Seite mit Werbung auf, z. B. https://www.pcwelt.de/

Du siehst vermutlich oben und rechts jeweils ein Werbebanner.

Jetzt uBlock Origin installieren...

### Installieren
Installiere die Erweiterung über die Addon-Seite: https://addons.mozilla.org/de/firefox/addon/ublock-origin/

Oder suche sie so:

* In Firefox das Menü oben rechts öffnen
* Add-Ons auswählen (oder Strg+Umschalt+A drücken)
* Oben rechts in die Suchleiste "ublock" oder "ublock origin" eingeben. Enter-Taste zum Suchen
* uBlock Origin auswählen und installieren

* Testseite erneut aufrufen
    * Besuche nun erneut die Seite https://www.pcwelt.de/
    * Die Werbebanner sollten nun weg sein.
    * Am uBlock-Origin-Symbol oben rechts sieht man die Anzahl der Elemente, die blockiert wurden.
    * Mit Klick auf das Symbol und den großen blauen Aus-Knopf kann man das Blockieren auch
    zeitweise ausschalten.

### Einzelne Elemente manuell blockieren / Platzhalter
* mit der Pipette, z. B. auf http://www.easy-10-finger.de oder andere Lieblingsseite
* Eigene Filter über MyFilters wieder löschen
* mit Platzhaltern arbeiten. / ggf. Baustein: "HTTP-Adresse" Web-Basics (www / de / # / Link kopieren, ggf. ändern und einfügen)

Siehe auch [mehr](mehr.md).

Häufige Fragen
--------------
### Historie löschen
Historie aus der Adressleiste löschen:

* Adressleiste aufmachen
* Eintrag markieren
* **Entf**-Taste drücken

Flagfox (Erweiterung)
---------------------
https://addons.mozilla.org/de/firefox/addon/flagfox/

* Zeigt an, wo der Server der angesurften Webseite steht.
* Gut zum Lernen von Länderflaggen.
* Gut zum Erkennen, wo überall auf der Welt die Rechner stehen

Verschiedenes
-------------
### View Page Info
* Rechte Maustaste -> View Page Info -> Media -> Zeigt alle eingebetten Medienelement wie Bilder und Videos.

### Mehr
* [mehr][mehr.md]
