Firewall ausschalten
====================

Standardmäßig ist die openSUSE-Firewall aktiviert. Dadurch werden alle vom eigenen Rechner angebotenen Dienste (wie z. B. Dateiaustausch, Chat-Programme) durch Zugriff von anderen Rechnern blockiert.

Wir befinden uns im Klassenraum in einer Umgebung, wo wir uns gegenseitig und den anwesenden Rechnern vertrauen. Gegenüber dem Internet sind wir durch eine Netzwerkfirewall geschützt. Um etwas über Netzwerke zu lernen, schalten wir daher unsere lokale Firewall aus. Damit kommen alle Anfragen bei den entsprechenden Dienste an und können verarbeitet werden.

Eine abgeschaltete Firewall bedeutet *nicht*, dass dann sofort jeder beliebig auf den eigenen Rechner zugreifen kann. Voraussetzung für einen Fremdzugriff ist dann immer noch eine Sicherheitslücke oder falsche Konfigurationen.

Firewall abschalten
-------------------
Einmalig: SuSEfirewall2 deinstallieren: `sudo zypper remove SuSEfirewall2`. Damit bleibt firewalld übrig.

Status prüfen: `systemctl status firewalld`

        firewalld.service - firewalld - dynamic firewall daemon
        Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: disabled)
        Active: active (running) since Thu 2018-04-12 13:50:04 CEST; 40min ago

ABSCHALTEN: `sudo systemctl stop firewalld`

EINSCHALTEN: `sudo systemctl start firewalld`

Hintergrund
-----------
### Allgemein
* https://de.wikipedia.org/wiki/Personal_Firewall
* https://de.wikipedia.org/wiki/Personal_Firewall#Personal_Firewall_als_Schutzma.C3.9Fnahme
* https://www.firewalld.org/
* https://en.wikipedia.org/wiki/Firewalld
* https://en.opensuse.org/Firewalld

### Tumbleweed-Verwirrung Januar 2018
* https://forums.opensuse.org/showthread.php/529290-can-anyone-provide-a-clear-overview-of-the-move-to-firewalld
    * https://forums.opensuse.org/showthread.php/529287-Update-after-kernel-14-11-1-killed-firewall-in-YaST
        * "Tumbleweed switched from SuSEFirewall2 to firewalld"
        * "Make sure that the firewalld.service is enabled, and configure it appropriately."
        * "Probably best to uninstall SuSEFirewall2 though"
* https://forums.opensuse.org/showthread.php/529283-Cannot-Turn-Off-Firewall-In-YAST-Firewall-Section
