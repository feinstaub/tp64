#include <stdio.h>
#include <iostream>
#include <chrono>       // std::chrono::steady_clock
#include <unistd.h>     // fork, execl, usleep
#include "termios_util.h"

using namespace std;

/**
 *    \x1B
 * == \033
 * == 27
 * == \e (non-standard GCC extension),
 *      https://stackoverflow.com/questions/10220401/rules-for-c-string-literals-escape-character,
 *      https://de.wikipedia.org/wiki/Escape-Sequenz
 */
namespace term
{
void clear()
{
    // https://stackoverflow.com/questions/4062045/clearing-terminal-in-linux-with-c-code
    // -> https://en.wikipedia.org/wiki/ANSI_escape_code#Unix-like_systems
    //    https://stackoverflow.com/questions/6486289/how-can-i-clear-console

    // \x1B == \033 == 27
    cout << "\x1B[2J\x1B[H"; // 2J = Clear Screen, H = move curser to the top-left corner
}

void newline()
{
    // CSI n B 	CUD 	Cursor Down
    // CSI n G 	CHA 	Cursor Horizontal Absolute 	Moves the cursor to column n (default 1)
    cout << "\x1B[B";
    cout << "\x1B[G";
}

void cursor_right(int x)
{
    cout << "\x1B[" << x << "C";
}
} // term

void sleep_ms(int ms)
{
    usleep(ms * 1000);
}

class StopWatch
{
public:
    StopWatch()
    {
        start = chrono::steady_clock::now();
    }

    int seconds()
    {
        return chrono::duration_cast<chrono::seconds>(chrono::steady_clock::now() - start).count();
    }

private:
    chrono::steady_clock::time_point start;
};

class Timer
{
public:
    Timer()
    {
    }

    void start(int ms)
    {
        duration_ms = ms;
        start_ = chrono::steady_clock::now();
    }

    bool finish()
    {
        return chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now() - start_) > chrono::milliseconds(duration_ms);
    }

    void restart()
    {
        start_ = chrono::steady_clock::now();
    }

private:
    chrono::steady_clock::time_point start_;
    int duration_ms;
};

class SlidingWord
{
public:
    SlidingWord(string word_)
    {
        word = word_;
        timer.start(150);
    }

    void draw()
    {
        for (int i = 0; i < ypos; i++) {
            term::newline();
        }
        term::cursor_right(xpos);
        cout << word;
        term::newline();
    }

    void calc()
    {
        if (timer.finish()) {
            timer.restart();
            xpos++;
        }
    }

    string word;
    int xpos;
    int ypos;

private:
    Timer timer;
};

class App
{
public:
    void do_getchar()
    {
        char ch = getchar();
        if (ch != -1) {
            lastchar = ch;
        }

        if (ch == 's') {
            word1.ypos++;
        }
        else if (ch == 'w') {
            word1.ypos--;
        }
    }

    void calc()
    {
        word1.calc();
    }

    void draw()
    {
        term::clear();
        cout << "Terminal Game Loop";
        term::newline(); // instead of \n use this!
        cout << "Draw counter: " << counter;
        term::newline();
        cout << sw.seconds() << " Sekunden seit Beginn";
        term::newline();
        cout << "lastchar: " << lastchar;
        term::newline();
        cout << "Verwende A S D W zum Steuern...";
        term::newline();
        word1.draw();
        term::newline();
        cout << "Press e to exit";
        counter++;
        // cout << flush; // not allowed
    }

    bool exit()
    {
        return lastchar == 'e';
    }

private:
    int counter = 0;
    StopWatch sw;
    SlidingWord word1 { "Hallo Test" };
    char lastchar;
};

int main()
{
    termios_guard tg;
    tg.echo = false;
    tg.canon = false;
    tg.blocking = false;
    tg.show_cursor = false;
    tg.setup();

    term::show_cursor(false);

    App app;
//     Timer calc_timer;
//     calc_timer.start(50);
    Timer draw_timer;
    //draw_timer.start(100); // Zeichne alle 100 ms => 10x/Sekunde
    draw_timer.start(50);

    while (!app.exit())
    {
        app.do_getchar();

//         if (calc_timer.finish()) {
//             calc_timer.restart();
//             app.calc();
//         }

        if (draw_timer.finish()) {
            draw_timer.restart();
            app.calc();
            app.draw();
        }

//         if (ch != -1) {
//             cout << "_" << ch << flush;
//             sleep_ms(500);
//         }

        sleep_ms(1); // Bremse
    }
    cout << endl;

    // TODO: catch Ctrl+C signal to show cursor again
    //   https://stackoverflow.com/questions/1641182/how-can-i-catch-a-ctrl-c-event

    return 0;
}
