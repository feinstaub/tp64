#!/usr/bin/env python3

# hilfe:py:print

print("Wie alt bist du?")
print("Ich bin 12 Jahre alt.")
print("Das ist schön.")

# AUFGABE:
#
# 1. Schreibe "Das ist ein Tannenbaum:" auf die Konsole
#    und gebe einen symbolischen Baum aus, z. B.
#
#       *
#      ***
#     *****
#       *
#       ^
