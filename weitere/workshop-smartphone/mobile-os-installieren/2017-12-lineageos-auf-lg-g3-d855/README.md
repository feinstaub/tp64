LineageOS auf LG G3 D855
========================

HARDWARE FAIL

<!-- toc -->

- [Telefon-Info Werkszustand](#telefon-info-werkszustand)
- [Installation](#installation)
- [Das Gerät rooten](#das-gerat-rooten)
  * [20.12.2017 - Versuch 1: PurpleDrake geht nicht](#20122017---versuch-1-purpledrake-geht-nicht)
  * [21.12.2017 - Versuch 2: KingRoot - unterbrochen](#21122017---versuch-2-kingroot---unterbrochen)
  * [05.01.2018 - Versuch 3: KingRoot - funktioniert nicht](#05012018---versuch-3-kingroot---funktioniert-nicht)
  * [08.01.2018 - Akku-Problem herausgefunden](#08012018---akku-problem-herausgefunden)
  * [12.01.2018 - Zweiten Akku ausprobieren](#12012018---zweiten-akku-ausprobieren)
  * [16.01.2018 - Aufgeben aufgrund Hardwaredefekt](#16012018---aufgeben-aufgrund-hardwaredefekt)

<!-- tocstop -->

Telefon-Info Werkszustand
-------------------------
* Einstellungen -> Über das Telefon -> Hardwareinfos
    * Modellbezeichnung: LG-D855
* Einstellungen -> Über das Telefon  -> Software-Infos
    * Android-Version: 6.0
    * Android-Sicherheitspatchlevel: 2016-08-01
    * Kernel-Version: 3.4.0
    * Build-Nummer: MRA58K
    * Software-Version: V30n-262-02

Installation
------------
* siehe: https://wiki.lineageos.org/devices/d855 "LG G3 (International) (d855)"
* Download Build from https://download.lineageos.org/d855
    * nightly
    * Version: 14.1
    * Dateiname: lineage-14.1-20171218-nightly-d855-signed.zip
        * Ziel: ~/dataDocuments/aux/mobile_phone/LG-G3/
    * 326.85 MB
    * Datum: 2017-12-18


Das Gerät rooten
----------------

### 20.12.2017 - Versuch 1: PurpleDrake geht nicht

* https://wiki.lineageos.org/devices/d855/install
    * https://forum.xda-developers.com/lg-g3/general/guide-root-lg-firmwares-kitkat-lollipop-t3056951
* selber gefunden für Linux
    * [xda-linux](https://forum.xda-developers.com/lg-g3/development/root-root-lg-g3-easily-purpledrake-lite-t2821000)
    * https://www.youtube.com/watch?v=TBJYUQxm7tc

* "[xda-linux]"
    * DOWNLOAD: http://downloads.codefi.re/thecubed/lg_g3/purpledrake/PurpleDrake-Lite_R03.tar.gz
        * Ziel: ~/dataDocuments/aux/mobile_phone/LG-G3/PurpleDrake-Lite_R03.tar.gz
    * "Ensure that your phone is in ADB mode (PTP Mode)"
    * Telefon mit USB verbinden
    * Statusleiste -> USB-Verbinden -> "Bilder senden (PTP)" auswählen
    * $ ./purpledrake_linux.sh

        chmod: cannot access './assets/linux/': No such file or directory
        Launching PurpleDrake for Linux

        Welcome to PurpleDrake!

        Please ensure your phone is connected, powered on, and USB debugging is properly enabled.
        Your phone should be showing your homescreen with no other apps open. This is to ensure a clean state.
        When you're ready, press [enter]...

        Press [enter] to continue...
        Waiting for device... .........


* ["Enable the Hidden Developer Options & USB Debugging on the LG G3 "](https://lg-g3.gadgethacks.com/how-to/enable-hidden-developer-options-usb-debugging-lg-g3-0156280/)
    * Einstellungen -> Über das Telefon -> Software-Infos -> Build-Nummer
    * Build-Nummer 7 mal antippen
    * "Sie sind nun ein Entwickler"
    * Einstellungen -> Entwickleroptionen (über "Über das Telefon")
        * Entwickleroptionen oben rechts aus und wieder anschalten (dann erst wird "USB-Debugging" aktiv
        * "USB-Debugging" aktivieren
        * Zulassen, dass der Computer auf das Telefon zugreift
        * $ adb
            * daemon not running. starting it now on port 5037 *
            * daemon started successfully *
            List of devices attached
            LGD8553310a08a  unauthorized
        * Telefon: "USB-Debugging zulassen?" --> Bestätigen
        * $ adb
            List of devices attached
            LGD8553310a08a  device

* "[xda-linux]"
    * $ ./purpledrake_linux.sh
        chmod: cannot access './assets/linux/': No such file or directory
        Launching PurpleDrake for Linux

        Welcome to PurpleDrake!

        Please ensure your phone is connected, powered on, and USB debugging is properly enabled.
        Your phone should be showing your homescreen with no other apps open. This is to ensure a clean state.
        When you're ready, press [enter]...

        Press [enter] to continue...
    * Homescreen anzeigen
    * Enter drücken
        Waiting for device... . Found device!

        Your phone is a [ LG-D855 ] on [ VDF ] running SW version [ V30n ] ...

        Alright. We're ready to go. We're going to temproot your phone first. Press enter when you're ready!
        Press [enter] to continue...

    * Enter drücken
        Rebooting phone...
        Waiting for device... ................. Found device!

        Found device, checking if vulnerable...

        ERROR: Your phone is not running a compatible software version. No vulnerable mode detected, rebooting phone.
        We tried to temproot you, and it didn't work. This phone isn't vulnerable, it would appear.
        Please post on XDA with your phone model, carrier, and SW version in the PurpleDrake thread and someone will take a look.
        Your phone has been rebooted back to normal system mode, no changes have been made. Thanks for trying!

* https://forum.xda-developers.com/lg-g3/development/root-root-lg-g3-easily-purpledrake-lite-t2821000/page103
    * "Works only on L not on MM. Try with kingroot"
        * https://en.wikipedia.org/wiki/Android_version_history
            * KitKat        4.4 – 4.4.4
            * Lollipop      5.0 – 5.1.1
            * Marshmallow   6.0 – 6.0.1

* Telefon ist plötzlich aus und geht nicht mehr an. ('n schwarzen Bildschirm hab ich!) (vermutlich erste Manifestierung des Akku-Problems)
    * => Akku raus und wieder rein. Startet wieder.
    * Beim Aktivieren der USB-Verbindung kommt ein lautes Kratzen aus dem Lautsprecher und das System hängt! :(
    * Wieder aus.
    * Nochmal dasselbe.
    * Neustart. USB reinstecken => Schwarz.
    * Jetzt geht es gar nicht mehr an.
    * Akku rein/raus, USB ab/dran
    * Startet wieder
    * Startbildschirm, lautes Kratzen aus dem Lautsprecher => Hängt => Schwarz
    * Scheiße.

* Factory Reset über die Einstellungen (ohne USB-Kabel angeschlossen)
    * Startet neu ("Setze Daten zurück..." oder ähnlich)
    * Startet neu. "Optimiere App 1 von 63" (ca.).
        * Bleibt bei ca. 10 hängen und schwarzer Bildschirm (geht aus).
        * Akku raus rein etc.
    * Anmachen. "Optimiere App 1 von 42".
        * Bleibt bei ca. 10 hängen und schwarzer Bildschirm (geht aus).
        * Akku raus rein etc.
    * Anmachen. "Optimiere App 1 von 34".
        * Bleibt bei ca. 10 hängen und schwarzer Bildschirm (geht aus).
        * Akku raus rein etc.
    * Anmachen. "Optimiere App 1 von 24".
        * Bleibt bei ca. 1 hängen und schwarzer Bildschirm (geht aus).
        * Akku raus rein etc.
    * Anmachen. "Optimiere App 1 von 24".
        * Bleibt bei ca. 2 hängen und schwarzer Bildschirm (geht aus).
        * Akku raus rein etc.
    * Anmachen. LG-Gong und Vodafone Logo (wie immer). "Optimiere App 1 von 23".
        * Geht aus.
        * Warte.
        * Akku raus rein etc.
    * Anmachen. "Optimiere App 15 von 22".
        * Geht aus.
        * Akku raus rein etc.
    * Anmachen. "Optimiere App 7 von 7".
        * Android wird gestartet...
        * Es kommen die Willkommensbildschirme.
        * Bei einem der Bildschirme bleibt es hängen.
    * Akku raus rein. Anmachen. Wird schon nach dem ersten. LG-Logo schwarz.
    * Akku raus rein. Anmachen. Wird schon nach dem ersten. LG-Logo schwarz.
    * Akku raus rein. Anmachen. Startet. Bleibt beim ersten erscheinenden Wizard-Dialog hängen.
    * Besitzer geht schlafen.

### 21.12.2017 - Versuch 2: KingRoot - unterbrochen

* Download APK: https://root-apk.kingoapp.com/kingoroot-download.htm
    * Ziel: ~/dataDocuments/aux/mobile_phone/LG-G3/
* Akku rein. Gerät starten. Wizards durchmachen. Entwickleroptionen aktivieren. USB-Debugging aktivieren.
* Das ging jetzt alles. Irgendetwas war wohl gestern noch im Speicher. (oder der Akku ist jetzt wieder kalt)
* $ adb push KingoRoot.apk /sdcard/KingoRoot.apk
* KingoRoot.apk anklicken
* Unbekannte Quellen zulassen. Nochmal.
* "Kingo ROOT. App wurde installiert."
* Starten
    * Kingo ROOT
    * Kein Root
    * Gerätemodell: G3
    * Android-Version: 6.0
    * Install recommened app: Haken drinlassen.
    * "Bitte prüfen Sie ihre Internetverbindung"

* Anleitung für LG:
    * https://www.kingoapp.com/root-lg.htm
    * https://root-android.kingoapp.com/lg/how-to-root-lg-g3-d850-android-devices.htm
        * "The device should be powered on and the battery level should be at least 50% and Internet should be connected (Wi-Fi is recommended). As rooting make lose your data so it is better to backup your data before you root. Remember to enable install from unknown sources on your device otherwise the apk can not be installed on your device."

* TODO:
    * http://www.jellydroid.com/2016/11/how-to-root-lg-k5-with-android-5-1-1-lollipop-via-kingroot-tutorial/
    * https://www.kingoapp.com/root-tutorials/top-10-must-to-do-after-rooting.htm

### 05.01.2018 - Versuch 3: KingRoot - funktioniert nicht

* WLAN auf Fritzbox aktiviert
* Mit dem LG auf das WLAN verbinden
* Es fangen im Hintergrund verschiedene Updates an
* Kingoroot aufrufen
    * "Erneut rooten"
        * Fortschrittsanzeige beginnt
        * "Fehlgeschlagen"
    * "Jetzt rooten"
        * Fortschrittsanzeige beginnt
        * Nach ca. 20 Sekunden Bildschirm schwarz. Nichts geht mehr. Kein Lichtchen etc.
        * Interessanterweise erkennt der Rechner ein eingestecktes USB-Kabel
    * Suche nach [LG geht nicht mehr an](https://forums.androidcentral.com/ask-question/427785-lg-g3-will-not-turn-charge.html)
        * Erfahrungsberichte von Neu-Käufern
            * Man soll das Telefon in den Wand-Charger stecken und 2 - 10 Minuten warten.
        * Bei mir hat es erst dann geklappt, als ich auch noch die Batterie raus habe.
* Kingoroot V4.3.4 erneut aufrufen
    * Wieder kein Erfolg mit Fehlercode.
    * Laut Angabe soll das Windows-Tool besseren Erfolg versprechen => TODO: Das ausprobieren
    * Gerät geht wieder aus und startet neu.
* Jetzt befindet sich das Gerät in einer Boot-Schleife => Nach dem Sperrbildschirm geht es wieder ins Reboot.
* Das passierte 2 - 3 x. Jetzt wieder aus.
* Wand-Charger-Trick
    * jetzt Bootschleife nur mit dem Bootscreen ("LG - Live's good - powered by Android")
    * ca. 3 x
    * Aber jetzt geht es wieder.
* Immer noch kein Root

* TODO next:
    * https://www.kingoapp.com/root-lg.htm
        * Download for Windows: https://www.kingoapp.com/android-root/download.htm
            * Server Error (500)
    * Dr.Fone-Script für Windows probieren: https://drfone.wondershare.com/root/lg-one-click-root.html
        * Datei runterladen: drfone_setup_full3360.exe
        * check mit VirusTotal: https://www.virustotal.com/#/file/8d8c842c1a7737e81558822962a83478e45b2f4a801921c77865f81179454ccd/detection
            * Clean

### 08.01.2018 - Akku-Problem herausgefunden
* ...
* siehe z. B. http://www.giga.de/smartphones/lg-g3/tipps/lg-g3-die-haeufigsten-probleme-und-loesungsansaetze/
* Gelernt: vorher zum Modell nach "die häufigsten Probleme und Lösungsansätze" suchen
* ...

### 12.01.2018 - Zweiten Akku ausprobieren
(auch beiden Akkus steht übrigens ein schwaches handgeschriebenes OK drauf)
* Erster Akku
    * Über eine Stunde YT-Videos geschaut und nix passiert.
    * USB-Kabel angeschlossen => nichts passiert, aber gemerkt, dass der Akku nicht aufgeladen wurde
    * Wall-Charger angeschlossen => erst normal. Dann manuell neugestartet. Nach ca. 5 Minuten nochmal geschaut. Das Gerät ist aus und geht nicht mehr an!
    * Bootschleife und dann geht wieder aus.
* Zweiten Akku einsetzen
    * 100% voll
    * Videos schauen
    * Laden ab 55 %
    * kein Fehler beobachtet
    * 100% voll
    * kein Fehler beobachtet

### 16.01.2018 - Aufgeben aufgrund Hardwaredefekt
* verschenkt
