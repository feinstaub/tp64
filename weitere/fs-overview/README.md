Freie Software: ein Überblick
=============================

* [Präsenzmaterial](praesenz)

* TODO:
    * Inhalte von FreieSW-Überblicksworkshop.odp einarbeiten
    * siehe Ordner "openSUSE-LIve-DVD-Demo--todo--Init-Script-ausarbeiten" / openSUSE-Live-DVD erstellen
    * E-Mail-Anbieter-Wechsel ist immer aktuell


TODO / Sichten:

* FreieSoftwareOG:
    * http://www.freiesoftwareog.org
    * Flyer mit Beispielen für freie SW-Programme
        * http://www.freiesoftwareog.org/downloads/FSOG_Flyer-Debian.pdf
            * VLC, LibreOffice, GIMP, Inkscape, Audacity
            * "Informieren Sie sich bei uns, wie Sie mit Freier Software neue Freiheiten für sich eröffnen!"
            * Was bedeutet "Freie Software"?
                * Richard Matthew Stallman
            * FreieSoftwareOG : Was machen wir?
            * FreieSoftwareOG : Was bieten wir?
    * http://www.freiesoftwareog.org/downloads.html
        * "Festplatten Testen, reparieren, defragmentieren"
        * "Audio- und Videos konvertieren mit GNU/Linux"
        * http://www.freiesoftwareog.org/downloads/FSOG_EventsDesJahres2017.pdf
            * VHS-Kurse: Linux-Kurs, Backup-Workshop, Gimp/Scribus, Mindmapping
            * Vorstellung Distros
        * "Webradio mit GNU/Linux"
        * "Virtualisierung unter GNU/Linux für Einsteiger"
        * "Programmieren für Einsteiger mit Freier Software"
        * "Screenshots mit GNU/Linux"
        * "Spiele unter GNU/Linux"

* https://mehr-freiheit.org
    * "Flyer für Freie Software" - "Acht Freie Computerprogramme, die Deine Rechte als Nutzer/-in respektieren und schützen."
        * "Internet & World Wide Web"
        * "Büro & Textverarbeitung"
        * "Musik & Tonbearbeitung"
        * "Video & Multimedia"
        * "Lernen & Entdecken"
        * "Grafik & Design"
        * "Spiel & Spaß"

* https://fsfe.org/campaigns/android/android.de.html
    * "Befreien Sie Ihr Android!"
    * unterstützt durch https://digitalcourage.de

* Fotobuch erstellen mit Scribus:
    * TODO sichten
        * ["Kapitel 5.1: Ein Fotobuch mit Scribus frei erstellen"](http://www.fotobuchmagazin.de/fotobuchgestaltung/kapitel-5/ein-fotobuch-mit-scribus-frei-erstellen/) auf fotobuchmagazin.de
        * ["Fotobuch-Profi Teil 4: Fotobuch erstellen mit Scribus - Erste Schritte und Überblick (Tutorial)"](http://www.fotobuchberater.de/fotobuch-ratgeber/382/fotobuch-profi-teil-4-fotobuch-erstellen-mit-scribus-erste-schritte-und-Ueberblick-(tutorial).html) auf fotobuchberater.de
