Kate
====

<!-- toc -->

- [Installieren](#installieren)
- [Aufrufen](#aufrufen)
- [Neue Datei erzeugen und speichern](#neue-datei-erzeugen-und-speichern)
- [Tipps fürs Programmieren](#tipps-furs-programmieren)
  * [Zeilen verschieben / kopieren](#zeilen-verschieben--kopieren)
  * [Auskommentieren](#auskommentieren)
  * [Zeilennummern](#zeilennummern)
  * [Das Wort unter dem Cursor suchen](#das-wort-unter-dem-cursor-suchen)
- [Programmieren](#programmieren)
  * [Terminal-Plugin aktivieren](#terminal-plugin-aktivieren)
  * [Minimap](#minimap)
  * [Code suchen und finden](#code-suchen-und-finden)
  * [Programm per Taste im Terminal ausführen](#programm-per-taste-im-terminal-ausfuhren)
- [Ideen für Projekte](#ideen-fur-projekte)
  * [XML-Tools](#xml-tools)
  * [Markdown](#markdown)
  * [Keyboard-Shortcut-Map](#keyboard-shortcut-map)
  * [Build-Plugin](#build-plugin)
- [NEU](#neu)
  * [Filter Text through Command line](#filter-text-through-command-line)

<!-- tocstop -->

Kate ist ein Texteditor mit speziellen Funktionen, so dass er sich gut zum Programmieren eignet.

Installieren
------------
    $ sudo zypper install kate

Aufrufen
--------
Zum Beispiel über KRunner (Alt+Leertaste) und "Kate" eingeben.

Neue Datei erzeugen und speichern
---------------------------------
* **Strg+N** (N wie neu) erzeugt einen neuen Eingabe-Puffer.
* Schreibe etwas Text hinein, z. B. "Hallo Text-Welt"
* Speichern mit **Strg+S** (S wie Speichern);
    * wähle dann einen Dateinamen aus, z. B. erzeuge einen neuen Ordner **~/ag** (falls nicht schon vorhanden) und nenne die Datei dort **neu.txt**.

Tipps fürs Programmieren
------------------------
Folgende Tipps sind beim Programmieren oft hilfreich.

### Zeilen verschieben / kopieren
* Zeile (oder alle Zeilen in der aktuellen Markierung)...
    * nach oben/unten **verschieben**: Strg+Umschalt+Cursor hoch/runter
    * nach oben/unten **kopieren**: Strg+Alt+Cursor hoch/runter

### Auskommentieren
* Auskommentieren: **Strg+D**
* Einkommentieren: **Strg+Umschalt+D**

### Zeilennummern
* Zeilennummern ein- oder ausblenden: **F11**
* Dauerhaft einblenden: Menü Einstellungen > Kate einrichten... > Erscheinung > Ränder > Zeilennummern anzeigen

### Das Wort unter dem Cursor suchen
* **Strg+H** (oder rückwärts mit **Strg+Umschalt+H**)

Programmieren
-------------
### Terminal-Plugin aktivieren
* Einstellungen -> Configure kate... -> Plugins -> **Terminal Tool View**
* Terminal -> Automatically sync with current document
* Eigenen Shortcut setzen, z. B. F4:
    * Tools → Shortcuts → Focus Terminal: F4

### Minimap
* Tools --> Einstellungen --> Editor-Komponente --> Erscheinungsbild --> Ränder --> Scrollbar Minimap anzeigen.

### Code suchen und finden
* siehe ../../anleitungen/dateien-suchen/README.md
* siehe auch [KDE: code-suchen-und-finden](../kde/code-suchen-und-finden.md)

### Programm per Taste im Terminal ausführen
* siehe "Run in terminal": https://bugs.kde.org/show_bug.cgi?id=349756
    * Configure Kate > Terminal > Set Prefix
    * Configure Shortcuts > Run Current Document > Shift+F4 (oder F5 anstelle Refresh)
    * Verbesserungsvorschläge:
        * zuerst Strg+A/D senden, um Zeile zu löschen

Ideen für Projekte
------------------
### XML-Tools
* "Format Document": bringt Übersichtlichkeit, falls ein XML-Dokument nur in einer Zeile vorliegt
* "Gehe zum Parent-Knoten": hilft den Überblick zu bewahren, falls sehr viele Knoten vorhanden sind

### Markdown
* Markdown-Hierarchie in einem Baum anzeigen

### Keyboard-Shortcut-Map
* siehe Configure Shortcuts -> Print, aber nur die Commands, die tatsächlich eine Taste zugewiesen haben
* und kompakteres Format

### Build-Plugin
* ...

NEU
---
### Filter Text through Command line
* siehe https://bugs.kde.org/show_bug.cgi?id=396864 - "Text Filter needs a better name or caption that reveals its true power"
