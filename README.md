# tp64

Programmieren lernen und GNU/Linux-Anleitungen.

Einstieg:

* [GNU/Linux auf USB-Stick installieren](themen/linux-installieren/opensuse-auf-usb-stick)
* Python: anleitungen/python-howtothink.md
* Nach Jahrgangsstufe: [5 und 6](anleitungen/einstieg-jgst-6.md), [7 und 8](anleitungen/einstieg-jgst-7.md), [11](anleitungen/einstieg-jgst-11.md)
* [Einstieg mit JavaScript im Browser](anleitungen/javascript-jshero.md)
* anleitungen/programmieraufgaben.md
    * Python-Turtle, Breakout-Spiel
* ShellTux
* Sonstiges: [Arbeitsblätter-Root](anleitungen), [Repair / spontan](weitere/repaircafe)

Weiteres:

* [Anleitungen](anleitungen) - Anleitungen und Aufgaben
* [Themen](themen)
    * [QML](themen/programmieren-qml)
* [Hintergrundwissen](hintergrundwissen)
* Halbfertig:
    * [themen/html/baum](themen/html/baum) (kam noch nicht zum Einsatz)
    * [themen/javascript/jsgl](themen/javascript/jsgl) (noch nicht verwendet)
    * [themen/unsorted](themen/unsorted)
    * [Ideen für Zwischendurch](anleitungen/ideen-zwischendurch.md)
    * [sample-code](sample-code) (zum Verlinken von woanders, todo: merge and move away)
* Organisation:
    * [Klassenregeln](meta/klassenregeln.md)

Git-Repository, damit immer auch offline verfügbar.
