KDE - Code suchen und finden
============================

<!-- toc -->

- [Online: lxr.kde.org](#online-lxrkdeorg)

<!-- tocstop -->

Online: lxr.kde.org
-------------------
* Beispiel: https://lxr.kde.org/ident?_i=KCollapsibleGroupBox
    * Damit kann man im kompletten KDE-Code nach Code suchen, z. B. nach Verwendungen von KCollapsibleGroupBox.
        * Eine ist in ark (dem Archiv-Programm). Wenn du im Menü Archive > New machst, kann man sich gleich 3 Boxen anschauen.
