SSH auf openSUSE Linux
======================

<!-- toc -->

- [Neuen Unix-User anlegen](#neuen-unix-user-anlegen)
- [SSH-Server aktivieren](#ssh-server-aktivieren)

<!-- tocstop -->

Neuen Unix-User anlegen
-----------------------
TODO

SSH-Server aktivieren
---------------------
* siehe https://en.opensuse.org/SDB:Configure_openSSH

$ systemctl start sshd

$ systemctl status sshd

    ● sshd.service - OpenSSH Daemon
    Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: disabled)
    Active: active (running) since Fri 2017-12-08 11:08:15 CET; 2h 6min ago
    Process: 1255 ExecStartPre=/usr/sbin/sshd-gen-keys-start (code=exited, status=0/SUCCESS)
    Main PID: 1267 (sshd)
        Tasks: 1
    CGroup: /system.slice/sshd.service
            └─1267 /usr/sbin/sshd -D

$ systemctl stop sshd

$ systemctl status sshd

    ● sshd.service - OpenSSH Daemon
    Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: disabled)
    Active: inactive (dead) since Fri 2017-12-08 13:15:58 CET; 2s ago
    Process: 1267 ExecStart=/usr/sbin/sshd -D $SSHD_OPTS (code=exited, status=0/SUCCESS)
    Process: 1255 ExecStartPre=/usr/sbin/sshd-gen-keys-start (code=exited, status=0/SUCCESS)
    Main PID: 1267 (code=exited, status=0/SUCCESS)

Für auch nach dem Systemneustart:

$ systemctl enable sshd

$ systemctl disable sshd

TODO: check status and Firewall: https://serverfault.com/questions/766676/how-to-enable-ssh-on-startup-in-opensuse
