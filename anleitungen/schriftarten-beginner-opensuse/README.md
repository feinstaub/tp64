Schriftarten-Einstieg
=====================

<!-- toc -->

- [Schriftarten-Manager installieren (Fontmatrix)](#schriftarten-manager-installieren-fontmatrix)
- [Schriftartenliste anschauen](#schriftartenliste-anschauen)
  * [Bekannte Fallen: ``](#bekannte-fallen-)
  * [Bekannte Fehler](#bekannte-fehler)
- [Gute Schriftarten merken](#gute-schriftarten-merken)
  * [Einmalig](#einmalig)
  * [Dann](#dann)
- [Weitere Schriftarten installieren - Teil 1](#weitere-schriftarten-installieren---teil-1)
- [Weitere Schriftarten installieren - Teil 2](#weitere-schriftarten-installieren---teil-2)
- [Ausblick](#ausblick)
  * [Weitere Schriftarten online](#weitere-schriftarten-online)
  * [Eigenes Schriftenbuch erzeugen](#eigenes-schriftenbuch-erzeugen)
  * [Andere Schriftartenmanager](#andere-schriftartenmanager)
  * [Exkurs: ASCII-Schrift auf der Konsole](#exkurs-ascii-schrift-auf-der-konsole)
  * [Google Font Catalog](#google-font-catalog)

<!-- tocstop -->

Schriftarten-Manager installieren (Fontmatrix)
----------------------------------------------
Ein Schriftartenmanager ist empfehlenswert, damit du dir einen Überblick über die auf deinem System vorhandenen Schriftarten verschaffen kannst. Es gibt verschiedene Programme, eins davon ist **Fontmatrix**.

Fontmatrix installieren: https://software.opensuse.org/package/fontmatrix?search_term=fontmatrix (verwende das **KDE:Extra-Repository**).

Schriftartenliste anschauen
---------------------------
![](images/fontmatrix.png)

### Bekannte Fallen: `<name>`
* Unten in der Textbox steht `<name>`. Das heißt in der obigen Liste werden die Schriftartenamen angezeigt. Wenn man `<name>` ganz löscht, wird oben nichts mehr angezeigt.
    * (Die Textbox ist leicht mit einem Suchfeld zu verwechseln)
* Tipp: Anstelle von <name> kann ein eigener Text als Schriftprobe eingegeben werden.

### Bekannte Fehler
* openSUSE Tumbleweed 2017-04, fontmatrix v0.6.1
    * Die Ansichten auf der rechten Seite scheinen teilweise nicht zu funktionieren (leere Ansicht).

Gute Schriftarten merken
------------------------
Mit Fontmatrix kann man Schriftarten mit "Tags" versehen, um sie später besser wiederzufinden.

### Einmalig
* Oben im Menü **View** die **Tags**-Ansicht aktivieren.
* Eine Schriftart auswählen und (einmalig) einen Tag (z. B. "gut") erzeugen: mit Hilfe des **Add Tag**-Knopfes.

### Dann
Danach kann dieser Tag für weitere gute Schriftarten weiterverwendet werden.

![](images/fontmatrix-tags.png)

Nur "gute" Schriftarten anzeigen:

![](images/fontmatrix-view-tags.png)

Weitere Schriftarten installieren - Teil 1
------------------------------------------
Mit

```
$ sudo zypper install fetchmsttfonts
```

kannst du dir dann ein paar bekannte Schriftarten von Microsoft installieren (wie z. B. Arial oder Comic Sans).

Fontmatrix neu starten, damit die Schriftarten dort angezeigt werden.


Weitere Schriftarten installieren - Teil 2
------------------------------------------
Wenn es etwas Ausgefalleneres sein soll, gibt es hier weitere Schriftarten:

https://fontlibrary.org/en/search?category=handwriting

Die Schriften kannst du dir einzeln runterladen und installieren:

* Schriftart anklicken
* Oben rechts auf "Download"
* Öffne dazu nach dem Download das Archiv mit **Ark** und klicke die **otf**-Datei an (und wenn keine da ist, dann die ttf-Datei).

Es öffnet sich eine Schriftart-Vorschau. Unten rechts ist ein "Installieren..."-Knopf. Drücke da drauf. Es öffnet sich eine Frage "Persönlich" oder "System". Wähle "Persönlich", dann landen die Schriften in deinem Home-Verzeichnis unter **~/.fonts** und du kannst sie später besser sichern (bzw. sie bleiben erhalten, wenn du Linux neu installierst).

![](images/install-font.png)

Fontmatrix neu starten, damit die Schriftarten dort angezeigt werden.

Ausblick
--------
### Weitere Schriftarten online
* https://fontlibrary.org

* Art Deco: http://www.1001fonts.com/art-deco-fonts.html

* Artistic: http://www.fontspace.com/category/artistic

* Linux Libertine: http://www.linuxlibertine.org/index.php

* http://www.glukfonts.pl/fonts.php

### Eigenes Schriftenbuch erzeugen
Wenn du deine Lieblingsschriftarten übersichtlich ausdrucken möchtest, kann man mit Fontmatrix ein Schriftenbuch im PDF-Format erzeugen:

* Datei --> Schriftenbuch exportieren...

### Andere Schriftartenmanager
* Plasma "Font Management" System Setting Module
    * Schnelle Vorschau von Schriftarten
    * Schriftarten in Ordnern gruppieren
        * (todo: waren die Gruppen plötzlich weg?)

### Exkurs: ASCII-Schrift auf der Konsole
```
$ sudo zypper install figlet
```

```
$ figlet Hallo Test
 _   _       _ _         _____         _
| | | | __ _| | | ___   |_   _|__  ___| |_
| |_| |/ _` | | |/ _ \    | |/ _ \/ __| __|
|  _  | (_| | | | (_) |   | |  __/\__ \ |_
|_| |_|\__,_|_|_|\___/    |_|\___||___/\__|
```

* Mehr siehe z. B. hier https://www.shellhacks.com/create-ascii-text-banner-linux-command-line/

### Google Font Catalog
* Weitere freie Schriftarten gibt es hier: https://fonts.google.com/
    * Entweder: manuell einzeln herunterladen
    * Oder: komfortabler mit diesem Programm: [Typecatcher](https://github.com/andrewsomething/typecatcher)
        * Die ist derzeit nicht für openSUSE paketiert; Projekt-Idee: selber kompilieren
