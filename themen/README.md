Themen
======

Inhalt
------
* Für Umgang mit Texteditor, siehe Baustein "Kate"
* [Internet: E-Mail](internet-email)
* [Plasma-Desktop](plasma-desktop)
* ...
* [Webentwicklung](programmieren-web)
* [Lego NXT mit Python](lego-nxt-mit-python)
* [Eigener Onlinespeicher mit nextCloud](internet-datenaustausch/README-nextcloud.md)
* [Behandlung von Linux-Ausnahme-Situationen](linux-verwenden/probleme.md)
* [Physik-Simulationen](physik-simulationen)
* [Programmiersprache Rust](programmieren-rust)
* [Musik](musik)

### Verschiedenes
* [IT-Sicherheit](../hintergrundwissen/itsec)
* [Datensicherung](../hintergrundwissen/datensicherung)

Für Zuhause - Programme entdecken 1
-----------------------------------
* Öffne das K- oder openSUSE-Menü (unten links in der Ecke)
    * Starte ein Programm, das sich interessant anhört.
    * Versuche es zu verstehen.
    * Gehe weiter zum nächsten.

* Lese dir durch, wie man [neue Software findet und installiert](https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Eigenes_System/Software_finden_und_installieren). (todo: move content to here)
    * Erkunde das, was sich interessant anhört.

* Erkunde die Liste der [empfehleneswerten Programme](programme/README.md)

Für Zuhause - Programme entdecken 2
-----------------------------------
* [Spiele-Liste](programme/spiele.md)
* Konsole
    * cowsay
    * hollywood - Console toy
    * no-more-secrets - Console toy
    * ponysay - Console toy

Für Zuhause - Hardwarekauf
--------------------------
* [Was man beim Hardwarekauf beachten kann](hardwarekauf)

Bausteine
---------
Suche nach "Baustein:"

### Baustein: "AsciiArt"
* Stufe 1 - cowsay, figlet:
    * ...können cowsay verwenden
    * ...können figlet verwenden
    * über eigenes Linux oder OnWorks

    * cowsay online
        * http://cowsay.morecode.org/
        * https://helloacm.com/cowsay/

    * figlet:
        * figlet Hallo
        * showfigfonts
            * z. B. figlet -f banner Hallo
        * figlet online
            * https://www.askapache.com/online-tools/figlet-ascii/
            * https://www.kammerl.de/ascii/AsciiSignature.php

* Stufe 2:
    * ...wissen den Unterschied zwischen Proportional- und Fixed-Fonts
    * ...kreativ mit AsciiArt
    * Experimentieren mit LibreOffice
        * figlet Hallo vergrßern und "versteckten Text" in die Leerräume der Buchstaben schreiben
    * Grußkarten mit AsciiArt (cowsay und figlet) verschönern

* Stufe 3:
    * ...kennen die ASCII-Tabelle
    * Programmieraufgabe: (z. B. mit C++) ASCII-Zeichen ausgeben
    * bash oder per Hand: cowfile list to file
        * Programmieraufgabe: die Datei auslesen und interaktiv den Nutzer aus verschiedenen cowsfiles wählen lassen

* Stufe 4:
    * ...können ein vorhandenes Programm selber kompilieren
    * cowsay-Quellcode hernehmen und selber kompilieren

* Stufe 5:
    * ...können ein Programm verändern
    * cowsay erweitern
        * z. B. mit Farben, mit echo-Codes oder ncurses?

### Baustein: "Kate"
* ...können mit einem Texteditor umgehen

* Stufe 1:
    * Level-Dateien bearbeiten:
        * siehe c-with-3guys
        * Game of Life?
        * HTML?
* Stufe 2:
    * Terminal-View
    * Editor-Shortcuts
    * ... ...
    * ... ...
* Stufe n:
    * Mit ShellTux Redirect File output lernen und mit Kate Ergebnis anschauen.
