Remote Desktop
==============

<!-- toc -->

- [Von Windows aus auf Linux verbinden](#von-windows-aus-auf-linux-verbinden)
- [Exkurs - mc Midnight Commander](#exkurs---mc-midnight-commander)
- [Exkurs - Datenaustausch](#exkurs---datenaustausch)
  * [Linux - LAN - Linux](#linux---lan---linux)
  * [Linux - LAN - Windows](#linux---lan---windows)
  * [Linux/Windows - LAN - Linux/Windows](#linuxwindows---lan---linuxwindows)
  * [Linux/Windows - Internet - Linux/Windows](#linuxwindows---internet---linuxwindows)

<!-- tocstop -->

Verschiedene Möglichkeiten mit einem Remote-Desktop zu arbeiten.

Von Windows aus auf Linux verbinden
-----------------------------------
siehe README-xserver-for-win.md

Exkurs - mc Midnight Commander
------------------------------
    $ sudo zypper install mc
    $ mc

* [Einführung (engl.)](https://www.linode.com/docs/tools-reference/tools/how-to-install-midnight-commander)
    * Interessant: die Quelle der Seite liegt auf [github als Markdown](https://github.com/linode/docs/blob/master/docs/tools-reference/tools/how-to-install-midnight-commander.md)
        * Einstieg: https://github.com/linode/docs
        * Linode - "Get a Server Running in Seconds - Deploy a Linux virtual server in seconds from the Linode Cloud. Choose your resources, Linux distro, and node location right from the Manager."

Exkurs - Datenaustausch
-----------------------
Wie kann Daten untereinander austauschen kann, indem man Dateien transferiert.

### Linux - LAN - Linux
* scp
    ** transfer-User anlegen
    ** ...
    ** siehe http://unix.stackexchange.com/questions/106480/how-to-copy-files-from-one-machine-to-another-using-ssh
* NFS

### Linux - LAN - Windows
* https://winscp.net/[WinSCP] (https://winscp.net/eng/docs/license)
* Samba-Server: https://www.samba.org/

### Linux/Windows - LAN - Linux/Windows
* z. B. mit https://www.sharedrop.io/

### Linux/Windows - Internet - Linux/Windows
* z. B. mit https://www.sharedrop.io/
