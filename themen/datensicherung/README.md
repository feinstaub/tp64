Datensicherung
==============

<!-- toc -->

- [Backup-Software](#backup-software)
  * [2019](#2019)
  * [2017](#2017)
  * [Weitere](#weitere)
- [Wiederherstellung](#wiederherstellung)

<!-- tocstop -->

* Das Home-Verzeichnis sichern reicht in der Regel aus.
* Alle Dateien und Ordner manuell auf einen externen Datenträger kopieren.
* Oder Backupsoftware verwenden, wie z. B. FreeFileSync.
* Es gibt im Home-Verzeichnis, die *müssen nicht gesichert* werden, z. B.:
    * **~/.cache** (diese Dateien werden bei Bedarf von den jeweiligen Programmen neu erzeugt)

Backup-Software
---------------
### 2019
* Systembackup mit Timeshift: https://github.com/teejee2008/timeshift
* Datenbackup mit Back in Time: https://github.com/bit-team/backintime

### 2017
Ein gute Übersicht mit Erläuterungen bietet diese (englische) Seite: https://wiki.archlinux.org/index.php/Synchronization_and_backup_programs

Empfehlungen:

* [FreeFileSync](https://software.opensuse.org/package/FreeFileSync) (Stand 30.03.2017: Tumbleweed-Build schlägt fehl)
* [Grsync](http://www.opbyte.it/grsync/download.html)
* [luckyBackup](https://software.opensuse.org/package/luckybackup)

### Weitere
-----------
* Siehe http://askubuntu.com/questions/2596/comparison-of-backup-tools
* Siehe https://wiki.debian.org/BackupAndRecovery
* Auf dem openSUSE-Build-Service: https://build.opensuse.org/project/show/home:ecsos:Backup


Wiederherstellung
-----------------
Beim Erstellen einer Sicherung sollte man sich vorher Gedanken machen wie man das Gesicherte im Ernstfall wiederherstellen kann.
