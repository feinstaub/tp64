#pragma once
#include <SFML/Window/Keyboard.hpp> // siehe /usr/include/...
#include <string>
#include <vector>
#include "tools.h"

struct ProgramKey
{
    char key;
    int mod; // 1: Shift, 2: Ctrl, 3: Shift + Ctrl
    std::string text;

    std::string to_string() const
    {
        using namespace std;
        string mod_s;
        if (mod == 1) {
            mod_s = "Shift+";
        } else if (mod == 2) {
            mod_s = "Ctrl+";
        } else if (mod == 3) {
            mod_s = "Ctrl+Shift+";
        }
        char key_up = toupper(key);
        return tools::tt_bold_color(mod_s + key_up, 32) + "[" + text + "]";
    }
};

class ProgramKeys
{
public:
    /**
     * returns key handle
     */
    int register_key(char key, int mod, const std::string& text)
    {
        keys.push_back({ key, mod, text });
        return keys.size() - 1;
    }

    /**
     */
    bool shiftPressed()
    {
        // https://www.sfml-dev.org/documentation/2.5.1/classsf_1_1Keyboard.php
        return sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)
                    || sf::Keyboard::isKeyPressed(sf::Keyboard::RShift);
    }

    bool ctrlPressed()
    {
        return sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)
                    || sf::Keyboard::isKeyPressed(sf::Keyboard::RControl);
    }

    /**
     * ch: the key from getch
     * In combination with sf::Keyboard::isKeyPressed for Shift and Ctrl
     * returns the key handle or -1 if not registered
     */
    int key_pressed(char ch)
    {
        using namespace std;

        bool shift = shiftPressed();
        bool ctrl = ctrlPressed();

        if (ctrl) {
            // Strg+a liefert eine 1
            // Strg+b liefert eine 2
            // etc.
            ch = ch + 'A' - 1;
        }

        for (int i = 0; i < keys.size(); i++)
        {
            const ProgramKey& pkey = keys[i];
            if (std::tolower(pkey.key) == std::tolower(ch)) {
                if (pkey.mod == 0 && !shift && !ctrl)
                    return i;
                else if (pkey.mod == 1 && shift && !ctrl)
                    return i;
                else if (pkey.mod == 2 && !shift && ctrl)
                    return i;
                else if (pkey.mod == 3 && shift && ctrl)
                    return i;
            }
        }

        return -1;
    }

    const std::vector<ProgramKey>& get() const
    {
        return keys;
    }

private:
    std::vector<ProgramKey> keys;
};
