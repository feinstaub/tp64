#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

struct Vok
{
    // Verben
    string v_singularstamm;             // putare
    string v_perfektstamm;              // putav
    //string v_singular_endung;
    //string v_perfekt_endung;
};

vector<Vok> lade_vokabeln()
{
    vector<Vok> liste;

//     Vok v;// { "putare", "putav" };
//     v.v_singularstamm = "putare";
//     v.v_perfektstamm = "putav";
//     liste.push_back(v);
//
//     v.v_singularstamm = "hallo";
//     v.v_perfektstamm = "test";
//     liste.push_back(v);

    string filename = "vokabeln.txt";
    ifstream ifs(filename);

    if (!ifs.is_open()) {
        throw runtime_error("Unable to load file: " + filename);
    }

    while (!ifs.eof())
    {
        string a;
        string b;
        ifs >> a;
        ifs >> b;

        Vok v;
        v.v_singularstamm = a;
        v.v_perfektstamm = b;
        liste.push_back(v);
    }

    return liste;
}

int main()
{
    vector<Vok> vokabeln = lade_vokabeln();

    for (Vok vok : vokabeln)
    {
        cout << vok.v_singularstamm << " " << vok.v_perfektstamm << endl;
    }

    return 0;
}
