Lego Mindstorm NOTES
====================
/ Roboter

NXT
---
* wird eingestellt: https://education.lego.com/de-de/lesi/middle-school/mindstorms-education-ev3/nxt-vs-ev3
    * ABER: dank nxt-python (siehe unten) kann die vorhandene Hardware weiterhin betrieben werden
* https://de.wikipedia.org/wiki/Lego_Mindstorms_NXT

nxt-python
~~~~~~~~~~
* https://github.com/Eelviny/nxt-python/wiki/Tutorial
* https://github.com/Eelviny/nxt-python/wiki/Where-to-find-help
* https://github.com/Eelviny/nxt-python/wiki/FAQ
* https://github.com/Eelviny/nxt-python/wiki/Digital-Sensor-Guide

* https://en.wikipedia.org/wiki/Lego_Mindstorms_NXT#NXT-Python

Weiteres
~~~~~~~~
* http://bricks.stackexchange.com/questions/1404/is-it-possible-to-retrive-a-text-file-from-an-nxt
    * dafür gibt es in nxt-python den FileReader etc.
    * Bricx Command Center 3.3 - aber in Delphi geschrieben und hauptsächlich für Windows

* Using Python to Program LEGO MINDSTORMS Robots: The PyNXC Project: https://www.researchgate.net/publication/49336178_Using_Python_to_Program_LEGO_MINDSTORMS_Robots_The_PyNXC_Project

* Raspberry controls NXT: https://rebeccaajones.wordpress.com/2013/06/19/raspberry-pi-robotics-part-1-pi-controlling-the-nxt/

EV3
---
- http://www.ev3dev.org/
    ev3dev is a Debian Linux-based operating system that runs on several LEGO® MINDSTORMS
    compatible platforms including the LEGO® MINDSTORMS EV3 and Raspberry Pi-powered BrickPi.

    - TODO: http://www.ev3dev.org/docs/getting-started/
    - Written in python: http://www.ev3dev.org/projects/2015/05/06/EV3-Print3rbot/

Weitere Ideen
-------------
* Schall-Richtungserkennung
    * Ein Kopf mit Augen (oder eine Webcam) wird drehbar
        * Webcam ansteuern? (siehe hier? http://stackoverflow.com/questions/9711946/how-to-programmatically-capture-a-webcam-photo)
    * 3 Schallsensoren sorgen für die Erkennung der Richtung, wo der Schall herkommt, geht das?
