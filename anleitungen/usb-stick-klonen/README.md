USB-Stick klonen
================

ACHTUNG: Fortgeschrittenes Thema. Kann zu Datenverlust führen.

* "dd dient zum bit-genauen Kopieren von Festplatten, Partitionen oder Dateien. "Bit-genaues" Kopieren bedeutet, dass der Datenträger Bit-für-Bit bzw. Byte-für-Byte ausgelesen und beschrieben wird, unabhängig von dessen Inhalt und Belegung." siehe https://wiki.ubuntuusers.de/dd/

Beispiel-Aufruf:

```
$ dd if=/dev/sda of=/dev/sdb
```

* if = Quelle
* of = Ziel
    * Man muss sich 100% sicher sein, dass das richtig ist, ansonsten Datenverlust.
    * Muss gleich groß oder größer als Quelle sein

Man merkt, dass das Programm fertig ist, wenn es sich beendet und das Shell-Prompt wieder da ist:

"Läuft noch":
```
$ dd if=/dev/sda of=/dev/sdb

```

"Fertig":
```
$ dd if=/dev/sda of=/dev/sdb
$
```


Fortschrittsanzeige
-------------------

### Variante mit dd
dd zeigt beim Kopieren standardmäßig keinen Status an. Da ein Kopiervorgang eines 30 GB-Sticks länger als 45 Minuten braucht, wäre es hilfreich den Fortschritt zu sehen.

Siehe hier: https://blog.cscholz.io/dd-mit-statusanzeige/79/

### Variante mit dc3dd

* $ zypper info dc3dd
* $ sudo zypper install dc3dd

Da ist die Fortschrittsanzeige schon eingebaut.
