Suche neues Smartphone 2018 (1)
===============================

2018
----
[List of open-source mobile phones](https://en.wikipedia.org/wiki/List_of_open-source_mobile_phones)

* https://en.wikipedia.org/wiki/Meizu_PRO_5
    * [ebay](http://www.ebay.de/itm/Meizu-Pro-5-Smartphone-Handy/253352199897?hash=item3afcf7c6d9:g:zagAAOSwVLRaT01v) - 210 EUR
    * [ebay](https://www.ebay.de/itm/MEIZU-MX4-16GB-Ohne-Simlock-Smartphone-gray/142656121517?hash=item2136f7aaad:g:98UAAOSwXoxaW6xB), originalverpackt

https://wiki.lineageos.org/devices/

### Sony Xperia Z (verworfen)
* https://wiki.lineageos.org/devices/yuga
    * Released: 2013-02
    * RAM: 2 GB LPDDR2
    * CPU: Quad-core Krait, 1.5 GHz
    * Storage: 16 GB
    * Battery: Non-removable 2330 mAh / Akku fest verbaut
    * leicht zu rooten
* [ebay-Suche](https://www.ebay.de/sch/i.html?_from=R40&_sacat=0&_nkw=sony%20xperia%20z&_dcat=9355&Modell=Sony%2520Xperia%2520Z&rt=nc&_trksid=p2045573.m1684)
    * ["Sony Xperia Z - 16GB - Schwarz (Ohne Simlock) Smartphone. Ohne Branding."](https://www.ebay.de/itm/Sony-Xperia-Z-16GB-Schwarz-Ohne-Simlock-Smartphone-Ohne-Branding/322998320618?hash=item4b34333dea:g:RoEAAOSwcSxaWgz1) - 90 EUR
        * "Vom Verkäufer generalüberholt"
        * "Darüber hinaus wurde das Display vor 4 Monaten erneuert, der Akku und der Vibrationsalarn Buzzer."
            * [Forum: "Vibration geht nicht!"](https://www.android-hilfe.de/forum/sony-xperia-p.557/vibration-geht-nicht.337095.html)
            * [Akku selber wechseln mit Video](https://www.kaputt.de/anleitungen/12070/sony-xperia-z-akku)
    * ["Sony Xperia Z", gebraucht](https://www.ebay.de/itm/Sony-Xperia-Z/192425186776?hash=item2ccd6f55d8:g:VewAAOSwVLRaWJR3) - 52 EUR
* Suche "sony xperia z häufige probleme"
    * WLAN-Probleme? (https://www.android-hilfe.de/forum/sony-xperia-xz1-compact.3122/wlan-probleme-abbrueche.853538.html)

### Ebay LineageOS vorinstalliert
* https://www.ebay.de/sch/i.html?_from=R40&_trksid=p2047675.m570.l1313.TR3.TRC1.A0.H0.Xlineageos.TRS0&_nkw=lineageos&_sacat=0
    * u. a. auch Samsung S4 mini
        * https://wiki.lineageos.org/devices/serranoltexx

### Motorola Moto G 1. Generation
* siehe z. B. https://community.bahn.de/questions/1420108-db-navigator-fur-android-apk-download
* https://wiki.lineageos.org/devices/falcon
* **ca. 60 EUR** ebay mit LineageOS vorinstalliert, siehe themen/eigenes-mobil-betriebssystem-installieren/lineageos-auf-motorola-g/README.md
