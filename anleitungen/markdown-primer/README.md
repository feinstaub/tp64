Markdown für Einsteiger
=======================

<!-- toc -->

- [Was ist Markdown?](#was-ist-markdown)
- [Lokale Vorschau im Browser](#lokale-vorschau-im-browser)
  * [- grip installieren](#--grip-installieren)
  * [- grip verwenden](#--grip-verwenden)
  * [- So könnte es aussehen](#--so-konnte-es-aussehen)
  * [- Ändern und speichern](#--andern-und-speichern)
- [Inhaltsverzeichnis mit markdown-toc](#inhaltsverzeichnis-mit-markdown-toc)
  * [- Installieren](#--installieren)
  * [- Verwenden](#--verwenden)
- [Weitere Tools](#weitere-tools)
  * [- MUP](#--mup)
  * [- Markmywords](#--markmywords)
  * [- CuteMarkEd](#--cutemarked)
  * [- siehe auch](#--siehe-auch)

<!-- tocstop -->

Was ist Markdown?
-----------------
* Der vorliegende Text wurde mit Markdown erstellt. So sehen Teile des Quellcode aus:


        Was ist Markdown?
        -----------------
        * Der vorliegende Text wurde mit Markdown erstellt. So sehen Teile des Quellcode aus:

                [...dieser Code-Block...]

        * Weitere Beispiele: https://de.wikipedia.org/wiki/Markdown#Auszeichnungsbeispiele

        Lokale Vorschau im Browser
        --------------------------
        Unter https://github.com/joeyespo/grip ist ein Programm zu finden, mit dem man eine Vorschau im Browser erhält; mit Auto-Refresh. Das Programm ist in Python geschrieben und verwendet das Werkzeug-Web-Framework.

        ### Installieren

        ```
        $ pip install --user grip
        ```
        Damit wird grip nach ~/.local/bin installiert und sollte somit von der Kommandozeile verfügbar sein.

        ### Verwenden

        README.md in Kate öffnen.


* Weitere Beispiele: https://de.wikipedia.org/wiki/Markdown#Auszeichnungsbeispiele

Lokale Vorschau im Browser
--------------------------
Unter https://github.com/joeyespo/grip ist das Programm **grip** zu finden, mit dem man eine Vorschau seines Markdown-Textes im Browser erhält; mit Auto-Refresh. Das Programm ist in Python geschrieben und verwendet das _Werkzeug_-Web-Framework.

### - grip installieren

```
$ pip install --user grip
```

Damit wird grip nach ~/.local/bin installiert und sollte somit von der Kommandozeile verfügbar sein. TODO: Was tun, wenn nicht?

### - grip verwenden

Erstelle eine Datei **README.md** (z. B. mit Kate). Kopiere den Inhalt der Datei [source.txt](source.txt) dort hinein und speichere die Datei.

Im Terminal grip aufrufen:

```
$ grip
 * Running on http://localhost:6419/ (Press CTRL+C to quit)
```

Achtung. Dieser Aufruf verwendet das Github-Online-API. Daher bitte den Offline-Renderer verwenden, siehe [tool-markdown-grip-offline](../../meta/tool-markdown-grip-offline).

Mit dem Browser die angegebene Adresse aufrufen.

### - So könnte es aussehen

![screenshot](images/screenshot.png)

### - Ändern und speichern

Irgendetwas README.md ändern, speichern und sehen, dass die Vorschau im Browser automatisch aktualisiert wird.


Inhaltsverzeichnis mit markdown-toc
-----------------------------------

Webseite: https://github.com/jonschlinkert/markdown-toc

TOC = Table of Contents = Inhaltsverzeichnis

### - Installieren

EINMALIG: npm-Paket-Manager installieren und initialisieren

```
$ sudo zypper install npm
$ cd ~
$ npm init
```

EINMALIG: markdown-toc installieren (siehe https://github.com/jonschlinkert/markdown-toc#install)

```
$ npm install --save markdown-toc
```

Die Pakete werden nach ~/node_modules/ installiert. Die ausführbare Datei markdown-toc nach ~/node_modules/.bin. Damit man markdown-toc von überall in der Shell verwenden kann, muss ~/node_modules/.bin zur PATH-Umgebungsvariable hinzugefügt werden. Frage nach, wie das geht.

Test, ob Installation erfolgreich (zeigt Hilfetext):
```
$ markdown-toc
```

### - Verwenden

<!- - toc --> (ohne das Leerzeichen zwischen den ersten beiden Bindestrichen; das ist hier nötig, damit das Programm nicht hier das TOC einfügt) an die Stelle in der Markdown-Datei (z. B. README.md) schreiben, wo das Inhaltsverzeichnis eingefügt werden soll.

Dann folgendes zum Test aufrufen (gibt das Inhaltsverzeichnis auf die Konsole aus):

```
markdown-toc README.md
```

Bei der ersten Verwendung des folgenden Befehls sicherstellen, dass man die Datei gesichert hat, falls etwas schiefgeht:

```
markdown-toc -i README.md
```


Weitere Tools
-------------
### - MUP
https://github.com/agateau/mup - MUP: a Markup Previewer

Installieren:

```
$ mkdir ~/ag/mup
$ cd ~/ag/mup
$ git clone https://github.com/agateau/mup.git`
$ sudo zypper in python3-pyxdg python3-PyYAML
$ python3 setup.py install --user
```

Verwenden:

```
$ mup README.md
```

Oben rechts "python-markdown" auswählen.

Zeigt eine Vorschau der README.md.

(Stürzt manchmal ohne weitere Fehlermeldung ab)

### - Markmywords
    $ sudo zypper install markmywords
    $ mark-my-words

einfaches GTK-Programm: https://github.com/voldyman/MarkMyWords

### - CuteMarkEd
* http://cloose.github.io/CuteMarkEd
* in Qt geschrieben
* Stand 2017-03-29: nicht für openSUSE-Tumbleweed paketiert.

### - siehe auch
* Exkurs für Entwickler: Siehe "Readme-driven development" http://tom.preston-werner.com/2010/08/23/readme-driven-development.html
