// boost

// sudo zypper in boost-devel
// #include <boost/algorithm/string.hpp> -> to_lower, trim
// https://www.boost.org/doc/libs/1_73_0/doc/html/boost/algorithm/to_lower.html

#include <iostream>
#include <boost/algorithm/string.hpp> // header only, kein Linken notwendig

using namespace std;

int main()
{
    string a = "Hallo";
    boost::to_lower(a);
    cout << a << endl;

    a = "       Hallo      ";
    cout << "|" << boost::trim_copy(a) << "|\n";

    return 0;
}
