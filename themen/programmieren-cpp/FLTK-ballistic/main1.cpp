#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/fl_draw.H>

class MyWindow : public Fl_Double_Window
{
public:
    MyWindow(int w, int h, const char* title)
        : Fl_Double_Window(w, h, title)
    {

    }

    void draw() override
    {
        fl_line_style(FL_SOLID); // Reset Stift

        // schwarzes Rechteck als Hintergrund
        fl_rectf(0, 0, w(), h(), FL_BLACK);

        // kleines magenta Rechteck
        fl_rectf(10, 10, 20, 20, fl_rgb_color(255, 0, 255));

        // hellblaues Tortenstück
        fl_color(100, 100, 255);
        fl_pie(50, 20, 50, 30, 0, 300);

        // grauer ausgefüllter Kreis
        fl_color(100, 100, 100);
        fl_pie(200, 20, 50, 50, 0, 360);

        // türkiser Kreis
        fl_color(0, 255, 255);
        fl_circle(100, 100, 50);

        // 10 Kreise
        for (double i = 0; i < 1.0; i += 0.1)
        {
            fl_color(0, 255 * i, 255);
            fl_circle(100 + i * 30, 200, 50 * i);
        }

        // Eine weiße, gestrichelte Linie
        fl_color(255, 255, 255);
        fl_line_style(FL_DASH, 3);
        fl_line(0, 300, w(), 200);
    }

protected:

private:

};

int main(int argc, char **argv) {
    MyWindow* window = new MyWindow(340, 480, "Hallo");
    // ggf. füllen mit Elementen wie Buttons und Textfelder
    window->end();
    window->show(argc, argv);
    return Fl::run();
}
