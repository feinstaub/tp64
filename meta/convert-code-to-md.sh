#/bin/bash

#
# 2019: not used anymore
#
# Wenn in einem Verzeichnis eine Datei .do_convert_code_to_md vorhanden ist,
# dann wird nach Dateien, die nicht mit .md enden oder mit . anfangen (und weiteren Ausnahmen), gesucht.
# Durchsucht keine .-Verzeichnisse
# Jede dieser Datei wird in eine md-Datei umgewandelt und in .codeout/ gespeichert
#

function convert_code_to_md {
    F=$1                  # ./meta2/mycode.py
    F_BASE=$(basename "$F") # mycode.py
    F_DIR=$(dirname "$F")   # ./meta2
    OUT_DIR=$F_DIR/.codeout
    OUT_FILE=$OUT_DIR/$F_BASE.md

    if [ -f $F_DIR/.do_convert_code_to_md ]; then
        echo -n "convert_code_to_md: $F ... "
        mkdir -p $OUT_DIR
        # http://stackoverflow.com/questions/13586349/how-can-i-prepend-a-string-to-the-beginning-of-each-line-in-a-file

        #                                                     this also prefixes empty lines which will be removed again by the toc command
        # echo "$F_BASE" > $OUT_FILE && echo >> $OUT_FILE && awk '{print "    " $0}' $F >> $OUT_FILE

        #                                                       do no prefix empty lines
        echo "$F_BASE" > $OUT_FILE && echo >> $OUT_FILE && sed 's/\(.\+\)/    \1/g' $F >> $OUT_FILE

        echo "$F_BASE DONE"
    else
        # echo "$F_BASE SKIPPED"
        test
    fi
}

export -f convert_code_to_md

# https://unix.stackexchange.com/questions/158042/problem-combining-or-and-exec-with-find-command
# https://stackoverflow.com/questions/4321456/find-exec-a-shell-function
# https://askubuntu.com/questions/266179/how-to-exclude-ignore-hidden-files-and-directories-in-a-wildcard-embedded-find
find -not -path '*/\.*' -type f \( -not -iname '*.md' -and -not -iname '.*' -and -not -iname '*.out' -and -not -iname '*.gif' -and -not -iname '*.png' -and -not -iname '*.jpg' \) -exec bash -c 'convert_code_to_md "$0"' {} \;

####### DEBUG / TEST ##########
# convert_code_to_md ./meta/convert-script-test.py
