Dolphin
=======

<!-- toc -->

- [Einfach- oder Doppelklick](#einfach--oder-doppelklick)
  * [Markieren oder Öffnen](#markieren-oder-offnen)
- [Konsole mit F4](#konsole-mit-f4)

<!-- tocstop -->

Der Standard-Datei-Manager für den Plasma-Deskop.

Einfach- oder Doppelklick
-------------------------
Grundsätzlich öffnen sich Elemente mit einem Einfachklick. In den Optionen kann man aber auch den Doppelklick aktivieren.

### Markieren oder Öffnen
Wenn man mit der Maus über Verzeichnis-Elemente fährt und genau hinschaut, sieht man links oder links oben einen runden Kreis mit einem Plus- oder Minus-Zeichen.

* Plus bedeutet: ein Klick darauf fügt das Element zur Auswahl hinzu.
* Minus bedeutet: ein Klick darauf entfernt das Element aus der Auswahl.

Wenn man nicht auf Plus oder Minus klickt, dann wird das Element sofort mit dem ihm zugeordneten Programm geöffnet.

Konsole mit F4
--------------
Die Taste F4 öffnet eine Konsole, die mit dem aktuellen Verzeichnis initialisiert wird
