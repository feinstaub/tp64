#!/usr/bin/python3

g = 100

a = 1
while a <= g:
    b = 1
    while b <= g:
        print("{} x {} = {}".format(a, b, a * b))
        b += 1
    a += 1
