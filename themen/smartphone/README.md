Smartphone
==========

Allgemein
---------
* siehe workshop-smartphone

Unsortiert
----------
### Freie Mobilbetriebssysteme
* Android-basiert
    * https://de.wikipedia.org/wiki/LineageOS (GNU/Linux -> Android -> LineageOS)
    * [Replicant](https://de.wikipedia.org/wiki/Replicant_(Betriebssystem))
    * [F-Droid-App-Store](https://de.wikipedia.org/wiki/F-Droid)
    * siehe workshop-smartphone
* Plasma Mobile
    * siehe workshop-smartphone

### Geschichtliches
* 2016
    * Bis 2016 war CyanogenMod eine Google-Freie Android-Variante. Diese wird durch LineageOS unter anderem Namen fortgeführt. Siehe auch Heise-Artikel
    * [Plasma Mobile schwenkt auf LineageOS als Basis](http://blog.bshah.in/2016/12/28/cyanogenmod-lineage-os-and-plasma-mobile/), 2016
* 2013
    * [Artikel über Google und Android](https://arstechnica.com/gadgets/2013/10/googles-iron-grip-on-android-controlling-open-source-by-any-means-necessary), inkl. zeitlicher Verlauf der Verbreitung verschiedener Mobil-Betriebssysteme

### Messenger
* [Grenzen der Ende-zu-Ende-Verschlüsselung](https://www.kuketz-blog.de/grenzen-der-ende-zu-ende-verschluesselung/), kuketz-blog.de, 2016, siehe auch Benutzer-Kommentare, warum freie Software im Sicherheitsbereich wichtig ist
* siehe workshop-smartphone
