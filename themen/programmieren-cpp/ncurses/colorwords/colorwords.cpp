// colorwords
// colorwords --demo1
// colorwords --demo2
// colorwords --demo3
// colorwords --demo4
//
// Demo 1:
// - Minimales Programm
// - mvaddstr()
//
// Demo 2:
// - Mit einer Schleife von 0 bis 25 Buchstaben ausgeben
// - char-Array mit einem Element. Character addieren.
//
// Demo 3:
// - Doppelschleife
//
// Demo 4:
// - Einfärben
// - start_color()          - https://de.wikibooks.org/wiki/Ncurses:_Farben_und_andere_Attribute
// - init_pair()
// - color_set()
// - mvprintw()
// - Teilen mit Rest, Modulo, %
//
// Demo 5:
// - sleep mit usleep()     - man usleep
// - Animation: Buchstabenzeilen rechtsbündig ausrichten
//   (recht komplizierte Schleifen)
//
// Demo 6:
// - Weihnachtsbäume zufällig übereinanderlegen
// - std::rand()
// - curs_set(0)
//
// Referenz:
// ITEM: main               - https://en.cppreference.com/w/cpp/language/main_function
// ITEM: printf

#include <curses.h>
#include <string>
#include <unistd.h> // usleep
//#include <stdlib.h>
// #include <cstdio>
// #include <vector>
// #include <iostream>
// #include <fstream>

/**
 * Calls initscr on construction
 * and endwin() on destruction
 *
 * Alternative zu atexit(quit);
 */
class myncurses_init
{
public:
    myncurses_init()
    {
        initscr();
    }

    ~myncurses_init()
    {
        endwin();
    }
};

void demo1()
{
    myncurses_init guard;
    clear(); // geht auch ohne
    mvaddstr(0, 0, "Programm beenden durch Drücken einer Taste...");
    refresh(); // geht auch ohne
    getch();
}

void demo2()
{
    myncurses_init guard;
    start_color();
    clear();

    for (int i = 0; i < 26; i++) {
        char c[1];
        c[0] = 'A' + i;
        mvaddstr(i, 0, c);
    }

    refresh();
    getch();
}

void demo3()
{
    myncurses_init guard;
    clear();

    for (int i = 0; i < 26; i++) {
        for (int k = 0; k <= i; k++) {
            char c[1];
            c[0] = 'A' + i;
            mvaddstr(i, k, c);
        }
    }

    refresh();
    getch();
}

void demo4()
{
    myncurses_init guard;
    start_color();
    clear();

    //init_pair(0, COLOR_RED,     COLOR_BLACK); // weiß
    init_pair(1, COLOR_RED,     COLOR_BLACK);
    init_pair(2, COLOR_GREEN,   COLOR_BLACK);
    init_pair(3, COLOR_YELLOW,  COLOR_BLACK);
    init_pair(4, COLOR_BLUE,    COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(6, COLOR_CYAN,    COLOR_BLACK);
    init_pair(7, COLOR_WHITE,   COLOR_BLACK); // grau

    color_set(7, 0);
    mvprintw(0, 10, "%d", COLOR_PAIRS);

    for (int i = 0; i < 26; i++) {
        color_set(i % 8, 0);
        for (int k = 0; k <= i; k++) {
            char c[1];
            c[0] = 'A' + i;
            mvaddstr(i, k, c);
        }
    }

    refresh();
    getch();
}

void demo5()
{
    myncurses_init guard;
    start_color();

    //init_pair(0, COLOR_RED,     COLOR_BLACK); // weiß
    init_pair(1, COLOR_RED,     COLOR_BLACK);
    init_pair(2, COLOR_GREEN,   COLOR_BLACK);
    init_pair(3, COLOR_YELLOW,  COLOR_BLACK);
    init_pair(4, COLOR_BLUE,    COLOR_BLACK);
    init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(6, COLOR_CYAN,    COLOR_BLACK);
    init_pair(7, COLOR_WHITE,   COLOR_BLACK); // grau

    color_set(7, 0);
    mvprintw(0, 10, "%d", COLOR_PAIRS);

    for (int a = 0; a < 25; a++) { // anim zeilenweise von A bis Y (0..24)
        for (int b = 0; b < (26 - a); b++) { // jede Zeile wird weniger nach rechts verschoben
                                             // a     b
                                             // 0   0..24
                                             // 1   0..23
                                             // 2   0..22
                                             // 12  0..12
                                             // 13  0..11
                                             // 15  0..9
                                             // 20  0..4
                                             // 21  0..3
                                             // 22  0..2
                                             // 23  0..1
            clear();

            for (int i = 0; i < 26; i++) { // von A bis Z
                color_set(i % 8, 0);
                for (int k = 0; k <= i; k++) {
                    char c[1];
                    c[0] = 'A' + i;
                    int x = k; // left aligned
                    if (a == i) { // anim
                        x += b;
                    } else if (a > i) { // fix right aligned
                        x += 25 - i;
                    }
                    mvaddstr(i, x, c);
                }
            }

            refresh();
            usleep(10 * 1000);
        }
    }

    refresh();
    getch();
}

#include "demo6.h"

int main(int argc, char *argv[]) {
    if (argc < 2) {
        demo1();
        return 0;
    }

    std::string opt { argv[1] };
    if (opt == "--demo1") {
        demo1();
    }
    else if (opt == "--demo2") {
        demo2();
    } else if (opt == "--demo3") {
        demo3();
    } else if (opt == "--demo4") {
        demo4();
    } else if (opt == "--demo5") {
        demo5();
    } else if (opt == "--demo6") {
        demo6();
    } else {
        printf("Unbekannte Option\n");
    }
    return 0;
}
