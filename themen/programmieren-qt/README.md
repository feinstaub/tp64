Qt-Framework
============

<!-- toc -->

- [Einstieg mit Python](#einstieg-mit-python)
- [Programme, die mit Qt/C++ geschrieben sind](#programme-die-mit-qtc-geschrieben-sind)
  * [KSnip](#ksnip)
  * [Nanonote](#nanonote)
  * [Die meisten KDE-Programme](#die-meisten-kde-programme)
  * [QGIS](#qgis)
- [Programme, die mit PyQt geschrieben sind](#programme-die-mit-pyqt-geschrieben-sind)
  * [git-cola](#git-cola)
  * [MUP](#mup)
  * [ANGRYsearch](#angrysearch)
- [Qt lernen](#qt-lernen)
- [Werkzeuge](#werkzeuge)
  * [GammaRay](#gammaray)
- [Verschiedenes](#verschiedenes)
  * [Building Qt apps with Travis CI and Docker](#building-qt-apps-with-travis-ci-and-docker)
  * [More Efficient String Construction](#more-efficient-string-construction)
  * [QR-Code scannen](#qr-code-scannen)
  * [Model/View Tutorial](#modelview-tutorial)
  * [Qbs](#qbs)
  * [C++ and Qt](#c-and-qt)
  * [Coroutines](#coroutines)
  * [Build Qt](#build-qt)

<!-- tocstop -->

Einstieg mit Python
-------------------
[Python-Qt-Einstieg](../../anleitungen/python-qt)

Programme, die mit Qt/C++ geschrieben sind
------------------------------------------
### KSnip
* https://github.com/DamirPorobic/ksnip
* Schöne Code-Organisation

### Nanonote
* https://github.com/agateau/nanonote
* Schönes CMake

### Die meisten KDE-Programme
* ...
* Plasma Desktop
* ...

### QGIS
* https://qgis.org/
    * programmierbar mit Python

Programme, die mit PyQt geschrieben sind
----------------------------------------
### git-cola
* [git-cola](https://github.com/git-cola)

### MUP
* [MUP: a Markup Previewer](https://github.com/agateau/mup)

### ANGRYsearch
* [ANGRYsearch](https://github.com/DoTheEvo/ANGRYsearch)

Qt lernen
---------
* siehe elsewhere here: todo
* https://wiki.qt.io/Qt_for_Beginners, 2018
* Die Beispiele aus dem Qt-Creator (vs. CMake/C++)
* https://wiki.qt.io/Basic_Qt_Programming_Tutorial, 2016

* Frage: "programming linux desktop app with high level language?"
    * https://www.fossmint.com/best-programming-languages-for-developing-linux-desktop-applications/
        * C/C++
        * Java
        * Python
        * JavaScript/GitHub Electron
        * Shell
    * https://stackoverflow.com/questions/4563888/high-level-language-for-developing-applications-on-linux
        * ...
        * Smalltalk: Squeak and Pharo
        * ...
    * https://www.quora.com/What-is-the-best-programming-language-to-develop-a-desktop-application-It-should-be-cross-platform-free-easy-to-learn-and-have-a-good-community
        * ...

* Wiki-Buch (Qt 4 mit C++): https://de.wikibooks.org/wiki/Qt_f%C3%BCr_C%2B%2B-Anf%C3%A4nger

Werkzeuge
---------
### GammaRay
* insbesondere [Widget Inspector](https://doc.qt.io/GammaRay/gammaray-widget-inspector.html)

Verschiedenes
-------------
### Building Qt apps with Travis CI and Docker
* http://agateau.com/2019/building-qt-apps-with-travis-and-docker/
    * TODO

### More Efficient String Construction
* http://doc.qt.io/qt-5/qstring.html#more-efficient-string-construction

### QR-Code scannen
* 2017: https://www.kdab.com/efficient-barcode-scanning-qzxing/

### Model/View Tutorial
* 2018: https://doc.qt.io/qt-5/modelview.html
    * https://www.kdab.com/new-in-qt-5-11-improvements-to-the-model-view-apis-part-1/

### Qbs
* 2018: http://doc.qt.io/qbs/overview.html

### C++ and Qt
* 2018: https://www.angrycane.com.br/en/2018/06/21/the-day-kate-gregory-enjoyed-qt/

### Coroutines
* 2018: https://blog.qt.io/blog/2018/05/29/playing-coroutines-qt/

### Build Qt
* https://doc.qt.io/qt-5/build-sources.html
