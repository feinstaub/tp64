Python how2think
================

Deutsch auf python4kids.net (Py 2)
----------------------------------
* Python2-basiert (Fragen, ob Python3-Anpassung geplant ist; Repo?)
    * offline: "python4kids.net-todo"

### Aufgabe: Primzahlen
* Was sind Primzahlen? -> Schöne Geschichte erzählen.
    * z. B. ähnlich wie Fermats letzter Satz
* Arbeite how2think von Beginn an durch, bis du ein Programm schreiben kannst, das Primzahlen ausrechnen kann. :-)
* todo: Eingabe via Programm-Parameter
    * dann Benchmark-Input: 1 2 3 4 5 6 7 100 111 201 1001 10001 100001 1000001 1000001 1000001 10000001 100000001 100000001 1001 100000001
    * Output:               n j j n j n j n   ?

HowToThink-github / "Programming for Life Scientists"
----------------------------------------------------
* Python 3
* Inhaltsverzeichnis: https://howtothink.readthedocs.io/en/latest/
* PDF-Download (> 300 S.): https://media.readthedocs.org/pdf/howtothink/latest/howtothink.pdf
* Contribute / Source: https://github.com/HowToThink/HowToThink - Last update 2019
    * forked from https://code.launchpad.net/~thinkcspy-rle-team/thinkcspy/thinkcspy3-rle

* Ausgewählte Inhalte
    * **Dateien lesen und schreiben:** https://howtothink.readthedocs.io/en/latest/PvL_07.html
    * **Rekursion mit Koch-Kurve:** https://howtothink.readthedocs.io/en/latest/PvL_C.html

Learning with Python 3 (RLE) 2012
---------------------------------
* "The Rhodes Local Edition (RLE) targets Python 3, Windows and PyScripter"
* Start here: http://openbookproject.net/thinkcs/python/english3e/
* Ausgewählte Inhalte
    * Dateien lesen und schreiben: http://openbookproject.net/thinkcs/python/english3e/files.html
        * **Achtung: noch alter Python 2-Stil**
    * Rekursion mit Koch-Kurve: http://openbookproject.net/thinkcs/python/english3e/recursion.html

* Source code: https://code.launchpad.net/~thinkcspy-rle-team/thinkcspy/thinkcspy3-rle
    * Last update: October 2012 ?

Meta
----
"GNU Free Documentation License, Version 1.3" (https://howtothink.readthedocs.io/en/latest/copyright.html)
