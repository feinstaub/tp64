import QtQuick 2.0
import QtMultimedia 5.0

Item {
    id: item1

     Rectangle {
         id: r1
         x: 25
         y: 26
         width: 100
         height: 100
         color: "#aefb9f"

         MediaPlayer {
             id: mediaplayer
             autoPlay: true
             loops: MediaPlayer.Infinite
             source: "/usr/share/texmf/doc/latex/media9/files/cube.mp4"
             volume: 0             
         }

         VideoOutput {
             id: vo1
             anchors.fill: parent
             fillMode: VideoOutput.PreserveAspectFit
             source: mediaplayer
         }
     }

     Rectangle {
         id: r2
         x: 167
         y: 26
         width: 200
         height: 200
         color: "#f47070"

         MediaPlayer {
             id: mediaplayer2
             autoPlay: true
             loops: MediaPlayer.Infinite
             source: "/usr/share/texmf/doc/latex/media9/files/random.mp4"
             volume: 0
         }

         VideoOutput {
             id: vo2
             anchors.fill: parent
             fillMode: VideoOutput.PreserveAspectFit
             source: mediaplayer2
         }
     }

    Rectangle {
        x: 53
        y: 232
        width: 570
        height: 228
        color: "#aaa7ff"
        AnimatedImage { id: animation; source: "open-suse-welcome.gif" }
    }
}
