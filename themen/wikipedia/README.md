Wikipedia
=========

* "Wikipedia ist ein gemeinschaftliches Projekt mit dem Ziel, eine Enzyklopädie von bestmöglicher Qualität zu schaffen." (https://de.wikipedia.org/wiki/Wikipedia:Grundprinzipien)

* Mache dich mit den Grundprinzipien vertraut: https://de.wikipedia.org/wiki/Wikipedia:Grundprinzipien

* Experimentiere mit der Technik der Wikipedia auf der **Spielwiese**: https://de.wikipedia.org/wiki/Wikipedia:Spielwiese

* TODO: Arbeitsblatt erstellen

Unsortiert
----------
### Bücher
* Buchempfehlung: siehe https://www.quora.com/What-are-the-best-books-about-Wikipedia

### Die Offenen Naturführer
* http://offene-naturfuehrer.de/web/
