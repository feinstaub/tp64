Blender - 3D-Modellierung und -Animation
========================================

<!-- toc -->

- [Einstieg](#einstieg)
  * [Blender installieren und starten](#blender-installieren-und-starten)
  * [Anleitungen für Einsteiger in deutscher Sprache](#anleitungen-fur-einsteiger-in-deutscher-sprache)
  * [FAQ für das GUI](#faq-fur-das-gui)
  * [Kamera](#kamera)
  * [Material](#material)
  * [Animation](#animation)
  * [Weiteres](#weiteres)
- [Ergänzendes Wissen](#erganzendes-wissen)
- [Projekt-Idee: Anleitungen für Anfänger schreiben - Aller Anfang ist leicht](#projekt-idee-anleitungen-fur-anfanger-schreiben---aller-anfang-ist-leicht)
  * [Einzelthemen-Anleitungen](#einzelthemen-anleitungen)

<!-- tocstop -->

Einstieg
--------
### Blender installieren und starten
* Das Paket heißt _blender_. Dieses Paket installieren. Version Stand 2020: 2.82a
* Nächster Schritt: Anleitung für Anfänger

### Anleitungen für Einsteiger in deutscher Sprache
* Blender 2.8, 2019
    * "Blender 2.8 Anfänger Tutorial deutsch - Teil 01 Voreinstellungen: https://www.youtube.com/watch?v=zBiY15jCqaI, 3 min
    * "Teil 02 - Oberfläche (Interface)", 12 min
    * "Teil 03 - 3D View Navigation", 13 min
    * "Teil 04 - Objekte und Farben", 9 min
    * "Teil 05 | Animation Basics", 6 min
    * etc.
    * ggf. die Playback-Speed bei Youtube auf 1.25 oder 1.5 stellen.
* [https://www.youtube.com/watch?v=_nwBhh_UuIM|Blender 3D Einsteiger Training - B01 - Modeling Grundlagen (Tutorial Deutsch)] (2015) von http://blenderhilfe.de/
* [https://www.youtube.com/watch?v=pDS5zkqT0Ls|Blender Tutorial #1 - Grundlagen und Modelling] (2014)
* http://blender-tutorial.de/
* https://de.wikibooks.org/wiki/Blender_Dokumentation (wikibooks)
* http://www.blenderbuch.de/
* Blender-Buch
    * z. B. [Blender 2.7 - Das umfassende Handbuch - von Thomas Beck](https://www.rheinwerk-verlag.de/blender-27_3404/)

### FAQ für das GUI
* http://blender.stackexchange.com/questions/5631/how-to-close-view-windows

### Kamera
* http://wiki.blender.org/index.php/Doc:2.4/Manual/3D_interaction/Navigating/Camera_View
    * NumPad
* http://wiki.blender.org/index.php/Doc:2.4/Manual/3D_interaction/Navigating#Fly_mode
    * FlyMode
* Lock to View
* https://www.youtube.com/watch?v=hn-aokImhaU (10 min, DE)

### Material
* https://www.youtube.com/watch?v=7FobUOuISak (35 min, englisch)
    * BrushTextures
    * Material
    * Sculpting Mode
    * Seamless images
    * https://search.creativecommons.org/
    * weitere Tipps
* https://www.youtube.com/watch?v=HDWYvy57TOs (10 min, DE)
    * Licht und Schatten

### Animation
* https://www.youtube.com/watch?v=qn3LnECsSj0 (15 min, DE)
    * Modifier

Mehr Infos:
* https://www.blender.org/
* [http://blenderhilfe.de/?p=1380|OpenSource, OpenContent, CreativeCommons erklärt von blenderhilfe.de]

### Weiteres
* [Mini-Howto](mini-howto-1.md) - Unvollständiges Blenderwissen und Tipps
* Licht: todo

Ergänzendes Wissen
------------------
* [Schriftarten ansehen und neue installieren](../schriftarten-beginner-opensuse)

Projekt-Idee: Anleitungen für Anfänger schreiben - Aller Anfang ist leicht
--------------------------------------------------------------------------
* Ziel: hier eine Anleitung ergänzen, die einen einfachen Einstieg in Blender ermöglicht, für Leute, die noch nie etwas mit Blender gemacht haben.
* Projekt "Blender-Anleitungen für Anfänger erstellen"
    * Wie schreibt man eine Anleitung?
        * in Markdown: [Lerne Markdown](../markdown-primer)
            * Warum Markdown und keine LibreOffice-Writer-Dokument? -> Weil man auch Animationen (gif-Bilder) einbinden kann
            * Alternative: HTML-Seiten erstellen (auch gut, dann lernt man HTML siehe [HTML-Schnelleinstieg](../../themen/html))
        * [Lerne wie man Screenshots und Screencasts macht](../screenshots-und-videos)
        * todo: die eigentliche Anleitung schreiben
        * Anleitung an **Testperson** weitergeben
            * z. B. mit [BeeBEEP](../beebeep-chat) oder das HTML direkt mit [Droopy](../dateitausch-mit-droopy) freigeben und konstruktive Rückmeldung einarbeiten.
    * Idee für den Inhalt:
        1. Grundobjekte erstellen (Kugel, Quader, Pyramide, Zylinder)
        1. auf eine Grundplatte stellen
        1. Löcher aus der Grundplatte schneiden, wo die Figuren reinpassen
        1. Löcher etwas größer machen
        1. Kanten verrunden
        1. Animation: die vier Grundobjekte fliegen in die passenden Stellen
        1. verschiedene Materialien anwenden
        1. Lichtquellen anbringen
* Tipps für gute Anleitungen
    * Erst selber die Anleitung erstellen
        * z. B. in HTML
            * (lokal mit grip kann das dann z. B. so verlinkt werden: [vom Home-Verzeichnis](file:///~/public_html/aaa/index.html) oder [absolut](file:///usr/share/gimp/2.0/help/en/script-fu-waves-anim.html))
    * Dann prüfen, ob folgender Aufbau vorhanden ist und Anleitung ggf. anpassen
        * **Ziel**: z. B. "einen 3-D-Text in den leeren Raum stellen"
        * **Ausgangssituation**:
            * z. B. "Blender mit Standard-Szene starten"
            * oder "Starte mit dieser Blender-Datei"
        * In **Einzelschritte** gliedern mit Screenshots
        * ggf. separat **Begründungen**, warum etwas so und nicht anders gemacht werden soll
        * (todo: Beispiel-Vorlage erstellen)
    * Anleitung überarbeiten
    * Testkandidat vorhanden? -> Anleitung durchspielen lassen

### Einzelthemen-Anleitungen
Anhand von bereits vorgegebenen Blender-Dateien, verschiedene Einzelthemen erklären.

* Ideen für Grundlagen-Themen:
    * Objekte mit **Material** versehen
    * Eine **Lichtquelle** hinzufügen
    * Eine Szene **rendern**
    * Eine **Animation** erstellen

* Ideen für Spezial-Themen:
    * Ein Material oder 3D-Objekt von einer Blender-Datei in eine andere übertragen
    * Das richtige Ausgabe-Format für Animationen wählen, sichern und vorführen

* Ideen für Modelle und Szenen:
    * Ein Tisch (mit Beinen und Tischplatte) und ein paar einfachen Objekten drauf
        * eine Lampe oben aufhängen
        * die Lampe schwingen lassen
        * mit Kamerafahrt durch die Szene
