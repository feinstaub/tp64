Csound-Einstieg
===============

<!-- toc -->

- [CsoundQt runterladen, bauen und starten](#csoundqt-runterladen-bauen-und-starten)
  * [Csound 6](#csound-6)
  * [CsoundQt](#csoundqt)
- [CsoundQt: Erster Start](#csoundqt-erster-start)
  * [Einfache Aufgabe](#einfache-aufgabe)
- [Frühere Experimente](#fruhere-experimente)
  * [Beispiele aus Csound Getting Started](#beispiele-aus-csound-getting-started)
  * [Experimente / Beispiel aus Wikipedia](#experimente--beispiel-aus-wikipedia)
  * [TODO next](#todo-next)
  * [Weiteres](#weiteres)

<!-- tocstop -->

CsoundQt runterladen, bauen und starten
---------------------------------------
### Csound 6
Csound 6 selber bauen, weil openSUSE Tumbleweed bisher nur csound5 hat.

    (((CsoundQt: # Projekt mit csound5 konfigurieren funktioniert nicht
    $ qmake-qt5 "CONFIG+=csound5" qcs.pro
    # Project MESSAGE: Building for Csound 5 (unsupported).
    # Bauen schlägt dann fehl
    )))

* https://github.com/csound/csound/releases
    * Source code (zip) --> **csound-6.10.0.zip (42 MB)**
    * Entpacken

    $ mkdir cs6make
    $ cd cs6make
    $ cmake ..
    $ make -j4
    $ sudo make install
    $ sudo ldconfig

```
# "ldconfig  creates  the necessary links and cache to the most recent shared libraries found in the directories specified on the command line, in the file /etc/ld.so.conf, "
```

OK.

### CsoundQt
* https://csoundqt.github.io/pages/about.html
    * https://csoundqt.github.io/pages/download.html
        * CsoundQt runterladen und bauen: https://csoundqt.github.io/pages/building.html
            * https://github.com/CsoundQt/CsoundQt/blob/master/BUILDING.md

```
# csound5 vorsichtshalber deinstallieren:
$ sudo zypper remove csound csound-devel

$ cd ~/ag/dev

$ git clone https://github.com/CsoundQt/CsoundQt.git
# 220 MB download

$ cd CsoundQt

# latest stable release:
$ git checkout master

# Projekt konfigurieren
$ qmake-qt5 qcs.pro

# Bauen
$ make -j4
```

```
# (Installieren)
($ sudo make install)
```

CsoundQt: Erster Start
----------------------
    $ ./bin/CsoundQt-d-cs6

Examples -> Getting Started -> Basics -> Hello World

Run. Falls kein Ton kommt, dann Configure -> RT Audio Module: von pulse auf alsa stellen und dann wieder auf pulse.

Siehe Slider_Widget-2-Instr-parallel.csd: **alsa** scheint besser zu sein, wenn man die Slider bewegt.

### Einfache Aufgabe
* ...
* Füge einen Ton hinzu (todo)
* ...
* eine Melodie, siehe themen/musik/README.md

Frühere Experimente
-------------------
### Beispiele aus Csound Getting Started
* http://csound.github.io/get-started.html
* Offizielle Referenz: http://csound.github.io/docs/manual/index.html
    * Beispiel für vco2: http://csound.github.io/docs/manual/vco2.html

* TODO: weiter mit dem Getting-Started

### Experimente / Beispiel aus Wikipedia
* Code siehe https://en.wikipedia.org/wiki/Csound

Abändern:

```
i1 0.0 1.0 20000 2000        ; Play one second of one kHz at amplitude 20000.
i1 0.2 1.0 20000 1000
i1 0.3 1.0 20000 3000
i1 0.5 0.5 20000 1000
```

siehe experimente/

Abspielen:

    $ csound helloworld.csd ; play test.wav    # einfacher Ton
    $ csound test2.csd                         # Generierte eine test.wav mit über 3 GB!!!
    $ csound test3.csd ; play tone.wav         # Tonkombination
    $ csound test4.csd                         # invalid
    $ csound vco2.csd ; play vco2.wav          # Verschiedene auf- und absteigende Töne

### TODO next
* [CsoundQt 0 9 2 1 demo](https://www.youtube.com/watch?v=xmUiAxs1EwI), 3 min
    * Wie bekommt man das Virtuelle Keyboard ans Laufen?
        * http://www.csounds.com/manual/html/MidiTop.html
* Wie kann man eine Midi-Note in eine Frequenz konvertierten?
    * ? http://www.csounds.com/manual/html/cpsmidinn.html
    * https://music.stackexchange.com/questions/6899/inputtting-note-names-into-csound
        * Csbeats? http://www.csounds.com/manual/html/CsBeats.html
* Weiter mit [Csound FLOSS Manual](http://floss.booktype.pro/csound/preface/)
    * [Arbeit an einer Übersetzung](csound-floss-manual-de.md)

### Weiteres
* Hilfeseiten
    * Offizielles Getting started
        * http://csound.github.io/get-started.html
    * "Learning Csound is a lot like learning Chinese. Both are languages that have over a thousand different characters, each one of which is capable of being used in different contexts, with a resultant different effect."
        * http://csoundjournal.com/issue10/TheTwoWorldsCsound.html
    * FLOSS manuals: http://write.flossmanuals.net/csound/_draft/_v/1.1/preface/
        * veraltet? oder zu neu?
* weitere IDEs
    * http://csound.github.io/frontends.html
    * Cabbage runterladen und bauen: http://cabbageaudio.com/download/
    * http://blue.kunstmusik.com/
