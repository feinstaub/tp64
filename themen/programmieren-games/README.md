Spiele programmieren
====================

<!-- toc -->

- [Spiel-Ideen](#spiel-ideen)
  * [Pong](#pong)
  * [Leaper (Frogger)](#leaper-frogger)
  * [Snake](#snake)
  * [Tetris](#tetris)
  * [Antix](#antix)
  * [Breakout](#breakout)
- [2D Platformer](#2d-platformer)
  * [The Python Arcade Library](#the-python-arcade-library)
  * [Tiled Map Editor (Qt)](#tiled-map-editor-qt)
  * [SDL/C](#sdlc)
- [To sort](#to-sort)
  * [Python Games](#python-games)
  * [Qt Based Games](#qt-based-games)
  * [Java Games](#java-games)
  * [Pygame](#pygame)
  * [Program Arcade Games With Python And Pygame](#program-arcade-games-with-python-and-pygame)
  * [Python Game Engines](#python-game-engines)
  * [LÖVE](#love)

<!-- tocstop -->

Spiel-Ideen
-----------
z. B.  Retrospiele

### Pong
* siehe python-qt/aufgabe-pong.md

### Leaper (Frogger)
*  mit Qt, mit Graphiken, Sound

### Snake
* C/C++ and ncurses: snacurses/README.md, inklusive weiterer Beispiele
* http://zetcode.com/gui/qt5/snake/
* https://doc.qt.io/archives/qt-4.8/qt-demos-declarative-snake-example.html

### Tetris
* ...

### Antix
* siehe kbounce
* mit ncurses? mit qt? mit sdl?

### Breakout
* siehe pyqt-breakout-spiel

2D Platformer
-------------
### The Python Arcade Library
Start: http://arcade.academy/

Build Your Own 2D Platformer Game: http://arcade.academy/examples/platform_tutorial/index.html

    "In this tutorial, use Python 3.6+ and the Arcade library to create your own 2D platformer. Learn to work with Sprites and the Tiled Map Editor to create your own games. Add coins, ramps, moving platforms, enemies, and more.

    The tutorial is divided into these parts:

        Step 1 - Install and Open a Window
        Step 2 - Add Sprites
        Step 3 - Add User Control
        Step 4 - Add Gravity
        Step 5 - Add Scrolling
        Step 6 - Add Coins And Sound
        Step 7 - Display The Score
        Step 8 - Use a Map Editor
        Step 9 - Multiple Levels and Other Layers"

    "Download this bundle with code, images, and sounds."

Alternative Anleitung: https://opensource.com/article/18/4/easy-2d-game-creation-python-and-arcade

Beispiel-Spiele: http://arcade.academy/sample_games.html

### Tiled Map Editor (Qt)
https://www.mapeditor.org

Nutzung wird auf http://arcade.academy/examples/platform_tutorial/index.html erklärt

### SDL/C
https://www.parallelrealities.co.uk/tutorials/#ppp

To sort
-------
### Python Games
    * Übersicht: https://wiki.python.org/moin/PythonGames
    * pyglet: https://bitbucket.org/pyglet/pyglet/wiki/Home
    ** nicht ausprobiert, welche Vorteile hat es?

### Qt Based Games
https://wiki.qt.io/Qt_Based_Games

### Java Games
    === zetcode.com: Java 2D games tutorial ===

    http://zetcode.com/tutorials/javagamestutorial/

    TOC: Basics, Animation, Moving sprites, Collision detection, Puzzle, Snake, Breakout, Tetris, Pacman, Space Invaders, Minesweeper, Sokoban

### Pygame
https://www.pygame.org/wiki/GettingStarted (weniger brauchbar)

### Program Arcade Games With Python And Pygame
    http://programarcadegames.com/index.php

    <source>
    Before getting started...
    1: Create a Custom Calculator
    2: What is a Computer Language?
    3: Quiz Games and If Statements
    4: Guessing Games with Random Numbers and Loops
    5: Introduction to Graphics
    6: Back to Looping
    7: Introduction to Lists
    8: Introduction to Animation
    9: Functions
    10: Controllers and Graphics
    11: Bitmapped Graphics and Sound
    12: Introduction to Classes
    13: Introduction to Sprites
    14: Libraries and Modules
    15: Searching
    16: Array-Backed Grids
    17: Sorting
    18: Exceptions
    19: Recursion
    20: Formatting
    Labs
    </source>

    Viele '''Beispiele''':
    * http://programarcadegames.com/index.php?chapter=example_code&lang=en
    * z. B.: http://programarcadegames.com/python_examples/show_file.php?file=moving_sprites.py
    ** Beispiel-Code in eine Datei `moving_sprites.py` kopieren.
    ** Vor import pygame noch <source>
    import pygame_sdl2
    pygame_sdl2.import_as_pygame()
    </source> einfügen
    ** <tt>$ python3 moving_sprites.py</tt>
    ** Ein kleines Spiel zum Fangen von herunterfallenden Rechtecken erscheint.
    ** Hinweis zu kdevelop: Für pygame gibt es keine Autocompletion. 2018: immer noch?
    ** Die verwendete sprite-Klasse ist hier dokumentiert: http://www.pygame.org/docs/ref/sprite.html#pygame.sprite.Group
    * Komplexerer "Platformer"-Beispiele:
    ** http://programarcadegames.com/index.php?chapter=example_code&lang=en
    *** u. a. Sprite-Sheets
    ** Longer Game Examples: http://programarcadegames.com/index.php?&chapter=example_code_longer_examples
    *** u. a. ein Frogger-Video (aka Leaper)


    '''Quiz-Fragen''': http://programarcadegames.com/quiz/progress.php

    '''Arbeitsblätter''': http://programarcadegames.com/index.php?chapter=sample_worksheets

    '''Übungen''':
    * http://programarcadegames.com/index.php?chapter=labs
    * z. B. http://programarcadegames.com/index.php?chapter=lab_create_a_picture

### Python Game Engines
https://codeboje.de/2d-and-3d-game-and-rendering-engines-python/

### LÖVE
* https://love2d.org/
* make 2D games in Lua
