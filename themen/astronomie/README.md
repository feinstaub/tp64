Astronomie
==========

<!-- toc -->

- [Marble Virtual Globe](#marble-virtual-globe)
- [Unsortiert](#unsortiert)
  * [KStars](#kstars)
  * [Stellarium](#stellarium)
  * [Videos](#videos)

<!-- tocstop -->

Marble Virtual Globe
--------------------
* Marble Virtual Globe (Political Map, Moon, Historical map, Create movie, Sternbilder) (-> https://software.opensuse.org/package/marble[Installieren])
* View
    * Stars - Sternbilder anzeigen
    * Sun Control... - Sonnenschatten anzeigen
    * Eclipses - Sonnenfinsternisse
* View -> Online Services
    * Earthquakes/Erdbeben
    * Weather/Wetter
* Strecken messen (z. B. auf dem Mond (Karte wechseln) von den Apollo-Missionen) und mit Deutschland vergleichen
* Programmieren:
    * MarbleMaps-App mit Kirigami
        * http://bernkastelsgsoc.blogspot.de/2017/06/moving-forward-dialing-phones-and.html

Unsortiert
----------
### KStars
* KStars
    * https://edu.kde.org/kstars/

### Stellarium
* Stellarium - Virtueller Sternenhimmel (-> https://software.opensuse.org/package/stellarium?search_term=stellarium[Installieren])

### Videos
* Animationen mit Stellarium und Celestia erstellt: https://www.youtube.com/watch?v=qj7I_dqilcI&index=16&list=PL9BE806CD0702F112[AstroViews, 2012, 16 Folgen]
