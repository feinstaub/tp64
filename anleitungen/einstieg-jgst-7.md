Einstieg - Jgst. 7 und 8
========================

Start
-----
* [GNU/Linux auf USB-Stick installieren](../themen/linux-installieren/opensuse-auf-usb-stick) oder Live-DVD verwenden
    * Nachträgliche Korrekturen:
        * Sprache umstellen: siehe linux-verwenden/README-sprache-umstellen.md
        * automatische Anmeldung deaktivieren: todo
    * Warum eigentlich GNU/Linux?
        * siehe z. B. [dieser engl. Artikel](https://www.tecmint.com/install-linux-today/): 1. frei (auf zwei Arten), 2. alte Hardware zum Leben erwecken, 3. lernen wie ein Computer funktioniert, 4. gut zum Programmieren lernen, 5. eine Menge erstklassiger Software vorhanden
* Installierte Programme erkunden
    * z. B. LibreOffice Writer, Calc, Mozilla Firefox, Dophin-Dateimanager
    * siehe auch [Empfehlenswerte_Programme](../themen/programme)
    * [Weitere Programme entdecken - für zuhause](../themen)
* Neue Software installieren:
    * [Software_installieren](http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Arbeitsbl%C3%A4tter/Software_installieren)
* Nebenbei:
    * [Internet-Tipps](http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Eigenes_System/Internet-Tipps)

Programmieren
-------------
* Programmiereinstieg:
    * [Python-Einstieg mit Turtle](python-turtle/python-turtle-on-opentechschool.md)
    * [Python-Grundlagen-Kurs auf CS-Circles](python-on-cscircles.md)
    * [JS Hero](javascript-jshero.md)
* Programmiereinstieg mit Python:
    * [Interaktiv](http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Programmier-Einstieg/Python_interaktiv)
    * Programmieren mit dem Kate-Editor:
        * [Python-Code_in_Dateien](http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Programmier-Einstieg/Python-Code_in_Dateien)
        * [Kate-Tipps](http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/FAQ/Kate-Tipps) (bis Abschnitt 1.3)
* Programmieren Weg 1: Rechnen mit Python
    * [Rechnen_und_Schleifen](http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Programmier-Einstieg/Rechnen_und_Schleifen)
* Programmieren Weg 2: Graphische Turtle mit Python
    * [Python-Einstieg mit Turtle](python-turtle/python-turtle-on-opentechschool.md)
    * [Fraktale](../themen/graphik-fraktale/)
* Programmieren Weg 3: Primzahlen ausrechnen:
    * [Erstes_Ziel_-_Primzahlen_ausrechnen](http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Arbeitsbl%C3%A4tter/Erstes_Ziel_-_Primzahlen_ausrechnen)

Linux / Unix
------------
* Vorhandene **Software selber kompilieren** und ändern
    * Moon-Lander anpassen: siehe c-cpp-moonlander, C/C++
* Unix-Werkzeuge => ShellTux

Weitere Themen
--------------
* [Software und Anleitungen vom Englischen ins Deutsche übersetzen](../anleitungen/translate-to-german)
* [3D-Modellierung mit Blender](../anleitungen/blender-mini-howto)
* [Über das Netzwerk Dateien austauschen](dateitausch-mit-droopy)
* Plasma anpassen:
    * ...
    * todo: Dolphin service menus
* Komplexere Themen:
    * todo: Multimedia
    * todo: Spreed für Video-Chat
