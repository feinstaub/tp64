#!/bin/bash

# COPIED from workshop-smartphone
# more details there

# Dependencies
# ------------
# - markdown-pdf
#    $ cd ~
#    $ npm install --save phantomjs
#    $ npm install --save markdown-pdf
#    https://github.com/alanshaw/markdown-pdf/issues/30 -- Links are not created as links

# 2017: does not preserve hyperlinks
# pandoc praesenz/README.md -o _out/lokale-datensicherung-start-$VERSION.pdf

# 2017: does not preserve hyperlinks
# pandoc praesenz/README.md --reference-links -o _out/lokale-datensicherung-start-$VERSION.pdf

VERSION=1.0

mkdir -p _out

# https://forum.antergos.com/topic/5239/phantomjs-complains-about-missing-qt-platform-plugin-xcb/3
#
export QT_QPA_PLATFORM=''

# $ hyperlinks still do not work but at least the url is visible
markdown-pdf -o _out/lokale-datensicherung-start-$VERSION.pdf praesenz/README.md


# Altmaterial (todo: move to here)
cd ~/dataDocuments/FS-Angebote-weitere/fsworkshop.net
./build.sh
cd -
cp ~/dataDocuments/FS-Angebote-weitere/fsworkshop.net/_out/datensicherung-0.9.4.pdf _out/
