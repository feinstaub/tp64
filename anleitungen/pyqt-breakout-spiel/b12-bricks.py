#!/usr/bin/python3

from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QPen, QBrush, QColor, QResizeEvent, QFont, QFontDatabase
from PyQt5.QtCore import Qt, QTimer, QRect
from PyQt5.QtMultimedia import QSound
import sys, random

class Ball:
    mx = 100     # Mittelpunkt x
    my = 100     # Mittelpunkt y
    r = 10     # Radius
    x = -1     # x links
    y = -1     # y oben
    x2 = -1    # x rechts
    y2 = -1    # y unten
    vx = 15
    vy = 10

    def set_mx(self, mx):
        self.mx = mx
        self.x = self.mx - self.r
        self.y = self.my - self.r
        self.x2 = self.mx + self.r
        self.y2 = self.my + self.r

    def set_my(self, my):
        self.my = my
        self.x = self.mx - self.r
        self.y = self.my - self.r
        self.x2 = self.mx + self.r
        self.y2 = self.my + self.r

    def next_step(self):
        self.set_mx(self.mx + self.vx)
        self.set_my(self.my + self.vy)

class Stein:
    x = 0
    y = 0
    w = 50 # 100 # 50
    h = 35 # 70  # 35
    farbe = QColor(100, 100, 100)
    getroffen = False

class Spielmodus:
    Start = 0
    BallFliegt = 1
    BallVerloren = 2
    AlleSteineWeg = 3

class Fenster(QWidget):
    modus = Spielmodus.Start

    # Feld
    fx = 2
    fy = 50
    fh = -1
    fw = -1
    fx2 = -1  # x rechts
    fy2 = -1  # y unten

    # Racket/Schläger
    rx = 20
    ry = 0
    rw = 100
    rh = 20

    # Ball
    b = Ball()

    steine = []

    sound1 = None
    sound_off1 = None

    punkte = 0

    def __init__(self):
        super().__init__()
        self.setGeometry(300, 300, 600, 400)
        self.setWindowTitle("Breakout")
        self.setMouseTracking(True)
        self.setFocusPolicy(Qt.ClickFocus)

        # Timer starten:
        timer = QTimer(self)
        timer.setSingleShot(False)
        timer.timeout.connect(self.mainTimer) # siehe auch http://zetcode.com/gui/pyqt5/eventssignals/
        timer.start(50) # 50 Millisekunden; 25 ms => ohne Ruckelspur beim Ball

        self.sound1 = QSound("jump1.wav", self)
        self.sound_off1 = QSound("off1.wav", self)

        self.show()

        self.initSteine()

    def initSteine(self):
        for x in range(0, int(self.fw / Stein.w)):
            stein = self.neuerStein(x * Stein.w + 30, 150, QColor(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)))
            self.steine.append(stein)

    def resizeEvent(self, e):
        w = e.size().width()
        h = e.size().height()
        self.fw = w - self.fx - 1
        self.fh = h - self.fy - 1
        self.fx2 = self.fx + self.fw
        self.fy2 = self.fy + self.fh
        self.ry = h - self.rh - 10
        self.update()

    def mouseMoveEvent(self, e):
        x = e.x()
        y = e.y()
        self.rx = x - self.rw / 2

        self.halteRacketImFeld()

        if not self.modus == Spielmodus.BallFliegt:
            self.halteBallAmRacket()

        self.update()

    def mousePressEvent(self, e):
        if self.modus == Spielmodus.BallFliegt:
            stein = self.neuerStein(e.x(), e.y(), QColor(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)))
            self.steine.append(stein)
        elif self.modus == Spielmodus.AlleSteineWeg:
            self.initSteine()
            self.modus = Spielmodus.BallFliegt
        else:
            self.modus = Spielmodus.BallFliegt

        self.update()

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Left:
            self.rx = self.rx - 25
        elif e.key() == Qt.Key_Right:
            self.rx = self.rx + 25

        self.halteRacketImFeld()

        if not self.modus == Spielmodus.BallFliegt:
            self.halteBallAmRacket()

        self.update()

    def halteBallAmRacket(self):
        self.b.set_mx(self.rx + self.rw / 2)
        self.b.set_my(self.ry - self.b.r)

    def halteRacketImFeld(self):
        if self.rx < self.fx:
            self.rx = self.fx

        if self.rx + self.rw > self.fx2:
            self.rx = self.fx2 - self.rw

    def neuerStein(self, x, y, farbe):
        stein = Stein()
        stein.x = x
        stein.y = y
        stein.farbe = farbe
        return stein

    def ballAmRacketBehandlung(self):
        # rechts
        if self.b.x2 >= self.fx2:
            self.b.set_mx(self.fx2 - self.b.r)
            self.b.vx = -self.b.vx
            #self.sound1.play()
        # unten
        if self.b.y2 >= self.fy2:
            self.b.set_my(self.fy2 - self.b.r)
            self.b.vy = -self.b.vy
            self.sound_off1.play()
            self.modus = Spielmodus.BallVerloren
            self.punkte = self.punkte - 1
        # links
        if self.b.x <= self.fx:
            self.b.set_mx(self.fx + self.b.r)
            self.b.vx = -self.b.vx
            #self.sound1.play()
        # oben
        if self.b.y <= self.fy:
            self.b.set_my(self.fy + self.b.r)
            self.b.vy = -self.b.vy
            #self.sound1.play()
        # Racket
        if self.b.y2 >= self.ry and self.b.x > self.rx and self.b.x2 < self.rx + self.rw:
            self.b.set_my(self.ry - self.b.r)
            self.b.vy = -self.b.vy
            self.sound1.play()

    def ballAnSteinenBehandlung(self):
        for stein in self.steine:
            if stein.getroffen:
                continue
            rect = QRect(stein.x, stein.y, stein.w, stein.h)
            if self.isInRectX(rect):
                stein.getroffen = True
                self.punkte = self.punkte + 1
                self.b.vx = -self.b.vx
                self.sound1.play()
            elif self.isInRectY(rect):
                stein.getroffen = True
                self.punkte = self.punkte + 1
                self.b.vy = -self.b.vy
                self.sound1.play()

    def isInRectX(self, rect):
        if self.b.y2 >= rect.y() and self.b.y <= rect.y() + rect.height():
            if self.b.vx > 0 and self.b.x2 >= rect.x() and self.b.x2 - self.b.vx <= rect.x():
                return True
            if self.b.vx < 0 and self.b.x <= rect.x() + rect.width() and self.b.x - self.b.vx >= rect.x() + rect.width():
                return True
        return False

    def isInRectY(self, rect):
        if self.b.x2 >= rect.x() and self.b.x <= rect.x() + rect.width():
            if self.b.vy > 0 and self.b.y2 >= rect.y() and self.b.y2 - self.b.vy <= rect.y():
                return True
            if self.b.vy < 0 and self.b.y <= rect.y() + rect.height() and self.b.y - self.b.vy >= rect.y() + rect.height():
                return True
        return False

    def mainTimer(self):
        if self.modus == Spielmodus.BallFliegt:
            self.b.next_step()
            self.ballAmRacketBehandlung()
            self.ballAnSteinenBehandlung()

            alleWeg = True
            for stein in self.steine:
                if not stein.getroffen:
                    alleWeg = False
                    break
            if alleWeg:
                self.modus = Spielmodus.AlleSteineWeg
        else:
            self.halteBallAmRacket()

        self.update()

    def drawTopText(self, qp, x, text):
        qp.setPen(QColor(0, 0, 255))
        qp.setFont(QFont('Open Sans Light', 14))
        qp.drawText(x, 20, text)

    def drawBackground(self, qp):
        qp.setPen(QPen(QBrush(QColor(255, 0, 0)), 2.0))
        qp.setBrush(QBrush(QColor(0, 0, 0)))
        qp.drawRect(self.fx, self.fy, self.fw, self.fh)

    def drawSteine(self, qp):
        qp.setPen(QColor(0, 0, 0))
        for stein in self.steine:
            if not stein.getroffen:
                qp.setBrush(stein.farbe)
                qp.drawRect(stein.x, stein.y, stein.w, stein.h)

    def drawRacket(self, qp):
        qp.setPen(QColor(255, 0, 0))
        qp.setBrush(QBrush(QColor(255, 0, 0)))
        qp.drawRect(self.rx, self.ry, self.rw, self.rh)

    def drawBall(self, qp):
        qp.setPen(QPen(QColor(0, 0, 0), 2.0))
        qp.setBrush(QBrush(QColor(255, 0, 255)))
        qp.drawEllipse(self.b.x, self.b.y, self.b.r * 2, self.b.r * 2)

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawTopText(qp, 0, "Ball: " + str(self.b.mx) + "," + str(self.b.my))
        self.drawTopText(qp, 150, "Punktestand: " + str(self.punkte))
        self.drawBackground(qp)
        self.drawSteine(qp)
        self.drawRacket(qp)
        self.drawBall(qp)

        if self.modus == Spielmodus.Start:
            self.drawTopText(qp, 300, "Start: Klicke zum Starten")
        elif self.modus == Spielmodus.BallFliegt:
            self.drawTopText(qp, 300, "Go...")
        elif self.modus == Spielmodus.BallVerloren:
            self.drawTopText(qp, 300, ":-( Klicke zum Starten")
        elif self.modus == Spielmodus.AlleSteineWeg:
            self.drawTopText(qp, 300, "GEWONNEN")
        else:
            self.drawTopText(qp, 300, "???")

        qp.end()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWidget = Fenster()
    sys.exit(app.exec_())
