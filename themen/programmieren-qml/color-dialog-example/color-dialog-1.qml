import QtQuick 2.2
import QtQuick.Dialogs 1.0

// http://doc.qt.io/qt-5/qml-qtquick-dialogs-colordialog.html

ColorDialog {
    id: colorDialog
    title: "Please choose a color"
    onAccepted: {
        console.log("You chose: " + colorDialog.color)
        Qt.quit()
    }
    onRejected: {
        console.log("Canceled")
        Qt.quit()
    }
    Component.onCompleted: visible = true
}

