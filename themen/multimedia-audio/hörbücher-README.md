Hörbuch / Hörbücher
===================

Audio Book Player zeichnen sich insbesondere dadurch aus, dass sich die letzte Position gemerkt wird.

<!-- toc -->

- [Welche Software gibt es?](#welche-software-gibt-es)
  * [DeaDBeeF](#deadbeef)
  * [Cozy](#cozy)
  * [gmusicbrowser](#gmusicbrowser)

<!-- tocstop -->

Welche Software gibt es?
------------------------

### DeaDBeeF

http://deadbeef.sourceforge.net/

    $ sudo zypper in deadbeef deadbeef-plugin-statusnotifier

https://softwarerecs.stackexchange.com/questions/4057/audiobook-player-for-linux

    * "with View -> Preferences -> Playback -> Resume previous session on startup DeaDBeeF remembers the exact track and postion"
    " it's completely based on playlists (no library) and files/folders can be added by drag'n drop, menu items, CLI or plugins like the file browser"


### Cozy

https://github.com/geigi/cozy

[für openSUSE Tumbleweed](https://software.opensuse.org//download.html?project=home%3Ageigi&package=com.github.geigi.cozy)


### gmusicbrowser

http://gmusicbrowser.org/

    $ sudo zypper in gmusicbrowser

https://softwarerecs.stackexchange.com/questions/4057/audiobook-player-for-linux

    * "In settings, check "Remember playing position between sessions""
    * "You can add a music file at a time, even though it is designed for collection folders."
    * "it has extensive hotkey capabilities"
        * Seek 1 second forward / backward kann man auf Cursor-Tasten legen
