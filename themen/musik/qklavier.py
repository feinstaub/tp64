#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtCore import QRect
from PyQt5.QtWidgets import QMainWindow, QTextEdit, QAction, QApplication, QWidget
from PyQt5.QtGui import QIcon, QPainter, QPen, QBrush, QColor

class Taste:
    def __init__(self, okt, i, col):

        # Breite einer weißen Taste in Pixel
        self.kw = 40

        # Höhe der weißen Taste
        self.kh = 150

        # Rand von oben / margin y
        self.my = 10

        self.okt = okt
        self.key = i
        self.color = col

        x = 0
        y = 0
        w = 0
        h = 0
        if self.color == "white":
            x = self.kw * 7 * okt + self.kw * i
            y = self.my
            w = self.kw
            h = self.kh
        else:
            w = self.kw * 0.66
            y = self.my
            x = self.kw * 7 * okt + self.kw * (i + 1) - w / 2
            h = self.kh * 0.66

        self.rect = QRect(x, y, w, h)

        self.hover = False

    def paint(self, p):
        if self.color == "white":
            self.paintWhiteKey(p, self.okt, self.key)
        else:
            self.paintBlackKey(p, self.okt, self.key)

    def paintWhiteKey(self, p, okt, i):
        s = 0
        if self.hover:
            s = -30
        p.setBrush(QBrush(QColor(255 + s, 255 + s, 255 + s)))
        p.drawRect(self.rect)

    def paintBlackKey(self, p, okt, i):
        s = 0
        if self.hover:
            s = 100
        p.setBrush(QBrush(QColor(0 + s, 0 + s, 0 + s)))
        p.drawRect(self.rect)

    def mouseOver(self, x, y):
        return self.rect.contains(x, y)


class KlavierWidget(QWidget):
    def __init__(self):
        super().__init__()

        self.keys = []
        self.addOktave(0)
        self.addOktave(1)

        self.setMouseTracking(True) # damit mouseMoveEvent funktioniert

    def paintEvent(self, e):
        p = QPainter()
        p.begin(self)
        for key in self.keys:
            key.paint(p)
        p.end()

    def addOktave(self, okt):
        """
        Erst alle weißen, dann alle schwarzen
        Vorwärts durchgehen fürs Zeichnen.
        Rückwärts durchgehen für Mouse-Hit-Test.
        """
        self.addKey(self.keys, okt, 0, "white")
        self.addKey(self.keys, okt, 1, "white")
        self.addKey(self.keys, okt, 2, "white")
        self.addKey(self.keys, okt, 3, "white")
        self.addKey(self.keys, okt, 4, "white")
        self.addKey(self.keys, okt, 5, "white")
        self.addKey(self.keys, okt, 6, "white")

        self.addKey(self.keys, okt, 0, "black")
        self.addKey(self.keys, okt, 1, "black")
        self.addKey(self.keys, okt, 3, "black")
        self.addKey(self.keys, okt, 4, "black")
        self.addKey(self.keys, okt, 5, "black")

    def addKey(self, keys, okt, i, col):
        keys.append(Taste(okt, i, col))

    def mouseMoveEvent(self, e):
        mx = e.x()
        my = e.y()

        for i in range(0, len(self.keys)):
            self.keys[i].hover = False

        for i in reversed(range(0, len(self.keys))):
            if self.keys[i].mouseOver(mx, my):
                self.keys[i].hover = True
                break

        self.update()


class Example(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        paintWidget = KlavierWidget()
        self.setCentralWidget(paintWidget)

        exitAct = QAction(QIcon.fromTheme('application-exit'), 'Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(self.close)

        self.statusBar()

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAct)

        toolbar = self.addToolBar('main')
        toolbar.addAction(exitAct)

        self.setGeometry(300, 300, 650, 350)
        self.setWindowTitle('Main window')
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
