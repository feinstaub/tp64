#
# taken from http://www.pygame.org/docs/tut/intro/intro.html and modified
#
import sys

# see http://pygame-sdl2.readthedocs.io/en/latest/
import pygame_sdl2
pygame_sdl2.import_as_pygame()

import pygame

pygame.init()

size = width, height = 620, 440
speed = [20, 10]
black = 0, 0, 0
BLUE  = (0, 0, 255)

screen = pygame.display.set_mode(size)

#ball = pygame.image.load("ball.gif") # needs the image file from the tutorial
# we create our own ball:
ball = pygame.Surface((100, 100)) # http://www.pygame.org/docs/ref/surface.html
pygame.draw.circle(ball, BLUE, (50, 50), 50) # http://www.pygame.org/docs/ref/draw.html
ballrect = ball.get_rect()
clock = pygame.time.Clock()

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

    ballrect = ballrect.move(speed[0], speed[1])
    if ballrect.left < 0 or ballrect.right > width:
        speed[0] = -speed[0]
    if ballrect.top < 0 or ballrect.bottom > height:
        speed[1] = -speed[1]

    screen.fill(black)
    screen.blit(ball, ballrect)
    pygame.display.flip()
    # http://www.pygame.org/docs/ref/time.html
    # By calling Clock.tick(30) once per frame, the program will never run at more than 30 frames per second.
    # leider trotzdem tearing, siehe http://stackoverflow.com/questions/1082562/how-to-avoid-tearing-with-pygame-on-linux-x11
    # (wird mit Wayland kein Problem mehr sein)
    clock.tick(30)
