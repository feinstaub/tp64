Bootbaren USB-Stick erzeugen
============================

Einleitung
----------
Ziel: openSUSE Tumbleweed auf einen Rechner ohne DVD-Laufwerk installieren.

Besonderheit: da Rechner der kein DVD-Laufwerk hat, verwenden wir einen USB-Stick als Installationsmedium.

Notwendiges Material
--------------------
* Für diese Anleitung: 1 USB-Stick mit mindestens 5 GB freiem Speicher
* Für spätere Schritte: 1 weiteren USB-Stick mit mindestens 32 GB Speicher

Schritt für Schritt
-------------------
### Installationsmedium herunterladen

https://software.opensuse.org/distributions/tumbleweed

**x86_64** (= 64 Bit)

    DVD-Image (4.7GB)
    Für DVD und USB Stick

    Enthält eine große Anzahl an Software für Desktop- oder Server-Benutzung. Zur Installation oder Aktualisierung geeignet.
    Direkter Link

http://download.opensuse.org/tumbleweed/iso/openSUSE-Tumbleweed-DVD-x86_64-Current.iso

(siehe auch https://en.opensuse.org/openSUSE:Tumbleweed_installation)

### Checksumme prüfen

* SHA256 herunterladen
* Datei openSUSE-Tumbleweed-DVD-x86_64-Snapshot20171028-Media.iso.sha256 mit Kate öffnen
* Den ersten Wert in die Zwischenablage kopieren, z. B. 5614843193128274386f41ebb445df12bba1df12dba0cc696d00a4bbea7990da
* Rechte Maustaste auf openSUSE-Tumbleweed-DVD-x86_64-Snapshot20171028-Media.iso -> Eigenschaften -> Checksums -> Paste -> ca. 1 Minute warten

### openSUSE imagewriter installieren

    $ sudo zypper install imagewriter

* Imagewriter starten
* iso-Datei draufziehen
* USB-Stick auswählen. ACHTUNG: wird gelöscht!
* **Write**-Knopf drücken.
* Einige Minuten warten.
* Imagewriter schließen.

Der USB-Stick sollte nun bereit sein, um von ihm eine openSUSE-Installation zu starten.
Entweder direkt auf die Festplatte eines Rechners, oder einen USB-Stick.

TODO: Link auf openSUSE-Installation

Quellen
-------
* https://software.opensuse.org/newsite/tumbleweed/
    * "How to create a bootable USB stick on Linux." --> https://en.opensuse.org/SDB:Live_USB_stick

* Letztes Update: November 2017
