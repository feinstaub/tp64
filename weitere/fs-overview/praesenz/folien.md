Freie Software: Ein Überblick
=============================

## 2017

---

## Teilnehmer-Erwartungen

* entfällt

---

##  Überblick von mehr-freiheit.org / München
"Freie Computerprogramme, die Deine Rechte als Nutzer/-in respektieren und schützen."

* Sortiert nach Kategorien.
* Lauffähig auf Windows, Mac, GNU/Linux

Internet und WWW:

* **Mozilla Firefox** - Webbrowser
* **Mozilla Thunderbird** - E-Mail-Programm
    * dadurch auch schrittweiser Wechsel des Providers möglich, falls nötig
* RSSOwl - Feedreader

(https://mehr-freiheit.org/pdf/web.pdf)

---

## Einschub: Was ist »Freie Software«?
* siehe https://mehr-freiheit.org/pdf/web.pdf
* "Freie Computerprogramme, die Deine Rechte als Nutzer/-in respektieren und schützen."
* vier wesentliche Freiheiten
* Lizenz
* Quellcode
* Finanzierung
* Ethische Grundlage

---

## Programme: Büro- und Textverarbeitung
* **LibreOffice** - Office-Paket
    * auch PDF-Bearbeitung möglich (z. B. Textblöcke ändern)
* Calibre - E-Book-Verwaltung
* XMind - Mind-Mapping
    * FreeMind, [siehe auch](https://wiki.ubuntuusers.de/Mind_Map/)
* PDFsam - PDF-Bearbeitung
* Task Coach - Task Coach
* TIPP10 - Zehnfingersystem lernen

(https://mehr-freiheit.org/pdf/office.pdf)

---

## Online-Video-Konferenz
* Jitsi Meet
* https://demo.bigbluebutton.org

---

## Einschub: Öffentliche Infrastruktur / LiMux
* https://www.mehr-freiheit.org/limux

"Öffentliche Infrastruktur muss unabhängig von kommerziellen Einzelinteressen, die bekanntermaßen Innovationen ausbremsen, bleiben. Freie Software bietet die einmalige Möglichkeit in gemeinsame Werte zu investieren und von den Beiträgen aller zu profitieren, während die Kontrolle, was wann eingesetzt wird, erhalten bleibt. Lokale Dienstleister, die in gesundem Wettbewerb miteinander stehen, stärken die lokale Wirtschaft und sichern die optimale Nutzung von Steuergeldern."

* Mitte 2017: Der LiMux-Weg wurde mit der Begründung "zu teuer" wieder verlassen (leider)

* 02.12.2017: "Bund will Windows 10 über Bundesclient sicher nutzen können"
    * https://www.heise.de/ix/meldung/Bund-will-Windows-10-ueber-Bundesclient-sicher-nutzen-koennen-3907088.html
    * "Über den Umfang der geplanten Aufwendungen schweigt sich das Bundesinnenministerium aus"
    * "Zur Absicherung des Bundesclients wurde eine Sicherheitsinfrastruktur entworfen, über deren Einzelheiten das Bundesinnenministerium ebenfalls Stillschweigen bewahren will" (Security by Obscurity)
    * "Generell ist mit der Einführung von Windows 10 das Datenschutzniveau stark gesunken"
        * https://www.heise.de/ix/heft/Datenschleuder-3356982.html, 2016
    * siehe Benutzer-Kommentare
        * das wirkt alles wesentlich teurer als eine gemeinwohl-orientierte Investition in freie Software
* ["Mehr Transparenz bei der Entwicklung eines Bundesclients gefordert"](https://gi.de/meldung/mehr-transparenz-bei-der-entwicklung-eines-bundesclients-gefordert), Gesellschaft für Informatik, 2018

---

## Programme: Musik und Tonbearbeitung
* **Audacity** - Aufnahme, Bearbeitung und Optimierung von Audiodateien (z. B. Reduktion von Störgeräuschen)
* LilyPond - Notendruck auf hohem ästhethischen Niveau
* Performous - Karaoke-Party-Spiel
* [Clementine](https://www.clementine-player.org)

(https://mehr-freiheit.org/pdf/audio.pdf)

---

## Programme: Video und Multimedia
* **VLC** - Mulitmediaplayer viele Audio- und Videoformate
* **MediathekView** - Abspielen und Speichern von Sendungen aus den Online-Mediatheken zahlreicher öffentlichrechtlicher Sender
    * https://mediathekview.de
    * siehe auch http://www.mediathekdirekt.de, rpm -i

(https://mehr-freiheit.org/pdf/video.pdf)

---

## Programme: Lernen und Entdecken
* **Stellarium** - interaktives Himmelszelt mit über 600.000 Sternen
* **Marble** - Virtueller Globus, enthält geographische Karten der Erde und anderer Himmelskörper
* **Kiwix** - Offline Reader; Programm um Wikipedia und andere Seiten offline zu lesen

(https://mehr-freiheit.org/pdf/edu.pdf)

---

## Programme: Backup
* **[FreeFileSync](http://www.freefilesync.org)**

---

## Einschub: Nutzerportraits
* https://freie.software - Das Freie Software-Fotobuch
    * stellt Menschen vor, die gerne Freie Software nutzen (neben Computerfachleuten auch ein Koch, Berater, Direktionsassistentin, Kindergärtnerin, Schulpsychologin, Renterin etc.)

---

## Programme: Graphik und Design
* **GIMP** - Bildbearbeitung, bekannteste freie Alternative zu Photoshop
* **Krita** - Bildbearbeitungs- und Malprogramm
* **Inkscape** - Vektorzeichenprogramm
* **Hugin** - Erstellung von Panoramen aus mehreren Einzelaufnahmen

(https://mehr-freiheit.org/pdf/grafik.pdf)

* * https://degooglisons-internet.org/alternatives?l=en (Framasoft)
    * https://svg-edit.github.io

* https://www.draw.io
    * https://github.com/jgraph/drawio-desktop

* **Pencil** - unter anderem Flussdiagramme erstellen
    * http://pencil.evolus.vn
    * auf openSUSE Li-f-e-DVD, openSUSE-Paket: pencil

* **Sweet Home 3D** - Wohnungsplan und Inneneinrichtung 3D sichtbar machen
    * http://www.sweethome3d.com
    * openSUSE-Paket: SweetHome3D

---

## Programme: Karten / GIS
* QGIS: https://www.heise.de/ix/heft/Freikarten-4054848.html

---

## Programme: Spiele
* **SuperTuxKart** - Arcade-Rennspiel
* **Minetest** - Minecraft-ähnliches Spiel
* **FlightGear** - Flugsimulator

(https://mehr-freiheit.org/pdf/games.pdf)

---

## Einschub: Proprietäre Fotoverwaltung

Ein Beispiel, wie proprietäre Software nachteilig für den Nutzer sein kann, am Beispiel der Fotosoftware Picasa.

* 2016: siehe Artikel "Google verkündet Aus für Picasa" http://www.heise.de/newsticker/meldung/Google-verkuendet-Aus-fuer-Picasa-3101945.html
    * ab sofort soll alles nur noch online sein!
    * "Eine mit Picasa vergleichbare Desktop-Bildverwaltung hat Google ebenfalls nicht im Angebot."
    * Nutzer, die sich auf Picasa verlassen haben, müssen nun umständlich ihre Sammlung migrieren
    * => Alternativen: einfache Dateisystem-Ablage oder freie Software **Digikam**

---

## Freie Software auf dem Smartphone

* https://de.wikipedia.org/wiki/F-Droid
    * siehe "Auswahl verfügbarer Apps"
    * https://f-droid.org/
* https://de.wikipedia.org/wiki/Liste_von_freien_und_Open-Source-Android-Applikationen
* https://de.wikipedia.org/wiki/Replicant_(Betriebssystem)
* https://prism-break.org/de/categories/android/

---

## Erwartungen abgleichen
* entfällt

---

## Ausblick
* https://mehr-freiheit.org - Offline-Flyer
    * https://wiki.fsfe.org/LocalGroups/Muenchen
* Regionales Angebot
    * https://wiki.fsfe.org/LocalGroups/RheinMain
    * Weinheim
    * Heidelberg
    * Mannheim
    * VHS-Kurse zu Linux, Blender, GIMP, freie SW, Privatsphäre, free your phone
* Schrittweiser Umstieg auf Freie Software
    * Auf dem aktuellen Betriebssystem schrittweise mehr Freie-Software-Programme verwenden
        * z. B. jedes Jahr ein neues Programm
    * Dann auf GNU/Linux umsteigen, um auch die Basis des Rechners frei zu machen

---

## Anhang

---

## freiesoftwareog.org
* Flyer mit Beispielen für freie SW-Programme
    * http://www.freiesoftwareog.org/downloads/FSOG_Flyer-Debian.pdf
    * http://www.freiesoftwareog.org/downloads.html - "Präsentationen der monatlichen Treffen"
        * Dezember - "Linux Mint 18.3 - Die Neuerungen"
            * Backup mit Timeshift
            * https://itsfoss.com/backup-restore-linux-timeshift/
            * http://www.teejeetech.in/p/timeshift.html
        * November - "Free your Android - Smartphone mit Freier Software betreiben"

---

## mehr-freiheit.org offline?
* siehe https://wiki.fsfe.org/LocalGroups/Muenchen
    * https://git.fsfe.org/fsfe-muc/FLOSS-Flyers/
        * https://gitea.io
    * https://git.fsfe.org/fsfe-muc/FLOSS-Flyers/src/branch/master/Brainstorming
        * [MakeHuman](http://www.makehuman.org), AGPL, Python

---

## Betriebssysteme
* [openSUSE](https://www.opensuse.org/)
* [Debian](https://www.debian.org/)

---

## Hintergründe - Sicherheit

* Unsortierte, unvollständige Sammlung
    * 2016/2017: Win10-Installation, Standardeinstellungen (händisch abstellbar) verletzen die Privatsphäre, was ist das für eine Vertrauensbasis?
* Freie Software ist ein Baustein um in Zeiten steigender Abhängigkeit von Rechnern aller Art eine Basis des Vertrauens zu schaffen und Eigenkontrolle zu ermöglichen
* Das BSI (Bundesamt für Sicherheit in der Informationstechnik) setzt sich auch [für den Einsatz und die Fortentwicklung von Freier Software ein](https://www.bsi.bund.de/DE/Themen/DigitaleGesellschaft/FreieSoftware/freiesoftware_node.html).

---

## Workflow für Grafikdesign mit freier Software

* [Workflow für Grafikdesign mit freier Software - Teil 1 - Ubucon 2014](https://www.youtube.com/watch?v=lXBMJEEg7Hw), 30 min
    * "Basiswissen zur Vorbereitung für professionelles Arbeiten. ICC-Farbprofile u.v.a.m. Es war der letzte Workshop auf der Ubucon in Katlenburg und wurde von Referent: Matthias Baran gehalten."
    * [Teil 2](https://www.youtube.com/watch?v=p3qSYOpBjBU), 20 min
* [Blender Einsteiger Workshop Teil 1 Dipl. Designer Matthias Baran](https://www.youtube.com/watch?v=ob14_WLsuZA), 2014, 55 min
    * [Teil 2](https://www.youtube.com/watch?v=1rZKBzKaiDQ), 50 min

---

## Quellcode vs. Binary

Beispiel von Software-Quellcode (so sieht es der Entwickler; damit können Menschen arbeiten):

![](img/quellcode-vs-bin-1-quell.png)

So wird die Software ausgeliefert (binär; für Menschen unleserlich):

![](img/quellcode-vs-bin-2-bin.resized.png)

---

## Vom Bug-Report zum Feature

Beispiele:

* ["Wish for context menu: Compare two files"](https://bugs.kde.org/show_bug.cgi?id=388559)
