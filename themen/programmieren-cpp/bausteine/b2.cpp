#include <iostream> // cout, cin
#include <cstdarg>  // va_start

/**
 * forward-Deklarationen und Übersicht
 */
void printf1();                 // Experimente mit printf und Zahlen
void myprintf(const char *fmt...);  // Custom variadic arguments

/**
 * Hauptprogramm bausteine2
 */
int main() {
    //printf1();
    //myprintf("Hallo %d %s", 5, "Zehn");

    return 0;
}

/**
 * SIEHE AUCH: Rechnen mit C für Operatoren: https://de.wikibooks.org/wiki/C-Programmierung:_Grundlagen#Ein_zweites_Beispiel:_Rechnen_in_C
 */
void printf1()
{
    printf("Hallo\n");
    //             d oder i:   The  int  argument  is  converted  to  signed decimal notation
    printf("Hallo %d\n", 5);
    int zahl = 10 * 5;
    printf("Hallo %d\n", zahl);
    int a = 30;
    int b = 11;
    //                 f oder F:   The double argument is rounded and converted to decimal notation
    printf("%d / %d = %f\n", a, b, (double)a / b);
    double m = 30.0;
    double n = 11.0;
    printf("printf: %f / %f = %f\n", m, n, m / n);
    std::cout << "  cout: " << m << " / " << n << " = " << m / n << std::endl;
}

/**
 * Experiment mit variadic arguements
 */
void myprintf(const char *fmt...)
{
    // https://stackoverflow.com/questions/5977326/call-printf-using-va-list
    va_list args;
    va_start(args, fmt);
    ::vprintf(fmt, args); // the v-variant must be used
    va_end(args);
}
