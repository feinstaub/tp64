Multimedia-Codecs installieren
------------------------------

* 'Online-Aktualisierung' suchen und starten (z. B. mit 'Alt+F2')

* Konfiguration -> Repositories aufrufen...

image:res/yast-repos-highlight-add.png[]

* Unten den 'Hinzufügen'-Knopf drücken

* 'Community Repositories' auswählen und Weiter

image:res/yast-repo-add-highlight-community.png[]

* Das 'packman'-Repository auswählen und OK

* Repository-Konfiguration mit OK abschließen

* 'Ansicht -> Repositories' einblenden

image:res/yast-sw-view-hl-repos.png[]

* 'Repositories'-Tab auswählen und links 'packman' auswählen

* 'Switch System packages' anklicken

image:res/yast-switch-sys-pack.png[]

* 'smplayer' und 'ffmpeg' installieren
