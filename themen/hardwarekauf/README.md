Hardwarekauf
============

<!-- toc -->

- [Neuer Computer oder Laptop](#neuer-computer-oder-laptop)
  * [Generell](#generell)
  * [Technik](#technik)
  * [Bezugsquellen, gebraucht](#bezugsquellen-gebraucht)
  * [Bezugsquellen Neugeräte](#bezugsquellen-neugerate)
- [Peripherie-Hardware und Sonstiges](#peripherie-hardware-und-sonstiges)
- [Drucker](#drucker)
- [Scanner](#scanner)

<!-- tocstop -->

Neuer Computer oder Laptop
--------------------------

Diese Empfehlungen beziehen sich auf einen Computer für zuhause; für folgende **Anwendungsgebiete** und unter der Berücksichtigung der Verwendung eines freien Betriebssystems:

* Büro-Anwendungen
* Graphik-Anwendungen
* Musik hören
* Filme schauen
* Programmieren
* Spielen (alle freien Software-Spiele, die es gibt; aber nicht das allerneuste proprietäre aus der Werbung mit hohen Hardwareanforderungen)

### Generell
Folgende **generelle Aspekte** kann man beim Kauf eines solchen Computers berücksichtigen und ggf. gegeneinander **abwägen** kann:

* **Portabilität** - soll der Rechner auch mal mitgenommen werden?

* **Betriebsgeräusche** - Lautstärke eines eventuellen Lüfters?

* **Leistung** - Geschwindigkeit und Speicherplatz (siehe weiter unten)

* **Abwägung Bauweise** - Standrechner oder Notebook?
    * Vorteile Standrechner
        * potentiell billiger bei gleicher Leistung oder leistungsfähiger bei gleichem Preis
        * alle Komponenten austauschbar
    * Nachteile Standrechner
        * Höherer Stromverbrauch
        * potentiell lauter (Lüfter)
    * Vorteile Notebook
        * kleiner, leichter, portabel
        * weniger Stromverbrauch
        * potentiell leiser (Lüfter)
        * dank Akku gegen Stromausfälle gewappnet
        * Bildschirm und Tastatur inklusive
        * Empfehlung: mit **Dockingstation** zum komfortablen Arbeiten mit großem Bildschirm und externer Tastatur und Maus verwenden
        * Mit Dockingstation genauso viele oder mehr Anschlüsse (USB etc.) als Stand-PC
        * zumindest die wichtigsten Komponenten sind austauschbar (Festplatte, Arbeitsspeicher, optisches Laufwerk), die man eventuell später aufrüsten möchte

* **Produktionsbedingungen**
    * Faire Elektronik (Umwelt und Handel) ist ein komplexes Thema. Beispiele von Unternehmen, die versuchen das Thema in den Vordergrund zu rücken:
        * https://www.nager-it.de/ - Faire Computermäuse, inklusive Erklärung der Problematik
        * https://www.fairphone.com/

* **Einkauf** - lokal vor Ort oder Online-Bestellung?
    * Hinweis: wenn auf dem Rechner Windows vorinstalliert ist, hat der Händler Lizenzgebühren an Microsoft bezahlt. Wenn man also vorhat, nur GNU/Linux zu verwenden, dann kann man versuchen nach einem Rechner ohne Windows-Vorinstallation bitten, um etwas Geld zu sparen.

### Technik
(Nichterschöpfende) Auswahl an empfohlenen Mindestanforderungen und Komponenten:

* **Gute CPU** ist wichtig, da man dies später am schwierigsten oder gar nicht aufrüsten kann - im Gegensatz zu Festplatte und Arbeitsspeicher (RAM),
* **Festplatte**: SSD-Technik, mindestens 256 GB, besser 512 GB
* **Arbeitsspeicher**: 8 GB reichen für das Betriebssystem und eine virtuelle Maschine aus. 16 GB sind aber auch nicht verkehrt
* Graphikkarte: auf Linux-Freundlichkeit achten (siehe weiter unten), aber das meiste heutzutage funktioniert gut
* Bei Laptop:
    * Bildschirmart (matt oder glänzend? -> matt ist oft besser)
    * Bildschirmgröße: 15 Zoll ist meist ein guter Kompromiss zwischen Gewicht und komfortablem Arbeiten ohne Dockingstation
* Anschlüsse:
    * min. ein paar USB-3.0-Buchsen
    * SD-Kartenleser (zum Übertragen von Kamera-Bildern oder Bespielen eines RaspberryPi-Speichers)

**GNU/Linux-Freundlichkeit:**

Da einige Hardware-Hersteller nicht auf Linux-Kompatibilität achten, gibt es bei mancher vorhandener Hardware Probleme. Daher ist dies speziell zu berücksichtigen. Im Allgemeinen sind Markengeräte besser als billiger No-Name-Kram.

* [http://www.pcwelt.de/ratgeber/Das_ideale_Linux-Notebook-10019715.html PC-Welt-Artikel 2016 über Linux-Notebooks]

### Bezugsquellen, gebraucht
Überall gibt es gebrauchte Geschäfts-Notebooks aus Leasingrückläufern, siehe z. B.

* https://www.am-computershop.de/gebrauchte-notebooks-laptops.html
* https://www.notebookgalerie.de/

### Bezugsquellen Neugeräte
* Beispiele von Läden, die fertige Rechner verkaufen:
    * Tuxedo Computers: https://www.tuxedocomputers.com
        * Online-Shop, Sitz: 86343 Königsbrunn
        * "TUXEDO legt viel Wert auf Flexibilität, daher sind generell alle Geräte individuell konfigurierbar. Dies setzt sich auch in den Garantiebedingungen fort, welche das Öffnen zu Wartungszwecken und zum Aufrüsten ausdrücklich erlauben."
    * KDE Slimbook - Notebooks: http://kde.slimbook.es
        * Online-Shop, Sitz: Italien
        * [Review: "Introducing my new friend: a Slimbook"](https://toscalix.com/2018/01/18/introducing-my-new-partner-a-slimbook/), 2018
            * [PRO2 Intel i7](https://slimbook.es/en/store/slimbook-pro-13/pro-intel-i7-1-comprar)
            * "[tlp](http://linrunner.de/en/tlp/docs/tlp-linux-advanced-power-management.html) is something you might consider to install and learn how to manage in order to significantly increase the working time with your laptop while on battery"
    * Technoethical: https://tehnoetic.com
        * gebrauchte Hardware mit freier Software, Sitz: Rumänien
    * Purism: https://puri.sm
        * Laptops; Smartphone in Planung
        * es wird besonderen Wert auf Privatsphäre gelegt; Kamera und Mikrofon haben einen Hardwareschalter
        * Sitz: San Francisco, USA

Peripherie-Hardware und Sonstiges
---------------------------------
* https://de.opensuse.org/Hardware
* https://h-node.org/home/index/de (siehe auch http://www.fsf.org/resources/hw)
* http://www.tuxmobil.org/ (Linux With Laptops, Notebooks, PDAs, Mobile Phones, PMPs, eBooks, GPS & Wearables)
* http://www.bretschneidernet.de/linux/hardware.html

Drucker
-------
* Sehr gute Unterstützung bietet HP mit seinen freien Softwaretreibern HPLIP, siehe https://en.wikipedia.org/wiki/HP_Linux_Imaging_and_Printing
* http://www.openprinting.org/printers
* Interessant: "Tracking-Dots" siehe
    * https://www.eff.org/issues/printers
    * https://www.eff.org/pages/list-printers-which-do-or-do-not-display-tracking-dots

Scanner
-------
* Vorbildlich ist hier derzeit ebenfalls HP, siehe Drucker.
