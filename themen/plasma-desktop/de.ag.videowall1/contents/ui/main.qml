import QtQuick 2.0
import QtMultimedia 5.0

Item {
     Rectangle {
         id: r1
         x: 0
         y: 0
         width: parent.width / 2
         height: parent.height
         color: "black"

         MediaPlayer {
             id: mediaplayer
             autoPlay: true
             loops: MediaPlayer.Infinite
             source: "videos/video1.ogv"
             volume: 0
         }

         VideoOutput {
             id: vo1
             fillMode: VideoOutput.PreserveAspectFit
             anchors.fill: parent
             source: mediaplayer
         }
     }

     Rectangle {
         id: r2
         width: parent.width / 2
         height: parent.height / 2
         color: "#000000"
         anchors.left: r1.right

         MediaPlayer {
             id: mediaplayer2
             autoPlay: true
             loops: MediaPlayer.Infinite
             source: "videos/video2.ogv"
             volume: 0.5
         }

         VideoOutput {
             id: vo2
             fillMode: VideoOutput.PreserveAspectFit
             anchors.fill: parent
             source: mediaplayer2
         }
     }

     Rectangle {
         id: r3
         width: parent.width / 2
         height: parent.height / 2
         color: "#000000"
         anchors.left: r1.right
         anchors.top: r2.bottom

         MediaPlayer {
             id: mediaplayer3
             autoPlay: true
             loops: MediaPlayer.Infinite
             source: "videos/video3.ogv"
             volume: 0
             //playbackRate: 0.1
         }

         VideoOutput {
             id: vo3
             fillMode: VideoOutput.PreserveAspectFit
             anchors.fill: parent
             source: mediaplayer3
         }
     }
}
