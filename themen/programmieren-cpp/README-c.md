C lernen
========

<!-- toc -->

- [Einstieg mit C-HowTo](#einstieg-mit-c-howto)
  * [Start](#start)
  * [Infos](#infos)
- [de.wikibooks.org/wiki/C-Programmierung](#dewikibooksorgwikic-programmierung)
- [wikihow.com](#wikihowcom)
- [Programmieraufgaben](#programmieraufgaben)
  * [Allgemein](#allgemein)
  * [C and SDL with 3Guys](#c-and-sdl-with-3guys)
  * [Tunneler anpassen](#tunneler-anpassen)
- [C IDEs](#c-ides)

<!-- tocstop -->

Einstieg mit C-HowTo
--------------------
### Start
* Starte hier: http://www.c-howto.de/tutorial/einfuehrung/warum-c/

### Infos
* ...
* Was ist ein Compiler: http://www.c-howto.de/tutorial/einfuehrung/compiler/
* Compilieren unter Linux: http://www.c-howto.de/tutorial/einfuehrung/compiler/linux/
    * gcc hello.c -o hello
* "Getränke-Automat"-Übung: http://www.c-howto.de/tutorial/verzweigungen/
* ...

de.wikibooks.org/wiki/C-Programmierung
--------------------------------------
Alternativer Einstieg

* Start mit Buch: https://de.wikibooks.org/wiki/C-Programmierung
    * Lizenz: CC-by-SA
    * Infos
        * Schöner Text
        * Aber wenige Übungen am Anfang
        * Teilweise etwas ungeordnet, siehe z. B. https://de.wikibooks.org/wiki/Diskussion:C-Programmierung:_static_%26_Co.#Abschnitt_%22static_und_Co.%22_hinter_%22Funktionen%22_einsortieren

wikihow.com
-----------
Alternativer Einstieg

* Start: https://de.wikihow.com/Mit-C-programmieren-lernen
    * Interessanter Schnelleinstieg

Programmieraufgaben
-------------------
### Allgemein
siehe ../anleitungen/programmieraufgaben.md

### C and SDL with 3Guys
* siehe c-with-3guys/README.md

### Tunneler anpassen
* siehe c-tunneler-anpassen/README.md

C IDEs
------
* siehe C++ IDEs
