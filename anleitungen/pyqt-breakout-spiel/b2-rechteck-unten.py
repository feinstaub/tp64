#!/usr/bin/python3

from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QPen, QBrush, QColor, QResizeEvent
from PyQt5.QtCore import Qt
import sys

class Fenster(QWidget):
    # Klassen-Variablen:
    ry = 30 # y-Koordinate der linken oberen Ecke des Rechtecks
    rh = 70 # Höhe des Rechtecks

    def __init__(self):
        super().__init__()
        self.setGeometry(300, 300, 400, 300)
        self.setWindowTitle("Hallo Rechteck")
        self.show()

    def resizeEvent(self, e):
        w = e.size().width()
        h = e.size().height()
        print("resize: w={0}, h={1}".format(w, h))
        self.ry = h - self.rh
        self.update() # neu zeichnen

    def paintEvent(self, e):
        print("paint...")
        qp = QPainter()
        qp.begin(self)
        qp.setBrush(QBrush(QColor(255, 0, 0)))
        qp.drawRect(20, self.ry, 100, self.rh)
        qp.end()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWidget = Fenster()
    sys.exit(app.exec_())
