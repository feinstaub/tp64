#!/bin/bash

# $1: Frequenz in Hertz
# $2: Dauer in Millisekunden

# NOTE: Dauert merklich länger, bis der Ton startet im Vergleich zu playtone-speakertest.sh

echo "playtone f=$1 d=$2"
python3 playtone-simpleaudio.py $1 $2
