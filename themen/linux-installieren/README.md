GNU/Linux installieren
======================

Installiere um GNU/Linux auf deinen Rechner.

Es gibt mehrere Möglichkeiten:

1. GNU/Linux auf die interne Festplatte deines Rechners installieren

a. entweder parallel zum vorhandenen Betriebssystem (verursacht oftmals Probleme beim Updaten)

b. oder als einziges System (dann wird das vorhandene Betriebssystem gelöscht)

2. GNU/Linux auf einen externen Datenträger (USB-Stick) installieren, so dass dein Rechner dabei nicht verändert wird.

Wir konzentrieren uns auf Möglichkeit 2 (externer Datenträger).

Überblick über die nötigen Schritte
-----------------------------------
1. Es gibt nicht *das* GNU/Linux, sondern verschiedene Zusammenstellungen der Softwarepakete. Das nennt man **Distribution**. Daher ist der erste Schritt, sich eine Distribution auszusuchen. Wir nehmen openSUSE Tumbleweed.

2. Du benötigst ein **Installationsmedium** (CD, DVD oder USB-Stick) mit den Installationsdateien der Distribution.

a. Entweder du hast schon ein fertiges Medium.

b. Oder du lädst dir die Installationsdateien aus dem Internet und kopierst sie selbst auf das Installationsmedium.
Die Dateien passen normalerweise genau auf eine DVD (< 5 GB)

3. Du benötigst einen **Ziel-Datenträger (32 GB)**, auf den openSUSE Tumbleweed installiert werden soll.
Es werden zwar mindenstens 40 GB empfohlen, für den Anfang reichen 32 GB aber aus.

4. Stecke den Ziel-Datenträger in den Rechner. Lege das Installationsmedium in den Rechner.

5. Starte den Rechner so, dass er **vom Installationsmedium bootet**. **Installation starten.**

6. Wenn fertig, Installationsmedium entfernen. Es ist nun nicht mehr nötig.

7. Wenn du openSUSE Linux starten willst, dann **boote den Rechner vom Ziel-Datenträger**.

Beispiel:

```
-------------------------------          ----------------------------------
|    openSUSE-Download-Seite  |--------->|  Installationsmedium           |
|    im Internet              |          |  DVD oder USB-Stick, < 5 GB    |
-------------------------------          ----------------------------------


------------------------------------
|            Dein Rechner          |
| startet vom Installationsmedium  |
|......................      .....................
| Installationsmedium .      .  Zieldatenträger  .
| eingelegt           .      .  32 GB            .
|......................      .....................
|           |                      |     ^
------------------------------------     |
            |                            |
            `----------------------------`
                    Installation


------------------------------------
|            Dein Rechner          |
| startet vom Zieldatenträger      |
|                            .....................
|      Hallo                 .  Zieldatenträger  .
|      openSUSE              .  32 GB            .
|                            .....................
|                                  |
------------------------------------
```

Los geht's: openSUSE auf USB-Stick
----------------------------------
* [Installationsdateien herunterladen und -Medium erzeugen](opensuse-auf-usb-stick/bootable-usb-stick-erzeugen.md)
* [openSUSE auf USB-Stick installieren](opensuse-auf-usb-stick)

2019
----
### Wie funktioniert UEFI?
* UEFI - Introduction to the standard, https://en.opensuse.org/openSUSE:UEFI
    * todo

### Desktop für schwache Hardware
* https://wiki.lxde.org/en/Main_Page
    * von: https://www.youtube.com/watch?v=PSq6-je_0jw - "How-to install openSUSE on old 32bit-Hardware", 2019, 10 min, Tumbleweed

2016
----
### Weiteres
* https://wiki.debian.org/FreedomBox, **FreedomBox, Private Cloud Server**
    * Installation als virtuelle Maschine wie hier beschrieben: https://wiki.debian.org/FreedomBox/Download
        * Bisher hat der Zugriff auf das Web-Interface dann nicht funktioniert.
    * [Quickstart](https://wiki.debian.org/FreedomBox/Manual#FreedomBox.2FManual.2FQuickStart.Quick_Start)
        * Hardware: https://wiki.debian.org/FreedomBox/Hardware
            * https://wiki.debian.org/FreedomBox/Hardware/Cubietruck
                * 90 Dollar: http://cubieboard.org/buy/

### osboxes
* Debian als virtuelle Maschine von osboxes.org
    * Debian_8.5-VB-32bit.7z, 1 GB, entpackt 4 GB
* Einstieg mit ssh vom Host aus scheint nicht so einfach zu sein, siehe [https://linuxconfig.org/enable-ssh-root-login-on-debian-linux-server] [https://forums.virtualbox.org/viewtopic.php?f=8&t=55766]

### Vagrant
* [Vagrant](https://de.wikipedia.org/wiki/Vagrant_(Software)) ist für openSUSE nur als 64 Bit verfügbar.
* Hier gibt es Vagrant-Maschinen: http://www.vagrantbox.es
    * Mit [vagrant ssh](https://www.maketecheasier.com/vagrant-on-linux) loggt man sich ein.
    * Wir nehmen diese hier: https://atlas.hashicorp.com/ARTACK/boxes/debian-jessie

### Konkrete Beispiele für Partitions-Setups
* siehe https://en.opensuse.org/SDB:Partitioning
    * LVM wird hier nur empfohlen, wenn man es wirklich braucht.
* [https://stikonas.eu/wordpress/2016/12/18/kde-partition-manager-3-0/ Neuigkeiten zum KDE Partition Manager 3.0]
    * Unter anderem "Both LVM on LUKS and LUKS on LVM configurations are now supported."

### Linux für alte Rechner
* Siehe [https://www.heise.de/newsticker/meldung/MX-16-Linux-Distribution-auch-fuer-aeltere-Rechner-3572664.html MX-16: Linux-Distribution auch für ältere Rechner]
* Siehe https://mxlinux.org/

### Tipps für Software
* Siehe https://mxlinux.org/current-release-features
