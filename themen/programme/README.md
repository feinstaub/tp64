Programm-Liste für Einsteiger
=============================

<!-- toc -->

- [Programme (vor Ort)](#programme-vor-ort)
  * [LibreOffice - Das freie Office-Paket](#libreoffice---das-freie-office-paket)
  * [Wörterbuch](#worterbuch)
  * [GIMP - Rastergraphik-Bearbeitung](#gimp---rastergraphik-bearbeitung)
  * [Inkscape - Vektorgraphik-Bearbeitung](#inkscape---vektorgraphik-bearbeitung)
  * [Marble Virtual Globe - Geographie](#marble-virtual-globe---geographie)
  * [Stellarium - Astronomie](#stellarium---astronomie)
  * [Hugin - Foto-Panoramas erzeugen](#hugin---foto-panoramas-erzeugen)
  * [Datensicherung](#datensicherung)
  * [Sweet Home 3D](#sweet-home-3d)
- [Programme (für zuhause)](#programme-fur-zuhause)
  * [10-Finger-Schreiben lernen](#10-finger-schreiben-lernen)
  * [MuseScore - Noten setzen](#musescore---noten-setzen)
  * [Audacity - Audio-Bearbeitung](#audacity---audio-bearbeitung)
  * [Videoschnitt](#videoschnitt)

<!-- tocstop -->

Programme (vor Ort)
-------------------
### LibreOffice - Das freie Office-Paket
* Texte schreiben mit '''Writer'''
* Präsentationen erstellen mit '''Impress'''
* Tabellen berechnen und Diagramme erstellen mit '''Calc'''
* Zeichnen mit '''Draw'''

### Wörterbuch
* offline
    * Englisch-Deutsch: '''ding''', '''kding'''
        * Praktische Tastenkombination für kding: '''Meta+Shift+T'''
* online
    * http://dict.leo.org

(gefunden auf [https://www.bitblokes.de/2013/07/offline-worterbucher-fur-linux-deutsch-englisch-ding-stardict-und-goldendict/ bitblokes.de])

### GIMP - Rastergraphik-Bearbeitung
* Schaue dir folgendes Tutorial an: https://de.wikibooks.org/wiki/GIMP

### Inkscape - Vektorgraphik-Bearbeitung
* Schaue dir folgendes Tutorial an: https://de.wikibooks.org/wiki/Inkscape

### Marble Virtual Globe - Geographie
* Paketname: **marble**
* $ sudo zypper install marble
* Tipps
    * Ansicht → Online Services
        * Earthquakes/Erdbeben anzeigen
    * Strecken messen (z. B. auf dem Mond (Karte wechseln) von den Apollo-Missionen) und mit Deutschland vergleichen

### Stellarium - Astronomie
* Paket: **stellarium** - Virtueller Sternenhimmel
* Aufgabe:
    * Stelle dich nachts bei klarem Himmel draußen hin und beobachte die Sterne.
    * Wenn dir etwas Bemerkenswertes auffällt, dann merke dir: '''wo''' du stehst, '''wohin''' du schaust (Himmelsrichtung) und wieviel Uhr es ist ('''wann''').
    * (Falls die Himmelsrichtung unklar war, öffne am nächsten Tag '''Marble,''' wähle die OpenStreetMap-Karte aus, zoome zu deinem Standort und versuche mit Hilfe der umgebenden Gebäude die Himmelsrichtung zu bestimmen.)
    * Öffne '''Stellarium''', stelle den Ort (linkes Randmenü, erster Eintrag), die Zeit (linkes Randmenü, zweiter Eintrag) und die Himmelsrichtung ein (mit der Maus) und schaue, ob du die Sterne von voriger Nacht wiedererkennst.

* Weitere Programme
    * **KStars**
    * **Marble** Virtual Globe (Political Map, Moon, Historical map, Create movie, Sternbilder) (→ Installieren)
        * Menü →Ansicht
            * Stars - Sternbilder anzeigen
            * Sun Control… - Sonnenschatten anzeigen
            * Eclipses - Sonnenfinsternisse
        * Ansicht → Online Services
            * Weather/Wetter

* siehe astronomie/README.md

### Hugin - Foto-Panoramas erzeugen
* Paketname: **hugin**
* Beschreibung: Toolchain for Stitching of Images and Creating Panoramas

### Datensicherung
* siehe ../../hintergrundwissen/datensicherung

### Sweet Home 3D
* Wohnungsplan und Inneneinrichtung 3D sichtbar machen
    * http://www.sweethome3d.com
    * openSUSE-Paket: SweetHome3D

Beispiel: home1.sh3d

Programme (für zuhause)
-----------------------
### 10-Finger-Schreiben lernen
* Tux Typing (Buchstaben mit Lasern abschießen)
* ...

### MuseScore - Noten setzen
* siehe ../musik

### Audacity - Audio-Bearbeitung
* ...
* TODO: how to record from playback? https://manual.audacityteam.org/man/tutorial_recording_computer_playback_on_linux.html

* **FretsOnFire** (todo: geht das noch?)
    * Beispiel mit den Spieldateien von FretsOnFire.
        * siehe FretsOnFire download package
        * Daten suchen: data/songs/bangbang
            * guitar.ogg
            * song.ogg
        * Beide Tracks in Audacity laden und wechselseitig stummschalten
            * /usr/share/games/fretsonfire/data/songs/bangbang/ (herausfinden z. B. über die yastfileliste)
    * Frets on Fire Song Packs - http://sourceforge.net/projects/fretsonfiresps/

### Videoschnitt
* Kdenlive
* Flowblade: https://jliljebl.github.io/flowblade/
