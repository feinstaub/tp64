Programmieraufgaben
===================

<!-- toc -->

- [Aufgaben mit Projektziel](#aufgaben-mit-projektziel)
  * [Python-Turtle](#python-turtle)
  * [Wiederholen, Wiederholen: Input, Output mit Zahlen](#wiederholen-wiederholen-input-output-mit-zahlen)
  * [Breakout](#breakout)
  * [Zählen, Einmaleins](#zahlen-einmaleins)
  * [Pong](#pong)
  * [Game of Life](#game-of-life)
  * [Tannenzapfenwerfen](#tannenzapfenwerfen)
  * [TicTacToe](#tictactoe)
  * [TyperTux](#typertux)
  * [Kartenspiel](#kartenspiel)
  * [Schere-Stein-Papier](#schere-stein-papier)
  * [Casino, Roulette](#casino-roulette)
  * [Travelling Salesman](#travelling-salesman)
  * [Eine Uhr mit Zeigern](#eine-uhr-mit-zeigern)
  * [Tabellen ausrechnen](#tabellen-ausrechnen)
  * [Pascal-Dreieck](#pascal-dreieck)
- [Aufgabensammlungen](#aufgabensammlungen)
  * [Python3-Lehrscript](#python3-lehrscript)
  * [C-HowTo](#c-howto)
  * [Aufgaben zum C/C++ Kurs](#aufgaben-zum-cc-kurs)
  * [Programming Challenges (sprach-agnostisch)](#programming-challenges-sprach-agnostisch)
  * [Aufgaben für den Informatikunterricht](#aufgaben-fur-den-informatikunterricht)
  * [Generelle Ideen](#generelle-ideen)
  * [wiki.zum.de](#wikizumde)
  * [Raytracing](#raytracing)
- [Vorhandene Programme ändern](#vorhandene-programme-andern)
  * [Moon-Lander, C++](#moon-lander-c)
  * [Moon-Buggy](#moon-buggy)
  * [Tunneler, C](#tunneler-c)
  * [c-with-3guys](#c-with-3guys)
  * [KSudoku](#ksudoku)
- [Later](#later)

<!-- tocstop -->

Wege:

    * Aufgaben mit einem Projektziel
    * Vorhandene Programme hernehmen und abändern
    * Aufgabensammlungen

Aufgaben mit Projektziel
------------------------
### Python-Turtle
* [Python-Einstieg mit Turtle](python-turtle/python-turtle-on-opentechschool.md)
    * Weitere Turtle-Programmierideen siehe dort

### Wiederholen, Wiederholen: Input, Output mit Zahlen
* Schreibe ein Programm, das eine Zahlen entgegennimmt und als Ausgabe ausgibt
    * Ist gerade oder ungerade?
    * Ist Prim-Zahl?
    * Ist Fibonacci-Zahl?

### Breakout
* py-qt-breakout-spiel

### Zählen, Einmaleins
* https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Programmier-Einstieg/Rechnen_und_Schleifen
    * Zählen für Anfänger
    * Schleife
    * Einmal-Eins-Tabelle

### Pong
* python-qt/aufgabe-pong.md

### Game of Life
* siehe game-of-life
* Zelluläre Automaten

### Tannenzapfenwerfen
* aka Curling
* aka Pferderennen

1. Untereinander eine Reihe von Ascii-Art-Zapfen oder -Bäume

```
  #
 ###
#####                                   |   |
  #                                      ---
```

2. Raten wer gewinnt

3. Go: Random-Lauf

### TicTacToe
```
  1 | 2 | 3
 --- --- ---
  4 | 5 | 6
 --- --- ---
  7 | 8 | 9

  o |   |
 --- --- ---
    | o | x
 --- --- ---
    |   | x

```

A vs. B

A vs. AI

### TyperTux
* siehe auch zehnfingersystem-README.md

1. Read words **from file**
2. ncurses: words move in
3. type them
4. OK:   fly out
5. fail: drop out
6. Punktestand

* mit ncurses
* Wortdatei einlesen, random, highlight matches (mit Klassen arbeiten)
* aspell de oder speed test
* oder auch erst mal Primzahlen oder gleich Raytracing - wichtig --- am Anfang viel zeigen

### Kartenspiel
* siehe python-kartenspiel

### Schere-Stein-Papier
* siehe python-schere-stein-papier
    * (anleitungen/python-schere-stein-papier/README.md)
    * todo: mit ncurses

### Casino, Roulette
* siehe programmieren-python/curses-python/arbeitsblatt-py-casino.adoc

### Travelling Salesman
* ...

### Eine Uhr mit Zeigern
* ... Winkel-Formel etc.

### Tabellen ausrechnen
* 1x1
    * mit Abständen
* Statistische Verteilungen?
* ASCII-Tabelle
* Was wird gerade in Mathe gemacht?
* mit LibreOffice Calc?

### Pascal-Dreieck
* https://de.wikipedia.org/wiki/Pascalsches_Dreieck, Rekursion

Aufgabensammlungen
------------------
### Python3-Lehrscript
* [Einführung ins Programmieren mit Python 3](http://pythonbuch.com)
    * "Dieses Skript wurde hauptsächlich für eine Einleitung ins Programmieren mit Python auf Gymnasiumsstufe geschrieben. Es richtet sich an Schülerinnen und Schüler eines Grundkurses, welche noch keine Programmiererfahrung haben. Es wurde darauf geachtet, dass auch nicht Computer-affine Personen dem Inhalt folgen können und davon profitieren. In dem Sinne ist es besser für einen Grundkurs als einen Leistungskurs geeignet."
    * Creative Commons Attribution-ShareAlike 4.0 International License - http://pythonbuch.com/impressum.html
    * Code: https://github.com/puremath/pythonbuch
        * https://www.juengling-edv.de/
            * https://www.juengling-edv.de/keine-schaetzungen-mehr/

### C-HowTo
* [C-HowTo](http://www.c-howto.de/tutorial/verzweigungen/uebung/), Jahr?, Lizenz?
    * schöne Aufgaben
    * http://www.c-howto.de/tutorial/zeiger/

### Aufgaben zum C/C++ Kurs
* ["Aufgaben zum C/C++ Kurs"](http://www.physik.uni-regensburg.de/studium/edverg/ckurs/Aufgabensammlung.pdf), Jahr?, Lizenz?
* ["Programmieren-Aufgabenkatalog"](http://www.nkode.io/assets/programming/prog-excercises.pdf), 2014, Java, Lizenz?, Uni-Einstieg
* ["CKurs - Aufgaben zu C"](http://www.ckurs.de/index.htm), Lizenz?, noch nicht probiert
* https://trainyourprogrammer.de/

### Programming Challenges (sprach-agnostisch)
* Schöne Programmier-Aufgaben (Programmiersprachen-agnostisch): https://ryanstutorials.net/programming-challenges/

### Aufgaben für den Informatikunterricht
* Aufgaben für den Informatikunterricht:
    * https://www.programmieraufgaben.ch/aufgabe/automat/gyqpuwic
        * Automat
    * http://www.michael-holzapfel.de/
        * Mathe- und Programmieraufgaben
    * Getränkeautomat:
        * http://schuljahr.inf-schule.de/2013-14/modellierung/zustandsmodellierung/endlicheautomaten/erkundung_automat

### Generelle Ideen
* Reverse String ohne reverse-Funktion

### wiki.zum.de
* https://wiki.zum.de/wiki/Programmieraufgaben
    * Gute, einfache Ideen:
        * Schleifen
        * Eingabe
        * Rechnen
        * Uhrsimulation (verschachtelte Schleifen)
        * Schleife und Zufallszahl: Würfelbecheraufgaben
    * Mehr:
        * https://www.programmieren-lernen.ch - Aufgaben für den Informatikunterricht

### Raytracing
* https://raytracing.github.io/books/RayTracingInOneWeekend.html

Vorhandene Programme ändern
---------------------------
### Moon-Lander, C++
* siehe c-cpp-moonlander, C++

### Moon-Buggy
* siehe ShellTux

### Tunneler, C
* siehe c-tunneler-anpassen/README.md

### c-with-3guys
* siehe c-with-3guys

### KSudoku
* siehe kde-kompilieren-beispiel-ksudoku.md

Later
-----
* Aufgaben-Ideen aus CS Circles mit Quellenangabe offline verfügbar machen
* einfache Programmieraufgaben mit einfacher Programmierreferenz
    * vergl. ShellTux
    * vergl. Spickzettel von CS Circles
