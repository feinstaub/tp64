Programmier-Einstieg mit Javascript-Hero
========================================

Start
-----
* Los geht's mit http://www.jshero.net/home.html

Häufige Fragen zu einzelnen Aufgaben
------------------------------------
* ...

Weitere Infos
-------------
* JavaScript-Hero gefunden auf: https://wiki.selfhtml.org/wiki/JavaScript/Tutorials
    * jshero.net verwendet localstorage
        * https://stackoverflow.com/questions/9948284/how-persistent-is-localstorage - in der Praxis nicht wirklich
* [JavaScript/Tutorials/Grundlagen des DOM und ein kleines Browserspiel](https://wiki.selfhtml.org/wiki/JavaScript/Tutorials/Grundlagen_des_DOM)
* https://hacks.mozilla.org/2014/08/programming-games-in-the-browser/ - 2014 - Stefan Trenkel, Robert Nyman
* [Leseliste: Funktionale Programmierung in JavaScript](https://forum.selfhtml.org/self/2016/dec/21/leseliste-funktionale-programmierung-in-javascript/1682961#m1682961)
