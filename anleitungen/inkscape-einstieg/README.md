Inkscape-Einstieg
=================

Baustein: "Inkscape"

...können mit Inkscape **Vektorgraphiken** erstellen und bearbeiten
...(kennen den Unterschied zu Rastergraphik, siehe kolourpaint oder GIMP)

Aufgabe 1
---------
Lese dir von diesem Wikipedia-Artikel (https://de.wikipedia.org/wiki/Vektorgrafik) die Einleitung und den ersten Abschnitt durch.

Nenne dann einen Unterschied zwischen Vektorgrapiken und Rastergraphiken.

Aufgabe 2
---------
Starte Inkscape auf deinem Rechner. Ggf. musst du es vorher noch installieren.

Aufgabe 3
---------
Falls Inkscape auf Englisch eingestellt ist, dann stelle die Sprache auf Deutsch um.

Erst selber probieren. Wenn du nicht weiterkommst, hier ist ein [Tipp](tipp-1.md).

Aufgabe 4
---------
Rufe im Menü folgendes auf: **Hilfe** > **Einführungen** > **Inkscape: Grundlagen**.

Es erscheint ein neues Inkscape-Fenster mit einem bebilderten Text. Maximiere das Fenster und scrolle etwas nach oben bis zum Anfang des Textes.

Lese dir den Text aufmerksam durch und probiere die Erklärungen selber aus.

Aufgabe 5
---------
Rufe im Menü Hilfe > Einführungen das Tutorial **Inkscape: Formen** auf und arbeite es durch.

Weiteres
--------
Schaue dir folgendes Tutorial an: https://de.wikibooks.org/wiki/Inkscape
