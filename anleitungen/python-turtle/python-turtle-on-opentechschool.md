Python-Einstieg mit Turtle
==========================

<!-- toc -->

- [Start](#start)
- [Aufgaben](#aufgaben)
  * [Haus vom Nikolaus](#haus-vom-nikolaus)
  * [Haus vom Nikolaus klein und groß](#haus-vom-nikolaus-klein-und-gross)
  * [Haus vom Nikolaus mit Zickzack](#haus-vom-nikolaus-mit-zickzack)
  * [Fraktale Schneeflocke, 3 Schritte](#fraktale-schneeflocke-3-schritte)
  * [Ein Schildkrötenrennen-Spiel](#ein-schildkrotenrennen-spiel)
  * [Spirograph](#spirograph)
  * [Mehr](#mehr)
- [Theoretische Fragen](#theoretische-fragen)

<!-- tocstop -->

Start
-----
Dies ist eine Python-3-Einführung, die mit dem turtle-Modul arbeitet:

* Öffne mit Mittelklick zur Übersicht das [Inhaltsverzeichnis](http://opentechschool.github.io/python-beginners/de/index.html)
* Stelle sicher, dass du weißt wie man [mit Kate Python-Programme bearbeitet und ausführt](https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Programmier-Einstieg/Python-Code_in_Dateien).
* **[Beginne mit den Aufgaben](http://opentechschool.github.io/python-beginners/de/simple_drawing.html)**

Aufgaben
--------
### Haus vom Nikolaus
* siehe http://opentechschool.github.io/python-beginners/de/variables.html#the-house-of-santa-claus

### Haus vom Nikolaus klein und groß
a) Baue das https://de.wikipedia.org/wiki/Haus_vom_Nikolaus[Haus vom Nikolaus] mit der Python-Turtle.

Hinweis zum Ausrechnen der Länge der schrägen Kanten: https://de.wikipedia.org/wiki/Satz_des_Pythagoras[Satz von Pythagoras] mit a = b.

b) Erstelle eine Funktion 'def haus(a):', wobei a die Länge der Grundfläche des Hauses ist.

Rufe die Funktion mit den Längen 100, 50 und 10 auf und zeichne damit 3 Häuser nebeneinander.

### Haus vom Nikolaus mit Zickzack
* siehe niko2.py

### Fraktale Schneeflocke, 3 Schritte
* siehe auch [Fraktale](../themen/graphik-fraktale/)

Ziel: Zeichne die Schneeflockenkurve - eigentlich https://de.wikipedia.org/wiki/Koch-Kurve[Koch-Kurve] - mit der Turtle-Graphik.

a) Erstelle eine Funktion, die die erste Iteration der Kurve zeichnet:

    def koch(a):

a: Länge einer Kante

b) Lerne Rekursion kennen, indem du die ersten n Elemente https://de.wikipedia.org/wiki/Fibonacci-Folge[Fibonacci-Folge] mit Hilfe einer rekursiven Funktion

    def fib(n):

ausgibst.

c) Erweitere die Schneeflocken-Funktion, so dass die Kurve mit einer einstellbaren Anzahl von Iterationen gezeichnet wird:

    def koch(a, n):

a: Länge einer Kante in der ersten Iteration
n: Anzahl der Iterationen zusätzlich zu der ersten (n = 0)

### Ein Schildkrötenrennen-Spiel
* later, aber irgendwie ohne Schildkröte
* penup(), pendown(), forward(), clear(), random, etc.

### Spirograph
* Spirograph (Blumen mit Schleifen)
    * siehe z. B. repaircafe/turtleblume.py

### Mehr
* https://docs.python.org/3/library/turtle.html
* Graphik mit Qt
* siehe Pygame mit Python 3

Theoretische Fragen
-------------------
Übungsfragen nach Fertigstellung:

* Was ist eine Variable?
* Was ist eine Schleife?
* Was ist eine Funktion?
* Was ist ein Parameter in einer Funktion?
* Was ist eine Verzweigung?
* Was sind logische Operatoren?
