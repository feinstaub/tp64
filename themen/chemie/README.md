Chemie
======

<!-- toc -->

- [Kalzium](#kalzium)

<!-- tocstop -->

Kalzium
-------
* Webseite: https://userbase.kde.org/Kalzium

* Interessante Funktionen
    * Rechner, Umrechner
    * Seitenleiste -> Ansicht -> **Animation** zu verschiedenen Werten
        * z. B. Entdeckungsdatum, Aggregatzustand je nach Temperatur, ...
