#!/usr/bin/env python3

import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QAction, QVBoxLayout, QLabel

class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'QLabel'
        self.left = 0
        self.top = 0
        self.width = 300
        self.height = 200
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.label1 = QLabel("Label 1 - <b>Hallo fett</b>")
        self.label2 = QLabel("Label 2 - <font color=\"red\">Hallo rot</font>")
        self.label3 = QLabel("Label 3 - <font color=\"#0000FF\">Hallo blau</font>")
        self.label4 = QLabel("Label 4 - <b><font face=\"Liberation Serif\" size=12 color=\"green\">Hallo Schrift</font></b>")
        self.label5 = QLabel("Label 5 - siehe auch QLabel::setTextFormat(Qt::TextFormat)")

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.label1)
        self.layout.addWidget(self.label2)
        self.layout.addWidget(self.label3)
        self.layout.addWidget(self.label4)
        self.layout.addWidget(self.label5)
        self.setLayout(self.layout)

        self.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
