import QtQuick 2.12
import QtGraphicalEffects 1.12

Item {
    id: mybackground

    anchors.fill: parent

    Rectangle {
        anchors.fill: parent
        // https://stackoverflow.com/questions/10078000/qml-gradients-with-an-orientation
//         width: parent.height
//         height: parent.width
//         anchors.centerIn: parent
//         rotation: 45
        gradient: Gradient {
            //start: Qt.point(0, 0)
            //end: Qt.point(width, height)
            GradientStop {
                position: 0.0
                color: "#fdc3c3"
            }
            GradientStop { position: 1.0; color: "#975050" }
        }

        // todo: why is LinearGradient only black and white?
    }
}
