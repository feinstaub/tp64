#include <iostream>

using namespace std;

void tictacto_hard(){
    string spst="Unentschieden";
    int skip=0;
    string cord[5][5]={{"-","-","-","-","-"},
                       {"-","-","-","-","-"},
                       {"-","-","-","-","-"},
                       {"-","-","-","-","-"},
                       {"-","-","-","-","-"}};
    int pl=1;
    int pl1=0;
    int pl2=0;
    int abbruch=1;
    int d=0;
    while(d==0){
        for(int a=0;a<=4;a++){//Überprüfschleife
            for(int b=0;b<=4;b++){
                int poss[8][6]={{-1,-1,-2,-2,-3,-3},{-1,0,-2,0,-3,0},
                                {-1,1,-2,2,-3,3},{0,1,0,2,0,3},
                                {1,1,2,2,3,3},{1,0,2,0,3,0},
                                {1,-1,2,-2,3,-3},{0,-1,0,-2,0,-3}};
                int pos1,pos12,pos2,pos22,pos3,pos32;
                if(cord[a][b]=="x"){
                    for(int c=0;c<=7;c++){
                        pos1=a + poss[c][0];
                        pos12=b + poss[c][1];
                        pos2=a + poss[c][2];
                        pos22=b + poss[c][3];
                        pos3=a + poss[c][4];
                        pos32=b + poss[c][5];
                        if(pos1>=0 && pos1<=4 && pos12>=0 && pos12<=4 &&
                            pos2>=0 && pos2<=4 && pos22>=0 && pos22<=4 &&
                            pos3>=0 && pos3<=4 && pos32>=0 && pos32<=4){
                            if("x"==cord[pos1][pos12] && "x"==cord[pos2][pos22] && "x"==cord[pos3][pos32]){
                                abbruch=0;
                                spst="Gewonnen Hat Spieler1:";
                            }
                        }
                    }
                }
                if(cord[a][b]=="o"){
                    for(int c=0;c<=7;c++){
                        pos1=a - poss[c][0];
                        pos12=b - poss[c][1];
                        pos2=a - poss[c][2];
                        pos22=b - poss[c][3];
                        pos3=a + poss[c][4];
                        pos32=b + poss[c][5];
                        if(pos1>=0 && pos1<=4 && pos12>=0 && pos12<=4 &&
                            pos2>=0 && pos2<=4 && pos22>=0 && pos22<=4 &&
                            pos3>=0 && pos3<=4 && pos32>=0 && pos32<=4){
                            if("o"==cord[pos1][pos12] && "o"==cord[pos2][pos22] &&
                                "o"==cord[pos3][pos32]){
                                abbruch=0;
                                spst="Gewonnen Hat Spieler2:";
                            }
                        }
                    }
                }
            }
        }




        if(abbruch==1){
            cout << "Es ist Spieler " << pl << " an der Reihe.\n" << endl;
        }
        cout << " |1|2|3|4|5|\n";
        cout << "1|" << cord[0][0] << "|" << cord[0][1] << "|" << cord[0][2] << "|" << cord[0][3]<< "|" << cord[0][4] << "|\n";

        cout << "2|" << cord[1][0] << "|" << cord[1][1] << "|" << cord[1][2] << "|" << cord[1][3]<< "|" << cord[1][4] << "|\n";

        cout << "3|" << cord[2][0] << "|" << cord[2][1] << "|" << cord[2][2] << "|" << cord[2][3]<< "|" << cord[2][4] << "|\n";

        cout << "4|" << cord[3][0] << "|" << cord[3][1] << "|" << cord[3][2] << "|" << cord[3][3]<< "|" << cord[3][4] << "|\n";

        cout << "5|" << cord[4][0] << "|" << cord[4][1] << "|" << cord[4][2] << "|" << cord[4][3]<< "|" << cord[4][4] << "|\n";
        string eingabe;
        if(abbruch==1){
            cout << "Mache eine Eingabe Spieler "<< pl << ":\n";
            cin >> eingabe;
        }
        string zahl="123456";
        int cord1=0;
        int cord2=0;
        skip=0;
        if(abbruch==1){
            if(eingabe=="stop"){
                abbruch=0;
                spst="Abruch";
            }
            if(eingabe[0]=='s'){
                for(int a=0;a<=4;a++){
                    if(eingabe[1]==zahl[a]){
                        cord1=a;
                    }
                    if(eingabe[2]==zahl[a]){
                        cord2=a;
                    }
                }
                cout << pl;
                    if(pl==1 && pl1<=5){
                        cord[cord1][cord2]="x";
                        pl=2;
                        pl1++;
                        skip=1;
                    }
                    if(pl==2 && pl2<=5 && skip==0){
                        cord[cord1][cord2]="o";
                        pl=1;
                        pl2++;
                    }


                }

            int richt1,richt2;
            int cordp1,cordp2;
            string richtung="oulr";
            int richtung2[4][2]={{-1,0},{1,0},{0,-1},{0,+1}};
            if(eingabe[0]=='p'){
                for(int a=0;a<=4;a++){
                    if(eingabe[2]==zahl[a]){
                        cord1=a;
                    }
                    if(eingabe[3]==zahl[a]){
                        cord2=a;
                    }
                    if(richtung[a]==eingabe[1] && a<=3){
                        richt1=richtung2[a][0];
                        richt2=richtung2[a][1];
                    }
                }
                int i=0;
                cordp1=cord1;
                cordp2=cord2;
                while(i==0){
                    cordp1=cordp1 + richt1;
                    cordp2=cordp2 + richt2;
                    if(cordp1>=0 && cordp2<=4 && cordp2>=0 && cordp2<=4){
                        if(cord[cordp1][cordp2]=="-"){
                            i++;
                            while(i==1){
                                if(cordp1==cord1 && cordp2==cord2){
                                    i++;
                                    cord[cordp1][cordp2]="-";
                                    if(pl==1){
                                        pl=2;
                                        skip=1;
                                    }
                                    if(pl==2 && skip==0){
                                        pl=1;
                                    }
                                }
                                else{
                                    cord[cordp1][cordp2]=cord[cordp1 - richt1][cordp2 - richt2];
                                }
                                cordp1=cordp1 - richt1;
                                cordp2=cordp2 -richt2;
                            }
                        }
                        else{
                        }
                    }
                    else{
                        i++;
                    }
                }
            }
        }
        if(abbruch==0){
            d++;
            cout << spst <<":Das Spiel ist vorbei.\n";
        }
    }
}
