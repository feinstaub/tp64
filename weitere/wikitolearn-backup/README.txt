Wiki2Learn Backup
=================

Kopie von https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python. Stand: 2018-08-06
Ziel: Schrittweise in tp64 einarbeiten und löschen

Einstieg in die Programmierung mit Python -
Einstieg in die Verwendung des Computers mit Hilfe von freier Software am Beispiel von GNU/Linux. Programmieren mit den Sprachen Python, C, C++, HTML und QML und Qt. Arbeit an und mit freien Softwareprojekten. Lernen von Informatikkonzepten anhand praktischer Beispiele (6. bis 13. Klasse).

Vor allem die Aufgaben raussuchen und ausbauen, die viel auf Wiederholung setzen.


1. Arbeitsblätter - Erstes Ziel - Primzahlen ausrechnen
----------------------------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Arbeitsbl%C3%A4tter/Erstes_Ziel_-_Primzahlen_ausrechnen

__TOC__

    1 Einstieg
    2 Linux installieren
    3 Arbeitsumgebung kennenlernen
        3.1 Neue Software installieren
        3.2 Die UNIX-Shell bash
    4 Python
        4.1 Interaktive Python-Shell
        4.2 Größere Programme mit Texteditor
        4.3 Programmierkonzepte
    5 Algorithmus
    6 Primzahlen-Programm
        6.1 Bonus mit grep
        6.2 Ergebnis innerhalb der Gruppe vergleichen


<source>
 ______________________________
/ Hey Tux, um was geht es hier \
\ eigentlich?                  /
 ------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
                               ____________
                              < 2 3 5 7... >
                               ------------
                                 \
                                  \
                                      .--.
                                     |o_o |
                                     |:_/ |
                                    //   \ \
                                   (|     | )
                                  /'\_   _/`\
                                  \___)=(___/

 _____
< ??? >
 -----
        \   ^__^
         \  (xx)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
                               _________________________________________
                              / ...11 13 17... Primzahlen. Aber         \
                              | Achtung: der Pfad zu den Primzahlen ist |
                              | etwas verschlungen, da wir sehr viel    |
                              \ neues kennenlernen werden.              /
                               -----------------------------------------
                                 \
                                  \
                                      .--.
                                     |o_o |
                                     |:_/ |
                                    //   \ \
                                   (|     | )
                                  /'\_   _/`\
                                  \___)=(___/

 _____________
< Los geht's! >
 -------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
</source>

== Einstieg ==

Lese dir folgende Abschnitte der Reihe nach durch:

* [[Course:Einstieg in die Programmierung mit Python/Programmier-Einstieg/Überblick|Programmier-Einstieg/Überblick]]
* [[Course:Einstieg in die Programmierung mit Python/Programmier-Einstieg/Initialisierung|Programmier-Einstieg/Initialisierung]]
* [[Course:Einstieg in die Programmierung mit Python/Programmier-Einstieg/Der_Computer|Programmier-Einstieg/Der_Computer]]


Lehrer: Ziel-Programm vorführen: https://github.com/feinstaub/tp64/blob/master/sample-code/prim-demo.py

== Linux installieren ==

History:
    * Sorge dafür, dass du openSUSE Linux verfügbar hast. Z. B. indem du es auf einen USB-Stick installierst. Frage dazu jemanden, der es schon gemacht hat, ob er dir hilft. Hinweise:
    ** Linux auf einen USB-Stick installieren: http://feinstaub.github.io/arbeitsblatt.net/arbeitsblatt-linux-stick.html
    ** Linux mit VirtualBox ausführen: http://feinstaub.github.io/arbeitsblatt.net/arbeitsblatt-linux-image-virtualbox.html

* Falls du damit fertig bist oder nicht weiterkommst
** suche dir etwas von folgender Seite aus: [[Course:Einstieg in die Programmierung mit Python/FAQ/Aufgaben für Zwischendurch|Aufgaben für Zwischendurch]]
** Bearbeite das Arbeitsblatt zu Binärzahlen: [[Course:Einstieg in die Programmierung mit Python/Arbeitsblätter/Binäres Zahlensystem]]

* Schülerfragen: Was ist der Vorteil von Linux? Was macht es aus?

== Arbeitsumgebung kennenlernen ==

=== Neue Software installieren ===
Mache dich damit vertraut, wie man neue Software installiert.
* Öffne dazu [[Course:Einstieg in die Programmierung mit Python/FAQ/Software installieren|FAQ/Software installieren]] und arbeite Punkt 1 und 2 durch.

=== Die UNIX-Shell bash ===
Arbeite folgendes durch:
* [[Course:Einstieg in die Programmierung mit Python/FAQ/Die Kommandozeile|FAQ/Die Kommandozeile]] (bis Abschnitt 1.4)


BONUS-Aufgabe: Mit der Shell kann man sehr viel machen, wenn man weiß wie. Siehe z. B.
* http://feinstaub.github.io/arbeitsblatt.net/arbeitsblatt-cowsay-fortune.html

== Python ==

=== Interaktive Python-Shell ===

* Arbeite folgendes durch: [[Course:Einstieg in die Programmierung mit Python/Programmier-Einstieg/Python interaktiv|Python interaktiv]]

=== Größere Programme mit Texteditor ===

* Arbeite folgendes durch: [[Course:Einstieg in die Programmierung mit Python/Programmier-Einstieg/Python py-Dateien|py-Dateien]]

=== Programmierkonzepte ===

* Arbeite folgendes durch: [[Course:Einstieg in die Programmierung mit Python/Programmier-Einstieg/Rechnen / Schleifen|Rechnen / Schleifen]]
* BONUS-Aufgabe: Das '''grep'''-Tool: [[Course:Einstieg in die Programmierung mit Python/Programmier-Einstieg/grep|Programmier-Einstieg/grep]]

== Algorithmus ==

Überlege zusammen mit deiner Gruppe einen '''Algorithmus''' (also eine schriftliche Vorgehensweise), der Primzahlen (2, 3, 5, 7, 11, 13, 17, ...)  ausgibt.

Also wie würde man systematisch herausfinden, welche der natürlichen Zahlen (ab 2) Primzahlen sind?

Führe deinen Algorithmus für die ersten 7 Primzahlen per Hand durch.

Hilfestellungen (immer einen Punkt weiterlesen, wenn du nicht weiterkommst):

1) Überlege dir, mit welchen Schritten du für eine beliebige Zahl (z. B. 15 oder 17) herausfinden würdest, ob es eine Primzahl ist oder nicht.

2) Erinnere dich, was eine Primzahl ausmacht (nur durch 1 und sich selbst teilbar).

3) Was wäre, wenn du nun für eine gegebene Zahl schaust, ob es eine Zahl zwischen 2 und <der Zahl minus 1> gibt, durch die die Zahl teilbar ist? Ist es dann eine Primzahl oder nicht?

4) Wenn du die Vorgehensweise für eine beliebige Zahl hast, dann kannst du sie auf alle natürlichen Zahlen der Reihe nach anwenden.

== Primzahlen-Programm ==

Du hast nun folgende Programmierkonzepte und Python-Sprachelemente kennengelernt:

* Variable (z. B. a)
* Zuweisung (z. B. a = 5)
* print-Ausgabe
* Vergleichsoperatoren (z. B. a < 5)
* if-Anweisung
* while-Schleife

Versuche nun den den Primzahlen-Algorithmus aus dem vorigen Schritt in ein '''Python-Programm''' umzusetzen. Gebe auch die Nummer der Primzahl aus. Die Ausgabe für die ersten 10 Primzahlen sieht so aus:

<source>
1: 2
2: 3
3: 5
4: 7
5: 11
6: 13
7: 17
8: 19
9: 23
10: 29
</source>

=== Bonus mit grep ===

1. Nutze '''grep''' (mit dem Pipe-Symbol), um zu bestätigen, dass die Zahl 676763 eine Primzahl ist.

2. Ist die Zahl 676767 auch eine Primzahl?

3. Suche nach der einemillionsten Primzahl.

=== Ergebnis innerhalb der Gruppe vergleichen ===

* Schreibe dein Programm so um, dass es bei Primzahlen größer als 1.000.000 mit der Berechnung aufhört.
* Leite das Ergebnis in die Datei ''prim_<dein Name>.txt'' um.
* Kopiere die Ergebnisdatei deines Teamkollegen auf deinen Rechner.
* Verwende das Programm '''KDiff3''' oder '''Kompare''', um festzustellen, ob beide Dateien gleich sind.


1. Arbeitsblätter - Software installieren
--------------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Arbeitsbl%C3%A4tter/Software_installieren

== Neue Software installieren ==

__TOC__

    1 Neue Software installieren
    2 Software mit YaST installieren (openSUSE)
        2.1 Hintergrund
        2.2 Schritt für Schritt
            2.2.1 Aufgabe 1: Neue Software mit YaST installieren
            2.2.2 Aufgabe 2: Software deinstallieren
            2.2.3 Aufgabe 3: Mehrere Programme auf einmal installieren
    3 Software mit der Kommandozeile installieren
        3.1 Ein Paket installieren
    4 Ausblick


<source>
 _______________________________________
/ Muh. Wie wird unter openSUSE Linux    \
\ eigentlich neue Software installiert? /
 ---------------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
                               _________________________________________
                              / Das steht auf dieser Seite. Und du bist \
                              \ auch dabei...                           /
                               -----------------------------------------
                                 \
                                  \
                                      .--.
                                     |o_o |
                                     |:_/ |
                                    //   \ \
                                   (|     | )
                                  /'\_   _/`\
                                  \___)=(___/

 _______
< Kuul. >
 -------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
</source>

== Software mit YaST installieren (openSUSE) ==

=== Hintergrund ===

YaST ist die Abkürzung für "Yet another Setup Tool" ("Noch ein Administrationswerkzeug"). Es wird für Administrationszwecke unter openSUSE Linux verwendet. Es enthält unter anderem ein Modul zum Installieren und Deinstallieren von Software.

=== Schritt für Schritt ===

==== Aufgabe 1: Neue Software mit YaST installieren ====

Funktioniert mit: openSUSE (13.2), 42.1, 42.2

'''Ziel: Texteditor 'kate' installieren'''

* '''YaST''' aus dem K-Menü aufrufen.
* Es öffnet sich ein Passwort-Fenster. Dort das '''root'''-Passwort eingeben. [[Course:Einstieg in die Programmierung mit Python/FAQ/Root-Passwort|Was ist das?]]
* "Software installieren oder löschen" [DE] bzw. "Software Management" [EN] anklicken.
* Reiter "Suchen" auswählen.
* "kate" eintippen.
* "Suchen"-Knopf drücken.
* Eintrag in der Liste rechts finden, der unter der Spalte "Paket" den Namen "kate" hat.
* In das leere Quadrat ein Häkchen klicken (alternativ: rechte Maustaste -> "Installieren" klicken).
* Reiter "Installationsübersicht" auswählen und ansehen, was tatsächlich installiert werden wird.
* "Übernehmen"-Knopf drücken (unten rechts).
* (kate wird heruntergeladen und installiert)
* "Beenden"-Knopf unten rechts drücken.
* Um zu prüfen, ob die Installation tatsächlich erfolgreich war:
** kate im K-Menü finden und starten.
** kate wieder schließen.

==== Aufgabe 2: Software deinstallieren ====

Ziel: Texteditor 'kate' wieder deinstallieren

* YaST-Modul "Software-Management" aufrufen.
** Entweder wie in Aufgabe 2
** oder alternativ:
*** '''Alt+Leertaste''' (oder mit KDE/Plasma-Version < 5: '''Alt+F2''') drücken.
*** "Software" eintippen.
*** "Software Management" mit den Cursor-Tasten auswählen und ENTER-Taste drücken.
* Reiter "Suchen" auswählen.
* "kate" eintippen.
* Eintrag in der Paket-Liste so lange anklicken, bis ein X erscheint (oder die rechte Maustaste verwenden).
* "Übernehmen"-Knopf drücken.
* (kate wird deinstalliert)


==== Aufgabe 3: Mehrere Programme auf einmal installieren ====

* YaST-Modul "Software-Management" aufrufen.
* Im Reiter "Suchen" "kate" eintippen und in der Paketliste abhaken.
* Im Reiter "Suchen" "chess" eintippen und in der Paketsliste den Eintrag "gnome-chess" abhaken.
* "Übernehmen"-Knopf drücken.
* (beide Programme werden installiert)
* 'Chess' (ein Schachprogramm) ausprobieren (Alt+Leertaste, "Chess")

== Software mit der Kommandozeile installieren ==

=== Ein Paket installieren ===

Ziel: Das Programm '''cowsay''' mit Hilfe des Tools '''zypper''' installieren.

* Das Programm "Konsole" öffnen (z. B. via K-Menü oder Alt+Leertaste, siehe auch [[Course:Einstieg in die Programmierung mit Python/FAQ/Die Kommandozeile|FAQ/Die Kommandozeile]].
* <tt>cowsay Muh</tt> eingeben und ENTER drücken.
** Befehl nicht gefunden.
** <tt>cnf cowsay</tt> eingeben.
*** Die Ausgabe sagt, was zu tun ist, um das (noch) nicht vorhandene Programm zu installieren.
* <tt>sudo zypper install cowsay</tt> eingeben.
** Nach Rückfragen wird cowsay installiert.
* <tt>cowsay Muh</tt> eingeben. Das sollte das Ergebnis sein:

<source>
 _____
< Muh >
 -----
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
</source>

* Aufgabe: <tt>cowsay Hallo Welt</tt> eingeben.

== Ausblick ==
* Siehe [[Course:Einstieg in die Programmierung mit Python/Eigenes System/Software finden und installieren|Eigenes System/Software finden und installieren]]


1. Arbeitsblätter - Tunneler anpassen
----------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Arbeitsbl%C3%A4tter/Tunneler_anpassen
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Arbeitsbl%C3%A4tter/Tunneler_anpassen_2

MOVED

2. Programmier-Einstieg - Theorie
---------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Programmier-Einstieg/Theorie (DEL)

== Überblick ==

Eine Treppe zum deinem ersten Programm:

<source>


                                                                                                                                :-D
                                                                                                                              .--------
                                                                                                                              |
                                                                                                         Primzahlen-          |
                                                                                                         Programm erstellen   |
                                                                                                       .----------------------'
                                                                                       Algorithmus     | (ggf. auch mal
                                                                                       überlegen       | zwei Stufen zurück)
                                                                                       (Wie findet man |
                                                                                       Primzahlen,     |
                                                                                     .-----------------'
                                                                     Python-         | wenn man es per
                                                                     Sprachelemente  | Hand machen würde?)
                                                                     kennenlernen    |
                                                                     (erste Bausteine|
                                                                   .-----------------'
                                              Programmierumgebung  | eines großen
                                              einrichten           | Baukastens)
                                              (python,             |
                                              kate-Texteditor,     |
                                              Konsole)             |
                                            .----------------------'
                    Software:               |
                    Eigenes Betriebssystem  |
                    installieren            |
                    (openSUSE Linux)        |
                  .------------------------'
        Hardware: |
        Computer  |
        vorhanden |
       -----------'
</source>

== Initialisierung ==

=== Begriffe ===

Hier ist eine Reihe von Computer/Informatik-Begriffen, die sich am Anfang vielleicht kompliziert anhören:

* zu GNU/Linux: "Konsole", "Bash-Shell", "Python-Shell", "Plasma-Shell", "ausführbare Datei", "Prozess"
* zum Programmieren: "Variable", "Wertzuweisung", "if-Anweisung", "Schleife", "Funktion", "formaler und aktueller Parameter", "Klasse", "Methode", "Instanz", "Instanzvariable", "Qt"
* zu Algorithmen: "Rekursion", "Fibonacci-Zahlen"
* zu Qt: "Signal/Slot", "QPainter"

Aber keine Angst; im Laufe des Kurses wirst du Schritt für Schritt lernen, damit umzugehen.

== Der Computer ==


'''Passende Hardware ist vorhanden''': der Computer steht vor dir auf dem Tisch.

'''Was kann der Computer gut?'''

-> Rechnen / immer dasselbe machen, aber dafür schnell

'''Wofür braucht man viel Rechenleistung?'''

-> Primzahlen finden!

(Primzahlen sind übrigens die Basis gängiger Verschlüsselungssysteme im Internet. Wer es schafft Primzahlen sehr schnell auszurechnen, kann heute gängige Krypto-Systeme knacken. Aber keine falsche Hoffnung: ohne Quantencomputer und den dazu passenden [https://www.itp.tu-berlin.de/fileadmin/a3233/upload/AG_Brandes/Seminar_SS_08/falk.pdf Quantenalgorithmen] ist dies bisher noch keinem gelungen.)

'''Wie sagt man dem Computer, was er tun soll?'''

-> Programmieren

Ein Computer ohne passende Software versteht erst mal nur Einsen und Nullen. Damit kann man als Mensch wenig anfangen.

-> '''Wir brauchen ein Betriebssystem''', auf dem die eigentlichen Anwendungen (also Programme, inkl. der Programmiersprache Python). Hier in der Schule ist Windows vorinstalliert. Wir wollen aber unser eigenes installieren. Das kann man auch mit Nachhause nehmen und bei Bedarf anderen zeigen wie es geht.


2. Programmier-Einstieg - Python interaktiv
-------------------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Programmier-Einstieg/Python_interaktiv

    1 Den Computer rechnen lassen
        1.1 Übung
    2 Variablen, Ausgabe
    3 Vergleichen
        3.1 Übung

Dieser Abschnitt erklärt den Umgang mit der Python-Shell, dem Kate-Editor und dem Konsolen-Suchtool grep.

__TOC__

=== Den Computer rechnen lassen ===

1. Öffne eine '''Bash-Shell''' indem du das Programm '''konsole''' startest (mehr Infos dazu: https://de.wikipedia.org/wiki/Bash_(Shell), https://de.wikipedia.org/wiki/Konsole_(KDE), TIPP: https://de.wikipedia.org/wiki/Yakuake).

<source>
gregor@linux-85rg:~/dev/ag>
</source>

2. Gebe <tt>python3</tt> und drücke ENTER.

<source lang="python">
gregor@linux-85rg:~/dev/ag> python3
Python 3.4.5 (default, Jul 03 2016, 13:32:18) [GCC] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
</source>

Du bist nun in der '''Python3-Shell'''.

3. Drücke '''Strg+D''' und du bist wieder draußen.

4. Drücke nochmal Strg+D und damit ist auch die Bash-Shell beendet.

5. Wiederhole nun Schritt 1. und 2., aber überspringe Schritt 3. und 4. :-)

6. Gebe <tt>100 + 50</tt> ein und drücke ENTER.

Das Plus-Zeichen (+) nennt man '''Operator'''.

==== Übung ====

1. Probiere die 4 Grundrechenarten aus. Die Operatoren dazu sind + (plus), - (minus), * (Multiplikation), / (Division)

2. Probiere Multiplikation mit großen, sehr großen und irrsinnig großen Zahlen aus. Wie schnell ist der Computer im Vergleich zum Ausrechnen per Hand?

3. Du kannst auch Potenzieren. Der Operator ist **. Berechne 2 hoch 4, 2 hoch 10, 2 hoch 1000 und 2 hoch 1.000.000.

TIPP: Wenn eine Berechnung zu lange dauert und du das Programm abbrechen möchtest, drücke ''Strg+C''.

=== Variablen, Ausgabe ===

Gebe folgendes selber der Reihe nach in die Python ein:

<source lang="python">
>>> a = 5
>>> b = 6
>>> print(a + b)
11
>>> c = a + 3 * b
>>> print(c)
23
>>> c = 10
>>> print(c)
10
>>>
</source>

Folgendes ist neu:
* Neue '''Variable definieren''' und einen Wert '''zuweisen'''.
* Mit Variablen rechnen
* Die '''Ausgabe'''-Funktion print
* Das Ergebnis einer Berechnung in einer Variablen speichern.
* Einer vorhandenen Variablen einen neuen Wert zuweisen.

=== Vergleichen ===

Gebe folgendes selber der Reihe nach in die Python ein

<source>
>>> a = 5
>>> b = 7
>>> a > b
False
>>> a < b
True
>>> a >= 5
True
>>> a > 5
False
>>> a == b
False
>>> a + 2 == b
True
>>> a != b
True
>>>
</source>

Das Ergebnis eines Vergleichsoperators ist entweder True oder False.

Der Operator ist übrigens ''binär'', weil er mit ''zwei'' Ausdrücken arbeitet; ein Ausdruck kann eine Zahl, eine Variable oder auch etwas kompliziertes sein.

Folgende Vergleichsoperatoren gibt es:

<source>
<
>
<=
>=
==
!=
</source>

==== Übung ====

Überlege dir vorher, was das Ergebnis der folgenden Ausdrücke ist, bevor du sie zur Kontrolle in Python eingibt:
<source lang="python">
>>> a = 5
>>> b = 7

a < b
a <= b - 1
a <= b - 2
a <= b - 3
a * 3 > b * 2
a * b == b * a
a + b == b + a
a - b != b - a
a + a - b > 2

# Man kann auch mit Klammern arbeiten:

(a + 5) / 2 == a

</source>


2. Programmier-Einstieg - Python-Code in Dateien
------------------------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Programmier-Einstieg/Python-Code_in_Dateien

    1 Ein einfaches Programm
        1.1 Kate öffnen
        1.2 Programmtext eingeben
        1.3 Datei speichern
        1.4 In die Kommandozeile wechseln
        1.5 Programm mit python ausführen
        1.6 Programm ausführbar machen
        1.7 Programm ausführen
        1.8 Fehler korrigieren
    2 Nächste Schritte

__TOC__

=== Ein einfaches Programm ===

Jetzt werden unsere Programme größer als nur eine Zeile.

==== Kate öffnen ====
Dazu brauchen wir einen '''Texteditor'''. Wir nehmen '''Kate'''.

Installieren mit
* <tt>sudo zypper install kate</tt>

Damit du die grundlegenden Funktionen des Kate-Editors kennenlernst, lese dir folgendes durch:
* [[Course:Einstieg in die Programmierung mit Python/FAQ/Kate-Tipps|Kate-Tipps]] (bis einschließlich Abschnitt 1.3)

==== Programmtext eingeben ====

Ein einfaches Python-Programm sieht so aus:

<source>
#!/usr/bin/python3

print("Hallo Welt")
</source>

Wenn Umlaute (ä, ö, ü usw.) korrekt dargestellt werden sollen, dann sieht es so aus:
<source>
#!/usr/bin/python3
# -*- coding: utf-8 -*-

print("Hallo Welt")
</source>

* Starte Kate.

* Tippe den obigen Text ab (für den Anfang reicht die erste Variante).

* Der Text befindet sich nun in einem ungespeicherten Dokument. Dies erkennt man man dem Sternchen oben in der Titelleiste rechts neben dem Dokumentnamen.

==== Datei speichern ====

* Drücke Strg+S oder wähle im Menü ''Datei'' den Punkt ''Speichern''.

* Das Speichern-Dialog öffnet sich.

* Wechsle in den Ordner ~/ag
** Die Tilde (~) steht für das persönliche Verzeichnis des eingloggten Benutzers
** Der Unterordner ''ag'' ist möglicherweise noch nicht vorhanden. Erstelle ihn mit dem entsprechenden Knopf im Speichern-Dialog

* Wähle einen Dateiname (Textfeld unten): ''hallo.py''.
** Achte darauf, dass du keine Leerzeichen am Anfang oder am Ende einfügst
** Beim Programmieren ist es empfehlenswert, die Dateinamen immer klein zu schreiben und anstelle von Leerzeichen Bindestriche (-) oder Unterstriche (_) zu verwenden.

* Drücke den Speichern-Knopf.

==== In die Kommandozeile wechseln ====

* Starte eine Bash-Kommandozeile.
* Wechsle in das Verzeichnis, in das du im vorigen Schritt die py-Datei gespeichert hast.
** Verwende dazu den Befehl '''cd''' und '''ls'''. Wie das geht, lernst du hier: [[Course:Einstieg_in_die_Programmierung_mit_Python/FAQ/Die_Kommandozeile#Dateien_und_Verzeichnisse|Dateien_und_Verzeichnisse]]

==== Programm mit python ausführen ====

Führe das Programm aus, indem du folgendes auf der Konsole eingibst (Erinnerung: das $-Zeichen wird nicht mit eingegeben):

<source>
$ python3 hallo.py
</source>

==== Programm ausführbar machen ====

Die Python-Datei wird mit folgendem Befehl (einmalig) ausführbar gemacht:

<source>
$ chmod +x hallo.py
</source>

==== Programm ausführen ====
Nun kannst du das Programm auch ausführen, ohne python3 davorzuschreiben, weil der python-Befehl schon in der ersten Zeile deiner Datei steht (<tt>#!/usr/bin/python3</tt>):

<source>
$ ./hallo.py
</source>

Bitte beachte das "./" am Anfang. Damit sagt man dem System, es soll die Datei im aktuellen Verzeichnis ausführen und nicht in den Systempfaden (siehe <tt>echo $PATH</tt>) suchen.

==== Fehler korrigieren ====

Wenn das Programm fehlerhaft ist, dann gehe zurück zum Kate-Editor und versuche den Fehler zu finden und zu korrigieren.

=== Nächste Schritte ===

[[Course:Einstieg in die Programmierung mit Python/Programmier-Einstieg/Rechnen / Schleifen|Programmier-Einstieg/Rechnen / Schleifen]]


2. Programmier-Einstieg - Rechnen und Schleifen
-----------------------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Programmier-Einstieg/Rechnen_und_Schleifen


    1 Variablen und Rechnen
    2 Ausgabe mit format
    3 Zählen für Anfänger
    4 Lass den Computer zählen mit einer Schleife
    5 Vorzeitiges Ende mit der if-Anweisung und break
    6 Übung: Quadratzahlen
    7 Verzögerung mit sleep
    8 Übung: Zahlenreihen
    9 (NEU) Übung: Tannenbaum
        9.1 Bonus: Baum mit HTML
    10 Teilen mit Rest
    11 (NEU) Übung: Teilbar ohne Rest?
    12 (NEU) Übung: Verschachtelte Schleifen: das SEHR große 1x1
    13 Bonus: Zufallszahlen und -Buchstaben
        13.1 ASCII
        13.2 Das random-Modul und help

__TOC__
<source>
 ______________________________
< Hey Tux, was lerne ich hier? >
 ------------------------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
                               ________________________________________
                              / Hier geht es um grundlegende           \
                              | Programmierkonzepte. Vor allem die     |
                              | Schleife hat es in sich. Wenn du alles |
                              | durchgearbeitet hast, sollte das aber  |
                              \ kein Problem mehr für dich sein.      /
                               ----------------------------------------
                                 \
                                  \
                                      .--.
                                     |o_o |
                                     |:_/ |
                                    //   \ \
                                   (|     | )
                                  /'\_   _/`\
                                  \___)=(___/

 _______
< Kuhl. >
 -------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

</source>

=== Variablen und Rechnen ===

Tippe folgendes in den kate-Editor:

<source lang=python>
#!/usr/bin/python3

a = 2
print(a)

a = a + 5
print(a)

b = 20
c = a + b
print(c)
</source>

Speichere es in eine Datei ''rechnen.py''.

Wechsle zur Kommandozeile, mache die Datei ausführbar und führe die Datei aus.

=== Ausgabe mit format ===

Wie zuvor: Programmcode abtippen, in eine Datei speichern (entweder die gleiche oder eine neue: ''format.py''), ausführbar machen und ausführen.

<source lang=python>
#!/usr/bin/python3

a = 1234
b = 4
print("a geteilt durch b = {}".format(a / b))
print("{} geteilt durch {} = {}".format(a, b, a / b))
</source>

Verstehst du wie der format-Befehl arbeitet?

=== Zählen für Anfänger ===

Wir zählen bis 5:

<source lang=python>
#!/usr/bin/python3

a = 0
print(a)

a = a + 1
print(a)

a = a + 1
print(a)

a += 1   # += ist eine Abkürzung für a = a + 1
print(a)

a += 1
print(a)

a += 1
print(a)
</source>

'''Aufgabe:''' Zähle auf diese Weise bis 50

(Tipp: die Befehle mit kate oft untereinander kopieren, Datei speichern und Programm auf der Konsole ausführen.)

=== Lass den Computer zählen mit einer Schleife ===

Nun lassen wir den Computer zählen und schauen zu wie schnell er ist. Dazu verwenden wir eine '''Schleife'''.

<source lang=python>
#!/usr/bin/python3

i = 0
while (True):
    print(i)
    i += 1
</source>

Du kannst das Programm mit '''Strg+C''' unterbrechen.

=== Vorzeitiges Ende mit der if-Anweisung und break ===

Damit der Computer nicht zu viel rechnet und wir sehen, was am Anfang passiert, machen wir folgendes:

<source lang=python>
#!/usr/bin/python3

i = 0
while (True):
    print(i)
    i += 1
    if i > 10:
        break
</source>

=== Übung: Quadratzahlen ===
* Anstelle einfach nur zu zählen, lasse den Computer Quadratzahlen ausgeben und zwar die ersten 20: <source>
1
4
9
16
...
</source>
* Verwende die format-Anweisung, um die Ausgabe detaillierter zu gestalten: <source>
1 * 1 = 1
2 * 2 = 4
3 * 3 = 9
4 * 4 = 16
...
</source>

=== Verzögerung mit sleep ===

Hier ein einfaches Beispiel wie man Verzögerungen in sein Programm einbauen kann:

<source lang=python>
#!/usr/bin/python3

import time

i = 0

while (True):
    print("{} Sekunde(n) vergangen.".format(i))
    time.sleep(1.0)
    i += 1
</source>

Man kann auch Bruchteile von Sekunden schlafen, z. B. eine halbe Sekunde mit sleep(0.5). Oder auch länger, z. B. 10 Minuten: sleep(10 * 60).

=== Übung: Zahlenreihen ===

Für die folgenden Rechenübungen kannst du sleep verwenden, damit die Ausgabe zur Kontrolle langsamer ist. Du kannst auch per if und break die Ausgabe vorzeitig abbrechen lassen.

1. Lass den Computer '''gerade''' natürliche Zahlen größer 0 ausgeben. Nehme dazu das Programm aus dem vorigen Abschnitt und schaue, an welcher Stelle du eine Anpassung vornehmen musst.

2. Lass den Computer '''ungerade''' natürliche Zahlen größer 0 ausgeben.

3. Lass den Computer gerade natürliche Zahlen größer '''ab -120''' ausgeben.

4. Lass den Computer Zahlen ausgeben, die '''durch 5 teilbar''' sind.

6. Lass den Computer die Zahlen von 100 bis 1 - also '''in umgekehrter Reihenfolge''' - ausgeben.


=== (NEU) Übung: Tannenbaum ===

Ziel ist es einen Tannenbaum in variabler Größe auszugeben. Ein Baum mit der Größe 7 (= Anzahl der Zeilen, die die Baumkrone bildet) sieht so aus:
<source>
                                *
                               ***
                              *****
                             *******
                            *********
                           ***********
                          *************
                               ***
                               ***
                              *****
</source>

'''1)''' Wir beginnen mit der Baumkrone und '''definieren''' eine '''Funktion:'''

<source lang=python>
def baumkrone(zeile):
    i = 0
    while i < zeile:
        print("*", end='')
        i += 1
    print()
</source>

Der end-Parameter mit dem Leerstring bei der print-Funktion ist notwendig, damit nach einem print keine neue Zeile eingefügt wird.

Die Funktion macht erst dann etwas, wenn man sie aufruft, z. B. so:

<source lang=python>
baumkrone(1)
baumkrone(2)
baumkrone(3)
baumkrone(4)
baumkrone(5)
</source>

Das Ergebnis sollte so aussehen:

<source>
*
**
***
****
*****
</source>

'''1b)''' Ändere die Funktionsaufrufe so, dass das Ergebnis auf dem Kopf steht (also die längste Zeile oben statt unten).

'''2)''' Verändere die Funktion, damit die Anzahl der Sterne dem des Zielbaums entsprechen:

<source>
*
***
*****
*******
*********
</source>

'''3)''' Gib der Funktion einen neuen Parameter "size" (= Größe) und verwende ihn, damit die Baumkronenzeilen zentriert ausgegeben werden, indem vor den Sternen eine passende Anzahl Leerzeichen ausgegeben wird:

<source lang=python>
def baumkrone(zeile, size): # Neuer Parameter 'size'
    i = 0
    while /* ??? */:
        print(" ", end='')
        i += 1

    i = 0
    while i < 2 * zeile - 1:
        print("*", end='')
        i += 1
    print()

baumkrone(1, 7) # Der neue Parameter muss auch beim Aufrufen angegeben werden
baumkrone(2, 7)
baumkrone(3, 7)
baumkrone(4, 7)
baumkrone(5, 7)
</source>

'''4)''' Schreibe eine zusätzliche Funktion '''baum(size)''', die unter Angabe der Baumgröße, die Funktion baumkrone so aufruft, dass verschieden große Tannenbaumkronen ausgegeben werden.

<source lang=python>
def baumkrone(zeile, size):
    /* siehe oben */

def baum(size):
    /* ??? */

# Baumkronen in vier verschiedenen Größen ausgeben:
baum(3)
baum(4)
baum(7)
baum(15)
</source>

'''5)''' Es fehlt noch der Stamm und der Baumstumpf. Ergänze dein Programm, so dass der vollständige Baum ausgegeben wird.

==== Bonus: Baum mit HTML ====

* Lade die '''drei Dateien''' von hier (https://github.com/feinstaub/tp64/tree/master/html/baum) herunter und speichere sie unter ~/ag/htmlbaum/ ab.

* Öffne die '''HTML-Datei''' mit '''Firefox'''.

* Öffne die Datei nun mit ''Kate'' und versuche sie soweit zu verstehen, dass du sie so abändern kannst, dass ein 6x6 großes Schachbrettmuster entsteht.
** Mit der '''Taste F5''' kann man im Firefox die html-Datei neu laden.

* Schreibe dein '''Python-Programm''' aus der vorigen Übung so um, dass es passenden '''HTML-Code ausgibt''', der einen Baum anzeigt.

* Diese Ausgabe kannst du mittels Bash-Dateiumleitung (>) '''in die Datei ''baum.html'' umleiten''' und das Ergebnis mit Firefox prüfen.

* Erstelle eine Baumkrone, die '''50 Zeilen hoch''' ist.

=== Teilen mit Rest ===

Erinnere dich an früher, wo du "Teilen mit Rest" gelernt hast.

Zum Beispiel:

11 geteilt durch 4 ist gleich 2 '''Rest 3''' (weil 2 * 4 = 8 und 8 + 3 = 11)

Beim Programmieren gibt es den Modulo-Operator, der durch das Prozent-Zeichen (%) ausgedrückt wird. Er liefert von zwei gegebenen Zahlen den Rest, wenn man sie durcheinander teilt. Für das Beispiel oben

<source>
>>> 11 % 4
3
</source>

Verwende die interaktive Python-Konsole, um den %-Operator auszuprobieren. Überlege aber vorher selber, was rauskommt.

<source>
0 % 5
1 % 5
2 % 5
3 % 5
4 % 5
5 % 5
6 % 5
</source>

=== (NEU) Übung: Teilbar ohne Rest? ===

Schreibe eine Programm, das die Zahlen von 1 bis 100 durchgeht und ausgibt, ob die jeweilige Zahl durch 7 ohne Rest teilbar ist.

Die Ausgabe soll also so aussehen:
<source>
Folgende Zahlen sind teilbar durch 7 ohne Rest:
  7
  14
  21
  ...
</source>

Verwende dazu unter anderem folgende Anweisungen / Funktionen:
* while-Schleife
* if-Anweisung
* %-Operator
* print-Anweisung


=== (NEU) Übung: Verschachtelte Schleifen: das SEHR große 1x1 ===

Ein guter Freund von dir hat in Mathe nicht aufgepasst und konnte zum wiederholten Mal das kleine 1x1 nicht auswendig aufsagen (zur Erinnerung, siehe https://de.wikipedia.org/wiki/Einmaleins#Tabelle).
Leider bekam er eine Strafarbeit und hat nun die Aufgabe, nicht nur das kleine und Große 1x1 (https://de.wikipedia.org/wiki/Einmaleins#Tabelle, zweite Tabelle), sondern auch noch das SEHR Große 1x1 aufzuschreiben. Also wie das kleine 1x1, nur dass die Zahlen nicht bis 10 gehen, sondern bis 100!

'''1)''' Wenn man alle Zahlen untereinander schreibt, wie viele Zeilen umfasst

'''a)''' das kleine 1x1

'''b)''' das Große 1x1

'''c)''' das SEHR Große 1x1?

Dein Freund hat diese Zahlen eben auch ausgerechnet und ist nun etwas verzweifelt. Wie soll er die Aufgabe lösen, ohne wochenlang nur mit Rechnen und Schreiben beschäftigt zu sein?

<source>
 _______________
< Bitte. Hilfe! >
 ---------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
</source>

'''2)''' Hilf deinem Freund, indem du ein Programm schreibst, dass das SEHR Große 1x1 auf der Konsole ausgibt. Gehe dabei folgendermaßen vor:

'''2a)''' Schreibe ein Programm '''einmaleins.py''', dass das kleine 1x1 ausgibt. So soll der Anfang und das Ende der Ausgabe aussehen:

<source>
1 x 1 = 1
1 x 2 = 2
1 x 3 = 3
1 x 4 = 4
1 x 5 = 5
1 x 6 = 6
1 x 7 = 7
1 x 8 = 8
1 x 9 = 9
1 x 10 = 10
2 x 1 = 2
2 x 2 = 4
...
9 x 8 = 72
9 x 9 = 81
9 x 10 = 90
10 x 1 = 10
10 x 2 = 20
10 x 3 = 30
10 x 4 = 40
10 x 5 = 50
10 x 6 = 60
10 x 7 = 70
10 x 8 = 80
10 x 9 = 90
10 x 10 = 100
</source>

TIPP 1: Verwende eine verschachtelte Schleife, also eine äußere und eine innere Schleife:
<source>
# ...
while (...):
    # ...
    while (...):
        # ...
    # ...
</source>

TIPP 2: Verwende die sleep-Funktion, wenn du in Zeitlupe sehen willst wie das Programm abläuft.

'''2b)''' Ändere das Programm so, dass es das Große 1x1 ausgibt (Zahlen von 1 bis 20).

'''2c)''' Jetzt sollte es leicht sein, auch noch das SEHR Große 1x1 auszugeben.

(Psst. Nicht verraten, dass es am Ende gar nicht so schwer war.)

'''3)''' BONUS: Um die Ausgabe deines Programms bequem deinem Freund schicken zu können, wäre es praktisch, wenn die Ausgabe in einer Datei stehen würde. Das geht mit der bash-Shell so (ohne das $-Zeichen):

<source>
$ ./einmaleins.py > tabelle.txt
</source>

Die Datei '''tabelle.txt''' wird erzeugt (oder - falls schon vorhanden - überschrieben). Öffne die Datei mit kate und schaue sie dir an.

=== Bonus: Zufallszahlen und -Buchstaben ===

==== ASCII ====
Probiere folgendes aus (die Kommentare zeigen jeweils das Ergebnis):

<source lang=python>
ord('A')  # 65
ord('B')  # 66
ord('a')  # 97

chr(65)  # 'A'
chr(66)  # 'B'
chr(97)  # 'a'
</source>

'''ord''' liefert also zu einem Buchstaben eine Zahl und '''chr''' zu einer Zahl einen Buchstabe.

Warum genau diese Zahlen und keine anderen? Weil das so in der '''ASCII-Tabelle''' steht: https://de.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange#ASCII-Tabelle

'''Übung:''' Suche die Buchstaben deines Namens aus der ASCII-Tabelle heraus und baue eine Zeichenkette zusammen, indem du mehrere chr-Ergebnisse mit plus verknüpfst. Zum Beispiel <tt>chr(65) + chr(66) + chr(67) == "ABC"</tt>

==== Das random-Modul und help ====

Öffne eine interaktive Python3-Shell und versuche folgendes nachzuvollziehen:

Wir brauchen das Modul random. Schauen, ob es schon da ist:
<source>
>>> random
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'random' is not defined
</source>

Scheint nicht so. Also importieren wir es:
<source>
>>> import random
</source>

Ist es jetzt da?
<source>
>>> random
<module 'random' from '/usr/lib64/python3.4/random.py'>
</source>

Ja, ist es. Neugierige können diese Datei einmal öffnen und sehen wie Original-Python-Bibliotheks-Code ausschaut.

Die Funktion randint aus dem Modul random verwenden:
<source>
>>> random.randint(1, 10)
7
>>> random.randint(1, 10)
3
>>> random.randint(1, 10)
8
</source>

# hilfe:py:randint

random.randint(1, 10) liefert Zufallszahlen zwischen 1 und 10. Aber ist 1 und 10 inklusive oder nicht? Rufe dazu die eingebaute Hilfe auf:

<source>
>>> help(random.randint)

Help on method randint in module random:

randint(a, b) method of random.Random instance
    Return random integer in range [a, b], including both end points.
</source>

Den Hilfeanzeigemodus kannst du mit der Taste '''q''' wieder verlassen.

Arbeite jetzt wieder mit Dateien:

'''Übung 1:''' Schreibe ein Programm, das zeilenweise Zufallszahlen zwischen -100 und 100 ausgibt.

'''Übung 2:''' Schreibe ein Programm, das zeilenweise Zeichenketten von je 10 Zufallsbuchstaben ausgibt, also z. B.: <source>
lqZYWUiHxj
UhoniELbgo
zBvObSTGfv
QjRJngcfuT
...
</source>

(todo später: https://github.com/feinstaub/tp64/blob/master/sample-code/rand-chr.py: for in range)


2. Programmier-Einstieg - grep
------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Programmier-Einstieg/grep


    1 In der Standard-Ausgabe suchen
    2 Beispiele / Zufallszeichenketten
        2.1 Übung
        2.2 Die Prozessorauslastung anschauen
        2.3 Bonus: Reguläre Ausdrücke
    3 Für zuhause: Sprachausgabe
    4 Interne Anmerkung

__TOC__

=== In der Standard-Ausgabe suchen ===

Die Standard-Ausgabe ist das, was du mit dem print-Befehl ausgibst. Auch andere Shell-Programme schreiben auf die Standard-Ausgabe (auch '''standard out''' genannt), z. B. der folgende Befehl listet alle Pakete auf, die das Wort 'test' enthalten:

<source>
zypper se test
</source>

Probiere es aus.

Mit dem '''Pipe'''-Symbol (|) kann man einen Shell-Ausgabestrom in ein anderes Programm umleiten. Ein Programm, das man oft auf die rechte Seite der Pipe schreibt ist '''grep''', ein Suchprogramm.

Beispiel:

<source>
zypper se test | grep samba
</source>

Jetzt werden nur noch Zeilen ausgegeben, die das Wort samba enthalten. Außerdem wird das Wort hervorgehoben.

=== Beispiele / Zufallszeichenketten ===

Hier ein Programm, dass Zufallszeichenketten ausgibt:

<source lang=python>
#!/usr/bin/python3

import random

i = 0
while (True):
    s = ""
    for a in range(0, 20):
        c = random.randint(65, 122)
        s += chr(c)
    print(str(i) + ":" + s)
    i += 1
</source>

Speichere es in der Datei '''rand-chr.py''' und lass das Programm laufen.

Dann probiere der Reihe nach folgende Zeilen aus und schaue, was passiert:

<source>
./rand-chr.py | grep 0000
./rand-chr.py | grep -i gre
</source>

==== Übung ====
* Suche mit grep nach deinem Namen in den Zufallsbuchstaben (nur die ersten 3 Buchstaben, sonst könnte es länger dauern)

==== Die Prozessorauslastung anschauen ====

Außer dem Strg+C kannst du ein Programm auch anders beenden: siehe KSysGuard.

==== Bonus: Reguläre Ausdrücke ====

(TODO: erklären was reguläre Ausdrücke sind)

<source>
./rand-chr.py | grep '\(abc\)\|\(def\)'
./rand-chr.py | grep -i -E '(abc)|(def)'
./rand-chr.py | grep -E '([abcdefg]abc)|(def.*a)'
</source>

=== Für zuhause: Sprachausgabe ===

Installiere das Programm '''espeak'''.

Schreibe ein kleines Zähl-Programm, das Zahlen ausgibt und speichere es in der Datei '''counter.py'''.

<source lang=python>
#!/usr/bin/python3

i = 0
while (True):
    print(i)
    i += 1
</source>

Probiere folgende Befehle aus:

<source>
./counter.py | espeak
./counter.py | espeak -vde
./counter.py | grep --line-buffered 1111 | espeak -vde
</source>


=== Interne Anmerkung ===

* grep scheint nicht zu funktionieren, wenn das Python-Programm sleep verwendet, um die Ausgabegeschwindigkeit zu drosseln. Warum? -> Lösung: import sys, sys.stdout.flush()


3. FAQ - Die Kommandozeile
--------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/FAQ/Die_Kommandozeile


    1 Kommandozeile starten
    2 Yakuake installieren
    3 Einstieg
        3.1 Erste Befehle absetzen
        3.2 Konvention: das $-Prompt
        3.3 Übung
    4 Dateien und Verzeichnisse
        4.1 Weiteres Material
    5 Erweitert
        5.1 Was ist eine Shell?
        5.2 Historie abrufen
            5.2.1 Strg+R
            5.2.2 $ history
        5.3 Tastenkürzel
            5.3.1 Bash
            5.3.2 Konsole / Yakuake
        5.4 Konsole in Dolphin
        5.5 Aktuelles Verzeichnis mit Dolphin öffnen
        5.6 Standardeingabe und -ausgabe
            5.6.1 mit Python
        5.7 Wo sind die Befehle? (which, $PATH)
        5.8 Überblickslisten
            5.8.1 😃 The Art of Command line (DE)
            5.8.2 Veraltete Konstrukte
    6 Bash-Fun
        6.1 cool-retro-term
        6.2 no-more-secrets
        6.3 Zusammenpipen
        6.4 Hacker-Spiel (engl., 2016)

Wenn man unter GNU/Linux mit der '''Kommandozeile''' Befehle absetzen möchte, dann kommt üblicherweise die [https://de.wikipedia.org/wiki/Bash_%28Shell%29 Bash] zum Einsatz. Die Bash ist eine '''UNIX-Shell'''. Man sagt auch '''Konsole''' oder '''Kommandozeile''' dazu.

Die folgenden Aufgaben zeigen, wie man die Bash startet und wie sie bedient wird. Wenn im Folgenden von 'Kommandozeile' gesprochen wird, dann ist die Bash gemeint.

__TOC__

== Kommandozeile starten ==
Unter KDE/Plasma:
* Alt+Leertaste drücken (-> Die '''krunner'''-Leiste öffnet sich am oberen Bildschirmrand)
* ''konsole'' eingeben und das Suchergebnis entweder mit der Maus anklicken oder die ENTER-Taste drücken.

Es öffnet sich ein Fenster mit schwarzem Hintergrund und hellem Text, z. B.:

 spiderman@linux-host1:~>

Diesen Text nennt man '''Prompt''', also Eingabeaufforderung. Dahinter ist ein helles Rechteck zu sehen, das anzeigt, wo die Buchstaben erscheinen, die der Benutzer mit der Tastatur eingibt.

Das Prompt setzt sich im Einzelnen so zusammen:

* '''spiderman''': Der Benutzername des eingeloggten Benutzers
* '''@''' (at-Zeichen): Trennzeichen
* '''linux-host1''': der Rechnername
* ''':''' (Doppelpunkt): Trennzeichen
* '''~''' (Tilde): Das aktuelle Verzeichnis. Die Tilde steht für das Home-Verzeichnis ("Persönlicher Ordner") des eingeloggten Benutzers.
* '''>''' (Größer-als-Zeichen): Trennzeichen
* '''Cursor''': hier werden Benutzereingaben eingefügt

== Yakuake installieren ==
* '''yakuake''' ist eine Konsole, die man mit der F12-Taste öffnen kann und die immer im Hintergrund bereit ist.
* Installiere sie mittels zypper ([[Course:Einstieg in die Programmierung mit Python/FAQ/Software installieren|Wie geht das?]]).
* yakuake einmalig mit krunner (Alt+Leertaste, ''yakuake'' eingeben, ENTER) starten.
* Ab jetzt kann yakuake mit der Taste '''F12''' geholt und mit nochmaligem Druck wieder versteckt werden.

== Einstieg ==
* Neue Zeichen, die über die Tastatur eingegeben erscheinen immer dort, wo sich der Cursor (ausgefülltes Rechteck) befindet
* Befehle werden immer '''zeilenweise''' eingegeben und jeweils mit der ENTER-Taste abgeschlossen.

=== Erste Befehle absetzen ===
* Öffne eine Kommandozeile (siehe oben). Es erscheint die Eingabeaufforderung, die so ähnlich aussieht: <source>user@linux-85rg:~></source>
* Gebe ''date'' über die Tastatur ein: <source>user@linux-85rg:~> date</source>
* Drücke ENTER. Das Ergebnis sieht ungefähr so aus:<source>user@linux-85rg:~> date
So 2. Okt 20:26:40 CEST 2016</source>
* Glückwunsch! Du hast den ersten Kommandozeilen-Befehl abgesetzt.

* Drücke nun Taste "Cursor hoch" ([https://de.wikipedia.org/wiki/Pfeiltaste das ist die obere Pfeiltaste aus dem Pfeiltastenblock auf der Tastatur]) => der letzte Befehl erscheint wieder.
* Drücke ENTER => Der Befehl wird erneut ausgeführt.

=== Konvention: das $-Prompt ===

Wenn im weiteren Verlauf des Kurses ein Bash-Befehl abgesetzt werden soll, dann wird das kenntlich gemacht, indem vor dem Befehl ein $-Zeichen steht. Also z. B.

<source>$ date</source>

bedeutet, dass du wie im vorigen Abschnitt beschrieben den date-Befehl ausführen sollst (das $-Zeichen gehört also ''nicht'' zum Befehl dazu).

=== Übung ===
* Führe den Befehl <source>$ ls</source> aus. => Das aktuelle Verzeichnis wird aufgelistet.
* Führe den Befehl <source>$ ls -la</source> aus. => Das aktuelle Verzeichnis wird ausführlicher aufgelistet.


 ------------- STOPP --------------

Wenn du von der Primzahlenaufgabe hierher kamst, dann bist du mit dieser Seite erst einmal fertig und kannst wieder zurückgehen und dort weitermachen.

 ------------- STOPP --------------

== Dateien und Verzeichnisse ==

* Umgang mit Dateien: http://www.selflinux.org/selflinux/html/dateien_unter_linux01.html

* Umgang mit Verzeichnissen: http://www.selflinux.org/selflinux/html/verzeichnisse_unter_linux02.html

=== Weiteres Material ===
* Linux-Verzeichnisstruktur: http://www.selflinux.org/selflinux/html/verzeichnisse_unter_linux01.html
** siehe auch https://de.wikipedia.org/wiki/Filesystem_Hierarchy_Standard
* Command line crash course: https://learnpythonthehardway.org/book/appendixa.html

== Erweitert ==

=== Was ist eine Shell? ===
* Lese dir das hier durch: [http://www.selflinux.org/selflinux/html/was_ist_shell01.html SelfLinux - Was ist eine Shell?]

* Die Bash ist eine [https://de.wikipedia.org/wiki/Unix-Shell Unix-Shell]. Eine Unix-Shell ist einer von vielen [https://de.wikipedia.org/wiki/Kommandozeileninterpreter Kommandozeileninterpretern], wie z. B. der [https://docs.python.org/3/tutorial/interpreter.html Python-interpreter].

* Die Kommandozeile ist einzeln, aber auch in vielen Programmen verfügbar, um schnell System-Befehle absetzen zu können. Die Funktionsweise ist dabei immer die gleiche.
** Konsole (einzeln als Programm)
** Yakuake (Taste F12)
** in Dolphin (Taste F4)
** in Kate (als Terminal-Plugin)

=== Historie abrufen ===

==== Strg+R ====
* Mit den Cursor-Tasten 'rauf' und 'runter' kann man sich vorher eingegebene Befehle wieder holen.
* Mit '''Strg+R''' kann man in der Historie suchen:

1. Strg+R drücken
2. Worte eingeben
3. So lange Strg+R drücken, bis die gewünschte Befehlszeile gefunden wurde.

==== $ history ====
Die Strg+R-Methode ist bei wenig Übung etwas hakelig. Folgendes geht auch:

<source>
$ history
</source>

oder direkt nach etwas suchen (z. B. nach "cowsay"):

<source>
$ history | grep cowsay
</source>

=== Tastenkürzel ===

==== Bash ====
* Strg+A - An den Anfang der Zeile
* Strg+E - Ans Ende der Zeile
* Strg+K - Alles rechts vom Cursor löschen
* Strg+A K - Ganze Zeile löschen
* Siehe http://www.selflinux.org/selflinux/html/bash_basic02.html#d27e146

==== Konsole / Yakuake ====
* Strg+Umschalt+T - '''Neue Session''' (= neuen Tab) öffnen
* Umschalt+Cursor rechts/links - Zwischen '''Sessions umschalten'''
* '''Geteilte Ansicht (split view)''': rechte Maustaste auf Session-Name -> "Split Left/Right"

=== Konsole in Dolphin ===
* Dolphin öffnen
* F4 drücken
* cd und ls verwenden und beobachten wie sich die Ordner-Ansicht oben synchronisiert.
* In der Ordner-Ansicht navigieren und beobachten wie sich die Konsole synchronisiert.

=== Aktuelles Verzeichnis mit Dolphin öffnen ===
<source>
$ xdg-open .
</source>

=== Standardeingabe und -ausgabe ===
* TODO: Crash-Kurs
* cowsay
** Etwas schreiben und mit Strg+D beenden
* create file aaa:
c      2
b      1
a      3
* sort < aaa
* sort -k 1 < aaa
* etc.

==== mit Python ====
todo:
* http://stackoverflow.com/questions/1450393/how-do-you-read-from-stdin-in-python
* https://en.wikibooks.org/wiki/Python_Programming/Input_and_Output

=== Wo sind die Befehle? (which, $PATH) ===
* todo:
** which
** $PATH
** ./
** rpm -qf `which qmlscene`

=== Überblickslisten ===
* https://github.com/alebcay/awesome-shell (Tipps für Programme und Listen von Tutorials; todo: durchgehen)

==== 😃 The Art of Command line (DE) ====
Ein sehr kompakter Fundus an Wissen:
* https://github.com/jlevy/the-art-of-command-line/blob/master/README-de.md

==== Veraltete Konstrukte ====
Hinweise, was nicht mehr verwendet werden sollte:
* http://wiki.bash-hackers.org/scripting/obsolete

== Bash-Fun ==

=== cool-retro-term ===
* Paket cool-retro-term installieren und starten.

===  no-more-secrets ===
* Paket no-more-secrets installieren.
* $ sneakers
* $ ls -la | nms

=== Zusammenpipen ===
Im cool-retro-term folgendes aufrufen:
<source>
$ cowsay Hallo | nms
</source>

=== Hacker-Spiel (engl., 2016) ===
Mache dich mit den Befehlen ls, cd und cat vertraut (siehe oben "Dateien und Verzeichnisse"). Wisse, was eine versteckte Datei ist. Dann bist du bereit für das Hackerspiel: siehe https://engineering.pinterest.com/blog/hack-pinterest. Die erste Aufgabe ist, aus dem Artikel herauszulesen, wie man das Spiel aufruft. *g*



3. FAQ - Aufgaben für Zwischendurch
--------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/FAQ/Aufgaben_f%C3%BCr_Zwischendurch

    1 Generell
    2 GNU/Linux installiert (und Internet vorhanden)
        2.1 Online-Kurse
    3 GNU/Linux installiert, aber kein Internet vorhanden
        3.1 Eigenes System erkunden
    4 GNU/Linux noch nicht installiert, aber Internet vorhanden
        4.1 Online-Python-Shells
        4.2 Online-Tutorials
    5 GNU/Linux noch nicht installiert und kein Internet vorhanden
    6 Hinweis zu deinem eigenen Projekt

__TOC__

Beantwortet die Frage: "Was soll ich als nächstes machen?", falls der Lehrer gerade nicht verfügbar ist.

=== Generell ===
* Situation: "Nichts zu tun", "Mir fällt nichts besseres ein als ein Computerspiel zu spielen"
* Lösung: Hilf deinem Teamkollegen oder schaue, was andere gerade tun und versuche von ihnen zu lernen oder zu helfen

=== GNU/Linux installiert (und Internet vorhanden) ===

==== Online-Kurse ====
* [http://opentechschool.github.io/python-beginners/de/index.html Programmiereinführung mit '''Python 3 von opentechschool.org''']
* Das '''Python-Tutorial''' (Python 3.3): https://py-tutorial-de.readthedocs.io/de/python-3.3/
** '''Beginne hier''': https://py-tutorial-de.readthedocs.io/de/python-3.3/introduction.html

* Schaue dir diesen Python3-Online-Kurs an: http://www.python-kurs.eu/python3_kurs.php. Links ist ein Menü, mit dem auf die Kursseiten kommt.
** z. B. Abschnitt zu '''Schleifen:''' http://www.python-kurs.eu/python3_schleifen.php

* Python 2.7 (Achtung, wir verwenden hier Python 3) unter Linux: https://de.wikibooks.org/wiki/Python_unter_Linux

* Schaue dir den Online-Kurs "Python - The hard way" an und arbeite daran: https://learnpythonthehardway.org/book/ (Englisch, leider nur Python 2)

=== GNU/Linux installiert, aber kein Internet vorhanden ===

==== Eigenes System erkunden ====

[[Course:Einstieg in die Programmierung mit Python/Eigenes System erkunden/Vorhandene Programme|Eigenes System erkunden - Vorhandene Programme]]

Dies kannst du auch gerne außerhalb dieses Kurses tun.

=== GNU/Linux noch nicht installiert, aber Internet vorhanden ===

==== Online-Python-Shells ====
* '''Einfache Python-Shell''': https://www.python.org/shell/
* '''Python-Interpreter mit Code- und Ausgabe-Fenster''': https://repl.it/languages/python3
* Codeverlauf visualisieren
** http://www.pythontutor.com/visualize.html
** http://www.pythontutor.com/live.html

==== Online-Tutorials ====

* Einführung in Javascript (nur auf Englisch, mit Kopfhörern): https://www.khanacademy.org/computing/computer-programming/programming

=== GNU/Linux noch nicht installiert und kein Internet vorhanden ===

* Schaue, was auf dem Schulrechner alles installiert ist
* Schaue bei anderen zu
* Überlege dir ein Programm, dass du programmieren möchtest und arbeite mit Zettel und Stift
* Lese ein Buch


=== Hinweis zu deinem eigenen Projekt ===

Die Konzepte, die wir hier lernen, sind bei allen (imperativen) Programmiersprachen ähnlich. Das heißt, wenn du schon ein eigenes Projekt (Programm, Spiel etc.) im Hinterkopf hast, kannst du dir im Verlauf schon mal überlegen, was dafür nützlich sein könnte.


3. FAQ - Weitere Fragen
-----------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/FAQ/Weitere_Fragen


    1 Schriftarten finden und installieren
    2 Linux
        2.1 Was ist der Vorteil von Linux?
            2.1.1 Vorhandenes Material
            2.1.2 Alternativ: selber zusammenführen
    3 Internet
        3.1 Was ist ein Cookie?
        3.2 Was ist mit digitaler Musik?
        3.3 P2P am Beispiel openSUSE-DVD-BitTorrent-Download

Fragen, die von den Kurs-Teilnehmern gestellt wurden.

__TOC__

== Schriftarten finden und installieren ==
Siehe hier: https://github.com/feinstaub/tp64/tree/master/tutorials/schriftarten-beginner-opensuse

== Linux ==
=== Was ist der Vorteil von Linux? ===

Annahme: Es geht um einen Vergleich mit anderen üblicherweise verwendeten Betriebssystemen (wie Windows oder MacOS).

Um diese Frage zu klären, ist Hintergrundwissen nötig.

==== Vorhandenes Material ====

* Was ist Linux? Und was hat das mit GNU zu tun?
** Einfacher Überblick http://www.sides.de/linux_wasist.php
** Mehr: https://de.wikipedia.org/wiki/Linux

* Vorteile
** http://www.sides.de/linux_vorteile.php
** http://www.bretschneidernet.de/linux/advantages.html
** Eigenkontrolle über private Datenverarbeitung
** Programmcode einsehbar; Möglichkeit den Code zu verstehen und nach eigenen Wünschen anpassen (zu lassen)

* Nachteile
** http://www.sides.de/linux_nachteile.php

* Mehr Lernen?
** http://www.sides.de/linux_vorwissen.php

* Wo wird Linux eingesetzt?
** http://www.bretschneidernet.de/linux/index.html
*** Serverfarmen im Internet
*** Basis des Android-Betriebssystems
*** im akademischen Umfeld und Forschungseinrichtungen
** In dieser AG: [[Course:Einstieg in die Programmierung mit Python/Org_Meta/Meta1]]

==== Alternativ: selber zusammenführen ====

(Sicht des Privatanwenders)

1. '''Privatsphäre:''' Finde heraus, was [https://de.wikipedia.org/wiki/Privatsph%C3%A4re Privatsphäre] mit [https://de.wikipedia.org/wiki/Demokratie Demokratie] zu tun hat. Frage dazu auch den Politik- oder Ethik-Lehrer. Schaue dir einen [https://de.wikipedia.org/wiki/Privatsph%C3%A4re#In_Film_und_Literatur dieser Filme an oder lese ein Buch] dazu. Informiere dich über die [https://de.wikipedia.org/wiki/%C3%9Cberwachung#Risiken_der_Personen.C3.BCberwachung Risiken der Personenüberwachung]. Ist Privatsphäre verzichtbar oder ein wichtiges Gut?

2. '''Computer, Software und Internet:''' Finde heraus, was man mit Software und Hardware alles machen kann. Frage dazu jemanden, der sich mit IT-Technik auskennt. Schaue dir dazu an, welche (sichtbaren) Komponenten dein Computer (oder Smartphone) hat, z. B. Datenspeicher für eigene Gedanken, Kamera, Mikrofon, Anschluss zum Internet. Schaue dir außerdem an, wo diese Gerät überall herumstehen oder herumgetragen werden. Sieht der Computernutzer oder die umstehenden Personen dem Gerät an, was es gerade macht? Wie wichtig ist es, der darauf laufenden Software vertrauen zu können?

3. '''Vertrauen:''' Was bedeutet [https://de.wikipedia.org/wiki/Vertrauen Vertrauen]? Frage dazu deinen Politik-, Ethik- oder Religions-Lehrer. Schaue dir an, wie Vertrauen in das Verhalten von Software mit Hilfe der Konzepte von [https://de.wikipedia.org/wiki/Freie_Software Freier Software] erreicht werden kann.

4. '''Zusammenführung:''' Schaue dir an, was ein [https://de.wikipedia.org/wiki/Betriebssystem Betriebssystem] ist. Frage dazu auch jemanden, der sich mit IT-Technik auskennt. Stelle fest, dass eine [https://de.wikipedia.org/wiki/Linux-Distribution Linux-Distribution] ein Betriebssystem ist, mit dem es möglich ist, einen Computer (bei Smartphones siehe z. B. [https://de.wikipedia.org/wiki/Replicant_(Betriebssystem) Replicant]) mit freier Software zu betreiben und ziehe deine eigenen Schlüsse, ob und welche Vorteile die Verwendung von GNU/Linux hat. Hat jemand Nachteile?

== Internet ==

=== Was ist ein Cookie? ===

Hier die Antwort aus der Wikipedia: https://de.wikipedia.org/wiki/HTTP-Cookie (unter anderem wird auf HTTP- und Javascript-Cookie-Methoden eingegangen).

Um Cookies wirklich zu verstehen, sollte man sich mit HTTP, HTML und Javascript beschäftigen. Ein guter Startpunkt ist die SELFHTML-Seite: https://wiki.selfhtml.org/wiki/Startseite und dort am besten das Schreinerei-Tutorial:

https://wiki.selfhtml.org/wiki/HTML/Tutorials/HTML-Einstieg

Speziell zu Cookies:
* https://wiki.selfhtml.org/wiki/Glossar:Cookie
* https://wiki.selfhtml.org/wiki/JavaScript/Anwendung_und_Praxis/cookies
* http://www.w3schools.com/js/js_cookies.asp
* Moderne Alternative: Web Storage API:
** https://wiki.selfhtml.org/wiki/JavaScript/Web_Storage

Weiterführende Aufgaben:
* [http://php.net/ PHP installieren] und einfache Webanwendungen programmieren, z. B. Nutzername merken oder das Ergebnis der letzten Berechnung. Üblicherweise für Warenkörbe verwendet.

=== Was ist mit digitaler Musik? ===

Musik im digitalen Zeitalter ist eine komplexe Angelegenheit, die sich im '''Spannungsfeld''' folgender Aspekte abspielt:

* Privatsphäre des Nutzers
* IT-Sicherheit auf Seiten des Nutzers
* Private oder kommerzielle Nutzung
* Urheberrechte der Künstler
* Kommerzielle Interessen der Musikindustrie; Geschäftsmodelle
* Rechteverwertung (GEMA)

Folgende Punkte kann man sich hierbei im Einzelnen anschauen:

* Ein Beispiel aus der Vergangenheit: '''Musik aus dem Radio auf Kassette aufzeichnen.''' Dazu folgende Lesetipps, die sich auch auf aktuelle digitale Musik beziehen:
** [http://www.hardwarejournal.de/musik-radio-aufnehmen-erlaubt.html Darf man Musik aus dem Radio aufnehmen?]
** Abschreckungskampagne aus dem Jahr 1980: [https://de.wikipedia.org/wiki/Home_Taping_Is_Killing_Music Home Taping Is Killing Music]

* Was ist eine '''Verwertungsgesellschaft'''?
** [https://de.wikipedia.org/wiki/Gesellschaft_f%C3%BCr_musikalische_Auff%C3%BChrungs-_und_mechanische_Vervielf%C3%A4ltigungsrechte GEMA]
** [https://de.wikipedia.org/wiki/Pauschalabgabe#Aktuelle_S.C3.A4tze_in_Deutschland Pauschalabgabe] (Zuschlag auf den Preis von Geräten oder Medien zum Anfertigen oder Speichern von Vervielfältigungen)

* '''Musik-Streaming'''
** Flat-Rates
** Wer aus [https://stallman.org/spotify.html persönlichen oder gemeinnützigen Erwägungen z. B. kein Spotify] verwenden möchte, der kann YouTube verwenden (siehe nächster Punkt) und/oder in Kombination mit einem eigenen Streaming-Server für zuhause, z. B.:
*** [http://www.fomori.org/cherrymusic/ Cherry­Music] - "it is intended to be an alternative to Last.fm, Spotify, Grooveshark"

* '''YouTube'''
** https://de.wikipedia.org/wiki/YouTube
** "Ein Mitschnitt der Tonspur von offiziell eingestellten Musikvideos als "Privatkopie" ist erlaubt." - sagt die Verbraucherzentrale Bremen e.V., [http://www.legal-box.de/musik/index.php?contentid=20 siehe hier].
** [http://www.echo-online.de/ratgeber/internet-und-elektronik/netzwelten/youtube-und-gema-einigen-sich-tausende-musikvideos-freigeschaltet_17438075.htm Echo-Artikel zur Einigung zwischen GEMA und Youtube], 2016

* '''Rechtliches'''
** Rechtliche Auseinandersetzungen am Beispiel Peer-to-Peer-Filesharing
*** https://de.wikipedia.org/wiki/Filesharing#Rechtliche_Auseinandersetzungen_um_Filesharing
*** https://de.wikipedia.org/wiki/Filesharing#Die_zivilrechtliche_Haftung
** [http://www.digitalwelt.org/themen/raubkopierer-sind-verbrecher/privatkopie-vs-raubkopie Privatkopie vs. Raubkopie - Wann man eine legale Kopie herstellen darf]
** [https://de.wikipedia.org/wiki/Privatkopie Privatkopie]
** [https://de.wikipedia.org/wiki/Fair_Use Fair Use]
** [https://en.wikipedia.org/wiki/Radio_music_ripping#Legal_issues Radio music ripping: Legal issues]

* '''Technik'''
** Internet, Internet Service Provider, IP-Adresse, Welche Akteure sehen welche Benutzer-Aktivitäten? Web-Technik, Web-Browser, Download, Upload, P2P-Systeme, Musik-Formate, Kompressionsverluste

* '''Leitfaden'''
** Das Abspielen von digitaler Musik sollte komplett mit Hilfe von [https://de.wikipedia.org/wiki/Freie_Software freier Software] möglich sein, um die Privatsphäre des Nutzers nicht zu beeinträchtigen.
*** Insbesondere im Zusammenhang von [https://de.wikipedia.org/wiki/Digitale_Rechteverwaltung DRM] und [https://de.wikipedia.org/wiki/Musikstreaming Musikstreaming] darauf achten.
** Rechtliche Themen können aufgrund verschiedener Interessenskonflikte hier nicht abschließend bewertet werden.
*** Ein Einstieg in das Thema bietet dieses Buch: [http://freie-software.bpb.de Freie Software - Zwischen Privat- und Gemeineigentum von Volker Grassmuck], Abschnitt '''"Rechtliche Herausforderungen durch Digitalmedien"'''.

=== P2P am Beispiel openSUSE-DVD-BitTorrent-Download ===
# https://software.opensuse.org/422/en besuchen
# BitTorrent-Link des openSUSE-DVD-Downloads auswählen
# Wird mit KTorrent geöffnet
# Warten, bis Download startet
# Peers-Tab öffnen und sehen, von welchen anderen Rechnern (inklusive Landesflagge) der eigene Rechner Daten herunterlädt werden.
# Wenn Download fertig, im Peers-Tab beobachten wie immer wieder andere Rechner aufblitzen, die Teile der Datei vom eigenen Rechner laden.


4. Eigenes System - GNU,Linux installieren
------------------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Eigenes_System/GNU,Linux_installieren

    1 In VirtualBox einbinden
    2 Auf USB-Stick
    3 Auf eingebauter Festplatte
        3.1 Als einziges Betriebssystem
        3.2 Als Dual-Boot-Setup

Es gibt verschiedene Möglichkeiten GNU/Linux zu installieren und zu verwenden.

Wir verwenden openSUSE Linux.

__TOC__

== In VirtualBox einbinden ==
* siehe http://feinstaub.github.io/arbeitsblatt.net/arbeitsblatt-linux-image-virtualbox.html
* (Hinweis: das vdi-image auf einen Stick sichern und dann wieder auf den Rechner kopieren hat nicht geklappt)
* (Hinweis: immer ein openSUSE-vdi-Image bereithaben, '''falls die Schulinstallation nicht funktioniert'''; auf DVD oder provide by Droopy)

== Auf USB-Stick ==
Genau Anleitung hier:

https://github.com/feinstaub/tp64/tree/master/anleitungen/linux-auf-usb-stick-installieren

== Auf eingebauter Festplatte ==
Allgemein: siehe http://feinstaub.github.io/arbeitsblatt.net/arbeitsblatt-linux-eigener-rechner.html

=== Als einziges Betriebssystem ===
Empfohlen, weil Dual-Boot-Installationen manchmal schwierig einzurichten sind.

=== Als Dual-Boot-Setup ===
Tipps:
* http://opensuse-guide.org/installation.php
* https://tweakhound.com/2015/11/04/dual-boot-opensuse-leap-and-windows-10-uefi/

Mögliche Probleme:
* Das vorhandene Windows startet nicht mehr.
** Lösung: Windows-CD/DVD erforderlich.
* Gar nichts startet mehr.
** Lösung: Windows- oder LInux-DVD erforderlich.

Hinweis:
* Wenn das System einmal erfolgreich installiert ist, gibt es normalerweise keine Probleme.
* Die treten dann eher wieder bei Neuinstallation von Windows oder GNU/Linux auf.


4. Eigenes System - Software finden und installieren
----------------------------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Eigenes_System/Software_finden_und_installieren

    1 Einstieg
        1.1 Arbeitsblatt
    2 Weiteres
        2.1 Mehrere Pakete auf einmal installieren
        2.2 Mehr Informationen zu zypper
    3 Befehlsübersicht
        3.1 zypper
        3.2 Allgemein
    4 Spezial-Infos
        4.1 Auto-Refresh deaktivieren
        4.2 Neueste KDE/Plasma-Versionen installieren
            4.2.1 Konsistentes Update mit zypper
            4.2.2 Verschiedenes
        4.3 Multimedia-Codes
        4.4 Repositories aufräumen
        4.5 Quellcode herunterladen
            4.5.1 Troubleshooting

__TOC__

== Einstieg ==
=== Arbeitsblatt ===
Zum Einstieg arbeite das [[Course:Einstieg in die Programmierung mit Python/Arbeitsblätter/Software installieren|Arbeitsblatt Software installieren]] durch.

== Weiteres ==
=== Mehrere Pakete auf einmal installieren ===

Manchmal kommt es vor, dass man mehrere Pakete auf einmal installieren möchte. Dazu werden die Paketnamen mit Leerzeichen getrennt hintereinander geschrieben, z. B.:

<source>sudo zypper install kate cowsay chess</source>

=== Mehr Informationen zu zypper ===

* Offizielle deutsche Hilfeseite: https://de.opensuse.org/SDB:Zypper_benutzen
** enthält u. a. auch Cheat-Sheets zum Ausdrucken (englisch)

== Befehlsübersicht ==

=== zypper ===

{| class="wikitable"
|-
! Befehl !! Beschreibung
|-
| zypper install <paket_name> || Paket installieren. Es können auch mehrere Pakete mit Leerzeichen getrennt angegeben werden.
|-
| zypper in <paket_name> || Kurzform für zypper install
|-
| zypper install --details <paket_name> || Zeigt Details zur Version und zum verwendeten Repository an, bevor installiert wird
|-
| zypper remove <paket_name> || Paket deinstallieren
|-
| zypper search <text> || In Paketnamen suchen
|-
| zypper se <text> || Kurzform für zypper search
|-
| zypper info <paket_name> || Informationen (Version, Beschreibung) über ein Paket anzeigen. Zeigt auch Hinweise an, aus welcher Quelle ein Paket installiert wurde ("Vendor: ...")
|-
| --- || ---
|-
| rpm -ql <paket_name> || Listet alle Dateien auf, die durch dieses Paket installiert wurden.
|-
| rpm -qf <datei_pfad> || Zeigt das Paket an, das für die Installation der Datei verantwortlich ist.
|-
| --- || ---
|-
| zypper lp || Zeigt verfügbare Patches an.
|-
| zypper lu || Zeigt verfügbare Updates an.
|-
| zypper lr -u || Software-Repositories inklusive der URL anzeigen.
|-
| zypper removerepo <nummer> || Repository mit Nummer aus zypper lr -u löschen.
|-
| --- || ---
|-
| zypper update --details || Alle Pakete aktualisieren. Vorher die Details ansehen.
|-
| zypper dup --from KDE:Frameworks5 --details || Konsistentes Update von einem bestimmten Repository.
|}

=== Allgemein ===

{| class="wikitable"
|-
! Befehl !! Beschreibung
|-
| cnf || command not found handler - siehe auch man cnf
|-
| sudo <befehl> || substitute user do - wird meistens dazu verwendet, um einen Befehl mit dem Superuser root auszuführen
|}


== Spezial-Infos ==

=== Auto-Refresh deaktivieren ===
* Damit das YaST-Softwaremanagement-Modul schneller startet, kann man das Auto-Refresh von Repositories deaktivieren: <source>
$ sudo zypper modifyrepo --all --no-refresh
</source>
** todo: ggf. vorher schauen, ob es Repos gibt, die schon kein Autorefresh haben, falls man die Aktion wieder rückgängig machen möchte.

* Manuelles Updaten der Repositories geht so: <source>
$ sudo zypper refresh
</source>

=== Neueste KDE/Plasma-Versionen installieren ===

Folgende KDE-Repositories sind einzubinden.

openSUSE 13.2:
* http://download.opensuse.org/repositories/KDE:/Qt5/openSUSE_13.2/
* http://download.opensuse.org/repositories/KDE:/Frameworks5/openSUSE_13.2/
* http://download.opensuse.org/repositories/KDE:/Applications/openSUSE_13.2/
** Alternativ (für yakuake): http://download.opensuse.org/repositories/home:/wolfi323:/branches:/KDE:/Frameworks5/openSUSE_13.2/
*** Die anderen yakuake-Pakete für openSUSE 13.2 (inklusive yakuake ohne 5) sind im Build-System deaktiviert oder funktionieren nicht.
*** Tipp: Mit openSUSE ab 42.1 (nur noch 64 Bit) gehen die Dinge einfacher.

openSUSE Leap 42.1
* http://download.opensuse.org/repositories/KDE:/Qt5/openSUSE_Leap_42.1/
* http://download.opensuse.org/repositories/KDE:/Frameworks5/openSUSE_Leap_42.1/
* http://download.opensuse.org/repositories/KDE:/Applications/openSUSE_Leap_42.1/

Beim Einbinden jeweils folgende Namen vergeben:
* KDE:Qt5
* KDE:Frameworks5
* KDE:Applications

==== Konsistentes Update mit zypper ====

Beim Update der selbst eingebundenen KDE-Repositories muss darauf geachtet werden, dass dies konsistent erfolgt. Dazu mit

<source>sudo zypper lr -u</source>

die vorhandenen Repositories auflisten und die Namen der (selber eingebundenen) KDE-Repositories raussuchen. Zum Beispiel: KDE:Qt5, KDE:Frameworks5, KDE:Applications.

Nun mit

<source>sudo zypper dup --from KDE:Qt5 --from KDE:Frameworks5 --from KDE:Applications --details</source>

diese drei Repositories als primäre Quelle für ein Distributions-Update verwenden.

=== Multimedia-Codecs ===
* Warum scheint es so schwierig zu sein '''ffmpeg''' richtig zu installieren?
** Antworten siehe [https://de.opensuse.org/FFmpeg openSUSE FFmpeg] und [https://de.opensuse.org/SDB:Multimediaf%C3%A4higkeiten_erweitern openSUSE Multimediafähigkeiten erweitern]
** siehe auch [http://opensuse-guide.org/codecs.php Codec Installation with 1-Click]

=== Repositories aufräumen ===

* Am Beispiel 42.1
* YAST -> Software Repositories öffnen
** Nach URL sortieren
** Schauen, ob welche doppelt sind. So löschen, das jeweils nur noch eins übrig ist.
** Fragliche Repositories rausschreiben
** Software Repositories schließen
* YAST -> Software Management öffnen
** Repositories Tab einblenden und aufmachen
** Die fraglichen Repositories durchgehen, schauen ob und welche Software nur von dort kommt
*** Dazu in der Tableiste unter der Paketliste den Versions Tab aufmachen und schauen, ob das Paket auch in anderen Repositories vorhanden ist
*** Entscheiden, ob das Repository gelöscht werden soll oder nicht (-> aufschreiben)

Beispiel:
* http://download.opensuse.org/repositories/Application:/Geo/openSUSE_Leap_42.1/
** Paket: qgis
* languages:haskell
** löschen
* languages:pascal
** löschen
* languages:perl
** löschen
* http://download.opensuse.org/repositories/home:/andra-gabr/openSUSE_Leap_42.1/
** löschen (pycharm)
* http://download.opensuse.org/repositories/home:/aspiers/openSUSE_Leap_42.1/
** screenkey - A screen-cast tool to show your keys and based on key-mon project
* http://download.opensuse.org/repositories/home:/darix:/playground/openSUSE_Leap_42.1/
** löschen
* http://download.opensuse.org/repositories/home:/ecsos:/jameica/openSUSE_Leap_42.1/
** hibiscus
* http://download.opensuse.org/repositories/home:/fcrozat:/branches:/openSUSE:/Leap:/42.1:/Update/standard/
** löschen
* http://download.opensuse.org/repositories/home:/illuusio/openSUSE_Leap_42.1/
** jp2a - Converts JPEG images to ASCII
* http://download.opensuse.org/repositories/home:/lackhove:/kdevelop-python3/openSUSE_Leap_42.1/
** löschen
* http://download.opensuse.org/repositories/home:/MargueriteSu/openSUSE_Leap_42.1/
** löschen
* http://download.opensuse.org/repositories/home:/Rotkraut:/Opt-Python/openSUSE_Leap_42.1/
** Python 3.5
* http://download.opensuse.org/repositories/home:/vkrause/openSUSE_Leap_42.1/
** löschen
* http://download.opensuse.org/repositories/home:/wolfi323:/branches:/KDE:/Frameworks5/openSUSE_Leap_42.1/
** löschen
* KDE:Extra
** Paket: gammaray (stürzt ab)
* Mono
** nuget
* http://download.opensuse.org/repositories/X11:/Enlightenment:/Factory/openSUSE_Leap_42.1/
** löschen
* Packman
** flacon - Split compressed Audio CD images to tracks
* http://download.opensuse.org/repositories/X11:/Sugar/openSUSE_Leap_42.1/
** löschen

=== Quellcode herunterladen ===
* Siehe Arbeitsblatt [[Course:Einstieg in die Programmierung mit Python/Arbeitsblätter/Tunneler anpassen|Tunneler anpassen]].

* Holen von Source-Code direkt vom openSUSE Build Service: siehe https://en.opensuse.org/Source_code, Beispiel xeyes:
<source>
$ rpm -q --qf '%{disturl}\n' xeyes
obs://build.opensuse.org/openSUSE:Leap:42.1/standard/75483092a51c227b815049b8300943b0-xeyes

$ osc co -r 75483092a51c227b815049b8300943b0 openSUSE:Leap:42.1 xeyes
A    openSUSE:Leap:42.1
A    openSUSE:Leap:42.1/xeyes
A    openSUSE:Leap:42.1/xeyes/xeyes-1.1.1.tar.bz2
A    openSUSE:Leap:42.1/xeyes/xeyes.changes
A    openSUSE:Leap:42.1/xeyes/xeyes.spec
At revision 75483092a51c227b815049b8300943b0.
</source>

==== Troubleshooting ====
* 'sudo zypper si xeyes' findet keine Quelle.
** Ist das Source-Repository eingebunden und aktiviert? -> [https://forums.opensuse.org/showthread.php/468816-installing-package-sources-with-zypper-(doesn-t-work) Beispiel siehe z. B. hier].

4. Eigenes System - Lokales Netzwerk 2
--------------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Eigenes_System/Lokales_Netzwerk_2

    1 Firewall
        1.1 Zone INT oder EXT?
        1.2 Zone, Netzwerkschnittstelle, Dienst
            1.2.1 Details
        1.3 Ein- und ausgehende Verbindungen
            1.3.1 Beispiel SMPT-Port
    2 (im Aufbau) Desktop-Freigabe im Netzwerk
        2.1 Einleitung
        2.2 Eigenen Rechner freigeben
        2.3 Auf freigegebenen Rechner zugreifen
    3 (im Aufbau) Heimnetzwerk
        3.1 mit Samba
        3.2 mit Extra-Hardware
    4 Nützliche Netzwerk-Tools

__TOC__

== Firewall ==

Siehe auch [[Course:Einstieg in die Programmierung mit Python/Eigenes System/Lokales Netzwerk|Lokales Netzwerk Teil 1]].

=== Zone INT oder EXT? ===
Eine Personal Firewall wird dann empfohlen, wenn der Rechner mit dem Internet verbunden ist. Es gibt allerdings verschiedene Szenarien, in denen es in Ordnung ist, die persönliche Firewall zu deaktivieren, da das Netzwerk durch andere Firewalls bereits geschützt ist.

<source>
Persönliche Firewall empfohlen (Zone EXT):
---------------------------------------------------------------
|  Eigenes Gebäude                                            |
|                                                             |
|                                                             |
|   --------------------            --------------------      |
|   |                  |            |                  |      |
|   | Eigener Rechner  |------------| Internet-Modem   |      |
|   |                  |            | ohne Firewall    |      |
|   --------------------            |                  |      |
|                                   --------------------      |
|                                           |                 |
|                                           |                 |
---------------------------------------------------------------
                                            |
                                            | Kabel
                                            |
           ----------------------------------------------------
           |  Andere Gebäude / Andere Rechner                 |
           |                                                  |
           |                   Internet Service Provider      |
           |                                                  |
           |             Das Internet                         |
           |                                                  |
           ----------------------------------------------------

</source>

<source>
Persönliche Firewall kann auch abgeschaltet werden (Zone INT):
---------------------------------------------------------------
|  Eigenes Gebäude                                            |
|                                                             |
|                                                             |
|   --------------------            --------------------      |
|   |                  |            |                  |      |
|   | Eigener Rechner  |------------| Internet-Router  |      |
|   |                  |            | mit Firewall     |      |
|   --------------------            |                  |      |
|                                   --------------------      |
|                                           |                 |
|                                           |                 |
---------------------------------------------------------------
                                            |
                                            | Kabel
                                            |
           ----------------------------------------------------
           |  Andere Gebäude / Andere Rechner                 |
           |                                                  |
           |                   Internet Service Provider      |
           |                                                  |
           |             Das Internet                         |
           |                                                  |
           ----------------------------------------------------
</source>

<source>
Persönliche Firewall kann auch abgeschaltet werden (Zone INT):
----------------------------------------------------------------------------------
|  Eigenes Gebäude                                                               |
|                                                                                |
|                                                                                |
|   --------------------        --------------------      ---------------------  |
|   |                  |        |                  |      |                   |  |
|   | Eigener Rechner  |--------| Lokales Netzwerk |------| Netzwerk-Firewall |  |
|   |                  |        |                  |      |                   |  |
|   --------------------        --------------------      ---------------------  |
|                                                                   |            |
|                                                                   |            |
----------------------------------------------------------------------------------
                                                                    |
                                                        .-----------'
                                                        |
           ----------------------------------------------------
           |  Andere Gebäude / Andere Rechner                 |
           |                                                  |
           |                   Internet Service Provider      |
           |                                                  |
           |             Das Internet                         |
           |                                                  |
           ----------------------------------------------------
</source>

=== Zone, Netzwerkschnittstelle, Dienst ===
Erklärung, was eine Firewall ist, am Beispiel von SuSEfirewall2.

Eine Firewall schützt vor Netzwerk-Angriffen, indem bestimmte ungewollte Pakete, die an der Netzwerkschnittstelle (Netzwerkkabel oder Wireless) angekommen, abgelehnt oder verworfen werden.

Folgende Begriffe sind hier von Relevanz:
* die '''Firewall-Zone'''
* die '''Netzwerkschnittstelle'''
* der '''Dienst'''

Die SuSEfirewall2 definiert standardmäßig drei Zonen:
* '''EXT''' - External Zone - unvertrauenswürdig, an das Internet angeschlossen
* '''INT''' - Internal Zone -  voll vertrauenswürdig, keine Filterung, LAN (= lokales Netzwerk)
* '''DMZ''' - Demilitarized Zone - für Server, die vom Internet erreichbar sein sollen, siehe auch [https://de.wikipedia.org/wiki/Demilitarized_Zone Demilitarized_Zone]

Folgende Netzwerkschnittstellen gibt es:
* '''lo''': LOOPBACK: diese Schnittstelle ist virtuell und existiert immer, auch wenn keine Netzwerkhardware installiert ist. Sie verbindet den Rechner mit sich selbst. Die IP-Adresse ist 127.0.0.1 (oder localhost). Im Zusammenhang mit der Firewall-Erklärung wird sie nicht weite betrachtet.
* '''eth0''': das erste installierte LAN-Netzwerkgerät (da, wo man das LAN-Kabel reinsteckt). Falls mehrere Geräte installiert sind, gibt es noch eth1, eth2 usw.
* '''wlan0''': das erste installierte Netzwerk-Gerät für WLAN-Verbindungen
* siehe $ ip address

Was ist ein Dienst?
* Der Begriff Dienst wird in unterschiedlichen Zusammenhängen verwendet. Hier eine verkürzte Erklärung.
* Ein Dienst ist ein Programm, das im Hintergrund auf deinem Rechner läuft, das auf einen TCP-Port registriert ist, z. B. ein HTTP-Server, mit dem im Internet Webseiten bereitgestellt werden.
* Ein Port ist eine Zahl von 0 bis 65535, siehe [https://de.wikipedia.org/wiki/Port_(Protokoll) Wikipedia-Eintrag]. Diese ist prinzipiell beliebig, es gibt aber [https://de.wikipedia.org/wiki/Liste_der_standardisierten_Ports eine Liste von standardisierten Ports]. Das http-Protokoll hat den Standard-Port 80, https hat 443.
* Ein Port kann nur von genau einem Programm verwendet werden. Wenn ein zweites Programm denselben Port öffnen möchte, liefert das Betriebssystem einen Fehler.
* Auf dem eigenen Rechner sind für die normale Verwendung im Internet keine Dienste und offenen Ports notwendig. Daher sollte ein Portscan auf die eigene IP auch nichts liefern, siehe z. B. [http://www.whatsmyip.org/port-scanner/server/ Server Port Test], [http://www.whatsmyip.org/port-scanner/apps/ Application Port Test].

Wie hängen Firewall-Zone, Netzwerkschnittstelle und Dienst zusammen?
<source>
------------------------
| Jede Firewall-Zone   |
| (EXT, INT, DMZ)      |
------------------------
    |                                 -----------------------------
    `-------> hat eine Liste von  ----| Erlaubten Diensten        |
    |                                 | (z. B. HTTP, TCP-Port 80) |
    |                                 | (oder nichts)             |
    |                                 -----------------------------
    |
    |                                            --------------------------
    `-------> gilt für für eine oder mehrere ----| Netzwerkschnittstellen |
                                                 | (z. B. eth0, wlan0)    |
                                                 --------------------------
    (Wenn eine Netzwerkschnittstelle keiner
    Zone zugeordnet ist, dann gilt EXT.)
</source>

==== Details ====
* siehe https://en.opensuse.org/SuSEfirewall2
** "SuSEfirewall2 is basically a script that generates iptables rules from configuration stored in the /etc/sysconfig/SuSEfirewall2 file"
** "By default all unassigned interfaces are automatically assigned to the external zone."
** "The variable FW_ZONES can be used to define additional zones. For example, if you don't want the restrictive filtering of the external zone in your WLAN, but also don't fully trust the WLAN so you can't use the internal zone, you could define a new zone."
* Mehr zur Loopback-Schnittstelle
** [https://de.wikipedia.org/wiki/Loopback auf Wikipedia], [http://askubuntu.com/questions/247625/what-is-the-loopback-device-and-how-do-i-use-it auf askubuntu.com], [https://www.juniper.net/techpubs/en_US/junos14.1/topics/concept/interface-security-loopback-understanding.html von juniper.net]
* Mehr zu Ports
** [https://de.wikipedia.org/wiki/Port_(Protokoll)#Dienstnamen Dienstnamen], siehe /etc/services
** [https://de.wikipedia.org/wiki/Port_(Protokoll)#Portfilter Portfilter]
** [https://de.wikipedia.org/wiki/Port_(Protokoll)#Portscanner Portscanner]
* IP-Protokolle außer TCP und UDP
** http://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml

=== Ein- und ausgehende Verbindungen ===
Siehe z. B. [http://www.pcwelt.de/ratgeber/Online-Sicherheit-Kleiner-Ratgeber-Das-wichtigste-zu-Firewalls-6177515.html PC-Welt-Artikel].

==== Beispiel SMPT-Port ====
* SMTP-Sperre?
** https://www.luis.uni-hannover.de/fwgw-smtp.html
*** Hier wird erklärt, warum früher eine SMTP-Sperre (Port 25 und 465) eingeführt wurde, wodurch man als Nutzer keine Mails mehr senden konnte, weil darüber auch Spam versendet wurde.
*** Nun gibt es aber den TCP-Port 587 ("submission"), der nicht blockiert werden muss, weil kein Spam versendet werden kann, aber der normale Nutzer seine E-Mails.
* Weiteres:
** http://linuxmuster.net/doku/howto_basiskurs/Die_linuxmuster_net_im_Unterricht.html?highlight=smtp
** https://www.uni-ulm.de/einrichtungen/kiz/service-katalog/netzwerk-konnektivitaet/einstellungen/
** http://alp.dillingen.de/netacad/materialien/Sichere_Internetanbindung_I.pdf
* Sonstiges:
** http://www.gesetze-bayern.de/Content/Document/BayVwV270218-43?AspxAutoDetectCookieSupport=1
** https://www.uni-due.de/zim/services/sicherheit/port_25.shtml
*** Hier werden z. B. nur Verbindungen zu einer vorgegeben Liste von bekannten SMTP-Servern zugelassen (gute Idee)

== (im Aufbau) Desktop-Freigabe im Netzwerk ==

=== Einleitung ===
Es ist möglich den eigenen Computer freizugeben, um anderen die Möglichkeit zu geben
* zu sehen wie der eigene Bildschirminhalt aussieht oder
* zwecks Hilfe den eigenen Rechner zusätzlich mit Maus- und Tastatureingaben fernzusteuern.

Wir arbeiten mit dem Plasma-Desktop und verwenden daher das Programm [https://www.kde.org/applications/system/krfb/ Krfb], um die Freigabe durchzuführen.

ACHTUNG Bug:
* Derzeit funktioniert die Desktop-Freigabe möglicherweise nicht, siehe [https://bugs.kde.org/show_bug.cgi?id=356782 Bug 356782 - krfb stops sending updates after the first frame]

=== Eigenen Rechner freigeben ===

Teil 1 - Einrichten:

* Stelle sicher, dass das Programm '''krfb''' installiert ist und starte es.

* Aktiviere den Haken "Enable Desktop Sharing"

* Notiere dir den Wert für die IP-Adresse und den Port, z. B. 192.168.178.30:5900

* Vergebe ein Passwort, dass derjenige benötigt, der sich auf deinen Rechner verbinden will.

Teil 2 - Selbsttest:

* Stelle sicher, dass das Programm '''krdc''' installiert ist und starte es.

* Unter "Connect to" wähle das Protokoll '''vnc''' aus.

* In die Textbox dahinter gebe die notierte IP-Adresse mit Port ein, z. B. 192.168.178.30:5900

* Bestätige den erscheinenden Dialog "Host Configuration" "mit OK.

* Es erscheint ein Hinweis, dass jemand auf deinen Desktop zugreifen will.

* Deaktiviere den Haken, der Maus- und Tastatur-Zugriff erlaubt.

* Es sollte der eigene Bildschirminhalt erscheinen und keine Fehlermeldung.

Teil 3 - Firewall:

* Stelle sicher, dass deine Firewall den Port 5900 nicht blockiert, siehe voriger Abschnitt.


Siehe auch:
* http://www.binarytides.com/remote-share-kde-desktop/
** ACHTUNG: Warum sehe ich das "Send Personal invitation"-Dialog nicht? --> Wurde abgeschafft, siehe [https://bugs.kde.org/show_bug.cgi?id=365018 Bug 365018 - Can't send invitations]

=== Auf freigegebenen Rechner zugreifen ===

Rechner BETA möchte auf Rechner ALPHA zugreifen:

<source>
  ------------------------               ----------------------------
  |                      |               |                          |
  |  Rechner BETA        | ------------> | Rechner ALPHA            |
  |                      |               | (Freigabe eingerichtet,  |
  ------------------------               | siehe voriger Abschnitt) |
  Möchte auf                             |                          |
  Rechner ALPHA zugreifen.               ----------------------------
</source>



* Rechner BETA: Stelle sicher, dass das Programm '''krdc''' installiert ist und starte es.

* Rechner BETA: Unter "Connect to" wähle das Protokoll '''vnc''' aus.

* Rechner BETA: In die Textbox dahinter gebe die von Rechner A notierte IP-Adresse mit Port ein, z. B. 192.168.178.30:5900

* Rechner BETA: Bestätige den erscheinenden Dialog "Host Configuration" "mit OK.

* Rechner ALPHA: Es erscheint ein Hinweis, dass jemand auf deinen Desktop zugreifen will.

* Rechner ALPHA: Deaktiviere den Haken, der Maus- und Tastatur-Zugriff erlaubt.

* Rechner BETA: Es sollte der Bildschirminhalt Rechner A erscheinen.

== (im Aufbau) Heimnetzwerk ==
=== mit Samba ===
* https://wiki.ubuntuusers.de/Heimnetzwerk/
* https://wiki.ubuntuusers.de/Samba_Server_KDE/

=== mit Extra-Hardware ===
* https://www.piratebox.cc/faq (Schweden)
** "PirateBox ist ein System zum Selberbauen, um ohne Internet Dateien auszutauschen und mittels Computern zu kommunizieren. Es baut auf freier Software auf und verwendet billige Hardware."

== Nützliche Netzwerk-Tools ==
* '''gnome-nettool''', siehe https://projects.gnome.org/gnome-network/screenshots.shtml, [https://projects.gnome.org/gnome-network/screenshots/info_lookup.jpg Screenshot für Lookup]


4. Eigenes System - Internet-Tipps
----------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Eigenes_System/Internet-Tipps (DEL)

    1 Der eigene Computer und das Internet
    2 Was ist das Internet?
    3 Mozilla Firefox
    4 Web-Suchmaschinen
        4.1 Beispiele
        4.2 Hintergrund: Die Filterblase
        4.3 Spezial-Suchmaschinen
    5 Schutz vor Gefahren
        5.1 Eigene Datensicherung durchführen
        5.2 Weitere Empfehlungen
    6 E-Mail
        6.1 Allgemein
        6.2 Mozilla Thunderbird
            6.2.1 Erweiterung: XNote++
            6.2.2 Tipp: Mail-Format (Text/HTML) ändern
        6.3 E-Mail-Provider-Auswahl
        6.4 Verschlüsselung
        6.5 Wegwerf-Adressen

__TOC__

== Der eigene Computer und das Internet ==

<source>
--------------------------------------------------------
| Eigener Computer                                     |
|                                                      |
|   mit GNU/Linux                                      |
|   und installierten Programmen                       |
|                                                      |
|   z. B.                                              |
|      Mozilla Firefox     zypper-Software-Management  |
|         |   |   |                             |      |
--------------------------------------------------------
          |   |   |                             |
          |   |   `------------------.          |
          |   `----------.           |          |         Das Internet
          |              |           |          |         = andere Computer
          v              |           |          |
   -----------------     |           `------.   |
   | tagesschau.de |     |                  v   v
   -----------------     |                 -------------------------
                         v                 | software.opensuse.org |
                -------------------        -------------------------
                | its-learning.de |
                -------------------
</source>

== Was ist das Internet? ==
...

Technisch gesehen besteht das Internet aus untereinander vernetzten Computern, die Dienste bereitstellen, die vom eigenen Rechner aus verwendet werden können. Siehe Bild oben.

Was das für Dienste sind und ob sie gut oder schlecht sind (z. B. ob sie die Daten der Nutzer vertraulich behandeln oder nicht), bestimmen die Eigentümer oder Mieter der jeweiligen Rechner.

== Mozilla Firefox ==
Von den [https://en.wikipedia.org/wiki/Web_browser großen Webbrowsern] ist Mozilla Firefox der einzige mit einer freien Software-Lizenz. Siehe auch [[Course:Einstieg in die Programmierung mit Python/FAQ/Firefox-Tipps|FAQ/Firefox-Tipps]].

== Web-Suchmaschinen ==
=== Beispiele ===
* Einsteiger-Empfehlung: Google-Suche ohne Filterblase
** http://startpage.com
* Weitere Empfehlungen
** https://prism-break.org/de/all/#web-search
* Aufgaben
** siehe auch http://feinstaub.github.io/arbeitsblatt.net/arbeitsblatt-inet-suchmaschinen.html
* '''searx''' - a privacy-respecting, hackable metasearch engine
** Webseite: https://searx.me/
** Info über: https://asciimoo.github.io/searx/
** Wikipedia-Eintrag: https://fr.wikipedia.org/wiki/Searx
** Liste von öffentlichen Installationen: https://github.com/asciimoo/searx/wiki/Searx-instances
** Code (geschrieben mit '''Python''', veröffentlicht unter der '''AGPL'''): https://github.com/asciimoo/searx

=== Hintergrund: Die Filterblase ===
* [http://www.tagesschau.de/multimedia/video/video-238713.html Was ist eine Filterblase (Tagesschau-Video, 2016)]
* [https://dontbubble.me/ Infos von startpage.com]

=== Spezial-Suchmaschinen ===
* z. B. https://search.creativecommons.org/

== Schutz vor Gefahren ==
=== Eigene Datensicherung durchführen ===
* Eine Datensicherung hilft gegen
** technische Fehler der Festplatte
** [https://de.wikipedia.org/wiki/Ransomware Erpressungssoftware], die eigene Dateien verschlüsselt und/oder löscht
*** siehe auch [https://de.wikipedia.org/wiki/Ransomware#Schutz-_und_Gegenma.C3.9Fnahmen Schutz und Gegenmaßnahmen]
** sonstigen technischen Probleme
* Software dazu siehe [[Course:Einstieg in die Programmierung mit Python/Eigenes System/Empfehlenswerte Programme|Eigenes System/Empfehlenswerte Programme]]/Datensicherung

=== Weitere Empfehlungen ===
* '''Regelmäßige Updates'''
** Artikel, warum regelmäßige Updates sind wichtiger als [http://www.golem.de/news/antivirensoftware-die-schlangenoel-branche-1612-125148.html Antivirensoftware, die oft selber gravierende Sicherheitslücken enthält] (Golem 2016)
* IT-Sicherheit im Jahr 2016: [http://www.golem.de/news/it-sicherheit-im-jahr-2016-der-nutzer-ist-nicht-schuld-1612-125259.html Der Nutzer ist oft nicht schuld], aber '''mitdenken''' schadet auch nicht!

== E-Mail ==
=== Allgemein ===
Damit private Nachrichten nicht dauerhaft auf einem fremden Rechner lagern und man auch ohne Internet Zugriff auf seine Nachrichten hat, ist die Verwendung eines lokalen E-Mail-Clients empfehlenswert.

Beispiel: '''Mozilla Thunderbird'''.

Weitere Vorteile: da die Unternehmen, die große E-Mail-Bestandskonten haben, vermehrt Angriffen ausgesetzt sind, ist es sinnvoll, so wenig wie möglich Privates dort zu lagern.

Nachteile: der Komfort von allen möglichen Rechnern immer auf alle E-Mails zugreifen zu können. Eigene Datensicherung notwendig.

Empfehlung: Geschickte Kombination der Nutzung des Webclients und des lokalen E-Mailprogramms.

Siehe auch: [https://lehrerfortbildung-bw.de/st_digital/medienwerkstatt/dossiers/sicherheit/email/mailer/ lehrerfortbildung-bw.de: "Ein Mail-Programm? Ein Webmailer ist doch so bequem!"]

Hinweis: Zum Senden von E-Mail aus dem E-Mail-Programm wird SMTP verwendet. In manchen Schul-Netzwerken ist leider der '''SMTP Submission-Port''' durch die Netzwerk-Firewall gesperrt.

=== Mozilla Thunderbird ===
Webseite und Download: https://www.mozilla.org/de/thunderbird/

==== Erweiterung: XNote++ ====
* https://addons.mozilla.org/en-US/thunderbird/addon/xnotepp/

==== Tipp: Mail-Format (Text/HTML) ändern ====
* http://kb.mozillazine.org/Plain_text_e-mail_(Thunderbird)#Change_your_mind
** "Just press the "Shift" key when clicking the "Write", "Reply", or "Reply to All" button and it will use the other format for that message. Unfortunately, this doesn't work for "Forward". "

=== E-Mail-Provider-Auswahl ===
Kriterien:
* Verwendet aktuelle Sicherheitsstandards wie z. B. [https://de.wikipedia.org/wiki/DNS-based_Authentication_of_Named_Entities DANE]?
* Wie wird bezahlt? (Mit den eigenen Daten? Durch Erdulden von Werbung? Direkt mit Geld? Letzteres erhöht die Wahrscheinlichkeit für guten Datenschutz)

=== Verschlüsselung ===
* https://emailselfdefense.fsf.org/de/

=== Wegwerf-Adressen ===
* trashmail.de
** "Sie möchten sich auf einer Webseite registrieren, aber die Anmeldung ist nur mit einer gültigen E-Mail-Adresse möglich, da ein Anmeldelink bestätigt werden muss. Ihre private E-Mail-Adresse wollen Sie aufgrund zunehmender Werbemails aber nur ungern weitergeben. Wir bieten Ihnen die Lösung mit einer Wegwerf-E-Mail-Adresse." -- http://www.trashmail.de/

* https://temp-mail.org
** mit API: https://temp-mail.org/en/api/

* Weitere: Suche nach "disposable mail account".


5. Unsortiert - Buch-Empfehlungen
----------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Unsortiert/Buch-Empfehlungen

    1 Themen
        1.1 Mathematik
        1.2 Das Linux-System
        1.3 Computer/Hacker-Welt
        1.4 Komplexe Systeme
        1.5 Technik-Folgen
        1.6 Gesellschaft
        1.7 Philosophie -- „Liebe zur Weisheit“
        1.8 Psychologie / Sich selber verstehen
        1.9 Wie funktioniert Wikipedia?
        1.10 Wie funktioniert freie Software?

__TOC__

== Themen ==

=== Mathematik ===
* '''Geheime Botschaften''' von Simon Singh - über Verschlüsselung und Geschichte, spannend geschrieben
* '''Fermats letzter Satz''' von Simon Singh - über Mathematik und Geschichte, spannend geschrieben

=== Das Linux-System ===
* Podcast über das Linux-System: http://cre.fm/cre209-das-linux-system (2015, knapp 3 h), interviewt wird Lennart Poettering, "der schon vorher verschiedene Subsysteme zur Linux-Landschaft beigetragen hat".

=== Computer/Hacker-Welt ===
* '''Kuckucksei''' (1989) von Clifford Stoll - Hackergeschichte; eher Sachbuch als Roman; UNIX-Systeme haben eine zentrale Rolle

=== Komplexe Systeme ===
* '''Jurrasic Park''' (1990) von Michael Crichton - Thema: komplexe Systeme und deren Unbeherrschbarkeit

=== Technik-Folgen ===
* Klassiker
** [https://de.wikipedia.org/wiki/1984_(Roman) '''1984'''] von George Orwell - Thema: mögliche Folgen einer totalitäre Überwachung; eine Gesellschaft ohne Privatsphäre

* Aktuell
** [https://de.wikipedia.org/wiki/Zero_%E2%80%93_Sie_wissen,_was_du_tust '''Zero'''] von Marc Elsberg - Themen: Privatsphäre in Zeiten totaler Vernetzung, „Aufklärung ist der Ausgang des Menschen aus seiner selbst verschuldeten Unmündigkeit." (https://de.wikipedia.org/wiki/Immanuel_Kant Immanuel Kant)
** '''Blackout''' von Marc Elsberg - Thema: IT-Sicherheit von Infrastruktur

=== Gesellschaft ===
* Klassiker
** [https://de.wikipedia.org/wiki/Sch%C3%B6ne_neue_Welt Schöne neue Welt] (1932)

=== Philosophie -- „Liebe zur Weisheit“ ===
* Überblick
** https://de.wikipedia.org/wiki/Philosophie
* Ein Teilgebiet der Philosophie erarbeitet die Grundlagen eines guten menschlichen Zusammenlebens
** z. B. https://de.wikipedia.org/wiki/Immanuel_Kant
* Software-Ethik
** z. B. https://www.gnu.org/philosophy/philosophy.en.html
* Historischer Abriss
** Sophies Welt
* Zeitgenössische Philosophen
** z. B. https://de.wikipedia.org/wiki/Richard_David_Precht

=== Psychologie / Sich selber verstehen ===
* '''Komm, ich erzähl dir eine Geschichte''' von Jorge Bucay

=== Wie funktioniert Wikipedia? ===
* siehe https://www.quora.com/What-are-the-best-books-about-Wikipedia

=== Wie funktioniert freie Software? ===
* '''Open Advice''' (2012, [http://www.lydiapintscher.de/books.php Lydia Pintscher], Hrsg.) - collection of essays by 42 prominent Free Software contributors - http://open-advice.org
* '''20 Years of KDE: Past, Present and Future''' (2016, Lydia Pintscher, Hrsg.) - https://20years.kde.org/book/, [https://20years.kde.org/book/20yearsofKDE.pdf pdf]


5. Unsortiert - Python3-Game-Programming
----------------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Unsortiert/Python3-Game-Programming

    1 Frameworks
        1.1 pygame
        1.2 Pygame_SDL2
            1.2.1 pybox2d
        1.3 arcade
        1.4 PySDL2
        1.5 Qt/QPainter
            1.5.1 zetcode.com Tetris
        1.7 kivy
        1.8 Weitere
    2 Tutorials / Ideen
        2.1 zetcode.com: Java 2D games tutorial
        2.2 programarcadegames.com: Program Arcade Games With Python And Pygame
    3 Andere
        3.1 LÖVE

__TOC__

Auf der Suche nach einer freien Python3-Spiele-Bibliothek inklusive Tutorials für Einsteiger. Gerne auch auf Englisch.

== Frameworks ==

=== pygame ===

* Die Tutorial-Doku scheint ziemlich veraltet zu sein (Jahr 2002):
** http://www.pygame.org/docs/index.html
* Die API-Doku scheint aktuell zu sein:
** z. B. http://www.pygame.org/docs/ref/rect.html#pygame.Rect.move
** Bei jeder Funktion kann man sich mittels Button '''Beispiel-Code''' anzeigen lassen.
* Gute '''Beispiele''' und '''didaktische Aufbereitung''' siehe unten ("programarcadegames.com: Program Arcade Games With Python And Pygame")
* Funktioniert nicht auf Anhieb ("Fatal Python error: (pygame parachute) Segmentation Fault"), aber mit Pygame_SDL2, siehe nächster Abschnitt

=== Pygame_SDL2 ===

* Doku: http://pygame-sdl2.readthedocs.io/en/latest/
** "There isn’t much pygame_sdl2-specific documentation at the moment. Since pygame_sdl2 tries to follow pygame as closely as possible, the best reference is the pygame documentation:"
*** verweist auf: http://www.pygame.org/docs/
* Repo: https://github.com/renpy/pygame_sdl2
* Installation auf openSUSE 42.1: <tt>sudo zypper install python3-pygame_sdl2</tt>

Wenn man vor dem <tt>import pygame</tt> folgendes schreibt, kann man pygame mit '''Python2 und 3''' verwenden:

<source>
#
# taken from http://www.pygame.org/docs/tut/intro/intro.html and modified
#
import sys

# see http://pygame-sdl2.readthedocs.io/en/latest/
import pygame_sdl2
pygame_sdl2.import_as_pygame()

import pygame

pygame.init()

size = width, height = 620, 440
speed = [20, 10]
black = 0, 0, 0
BLUE  = (0, 0, 255)

screen = pygame.display.set_mode(size)

#ball = pygame.image.load("ball.gif") # needs the image file from the tutorial
# we create our own ball:
ball = pygame.Surface((100, 100)) # http://www.pygame.org/docs/ref/surface.html
pygame.draw.circle(ball, BLUE, (50, 50), 50) # http://www.pygame.org/docs/ref/draw.html
ballrect = ball.get_rect()
clock = pygame.time.Clock()

while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

    ballrect = ballrect.move(speed[0], speed[1])
    if ballrect.left < 0 or ballrect.right > width:
        speed[0] = -speed[0]
    if ballrect.top < 0 or ballrect.bottom > height:
        speed[1] = -speed[1]

    screen.fill(black)
    screen.blit(ball, ballrect)
    pygame.display.flip()
    # http://www.pygame.org/docs/ref/time.html
    # By calling Clock.tick(30) once per frame, the program will never run at more than 30 frames per second.
    # leider trotzdem tearing, siehe http://stackoverflow.com/questions/1082562/how-to-avoid-tearing-with-pygame-on-linux-x11
    # (wird mit Wayland kein Problem mehr sein)
    clock.tick(30)
</source>

Run: <tt>python3 pygame1.py</tt>

==== pybox2d ====

"pybox2d is a 2D rigid body simulation library for games. Programmers can use it in their games to make objects move in believable ways and make the game world more interactive." (https://github.com/pybox2d/pybox2d/wiki/manual) (siehe auch [http://www.binarytides.com/javascript-box2d-tutorial/ Programming box2d in javascript – tutorial on basics" on BinaryTides - Genuine how-to guides on Linux, Ubuntu and FOSS])

* Code-Repo: https://github.com/pybox2d/pybox2d

Getting started (https://github.com/pybox2d/pybox2d/blob/master/INSTALL.md):

Install:

<source>
sudo zypper install python3-conda
conda create -n py34 python=3.4
source activate py34
conda install -c https://conda.anaconda.org/kne pybox2d
</source>

Install pygame_sdl2 backend and clone examples:

<source>
conda install -c https://conda.anaconda.org/kne pygame_sdl2
git clone https://github.com/pybox2d/pybox2d -b 2.3.1
</source>

Run an example:

<source>
$ cd pybox2d

# List all of the examples available:
$ ls examples/*.py

# Try the web example:
$ python -m examples.web --backend=pygame

# Or another:
$ python -m examples.rope --backend=pygame
$ python -m examples.pyramid --backend=pygame
</source>

Funktioniert (getestet auf openSUSE 42.1)


Anleitung:
* https://github.com/pybox2d/pybox2d/wiki/manual
* Bevor das eingesetzt wird, sollte man mal probieren eine eigene einfache Physik-Simulation zu programmieren (z. B. Newtonsche Formeln für freien Fall oder Federgleichung)

=== arcade ===

http://pythonhosted.org/arcade/index.html

Vergleich mit pygame: http://pythonhosted.org/arcade/pygame_comparison.html#pygame-comparison

Installation: http://pythonhosted.org/arcade/installation_linux.html, vorbildlich mit virtualenv:

<source>
#virtualenv ~/.virtualenvs/arcade -p python3
# für openSUSE 42.1 benötigen wir dieses Paket, weil arcade min. Python 3.5 braucht: https://software.opensuse.org/package/opt-python35
virtualenv ~/.virtualenvs/arcade -p /opt/python/bin/python3.5
source ~/.virtualenvs/arcade/bin/activate
pip install arcade
# dauert eine Weile, weil Dinge kompiliert werden
</source>

Mit dem python3.5 kommt folgende Fehlermeldung:

"Failed building wheel for pillow", der folgende Test-Code funktionierte aber trotzdem.

Beispiel-Code: http://pythonhosted.org/arcade/examples/index.html

Davon "Sprites: move with walls" herausgepickt:
* http://pythonhosted.org/arcade/examples/sprite_move_walls.html
* 1. Innerhalb der aktivierten virtualenv arbeiten
* 2. Code in sprites1.py kopieren
* 3. Zwei Bild-Dateien images/boxCrate_double.png und images/character.png erstellen
* 4. <tt>python sprites1.py</tt>
* 5. Ein Fenster erscheint, in dem man mit einer kleinen Figur gegen Mauern laufen kann.

=== PySDL2 ===

NOCH NICHT ausprobiert

https://pysdl2.readthedocs.io/en/rel_0_9_4/index.html

* Installation: https://pysdl2.readthedocs.io/en/rel_0_9_4/install.html
** Python 2.7, 3.2+, PyPy 1.8.0+, SDL2_*
* Tutorials: <source>
    Hello World
        Importing
        Window creation and image loading
        Making the application responsive
        And here it ends...
    The Pong Game
        Getting started
        Adding the game world
        Moving the ball
        Bouncing
        Reacting on player input
        Improved bouncing
        Creating an enemy
        Next steps
    PySDL2 for Pygamers
        Technical differences
        API differences
</source>

=== Qt/QPainter ===

==== zetcode.com Tetris ====

* http://zetcode.com/gui/pyqt5/tetris/
* Code kopieren und qt-zetcode-tetris.py speichern und ausführbar machen
* <tt>./qt-zetcode-tetris.py</tt>
** Funktioniert und das Fenster ist sogar skalierbar.

=== kivy ===

* "open source Python library for developing mobile apps and other multitouch application software with a natural user interface (NUI). It can run on Android, iOS, Linux, OS X, and Windows" (https://en.wikipedia.org/wiki/Kivy)
** MIT license, developed by the Kivy organization
** Spezielle Kv language - ähnlich zu QML
** sieht für mich wie eine Qt-Alternative aus
* Tutorial: Pong Game: https://kivy.org/docs/tutorials/pong.html
* Installation für openSUSE <tt>sudo zypper install python-Kivy</tt>
* Adhoc-Test schlug fehl:
** <tt>python kivy1.py</tt> -> "Fatal Python error: (pygame parachute) Segmentation Fault"
** <tt>python3 kivy1.py</tt> -> "ImportError: No module named 'kivy'"
** todo: mit virtualenv probieren


5. Unsortiert - Externe Materialien
-----------------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Unsortiert/Externe_Materialien

== Unsortierte Materialsammlungen ==

Unsortierte, möglichst deutschsprachige Materialien.

* '''zum.de''' -> https://www.zum.de, https://wiki.zum.de/wiki/Informatik
** [https://wiki.zum.de/wiki/Programmieraufgaben Programmieraufgaben] -- '''Gute, einfache Ideen'''
*** [https://wiki.zum.de/wiki/Wiki_als_Programmierumgebung Wiki als Programmierumgebung]
*** [http://programmingwiki.de/SQL_Bankraub SQL_Bankraub auf programmingwiki.de]
** [https://wiki.zum.de/wiki/Python Python]
*** [http://cscircles.cemc.uwaterloo.ca/de/ Computer Science Circles (Deutsch) Interaktive Python3-Kurs]
*** [http://www.wspiegel.de/pykurs/index.html wspiegel.de-Python2-Kurs]
** [https://wiki.zum.de/wiki/Gefahren_im_Internet Gefahren im Internet]
*** [https://wiki.zum.de/wiki/Gefahren_im_Internet/Modul_Video_und_Audio/Streams Streaming? Was ist das eigentlich? Ist das überhaupt legal?] --> z. B. bei YouTube: ja sagt [http://www.legal-box.de/musik/index.php?contentid=20 legal-box.de] (leider zeigt das verlinkte ein Erklär-Video einen Browser ohne Werbeblocker und das empfohlende Programm ist noch nicht mal frei)
*** [https://wiki.zum.de/wiki/Gefahren_im_Internet/Modul_Pers%C3%B6nliche_Daten Persönliche Daten]
*** [https://wiki.zum.de/wiki/Gefahren_im_Internet/Modul_Pers%C3%B6nliche_Daten/Anonymit%C3%A4t Anonymität]
*** [https://wiki.zum.de/wiki/Gefahren_im_Internet/Modul_Kreativ_im_Netz Kreativ im Netz]
** [https://wiki.zum.de/wiki/Wikieinsatz_im_Informatikunterricht Wikis im Informatikunterricht]
*** [https://wiki.zum.de/wiki/Kollaborative_Software#Editoren_.2F_Textverarbeitung Software zum gemeinsamen Arbeiten]
*** [https://wiki.zum.de/wiki/Unterrichtsreihe_Informatik_Grundlagen/Verschl%C3%BCsselung/Online-Aufgaben Online-Aufgaben Verschlüsselung]

* [http://www.opentechschool.org '''opentechschool.org''']
** [http://opentechschool.github.io/python-beginners/de/index.html Python3-Einführung]
** nur Englisch: [http://opentechschool.github.io/python-flask/ Websites with Python Flask]
** [http://learn.opentechschool.org/ Übersicht]

* Materialsammlung der '''Bergischen Universität Wuppertal''':
** Material für die Sekundarstufe I: http://ddi.uni-wuppertal.de/material/materialsammlung/mittelstufe.html (siehe Menü links, z. B. [http://ddi.uni-wuppertal.de/material/materialsammlung/mittelstufe/binaer/ab_01_rechnen_im_binaersystem.pdf Binäre Zahlen]
** z. B. für die Qualifikationsphase 1: http://ddi.uni-wuppertal.de/material/materialsammlung/klp/q1.html (Python, Listen, Queues, Stack, Suchen, Sortieren)
** Übersicht: http://ddi.uni-wuppertal.de/material/materialsammlung/material/aktuelles.html
* Materialsammlung des '''Gymnasium Odenthals'''
** Übersicht: http://projekte.gymnasium-odenthal.de/informatik/index.php?site=dateien
** z. B. Informatik/Jahrgangsstufe 08-09/Unterrichtsreihen/03 Binaerzahlen
*** [http://projekte.gymnasium-odenthal.de/informatik/dateien/Informatik/Jahrgangsstufe%2008-09/Unterrichtsreihen/03%20Binaerzahlen/Dokumente-Arbeitsblaetter/Skript%20Binaersystem.pdf Skript], [http://projekte.gymnasium-odenthal.de/informatik/anzeige.php?site=dateien&type=pdf&dateiname=Arbeitsblatt%20zu%20Binaerzahlen.pdf&verlauf=Informatik/Jahrgangsstufe%2008-09/Unterrichtsreihen/03%20Binaerzahlen/Dokumente-Arbeitsblaetter/Arbeitsblatt%20zu%20Binaerzahlen.pdf Arbeitsblatt]
** z. B. Fortbildungen: Compilerbau, Datenbanken, Netzwerke
* Materialsammlung des '''Schülerlabors der RWTH Aaachen'''
** Einstiegsseite: http://schuelerlabor.informatik.rwth-aachen.de/
** Module, laut Webseite mindestens 2 Tutoren nötig: http://schuelerlabor.informatik.rwth-aachen.de/module
** z. B. Station 1 - Wie funktioniert das Internet, Dezimalzahlen: http://schuelerlabor.informatik.rwth-aachen.de/sites/default/files/dokumente/Station1-Arbeitsblatt.pdf
* Arbeitsblattsammlung des '''Chemnitzer Schulmodells''':
** http://www.schulmodell.eu/informatik/279-arbeitsblaetter-informatik.html
* '''Wikiversity'''
** Schulprojekte: https://de.wikiversity.org/wiki/Wikiversity:Schule
** z. B. Kurs:Internet und Verschluesselung: https://de.wikiversity.org/wiki/Kurs:Internet_und_Verschluesselung (Ethernet, ...)
** z. B. Elektrotechnik Grundbildung/Übungsaufgaben: https://de.wikiversity.org/wiki/Schulprojekt:_Elektrotechnik_Grundbildung/%C3%9Cbungsaufgaben
** z. B. Python-Quiz: https://de.wikiversity.org/wiki/Python/Quizzes
* '''Self-Linux'''
** http://www.selflinux.org/selflinux/index.html
* Geschlossene, integrierte Angebote, Login erforderlich
** https://open.hpi.de/courses/pythonjunior2014 - Programmieren mit Python lernen (inkl. Quiz etc.) (noch nicht ausprobiert)


5. Unsortiert - Weitere Ideen
-----------------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Unsortiert/Weitere_Ideen (DEL)

== neu ==
* Arbeitsblatt: '''Quellcode-Suchspiel'''
** von bekannten Programmen den aktuellen Quellcode finden lassen, falls vorhanden und dann ein Lösungswort rausfinden (supertuxcart oder cowsay, z. B.)

* Webapplikationen mit '''Flask''':
** [https://www.fullstackpython.com/flask.html Flask auf fullstackpython.com]
*** [https://www.fullstackpython.com/web-development.html Was ist Webentwicklung?], inkl. Links zum Deployment
*** [https://www.reddit.com/r/flask/comments/2321oc/easiest_and_fastest_way_to_host_flask_python/ Ways to host Flask Python]
** [https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world The Flask Mega-Tutorial, Part I: Hello, World!], Umfangreiches Material zu Flask
* [https://library.oreilly.com/book/0636920031116/flask-web-development/43.xhtml?ref=toc Flask Web Development Buch]
* [https://jeffknupp.com/blog/2014/01/18/python-and-flask-are-ridiculously-powerful/ Python and Flask Are Ridiculously Powerful], Blog-Eintrag
* [https://www.quora.com/Should-I-learn-Flask-or-Django Should I learn Flask or Django?] (Flask ist einfacher, Django hat größere Community)

* Unterschied zwischen '''Arbeitsspeicher''' (RAM, flüchtig) und persistentem Speicher erklären.

* Etwas ähnliches wie [http://www.jibble.org/netdraw.php NetDraw]

* Idee: einen einfachen '''Streckeneditor''' für Ultimate Stunts programmieren (siehe z. B. /usr/share/ultimatestunts/tracks/hill.track) mit dem Fokus auf '''schnelles Erzeugen von coolen Strecken'''. Das könnte heißen:
** Mit Python/Qt
** Das Editieren findet immer in der Draufsicht statt. Z-Dimension z. B. anhand von Zahlen kenntlich machen.
*** Kann man überhaupt Straßen übereinander bauen?
*** Wenn ja, dann könnte das in einer zweiten Ausbaustufe geschehen: zusätzlich zu den Zahlen (die nur bei Nicht-Überlagerung funktionieren), könnte man einen Ebenen-Selektor einbauen.
** Toolbar oder Combobox mit einer Menge von vorher definierten Tiles
** Kein Fokus auf Landschaft, sondern auf die Strecken
** Feinheiten müssen mit den eingebauten Stunts-3D-Editor nachbearbeitet werden
** Gebaute Strecken können an den Autor geschickt werden, damit er sie für alle verfügbar ins Spiel einbaut.

* '''Betriebssystemprogrammierung:''' http://wiki.osdev.org/Main_Page

6. Org Meta - Meta1
-------------------
https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Org_Meta/Meta1 (DEL)

In diesem Kurs wird [https://fsfe.org/about/basics/freesoftware.de.html Freie Software] verwendet.

Siehe auch [https://de.wikipedia.org/wiki/Freie_Software Wikipedia]; nicht zu verwechseln mit proprietärer [https://de.wikipedia.org/wiki/Freeware Freeware].

Ziele des "Python unter Linux"-Kurses sind:
* Lernen wie man ein '''freies Betriebssystem''' installiert und verwendet (am Beispiel openSUSE Linux)
** dabei sollen aktuelle Software-Versionen verwendet werden
** didaktisches Konzept:
*** Schritt-für-Schritt-Anleitungen ('''deutsche Sprache'''), die schnell zum Ziel führen, um die am Anfang steile Lernkurve möglichst gut überwinden zu können
*** Einzelne Bausteine ('''Methoden und Programme''') im Verlauf des Kurses wiederholen oder hinzufügen
* '''Lernen einer Programmiersprache''' am Beispiel Python (multi-paradigmatisch: objekt-orientiert, funktional, aspekt-orientiert; dynamisch typisiert; [https://www.heise.de/newsticker/meldung/Programmiersprache-Python-3-6-ist-erschienen-3579846.html Python wird verstärkt als erste Sprache eingesetzt und hat hohe Praxisrelevanz] (heise 2016))
** dabei sollen aktuelle Versionen verwenden werden
** dabei sollen möglichst deutsche Tutorials zum Einsatz kommen; da dies nicht immer möglich ist, werden vorhandene (meist englische) '''Tutorials mit Bearbeitungshinweisen''' ausgestattet)
** dabei soll mit Tools gearbeitet werden, die auch in der freien Software-Entwicklung zum Einsatz kommen (also Verständlichmachen von '''vorhandenen, aktiv entwickelten Tools''')
** Zusammenspiel von eigenen Programmen mit dem Betriebssystem
* Motivation erzeugen, mit dem Gelernten '''kreativ zu werden''':
** freie Software für eigene Zwecke verwenden
** eigene Problemstellungen mit Hilfe des Programmierens lösen
** Möglicher Einstieg in die Mit-Entwicklung freier Software (z. B. unter KDE mit C++/Qt), also ein selbst verwendetes Programm in Kooperation mit dem oder den ursprünglichen Autoren verbessern.
* Das '''Material''' soll eigenständiges Arbeiten und unterschiedliche Lerngeschwindigkeiten unterstützen.
* Grundstein legen, damit die Lernenden sich in einer Welt informiert orientieren können, die zunehmend und in immer mehr Lebensbereichen Software und Digitaltechnik einsetzt.

Ausgewählte Aspekte freier Software [http://blit.org/2011/zeitplan/attachments/65_Potentiale%20Freier%20Software%20in%20der%20Schule.pdf (1)]:
* freies Nutzen
* freies Studieren
* freies Teilen (=> Heterogenität, sozialer Ausgleich, didaktischer Vorteil)
* Verbessern erlaubt
* Voraussetzung für wirksamen '''Datenschutz''' und '''Privatsphäre'''
* Digitale Selbstbestimmung
** z. B. Eigenkontrolle über die eigenen Daten auf dem eigenen Rechner
** z. B. sind Algorithmen einsehbar und veränderbar (dadurch wird unbemerkter Missbrauch schwieriger)
* Vertrauen (z. B. durch unabhängige und öffentliche Kontrolle)
* gemeinnützig
* demokratische Prinzipien bei der gemeinsamen Entwicklung (offener Diskurs, Teilhabe nach Fähigkeit und Talent)

Potentiale für öffentliche Bildungsträger:
* Unterstützt folgende pädagogischen Konzepte: Offenheit, Transparenz, Mitbestimmung, demokratische Prinzipien
* keine Lizenzkosten (wichtiger Hinweis: Softwareentwicklung ist nicht kostenlos; derzeit wird daher für die Entwicklung oft mit Spenden gearbeitet; kommerzielle Dienstleistungen z. B. zur Wartung sind möglich)
* Unabhängigkeit von Partikularinteressen gewisser Softwarehersteller
* '''Werbefreiheit'''
* [http://blit.org/2011/zeitplan/attachments/65_Potentiale%20Freier%20Software%20in%20der%20Schule.pdf Vortragsfolien: Potenziale Freier Software in der Schule, 2011]
* Beispiele:
** [http://www.rhs-dreieich.de/unterrichtsangebote/faecher/informatik/ Ricarda Huch Schule in Dreieich]

Potentiale für Eltern:
* Erstklassige Software ohne Lizenzkosten, inkl. einer Menge Spiele
* Sicherheit: Freie Software ist standardmäßig spyware-frei (und derzeit weitgehend virenfrei)
* Datenschutz und Privatsphäre auf dem privaten Rechner (im Gegensatz zu z. B. [http://www.heise.de/ix/meldung/ISD-2016-Windows-10-uebermittelt-haufenweise-persoenliche-Daten-3329967.html "Windows 10 übermittelt haufenweise persönliche Daten"], 2016)
** Kinder können sich einbringen, indem sie dabei helfen, die Heim-Rechner mit freier SW auszustatten.

Weiteres:
* https://de.wikipedia.org/wiki/Open-Source-Software_in_%C3%B6ffentlichen_Einrichtungen
* https://de.wikipedia.org/wiki/LiMux
* http://www.deutschlandfunk.de/computer-und-kommunikation.683.de.html
* http://blog.do-foss.de/freie-software/vorteile/
* http://www.autenrieths.de/links/linkslix.htm
* Auch für Firmware gilt: je offener, desto besser: http://www.tagesschau.de/ausland/spionage-cctv-101.html, 2016

Erste Schritte für Basisnutzung des Computers:
* LibreOffice als Office-Paket verwenden (Texte schreiben, Tabellenkalkulation, Präsentation)
* Mozilla Firefox als Internet-Browser verwenden
