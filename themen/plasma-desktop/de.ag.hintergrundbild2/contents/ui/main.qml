import QtQuick 2.0
import QtMultimedia 5.0
import QtQuick.Controls 1.5
import QtQuick.Extras 1.4

Item {
    Rectangle {
        id: rectangle1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }

            GradientStop {
                position: 1
                color: "#0725ca"
            }
        }

        // http://doc.qt.io/qt-5/qtquick-positioning-anchors.html

        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0

        anchors.fill: parent

        Rectangle {
            x: 79
            y: 107
            width: 570
            height: 228
            color: "#00000000"
            border.width: 0
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

            AnimatedImage {
                id: animation;
                antialiasing: false;
                rotation: 0;
                source: "open-suse-welcome.gif";
                //currentFrame: dial1.value
                currentFrame: slider1.value
                playing: checkBox1.checked

                // http://doc.qt.io/qt-5/qml-qtquick-mousearea.html
                MouseArea {
                    hoverEnabled: true // funktioniert nicht, wenn als Plasma-Hintergrund verwendet wird
                    // maybe see here: https://stackoverflow.com/questions/47451218/querying-global-mouse-position-in-qml

                    anchors.fill: parent
                    onPositionChanged: {
                        console.log("mouseX: " + mouseX + ", mouseY: " + mouseY)
                        mr.x = mouseX - 5
                        mr.y = mouseY - 5
                    }
                    onClicked: {
                        console.log("CLICK")
                    }
                }

                Rectangle {
                    id: mr
                    x: 0
                    y: 0
                    width: 10
                    height: 10
                }

                TextInput {
                    id: textInput1
                    x: -17
                    y: 100
                    width: 215
                    height: 20
                    text: qsTr("Hallo?")
                    horizontalAlignment: Text.AlignRight
                    font.pixelSize: 12
                }

                Dial {
                    id: dial1
                    x: 440
                    y: 37
                    tickmarksVisible: true
                    value: animation.currentFrame
                    minimumValue: 0
                    maximumValue: 51 // animation.frameCount
                    stepSize: 1
                    onValueChanged: {
                        console.log("hallo value: " + value)
                    }
                }

                Text {
                    id: text1
                    x: 327
                    y: 21
                    width: 243
                    height: 17
                    text: "control rotation or background gradient"
                    font.pixelSize: 12
                }

            }

            CheckBox {
                id: checkBox1
                x: 0
                y: 217
                text: qsTr("Animation")
                checked: true
            }

//         Rectangle {
//             property int frames: animation.frameCount
//
//             width: 4; height: 8
//             x: (animation.width - width) * animation.currentFrame / frames
        //             y: animation.height
        //             color: "red"
        //         }
        }

        Button {
            id: button1
            x: 25
            y: 27
            text: qsTr("Button")
            rotation: 0
        }

        Slider {
            id: slider1
            x: 394
            y: 360
            minimumValue: 0
            maximumValue: 51 // animation.frameCount
            stepSize: 1
            value: animation.currentFrame
        }

    }

//    Connections {
//        target: animation
//        onClicked: console.log("anim clicked") // print("clicked")
//    }
}

