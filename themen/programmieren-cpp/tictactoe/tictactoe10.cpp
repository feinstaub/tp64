#include <iostream>
#include <vector>
#include "tictactoe10.h"
#include "tools.h"
#include "game.h"
#include "tencurses.h"

using namespace tc;

namespace game {

std::vector<std::string> banner;
Field f0;
Field f1;
Form* tcform;

// todo: use exceptions https://isocpp.org/wiki/faq/exceptions
bool load_data()
{
    const char* banner_file = "ttt10/banner.txt";
    std::fstream file(banner_file, std::ios::in);
    if (!file.good())
    {
        printf("Error loading file %s\n", banner_file);
        return false;
    }

    std::string s;
    while (!file.eof() && file.good())
    {
        getline(file, s);
        banner.push_back(s);
    }

    file.close();

    return true;
}

void display_welcomescreen(TenCurses& tc)
{
    //
    // read player field positions
    //
    int y = 0;
    int p = 0;
    for (auto& line : banner) {
        int len = 0;
        int x = find_x_pos(&len, line);
        if (x >= 0) {
            if (p == 0) {
                f0.y = y;
                f0.x = x;
                f0.w = len;
            } else if (p == 1) {
                f1.y = y;
                f1.x = x;
                f1.w = len;
            } else {
                // too much # lines
            }

            p++;
        }
        y++;
    }

    //
    // form must be created first; then other output
    //
    tcform = &tc.create_form({ f0, f1 });

    //
    // print banner text
    //
    tc.move(0, 0);
    for (auto& line : banner) {
        tc.printw("%s\n", line.c_str());
    }

    //
    // make forms visible again
    //
    tcform->wa_make_visible();
}

} // game

int main()
{
    tools::random_init();

    printf("Loading...\n");
    if (!game::load_data()) {
        printf("Loading failed. Exit 1.");
        return 1;
    }

    TenCurses tc;
    tc.init();

    game::display_welcomescreen(tc);

    int c = 0;
    bool finish = false;
    while (!finish && (c = tc.getch()) != ERR)
    {
        switch (c)
        {
        case KEY_F(10):
            finish = true;
            break;

        default:
            game::tcform->driver(c);
            break;
        }
    }

    printf("Good bye...\n");
    return 0;
}
