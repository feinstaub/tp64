X-Server-for-Windows
====================

<!-- toc -->

- [Einloggen auf den Klassenserver vom Windows-Rechner](#einloggen-auf-den-klassenserver-vom-windows-rechner)
  * [Phase 1: Windows-Rechner vorbereiten](#phase-1-windows-rechner-vorbereiten)
  * [Phase 2: Nutzerkonto erstellen](#phase-2-nutzerkonto-erstellen)
  * [Phase 3: Test](#phase-3-test)
- [Probleme mit Qt-Programmen?](#probleme-mit-qt-programmen)
- [X-Server mit Putty bereitstellen](#x-server-mit-putty-bereitstellen)
  * [VcXsrv.zip](#vcxsrvzip)
  * [Alt](#alt)
- [Cygwin](#cygwin)
- [xrdp](#xrdp)
- [X2Go](#x2go)

<!-- tocstop -->

Einloggen auf den Klassenserver vom Windows-Rechner
---------------------------------------------------
EINMALIG:

### Phase 1: Windows-Rechner vorbereiten
(nicht nötig für Linux)

1. Windows-Explorer: \\ag-pc\public
    * **VcXsrv.zip** auf den Rechner kopieren und entpacken.
2. Doppelklick **xlaunch.exe**
    * Firewall-Meldung ignorieren
    * **Fullscreen** (früher **One Large Window** oder **Multiple Windows**) auswählen
    * rechte Maustaste auf das **X-Icon neben der Uhr**
    * **New putty session** auswählen
3. Putty ist offen
    * Bei **Host or IP address** folgendes eingeben: ag-pc oder IP 192.168.178.33 oder 10.2.133.168 oder ...
    * ggf. Fingerprint vergleichen: "ssh-rsa 2048 15:ea:a7:14:91:0f:ae:9b:d3:45:fb:04:f1:16:89:8f" und bestätigen
    
### Phase 2: Nutzerkonto erstellen
1. Login as **ag** (Tutor-Passwort)
2. Benutzernamen aussuchen: "Max Mustermann" --> "maxm"
3. Befehle ausführen: siehe [admin-new-student-account.md](admin-new-student-account.md)
    * $ konsole # besseres Terminal, sonst werden Umlaute ggf. nicht richtig angezeigt
    * siehe admin-debian-student-account.md
    * ag AUSLOGGEN

### Phase 3: Test
* Stelle sicher, dass der X-Server läuft (**xlaunch.exe**)
* Mit **putty.exe**: Logge dich auf ag-pc ein (ggf. passende IP-Adresse in /etc/hosts eintragen)
    * mit deinem User und Passwort
    * putty: **Profil speichern**
* **konsole** starten
    * Mit Strg++ die Schriftgröße erhöhen
* oder: **xfce4-session**

Probleme mit Qt-Programmen?
---------------------------
siehe QT_QPA_PLATFORMTHEME unter admin-main.md

X-Server mit Putty bereitstellen
--------------------------------
### VcXsrv.zip
* VcXsrv.zip herunterladen (45 MB)
    * von http://www.scrc.umanitoba.ca/doc/tutorial/T19_3a_xmingputty.htm "Xsrv+PuTTY on a Stick"
    * siehe ~/Downloads
    * Entpackt (80 MB, 5000 Dateien)

Datei auf odroid bereitstellen, siehe z. B. admin-odroid-samba-freigabe.md

    $ scp VcXsrv.zip ag@ag-pc:~/samba_share/public

Entpackte Version bereitstellen
(falls Ausführung von Netzlaufwerken erlaubt ist oder kein Unzip-Programm da ist)

* Login mit ag
* $ cd ~/samba_share/public
* $ mkdir VcXsrv
* $ cd VcXsrv
* $ unzip ../VcXsrv.zip

### Alt
* Selber Cygwin installieren und zippen
    * zu groß: über 200 MB; geht sicher auch kleiner
* http://www.straightrunning.com/xmingnotes/portable.php
    * ging nicht, weil man Passwort braucht, siehe http://www.straightrunning.com/XmingNotes/
* https://sourceforge.net/projects/portable-x-server/files/
    * nicht probiert; kein putty dabei
* https://sourceforge.net/projects/vcxsrv/
    * X-Server für Windows mit Installer, todo später

Cygwin
------
TODO unter Windows:

1. Cygwin runterladen, installieren, ausprobieren
2. Installation zippen --> "portable"
3. Entpacken auf einem anderen Rechner

xrdp
----
* http://www.admin-magazin.de/Das-Heft/2009/02/Mit-Windows-Clients-auf-Linux-Server-zugreifen
* http://www.xrdp.org/ (siehe auch http://www.rdesktop.org/)
* sudo zypper install xrdp
* sudo service xrdp start
* /sbin/ifconfig -> IP
* Firewall AUS
* Connect mit Windows-Remote-Desktop auf IP
* Einloggen mit u1 (und z. B. PlanetPenguinRacer starten)

* Script: tool-network-xrdp-start (todo: Link to codestruct-util)

X2Go
----
* "X2Go ist eine freie Terminalserver-Lösung. Benutzer, die sich mit dem Client an einem X2Go-Server angemeldet haben, können Programme auf dem Server ausführen." (https://de.wikipedia.org/wiki/X2Go)
* TODO: Ausprobieren:
    * https://wiki.x2go.org/doku.php/doc:newtox2go
    * https://wiki.x2go.org/doku.php/doc:faq:start
    * Siehe auch [ODROID als lokaler Server](../odroid-server)
        * Vielleicht Server auf dem odroid installieren
    * Windows-Client: https://wiki.x2go.org/doku.php/download:start
