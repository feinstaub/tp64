#include <string>
#include <iostream>
#include <vector>

using namespace std;

struct grafic{
    string zeichen;
    int poss1;
    int poss2;
    int next;
};

// TODO: globale Variablen sollten weg
int out;
int pack;
int packcount;
string mod2;
string eingabe; // TODO: pot. problem
string eingabe2;

int hangman(int pack, int packcount,string mod2){

    vector<string>words[]={{"atomreaktor",//atom
                          "kernspaltung",
                          "greminum",
                          "kühlkreißlauf",
                          "tschernobel"},

                         {"strand am meer",//rund ums Mehr
                          "sandburgen bauen",
                          "algenplantage",
                          "sushi am band",
                          "seeigel"},

                         {"pflanzen gießen",//Pflanzen
                          "gute erde",
                          "tröpfchenbewässerung",
                          "fleischfressendepflanze",
                          "geldbaum"},

                         {"auszaehlung",//Us Wahlen
                          "praesident",
                          "wahlmaenner",
                          "staten",
                          "altes wahlsystem"},

                         {"oeffentliche verkehrsmittel",//transportmittel
                          "vakuumzug",
                          "flugzeugturbine",
                          "auf magneten schweben",
                          "oeffentliche verkehrsmittel",
                          "verspaetung bei der deutscher bahn"},

                         {"spuele",
                          "wasserkocher",
                          "tuetensuppe",
                          "Dunstabzugshaube saeubern",
                          "gesunde ploere aus dem mixer",}};



    vector<string> graf={"          ",
                         "          ",
                         "          ",
                         "          ",
                         "          ",
                         "          ",
                         "          "};

    vector<string> lose={"   ____   ",
                         "   |/ |   ",
                         "   | (o/  ",
                         "   |  |   ",
                         "  _|_/ )  ",
                         " /   )    ",
                         "/     )   "};
//     vector<string> lose2={"
//                           "
//                           "   o
//                           "  /|
//                           " __|__
//                           " //|))
//                           "///|)))

    vector<grafic> mittel={{"/",6,0,1},
                           {"_",5,1,1},
                           {"_",5,2,1},
                           {"_",5,3,1},
                           {"_",5,4,1},
                           {"_",5,5,1},
                           {")",6,6,0},

                           {")",5,5,1},
                           {"/",5,1,1},
                           {" ",5,2,1},
                           {" ",5,3,1},
                           {" ",5,4,1},
                           {"_",4,2,1},
                           {"_",4,4,0},

                           {"-",4,3,1},
                           {"-",4,4,1},
                           {"-",4,5,1},
                           {"-",4,6,0},

                           {" ",4,3,1},
                           {" ",4,4,1},
                           {" ",4,5,1},
                           {" ",4,6,1},
                           {"_",4,4,1},
                           {"/",4,3,1},
                           {"/",3,4,1},
                           {"/",2,5,1},
                           {"/",1,6,0},

                           {" ",4,3,1},
                           {" ",3,4,1},
                           {" ",2,5,1},
                           {" ",1,6,1},
                           {"|",4,3,1},
                           {"|",3,3,1},
                           {"|",2,3,1},
                           {"|",1,3,0},

                           {"/",1,4,0},

                           {"|",4,5,1},
                           {"|",3,5,1},
                           {"|",2,5,1},
                           {"|",1,5,0},

                           {" ",4,5,1},
                           {" ",3,5,1},
                           {" ",2,5,1},
                           {" ",1,5,1},
                           {"_",0,3,1},
                           {"_",0,4,1},
                           {"_",0,5,1},
                           {"_",0,6,0},

                           {"|",1,6,0},

                           {"(",3,6,1},
                           {"/",3,8,1},
                           {"|",4,7,1},
                           {"/",5,6,1},
                           {")",5,8,1},
                           {"o",3,7,0},

                           {" ",3,6,1},
                           {" ",3,8,1},
                           {" ",2,7,1},
                           {" ",5,6,1},
                           {" ",5,8,1},
                           {" ",3,7,1},
                           {"(",2,5,1},
                           {"/",2,7,1},
                           {"|",3,6,1},
                           {"/",4,5,1},
                           {")",4,7,1},
                           {"o",2,6,0}};

    vector<grafic> schwer={{"/",6,0,1},
                           {"/",5,1,1},
                           {")",5,5,1},
                           {")",6,6,1},
                           {"_",4,2,1},
                           {"_",4,4,0},
                           {"|",4,3,1},
                           {"|",3,3,1},
                           {"|",2,3,1},
                           {"|",1,3,0},
                           {"/",1,4,0},
                           {"_",0,3,1},
                           {"_",0,4,1},
                           {"_",0,5,1},
                           {"_",0,6,0},
                           {"|",1,6,0},
                           {"(",2,5,1},
                           {"/",2,7,1},
                           {"|",3,6,1},
                           {"/",4,5,1},
                           {")",4,7,1},
                           {"o",2,6,0}};

    vector<grafic> einfach={{"/",6,0,0},
                           {"/",5,1,0},
                           {")",5,5,0},
                           {")",6,6,0},
                           {"_",4,2,0},
                           {"_",4,4,0},
                           {"|",4,3,0},
                           {"|",3,3,0},
                           {"|",2,3,0},
                           {"|",1,3,0},
                           {"/",1,4,0},
                           {"_",0,3,0},
                           {"_",0,4,0},
                           {"_",0,5,0},
                           {"_",0,6,0},
                           {"|",1,6,0},
                           {"(",2,5,0},
                           {"/",2,7,0},
                           {"|",3,6,0},
                           {"/",4,5,0},
                           {")",4,7,0},
                           {"o",2,6,0}};
    string mode=mod2;
    vector<grafic> galgen;
    int i=0;
    string eingabe;
    string ausgabe;
    string wort=words[pack][packcount];
    int groesse;
    int weiter=0;
    int c=1;
    int d=0;
    int win=0;
    string winausgabe;

    if(mode=="schwer"){
        galgen=schwer;
    }
    if(mode=="mittel"){
        galgen=mittel;
    }
    if(mode=="einfach"){
        galgen=einfach;
    }
    groesse=wort.size();//ausgabe einrichten
    for(int a=0;a<=groesse-1;a++){
        if(wort[a]==' '){
            ausgabe=ausgabe+" ";
        }
        if(wort[a]!=' '){
            ausgabe=ausgabe+"-";
        }
    }
    while(i==0){

        for(int a=0;a<=6;a++){
                for(int b=0;b<=9;b++){
                    cout << graf[a][b];
                }
                cout << endl;
            }
            cout << ausgabe << endl;

        if(wort==ausgabe){//winn
            winausgabe="Richtig das Spiel ist vorbei";
            win=1;
        }
        if(graf==lose){//lose
            win=1;
            winausgabe="Du hängst; keine Chance mehr";
        }

        if(win==0){

            cout << "Mache eine Eingabe:";
            cin >> eingabe;//eingabe
            cout << endl;
            weiter=0;
            if(eingabe=="stop"){
                win=1;
                winausgabe="Das Spiel wurde abgebrochen";
            }
            for(int a=0;a<=groesse;a++){//richtiger Buchstabe einsetzen
                if(wort[a]==eingabe[0]){
                    ausgabe[a]=wort[a];
                    weiter=1;
                }
            }

            if(weiter==0){//bei falscher Eingabe Galgen erweitern
                c=1;
                while(c==1){
                    graf[galgen[d].poss1][galgen[d].poss2]=galgen[d].zeichen[0];
                    c=galgen[d].next;
                    d++;
                }
            }
        }

        if(win==1){
            cout << winausgabe << endl;
            cout << "Das richtige Wort war: " << wort << endl;
            i++;
        }

    }
    return 0;
}

void hangman_start()
{
    packcount=0;
    cout << "Welchen schwierigkeitsgrad wählst du?:";
    cin >> eingabe;
    mod2=eingabe;
    cout << "\n";
    cout << "Welches Wortsetwählst du?(1-5):";
    cin >> pack;
    pack=pack-1;
    if(pack<0){
        pack=0;
    }
    if(pack>5){
        pack=5;
    }
    out=hangman(pack,packcount,mod2);
    for(int a=1;a<=4;a++){
        packcount=a;
        cout << "Möchtest du fortfahren?(y|n):";
        cin >> eingabe;
        cout << endl;
        if(eingabe=="y"){
            out=hangman(pack,packcount,mod2);
        }
        else{
            a=10000;
        }
    }
}
