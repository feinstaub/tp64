#pragma once

#include <memory> // unique_ptr
#include <vector>

#include <curses.h>
//#include <ncurses/curses.h> // to make constants available to user, e.g. ERR, OK, KEY_F(1) etc.

#undef getch

namespace tc {

/**
 * see Form
 */
struct Field
{
    // same order as in new_field
    int h = 1;
    int w = 10;
    int y = 0;
    int x = 0;
};

class FormImpl;

/**
 * https://de.wikibooks.org/wiki/Ncurses:_Formulare
 */
class Form
{
public:
    explicit Form(const std::vector<Field>& fields);
    ~Form();

    int driver(int c);

    /**
     * workaround to make all fields visible
     * in case other drawing operations took place
     */
    void wa_make_visible();

private:
    std::unique_ptr<FormImpl> impl;
};

/**
 * NCurses-Wrapper
 */
class TenCurses
{
public:
    /**
     * Calls endwin() and cleanup everything else
     */
    ~TenCurses();

    /**
     * Init before first use
     */
    void init();

    /**
     * 1. End ncurses
     * 2. Pause nurses; continue with refresh()
     */
    void endwin();

    /**
     * return ERR upon failure
     */
    int refresh();

    /**
     * Cursor-Sichtbarkeit
     *
     * 0: invisible             - empfohlen, wenn keine Texteingabe erforderlich
     * 1: normal                - empfohlen für Form-Modus
     * 2: very visible          - ?
     */
    int curs_set(int state);

    /**
     * move curses window cursor to (y, x)
     */
    void move(int y, int x);

    /**
     * printw - print in curses *w*indows
     */
    void printw(const char *fmt...);

    /**
     * Wie printw, nur verschiebt Cursor nach (y, x)
     */
    void printw(int y, int x, const char *fmt...);

    /**
     * see man getch
     */
    int getch();

    /**
     * sleep for ms milliseconds
     */
    void napms(int ms);

    Form& create_form(const std::vector<Field>& fields);

private:
    std::unique_ptr<Form> form_;
};
} // namespace tc

/**
 * EXAMPLES

 EXAMPLE "ncurses temporär abschalten"

    while ((c = tc.getch()) != ERR)
    {
        //printf("%c-", c); // Enter bringt dann Probleme; darf nicht mit ncurses gemischt werden
        tc.printw("%c", c); // funktioniert wie printf

        if (c == 'a') {
            tc.endwin(); // ncurses temporär abschalten
            terminal::clear();
            printf("Hallo\n");
            tc.napms(1 * 1000); // The napms routine is used to sleep for ms milliseconds.
            tc.refresh(); // "Der ncurses-Modus kann durch endwin auch zeitweilig unterbrochen werden. Die Rückkehr zum ncurses-Modus erfolgt durch eine Refresh-Anweisung."
        }
    }

 */
