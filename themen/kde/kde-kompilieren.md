KDE kompilieren
===============

<!-- toc -->

- [Voraussetzungen](#voraussetzungen)
- [Versionsverwaltung konfigurieren: Git remote prefix](#versionsverwaltung-konfigurieren-git-remote-prefix)
- [Arbeitsverzeichnis](#arbeitsverzeichnis)
- [Einige Pakete installieren](#einige-pakete-installieren)
- [kdesrc-build installieren und konfigurieren](#kdesrc-build-installieren-und-konfigurieren)
  * [kdesrc-build installieren](#kdesrc-build-installieren)
  * [kdesrc-build aktualisieren](#kdesrc-build-aktualisieren)
  * [kdesrc-build konfigurieren](#kdesrc-build-konfigurieren)
  * [EINMALIG: Helfer-Scripte bereitlegen](#einmalig-helfer-scripte-bereitlegen)
- [Mit kdesrc-build ein Projekt bauen](#mit-kdesrc-build-ein-projekt-bauen)
  * [Beispiel 1: KCalc bauen](#beispiel-1-kcalc-bauen)
  * [Warum . dev-kf5-env-setup?](#warum--dev-kf5-env-setup)
  * [Beispiel 2: KDevelop bauen](#beispiel-2-kdevelop-bauen)
- [Ein gebautes Projekt ausführen](#ein-gebautes-projekt-ausfuhren)
- [Mehr](#mehr)

<!-- tocstop -->

Voraussetzungen
---------------
* [Eine aktuelle Version von openSUSE-Tumbleweed installieren](../linux-installieren/opensuse-auf-usb-stick)
* Darauf achten, dass (je nach Größe der Projekte) **40 GB freier Festplattenspeicher** auf /home frei sind.
    * todo: howto freien Speicher prüfen
* Wichtige Quelle von Informationen: https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source

Versionsverwaltung konfigurieren: Git remote prefix
---------------------------------------------------
(EINMALIG)

* git installieren
    * `$ sudo zypper install git`
* Schnellzugriff zu KDE-Repositories konfigurieren
    * ~/.gitconfig öffnen
    * Die 4 Zeilen ergänzen, die hier stehen: https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source#Git_remote_prefix
    * Speichern

Arbeitsverzeichnis
------------------
* Das Arbeitsverzeichnis für alle Entwicklungsaktivitäten für KDE legen wir auf **~/kde** fest.
    * Dieses Verzeichnis erzeugen: `$ mkdir ~/kde`

Einige Pakete installieren
--------------------------
* Damit später möglichst wenige fehlende Pakete nachinstalliert werden müssen, installieren wir erst einmal die empfohlene Menge an Paketen:
    * siehe https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Install_the_dependencies#openSuSE_Tumbleweed


kdesrc-build installieren und konfigurieren
-------------------------------------------
(EINMALIG)

**kdesrc-build** ist ein Werkzeug, mit dem der Quellcode von KDE-Projekten heruntergeladen und gebaut werden kann.


### kdesrc-build installieren

Eine Konsole öffnen und folgendes der Reihe nach eingeben:

```
mkdir -p ~/kde/src
cd ~/kde/src
git clone kde:kdesrc-build
```

Feststellen, dass kdesrc-build noch nicht "im Pfad" ist:

```
$ kdesrc-build
If 'kdesrc-build' is not a typo you can use command-not-found to lookup the package that contains it, like this
```

kdesrc-build dauerhaft in den Pfad aufnehmen:

* Das Verzeichnis **~/bin** erzeugen, falls nicht schon vorhanden.
* Einen symbolischen Link anlegen:
    * `ln -s ~/kde/src/kdesrc-build/kdesrc-build ~/bin`
* Dafür sorgen, dass ~/bin in der PATH-Variable ist
    * todo --> ShellTux

Prüfen, ob kdesrc-build im Pfad ist:

```
$ which kdesrc-build
$ /home/USER/bin/kdesrc-build
```
OK.


### kdesrc-build aktualisieren

Nach längerer Entwicklungspause kann kdesrc-build hiermit aktualisiert werden:

    $ cd ~/kde/src/kdesrc-build
    $ git pull


### kdesrc-build konfigurieren

```
$ ~/kde/src/kdesrc-build
$ ./kdesrc-build-setup
```

Ein Wizard erscheint. Folgende Fragen beantworten:

* See the tutorial?
    * No
* Do you have commit access?
    * Standard verwenden: Yes (todo: welchen Unterschied macht Ja oder Nein?)
* HTTP-Proxy?
    * No
* Where to install Software?
    * Standard verwenden: home (~/kde-latest)
* Which module groups?
    * Standard verwenden: frameworks, workspace, base
* How many CPU cores to use for building?
    * Standard verwenden: 4

Folgende Datei ist nun erstellt worden: ~/.kdesrc-buildrc

In der **~/.kdesrc-buildrc** folgendes ggf. **manuell anpassen**, so dass es am Ende jeweils so aussieht:

* siehe auch https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source#Configure_kdesrc-build

```
    # The path to your Qt installation.
    # qtdir ~/qt5
    qtdir /usr # If system Qt
```

```
    # Install directory for KDE software
    # kdedir ~/kde-latest
    kdedir ~/kde/usr
```

```
    # Directory for downloaded source code
    # source-dir ~/kdesrc
    source-dir ~/kde/src
```

### EINMALIG: Helfer-Scripte bereitlegen
Folgende Scripte so hinlegen, dass sie im PATH liegen:

* https://cgit.kde.org/scratch/gregormi/codestruct-util.git/plain/dev-kde/dev-kf5-define-variables
* https://cgit.kde.org/scratch/gregormi/codestruct-util.git/plain/dev-kde/dev-kf5-env-setup

Beide Dateien herunterladen, unter ~/bin speichern und dev-kf5-env-setup ausführbar machen. ~/bin muss im PATH sein.

Mit kdesrc-build ein Projekt bauen
----------------------------------

### Beispiel 1: KCalc bauen

Wir bauen nun KCalc:

    $ cd ~/kde/src
    $ . dev-kf5-env-setup
    $ kdesrc-build kcalc

Das dauert ca. 1 Minute.

### Warum . dev-kf5-env-setup?
Wenn man es weglässt, sieht die erste Zeile von cmake.log so aus:

    # kdesrc-build running: 'cmake' '/home/gregor/kde/src/kde/kdegames/ksudoku' ... '-DCMAKE_INSTALL_PREFIX=/home/gregor/kde/usr' '-DCMAKE_PREFIX_PATH=/usr'

Da hat zur Folge, dass Module vom Systemverzeichnis gefunden werden:

    -- Found KF5Archive: /usr/lib64/cmake/KF5Archive/KF5ArchiveConfig.cmake (found version "5.44.0")

Wenn man es nicht weglässt, sieht es so aus:

    # kdesrc-build running: 'cmake' '/home/gregor/kde/src/kde/kdegames/ksudoku' ... '-DCMAKE_INSTALL_PREFIX=/home/gregor/kde/usr'
    ...
    -- Found KF5Archive: /home/gregor/kde/usr/lib64/cmake/KF5Archive/KF5ArchiveConfig.cmake (found version "5.46.0")

Das Problem tritt anscheinend nur bei Anwendungen auf. Bei Frameworks sehen die Log-Einträge anders aus (ohne Pfad beim "Found ..."). Trotzdem sicherheitshalber immer verwenden.

### Beispiel 2: KDevelop bauen
Mit dem folgenden wird die Entwicklungsumgebung KDevelop mit allen direkten Abhängigkeiten (es gibt zwar noch mehr Abhängigkeiten, aber mit einer aktuellen Distribution wie openSUSE Tumbleweed sollten diese alle aktuell genug sein) gebaut:

    $ cd ~/kde/src
    $ . dev-kf5-env-setup
    $ kdesrc-build grantlee libkomparediff2 kdevplatform kdevelop-pg-qt kdevelop

Das dauert ca. 30 Minuten.

Ein gebautes Projekt ausführen
------------------------------
    $ . dev-kf5-env-setup # falls nicht schon getan
    $ kcalc

Mehr
----
* siehe https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source
