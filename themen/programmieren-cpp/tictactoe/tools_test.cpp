#include <iostream> // cout, cin
#include "tools.h"
#include "game.h"

void terminal_test();           // Terminal: cursor left, down
void tools_homedir();           // Homedir herausfinden
void tools_make_config_dir();   // Ein Config-Dir erzeugen
void game_feld_array();

/**
 * Hauptprogramm
 */
int main() {
    //terminal_test();
    tools_homedir();
    tools_make_config_dir();
    game_feld_array();
    //tools::play_ogg_blocking("sounds/beltHandle1.ogg");
    return 0;
}

void terminal_test()
{
    terminal::clear();

    for (int c = 0; c < 5; c++) {
        std::cout << "a";
        terminal::cursor_left();
        terminal::cursor_down();
        // => 5 mal 'a' untereinander
    }

    terminal::cursor_down_fwrite();
    terminal::cursor_down_fwrite();
    std::cout << "b";
    terminal::cursor_down_fwrite();

    // ... ...
}

void tools_homedir()
{
    const char* homedir = tools::get_homedir();
    std::cout << "Homedir: " << homedir << std::endl;
}

void tools_make_config_dir()
{
    std::cout << "config dir: " << tools::make_config_dir("_test") << std::endl;
}

void game_feld_array()
{
    game::Feld feld;
    //feld.fill_with_numbers();
    feld.debug_print_feld();
}
