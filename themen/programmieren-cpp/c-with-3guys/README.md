3Guys
=====

<!-- toc -->

- [Einleitung](#einleitung)
- [Aufgabe 1: Bauen](#aufgabe-1-bauen)
  * [Bauen](#bauen)
  * [Ausführen](#ausfuhren)
  * [Einige Level spielen](#einige-level-spielen)
- [Aufgabe 2: Leveldateien](#aufgabe-2-leveldateien)
  * [Vorhandenes Level anpassen](#vorhandenes-level-anpassen)
  * [Neues Level bauen](#neues-level-bauen)
- [Spiel auf Deutsch übersetzen](#spiel-auf-deutsch-ubersetzen)
- [Level-Editor bauen](#level-editor-bauen)
- [Tastensteuerung einbauen](#tastensteuerung-einbauen)
- [Auto-Solver bauen](#auto-solver-bauen)

<!-- tocstop -->

Einleitung
----------
* Webseite: https://www.parallelrealities.co.uk/games/threeGuys
* Code: https://github.com/stephenjsweeney/3Guys
* GPLv3
* Sprache: C
* Bibliothken: SDL
    * SDL-Tutorials: https://www.parallelrealities.co.uk/tutorials/
        * u. a. Platform-Game
        * siehe auch themen/programmieren-games/README.md

Abhängigkeiten:

    # openSUSE:
    sudo zypper install libSDL2-devel libSDL2_mixer-devel libSDL2_image-devel libSDL2_ttf-devel

    # Ubuntu:
    sudo apt install libsdl2-dev libsdl2-mixer-dev libsdl2-image-dev libsdl2-ttf-dev libpng-dev

    (todo https://unix.stackexchange.com/questions/109117/virt-manager-copy-paste-functionality-to-the-vm auf kubuntu-18.04-desktop-i386.iso VM)

Programmieren mit KDevelop.

Additional make options:

    PREFIX=/home/gregor/dev/src-view/3Guys/inst-usr DATA_DIR=/home/gregor/dev/src-view/3Guys/inst-data

Game-Data:

* https://www.parallelrealities.co.uk/downloads/3Guys/3Guys-demo-data.tar.gz
* direkt im src-Verzeichnis entpacken (locale ist doppelt)
* (Sounds erzeugt mit SFXR, siehe sfxr-qt)

Run-Env:

    HOME=/home/gregor/dev/src-view/3Guys/inst-home


Aufgabe 1: Bauen
----------------
### Bauen
...

### Ausführen
...

### Einige Level spielen
...
Save-Game-Location
...

Aufgabe 2: Leveldateien
-----------------------
### Vorhandenes Level anpassen
* ...
* Verzeichnis-Struktur erklären
* JSON
* ...

### Neues Level bauen
* ...
* Versteckte Features finden, wie z. B. TNT
    * Dokumentieren
* ...

Spiel auf Deutsch übersetzen
----------------------------
    if ((lang = getenv("LC_ALL")) || (lang = getenv("LC_CTYPE")) || (lang = getenv("LANG")))

    echo $LC_ALL
    echo $LC_CTYPE
        en_US
    echo $LANG
        de_DE.UTF-8

"de_DE" merken.

**Lokalize** starten

    sudo zypper install lokalize

locale/3Guys.pot öffnen.

Gewünschtes übersetzen.

Speichern unter: **de_DE.po** (in locale)

    make         # erzeugt die passende mo-Datei

    make install # Achtung, passendes PREFIX und DATA_DIR wählen

    LC_ALL=de_DE inst-usr/bin/3Guys
        INFO: Locale is de_DE
        INFO: FILE: /home/gregor/dev/src-view/3Guys/inst-usr/share/locale/de_DE/LC_MESSAGES/3Guys.mo

Hintergrund:

* https://www.icanlocalize.com/site/tutorials/how-to-translate-with-gettext-po-and-pot-files/
    * POT – Portable Object Template
    * PO – Portable Object
    * MO – Machine Object

Nicht alles kann so übersetzt werden? --> Übersetzung der Menüs via Widgets:

* siehe z. B. inst-data/data/widgets/title.json
* Fix Translation:

    STRNCPY(w->label, _(cJSON_GetObjectItem(root, "label")->valuestring), MAX_NAME_LENGTH);
    https://github.com/feinstaub/3Guys/commit/7147757fd2fcc08dfae869252f970d1fe2285663

Level-Editor bauen
------------------
* ...
* Extern mit Qt
* Intern mit SDL

Tastensteuerung einbauen
------------------------
* Derzeit ist das Spiel hauptsächlich mit der Maus bedienbar.
* => Tasten einbauen, z. B. Umschalt+Cursor

Auto-Solver bauen
-----------------
* Mit Backtracking herausfinden, ob ein Level in 1, 2, 3, ... Schritten lösbar ist
* Geht das auch mit Dijkstra? (https://de.wikipedia.org/wiki/Dijkstra-Algorithmus)

