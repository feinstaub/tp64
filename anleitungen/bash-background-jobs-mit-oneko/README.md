Bash - Hintergrundjobs mit o-neko
=================================

Baustein: "ONeko"

<!-- toc -->

- [Stufe 1](#stufe-1)
  * [Installieren](#installieren)
  * [Starten, Stoppen und Pausieren](#starten-stoppen-und-pausieren)
- [Stufe 2 - Programm-Optionen](#stufe-2---programm-optionen)
- [Stufe 3 - Bash-Hintergrundjobs](#stufe-3---bash-hintergrundjobs)
  * [Starten](#starten)
  * [Jobs im Hintergrund starten](#jobs-im-hintergrund-starten)
  * [Übung](#ubung)
  * [Jobs in den Hintergrund schicken](#jobs-in-den-hintergrund-schicken)

<!-- tocstop -->

Stufe 1
-------
### Installieren

Wir installierten [Neko, die Katze](https://en.wikipedia.org/wiki/Neko_(software)).

```
$ sudo zypper install oneko
```

Gebe nun folgendes der Reihe nach in die Konsole ein und vollziehe nach, was passiert.

### Starten, Stoppen und Pausieren

```
$ oneko
```
Programm wird gestartet. Katze verfolgt Maus.

Taste **Strg+C** drücken - Programm wird beendet.

```
$ oneko
```

Stufe 2 - Programm-Optionen
---------------------------
    oneko --help

    -tora
    -dog

    -speed

    -fg
    -bg


Stufe 3 - Bash-Hintergrundjobs
------------------------------

### Starten

**Strg+Z** - Programm wird pausiert. Katze friert ein.

```
$ fg
```
Programm wird fortgesetzt. 'fg' steht für foreground = Vordergrund.

Strg+C - Programm wird beendet.


### Jobs im Hintergrund starten
```
$ oneko -dog
```
Hund im Vordergrund starten.

Mit Strg+C wieder beenden.

```
$ oneko -dog &
```
Hund im Hintergrund starten. Die Bash ist frei für weitere Befehle.

```
$ oneko -dog &
$ oneko -dog &
$ oneko -dog &
```
Weitere Hunde starten.

```
$ jobs
```
Zeigt eine Liste aller Jobs.

### Übung

Verwende $ fg und Strg+C mehrmals, um alle Hunde zu beenden.

### Jobs in den Hintergrund schicken
todo:

```
$ oneko -tora
Strg+Z
$ bg
$ oneko -dog &
$ oneko &
$ jobs
$ kill %1
$ jobs -l
$ kill 'pid'
```

(siehe auch http://stackoverflow.com/questions/1624691/linux-kill-background-task)
(siehe dort auch $ skill)
