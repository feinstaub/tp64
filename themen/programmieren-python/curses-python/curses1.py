#import curses
#stdscr = curses.initscr()
#stdscr

from curses import wrapper
import curses
import time

import threading

# http://stackoverflow.com/questions/3310049/proper-use-of-mutexes-in-python
mutex = threading.Lock()

class KeyModel:
    direction = 0
    exit = False
    #key = ""

class ViewModel:
    i1 = 0
    i2 = 0
    i2_red = False

    i = 0
    x = 5
    y = 5
    text = "Hallo"

    key = ""

    def init(self):
        curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
        curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_RED)


    def draw(self, stdscr):
        stdscr.addstr(0, 0, "Keys:")
        stdscr.addstr(1, 0, "  Left / Right: move the controller")
        stdscr.addstr(2, 0, "  END key: exit the program")
        stdscr.addstr(4, 0, "Debug: last key={}".format(self.key))

        col = 0
        if self.i % 100 > 50:
            col = curses.A_REVERSE

        stdscr.addstr(self.y, self.x, self.text, col)

        cp2 = 1
        if self.i2_red:
            cp2 = 2

        stdscr.addstr(25, self.i2, "________", curses.color_pair(cp2))

        stdscr.addstr(30, 0, (self.i1 * " ") + "________" + (60 - self.i1) * " ")


view = ViewModel()
keymodel = KeyModel()


def draw_loop(stdscr):
    global keymodel
    global view

    while True:
        stdscr.clear()
        view.draw(stdscr)
        stdscr.refresh()
        time.sleep(0.005)

def on_key(key):
    global keymodel

    with mutex:
        if key == 'KEY_LEFT':
            if keymodel.direction == -1:
                keymodel.direction = 0
            else:
                keymodel.direction = -1
        elif key == 'KEY_RIGHT':
            if keymodel.direction == 1:
                keymodel.direction = 0
            else:
                keymodel.direction = 1
        elif key == 'KEY_END':
            keymodel.exit = True

        view.key = key

def key_loop(stdscr):
    while True:
        key = stdscr.getkey()
        # stdscr.addstr(0, 0, "Key: " + key) # KEY_LEFT, KEY_RIGHT
        on_key(key)


def main(stdscr):
    # Clear screen
    stdscr.clear()

    t = threading.Thread(target=draw_loop, args=[stdscr])
    t.daemon = True  # thread dies when main thread (only non-daemon thread) exits.
    t.start()

    t2 = threading.Thread(target=key_loop, args=[stdscr])
    t2.daemon = True  # thread dies when main thread (only non-daemon thread) exits.
    t2.start()


    #This raises ZeroDivisionError when i == 10.
    for i in range(0, 9):
        v = i-10
        stdscr.addstr(i, 0, '10 divided by {} is {}'.format(v, 10/v))

    stdscr.addstr(0, 0, "Hallo")
    stdscr.addstr(10, 50, "Hallo")

    stdscr.refresh()

    stdscr.clear()

    #stdscr.start_color()
    view.init()

    x = 5
    y = 5
    d = 3
    dy = 1
    i = 0

    view.i1 = 1
    d1 = 1

    view.i2 = 1
    d2 = 0


    def restrict(x):
        if x > 60:
            return 60

        if x < 0:
            return 0

        return x

    while not keymodel.exit:
        with mutex:
            d2 = keymodel.direction
            # keymodel.direction = 0

        i += 1

        if i % 10 == 0:

            if x > 30 or x < 3:
                d = -d

            if x < 3:
                x = 3

            x = x + d
            y = y + dy
            if y > 20:
                y = 20
                dy = -1

            if y < 0:
                y = 0
                dy = 1

        if i % 2 == 0:
            view.i1 += d1
            view.i1 = restrict(view.i1)
            if view.i1 == 60:
                d1 = -1
            if view.i1 == 0:
                d1 = 1

        view.i2 += d2
        view.i2 = restrict(view.i2)
        if view.i2 > 0:
            view.i2_red = True
        else:
            view.i2_red = False

        with mutex:
            view.x = x
            view.y = y
            view.i = i

        time.sleep(0.01)

        # stdscr.clear()
        #print("{} {}".format(x, y))

    #time.sleep(10)
    #print("press for exit")
    #stdscr.getkey()


wrapper(main)
