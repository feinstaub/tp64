Smartphone und Privatsphäre: Workshop
=====================================

* Smartphone
    * derzeit optimal für Android:
        1. PlasmaOS (in Entwicklung)
        2. z. B. Replicant
        3. z. B. LinageOS
    * erste einfache Schritte:
        * Spy-Apps deinstallieren
        * F-Droid installieren
        * freie Apps verwenden

* Mögliche Kriterien für neue Hardware
    * [2014](neues-smartphone-2014) - Huawei
    * [2016](neues-smartphone-2016) - Tehnoetic S2
    * [2017 (1)](neues-smartphone-2017-1)
    * [2017 (2)](neues-smartphone-2017-2)
    * [2018](neues-smartphone-2018-1) - Motorola

App-Auswahl 2018
----------------
* siehe folien-main.md

Selber installieren
-------------------
* siehe ./mobile-os-installieren
