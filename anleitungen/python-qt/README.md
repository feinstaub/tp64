Qt mit Python
=============

<!-- toc -->

- [Einstieg (englisch) mit zetcode](#einstieg-englisch-mit-zetcode)
- [Breakout-Spiel](#breakout-spiel)
- [Aufgaben und Projekt-Ideen](#aufgaben-und-projekt-ideen)
  * [Projekt Pong](#projekt-pong)
  * [Netzwerk-Pong](#netzwerk-pong)
  * [Kurs-Rechner mit Speichern in Dateien](#kurs-rechner-mit-speichern-in-dateien)
  * [Schneeflockenkurve](#schneeflockenkurve)
  * [Standardfenster-Anwendung mit xeyes](#standardfenster-anwendung-mit-xeyes)
  * [Image-Size-Reducer](#image-size-reducer)
  * [Share with Droopy im Dolphin Service-Menü](#share-with-droopy-im-dolphin-service-menu)
  * [Receive files with Droopy here](#receive-files-with-droopy-here)
- [Installation von PyQt](#installation-von-pyqt)
  * [Wo soll ich meine Programme speichern](#wo-soll-ich-meine-programme-speichern)
  * [Wo bekomme ich ein Icon her?](#wo-bekomme-ich-ein-icon-her)
- [PyQt5-Offline-Beispiele](#pyqt5-offline-beispiele)
- [Weiteres](#weiteres)
  * [2018: Qt for Python - PySide2](#2018-qt-for-python---pyside2)
  * [QML](#qml)
  * [Programme, die mit **PyQt** geschrieben sind](#programme-die-mit-pyqt-geschrieben-sind)
  * [Werkzeuge](#werkzeuge)
- [Weiteres 2](#weiteres-2)

<!-- tocstop -->

Einstieg (englisch) mit zetcode
-------------------------------
Als Basis für die Qt-Einführung mit Python3 verwenden wir dieses englischsprachige Tutorial:

* [zetcode-pyqt5](zetcode-pyqt5.md)

Breakout-Spiel
--------------
* Schritt-für-Schritt-Anleitung zum eigenen [Breakout-Spiel mit PyQt](../pyqt-breakout-spiel)

Aufgaben und Projekt-Ideen
--------------------------
### Projekt Pong
* siehe [aufgabe-pong.md](aufgabe-pong.md)

### Netzwerk-Pong
* network pong with two screens
* use udp to pong across two computers
    * TODO: impl

### Kurs-Rechner mit Speichern in Dateien
* Voraussetzungen
    * Qt-Basiswissen (Layouts, Widgets)
    * QTableView: siehe py-qtableview.md
    * Dateien lesen und schreiben: siehe ../python-howtothink.md
        * TODO: Wie schreibt man Tabellendaten in eine Datei?

### Schneeflockenkurve
* siehe ../python-howtothink.md
* siehe ../python-turtle/python-turtle-on-opentechschool.md

### Standardfenster-Anwendung mit xeyes
* **xeyes** nachprogrammieren, siehe dazu auch https://de.wikipedia.org/wiki/Ellipse
* TODO
    * Start mit Vorlage
        * Menü und Toolbar und leeres Hauptwidget
    * Ellipse zeichnen
    * Toolbar-Button hinzufügen mit Theme-Icon
        * Klick zeichnet Rechteck
    * Maus-Koo abfragen
    * ...
    * xeyes

### Image-Size-Reducer
...

### Share with Droopy im Dolphin Service-Menü
...

### Receive files with Droopy here
* Ziel: Jemand möchte einem eine Datei über das Netzwerk schicken. Dafür macht man droopy auf gibt dem anderen die URL.
* als Dolphin Service-Menü, damit man droopy schnell starten kann und bestimmen kann, wo die empfangenen Dateien landen sollen

Installation von PyQt
---------------------
Das erste Beispiel wird schon nicht funktionieren, weil PyQt5 nicht installiert ist. Probiere es trotzdem aus und schaue dir die Fehlermeldung an. Dann installiere PyQt5:

PyQt5 auf openSUSE installieren:

    sudo zypper in python3-qt5

### Wo soll ich meine Programme speichern
* Am besten unter **~/ag/qt/**
* Benenne die Dateien nach den Kapitelnummern also z. B. **qt2einl.py** für dieses Kapitel (2. Kapitel und 'einl' als Abkürzung für Einleitung).

### Wo bekomme ich ein Icon her?
* Öffne **Dolphin** und navigiere damit ins Verzeichnis **/usr/share/icons/hicolor/32x32/apps/**
* Aktiviere die Icons-Ansicht
* Aktiviere die Preview-Ansicht (Vorschau)
* Regle die Größe mit dem Schieberegler unten rechts
* Suche dir eine png-Datei aus und kopiere sie in das Verzeichnis, wo dein Python-Programm liegt.

PyQt5-Offline-Beispiele
-----------------------
Tipp: Es gibt einen Qt-Beispiel-Browser, mit denen man sich alle Qt-Beispiele anschauen kann.

Installation auf openSUSE Tumbleweed:

    sudo zypper install python3-qt5-devel

TODO: 2019: diese Beispiele sind nicht mehr im Paket enthalten

Kommandozeile öffnen:

    cd /usr/share/doc/packages/python3-qt5-devel/examples/
    cd qtdemo
    python3 qtdemo.py

Alternativ kann man sich die einzelnen Beispiele auch separat anschauen:

    cd /usr/share/doc/packages/python3-qt5-devel/examples/
    cd painting/basicdrawing
    python3 basicdrawing.py

    cd /usr/share/doc/packages/python3-qt5-devel/examples/
    cd widgets
    # verschiedene Widgets:
    python3 groupbox.py
    # Tetris-Spiel:
    python3 tetrix.py

    cd /usr/share/doc/packages/python3-qt5-devel/examples/
    cd widgets/tooltips
    # Image-Buttons, simple drawings, tooltips:
    python3 tooltips.py

Tipp: um sich die Beispiele näher anzuschauen, kann man aus einer Kommandozeile den Dateibrowser (dolphin) mit dem aktuellen Verzeichnis öffnen:

    xdg-open .

Weiteres
--------
* weiteres: http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python
* weiteres: http://feinstaub.github.io/arbeitsblatt.net/arbeitsblatt-py-qt.html TODO: move to here
* Ergänzungen
    * $ assistant
    * $ sudo zypper install **libqt5-qtdoc** (sonst fehlt die Qt-Dokumenation im Qt 5 Assistant)

### 2018: Qt for Python - PySide2
* Startseite: https://www.qt.io/qt-for-python
* API-Dok: http://doc.qt.io/qtforpython/
* ["Qt for Python - Making a QML Application in Python {tutorial}"](https://www.youtube.com/watch?v=JxfiUx60Mbg)
    * and more tutorials

### QML
* siehe [QML](../../programmieren-qml)

### Programme, die mit **PyQt** geschrieben sind
* siehe (programmieren-qt)(../../themen/programmieren-qt/README.md)

### Werkzeuge
* siehe (programmieren-qt)(../../themen/programmieren-qt/README.md)

Weiteres 2
----------
* Deutsche Anleitungen?
    * ["Qt von 0 auf 100 - Qt-Creator Programmierung für Dummies und Anfänger oder Qt Tutorial in deutsch"](https://qtvon0auf100.wordpress.com/)
* Englisch-sprachiger Linux-Benutzer und -Entwickler, der einige Qt-Programme geschrieben hat:
    * https://gottcode.org/
    * bekannt für [FocusWriter](https://gottcode.org/focuswriter/)
        * Programmiersprache hauptsächlich C++
