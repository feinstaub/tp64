#!/usr/bin/python3

"""
Erzeugt ein Fenster und zeichnet ein Rechteck
Siehe auch http://zetcode.com/gui/pyqt5/painting/
"""

from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QPen, QBrush, QColor
from PyQt5.QtCore import Qt
import sys

class Fenster(QWidget):
    def __init__(self):
        super().__init__()
        self.setGeometry(300, 300, 400, 300)
        self.setWindowTitle("Hallo Rechteck")
        self.show()

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        qp.setBrush(QBrush(QColor(255, 0, 0)))
        qp.drawRect(20, 30, 100, 70)
        qp.end()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWidget = Fenster()
    sys.exit(app.exec_())
