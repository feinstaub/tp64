Sprache umstellen
=================

<!-- toc -->

- [Tastatur-Layout auf Deutsch umstellen](#tastatur-layout-auf-deutsch-umstellen)
- [System-Sprache von Englisch auf Deutsch umstellen](#system-sprache-von-englisch-auf-deutsch-umstellen)
  * [mit YaST](#mit-yast)
  * [Hinweis](#hinweis)
- [Offline-Wörterbuch](#offline-worterbuch)
- [Häufige Fragen](#haufige-fragen)
- [Andere Sprachen ausprobieren](#andere-sprachen-ausprobieren)

<!-- tocstop -->

Tastatur-Layout auf Deutsch umstellen
-------------------------------------
Auf dem KDE/Plasma-Desktop (gestestet mit openSUSE 42.1):

- Drücke **Alt+Leertaste**, um Krunner zu aktivieren
- Gebe 'keyboard' ein (oder 'tastatur')
- Wähle das Keyboard- (oder Tastatur-)Modul aus den System Settings aus
- Wähle den Tab "Layouts" aus.
- Mache das Fenster etwas größer.
- In der Mitte rechts drücke auf den Knopf "Add" (oder "Hinzufügen").
- Wähle dann in der zweiten Zeile ("Layout:") den Eintrag "German" (oder
  "Deutsch") aus.
- Bestätige zweimal mit OK.

System-Sprache von Englisch auf Deutsch umstellen
-------------------------------------------------
Dein openSUSE-Linux-System ist auf Englisch installiert und du willst es
auf Deutsch haben?

### mit YaST
Die folgende Anleitung erfordert eine Internetverbindung, falls die
deutschen Sprachpakete noch nicht installiert sind: System-Sprache von
Englisch auf Deutsch umstellen, inklusive Zeitzone und Tastaturlayout.

1. Das K-Menü öffnen und **YaST starten**.

2. root-Passwort eingeben. [Was ist das?](../hintergrundwissen/superuser-root/)

3. Unter der Kategorie "System" ein Eintrag **Language** anklicken.

4. Unter "Primary Language Settings" die "Primary Language" **German - Deutsch** auswählen.

5. Die Option **Adapt Keyboard Layout to German** aktivieren.

6. Die Option **Adapt Time Zone to Europe / Germany** aktivieren.

7. OK klicken.

=> Die benötigten Sprach-Pakete werden automatisch heruntergeladen und die
System-Sprache, die Zeitzone und das Tastaturlayout umgestellt.

Jetzt ausloggen (K-Menü -> Logout) und **neu anmelden**.

### Hinweis
- GNU/Linux kann man in vielen Sprachen betreiben, darunter Englisch
  (Originalsprache), Deutsch, Französisch, alle europäischen Sprachen),
  Russisch, Chinesisch etc.
- Man kann auch einzelne Programme in einer anderen Sprache starten, z. B.
  Chinesisch, siehe "Andere Sprachen ausprobieren"

Offline-Wörterbuch
------------------
* siehe z. B. [Offline-Wörterbuch mit KDing](../offline-woerterbuch-kding)

Häufige Fragen
--------------
* Frage: Kann man auch die Ausgaben von **Kommandozeilenprogrammen** wie zypper auf Deutsch übersetzen?
    * Wenn ja, wie? Oder lieber Englisch lernen?

Andere Sprachen ausprobieren
----------------------------
...im Aufbau...

* YaST-Modul 'Language' starten.

* Im Bereich "Secondary Languages" folgendes auswählen:
    * Vereinfachtes Chinesisch
    * Spanisch
    * Schwedisch
    * Türkisch

* Übernehmen.
    (=> Es werden über 100 neue Pakete heruntergeladen und installiert, darunter Übersetzungen von Programmen und Dokumenation, Schriftarten und Programme zur Eingabe von chinesischen Schriftzeichen.)

HINWEIS: Durch die zusätzlichen Sprachpakete werden zukünftige Updates länger dauern, weil mehr heruntergeladen und installiert werden muss.

Wenn man der Installation der Pakete genau hinschaut, sieht man in den Paketnamen folgende Schlüsselworte:

de_DE - Deutsch
en_US - Englisch
sv_SE - Schwedisch
zh_CN - Chinesisch
es_ES - Spanisch
tr_TR - Türkisch

siehe z. B. auch [ISO Language Code Table](http://www.lingoes.net/en/translator/langcode.htm)

```
export LANG=sv_SE
kwrite
```

HINWEIS: Wenn die installierten Pakete unter KDE Probleme bereiten, könnte das an den Paketen `gcin`, `ibus` und/oder `kvkbd` liegen. Diese Pakete und ihre Abhängigkeiten sollten dann wieder deinstalliert werden.
