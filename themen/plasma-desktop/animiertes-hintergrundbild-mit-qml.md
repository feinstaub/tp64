Animiertes Hintergrundbild mit QML
==================================

<!-- toc -->

- [Einleitung](#einleitung)
  * [Blog: Templates to create your own Plasma Wallpaper plugin](#blog-templates-to-create-your-own-plasma-wallpaper-plugin)
- [Animation runterladen](#animation-runterladen)
- [Plasma-Plugin erstellen](#plasma-plugin-erstellen)
- [Ausprobieren](#ausprobieren)
  * [qmlscene nicht installiert?](#qmlscene-nicht-installiert)
- [Als Hintergrundbild installieren](#als-hintergrundbild-installieren)
- [Experimentieren](#experimentieren)
  * [Farbwerte verändern](#farbwerte-verandern)
  * [main.qml ändern => Plasma-Neustart](#mainqml-andern--plasma-neustart)
- [Nächste Schritte](#nachste-schritte)
  * [Quellen / Fragen, Probleme](#quellen--fragen-probleme)
- [QML-Ausblick](#qml-ausblick)

<!-- tocstop -->

```
 _______________________________________
/ In diesem Abschnitt lernst du wie man \
| mit der Sprache QML Graphiken auf dem |
\ Bildschirm darstellt.                 /
 ---------------------------------------
   \
    \
        .--.
       |o_o |
       |:_/ |
      //   \ \
     (|     | )
    /'\_   _/`\
    \___)=(___/
                               __________________________
                              < Kuh M L...? Sounds good. >
                               --------------------------
                                      \   ^__^
                                       \  (oo)\_______
                                          (__)\       )\/\
                                              ||----w |
                                              ||     ||
```

Einleitung
----------
* Auf https://www.opensuse.org/searchPage ist ein animiertes gif-Bild mit einem Gecko zu finden.
* Wie kann man nun diese **Animation als Desktop-Hintergrundbild** einrichten?

### Blog: Templates to create your own Plasma Wallpaper plugin
* https://frinring.wordpress.com/2018/04/04/templates-to-create-your-own-plasma-wallpaper-plugin/
    * siehe KAppTemplate, KDevelop

Animation runterladen
---------------------
* Die Animation herunterladen und ins Verzeichnis: **~/ag** speichern
* Der vollständige Pfad ist dann: **~/ag/open-suse-welcome.gif**

Plasma-Plugin erstellen
-----------------------
Ein neues Verzeichnis **de.ag.hintergrundbild1** erstellen:

    $ cd ~/dev/ag
    $ mkdir de.ag.hintergrundbild1

Dort eine neue Datei **metadata.desktop** erstellen, mit folgendem Inhalt:

    [Desktop Entry]
    Encoding=UTF-8
    Name=Hintergrundbild1
    Name[x-test]=xxHintergrundbild1xx

    Type=Service
    ServiceTypes=Plasma/DeclarativeWallpaper
    Icon=preferences-desktop-wallpaper
    X-Plasma-MainScript=ui/main.qml
    X-KDE-PluginInfo-Name=de.ag.hintergrundbild1
    X-KDE-PluginInfo-EnabledByDefault=true


Zwei weitere Ordner erstellen:

~/ag/de.ag.hintergrundbild1/**contents/ui**

Dort eine Datei **main.qml** erstellen, mit folgendem Inhalt:

    import QtQuick 2.0
    import QtMultimedia 5.0

    Item {
        Rectangle {
            id: rectangle1
            color: "#1d3be3"

            anchors.fill: parent

            Rectangle {
                x: 79
                y: 107
                width: 570
                height: 228
                color: "#aaa7ff"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter

                AnimatedImage { id: animation; source: "animated-image.gif" }
            }
        }
    }

Bild kopieren:

Das Bild `~/ag/open-suse-welcome.gif` kopieren nach **~/ag/de.ag.hintergrundbild1/contents/ui/animated-image.gif**

So sieht nun die Verzeichnisstruktur aus:

    ~/ag/de.ag.hintergrundbild1/
        - contents/
            - ui/
                - main.qml
                - animated-image.gif
        - metadata.desktop

Ausprobieren
------------
    $ cd ~/ag/de.ag.hintergrundbild1/contents/ui
    $ qmlscene main.qml

oder:

    $ qmlscene --fullscreen main.qml
    # Alt+F4 zum Beenden

### qmlscene nicht installiert?
Falls das Programm qmlscene nicht installiert ist... es befindet sich in diesem Paket (siehe $ rpm -qf `which qmlscene`):

* **libqt5-qtdeclarative-tools** (Installieren siehe "Software installieren", todo)

Als Hintergrundbild installieren
--------------------------------
* Den Ordner de.ag.hintergrundbild1 nach **~/.local/share/plasma/wallpapers** verschieben (oder kopieren oder mit einem symbolischen Link versehen).
    * HINWEIS: das Unterverzeichnis "plasma/wallpapers/" existiert möglicherweise nicht und muss manuell erstellt werden:
    * 2019: geht leider nicht mehr, siehe install.sh

    $ mkdir -p ~/.local/share/plasma/wallpapers/

* Das Dialog zum Einstellen des Hintergrundbildes aufrufen (rechte Maustaste Configure Desktop -> Hintergrundbilder),
* Dann unter Wallpaper Type (Hintergrundbildtyp) das neue Hintergrundbild **Hintergrund1** auswählen.


Experimentieren
---------------
### Farbwerte verändern
* siehe **main.qml**
* **KColorchooser** verwenden, um einen vorhandenen Farbcode zu ändern oder einen neuen zu erstellen.
    * Tipp: **Pick color from screen**

### main.qml ändern => Plasma-Neustart
* Wenn die installiert main.qml im Nachhinein geändert wird, muss Plasma neu gestartet werden, damit die Änderungen greifen.

    kquitapp plasmashell
    plasmashell

hilft bisher nicht.

https://stackoverflow.com/questions/43788101/restart-kde-without-reboot

Nächste Schritte
----------------
* [QML-Video-Hintergrund](qml-video-hintergrund.md)
* TODO/Ideen:
    * Choose a color directly from QML color chooser dialog
    * generischer Video-Wallpapertype mit Konfigurationdialog

### Quellen / Fragen, Probleme
* Quelle: https://forum.kde.org/viewtopic.php?f=289&t=131783 - "Video and HTML wallpaper types"
* siehe auch de.ag.hintergrundbild1
* todo: Wenn das jetzt zu schwierig war, sollten wir die "'''Dateien und Verzeichnisse'''"-Übung aus der Kommandozeile machen.)

QML-Ausblick
------------
* [https://www.proggen.org/doku.php?id=frameworks:qt:start Einführung in QML auf proggen.org]
* Das QML-Buch: http://qmlbook.github.io/index.html
* QT Assistent (offline): QML Advanced Tutorial - 4 Schritte zu einem Spiel
* Property Binding: http://doc.qt.io/qt-5/qtqml-syntax-propertybinding.html
* Das Canvas-Element
    * http://qmlbook.github.io/ch07/
    * tp64/programmieren-qml/canvas/canvas1.qml
* Einstellungen merken: Settings QML Type: http://doc.qt.io/qt-5/qml-qt-labs-settings-settings.html
* Fluid-Elements: http://qmlbook.github.io/en/ch05/index.html
* Smooth animations mit Canvas: https://woboq.com/blog/animations-using-the-qtquick-canvas.html
