// https://de.wikibooks.org/wiki/C%2B%2B-Programmierung/_Weitere_Grundelemente/_Rekursion
#include <iostream>       // Für std::cin und std::cout
unsigned int fakultaet(unsigned int zahl) {
    if (zahl <= 1) {
        return 1; // Die Fakultät von 0 und 1 ist als 1 definiert.
    } else {
        return zahl * fakultaet(zahl - 1);
    }
}

int main() {
    unsigned long a = -1;
    unsigned long long b = -1;
    unsigned int c = -1;
    std::cout << a << std::endl;
    std::cout << b << std::endl;
    std::cout << c << std::endl;
    unsigned int zahl;
    std::cout << "Bitte Zahl eingeben: ";
    std::cin >> zahl;                           // Zahl einlesen
    std::cout << "Die Fakultät von " << zahl << // Antwort ausgeben
        " ist " << fakultaet(zahl) << "." << std::endl;
}
