import turtle

turtle.speed(50)
turtle.pensize(2)
turtle.setx(-400)
turtle.sety(-400)

a = 1
for i in range(1, 50000):
    turtle.forward(50)
    turtle.left(10 + a)
    a = a + 2

    if i == 1:
        turtle.pencolor((0, 0, 1))

    if i == 100:
        turtle.pencolor((0, 1, 0))

    if i == 200:
        turtle.pencolor((1, 1, 0))

    if i == 300:
        turtle.pencolor((1, 0, 1))

    if a > 360:
        a = 0

turtle.exitonclick()
