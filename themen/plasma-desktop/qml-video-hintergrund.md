QML-Video-Hintergrund
=====================

<!-- toc -->

- [Schritt 1: Videos beschaffen](#schritt-1-videos-beschaffen)
- [Schritt 2: Rechteck und Farben](#schritt-2-rechteck-und-farben)
  * [Rechteck](#rechteck)
  * [Farben](#farben)
  * [Schritt 3: Video-Datei einbinden](#schritt-3-video-datei-einbinden)
  * [Schritt 4: Video-Ausgabe verfeinern](#schritt-4-video-ausgabe-verfeinern)
  * [Ausblick](#ausblick)
- [Nächste Schritte](#nachste-schritte)
  * [Schöne Videos](#schone-videos)

<!-- tocstop -->

In diesem Arbeitsblatt lernst du, wie du mit dem MediaPlayer-QML-Element Videos (inklusive Ton) in ein QML-Dokument einbetten kannst.

(MOVED from wiki: todo: formatting)

Schritt 1: Videos beschaffen
----------------------------

Gehe in den Ordner ~/ag/

Erstelle dort einen neuen Ordner: qmlvideo

Erstelle dort einen weiteren neuen Ordner: videos

Gehe in den Ordner '''~/ag/qmlvideo/videos''' und führe folgende Befehle aus:

    wget -O video1.ogv https://upload.wikimedia.org/wikipedia/commons/9/9a/Time_Lapse_of_New_York_City.ogv
    wget -O video2.ogv https://upload.wikimedia.org/wikipedia/commons/5/50/Flash-Lightning_over_Germany.ogv
    wget -O video3.ogv https://upload.wikimedia.org/wikipedia/commons/2/27/Akame_48_Waterfalls-Senjyudaki-video.ogv


Damit werden drei verschiedene Videos aus den Wikipedia-Commons heruntergeladen.

Öffne die Videos der Reihe nach mit einem Media-Player um zu schauen, ob sie funktionieren.
BITTE den Ton regulieren, so dass andere nicht gestört werden oder Kopfhörer verwenden.


Schritt 2: Rechteck und Farben
------------------------------

Wir arbeiten jetzt im Ordner '''~/ag/qmlvideo'''.

### Rechteck
Erstelle eine Datei (~/ag/qmlvideo/'''simple1.qml''') mit Kate mit folgendem Inhalt:

```
import QtQuick 2.0

Rectangle {
    color: "#000000"
}
```

Lass die erstellte Datei ablaufen:

    $ qmlscene simple1.qml

Erklärungen:
* Die Zeile "import QtQuick 2.0" brauchen wir, damit der Typ "Rectangle" verwendet werden kann.
* Alle Typen im QtQuick-Modul kann man hier nachlesen: http://doc.qt.io/qt-5/qtquick-qmlmodule.html
** z. B. den Rotationstyp, den wir später kennenlernen

### Farben
Gib dem Rechteck andere Farben. Zum Beispiel mit Hexadezimal:

```
#000000 - Schwarz
#FF0000 - Rot
#00FF00 - Grün
#0000FF - Blau
```

Probiere die Farben jeweils aus:

    $ qmlscene simple1.qml


Oder verwende Farbschlüsselworte:

* aqua
* blue
* forestgreen
* gray
* red
* Alle Schlüsselwörter siehe hier: https://www.w3.org/TR/SVG/types.html#ColorKeywords

Weitere Details zu QML-Farben siehe hier:
* http://doc.qt.io/qt-5/qml-color.html

### Schritt 3: Video-Datei einbinden
Erstelle eine neue Datei (~/ag/qmlvideo/'''simple2.qml''') mit Kate mit folgendem Inhalt.

Bitte alles selber abtippen. Die Quellcode-Kommentare (beginnend mit //) weglassen, das spart Tipparbeit.

```
import QtQuick 2.0
// für den MediaPlayer- und VideoOutput-Typ:
import QtMultimedia 5.0

Rectangle {
    color: "#000000"

    MediaPlayer {
        // Die ID (= Identifizier = Bezeichner dieses Objekts)
        id: mediaplayer1

        // Pfad zu dem Video, das abgespielt werden soll:
        source: "videos/video2.ogv"

        // Das Video soll sofort anfangen:
        autoPlay: true

        // Das Video soll unendlich wiederholt werden:
        loops: MediaPlayer.Infinite

        // Die Lautstärke soll Null sein (1.0 ist Maximum)
        volume: 0.0
    }

    // Das MediaPlayer-Objekt spielt die Videodatei ab, aber
    // es muss auch irgendwo angezeigt werden.
    // Stell dir den MediaPlayer wie einen DVD-Spieler vor
    // und das folgende VideoOutput-Objekt wie einen Bildschirm.
    // Mit der ID werden Abspielgerät und Bildschirm verbunden.
    VideoOutput {
        // Die ID des MediaPlayer-Objekts
        source: mediaplayer1
    }
}
```

Schaue, ob die Datei funktioniert:

    $ qmlscene simple2.qml

### Schritt 4: Video-Ausgabe verfeinern

Wenn du das Ausgabefenster mit der Maus vergrößerst, dann bleibt die Videogröße unverändert.
Dies kann man ändern.

Erstelle eine neue Datei (~/ag/qmlvideo/'''simple3.qml''') mit Kate mit folgendem Inhalt.

```
import QtQuick 2.0
import QtMultimedia 5.0

Rectangle {
    color: "#000000"

    MediaPlayer {
        id: mediaplayer1
        source: "videos/video1.ogv" // diesmal das Video Nr. 1
        autoPlay: true
        loops: MediaPlayer.Infinite
        volume: 0.0
    }

    VideoOutput {
        source: mediaplayer1

        // Das ganze Rechteck soll mit dem Video gefüllt sein:
        anchors.fill: parent

        // Füllungsmodus:
        fillMode: VideoOutput.PreserveAspectFit
    }
}
```

Probiere die verschiedenen Füllungsmodi aus:
* '''PreserveAspectFit''' - Das Seitenverhältnis wird beibehalten und das Video ist immer vollständig sichtbar
* '''Stretch''' - Das Video wird genau in die Ausgabefläche "gezogen".
* '''PreserveAspectCrop''' - Das Seitenverhältnis wird immer beibehalten, aber die Ausgabefläche wird auch immer vollständig gefüllt, so dass das Video eventuell abgeschnitten werden muss

Welcher Füllungsmodus wird automatisch verwendet, wenn die fillMode-Eigenschaft weggelassen wird?

Details zu MediaPlayer und VideoOutput:

* http://doc.qt.io/qt-5/qml-qtmultimedia-mediaplayer.html
* http://doc.qt.io/qt-5/qml-qtmultimedia-videooutput.html


### Ausblick
* Video-Download-Script mit Kommentaren: [download-videos.sh](../qml/de.ag.videowall1/contents/ui/videos/download-videos.sh)
* Fertiges Beispiel: [de.ag.videowall1](../qml/de.ag.videowall1)
* Siehe QML-Ausblick

Nächste Schritte
----------------
### Schöne Videos
* hier gefunden: https://github.com/jarzebski/Plasma-Wallpaper-DreamDesktop
    * www.dreamscene.org/gallery.php?Cmd=Show&site=scifi_space
        * ["DreamScene - Flying through Stars"](https://www.youtube.com/watch?v=TheFr7Nl-zY), 30 sec
        * ["Moon View - video designed by dreamscene.org"](https://www.youtube.com/watch?v=d2VqcdQNd8w), 1 min
            * Tipp: langsamer abspielen
