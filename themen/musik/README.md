Musik
=====

siehe auch ../audio

<!-- toc -->

- [Einstieg](#einstieg)
  * [MuseScore](#musescore)
  * [Csound](#csound)
- [Programmieren](#programmieren)
  * [Noten abspielen](#noten-abspielen)
  * [Noten berechnen](#noten-berechnen)
  * [Eigene Klaviertastatur mit Qt](#eigene-klaviertastatur-mit-qt)
- [Weiteres](#weiteres)
  * [SuperCollider](#supercollider)
  * [Piano-Tutorials](#piano-tutorials)

<!-- tocstop -->

Einstieg
--------
### MuseScore
siehe musescore.md

### Csound
[Einstieg in die Computermusik mit Csound und CsoundQt](../anleitungen/csound-einstieg)

Programmieren
-------------
### Noten abspielen
* Tetris: siehe pyaudio-and-to-sort/README-pyaudio.md
* Frequenzen: siehe pyaudio-and-to-sort/README-pyaudio.md
* Hänschen-Klein: todo
* siehe audaspace/*

### Noten berechnen (Formel), Abspielen mit py simpleaudio
* siehe playtone-simpleaudio.py

### Eigene Klaviertastatur mit Qt
* Alle weißen Tasten: https://de.wikipedia.org/wiki/Klaviatur#/media/File:KlaviaturMitNoten.svg
* Eine Oktave mit schwarzen Tasten: https://de.wikipedia.org/wiki/Klaviatur#/media/File:Klaviatur_(Tasten).png
* siehe qklavier.py
    * TODO: als eigenes Widget in C++ (Qt oder FLTK)
* TODO
    * Weiße und schwarze Tasten
    * Bei Klick einen Ton
    * Mit Beschriftung
        * Note (z. B. A)
        * und Frequenz (z. B. 440 Hz)
    * Töne mit sox

Noten
-----
* siehe ncurses-pianotyper/noten/*

### Piano-Tutorials / Noten

* [Tobu - Hope](https://www.youtube.com/watch?v=mDcXj4iUtlg) - mit Midi-Download
    * siehe Tobu - Hope.mid
        * siehe auch dataDocuments/Freizeit/Musik/Noten_und_Midi/
    * themen/multimedia-audio/README.md > Midi > Noten extrahieren
* [Avicii - Wake Me Up](https://www.youtube.com/watch?v=hfcLWy0EbzA) - schwer
* [Something Just Like This](https://www.youtube.com/watch?v=zTJvJp9CoqA) - impossible

Weiteres
--------
### SuperCollider
* http://supercollider.github.io
* `sudo zypper in supercollider`
