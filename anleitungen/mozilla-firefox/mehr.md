Mozilla Firefox Mehr
====================

<!-- toc -->

- [Mehr zu uBlock Origin](#mehr-zu-ublock-origin)
  * [Blockiert Werbung](#blockiert-werbung)
  * [Blockiert versteckte Tracker](#blockiert-versteckte-tracker)
  * [Andere Erweiterungen](#andere-erweiterungen)
  * [I don't care about cookies (Erweiterung)](#i-dont-care-about-cookies-erweiterung)
  * [Self-Destructing Cookies (Erweiterung)](#self-destructing-cookies-erweiterung)
- [Verschiedenes](#verschiedenes)
  * [More results in Awesomebar instead of only 12](#more-results-in-awesomebar-instead-of-only-12)
  * [Search for bookmarks and/or tags only](#search-for-bookmarks-andor-tags-only)

<!-- tocstop -->

Mehr zu uBlock Origin
---------------------
### Blockiert Werbung
* Hinweis zu Adblock Plus: in Adblock Plus wird seit einiger Zeit standardmäßig sogenannte [https://adblockplus.org/de/acceptable-ads-agreements "Akzeptable Werbung"] durchgelassen. [https://de.wikipedia.org/wiki/UBlock_Origin uBlock Origin] dagegen blockiert alles, was in den gemeinsam genutzten - von vielen Freiwilligen gepflegten - Listen definiert ist. Der Autor von uBlock Origin empfiehlt sogar, falls man etwas spenden möchte, vor allem auch an die Listen-Pfleger zu denken.
* Beispiel, wie Internet-Werbung (von angegriffenen Drittanbietern) schaden kann:
** [https://www.heise.de/newsticker/meldung/Exploit-Kit-liefert-Schadcode-in-Bildern-versteckt-3567766.html Schadcode in Bildern versteckt], heise.de, 2016

### Blockiert versteckte Tracker
Auf vielen Webseiten werden oft achtlos (also ohne die Interessen der Webseiten-Besucher zu berücksichtigen) z. B. Facebook-Tracker eingebaut (to track, engl. = verfolgen). Vorbildlich wäre die Verwendung eines 2-Klick-Empfehlungsbuttons (siehe z. B. den [https://www.heise.de/newsticker/meldung/Fuer-mehr-Datenschutz-Neue-Version-der-2-Klick-Empfehlungsbuttons-2101045.html 2-Klick-Empfehlungsbuttons von heise.de]).

Tracker sind für den normalen Nutzer unsichtbar und arbeiten im Hintergrund (versierte Nutzer kennen Werkzeuge, mit denen man diese Tracker sehen kann, z. B. mit Strg+Umschalt+I). Man kann uBlock Origin so konfigurieren, dass bestimmte Social-Media-Tracker ebenfalls blockiert werden:

* uBlock Origin-Icon anklicken
* Ganz oben die Leiste für das "Dashboard" anklicken.
* "3rd party filters" anklicken und folgende Listen auswählen
** "Anti-ThirdpartySocial (see warning inside list)‎"
*** Nur bei Verwendung dieser zusätzlichen Liste werden alle Verbindungen zu Facebook blockiert (falls diese von anderen Seiten, als Facebook selber erfolgen sollen). Die erwähnte "warning" besagt, dass dann aber auch manche auf Facebook basierende Kommentarseiten nicht mehr funktionieren.
** "Fanboy’s Annoyance List‎"
** "Fanboy’s Social Blocking List‎"
* Oben rechts "Apply changes" klicken.

* Beispiel: [http://www.echo-online.de/ratgeber/internet-und-elektronik/netzwelten/youtube-und-gema-einigen-sich-tausende-musikvideos-freigeschaltet_17438075.htm Diesen Artikel aufrufen]
** Mit Hilfe des uBlock Origin Loggers sehen, dass ohne die "Anti-ThirdpartySocial"-Liste einige Verbindungen zu Facebook aufgebaut werden, während mit der Liste alles sauber blockiert wird.

* https://stallman.org/facebook.html (englisch) - Interessante Zusammenstellung zu den Risiken und Nebenwirkungen von Facebook.

### Andere Erweiterungen
* Abgeraten wird von der "Privatsphäre"-Erweiterung [https://de.wikipedia.org/wiki/Ghostery Ghostery], siehe [https://de.wikipedia.org/wiki/Ghostery#Kritik Kritik].

### I don't care about cookies (Erweiterung)
https://addons.mozilla.org/de/firefox/addon/i-dont-care-about-cookies

* Hintergrund: Eine EU-Regulierung aus dem Jahr 2009 schreibt vor, dass Webseiten, die beim Besucher Cookies hinterlassen, vorher eine Meldung anzeigen müssen. Wenn man seinen Browser so eingestellt hat, dass er regelmäßig Cookies löscht, dann muss man als Nutzer diese Warnung ständig von neuem wegklicken. Dies kann auf Dauer stören.
* Diese Erweiterung sorgt dafür, dass man diese Pflicht-Warnung auf den meisten Seiten nicht mehr zu sehen bekommt.

### Self-Destructing Cookies (Erweiterung)
https://addons.mozilla.org/en-US/firefox/addon/self-destructing-cookies/

* siehe ausführliche Addon-Beschreibung

Verschiedenes
-------------
### More results in Awesomebar instead of only 12

about:config:

* `browser.urlbar.maxRichResults` Set to 330 instead of 12
* `places.frecency.bookmarkVisitBonus` set to 300
* `places.frecency.unvisitedBookmarkBonus` set to 300

(see http://lifehacker.com/the-best-about-config-tweaks-that-make-firefox-better-1442137111)

### Search for bookmarks and/or tags only

Append these to awesomebar search string separated by spaces

```
Add + to search for matches in pages you've tagged.

Add * to search for matches in your bookmarks.

Add ^ to search for matches in your browsing history.
Add % to search for matches in your currently open tabs.
Add ~ to search for matches in pages you've typed.
Add # to search for matches in page titles.
Add @ to search for matches in web addresses (URLs).
```

(see https://support.mozilla.org/en-US/kb/awesome-bar-find-your-bookmarks-history-and-tabs#w_what-can-i-do-to-get-the-best-results)
