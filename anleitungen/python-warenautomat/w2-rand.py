#!/usr/bin/env python3

# hilfe:py:randint - randint(a, b) --> a <= N <= b
# hilfe:py:str

import random

print("Wie alt bist du?")
a = random.randint(8, 16)
print("Du bist " + str(a) + " Jahre alt.")

# AUFGABE:
#
# 1. Führe das Programm mehrmals aus.
#
# 2. Gebe Folgendes aus: "Am 13.05.2017 bin ich aufgestanden und habe gefrühstückt."
#    Dabei soll das Datum immer plausibel sein, "35.12.1900" ist nicht erlaubt.
