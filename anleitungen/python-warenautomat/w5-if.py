#!/usr/bin/env python3

# hilfe:py:isnumeric
# hilfe:py:elif
# hilfe:py:Rechen-Operatoren: +, -, *, /
# hilfe:py:Vergleichs-Operatoren

print("Du stehst auf einem Steinblock.")

print("   O    ")
print("  / \   ")
print("   |    ")
print("  a a   ")
print("``````` ")
print("``````` ")

s1 = input("Wie groß bist du (in cm)? ")
gr = -1
if s1.isnumeric():
    gr = int(s1)
    print("Du bist " + str(gr) + " cm groß.")
else:
    print("Da stimmt was nicht.")

s2 = input("Wie hoch ist der Steinblock (in cm)? ")
st = int(s2)

if st == 1:
    print("Der Stein nicht nicht ganz flach.")
elif st <= 10:
    print("Der Stein ist kleiner oder gleich 10 cm.")
elif st < 100:
    print("Der Stein ist kleiner als 1 Meter.")
elif st >= 500 and st <= 1000:
    print("Der Stein ist zwischen 5 und 10 Meter hoch. Scheint ein hoher Stein zu sein.")
elif st < 10000:
    print("Der Stein ist kleiner als 100 Meter.")
else:
    print("Stehst du auf einem Berg?")

summe_cm = gr + st

print("Ihr seid zusammen " + str(summe_cm) + " cm hoch.")

print("Das sind ca. " + str(summe_cm / 100) + " Meter.")

# AUFGABEN:
#
# 1. Sorge dafür, dass auch die Abfrage nach der Steinhöhe Fehleingaben nicht zulässt.
#
# 2. Gebe die Gesamt-Höhe zusätzlich in Kilometern aus.
#
# 3. Schreibe ein Programm:
#    a) Zuerst fragt es nach dem Geburtsjahr des Benutzers.
#    b) Dann wählt es zufällig eine Jahreszahl J zwischen 2005 und 2019 aus (verwende randint).
#    c) Dann gibt es aus, wie alt der Benutzer im Jahr J war.
#
# 4. Erweitere das Programm aus 3. so, dass, nachdem es nach dem Alter des Benutzers gefragt hat,
#    nicht nur ein Zufallsjahr ermittelt, sondern zehn, und für diese zehn Jahre
#    die Berechnung durchführt.
