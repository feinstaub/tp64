#!/usr/bin/env python3

# hilfe:py:if
# hilfe:py:int
# hilfe:py:Vergleichs-Operatoren

a = int(input("Wie alt bist du? "))
if a == 1:
    print("Du bist " + str(a) + " Jahr alt.")
else:
    print("Du bist " + str(a) + " Jahre alt.")

if a > 10:
    print("Du bist älter als 10.")

if a < 10:
    print("Du bist jünger als 10.")

# AUFGABEN:
#
# 1. Gebe Folgendes aus, wenn es jeweils zutrifft
#    a) "Du bist älter als 18 Jahre."
#    b) "Bist du wirklich so alt?"
#    c) "Du kannst kein negatives Alter haben!"
