#!/usr/bin/python3

from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QPen, QBrush, QColor, QResizeEvent, QFont, QFontDatabase
from PyQt5.QtCore import Qt
import sys

class Fenster(QWidget):
    ry = 30
    rh = 30

    def __init__(self):
        super().__init__()
        self.setGeometry(300, 300, 400, 300)
        self.setWindowTitle("Hallo Text")
        self.show()

        # Verfügbare Schriftarten auf der Konsole ausgeben
        print(QFontDatabase().families())

    def resizeEvent(self, e):
        h = e.size().height()
        self.ry = h - self.rh
        self.update()

    def drawTopText(self, qp, text):
        qp.setPen(QColor(0, 0, 255))
        qp.setFont(QFont('Sans Serif', 14))
        qp.drawText(0, 20, text)

    def drawBackground(self, qp):
        qp.setPen(Qt.NoPen)
        qp.setBrush(QBrush(QColor(0, 255, 255)))
        qp.drawRect(0, 50, self.width(), self.height() - 50)

    def drawRacket(self, qp):
        qp.setBrush(QBrush(QColor(255, 0, 0)))
        qp.drawRect(20, self.ry, 100, self.rh - 10)

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawTopText(qp, "Fenstergröße: " + str(self.width()) + "," + str(self.height()))
        self.drawBackground(qp)
        self.drawRacket(qp)
        qp.end()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWidget = Fenster()
    sys.exit(app.exec_())
