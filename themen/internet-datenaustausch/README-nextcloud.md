Eigener Onlinespeicher mit Nextcloud
====================================

Ziel: Lernen wie man einen Online-Speicher (auch Cloud genannt) mit freier Software nutzen kann

<!-- toc -->

- [Die Software](#die-software)
- [Möglichkeiten der Nutzung](#moglichkeiten-der-nutzung)
- [Einen Anbieter auswählen](#einen-anbieter-auswahlen)
- [NextCloudPi](#nextcloudpi)
- [Nextcloud auf eigenem Server im Internet selber installieren](#nextcloud-auf-eigenem-server-im-internet-selber-installieren)
- [Nextcloud auf eigener Hardware selber installieren](#nextcloud-auf-eigener-hardware-selber-installieren)
- [(Fertige Box kaufen)](#fertige-box-kaufen)
- [(Auf einem bestimmten NAS installieren)](#auf-einem-bestimmten-nas-installieren)
- [In den Plasma-Desktop einbinden](#in-den-plasma-desktop-einbinden)
- [Mit WEBDAV in Dolphin](#mit-webdav-in-dolphin)
- [Weitere Ideen](#weitere-ideen)

<!-- tocstop -->

Die Software
------------
Stand 2017, 2019 ist Nextcloud aus Freier-Software-Sicht das Flaggschiff-Produkt:

* Wikipedia: https://de.wikipedia.org/wiki/Nextcloud
* Programmiersprache: PHP, JavaScript
* Webseite: https://nextcloud.com/

* Online ausprobieren: https://demo.nextcloud.com/ -> Start instant trial
    * (hat bei mir damals nicht funktioniert)

Möglichkeiten der Nutzung
-------------------------
## Einen Anbieter auswählen
* siehe https://nextcloud.com/providers/
    * zum Ausprobieren bei "Country" "de" auswählen und "Show only free plans" anwählen. Dann erscheinen nur deutsche Anbieter, die einen kostenlosen Testaccount anbieten.

## NextCloudPi
Das NextCloudPi-Projekt:

* https://ownyourbits.com/nextcloudpi/
    * "Easy to set up for non technical users"
    * https://docs.nextcloudpi.com

* Tutorial? z. B. https://www.youtube.com/watch?v=rbxI4pGwfF0

## Nextcloud auf eigenem Server im Internet selber installieren
* Dieser deutsche Hoster (https://schokokeks.org/) setzt voll auf freie Software und bietet ab 8 EUR / Monat einen Linux-Nutzeraccount auf einem Server an.
* Details: https://schokokeks.org/webhosting
* Dort könnte man Nextcloud installieren (aber am besten vorher nachfragen)

## Nextcloud auf eigener Hardware selber installieren
* Offizieller Einstieg: https://nextcloud.com/de/install/
* Eine Anleitung zur Installation der Nextcloud: https://eigene-cloud-einrichten.de/nextcloud-installieren-und-einrichten-so-gehts
    * (noch nicht ausprobiert)
    * https://eigene-cloud-einrichten.de/nextcloud-der-owncloud-fork-vorgestellt

## (Fertige Box kaufen)
* 2016: siehe z. B. https://www.dzwebdesign.de/nextcloud/nextcloud-box-vorgestellt

## (Auf einem bestimmten NAS installieren)
* 2017: siehe z. B. http://blog.viking-studios.net/die-eigene-cloud-nextcloud-installation-auf-einer-synology-diskstation-mit-dsm-6/

In den Plasma-Desktop einbinden
-------------------------------
## Mit WEBDAV in Dolphin
* Das bedeutet der Online-Ordner ist nahtlos in Dolphin integriert.
* 2017: webdav owncloud geht nicht mit dem Plasma "Network Folder Wizard": TODO: report bug
* Beispiel owncloud: Unten links bei "Einstellungen" gibt es eine WebDav-Option. Der Link hat bei mir so direkt nicht funktioniert, aber wenn du in Dolphin diese Adresse eingibst (und dann user und password), dann hast du den Ordner im Dolphin eingebunden: webdavs://<server>/remote.php/dav/files/<benutzer>/<ordner>

Weitere Ideen
-------------
* Idee: Cloud-Software-unabhängiges Programm zum Austausch von sehr großen Dateien
    * https://github.com/feinstaub/feinstaub.github.io/blob/master/asr/index.md (Prototyp, Entwicklung derzeit pausiert), todo: move elsewhere
