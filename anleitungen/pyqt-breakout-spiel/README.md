Mein Breakout-Spiel
===================

<!-- toc -->

  * [Ziel](#ziel)
  * [Schritt 1: Rechteck zeichnen](#schritt-1-rechteck-zeichnen)
    + [Farbe ändern](#farbe-andern)
    + [Position und Größe ändern](#position-und-grosse-andern)
    + [Fenstertitel ändern](#fenstertitel-andern)
    + [Fenstergröße ändern](#fenstergrosse-andern)
    + [Noch ein Rechteck](#noch-ein-rechteck)
  * [Schritt 2: Rechtecke verankern](#schritt-2-rechtecke-verankern)
  * [Schritt 3: Hintergrund und Text](#schritt-3-hintergrund-und-text)
- [Verfügbare Schriftarten auf der Konsole ausgeben](#verfugbare-schriftarten-auf-der-konsole-ausgeben)
  * [Schritt 4: Maus](#schritt-4-maus)

<!-- tocstop -->

Ziel
----
Das Ergebnis soll ungefähr so aussehen:

![](ziel.png)


Schritt 1: Rechteck zeichnen
----------------------------
Diese Datei bei dir speichern [b1-ein-rechteck.py](b1-ein-rechteck.py).

Programm starten

    python3 b1-ein-rechteck.py

### Farbe ändern
Schaue dir diese Zeile an:

    qp.setBrush(QBrush(QColor(255, 0, 0)))

Die Farbe wird aus Rot, Grün und Blau gebildet. Jeweils von 0 bis 255.
Das heißt für jeden der Farbkanäle Rot, Grün und Blau gibt es 256 Stufen.
0 bedeutet "Farbe ganz aus" und 255 bedeutet "Farbe ist ganz an".

    QColor(255, 0, 0)
            ^   ^  ^
            |   |  `------ blau
            |   |
            |   `--------- grün
            |
            `------------- rot

                    R    G    B
                ----------------
    Das ist Rot:  (255,   0,   0)
    Das ist Blau: (  0,   0, 255)
    Das ist Gelb: (255, 255,   0) (Rot + Grün = Gelb)

AUFGABE 1: Ändere die Farbe des Rechtecks auf Grün ab.

AUFGABE 2: Ändere die Farbe des Rechtecks auf Blau ab.

AUFGABE 3: Ändere die Farbe des Rechtecks auf Weiß ab.

AUFGABE 4: Ändere die Farbe des Rechtecks auf Schwarz ab.

AUFGABE 5: Ändere die Farbe des Rechtecks auf Gelb ab.

AUFGABE 6: Welche Farbe ergibt Rot + Blau?

AUFGABE 7: Welche Farbe ergibt Grün + Blau?

AUFGABE 8: Ändere die Farbe des Rechtecks auf einer Farbe deiner Wahl ab.

AUFGABE 9 (optional): Starte das Programm KColorChooser (siehe [Mehr zu Pixel und Farben](../../themen/graphik-und-zeichnen/README.md)), mische eine Farbe und übernehme die RGB-Werte in dein Programm.

AUFGABE 10 (optional): Starte KColorChooser, klicke "Farbe von Bildschirm" übernehmen und suche dir mit der Maus eine Farbe vom Bildschirm aus. Übernehme die RGB-Werte in dein Programm.

AUFGABE 11 (optional): Starte das Programm KMag und schaue dir damit die Pixel und Farben auf deinem Bildschirm genauer an. Dazu Vergrößerung auf 1:12 erhöhen und "Maus folgen" aktivieren.

### Position und Größe ändern
Schaue dir diese Zeile an:

    qp.drawRect(20, 30, 100, 70)

Das bedeuten die Zahlen:

    drawRect(20, 30, 100, 70)
              x   y   w    h

     -------------------------------------------------------
    |Fenster        ^
    |               |
    |               y
    |               |
    |               |     Rechteck
    |            -------------------   ^
    |<-- x ---->|                   |  |
    |           |<------- w ------->|  h = height = Höhe
    |           |  = width = Breite |  |
    |           |                   |  |
    |            -------------------   v
    |
     -------------------------------------------------------

AUFGABE 1: Verschiebe das Rechteck von oben links nach unten rechts.

AUFGABE 2: Ändere die Größe: 30 Pixel breit und 200 Pixel hoch.

### Fenstertitel ändern
Schaue dir diese Zeile an:

    self.setWindowTitle("Hallo Rechteck")

AUFGABE: Ändere den Titel auf einen Text deiner Wahl.

### Fenstergröße ändern
Schaue dir diese Zeile an:

    self.setGeometry(300, 300, 400, 300)
                      x    y    w    h

Sie bestimmt, wie das Fenster beim Start des Programms aussieht:

    x: Position des Fensters vom Bildschirmrand
    y: Position des Fensters vom Bildschirmrand
    w: Breite des Fensters
    h: Höhe des Fensters

AUFGABE: Mache das Fenster größer, z. B. 800 Pixel breit und 600 Pixel hoch.

### Noch ein Rechteck
Kopiere die Zeile

    qp.drawRect(20, 30, 100, 70)

direkt darunter, ändere die x- und y-Position und starte das Programm.
Es sollte ein zweites Rechteck erscheinen.

AUFGABE 1: Kopiere auch die Zeile mit setBrush und ändere die Farbe des zweiten Rechtecks.

AUFGABE 2: Verwende die Werte von Fensterbreite -und Höhe, um das zweite Rechteck ganz unten rechts in die Ecke zu zeichnen.
Was passiert, wenn man das Fenster dann mit der Maus größer oder kleiner zieht?


Schritt 2: Rechtecke verankern
------------------------------
Diese Datei bei dir speichern [b2-rechteck-unten.py](b2-rechteck-unten.py).

AUFGABE 1: Programm starten und das Fenster mit der Maus größer und kleiner machen. Was fällt auf?

Das wurde im Programmcode geändert:

* Bestimmte Parameter des Rechtecks werden jetzt in Variablen gespeichert.
* Die neue Funktion `def resizeEvent(self, e)`, die immer beim Ändern der Fenstergröße aufgerufen wird, sorgt dafür, dass die y-Koordinate des Rechtecks an die Fensterhöhe angepasst wird.

AUFGABE 2: Erzeuge insgesamt 5 Rechtecke. Eins oben links, eins oben rechts, eins unten links, eins unten rechts und eins in der Mitte des Fensters.

Jedes Rechteck soll eine andere Farbe bekommen.

AUFGABE 3: Wenn du die Größe des Fensters mit der Maus änderst, sollen die fünf Rechtecke weiterhin in ihrer Ecke bleiben.

* Das Rechteck links oben und links unten ist schon in Ordnung.
* Passe als nächstes das Rechteck oben rechts an.
* Dann unten rechts.
* Und zum Schluss das in der Mitte. Hinweis: Das sind die Grundrechenarten: `+ (Addition), - (Subtraktion), * (Multiplikation), / (Division)`


Schritt 3: Hintergrund und Text
-------------------------------
Diese Datei bei dir speichern [b3-text-und-hintergrund.py](b3-text-und-hintergrund.py).

AUFGABE 1: Programm starten und schauen, was sich geändert hat.

Neues:

1)

    # Verfügbare Schriftarten auf der Konsole ausgeben
    print(QFontDatabase().families())

2)

    Es wird oben ein blauer Text gezeichnet.
    qp.drawText(0, 20, text)
                x   ^
                    `---- y-Wert der Basislinie der Schrift. Auf der Basislinie "sitzen" die Buchstaben:
                          Die Buchstaben "A B C a b c" z. B. berühren unten genau die Basislinie.
                          g, y, p, das Komma (,) und die Klammern () ragen über die
                          Basislinie nach unten hinaus.
3)

    Es wird ein türkises Rechteck für den Hintergrund gezeichnet.

AUFGABE 2: Ändere Schriftgröße und Farbe Textes von blau auf schwarz.

AUFGABE 3: Suche dir in der Schriftartenliste eine andere Schrift aus (z. B. URW Bookman L) und verwende sie für den Text.

AUFGABE 4: Sorge dafür, dass die Schriftartenliste nicht mehr auf der Konsole erscheint, indem du die print-Zeile auskommentierst.

AUFGABE 5: Ändere die Schriftgröße und 14 auf 18.

AUFGABE 6: Ändere die Farbe des Hintergrund-Rechtecks auf schwarz.


Schritt 4: Maus
---------------
Schaue dir in [b4-maus.py](b4-maus.py) an wie man Mausereignisse behandelt.

AUFGABE 1: Sorge dafür, dass sich nur noch der Schläger mit der Maus bewegt (der Ball soll sich also nicht mehr bewegen).

AUFGABE 2: Sorge dafür, dass sich der Schläger rechts nicht mehr rausbewegen lässt. Verwende eine if-Anweisung.

Weiter mit [README2.md](README2.md).
