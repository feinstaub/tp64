Neuen Schüler-User anlegen
==========================

* Einloggen: $ ssh ag@ag-pc
    * siehe [admin-odroid-vorbereiten.md](admin-odroid-vorbereiten.md)

Alle bisherigen User mit Home-Verzeichnis auflisten

    $ cat /etc/passwd | grep /home

Alle Gruppen auflisten

    * $ cat /etc/group
    * $ cut -d: -f1 /etc/group
    * Die ID der users-Gruppe: $ cat /etc/group | grep users

Neuen Test-User s2 hinzufügen
-----------------------------
useradd:

    $ sudo useradd --shell /bin/bash --gid 100 --create-home s2

Passwort festlegen (Schüler tippt das ein):

    $ sudo passwd s2

Test-Einloggen:

    $ ssh s2@ag-pc

Danach für XFCE4 die .xsessionrc kopieren, siehe admin-main.md
