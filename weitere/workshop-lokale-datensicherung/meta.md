Lokale Datensicherung / Meta
============================

Alternative Titel:

* "Lokale Datensicherung im Privatumfeld"
* "Private Datensicherung"

Lernziele:

* Gutes Gefühl erzeugen, dass eine einfache USB-Offline-Sicherung, wo das Medium jedesmal manuell eingebunden wird, eine gute Maßnahme für die private Datensicherung ist.
    * Vor- und Nachteile von always-connected-Lösungen aufzeigen
* Praktisch einüben

Kursleiter bringt mit:

* Laptop mit Präsentation
* Externe Festplatte als Demo
* USB-Stick als Demo
* FreeFileSync heruntergeladen von https://www.freefilesync.org/download.php
    * FreeFileSync_9.0_Windows_Setup.exe
    * Extraktion:
        * Kann zwar mit p7zip extrahiert werden ($ 7z x ...), aber die extrahierte Datendatei dann nicht mehr
        * Setup mit wine möglich
            * lokal auswählen; die portable Version ist nur mit der "Spendenversion" möglich
            * Daten sind dann hier: ~/.wine/drive_c/Program Files/FreeFileSync/
* Bereitet vor:
    * Link zum Kursmaterial ins Kursnetzwerk legen oder anschreiben
    * Ggf. System openSUSE-Live-DVD starten und FreeFileSync installieren als Demo-Rechner

Teilnehmer bringen mit:

* eigenen Rechner (ansonsten EDV-Raum-Rechner, ggf. mit openSUSE-Live-DVD)
* eigenen USB-Stick (ansonsten Übung mit einem Verzeichnis auf dem gleichen Dateisystem)

Einstieg:

* verbrannter Laptop
* Warum sind Sie hier?
    * Art der Daten / Welche Daten?
        * Bisherige TL-Wünsche: Bilder, Firefox-Einstellungen, E-Mail
    * Finden sich da alle wieder?
    * Erwartungen? -> Aufschreiben -> auf TL-Wünsche eingehen (gerade, wenn es wenige TL sind)
* Abwägung Vertrauen / Anvertrauen privater Daten
    * Cloud: große intern. IT-Unternehmen?
    * lokal: eigenes Wissen / lokale Hilfe durch vertrauenswürdige Personen

"Cloud":

* = fremde Rechner im Internet, siehe auch cloud.meta (todo)
* AGBs gelesen?
* je größer, desto interessanter für Angriffe
* folgendes sollte man nicht in fremde Hände geben: ...
* "Cloud clouds peoples minds" (RMS). Eine Cloud sind die Computer anderer Leute in potentiell anderen Ländern unter anderen Jurisdiktionen

Ende:

* Wer möchte das Kursmaterial per E-Mail zugesandt bekommen?
* PDF: siehe ~/dataDocuments/FS-Angebote-weitere/fsworkshop.net/datensicherung.meta / todo

TODO/offene Punkte:

* LibreOffice-Dokumente per Kommandozeile vergleichen
* siehe ~/dataDocuments/FS-Angebote-weitere/fsworkshop.net/datensicherung.meta

Ausblick / Weitere Ideen:

* ownCloud, nextcloud als Backup/Sync-Server
* Wiederherstellungstool für gelöschte Dateien:
    * ?, todo?
    * Vorteil externer Datenträger mit obiger Backup-Strategie:
        * Alle Dateien liegen darauf herum und können ohne Zusatzsoftware von jedem Computer aus angesehen werden.
        * andere Personen können helfen
* Datensicherung vom Mobiltelefon
* Bilder kleiner machen bei Samsung-Handy
* CloneZilla
* Kursdauer: derzeit 2 h, bei 3 interessierten Personen sind 2,5 h möglicherweise besser

Erwartungen 2017

- "Wie sichere ich meine Fotos? Bisher nicht systematisch gesichert."
- "Bisher Ordner 1:1 auf externe Festplatte kopiert. Neues soll einfach dazu."
    - will wissen, welche Systemdateien/Programme notwendig sind, um Daten später wieder zu lesen
- "Interne Festplatte auf externe spiegeln"

