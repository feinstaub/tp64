v1

- Basic Wiring


v2

- Create an array of cells with Javascript


v3

- Replace Background image with custom gradient: MyBackground.qml
- Replace Stone image with custom component: Block.qml and use in Cell.qml
- Cell.qml: use property
- samegame.qml: Start script on startup and react on size changes
    - https://stackoverflow.com/questions/19987177/does-an-item-emit-a-signal-when-its-position-size-is-changed-in-qml

v4

- TODO weiter mit: QML Advanced Tutorial 3 - Implementing the Game Logic
    - siehe
        - https://doc.qt.io/qt-5/qtdoc-demos-maroon-example.html
        - https://doc.qt.io/qt-5/qtdoc-demos-samegame-content-gamearea-qml.html
        - https://doc.qt.io/qt-5/qtdoc-demos-samegame-samegame-qml.html
        - https://doc.qt.io/qt-5/qtdoc-demos-samegame-content-samegame-js.html
        - https://doc.qt.io/qt-5/qtdoc-demos-samegame-example.html
- next: implementing timer and game loop
