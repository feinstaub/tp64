Replicant 6 auf Samsung Galaxy S2
=================================

OK

<!-- toc -->

- [Hintergrund](#hintergrund)
- [Installation](#installation)
  * [Arbeitsverzeichnis anlegen](#arbeitsverzeichnis-anlegen)
  * [Dateien herunterladen](#dateien-herunterladen)
  * [Heimdall installieren](#heimdall-installieren)
  * [adb installieren](#adb-installieren)
  * [Dateien auf das Gerät kopieren](#dateien-auf-das-gerat-kopieren)
  * [Gerät vorbereiten](#gerat-vorbereiten)
  * [Images installieren](#images-installieren)
  * [Einrichten](#einrichten)
- [Dateitransfer](#dateitransfer)
  * [USB-Mass-Storage scheint nicht zu gehen](#usb-mass-storage-scheint-nicht-zu-gehen)
  * [via ADB](#via-adb)
- [Reverse Thethering (dem Smartphone via USB zu Internet verhelfen)](#reverse-thethering-dem-smartphone-via-usb-zu-internet-verhelfen)
  * [Vorbereitung](#vorbereitung)
  * [Root access für ADB](#root-access-fur-adb)
  * [Script kopieren und starten](#script-kopieren-und-starten)
  * [Neue Internet-Connection mit Plasma anlegen (ging nicht)](#neue-internet-connection-mit-plasma-anlegen-ging-nicht)
- [Ende 2017: Hardwarefehler](#ende-2017-hardwarefehler)

<!-- tocstop -->

Hintergrund
-----------
* Replicant-Webseite: https://www.replicant.us/
* Material:
    * Gerät: Samsung Galaxy S2
    * Computer mit openSUSE Linux Tumbleweed
    * USB-Kabel (normal auf Micro-USB)
* Durchführung: 2017 => ERFOLG

Installation
------------
Quellen:

* Wiki-Seite zum Gerät: https://redmine.replicant.us/projects/replicant/wiki/GalaxyS2I9100
* Installationsanleitung: https://redmine.replicant.us/projects/replicant/wiki/GalaxyS2I9100Installation
    * "Moreover, it is assumed that anyone performing the installation knows how to use command lines in a terminal and has basic knowledge about it."

### Arbeitsverzeichnis anlegen

    $ mkdir ~/dev/replicant_s2/
    $ cd ~/dev/replicant_s2/

### Dateien herunterladen

Quelle: https://redmine.replicant.us/projects/replicant/wiki/ReplicantImages#Replicant-60-0002-images
Dateien:

* replicant-6.0-i9100.zip, ca. 270 MB
* replicant-6.0-i9100.zip.asc
* recovery-i9100.img, ca. 5 MB
* recovery-i9100.img.asc
* i9100.sha256

Release-Schlüssel importieren:

    $ gpg --recv-key 5816A24C10757FC4

    key 16D1FEEE4A80EB23:
    5 signatures not checked due to missing keys
    gpg: key 16D1FEEE4A80EB23: public key "Replicant project release key <contact@replicant.us>" imported
    gpg: marginals needed: 3  completes needed: 1  trust model: pgp
    gpg: depth: 0  valid:   1  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 1u
    gpg: Total number processed: 1
    gpg:               imported: 1

Downloads - Signatur verifizieren:

    $ gpg --armor --verify replicant-6.0-i9100.zip.asc replicant-6.0-i9100.zip

    gpg: Signature made So 10 Sep 2017 16:55:49 CEST
    gpg:                using RSA key 0F30D1A02F73F70A6FEE048E5816A24C10757FC4
    gpg: Good signature from "Wolfgang Wiedmeyer <wolfgang@wiedmeyer.de>" [unknown]
    gpg:                 aka "Wolfgang Wiedmeyer <wreg@wiedmeyer.de>" [unknown]
    gpg:                 aka "Wolfgang Wiedmeyer <wolfgit@wiedmeyer.de>" [unknown]
    gpg: WARNING: This key is not certified with a trusted signature!
    gpg:          There is no indication that the signature belongs to the owner.
    Primary key fingerprint: 0F30 D1A0 2F73 F70A 6FEE  048E 5816 A24C 1075 7FC4

    $ gpg --armor --verify recovery-i9100.img.asc recovery-i9100.img

    gpg: Signature made So 10 Sep 2017 16:55:46 CEST
    gpg:                using RSA key 0F30D1A02F73F70A6FEE048E5816A24C10757FC4
    gpg: Good signature from "Wolfgang Wiedmeyer <wolfgang@wiedmeyer.de>" [unknown]
    gpg:                 aka "Wolfgang Wiedmeyer <wreg@wiedmeyer.de>" [unknown]
    gpg:                 aka "Wolfgang Wiedmeyer <wolfgit@wiedmeyer.de>" [unknown]
    gpg: WARNING: This key is not certified with a trusted signature!
    gpg:          There is no indication that the signature belongs to the owner.
    Primary key fingerprint: 0F30 D1A0 2F73 F70A 6FEE  048E 5816 A24C 1075 7FC4


Downloads - Prüfsummen verifizieren:

    $ sha256sum -c i9100.sha256

    recovery-i9100.img: OK
    replicant-6.0-i9100.zip: OK


### Heimdall installieren

Heimdall ist ein Flash-Programm für Samsung-Geräte.

Installieren von openSUSE: https://software.opensuse.org/package/heimdall mit 1 Click Install von:

* Repository **hardware** / version 1.4.2

### adb installieren

ADB = Android Debug Bridge

    $ sudo zypper install android-tools

### Dateien auf das Gerät kopieren

* Gerät mit USB mit dem Computer verbinden
* Bisheriges Betriebssystem: Android 4.1.2
* Einstellungen > Drahtlos und Netzwerke > Weitere Einstellungen > USB-Verbindung > Massenspeichermodus aktivieren > Speicher mit PC verbinden
* "USB ist verbinden. Entfernen Sie das Kabel"
* Kabel ab. Nochmal klicken. Kabel dran.
* "Speicher mit PC verbinden" > OK. Einige Sekunden warten.
* Plasma Device Notifier > Open with File Manager

* Die Datei **replicant-6.0-i9100.zip** ins Wurzelverzeichnis (= oberste Ebene) kopieren.

* Plasma: "Savely Remove" device
* Gerät: "USB-Speicher ausschalten"

### Gerät vorbereiten

ACHTUNG. Hier beginnt der mögliche komplette Datenverlust auf dem Gerät.

* "Make sure the device is completely turned off and the USB cable is disconnected from the device"
    * Ausschalten, USB-Kabel trennen
* "Start the device by holding the following key combination: Volume down, Select, Power"
    * "Lautstärke runter" (linke Seite), "Home-Button" (mittlere Taste vorne unten), "Anschalt-Knopf" (rechte Seite)
    * Halten bis eine Warnung erscheint: "Warning!! A custom OS can cause critical problems in phone and installed applications"
* "Volume up: Continue" auswählen.
* Der Bildschirm zeigt
    "ODIN MODE
    PRODUCT NAME: GT-i9100
    CUSTOM BINARY DOWNLOAD: NO
    CURRENT BINARY: SAMSUNG OFFICIAL

    Downloading...
    Do not turn off target !!"

* "Connect the USB cable to both the computer and the device"

### Images installieren

Mit heimdall flashen (Dauer: 10 Sekunden).

    $ heimdall flash --KERNEL recovery-i9100.img

    Heimdall v1.4.2

    Copyright (c) 2010-2017 Benjamin Dobell, Glass Echidna
    http://www.glassechidna.com.au/

    This software is provided free of charge. Copying and redistribution is
    encouraged.

    If you appreciate this software and you would like to support future
    development please consider donating:
    http://www.glassechidna.com.au/donate/

    Initialising connection...
    Detecting device...
    Claiming interface...
    Setting up interface...

    Initialising protocol...
    Protocol initialisation successful.

    Beginning session...

    Some devices may take up to 2 minutes to respond.
    Please be patient!

    Session begun.

    Downloading device's PIT file...
    PIT file download successful.

    Uploading KERNEL
    100%
    KERNEL upload successful

    Ending session...
    Rebooting device...
    Releasing device interface...

Gerät startet neu. Man sieht ein gelbes Ausrufezeichen beim Samsung-Startbildschirm.

Danach sieht man folgenden Bildschirm:

    Replicant
    RECOVERY

    - Reboot system now
    - Apply update
    - Factory reset
    - Advanced

* **Factory reset** auswählen ("A data wipe is necessary if you switch from the factory image or a different Android distribution to Replicant")
* **Wipe data (keep media)** auswählen
* **YES** auswählen
* Warten (einige Sekunden)
* "Zurück" drücken (zum Hauptmenü)
* **Apply update** auswählen
* **Choose from sdcard0** auswählen
* **replicant-6.0-i9100.zip** auswählen
* Warten (einige Minuten). Man sieht eine Installationsanimation, einen gelben Fortschrittsbalken und Statustextausgaben.
    * bei ca. 25% geht es eine Weile nicht weiter. Dann ist es plötzlich fertig und man sieht wieder den "Replicant RECOVERY"-Bildschirm

* "If you did the data wipe after step 2, you will need to do a factory reset as well:"
    * Select **Factory reset**
    * Select **Full factory reset**
    * Confirm the factory reset by selecting **Yes**
    * Press the **back key** (if necessary) to get back to the general menu
* Select **Reboot system now** to reboot the device

* **Your device should now be running Replicant!**
    * Man sieht aber immer noch den Samsung-Startbildschirm mit dem gelben Ausrufezeichen.
        * TODO: Ist das normal?
    * Dann erscheint der Replicant-Boot-Bildschirm und bleibt eine recht lange Zeit (über 1 Minute)
    * Dann erscheint ein Fortschrittsbalken unten, wo 85 apps vorbereitet werden (über 3 MInuten)

### Einrichten

* Sprachauswahl
    * English (United States)
* Date & Time
    * Amsterdam, Berlin (GMT+1:00)
    * Datum und Zeit einstellen
* "SIM card missing" message
    * Skip
* Choose screen lock
    * PIN: 0000
* Replicant features
    * Enable Privacy guard? --> Erst mal nicht.
* FERTIG

* Ein bisschen rumklicken.
    * z. B. Kamera funktioniert
    * Root access für RepWifi einrichten. Geht aber nur, wenn der Wifi-Adapter angeschlossen ist.

* Neu starten.

Dateitransfer
-------------
### USB-Mass-Storage scheint nicht zu gehen
(TODO)

### via ADB
* Developer Options > Debugging > Android Debugging (Enable adb) > Confirm computers fingerprint

    $ adb devices

    List of devices attached
    00095da2148b6f  device

    $ adb push BoxCat_Games_-_19_-_Mission\ \(freemusicarchive.org\).mp3 /storage/611C-161B/Music

* Woher weiß man "/storage/611C-161B"? Siehe File Manager

* Musik geht.

Reverse Thethering (dem Smartphone via USB zu Internet verhelfen)
-----------------------------------------------------------------
- Warum? Weil kein WLAN funktioniert oder nicht verwendet werden will.
- STATUS: bisher fehlgeschlagen
- Nach dieser Anleitung: https://redmine.replicant.us/projects/replicant/wiki/ReplicantUSBNetworking

### Vorbereitung

    $ cd ~/dev/replicant_s2/
    $ mkdir tether
    $ cd tether
    $ wget https://git.replicant.us/replicant/user-scripts/plain/networking/usb/usb_networking_device.sh

### Root access für ADB

Settings > Developer Options (zweites von unten) > **Android debugging** (ungefähr in der Mitte) > enable

    $ adb devices
    $ adb shell
    $ adb root
    restarting adbd as root

Settings > Developer Options (zweites von unten) > **Root access** (ca. 10tes von oben) > "Apps und ADB"

    $ > logout
    $ adb remount
    remount succeeded

### Script kopieren und starten

    $ adb push usb_networking_device.sh /data/
    $ adb shell chmod a+x /data/usb_networking_device.sh
    $ adb shell /data/usb_networking_device.sh start1
    Starting Replicant USB networking, phase 1

Es poppt eine neue Verbindung auf, aber die geht nicht (findet keine Verbindung)

### Neue Internet-Connection mit Plasma anlegen (ging nicht)

1. In the network manager applet, create a new "Ethernet" or "Wired" connection.
2. In the tab for IPv4 settings, select the method "Shared to other computers".
3. Save the connection, preferably with a distinguishable name (The name can be changed at the top of the edit window).
4. Select this connection for your device. (todo: was heißt das?)

    $ adb shell /data/usb_networking_device.sh start2
    error: insufficient permissions for device
--> TODO

reconnect

oder: ohne start1:
    $ adb shell /data/usb_networking_device.sh start2
    Starting Replicant USB networking, phase 2
    Clearing network configuration
    400 0 destroyNetwork() failed (Machine is not on the network)
    200 0 Interface IP addresses cleared
    Configuring DHCP, please wait
    ifconfig: ioctl 8913: No such device

WARTE: https://redmine.replicant.us/boards/9/topics/6915

Ende 2017: Hardwarefehler
-------------------------
Plötzlich funktioniert die Mobilfunk-Verbindung nicht mehr. Kein Netz. Also kein Telefonieren oder Datentransfer mehr möglich.
