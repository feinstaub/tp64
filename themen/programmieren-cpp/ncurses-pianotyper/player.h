#pragma once
#include "noten_tasten.h"
#include "tools.h"
#include <filesystem>

void play_note_async(double sec, std::vector<int> freqs)
{
    using namespace std;
    namespace fs = std::filesystem;

    string filename = "tone-cache/tmp_";
    for (int freq : freqs) {
        filename += "sin_" + to_string(freq) + "_";
    }
    filename += to_string(sec) + "s";
    double vol = (440.0 / freqs[0]) / 6.0; // 6.0, um ca. maximal 1.0 zu haben
    filename += to_string(vol) + "vol";
    filename += ".wav";

    if (!fs::exists(filename)) {
        string cmd = "sox -n " + filename + " synth " + to_string(sec);

        for (int freq : freqs) {
            cmd += " sin " + to_string(freq);
        }
        cmd += " vol " + to_string(vol);
        //cout << cmd << endl;
        system(cmd.c_str());
    }

    // -q for quite
    string cmd = "play -q " + filename + " &"; // in background
    system(cmd.c_str());

    //system("rm tmp.wav"); // todo
}

void play_note_async(double sec, int freq)
{
    play_note_async(sec, { freq });
}

void play_note_async(double sec, int freq, int overtone_count)
{
    std::vector<int> freqs;
    for (int i = 1; i <= overtone_count + 1; i++)
    {
        freqs.push_back(freq * i);
    }
    play_note_async(sec, freqs);
}

void play_note_from_taste_async(int nr, double len)
{
    play_note_async(len, taste_zu_frequenz(nr), 2);
}

class NotePlayer
{
public:
    /**
     * play with default speed etc.
     * todo bpm wie in themen/multimedia-audio/audaspace/tetris.py
     */
    void play(const MusicSheet& sheet)
    {
        for (auto& noteColumn : sheet.noteColumns)
        {
            for (auto& note : noteColumn.notes)
            {
                play_note_from_taste_async(note.key, base_len / note.inv_dur);
            }
            tools::sleep_ms(base_len * 1000);
        }
    }

    // Dauer in Sekunden einer ganzer Note
    double base_len = 0.2;
};
