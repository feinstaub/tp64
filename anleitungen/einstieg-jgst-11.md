Einstieg - Jgst. 11
===================

Start
-----
* siehe [Einstieg Jgst. 7](einstieg-jgst-7.md)

Programmieren
-------------
* Qt: [Python-Qt-Einstieg](../anleitungen/python-qt)
* [KDE kompilieren](../anleitungen/kde-kompilieren)

Projekt-Ideen
-------------
### KDE / C++
* Ein Plugin für Kate schreiben, siehe [Kate-Tipps > Projektideen](../anleitungen/kate-tipps)

### Fortgeschritten
* Linux-basierte Smartphone-Apps mit Plasma Mobile Plasma Mobile und Kirigami, siehe programmieren-mobile
* [Das Projekt Oma-Linux](../themen/projekt-oma-linux)
* [Replicant oder LineageOS auf eigenem Mobiltelefon installieren](../themen/eigenes-mobil-betriebssystem-installieren)
