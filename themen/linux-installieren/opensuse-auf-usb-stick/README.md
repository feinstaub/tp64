GNU/Linux auf einen USB-Stick installieren
==========================================

ACHTUNG:

* Sehr genaues Arbeiten erforderlich, da es ansonsten zu Datenverlust kommen kann. Bei Unklarheiten nicht einfach weitermachen, sondern jemanden fragen.

<!-- toc -->

- [Übersicht](#ubersicht)
- [Installationsprogramm starten](#installationsprogramm-starten)
- [Installation durchführen](#installation-durchfuhren)
- [Rechner neu starten und von USB-Stick booten](#rechner-neu-starten-und-von-usb-stick-booten)
- [Nächste Schritte](#nachste-schritte)
- [Anhang](#anhang)
  * [Bekannte Probleme und Lösungen](#bekannte-probleme-und-losungen)
  * [Abgrenzung](#abgrenzung)
  * [Eignung des USB-Sticks testen](#eignung-des-usb-sticks-testen)
  * [openSUSE-Installations-DVD beschaffen](#opensuse-installations-dvd-beschaffen)
  * [Alternative zur Installations-DVD](#alternative-zur-installations-dvd)
  * [USB-Stick klonen](#usb-stick-klonen)
  * [Tumbleweed-System aktualisieren](#tumbleweed-system-aktualisieren)
  * [Leap-System aktualisieren](#leap-system-aktualisieren)
  * [Festplatten und USB-Sticks partitionieren](#festplatten-und-usb-sticks-partitionieren)
  * [openSUSE-Partitions-Setup beim Drüberinstallieren](#opensuse-partitions-setup-beim-druberinstallieren)
  * [Sonstiges](#sonstiges)

<!-- tocstop -->

Übersicht
---------
Folgende Dinge sind nötig:

* Ein **Rechner** mit DVD-Laufwerk, USB-Port und Zugang zum Boot-Menü (z. B. mit F12) vorhanden
    * Am besten 64 Bit, 32 Bit geht aber auch
    * Beim Laptop **Netzteil nicht vergessen**, wegen Akku.
* Ein leerer **USB-Stick** mit min. 32 GB Speicherplatz und ausreichend Geschwindigkeit
    * Eignungstest siehe Anhang
* Die openSUSE-Tumbleweed-**Installations-DVD**
    * Beschaffung siehe Anhang
    * Das Medium wird nur einmalig zur Installation benötigt. Updates und weitere Pakete werden mittels Paketmanager aus dem Internet laden.
    * Alternative: einen [Installations-**USB-Stick** erstellen](bootable-usb-stick-erzeugen.md)

Installationsprogramm starten
-----------------------------
* Rechner einschalten
* (z. B. mit F12) ins Bootmenü gelangen.
* openSUSE-Linux-Installations-DVD einlegen
* "CD/DVD" aus der Liste auswählen und Enter drücken.
    * Falls eine Fehlermeldung kommt, die DVD eingelegt lassen und den Rechner mit Strg+Alt+Entf neu starten.
* WARTEN, bis das Installationsprogramm geladen ist

Installation durchführen
------------------------
* Freie Lizenz akzeptieren
* Sprache einstellen
* Uhrzeit und Zeitzone einstellen
* ...
* **!!!** Bei der **Partitionierung** für einen USB-Stick gut aufpassen:
    * das Ziellaufwerk von /dev/sd**a** (falsch!) auf /dev/sd**b** (richtig) umstellen
* **Aufteilung/Partitionierung:**
    * Ab einer Größe des Sticks von 32-GB ist eine **Aufteilung von root (/) und /home** empfehlenswert
        * **root (/)**: 22 GB (hier werden das System und alle Programme gespeichert)
            * wenn man viele Programme und Dienste installiert, wird min. 40 GB empfohlen; mit 60 GB ist man auf der sicheren Seite (Desktop-Rechner)
            * Hier ist oft das Dateisystem btrfs ist vorausgewählt
                * für USB-Stick-Installationen besser **auf ext4 umstellen**
        * **/home**: 10 GB (hier landen alle Benutzer-Einstellungen und eigenen Dateien)
            * (empfohlende Größe ab 5 GB oder viel viel mehr, je nachdem, was man alles speichern möchte)
            * Das Dateisystem sollte **ebenfalls ext4** sein
                * (muss also ggf. von XFS umgestellt werden)
        * Bei einem 64-GB-Stick ist folgende Aufteilung empfehlenswert:
            * root: 40 GB, /home: 24 GB (falls man auf /home mehr braucht, kann man sich später einen Ordner von root reinlinken)
    * Kein swap für USB-Sticks
        * 2 GB swap - Auslagerungsplatz - wird nur empfohlen, wenn man direkt auf Festplatte installiert
* ...
* **Benutzernamen und Passwort wählen**
    * Das Passwort gut merken und aufpassen wegen Sonderzeichen oder y und z, wegen englischer vs. deutscher Tastatur
    * Wenn man das root-Passwort nicht mehr weiß, muss man das System neu installieren.
* Automatische Anmeldung **nicht** aktivieren
    * das heißt, man muss zum Einloggen sein Passwort eingeben
* ...
* **Installation starten**
    * WARTEN, dauert ca. 30 Minuten

Rechner neu starten und von USB-Stick booten
--------------------------------------------
* Im Bootmenü "USB" auswählen und Enter drücken.
* Wenn alles gut gegangen ist, startet das System und man kann sich mit seinem Benutzernamen und Passwort in die KDE/Plasma-Oberfläche einloggen.

Nächste Schritte
----------------
* System erkunden:
    * todo: einfache Dinge, die man mit einem frisch installierten openSUSE Tumbleweed machen kann, bevor es richtig losgeht
* Neue Software installieren:
    * http://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Arbeitsbl%C3%A4tter/Software_installieren (todo move to here)
* Arbeitsblätter:
    * Farben und Pixel (todo move to here)

Anhang
------

### Bekannte Probleme und Lösungen
* [openSUSE Probleme und Lösungen](opensuse-probleme-und-lösungen.md)

### Abgrenzung
* Es gibt verschiedene Arten GNU/Linux zu verwenden oder zu installieren, siehe z. B. https://de.wikitolearn.org/Course:Einstieg_in_die_Programmierung_mit_Python/Eigenes_System/GNU,Linux_installieren (VirtualBox, Live-CD, Festplatte etc.)

* Diese Anleitung beschreibt wie openSUSE Tumbleweed von einer Installations-DVD auf einen USB-Stick installiert wird.

* Hinweis: Dieses so installierte USB-Stick-System funktioniert möglicherweise auf anderes konfigurierter Hardware (z. B. unterschiedliche Anzahl von eingebauten Festplatten) nicht.

### Eignung des USB-Sticks testen
Eigenschaften des USB-Sticks, auf dem das System installiert werden soll:

* empfohlene Größe: 32 GB
* auf USB 3.0 achten (auch drauf achten, dass manchmal USB 3.0 draufsteht, obwohl der Speicher dennoch langsam ist)
* Alternativ: externe Festplatte; diese hat eine verlässlichere Übertragungsrate

* Hintergrund:
    * Ein Betriebssystem wie openSUSE Linux schreibt oft viele kleine Dateien. Das heißt, der Datenträger sollte, neben einer allgemeinen hohen Datenrate, besonders bei kleinen Dateien schnell sein.
    * Empfehlung derzeit (Stand 30.03.2017): [USB-Stick 32 GB SanDisk Ultra Fit](https://www.conrad.de/de/usb-stick-32-gb-sandisk-ultra-fit-schwarz-sdcz43-032g-g46-usb-30-1229587.html) (14 EUR)

USB-Stick-Testprogramm installieren und starten:

$ sudo zypper install [gnome-disk-utility](https://en.wikipedia.org/wiki/GNOME_Disks)

Das Programm ist unter dem Namen "Disks" zu finden.

USB-Stick testen:

* Disks starten
* USB-Stick einstecken
* Das Gerät erscheint in Disks
* Menü oben rechts -> Benchmark Disk...
* Unten links -> Start Benchmark...
* Number of samples: 100 ist ok
* Sample size: auf 1 MiB reduzieren
* Start Benchmarking...

Hier sind einige [Beispiele von Benchmark-Ergebnissen](gnome-disk-benchmark-beispiele.md).

### openSUSE-Installations-DVD beschaffen
* https://en.opensuse.org/openSUSE:Tumbleweed_installation besuchen

Installations-DVD-iso-Datei herunterladen:

* für 64 Bit-Rechner: "DVD Installation - x86_64" (ca. 4,5 GB) herunterladen
* für 32 Bit-Rechner: "DVD Installation - i586" (ca. 4,5 GB) herunterladen
* mittels "GPG signed SHA-256" Validität prüfen (mit Dolphin -> rechte Maustaste -> Properties -> Checksums)

Die iso-Datei auf ein leeres DVD-Medium brennen:

* Mit **K3B**: in Dolphin Klick auf die iso-Datei
* Oder mit **gnome-disk-utility** (falls installiert): rechte Maustaste auf die iso-Datei -> Open with... -> Disk Image Writer
* (Mit [Windows geht das so](https://www.pcwelt.de/a/iso-so-brennen-sie-image-dateien-richtig,2342556), 2017)

Die DVD beschriften.

### Alternative zur Installations-DVD
* auf einen weiteren USB-Stick "bootable" speichern.

### USB-Stick klonen
Es ist möglich einen bereits installierten USB-Stick 1:1 zu kopieren. Siehe [USB-Stick klonen](../usb-stick-klonen).

### Tumbleweed-System aktualisieren
* siehe https://cgit.kde.org/scratch/gregormi/codestruct-util.git/tree/bin (todo)
    * `tool-sysrepo-dist-upgrade` ("Make sure the main repos have 98 priority.")
* technischer Hintergrund:
    * 2017: release-based (openSUSE Leap) vs. rolling (openSUSE Tumbleweed) releases
        * Lesenswerter Artikel: https://lwn.net/Articles/717489/
    * 2016: Falls eine Installation komplett (traumatic) schiefgeht, könnte man auch immer noch Snapper nutzen, um den Systemzustand zurückzurollen:
        * https://en.opensuse.org/openSUSE:Snapper_Tutorial (siehe auch Screenshot unten auf der Seite). Ein Snapshot wird durch ein zypper dup automatisch ausgelöst. Snapper benötigt BTRFS als root-Dateisystem
    * April 2017: Fehlermeldung
        * https://forums.opensuse.org/showthread.php/524002-KDE-update-quot-A-security-trust-relationship-is-not-present-quot
        * Es gibt auch schon einen KDE-Bug dazu: https://bugs.kde.org/show_bug.cgi?id=378286, der wiederum auf https://bugzilla.opensuse.org/show_bug.cgi?id=1030829 verweist. Das Problem ist also bekannt und es wird dran gearbeitet.
* Nach einem Upgrade, falls zwei Tastatur-Symbole neben der Uhr sind:
    * siehe [openSUSE Probleme und Lösungen](opensuse-probleme-und-lösungen.md)

### Leap-System aktualisieren
* siehe z. B. [hier](https://www.heise.de/forum/heise-online/News-Kommentare/Linux-Distribution-OpenSuse-Leap-42-3-veroeffentlicht/laeuft/posting-30775889/show/)

    foobar:~ # sed -i s/42.2/42.3/ /etc/zypp/repos.d/*
    foobar:~ # zypper dup

### Festplatten und USB-Sticks partitionieren
* mit GParted
    * http://gparted.org
    * Live-CD: http://gparted.org/livecd.php

### openSUSE-Partitions-Setup beim Drüberinstallieren
* 2018: bei der 32-Bit-Version von openSUSE Tumbleweed gab es Berichte, dass beim Partitionssetup der Wizard zum Auswählen eines anderen Ziellaufwerks (USB-Stick) fehlt. In diesem Fall muss mit "Start with existing partitions" gearbeitet werden und die Partitionen manuell eingerichtet werden.

### Sonstiges
* [Sonstiges](sonstiges.md) (openSUSE-Tumbleweed-**Live**-DVD herunterladen, Live-DVD)
* [Blog-Eintrag zu GNOME Disks](http://pothos.blogsport.eu/2017/07/25/gnome-disks-integrate-resize-and-repair/) zu neuer Funktion, 2017
