Mathematische Funktionsplots
============================

* mit PyQt ([Python-Qt-Einstieg](../../anleitungen/python-qt))
* Achsen mit Beschriftung
* achsenspezifisches Zoomen

Ideen / andere Programme
------------------------
* http://fooplot.com
    * Besonderheit: Graph-**Export als SVG** (-> bearbeitbar mit Inkscape)
        * siehe Quellcode (JavaScript): https://github.com/dheera/fooplot
