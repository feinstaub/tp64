#!/usr/bin/env python3

import time

c = 0
i = 0
inc = 1

while c < 400:
    s = ""
    a = 0
    while a <= i:
        s = s + "#"
        a = a + 1
    #print(str(i) + " " + s)
    print(s + " " + str(i))

    if i > 50:
        inc = -inc
    elif inc < 0 and i < 1:
        inc = -inc
    i = i + inc

    if c == 100:
        inc = 2
    elif c == 200:
        inc = 4
    elif c == 300:
        inc = 1

    c = c + 1

    time.sleep(0.01)
