Inkscape-Sprache auf Deutsch einstellen.

* Menü **Edit** > **Preferences**
* links **Interface** auswählen
* ganz oben bei **Language** in der Combobox **German (de)** auswählen
* Inkscape neu starten


