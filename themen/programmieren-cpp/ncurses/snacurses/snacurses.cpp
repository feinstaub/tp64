// Snake with ncurses

// TODO!
//      Multiplayer mode
//          Status line: "press ASDW to enter with a new snake"
//          Press F7 to toggle AI
//      Levels (speed)
//          Status line: press Bild auf to increase speed
//      Multiwindow (status monitor)
//      Walls and Water
//      Status line:
//          Fruits left to eat
//          Yummy! einblenden
//      Load levels from disk?
//          Labyrinth?

#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <chrono>
#include <cstring>
#include <string>
#include <vector>
#include <queue>

struct Point
{
    explicit Point() { }

    explicit Point(int x_, int y_) {
        x = x_;
        y = y_;
    }

    int x;
    int y;
};

enum Direction { UP, DOWN, LEFT, RIGHT };

class Snake
{
public:
    /**
     * Creates an initial snake at x,y with total length
     */
    explicit Snake(int x, int y, int length) {
        points_.emplace_back(x, y);
        for (int i = 1; i < length; i++) {
            points_.emplace_back(x - i, y);
        }
    }

    const std::vector<Point>& points() const {
        return points_;
    }

    const Point& head() const {
        return points_.front();
    }

    Direction dir() {
        return direction_;
    }

    void change_dir(Direction dir) {
        dirqueue_.push(dir);
    }

    void next_step() {
        Point newpoint { head().x, head().y };
        if (!dirqueue_.empty()) {
            direction_ = dirqueue_.front();
            dirqueue_.pop();
        }
        switch (direction_)
        {
            case UP:
                newpoint.y--;
                break;
            case DOWN:
                newpoint.y++;
                break;
            case LEFT:
                newpoint.x--;
                break;
            case RIGHT:
                newpoint.x++;
                break;
        }

        auto oldbackpoint = points_.back(); // save for potential growing later

        //    0 1 2 3
        // => 0 0 1 2
        for (int i = points_.size(); i > 1; i--) {
            points_[i - 1] = points_[i - 2];
        }

        points_[0] = newpoint;

        if (grow_) {
            points_.push_back(oldbackpoint);
            grow_ = false;
        }
    }

    void grow() {
        grow_ = true;
    }

private:
    /**
     * First point is head; rest is tail
     */
    std::vector<Point> points_;

    /**
     * Direction of head
     */
    Direction direction_ = RIGHT;

    /**
     * We get keys faster than the refresh rate which means we have to queue
     */
    std::queue<Direction> dirqueue_;

    /**
     * True if on next move the snake shall grow
     */
    bool grow_;
};

void draw_snake(const Snake& snake)
{
    for (auto point : snake.points()) {
        mvprintw(point.y, point.x, "O");
    }
}

int main() {
    char buf[100] = {0}, *s = buf;
    int ch, cnt = 0, n = 1;
    WINDOW *w;

    if ((w = initscr()) == NULL) {
        fprintf(stderr, "Error: initscr()\n");
        exit(EXIT_FAILURE);
    }
    curs_set(0);
    keypad(stdscr, TRUE);
    noecho();
    cbreak();      // disable line-buffering
    timeout(10);  // wait x milliseconds for input
    //nodelay(stdscr, TRUE);

    Snake snake0(20, 8, 4);

    long oldms = 0;
    int side = 1;
    int x = 1;
    int delta_y = 0;
    int p1x = 0;
    int p1y = 8;
    int p2x = 0;
    int p2y = 9;
    while (n != 0) {

        using namespace std::chrono;
        long curms = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        if (curms - oldms > 50) { // our snake will not speed up even if we press buttons fast
            oldms = curms;
            cnt++;

            // Next step for snake and make sure that vertical movement is slowed down a bit
            // to create the impression of equal speed
            if ((snake0.dir() == LEFT || snake0.dir() == RIGHT) && cnt % 2 == 0) {
                snake0.next_step();
            } else if ((snake0.dir() == UP || snake0.dir() == DOWN) && cnt % 3 == 0) {
                snake0.next_step();
            }

            if (x == 0 || x == 30) {
                side = -side;
            }
            if (side == 1) {
                x++;
            } else {
                x--;
            }
        }

        erase();
        mvprintw(0, 0, "cnt: %d", cnt);
        mvprintw(1, 0, "buf: %s", buf);
        mvprintw(2, 0, "int: %d", n);
        mvprintw(3, 0, "int: %d", curms);
        mvprintw(4 + delta_y, x, "Hallo");
        mvprintw(5 + delta_y, x, "[ %s ]", buf);
        if (strcmp("Hallo", buf) == 0) {
                mvprintw(6, 0, "MATCH");
        }
        if (std::string("Hallo") == std::string(buf)) {
                mvprintw(7, 0, "MATCH");
        }
        mvprintw(p1y, p1x, "P1");
        mvprintw(p2y, p2x, "P2");
        draw_snake(snake0);
        refresh();

        // getch (with cbreak and timeout as above)
        // waits 100ms and returns ERR if it doesn't read anything.
        if ((ch = getch()) != ERR) {
            if (ch == '\n') {
                //*s = 0;
                sscanf(buf, "%d", &n);
                s = buf;
                *s = 0;
            }
            else if (ch == KEY_BACKSPACE) {
                if (s > buf)
                    *--s = 0;
            }
            else if (ch == KEY_DOWN) {
                delta_y++;
                p1y++;
                snake0.change_dir(DOWN);
            }
            else if (ch == KEY_UP) {
                delta_y--;
                p1y--;
                snake0.change_dir(UP);
            }
            else if (ch == KEY_LEFT) {
                p1x--;
                snake0.change_dir(LEFT);
            }
            else if (ch == KEY_RIGHT) {
                p1x++;
                snake0.change_dir(RIGHT);
            }
            else if (ch == 'a') {
                p2x--;
            }
            else if (ch == 'd') {
                p2x++;
            }
            else if (s - buf < (long)sizeof buf - 1) {
                *s++ = ch;
                *s = 0;
            }
        }
    }

    delwin(w);
    endwin();
    return 0;
}
