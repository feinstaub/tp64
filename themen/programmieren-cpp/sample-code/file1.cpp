// https://de.wikibooks.org/wiki/C-Programmierung:_Dateien

#include <stdio.h> // für fopen
#include <stdlib.h>

// für open
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h> // für write, read
#include <cstring>

void writefile()
{
    FILE *datei = fopen ("testdatei.txt", "w");
    if (datei == NULL) {
        printf("Fehler beim Öffnen der Datei");
        exit(1);
    }
    fprintf(datei, "Hallo, Welt\n");
    fclose(datei);
}

void readfile1()
{
    FILE *datei = fopen("testdatei.txt", "r");

    if (datei != NULL) {
        char text[100+1];
        fscanf(datei, "%s", text);  // %c: einzelnes Zeichen %s: Zeichenkette
        // String muss mit Nullbyte abgeschlossen sein
        text[100] = '\0';
        printf("%s\n", text);
        fclose(datei);
    }
}

void readfile2()
{
    FILE *datei = fopen("testdatei.txt", "r");

    if (datei != NULL) {
        char* text;
        size_t buflen = 0;
        size_t textlen = getline(&text, &buflen, datei); // liest auch das Zeilenende
        if (textlen == -1) {
            printf("Fehler beim Lesen der Datei");
            exit(1);
        }
        if (textlen != 0) {
            if (text[textlen - 1] == '\n') {
                text[textlen - 1] = 0; // Zeilenende entfernen
            }
            printf("buflen: %d, content: %s\n", buflen, text);
        }
        free(text); // valgrind --leak-check=full ./a.out
        fclose(datei);
    }
}

void writeread3()
{
    /* Zum Schreiben öffnen */
    int fd = open("testfile3.txt", O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
    if (fd == -1) {
        printf("Problem opening file\n");
        exit(-1);
    }

    const char *s = "Test-Text 0123\n";
    write(fd, s, strlen(s));
    close(fd);

    /* Zum Lesen öffnen */
    fd = open("testfile3.txt", O_RDONLY);
    if (fd == -1) {
        exit(-1);
    }

    printf("Oktal\tDezimal\tHexadezimal\tZeichen\n");
    char ret;
    while (read(fd, &ret, sizeof(char)) > 0) {
        printf ("%o\t%u\t%x\t\t%c\n", ret, ret, ret, ret);
    }
    close(fd);
}

int main (void)
{
    writefile();
    readfile1();
    readfile2();
    writeread3();
    return 0;
}
